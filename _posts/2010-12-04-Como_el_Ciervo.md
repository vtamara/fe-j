---
layout: post
title: "Como_el_Ciervo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Como el ciervo busca por las aguas, %%%
así clama mi alma por ti Señor. %%%
Día y noche yo tengo sed de ti %%%
solo a ti buscaré. %%%


//Lléname, lléname Señor, %%%
dame más, más de tu amor; %%%
yo tengo sed sólo de ti, lléname Señor// %%%

Como el ciervo busca por las aguas, %%%
así clama mi alma por ti Señor. %%%
Día y noche yo tengo sed de ti %%%
solo a ti buscaré. %%%

//Lléname, lléname Señor, %%%
dame más, más de tu amor; %%%
yo tengo sed sólo de ti, lléname Señor// %%%
 

* Letra de: http://www.tusacordes.com/secciones/ver_tema.php?id=10838
* Pista de dominio público interpretada y editada por Omar Baracaldo
** [Formato ogg ](http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/57-Como_el_ciervo.ogg), [Formato mp3 ](http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/57-Como_el_ciervo.mp3)
