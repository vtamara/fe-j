---
layout: post
categories:
- prédica
title: Ser testimonio de Cristo. 1 Juan 5:1-13
author: vtamara
image: "/assets/images/it-is-finished-16094529692xj.jpg"

---
# Ser testimonio de Cristo. 1 Juan 5:1-13

## Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 18.Jul.2021

## 1 Introducción

El Señor me ha regalado estos versículos sobre Jesús, Dios trino y su aplicación a la evangelización.

## 2. Texto: 1 Juan 5:1-13

1 Todo aquel que cree que Jesús es el Cristo, es nacido de Dios; y todo aquel que ama al que engendró, ama también al que ha sido engendrado por él.

2 En esto conocemos que amamos a los hijos de Dios, cuando amamos a Dios, y guardamos sus mandamientos.

3 Pues este es el amor a Dios, que guardemos sus mandamientos; y sus mandamientos no son gravosos.

4 Porque todo lo que es nacido de Dios vence al mundo; y esta es la victoria que ha vencido al mundo, nuestra fe.

5 ¿Quién es el que vence al mundo, sino el que cree que Jesús es el Hijo de Dios?

6 Este es Jesucristo, que vino mediante agua y sangre; no mediante agua solamente, sino mediante agua y sangre. Y el Espíritu es el que da testimonio; porque el Espíritu es la verdad.

7 Porque tres son los que dan testimonio en el cielo: el Padre, el Verbo y el Espíritu Santo; y estos tres son uno.

8 Y tres son los que dan testimonio en la tierra: el Espíritu, el agua y la sangre; y estos tres concuerdan.

9 Si recibimos el testimonio de los hombres, mayor es el testimonio de Dios; porque este es el testimonio con que Dios ha testificado acerca de su Hijo.

10 El que cree en el Hijo de Dios, tiene el testimonio en sí mismo; el que no cree a Dios, le ha hecho mentiroso, porque no ha creído en el testimonio que Dios ha dado acerca de su Hijo.

11 Y este es el testimonio: que Dios nos ha dado vida eterna; y esta vida está en su Hijo.

12 El que tiene al Hijo, tiene la vida; el que no tiene al Hijo de Dios no tiene la vida.

13 Estas cosas os he escrito a vosotros que creéis en el nombre del Hijo de Dios, para que sepáis que tenéis vida eterna, y para que creáis en el nombre del Hijo de Dios.

## 3. Contexto y análisis

Estos versículos resumen el plan de salvación y concuerdan prácticamente con toda la Biblia

| Versículos | Observación |
| --- | --- |
| 1 | “Mas a todos los que le recibieron, a los que creen en su nombre, les dio potestad de ser hechos hijos de Dios; ” Juan 1:12 |
| 2 | “ Jesús le dijo: Amarás al Señor tu Dios con todo tu corazón, y con toda tu alma, y con toda tu mente. Este es el primero y grande mandamiento. Y el segundo es semejante: Amarás a tu prójimo como a ti mismo.” Mateo 22:37-39Por esto cumplir los mandamientos de Dios incluye amar al otro. |
| 3 | “Si me amáis, guardad mis mandamientos.” Juan 14:15“Llevad mi yugo sobre vosotros, y aprended de mí, que soy manso y humilde de corazón; y hallaréis descanso para vuestras almas;porque mi yugo es fácil, y ligera mi carga" Mateo 11:29 |
| 4 | “ Antes, en todas estas cosas somos más que vencedores por medio de aquel que nos amó.” Romanos 8:37“Mas gracias sean dadas a Dios, que nos da la victoria por medio de nuestro Señor Jesucristo. ” 1 Corintios 15:57 |
| 5 | Juan 3:16 |
| 6 | Matthew Henry interpreta que dado que estamos contaminados interior y exteriormente, necesitamos 2 componentes espirituales: (1) agua para ser limpiados interiormente y (2) la sangre de Cristo en nuestro exterior para purgar el pecado y poder estar unidos y en la presencia de Dios. “Y casi todo es purificado, según la ley, con sangre; y sin derramamiento de sangre no se hace remisión.” Hebreos 9:22“Pero cuando venga el Espíritu de verdad, él os guiará a toda la verdad; porque no hablará por su propia cuenta, sino que hablará todo lo que oyere, y os hará saber las cosas que habrán de venir. ” Juan 16:13 |
| 7 | La trinidad enunciada, se presenta más en Mateo 3:16-17 “Y Jesús, después que fue bautizado, subió luego del agua; y he aquí los cielos le fueron abiertos, y vio al Espíritu de Dios que descendía como paloma, y venía sobre él. Y hubo una voz de los cielos, que decía: Este es mi Hijo amado, en quien tengo complacencia.” Cada uno da testimonio de Jesús. |
| 8 | Matthew henry interpretaEspíritu: “A este Espíritu pertenece no sólo la regeneración y la conversión de la iglesia, sino su santificación progresiva, victoria sobre el mundo, su paz, y amor, y gozo y toda la gracia por la cual esta se prepara para la herencia de los santos en la luz”Agua: “La pureza activa y real y santidad de sus discípulos … y el bautismo“Sangre: Con el sacrificio de Jesús por mis pecados tengo certeza de su amor sin condición y real. Nuestro amor por los demás también debe ser genuino |
| 9 | El testimonio desde el cielo es mayor que el que podemos dar los seres humanos --pero no podemos dejar de hacer nuestra parte. |
| 10 | Creer que Jesús es Dios nos hace portadores de ese testimonio y la responsabilidad que implica. |
| 11-12 | Tener vida eterna por la fe en Jesús implica dar testimonio. |
| 13 | Creer en Jesús es más profundo de lo que solemos entender por creer, es maravilloso y Santo. |

## 4. Conclusión y oración

Aquí vemos punto centrales para una evangelización efectiva:

* La intervención celestial de nuestro Dios trino convenciendo de pecado, mediante señales y prodigios y convenciendo que Jesús es el camino, la verdad y la vida.
* Nuestro testimonio requiere 3 partes: (1) pureza como el agua, (2) amor sacrificial y genuino como la sangre de Cristo, (3) santidad progresiva de la iglesia de la que hacemos parte.

Oremos:

Gracias Dios todopoderoso por la revelación de que Jesús es Dios (trino), de que se ha sacrificado por limpiar mis pecados y que creyendo esto tengo vida eterna. Gracias por Tu Santo Espíritu que siento habitar en mí y me confirma todo esto. Gracias por Tu bendita Palabra que me confirma todo esto y me enseña como vivir.

Señor quiero ser testimonio de que tengo vida eterna por creer en Jesús como Dios, Señor y Salvador. Por favor ayudame a dar testimonio de pureza (agua), amor genuino y sacrificial por los demás (sangre) y que en conjunto como iglesia avancemos en la santificación (Espíritu), para que el mundo Te conozca, pido en el nombre de Jesús.

La vida cristiana comienza con esa aceptación de que Jesús es Dios y como Señor es a quien debo obedecer. Si aún no lo ha hecho, le invito a hacer una Oración de Fe.

## 5. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* Matthew Henry Bible Commentary (complete). 1 John 5. [https://www.christianity.com/bible/commentary.php?com=mh&b=62&c=5](https://www.christianity.com/bible/commentary.php?com=mh&b=62&c=5 "https://www.christianity.com/bible/commentary.php?com=mh&b=62&c=5")

Esta prédica se cede al dominio público de acuerdo a la legislación colombiana

[https://www.pasosdejesus.org/dominio_publico_colombia.html](https://www.pasosdejesus.org/dominio_publico_colombia.html "https://www.pasosdejesus.org/dominio_publico_colombia.html")

Imagen de portada de dominio público disponible en: [https://www.publicdomainpictures.net/en/view-image.php?image=377134&picture=it-is-finished](https://www.publicdomainpictures.net/en/view-image.php?image=377134&picture=it-is-finished "https://www.publicdomainpictures.net/en/view-image.php?image=377134&picture=it-is-finished")