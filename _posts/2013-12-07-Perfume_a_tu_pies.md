---
layout: post
title: "Perfume_a_tu_pies"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En Espíritu y en Verdad

<pre>
Cuando pienso en Tu amor
y en Tu fidelidad
no puedo hacer mas que postrarme
y adorar
y cuando pienso en como he sido
y hasta donde me has traído
me asombro de Ti

CORO
//No me quiero conformar
he probado y quiero mas//
Yo quiero enamorarme más de Ti
enséñame a amarte y a vivir
conforme a tu justicia y tu verdad
con mi vida quiero adorar.
Todo lo que tengo y lo que soy
todo lo que he sido te lo doy
que mi vida sea para Ti
como un perfume a Tus pies

Cuando pienso en Tu cruz
y en todo lo que has dado
Tu sangre por mi
por borrar mi pecado
y cuando pienso en Tu mano
hasta aquí hemos llegado
por Tu fidelidad

CORO

</pre>

* Letra basada en: http://www.musica.com/letras.asp?letra=1266827
* Video: [http://www.youtube.com/watch?v=D1sHJN8Fp84]
