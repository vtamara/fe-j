---
layout: post
title: "Texto_hebreo_antiguo_decifrado"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En estos días, el profesor Gershon Galil (Universidad de Haifa) testifica que  ha descifrado un [Texto Hebreo Antiguo] que reportamos en Abril de 2009, de hecho es el texto hebreo más antiguo que se conoce hasta el momento.

[http://www.redorbit.com/modules/imglib/download.php?Url=/modules/news/upload/b67acf4eda9e77d153ebac3bf199744b.jpg]

[http://www.redorbit.com/modules/imglib/download.php?Url=/modules/news/upload/1b8fb494e2a99c79e89b2e72e541f7e6.jpg]

Según el profesor Galil dice {1}:

<pre>
1' ,l t?&#347; w?bd ,![t ?.?]
2' -pt&#61477; ![?]b![d] w,lm![n] -pt&#61477; yt![m]
3' ![w]gr ![r]b ?ll rb ![d]l w
4' ,![l]mn -qm ybd mlk
5' ,![b]yn ![w]?bd -k gr t![mk]
 
1' you shall not do ![it], but worship the ![Lord].
2' Judge the sla![ve] and the wid![ow] / Judge the orph![an] 
3' ![and] the stranger. ![Pl]ead for the infant / plead for the po![or and]
4' the widow. Rehabilitate ![the poor] at the hands of the king.
5' Protect the po![or and] the slave / ![supp]ort the stranger.

1' no debes hacerlo, sino que debes alabar al Señor
2' Juzga al esclavo y a la viudad / Juzga al huerfano
3' y al extranjero.  Defiende al niño, defiede al pobre y 
4' a la viuda.  Rehabilita al probre en las manos de rey.
5' Proteje al pobre y al esclavo / apoya al extranjero.

</pre>

Según {2}, las pruebas de Carbono 14 realizadas al Ostracon confirman que es del año
1000aC, y la Universidad de Haifa emitió un comunicado según el cual:
* Se trata del escrito Hebreo más antiguo que se conoce 
* Tal inscripción hebrea hace viable que la Biblia se hubiera escrito cientos de años antes de los estimados actuales.

También confirmarían la existencia de Israel para la época y junto con la fortaleza de Ela donde fue encontrado, confirmarían la monarquia unida .


!Referencias

* {1} http://www.israelnationalnews.com/News/News.aspx/135432
* {2} http://www.redorbit.com/news/science/1806444/oldest_hebrew_writing_found_deciphered/
