---
layout: post
categories: []
title: Diligentes en la Iglesia. Mateo 25:14-30
author: vtamara
image: "/assets/images/unnamed.png"

---
# Diligentes en la Iglesia. Mateo 25:14-30

## Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 3.Abr.2022

# 1 Introducción

Cuando oré pidiendo que compartir en esta prédica, a mi corazón llegó “En lo poco fuiste fiel, en lo mucho te pondré”, al respecto también predicaré para mi.

# 2. Texto: Mateo 25:14-30

14 Porque el reino de los cielos es como un hombre que yéndose lejos, llamó a sus siervos y les entregó sus bienes. 15 A uno dio cinco talentos, y a otro dos, y a otro uno, a cada uno conforme a su capacidad; y luego se fue lejos. 16 Y el que había recibido cinco talentos fue y negoció con ellos, y ganó otros cinco talentos. 17 Asimismo el que había recibido dos, ganó también otros dos. 18 Pero el que había recibido uno fue y cavó en la tierra, y escondió el dinero de su señor. 19 Después de mucho tiempo vino el señor de aquellos siervos, y arregló cuentas con ellos. 20 Y llegando el que había recibido cinco talentos, trajo otros cinco talentos, diciendo: Señor, cinco talentos me entregaste; aquí tienes, he ganado otros cinco talentos sobre ellos. 21 Y su señor le dijo: Bien, buen siervo y fiel; sobre poco has sido fiel, sobre mucho te pondré; entra en el gozo de tu señor. 22 Llegando también el que había recibido dos talentos, dijo: Señor, dos talentos me entregaste; aquí tienes, he ganado otros dos talentos sobre ellos. 23 Su señor le dijo: Bien, buen siervo y fiel; sobre poco has sido fiel, sobre mucho te pondré; entra en el gozo de tu señor. 24 Pero llegando también el que había recibido un talento, dijo: Señor, te conocía que eres hombre duro, que siegas donde no sembraste y recoges donde no esparciste; 25 por lo cual tuve miedo, y fui y escondí tu talento en la tierra; aquí tienes lo que es tuyo. 26 Respondiendo su señor, le dijo: Siervo malo y negligente, sabías que siego donde no sembré, y que recojo donde no esparcí. 27 Por tanto, debías haber dado mi dinero a los banqueros, y al venir yo, hubiera recibido lo que es mío con los intereses. 28 Quitadle, pues, el talento, y dadlo al que tiene diez talentos. 29 Porque al que tiene, le será dado, y tendrá más; y al que no tiene, aun lo que tiene le será quitado. 30 Y al siervo inútil echadle en las tinieblas de afuera; allí será el lloro y el crujir de dientes.

# 3. Contexto

Los pasajes que preceden y siguen son:

| --- | --- |
| Versículos | Título en RV1960 |
| 24:3-28 | Señales antes del fin |
| 24:29-50 | La venida del Hijo del Hombre |
| 25:1-13 | Parábola de las 10 vírgenes |
| 25:14-30 | Parábola de los talentos |
| 25:30-46 | El juicio a las naciones |

Así que el Señor se está refiriendo a asuntos importantes relacionados con los tiempos finales y su advenimiento.

En los versículos 15 y otros donde se habla de talentos, vemos en \[Thayer-BLB\] que proviene de la palabra griega “τάλαντον” que solía ser una unidad de peso, pero en este contexto es “una suma de dinero que pesaba un talento”

* Un talento ático correspondía a 60 minas áticas o 6000 dracmas. Según cita de \[Thayer-BLB\] podía corresponder en la actualidad a unos US$1.000
* Un talento de plata en Israel pesaba 45Kg
* Un talento de oro en Israel pesaba 90Kg

  
Así que la cantidad de bienes que el Señor había entregado a cada siervo, sería más considerable que una moneda, podemos pensar que cada talento serían alrededor de $4’000.000 COP.

![](https://lh3.googleusercontent.com/4UzFDhB4DDOAvdJNGnSqC8hsv8GC9fo6N5N8ls_4na_frxgz5dugjhf6XKKunlAkkYOHRXc0HZAmh3NYvKwgx1ilM1uMC9iuGgEPI4iFLqLQcwu9HK-tLXX5miyZy9L9o63l-z9C =199x199)

Dibujo de https://thumbs.dreamstime.com/z/character-pushing-wheelbarrow-full-gold-ingots-d-illustrat-character-pushing-wheelbarrow-full-gold-ingots-d-illustration-123951609.jpg 


Al final el siervo de los 5 talentos tendría algo equivalente a unos $44'000.000 COP. ¿Por qué?

# 4. Análisis

Me baso en la interpretación de Matthew Henry:

| --- | --- |
| Vers. | Observación |
| 14 | El hombre que va de viaje sería Jesucristo |
| 15 | Su muerte y su resurrección serían su partida al viaje, y había dejado constituida la iglesia, dejándole de manera exclusiva el Espíritu Santo, que en cada creyente corresponde a dones particulares además del fruto del Espíritu y las habilidades y oportunidades que Dios da a cada ser humano. |
| 16 | El primero apenas recibió sus dones y habilidades empezó a utilizarlos para la expansión del Reino por ejemplo compartiendo el evangelio como haciendo obras de caridad. Se me ocurre que también podrían representar personas a cargo, inicialmente le encargaron 5 y con su evangelización trajo a la vida cristiana a otros 5. |
| 17 | El segundo también fue diligente |
| 18 | Enterrar los talentos, como el tercero, es tener capacidades y oportunidades para hacer algo bueno (como compartir el evangelio o hacer obras de caridad), pero no energizar los dones interiores y habilidades que tenemos para emplearlos en las oportunidades. |
| 19 | El regreso sería el segundo advenimiento de Cristo que se aproxima. Jesús nos llamará a rendir cuentas, esperando más de aquellos a quienes más les ha dado. Jesús espera que cada quien haga su mejor esfuerzo de acuerdo a su capacidad y oportunidad |
| 20 y 22 | Los diligentes dieron cuenta de lo que habían recibido y de lo que había producido. |
| 21 y 23 | Cristo alaba al diligente en los asuntos de la Igleisa, le comisiona más y le da la bienvenida al Reino |
| 24-25 | Al que no hace lo que debía lo condena su propia boca. Matthew Henry dice “Que aquellos que tienen menos que hacer por Dios, con frecuencia hacen aún menos. Algunos convierten en una excusa para su pereza, que no tienen las mismas oportunidades de servir a Dios que otros tienen; Y como no tienen forma de hacer lo que dicen que harían, no hacen lo que estamos seguros que pueden hacer, y sólo se sientan y no hacen nada. Es realmente una agravante a su pereza, que tan sólo teniendo que cuidar un talento, lo descuiden” |
| 26 | La pereza del tercero se convierte en maldad, así que mejor no dejemos de hacer lo bueno que podemos hacer. |
| 27 | Jesús le muestra que al menos hubiera podido acudir a otros que algo hicieran con lo que tenía a cargo. |
| 28-29 | El que es responsable y diligente tendrá a cargo más con el honor y alabanza de parte de Dios que eso implica. Pero a quien quiera hacer el mal, no le quedarán ni palabras de consuelo de Dios. |
| 30 | Para el inútil en los asuntos del Reino de Dios no hay otro lugar que el infierno. |

# 5. Conclusión y oración

Se acerca el momento en el que cada uno tendrá que rendirle cuentas a Dios y seremos bien como los siervos diligentes que hicieron lo mejor que pudieron con lo que Dios les dió y entraron a su Reino o como el siervo perezoso y malo que fue arrojado al infierno. Ciertamente no somos salvos por obras, sino por la fe en Jesús pero los menonitas creemos que una fe obrante de los hacedores, no de oidores, ni de los habladores que sólo dicen “Señor, Señor” pero no le obedecen.

Recordemos que para servir en la iglesia invitando a más personas a creer y seguir a Jesús, antes y en simultáneo debemos ser diligentes con lo que Dios nos ha dado, comenzando con nuestra relación con Él, pasando por la familia y el trabajo.

Hagamos un pequeño ejercicio de reflexión personal en 3 minutos, preferible por escrito, indicando cómo puedes usar o mejorar el uso del don en cada oportunidad que tienes:

| Don/Oportunidad | Familia |  | Trabajo |  | Iglesia |  |
|  | Op 1 | Op 2 | Op 1 | Op 2 | Op 1 | Op 2 |
| --- | --- | --- | --- | --- | --- | --- |
| Don/hab 1 |  |  |  |  |  |  |
| Don/hab 2 |  |  |  |  |  |  |
| Don/hab 3 |  |  |  |  |  |  |

Gracias Dios por los dones, habilidades y oportunidades que nos has dado a cada uno. 
Por favor abre nuestros ojos para identificarlos bien y guianza para planear 
cómo y cuando mejorar el uso de cada don y habilidad en cada oportunidad. 
Gracias por recordarme la importancia de ser diligente. En el nombre de Jesús.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* \[Thayer-BLB\] [https://www.blueletterbible.org/lexicon/g5007/kjv/tr/0-1/](https://www.blueletterbible.org/lexicon/g5007/kjv/tr/0-1/ "https://www.blueletterbible.org/lexicon/g5007/kjv/tr/0-1/")
* [https://gardenofpraise.com/bibl52s.htm](https://gardenofpraise.com/bibl52s.htm "https://gardenofpraise.com/bibl52s.htm")
* Matthew Henry’s Commentary. <https://www.biblegateway.com/resources/matthew-henry/toc/>
  
  
La imagen es de  [http://josmomnaba.blogspot.com/2017/11/los-talentos.html?spref=pi](http://josmomnaba.blogspot.com/2017/11/los-talentos.html?spref=pi "http://josmomnaba.blogspot.com/2017/11/los-talentos.html?spref=pi")
