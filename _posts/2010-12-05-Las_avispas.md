---
layout: post
title: "Las_avispas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Juan Luis Guerra


Tengo un Dios admirable en los cielos %%%
y el Amor de su Espíritu Santo %%%
por su Gracia yo soy hombre nuevo %%%
y de gozo se llena mi canto %%%

De su imagen soy un reflejo (oh, oh)%%%
que me lleva por siempre en victoria (oh, oh), %%%
y me ha hecho cabeza y no cola %%%
en mi Cristo yo todo lo puedo %%%

Eh, Jesús me dijo %%%
que me riera %%%
si el enemigo %%%
me tienta en la carrera. %%%
Y también me dijo %%%
no te mortifiques %%%
que yo le envío %%%
mis avispas pa que lo piquen, %%%
es verdad %%%

Tengo un Dios admirable en los cielos %%%
que me libra de mal y temores %%%
es mi roca y mi gran fortaleza %%%
y me colma con sus bendiciones %%%

Mi Señor siempre me hace justicia (oh, oh) %%%
y me defiende de los opresores (oh, oh) %%%
no me deja ni me desampara %%%
pues mi Dios es Señor de señores %%%

Eh, Jesús me dijo %%%
que me riera %%%
si el enemigo %%%
me tienta en la carrera. %%%
Y también me dijo %%%
no te mortifiques %%%
que yo le envío %%%
mis avispas pa que lo piquen, %%%
es verdad %%%

Jesús me dijo %%%
ya lo ves %%%
que me riera %%%
si el enemigo %%%
me tienta en la carrera %%%
y también me dijo %%%
no te mortifiques %%%
tú lo ves %%%
que yo le envío %%%
mis avispas pa que lo piquen %%%
¡oh, oye! %%%

¡Avispas! %%%

Pa que lo piquen %%%
en la cara, ves %%%
pa que lo piquen %%%
y en los pies %%%
que yo le envío %%%
mis avispas pa que lo piquen %%%
lo piquen otra vez %%%

pa que lo piquen %%%
en la carretera %%%
pa que lo piquen %%%
y en medio de la acera %%%
que yo le envío %%%
mis avispas pa que lo piquen %%%
sí %%%

lo piquen, lo piquen, lo piquen y lo piquen %%%
lo piquen en el dedo más chiquito pa que afinque %%%
lo piquen, lo piquen, lo piquen y lo piquen %%%
lo piquen en el coco pa que salte como un lince %%%
lo piquen, lo piquen, lo piquen y lo piquen %%%
Señor ... %%%
lo piquen, lo piquen, lo piquen y lo piquen %%%
lo piquen en la cara pa que no me mortifique %%%

lo piquen, lo piquen, lo piquen y lo piquen %%%
que lo piquen en los huesos pa que salte y pa que brinque %%%
lo piquen, lo piquen, lo piquen y lo piquen %%%
donde quiera que se meta yo quisiera que lo piquen %%%
lo piquen, lo piquen, lo piquen y lo piquen %%%
Señor ... %%%
lo piquen, lo piquen, lo piquen y lo piquen, %%%
lo piquen en la cara pa que no me mortifique %%%

Jesús me dijo %%%
que me riera %%%
si el enemigo %%%
me tienta en la carrera. %%%
Y también me dijo %%%
no te mortifiques %%%
que yo le envío %%%
mis avispas pa que lo piquen, %%%
es verdad %%%

Jesús me dijo %%%
ya lo ves %%%
que me riera %%%
si el enemigo %%%
me tienta en la carrera %%%
y también me dijo %%%
no te mortifiques %%%
que yo le envío %%%
mis avispas pa que lo piquen %%%
¡oye! %%%

----
* Letra: http://www.musica.com/letras.asp?letra=60105
* Vídeo: http://www.youtube.com/watch?v=Ig9ZEXIUKiU
* Midi: http://www.destellodesugloria.org/MIDIS/Lasavispas.mid
