---
layout: post
title: "Felipe_Apostol"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
## Nombre

Ver [Felipe]

Uno de los doce apostoles.

##Genealogía

| Parentezco | Persona | Referencia |
| Papá | | |
| Mamá | | |

##Datos en la Biblia

##Evidencias arqueológicas

##Referencias


* {1} Easton Bible Dictionary. 1897.  http://www.ccel.org/ccel/easton/ebd2.html  Dominio Público.
* {2}  http://en.wikipedia.org/wiki/Philip_the_Apostle
* {3} http://www.evidenceofgod.com/addendums/Chapter%2036%20Addendum.pdf
