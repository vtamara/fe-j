---
layout: post
title: Adoracion Cuan grande es El
author: vtamara
categories:
- Alabanza
image: "/assets/images/98-01.jpg"
tags: Alabanza

---

Melodia popular sueca adaptada por Carl Boberg

Traducción a español de Arturo Hotton

<pre> Señor, mi Dios, al contemplar los cielos, El firmamento y las estrellas
mil. Al oír tu voz en los potentes truenos Y ver brillar el sol en su cenit.

CORO Mi corazón entona esta canción Cuan grande es Él, cuan grande es Él. Mi
corazón entona esta canción Cuan grande es Él. Cuan grande es Él.

Al recorrer los montes y los valles Y ver las bellas flores al pasar. Al
escuchar el canto de las aves Y el murmu
