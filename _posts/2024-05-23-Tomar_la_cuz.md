---
layout: post
categories:
- Predica
title: Tomar la cruz
author: vtamara
image: "/assets/images/tomar_la_cruz.jpg"

---
# Tomar la cruz

## Iglesia Menonita de Suba. Estudio Bíblico.
[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 22.May.2024.
Domínio público

[Imagen de dominio público de Ejov Igor](https://www.pexels.com/photo/sculpture-of-jesus-with-cross-14080464/)


## 1 Introducción

El Señor me regaló una interpretación de lo que significa Tomar la cruz, que
deseo compartir hoy.


## 2. Texto: Lc. 9:23-27


<blockquote>
<sup>23</sup> Y decía a todos: Si alguno quiere venir en pos de mí, niéguese
a sí mismo, tome su cruz cada día, y sígame. <sup>24</sup> Porque todo el que
quiera salvar su vida, la perderá; y todo el que pierda su vida por causa de mí,
este la salvará. <sup>25</sup> Pues ¿qué aprovecha al hombre, si gana todo el
mundo, y se destruye o se pierde a sí mismo? <sup>26</sup> Porque el que se
avergonzare de mí y de mis palabras, de este se avergonzará el Hijo del Hombre
cuando venga en su gloria, y en la del Padre, y de los santos ángeles.
<sup>27</sup> Pero os digo en verdad, que hay algunos de los que están aquí, que
no gustarán la muerte hasta que vean el reino de Dios.
</blockquote>


## 3. Contexto


### 3.1 Pasajes alrededor


<table>
  <tr>
   <td><strong>Versiculos</strong>
   </td>
   <td><strong>Tema</strong>
   </td>
  </tr>
  <tr>
   <td>9:1-6
   </td>
   <td>Misión de los doce
   </td>
  </tr>
  <tr>
   <td>9:7-9
   </td>
   <td>Muerte de Juan el Bautista
   </td>
  </tr>
  <tr>
   <td>9:10-17
   </td>
   <td>Alimentación de 5000
   </td>
  </tr>
  <tr>
   <td>9:18-20
   </td>
   <td>Pedro confiesa que Jesús es el Mesías
   </td>
  </tr>
  <tr>
   <td>9:21-22
   </td>
   <td>Profecía de Jesús sobre su muerte
   </td>
  </tr>
  <tr>
   <td>9:23-27
   </td>
   <td>Tomar la cruz. Lo único valioso. Vergüenza. Profecía de que verían su
reino.
   </td>
  </tr>
  <tr>
   <td>9:28-36
   </td>
   <td>La Transfiguración
   </td>
  </tr>
  <tr>
   <td>9:37-43
   </td>
   <td>Jesús sana a un muchacho endemoniado
   </td>
  </tr>
  <tr>
   <td>43b-45
   </td>
   <td>Jesús anuncia nuevamente su muerte
   </td>
  </tr>
  <tr>
   <td>46-48
   </td>
   <td>Enseñanza sobre quien es el mayor
   </td>
  </tr>
  <tr>
   <td>49-50
   </td>
   <td>El que no está contra nosotros, por nosotros es
   </td>
  </tr>
  <tr>
   <td>51-56
   </td>
   <td>Jesús reprende a Jacobo y Juan
   </td>
  </tr>
  <tr>
   <td>57-62
   </td>
   <td>Los que querian seguir a Jesús
   </td>
  </tr>
</table>

Impacta ver el poder de Jesús expresado en multiplicación de alimentos,
sanidades, transfiguración, revelación enseñanza de poder y que Él
mismo limita para morir por mi y por ti como profetizó en versículos 21-22.


### 3.2 Textos concordantes


#### 3.1.1 Mt 16:24-28

<blockquote>
<sup>24</sup> Entonces Jesús dijo a sus discípulos: Si alguno quiere venir en
pos de mí, niéguese a sí mismo, y tome su cruz, y sígame. <sup>25</sup> Porque
todo el que quiera salvar su vida, la perderá; y todo el que pierda su vida por
causa de mí, la hallará. <sup>26</sup> Porque ¿qué aprovechará al hombre, si
ganare todo el mundo, y perdiere su alma? ¿O qué recompensa dará el hombre por
su alma? <sup>27</sup> Porque el Hijo del Hombre vendrá en la gloria de su Padre
con sus ángeles, y entonces pagará a cada uno conforme a sus obras.
<sup>28</sup> De cierto os digo que hay algunos de los que están aquí, que no
gustarán la muerte, hasta que hayan visto al Hijo del Hombre viniendo en su
reino.
</blockquote>

#### 3.1.2 Mr 8:34-9:1

<blockquote>
<sup>34</sup> Y llamando a la gente y a sus discípulos, les dijo: Si alguno
quiere venir en pos de mí, niéguese a sí mismo, y tome su cruz, y sígame.
<sup>35</sup> Porque todo el que quiera salvar su vida, la perderá; y todo el
que pierda su vida por causa de mí y del evangelio, la salvará. <sup>36</sup>
Porque ¿qué aprovechará al hombre si ganare todo el mundo, y perdiere su alma?
<sup>37</sup> ¿O qué recompensa dará el hombre por su alma?

<sup>38</sup> Porque el que se avergonzare de mí y de mis palabras en esta
generación adúltera y pecadora, el Hijo del Hombre se avergonzará también de él,
cuando venga en la gloria de su Padre con los santos ángeles.  <sup>9:1</sup>
También les dijo: De cierto os digo que hay algunos de los que están aquí, que
no gustarán la muerte hasta que hayan visto el reino de Dios venido con poder.
</blockquote>


## 4. Análisis

### 4.1 Comparación de textos concordantes

<table>
  <tr>
   <td><strong>Lc</strong>
   </td>
   <td><strong>Mt</strong>
   </td>
   <td><strong>Mr</strong>
   </td>
   <td><strong>Observación</strong>
   </td>
  </tr>
  <tr>
   <td>23
   </td>
   <td>24
   </td>
   <td>34
   </td>
   <td>Mt. dice que Jesús se dirigió sólo a los discípulos, mientras que Lc y Mr dicen que a todos. Idéntico contenido de como
seguir a Jesús en 3 pasos: (1) negarme (hacer voluntad de Dios y no la mia como
indicaba en [Cargar la Cruz 2014]), 
(2) tomar mi cruz, (3) seguirlo
   </td>
  </tr>
  <tr>
   <td>24
   </td>
   <td>25
   </td>
   <td>35
   </td>
   <td>

1. Quiero salvar mi vida => La pierdo

2. Pierdo mi vida por Jesús (y evangelio) => seré salvo (encontraré vida).

Me recuerda la hermosa y breve canción de la hermana católica Glenda, 
Quien pierde su vida por mi <https://www.youtube.com/watch?v=-6B2DRSL0EU>

   </td>
  </tr>
  <tr>
   <td>25
   </td>
   <td>26
   </td>
   <td>36
   </td>
   <td>No sirve ganar el mundo si uno se pierde (o pierde su alma)
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>27
   </td>
   <td>
   </td>
   <td>Jesús irá al cielo con el Padre y los ángeles y dará a cada quien lo que
merece
   </td>
  </tr>
  <tr>
   <td>26
   </td>
   <td>
   </td>
   <td>37
   </td>
   <td>Si ahora me avergüenzo de Jesús, Él se avergonzará de mi cuando esté en
el cielo.
   </td>
  </tr>
  <tr>
   <td>27
   </td>
   <td>28
   </td>
   <td>9:1
   </td>
   <td>
Profecía: algunos de sus discípulos verían el Reino antes de morir.

Más claro en Mt. verían a Jesús "viniendo a su Reino" se cumplió durante la
ascensión de Jesús presenciada por todos los discípulos a los que le profetizó
excepto Judas.
   </td>
  </tr>
</table>


### 4.2 ¿Qué quería decir Jesús con tomar la cruz?

Su enseñanza ocurrió después de revelarle a los discípulos que debía sufrir a
manos de los judios y romanos y morir.   Él sabía que iba a morir crucificado
(que según  [Wikipedia Crucifixión] era el método para esclavos que se rebelaban
y para romanos considerados altos traidores contra su estado) y sabía que iba a
tener que cargar su cruz con su fuerza limitada como ser humano.

Tuvo que cargar la cruz como resultado del odio del enemigo y del mundo contra
Él.

Los que seguimos a Jesús nos exponemos a lo mismo.

Entonces cargar la cruz es asumirlo y soportar los sufrimientos que el mundo y
las tretas del enemigo logren darnos por querer ser más como Jesús.


## 5. Conclusión y oración

Señor Jesús por favor ayúdame a ser menos como yo quiero y más cómo Tu eres, por
favor ayúdame a soportar los sufrimientos que eso pueda acarrear.

Cancelamos los planes del enemigo contra nosotros en el nombre de Jesús.

¿Ya hicieste una [oración de fe](https://fe.pasosdejesus.org/Oracion_de_fe/)
para recibir a Jesús como Señor y Salvador?

## 6. Referencias Bibliográficas

* [RV1960] Biblia. Traducción Reina Valera 1960.
  [https://biblegateway.com](https://biblegateway.com) 
* [Wikipedia Crucifixión]
  [https://es.wikipedia.org/wiki/Crucifixi%C3%B3n](https://es.wikipedia.org/wiki/Crucifixi%C3%B3n) 
* [Cargar la Cruz 2014] Támara Patiño, Vladimir. 2014. Cargar la Cruz.
  <https://fe.pasosdejesus.org/Cargar_la_Cruz/>
