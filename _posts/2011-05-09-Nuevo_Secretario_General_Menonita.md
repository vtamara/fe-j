---
layout: post
title: "Nuevo_Secretario_General_Menonita"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Traducción libre de {1}.


La Conferencia Mundial Menonita (CMM) ha designado a Cesar García de Bogotá, Colombia como Secretario General elegido, siendo la primera vez que la denominación elije un líder del Sur.

La designación fue una de las primeras acciones tomadas en la reunión anual del Comité Ejecutivo de la CMM llevada a cabo en Taipei, Taiwan del 4 al 11 de Mayo de 2011.

"La confirmación de la candidatura de Cesar es un momento histórico para nosotros," dijo la presidenta del CMM Danisa Ndlov.  "Es un reconocimiento de nuestra integración positiva como una comunidad de fe, mientras vemos que el Sur Global ofrece sus riquezas al Norte Global."

García, de 39 años, será sucesor de Larry Miller como Secretario General el 1 Enero de 2012.

García dijo, "Estoy emocionado por la posibilidad de servir en el liderazgo de la CMM con el propósito de orar, pensar y actuar como parte de una iglesia global de Cristo.  Dios se glorifica cuando la interdependencia cultural de su iglesia se evidencia en nuestra forma de hacer teología, practicar eclesiología y llevar testimonios cristianos al mundo."

El comité ejecutivo también aprobó mover la localización de la oficina central de CMM a Bogotá, desde Estrasburgo, Francia.

García, quien fue director de la Iglesia Hermanos Menonitas de Colombia (Mennonite Brethren Churches of Colombia) de 2002 a 2008, actualmente está completando estudios de maestría en Fresno Pacific Biblical Seminary en Fresno, California, reportó la CMM.

También sirve como secretario de la Comisión de Misiones de la CMM y como miembro de la fuerza de trabajo de la CMM para crear una nueva red de ministerios de servicio.  Además ha estado activo en los esfuerzos ecuménicos e inter-Anabautistas en Colombia. García está casado con Sandra Baez  quien también está completando estudios en Fresno.  Tienen hijas adolescentes.

La CMM representa a más de 1.6 millones de miembros en 99 iglesias Menonitas y de Hermandad en Cristo en 56 paises.

Los menonitas están entre 'las iglesias históricas de paz' y sus orígenes se remontan hasta el ala radical de la Reforma en Europa.


!Referencias

{1} http://www.ekklesia.co.uk/node/14728
