---
layout: post
title: "Jehohanan_Crucificcion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!Jehohanan

[http://www.pbs.org/wgbh/pages/frontline/shows/religion/jesus/art/nailinfoot.jpg]

[http://upload.wikimedia.org/wikipedia/commons/a/a9/Subtalar_Joint.PNG]

* 1968. Jerusalén.
* Osuario siglo I aC. 'Jehohanan hijo de HGQWL'
* Hombre de 20 años
* Hueso Calcaneo
* Clavo de 11.5cm

!Evidencia arqueológica de una crucificción

[http://www.joezias.com/foot_tb.jpg]
[http://www.joezias.com/CrucifixionAntiquity_files/image004.gif]

* Restos de madera de olivo al lado derecho
* Sin clavos en manos o brazos.  Amarrado de brazos según Zias, aunque confirma que sería excepción.
* Según Haas había rayón en radio   

!Manos, Brazos

[http://upload.wikimedia.org/wikipedia/commons/f/fe/RightHumanPosteriorRadiusUlna.jpg]
[http://www.familia2000.org/Images/clavos.gif]

* Barbet (1930). No podía ser en palma de mano. No sostiene.
* Reed (2009) 'Quest for Truth' Pies sostiene suficiente. “La crueldad de los romanos los habría guiado a encontrar las palmas de las manos como las partes más dolorosas”.

!Crucificciones recientes

[http://trurl.sandiego.edu/~najmici/zdjecia/pan_am/sokichi.jpg]
* Foto de Felice Beato fotografo inglés en Ja&#7765;on ~1865
* Sokichi. 25 años. Crucificado por matar a su empleador.
* En la actualidad es legal en Iran y se han reportado incidentes recientes en Sudan (88 en 2002), Yemen, Japón (2GM), Alemania (1GM), Irlanda del Norte (2002). 

##REFERENCIAS

* Crucifixion in Antiquity. The Anthropological Evidence. Joe Zias. http://www.joezias.com/CrucifixionAntiquity.html
* http://es.wikipedia.org/wiki/Crucifixión
