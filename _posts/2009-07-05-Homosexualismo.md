---
layout: post
title: "Homosexualismo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!Canción 'Luca era gay'

Esta canción secular, de Giuseppe Povia que quedó en 2do lugar en festival de San Remo de 2009, aunque fue muy dificil que pudiera presentarse y aún hoy hablar de la misma, por la fuerte oposición de grupos pro-gays. Música y letra disponible en video:
http://www.youtube.com/watch?v=Qh2nXrStlBE&feature=related


Basado en traducción a español disponible en {2} y mejoras con ayuda de translate.google.com


Luca era gay - Giuseppe Povia


Luca, Luca era gay e adesso sta con lei %%%
__Luca, Luca era gay y ahora está con ella__ %%%
Luca parla con il cuore in mano. Luca dice sono un altro uomo,%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%

Luca dice: prima di raccontare il mio cambiamento sessuale%%%
__Luca dice: antes de contarles mi cambio sexual__%%%
volevo chiarire che se credo in Dio; non mi riconosco nel pensiero dell uomo%%%
__quería aclarar que si creo en Dios; no me reconozco en el pensamiento del hombre__%%%
che su questo argomento ? diviso,%%%
__que sobre este argumento está tan dividido,__%%%
non sono andato da psicologi psichiatri preti o scienziati%%%
__no visité psicólogos, psiquiatras, sacerdotes, o científicos__%%%
sono andato nel mio passato ho scavato e ho capito tante cose di me%%%
__fui a mi pasado, excavé y entendi muchas cosas sobre mi__%%%

mia madre mi ha voluto troppo bene un bene diventato ossessione%%%
__mi madre me quizo mucho, un amor que se volvió una obsesión__%%%
piena delle sue convinzioni ed io non respiravo per le sue attenzioni%%%
__llena de convicciones y yo me sofocaba por sus atenciones__%%%
mio padre non prendeva decisioni ed io non ci riuscivo mai a parlare%%%
__mi padre nunca tomaba decisiones y yo no lograba nunca hablar con él__%%%
stava fuori tutto il giorno per lavoro io avevo l\u2019impressione che non fosse troppo vero%%%
__estaba todo el día fuera por trabajo, y yo tenía la impresión que no era real__%%%
mamma infatti chiese la separazione avevo 12 anni non capivo bene%%%
__mi madre pidió el divorcio, yo tenía 12 años y no entendia muy bien__%%%
mio padredisse ? la giusta soluzione e dopo poco tempo cominció a bere%%%
__mi padre dijo que era la justa solución y poco tiempo después comenzó a beber__%%%
mamma mi parlava sempre male di papá mi diceva non sposarti mai per carit&#65533; %%%
__mi madre me hablaba siempre mal de mi papá, me decía: no te cases nunca!, por caridad!__%%%
delle mie amiche era gelosa morbosa e la mia identitá era sempre pi? confusa%%%
__de mis amigas era celosa, morbosa, y mi identidad se confundia siempre más__%%%

Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano Luca dice sono un altro uomo%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%
Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano Luca dice sono un altro uomo%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%


Sono un altro uomo ma in quel momento cercavo risposte%%%
__Soy un hombre distinto (ahora) pero en aquel momento buscaba respuestas__%%%
mi vergognavo e le cercavo di nascosto c\u2019era chi mi diceva \u201c? naturale\u201d%%%
__me avergonzaba, y las buscaba a escondidas, hay gente que decia: "es natural"__%%%
io studiavo Freud non la pensava uguale%%%
__yo estudiaba a Freud y no la pensaba de ese modo__%%%
poi arrivó la maturitá ma non sapevo che cos\u2019era la felicit&#65533;%%%
__luego llego el liceo(bachillerato) pero yo no sabia que cosa era la felicidad__%%%
un uomo grande mi fece tremare il cuore ed ? lí che ho scoperto di essere omosessuale%%%
__un hombre maduro me estremeció el corazón y fue entonces que descubrí que es ser homosexual__%%%
con lui nessuna inibizione il corteggiamento c\u2019era e io credevo fosse amore%%%
__con él ninguna inhibición, él me cortejaba y yo creia que era amor__%%%
si con lui riuscivo ad essere me stesso poi sembrava una gara a chi faceva meglio il sesso%%%
__con él lograba ser yo mi mismo, luego se volvió una competencia a ver quien era mejor en el sexo__%%%
e mi sentivo un colpevole prima o poi lo prendono%%%
__y me sentia culpable, de un momento a otro lo descubren__%%%
ma se spariscono le prove poi lo assolvono%%%
__pero como las pruebas desaparecen luego lo absolven__%%%
cercavo negli uomini chi era mio padre andavo con gli uomini per non tradire mia madre%%%
__yo buscaba en los hombres quién era mi padre, estaba con los hombres para no traicionar a mi madre__%%%

Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano luca dice sono un altro uomo%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%
Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano luca dice sono un altro uomo ,%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%

Luca dice per 4 anni sono stato con un uomo tra amore e inganni%%%
__Luca dice: por 4 años estuve con un hombre entre amor y engaños__%%%
spesso ci tradivamo io cercavo ancora la mia veritá quell\u2019amore grande per l\u2019eternit&#65533;%%%
__con muchas traiciones, yo buscaba mi verdad, aquel gran amor para la eternidad__%%%
poi ad una festa fra tanta gente ho conosciuto lei che non c\u2019entrava niente%%%
__y luego en una fiesta, entre tanta gente la conocí a ella, que no tenía nada que ver__%%%
lei mi ascoltava, lei mi spogliava, lei mi capiva%%%
__ella me escuchaba, ella me desnudaba, ella me entendia__%%%
ricordo solo che il giorno dopo mi mancava%%%
__recuerdo solo que el día siguiente me hacia falta__%%%

questa ? la mia storia solo la mia storia nessuna malattia nessuna guarigione%%%
__esta es mi historia, es sólo mi historia, ninguna enfermedad, ninguna rehabilitación__%%%
caro papá ti ho perdonato anche se qua non sei pi? tornato%%%
__querido papá te he perdonado incluso si no volviste nunca__%%%
mamma ti penso spesso ti voglio bene e a volte ho ancora il tuo riflesso%%%
__mamá te pienso siempre, te quiero mucho, y a veces recuerdo tu reflejo__%%%
ma adesso sono padre e sono innamorato dell\u2019unica donna che io abbia mai amato%%%
__pero ahora soy padre y estoy enamorado de la única mujer que yo haya amado__%%%

Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano luca dice sono un altro uomo%%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%
Luca era gay e adesso sta con lei%%%
__Luca era gay y ahora está con ella__%%%
Luca parla con il cuore in mano luca dice sono un altro uomo %%%
__Luca habla con el corazón en la mano. Luca dice: soy un hombre distinto__%%%


!¿Quien discrimina a quien?

Los grupos pro-homosexuales se opusieron con vehemencia a que esta canción fuera presentada argumentando que promovía la discriminación, amenazando a los organizadores del festival con demandarlos por esto.   Una nota de prensa italiana con subtítiulos en español al respecto está disponible en:
http://www.youtube.com/watch?v=s6t3iGal98A

!Psicología y psiquiatría

La APA (Asociación de psicología de America) declaró desde 1975 que el homosexualismo ya no era una enfermedad, tras una larga campaña de incidencia de grupos pro-homosexuales.   

Actualmente (ver {3}) propone:

* Que en cambió la discriminación a los homosexuales e incluso las terapias a las que asisten son más dañinas en estas personas.
* Que una persona "define" su orientación típicamente entre la niñez y la juventud, que les resulta piscologicamente mejor "sacar" o dar a conocer su identidad abiertamente,  que para esto requieren el apoyo de su familia, colegio y grupo social.  
* Que los homosexuales no tienen "cientificamente" inconveniente para adoptar y criar hijos.
* Para evitar la discriminación entre otras  recomiendan "examinar su propio sistema de creencias revisando la presencia de estereotipos antigay"

!El amor vs. la ciencia

Una canción como "Luca era gay" muestra la prevalencia del amor sobre las confusiones, sobre las atracciones erradas, sobre los problemas de la vida y hasta sobre los "criterios científicos."  

Vivimos en un mundo donde se usa la ciencia para confundir en lugar de aclarar, donde se usa para tratar de imponer nuevas normas en las familias, en los colegios, e incluso en las creencias.  Normas antinaturales, contrarías a la realidad, a la escencia, que justifican lo errado, en malos pensamientos que el enemigo siembra. Un mundo donde la ciencia es usada por el enemigo para justificar pecados.

Sin duda las personas homosexuales al igual que todos, anhelan amor.  Y  justamente es amor lo que debemos darles, no placeres, ni engaños, ni mentiras, sólo amor puro que proviene de Dios, tal como a todo prójimo.

!Iglesia luterana 

Infortunadamente, en agosto de 2009 la Iglesia Luterana de América aprobó una resolución que permite ordenar pastores que estén activamente en relaciones homosexuales.

Sin embargo hay división en esa misma iglesia y toda la igleisa luterana por la decisión, en palabras de un pastor luterano Rev. Richard Mahan "En ninguna parte de la Escritura dice que la homosexualidad y el matrimonio del mismo sexo sea aceptable ante Dios. Por el contrario, dice que es inmoral y pervertido."


!Biblia

Genesis 1:27
<pre>
Y creó Dios al hombre a su imagen, a image de Dios lo creó;
varón y hembra los creó.
</pre>

Genésis 2:24-25
<pre>
Por tanto dejará el hombre a su padre y a su madre, y se unirá a su 
mujer, y serán una sola carne.  Y estaban ambos desnudos, Adán y su 
mujer y no se avergonzaban.
</pre>
confirmado por el Señor Jesús en Mateo 19:4-5

Levítico 18:22
<pre>
No te echarás con varón como con mujer; es abominación.
</pre>

O en Lévitico 20:13 que es del pacto anterior, renovado por el Señor Jesucristo:
<pre>
Si alguno se ayuntare con varón como con mujer, abominación hicieron;
ambos han de ser muertos, sobre ellos será su sangre
</pre>
que hoy podemos interpretar como muerte espiritual y con la fe en la sangre de Cristo derramada para expiar pecados y su resurección, quien crea será juntamente resucitado espiritualmente por el Todopoderoso.


##Liberación

Gracias a Dios hay ministerios cristianos que ayudan en la restauración de personas homosexuales que desean un cambio en su vida.   Efectivamente se requiere liberación, aunque debe ser guiada por el Espíritu Santo, con su amor y poder.  Los grupos pro-homosexuales están pendientes de condenar estas prácticas, por lo que deben efectuarse sólo bajo la guianza del Espíritu Santo.   

Un video de un esfuerzo de liberación condenado por los grupos pro-homosexualismo en Junio de 2009, tuvo que ser retirado de youtube por
la iglesia que lo publicó (ver noticia en {4}) debido a las amenazas de demanda que recibió.  Sin embargo uno que parece ser el mismo video fue subido por otros y está disponible en:

http://www.youtube.com/watch?v=vhedHERfcXk

Hay ministerios cristianos que consideran erradas las liberaciones públicas, y seguramente le faltó amor a la señora que aparece en el video poniendo un pie sobre el muchacho --aunque efectivamente impresiona la forma como
se mueve el pecho del muchacho. El pastor grita expulsando el demonio e incluso parecería que aplicara fuerza física para controloar los impresionantes movimientos del muchacho, quien según algunos reportajes al final termina vomitando.   La situación tuvo que ser muy dificil para el joven y la iglesia.

Los grupos pro-homosexuales condenan la práctica diciendo que el muchacho casi resulta asfixiado, que fue torturado. También hay quienes condenan que el pastor gritara que expulsaba el demonio en el nombre de Jesús, aunque quien hace la liberación es el Señor y es en su nombre que los discípulos deben hacerlo, como dice en Marcos 16:17-18 

<pre>
Y esta señales seguirán a los que creen: En mi nombre echarán fuera 
demonios; hablarán nuevas lenguas; tomarán en las manos serpientes y si 
bebieren cosa mortífera, no les hará daño; sobre los enfermos pondrán sus 
manos y sanarán.
</pre>


La controversia ha sido ampliamente cubierta en Internet, aunque no tanto el testimonio del jóven (Jeffrey), por quien oramos al Creador para que lo guarde, pues sigue en la mira de grupos pro-homosexuales (en particular uno llamado True Colors donde le "ayudaron" a ser homosexual).  

[http://blackgaygossip.com/wp-content/uploads/2009/09/tyra-jeffrey.jpg]
(foto de {6}).

Según {6}, Jeffrey explicó en la entrevista televisada como `tuvo pensamientos homosexuales a los 5 años, comenzó a declararse gay a los 10 y comenzó a citarse con niños a los 14.  Un domingo dos mujeres invitaron al muchacho a Manifested Glory... Jeffrey ahora ya no se considera homosexual, aunque tiene "tentaciones".  De acuerdo a Jeffrey "es un proceso por el que tengo que ir para ser completamente liberado.  El espíritu de homosexualismo no está en mí porque he decidio no vivir de esa forma.  Permito a Dios, al Espíritu Santo venir a mi a cambiar lo que había en mi y poder vivir bien fuera de la iglesia"'


También pueden verse videos con estilos de liberación de homosexualismo más calmados (y finalmente alegres), confiados en el poder de Dios y donde justamente Su Poder se manifiesta con más claridad y menos controversia:

http://www.youtube.com/watch?v=zrmQ2C-S40U


##Ministerios

El Señor mueve diversos ministerios a trabajar en el tema de quebrantamiento sexual y en particular homosexualismo, en Internet además de otros citados en este artículo pueden verse:

* Esfuerzos en Francia http://charismamag.com/index.php/news/22407-christians-march-for-jesus-in-france
* http://www.zapatos-nuevos.org/
* En Bogotá http://www.elcorazondelpadre.org/ página de la cual transcribimos un testimonio a continuación

!Testimonio de Omar Castelblanco, lider de ministerio El Corazón del Padre

Nací en un hogar disfuncional donde la relación con  papá fue fría y lejana. A medida que yo crecía, extrañaba profundamente el pasar tiempo al lado de él. Este faltante emocional, sumado a los abusos sexuales que experimenté a partir de los cinco años, me llevó a desarrollar una vida de dependencia afectiva con personas de mi mismo género y trajo como consecuencia conductas adictivas homosexuales. Frustrado y engañado en este oscuro mundo, comencé a enfrentar situaciones paralelas como la promiscuidad y la prostitución, prácticas que me llevaron a vivir a diario  el rechazo, la vergüenza,  y el temor a ser portador del VIH.

Y aunque el homosexualismo fue la puerta que toqué en la búsqueda por encontrar el amor de mi padre, me di cuenta que fue la opción más dolorosa que cualquier ser humano puede llegar a experimentar, no sólo por el daño propio sino por el dolor que causé a las personas que estaban cerca de mí. Sólo hasta los 21 años encontré el amor perfecto a través de Jesucristo quien llegó a convertirse en la pasión de mi vida. Hoy después de un proceso de restauración soy feliz en mi rol masculino al lado de mi esposa y de mi pequeño hijo.

Disfruto de la gracia, sanidad y libertad, que Dios, trajo a mi vida y comparto un mensaje de esperanza a aquellos que alguna vez estuvieron lejos del Corazón del Padre.


Omar Castiblanco
Director del Ministerio



##Devolviendo la trampa del enemigo

El enemigo pone pensamientos sucios en la mente, por ejemplo  que a una mujer otra le parezca atractiva o que a un hombre otro le parezca atractivo, eso es típico, y después trata de convencer a la víctima de que el pensamiento fue de ella/él y por tanto es homosexual.

Los grupos prohomosexuales y ahora la Asociación de Psicología de America refuerzan esa idea falsa, diciendo que tener un mal pensamiento es ser homosexual, que falta es definirse abiertamente y que no podrá dejar de ser homosexual quien lo ha tenido.

Pero el poder de Dios está por encima de todo, podemos pedirle perdón y en su nombre podemos renunciar a esos malos pensamientos e incluso a acciones erradas en el pasado que pudieron tenernos cautivos, pues por boca del profeta Isaías (Isaías 45:2) Dios dice:
<pre>
Yo iré delante de ti, y enderezaré los lugares torcidos; quebrantaré puertas 
de bronce y cerrojos de hierro haré pedazos
</pre>

En su nombre podemos cortar las murmuraciones, las acusaciones y todo obstáculo, pues Dios mismo en Isaías 54:17 te dice:
<pre>
Ninguna arma forjada contra ti prosperará, y condenarás toda lengua que se 
levante contra ti en juicio. Esta es la herencia de los siervos de Jehová, 
y su salvación de mí vendrá, dijo Jehová.
</pre>

Dios hizo hermosos a hombres y mujeres, Satanas aprovecha tratando que deseemos e iniciemos relaciones ilicitas con personas que no son la pareja apropiada.  En el nombre de Jesús, cortemos los malos pensamientos cuando vengan, pidamos perdón y ayuda al Padre, puede que tengas que esperar más hasta conocer la pareja que es, y esa pareja también tendrá que ganara una lucha análoga.  Oremos entonces para que el Señor nos guarde y guarde a otras personas.

Más bien si ya hemos iniciado relaciones lícitas o ilícitas, entreguemos la relación al Señor y con amor hablemosle a esas personas de Dios.   Es el amor del Creador el que nos sana, libera y guía a Él tanto a tí como a las personas que Dios permite que se crucen en tu camino.  Invitemoslas a la iglesia, quien sabe si un jóven que invita a una jóven a la iglesia, termina invitando a la pareja que Dios bendiga, en hermoso noviazgo y un ardiente matrimonio con el fuego del Espíritu Santo.




##REFERENCIAS

* {1} http://www.elcorazondelpadre.org/
* {2} http://www.tcalo.com/index.php/luca-era-gay-povia-segunda-en-el-festival-de-sanremo-2009-traduccion-al-espanol.htm
* {3} http://www.apa.org/topics/sorientation.html
* {4} http://www.ktla.com/news/landing/ktla-gay-exorcism,0,5358043.story
* {5} http://blackgaygossip.com/?p=8096#more-8096


## PD

Compartido en Escuela Dominical del 25.Sep.2009 y 3.Oct.2009 en la Iglesia Menonita de Teusaquillo. Bogotá.
