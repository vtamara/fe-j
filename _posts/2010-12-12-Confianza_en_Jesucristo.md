---
layout: post
title: "Confianza_en_Jesucristo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Mr 10:17-31 de {1}

<pre>
Al salir Él para seguir su camino, vino uno corriendo, e hincando la 
rodilla delante de Él, le preguntó: Maestro bueno, ¿qué haré para heredar
la vida eterna?
Jesús le dijo: ¿Por qué me llamas bueno? Ninguno hay bueno, 
sino sólo uno, Dios.
Los mandamientos sabes: No adulteres. No mates. No hurtes. No digas falso 
testimonio. No defraudes. Honra a tu padre y a tu madre.
Él entonces, respondiendo, le dijo: Maestro, todo esto lo he guardado 
desde mi juventud.
Entonces Jesús, mirándole, le amó, y le dijo: Una cosa te falta: anda, 
vende todo lo que tienes, y dalo a los pobres, y tendrás tesoro en el 
cielo; y ven, sígueme, tomando tu cruz.

Pero él, afligido por esta palabra, se fue triste, porque tenía muchas 
posesiones.

Entonces Jesús, mirando alrededor, dijo a sus discípulos: !Cuán 
difícilmente entrarán en el reino de Dios los que tienen riquezas!
Los discípulos se asombraron de sus palabras; pero Jesús, respondiendo, 
volvió a decirles: Hijos, !cuán difícil les es entrar en el reino de Dios,
a los que confían en las riquezas!
Más fácil es pasar un camello por el ojo de una aguja, que entrar un rico 
en el reino de Dios.

Ellos se asombraban aun más, diciendo entre sí: ¿Quién, pues, podrá ser
salvo?
Entonces Jesús, mirándolos, dijo: Para los hombres es imposible, mas para 
Dios, no; porque todas las cosas son posibles para Dios.
Entonces Pedro comenzó a decirle: He aquí, nosotros lo hemos dejado todo, 
y te hemos seguido.
Respondió Jesús y dijo: De cierto os digo que no hay ninguno que haya 
dejado casa, o hermanos, o hermanas, o padre, o madre, o mujer, o hijos, 
o tierras, por causa de mí y del evangelio,
que no reciba cien veces más ahora en este tiempo; casas, hermanos, 
hermanas, madres, hijos, y tierras, con persecuciones; y en el siglo 
venidero la vida eterna.
Pero muchos primeros serán postreros, y los postreros, primeros.
</pre>


##1. Llamado al seguimiento

Jesús llamo de forma directa a algunos como se ve en Marcos 1:16-17:
<pre>
Andando junto al mar de Galilea, vio a Simón y a Andrés su hermano, que
echaban la red en el mar; porque eran pescadores.
Y les dijo Jesús: Venid en pos de mí, y haré que seáis pescadores de 
hombres. 
</pre>
El Señor amoroso enseñaba, sanaba, liberaba y hacía otros milagros. A sus discípulos les enseñaba más y los eligió y organizó para misionar como se ve en Mr 3:13-15:
<pre>
Después subió al monte, y llamó a sí a los que Él quiso; y vinieron a Él.
Y estableció a doce, para que estuviesen con Él, y para enviarlos a predicar,
y que tuviesen autoridad para sanar enfermedades y para echar fuera 
demonios
</pre>
y los envió en condiciones paradójicas como se ve en Mr 6:7-9:
<pre>
Después llamó a los doce, y comenzó a enviarlos de dos en dos; y les dio 
autoridad sobre los espíritus inmundos.
Y les mandó que no llevasen nada para el camino, sino solamente bordón; 
ni alforja, ni pan, ni dinero en el cinto, sino que calzasen sandalias, 
y no vistiesen dos túnicas. 
</pre>

Seguramente el joven rico vio milagros obrados por el Señor y sus discípulos y escuchó sus enseñanzas.  Fue a consultarle como heredar la vida eterna, porque seguramente lo veía con autoridad, pero no como al Mesias (posiblemente por eso de su parte no era correcto llamarlo bueno).

Jesús lo invitó a cumplir los mandamientos. Aunque el joven ya los cumplía,  sentía que algo le faltaba.  Jesús vio la vida de ese joven y lo amó, y le puso un reto de amor, que dejara aquello en lo que él confiaba: las riquezas. 
<pre>
Una cosa te falta: anda, vende todo lo que tienes, y dalo a los pobres, 
y tendrás tesoro en el cielo; y ven, sígueme, tomando tu cruz.
</pre>
Pero el joven no pudo, él confiaba demasiado en sus riquezas y eso no le ayudaba a ver al Mesías, ni a amarlo.

El Señor además a sus discípulos nos aclaró que la entrada al Reino requiere nuestra plena confianza en Él y no por ejemplo en las riquezas.  Requiere que lo amemos sobre todo y nos soltemos en sus brazos, confiando en Él como en nada, ni en nadie más se puede confiar.

Pedro y los otros discípulos así lo hicieron, pues habían dejado todo por seguirlo en amor, y ya no confiaban en el dinero pues habían dejado sus trabajos cuando el Señor los llamó.  Tuvieron que reorganizar radicalmente sus relaciones familiares, la forma de misión que el Señor les indicó así lo exigía.  No dejaron de responder por sus familias,  (Pedro por ejemplo era casado y después del llamamiento Jesús fue a su casa y sanó a su suegra) pero si tuvieron que reorganizar a partir de su nueva confianza.

##2. Caso personal

Pido perdón por resaltar un testimonio personal en esta prédica. Lo hago dando gracias al Señor y porque el pastor Jaime me lo ha solicitado.  Aunque me ha hecho falta más discernimiento por ejemplo para ir despacio porque vamos de afán, más tiempo con el Señor e incluso en ocasiones confianza y paciencia, se trata de un momento en el que confié en Él.  

En mi caso antes de 2001 confiaba en el dinero, en la "seguridad" de la academia y en el prestigio, aún cuando en la adolescencia había leído evangelios que mi madre me regaló.  Fui becado y recién casado a Alemania a hacer doctorado en 2000. En el 2001 el Señor se me mostró y lo amé, comencé a cambiar para seguirlo (por ejemplo renuncié a herencia y a un trabajo que financiaba guerra).  Eso mientras invitaba a mi entonces esposa, quien comenzó a cambiar y a apoyar algunos cambios (por ejemplo renunciar a ese trabajo por el anhelo de paz). En 2002 nació nuestra primera hija y en 2003 buscabamos adoptar (un médico nos había dicho que mi entonces esposa no podía quedar embarazada).  Lo más económico que encontramos fue adoptar en Sierra Leona en África, donde la tasa de mortalidad infantil era altísima, y aunque gracias a Dios ha disminuido hoy es de 192 niños por cada 1000, 10 veces más que en Colombia donde es 19 por cada mil (ver {2}). 

Hicimos contacto por Internet con una agencia de adopción, quienes nos enviaron cartas de recomendación a la embajada de Sierra Leona en Alemania. En medio de los preparativos para el viaje mi entonces esposa resultó esperando nuestra segunda hija y el Señor me dijo al corazón "En Sierra Leona faltan manos" que resulta muy literal pues durante la guerra civil de ese país (por diamantes que terminó en el 2001) amputaron piernas y brazos a muchas personas.  Yo entendí un llamado a misión.  Como anhelábamos adoptar desde antes de casarnos y queríamos conocer el sitio de la posible misión, nos afanamos a continuar y realizar el viaje en Junio de 2003, aún con nuestra segunda hija de 5 meses de gestación y la primera de año y medio. 

Llegamos al hospedaje que nos habían preparado los de la agencia de adopción en un barrio muy pobre de Freetown, la capital de uno de los paises más pobres del mundo (de acuerdo a {3} de los 182 paises, Colombia hoy está en el puesto 85 con 4985 dolares de PIB per capita anual, mientras que Sierra Leona está en el 173 con 332 dolares de PIB per capita anual), la  costumbre en Sierra Leona es comer una vez al día al almuerzo un arroz con una salsa de hojas de la matica de la papa.

Yo estaba tomando un antimalárico por sugerencia de un médico alemán. Los de la agencia nos llevaron a un orfanato con unos 10 niños mayorcitos de entre quienes escogimos una niña de unos 5 años y pagamos 1200 dolares a la agencia de adopción.  Un par de días después los de la agencia se llevaron a la niña para hacerle unos examenes, y empezaron a ser como hostiles con nosotros.  Comencé a delirar por el antimalárico --en mi el Lariam causó ese efecto como he sabido que le ha ocurrido a otras personas--  y en una noche, infortunadamente, hice un intento de suicido tirándome por una puerta desde un segundo piso, pero gracias a Dios caí en un balcón.  Mi entonces esposa ante la situación: empezando a descubrir que habíamos sido estafados, teniendo a cargo a Sara y Milagros, conmigo enfermo e intentando suicidio, decidió separarse, dejarme al "cuidado" de los estafadores y hacer las cosas en sus fuerzas. Fue a otro cuarto con mis hijas y me dejó con los nuevos cuidadores en continúa lucha por salir y ellos por no dejarme, fueron 3 días que me negué a comer casi no veía a mis hijas y entonces esposa, me llevaron a un médico con dinero enviado por nuestros padres, él confirmó que el problema era el Lariam.  En medio del delirio pude aclarar y tomar decisiones de fe, por ejemplo que Jesús es Dios y que no adoro a la virgen María ni me encomiendo a ella en el momento de mi muerte sino a Dios mismo a quien amé y amo más.  Al final me amarraban tan fuerte que no me circulaba bien la sangre, escuché los pasos del Señor, sentí cuando me alzó, me puso sobre la cama y me desamarró.  Después una señora que nos había conocido como en 3 minutos cuando llegamos al aeropuerto de Freetown, se enteró de nuestra situación y habló con los estafadores --que en ese momento eran más bien como secuestradores que esperaban que nuestros padres enviaran más dinero-- para que nos llevaran a casa de ella, y ellos tuvieron que hacerlo.  En casa de Fatú empecé a recuperarme y a comer mientras cuidaba a Sara y mi entonces esposa salía y hacía otras cosas entre las que averiguó con la  policía que el supuesto orfanato había sido organizado por vecinos del barrio que habían prestado a sus niños y niñas.  Volvimos a Alemania con Sara, mi entonces esposa se quedó unos días más en Sierra Leona y después se obstinó en volver con 7 meses de embarazó, por ser la mamá de mis hijas prefiero no dar más detalles sobre el caso de ella, pero mantuvo su decisión de separarse, y yo de seguir al Señor dispuesto a dejarlo todo,  para comienzos de 2004 estabamos en Colombia, ella se fue con su mamá quien la apoyo, mis padres no podían entenderme, le dejé el poco dinero que teníamos a ella mientras luchaba sin más ayuda que la de Dios para que no se llevará a mis hijas fuera del país, prácticamente obligado por el ICBF a no verlas.  

##3. El Señor recompensa

Hoy vengo a leer con detenimiento la promesa del Señor y a saber que la está cumpliendo y que la cumple a todo el que confié en Él (claro está por favor no se afanen, pidan más discernimiento y guianza del Señor para que no haya tanto tropiezo como en mi caso):

<pre>
Respondió Jesús y dijo: De cierto os digo que no hay ninguno que haya 
dejado casa, o hermanos, o hermanas, o padre, o madre, o mujer, o hijos, 
o tierras, por causa de mí y del evangelio,
que no reciba cien veces más ahora en este tiempo; casas, hermanos, 
hermanas, madres, hijos, y tierras, con persecuciones; y en el siglo 
venidero la vida eterna.
</pre>

No puedo más que darle gracias al Señor, porque me ha dado más de lo que esperaba, en el momento no hay el tiempo para explicar los detalles de como lo ha venido haciendo el Señor, pero hoy:
* Desde mediados de 2009, tengo visitas reguladas con mis hijas tras 6 años de lucha judicial
* Mis padres y hermanos respetan un poco más mis decisiones de fe y vida
* El Señor me ha dado casa
* Me ha sobreabundado el trabajo y los ingresos de forma que han llegado a trabajar dos personas más conmigo. Aunque bueno para el próximo año no tengo en el momento con quienes trabajar pero si mucho trabajo.
* Me ha dado más hij@s adoptivos por ejemplo en San Nicolas.
* Me ha dado el honor de conocer sus trabajadores, ser amigo de ellos y trabajar para Él por ejemplo en dirección, prédica, ministerio de danza. 
* Me ha permitido prepararme para la misión y me está ayudando a organizarla.

La confianza en el Señor no se puede entender, por eso no espero que me entiendan,  ni me justifiquen o me juzguen, el camino de cada quien es único, espero y oro para que podamos confiar en el Señor cada día más.

También oramos para que el Señor nos muestre como podemos servirle y para que nos guie en la forma de hacerlo.

Oramos por el trabajo el próximo año, y en particular personas que puedan trabajar conmigo.

Oramos por la salud para que nos la mantenga y mejore.

Por los dones y ministerios que el Señor nos ha dado para que los empleemos y demos testimonio de su poder.

##REFERENCIAS

* {1} Biblia Reina Valera 1960. http://www.biblegateway.com/quicksearch/?quicksearch=Marcos+10%3A17-31&qs_version=RVR1960
* {2} http://datos.bancomundial.org/indicador/SH.DYN.MORT?cid=GPDes_15
* {3} http://es.wikipedia.org/wiki/Anexo:Pa%C3%ADses_por_PIB_(nominal)_per_c%C3%A1pita
