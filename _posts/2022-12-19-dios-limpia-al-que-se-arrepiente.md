---
layout: post
categories:
- predica
title: Dios limpia al que se arrepiente. Isaías 4:2-6
author: vtamara
image: "/assets/images/winter-trees-and-snow.jpg"

---
# 1 Introducción

En la prédica anterior del pastor Jaime, él mencionaba varias maravillas que vinieron con el nacimiento del Señor Jesús, hoy quiero tratar dos más que habían sido profetizada por Isaías:

* Jesús trae limpieza de pecados y salvación a quienes se arrepientan y lo reciban como Señor.
* Jesús trae protección con Su Santo Espíritu para quienes lo han recibido.

# 2. Texto: Isaías 4:2-6

**2** En aquel tiempo el renuevo de Jehová será para hermosura y gloria, y el fruto de la tierra para grandeza y honra, a los sobrevivientes de Israel. **3** Y acontecerá que el que quedare en Sion, y el que fuere dejado en Jerusalén, será llamado santo; todos los que en Jerusalén estén registrados entre los vivientes, **4** cuando el Señor lave las inmundicias de las hijas de Sion, y limpie la sangre de Jerusalén de en medio de ella, con espíritu de juicio y con espíritu de devastación. **5** Y creará Jehová sobre toda la morada del monte de Sion, y sobre los lugares de sus convocaciones, nube y oscuridad de día, y de noche resplandor de fuego que eche llamas; porque sobre toda gloria habrá un dosel, **6** y habrá un abrigo para sombra contra el calor del día, para refugio y escondedero contra el turbión y contra el aguacero.

# 3. Contexto

Los reyes de Judá mencionados en Isaías 1:1 mezclados con los reyes de Israel del Norte con fechas acordes a Bruce, D. 2013:

| Rey de Juda | Rey de Israel del Norte (o Efraín) | Año | Tiempo de Reinado |
|  | Jeroboam II | 808 - 768 | 41 años |
| Uzías o Azarías |  | 805/794 - 754 | 52 años |

|  | Zacarías | 768 | 6 meses |
| --- | --- | --- | --- |
|  | Salum | 767 | 1 mes |
|  | Manahem | 767-757 | 10 años |
|  | Pekaia | 757-755 | 2 años |
|  | Peka | 758/757/755-738 | 20 años |
| Jotam |  | 757/754-738 | 16 años |
| Acaz |  | 742/738-727 | 16 años |
|  | Sin rey | 738-731 |  |
|  | Oseas | 731-721 | 9 años |
| Ezequías |  | 727-698 | 29 años |

Con los 4 reyes mencionados en Isaias 1:1 podemos pensar que Isaías vivió aproximadamente entre los años 775 y 700 en el reino de Juda con capital Jerusalén. El siguiente diagrama presenta el contexto de Judea durante el tiempo de vida de Isaías (disponible en [https://www.preceden.com/timelines/821198-cronolog-a-de-isa-as](https://www.preceden.com/timelines/821198-cronolog-a-de-isa-as "https://www.preceden.com/timelines/821198-cronolog-a-de-isa-as"))

![](/assets/images/reyes_isaias.png)

Seguramente durante los reinados de Uzías (también llamado Azarías) y después del de su hijo Jotam quienes más bien fueron fieles a Dios, Isaías en su niñez y juventud vio cómo prosperaba Judea, pero a la vez se enteraba como el reino vecino de Israel del Norte estaba en caos tanto por los asesinatos de un rey tras otro como por los ataques de Asiria contra ese reino, que llevaron por ejemplo a Menahem a pagarle tributo a Asiria. Isaías vió como Uzías era castigado por su propio pecado con lepra.

Isaías posiblemente en su adultez, también vivió en Judea el oscuro gobierno de Acaz quien según 2 Reyes 16:2 “no hizo lo recto delante de Jehova” pues sacrificó a sus hijos, idolatró a Baal y ofreció sacrificios en lugares altos. 2 Crónicas 28 dice que en consecuencia Israel del Norte liderado por Peka aliado con Siria liderado por Razín fueron contra Judea llevandose deportados y tesoros a Damasco capital de Siria y a Samaría capital del Reino de Israel del Norte. Ante esto Acaz le pidió ayuda a Pul rey de Asiria (también conocido como Tiglat-pileser III) enviándole tesoros del templo. Pul tomó a Damasco terminando con el imperio Sirio y asedió a Israel del norte tomando varias ciudades hacia el año 733. Posteriormente Asiria liderada por Sargon II hacia el año 722 (cuando Oseas reinaba en Israel del Norte) terminó definitivamente con el reino de Israel del Norte. Acaz idolatró con dioses Asirios y bandalizó el templo de Jerusalén modificándolo para que fuera como el de Asiria. Durante su reinado, también Edom llevó cautivos a habitantes de Judea y los filisteos recuperaron ciudades antes conquistadas por Judea. Incluso los Asirios a quienes les había pagado con los tesoros del templo lo redujeron. Ante el pecado de Acaz el pueblo de Judea pagó.

Finalmente Isaias vivió en Judea durante el buen reinado de Ezequías, quien a diferencia de su papá Acaz, fue fiel a Dios y aunque Asiria liderado por Senaquerib derrotó a Siria y tomó prácticamente todo Judea, no pudo tomar Jerusalén, donde estaban Isaías y Ezequías por la intervención milagrosa de Dios quien escuchando a Ezequías e Isaías destruyó en una noche más de 120.000 que atacaban a Jerusalén. Así que Isaías vivió una restauración y protección milagros de Dios.

Seguramente por las guerras y liberación milagrosa que vivió Isaías y por las que él se enteraba que vivía el reino hermano de Israel del Norte, Isaías podía describir vividamente profecías como las que se ven en los capítulos 1 a 3 de su libro, sobre la destrucción de Jerusalén y del templo que ocurriría más de 100 años después de la muerte de Isaías en el 586 a manos de los Babilonios. Y en el pasaje que vimos que al parecer está profetizando simultáneamente 2 eventos:

1. El regreso de los deportados de Babilonia y la restauración del templo tras el sangriento exilio
2. La restauración del pueblo de Dios con el advenimiento de Jesús.

Veamos brevemente que decían los capítulos 1 a 3:

| --- | --- |
| Versículo | Tema |
| 1:1 | Isaías vivió en Judea durante reinados de Azarías, Jotám, Acaz y Ezequías |
| 1:2-9 | Dios dice que Israel se rebeló contra Él. Qué en castigo hubo enfermedad y asolación por parte de extranjeros, aunque Él reservó un remanente de la destrucción total. |
| 1:10-20 | Dice que la ofrenda que le dan, las libaciones y sacrificios son vanos porque sus manos están llenas de sangre. Invita a que se laven y limpien dejando lo malo, restituyendo al agravado, hacer justicia al huérfano y amparar a las viudas. Y después que fueran a Él quien limpiará pecados. De lo contrario castigo |
| 1:21-31 | Dice las iniquidades de los moradores de Jerusalén. Pero dice que la limpiará, restaurando jueces y consejeros. Para los que no quieran conversión serán consumidos. |
| 2:1-4 | Profetiza que de Sión saldría la ley y de Jerusalén su Palabra. Y que las naciones vendrían Él y muchos se volverían pacíficos. |
| 2:5-22 | Denuncia que los hiijos de Jacob se llenaron de falsas costumbres, tenían riquezas e idolatraban. Profetiza que vendrá un día que abatirá su altivez. |
| 3:1-15 | Profetiza que los líderes de Judá y Jerusalén serían gobernados por muchachos y les iría mal a los impíos. |
| 3:16-26+4:1 | A las hijas de Sión les cambiaría sus lujos y hermosura por cilicio y quemadura. Y que los varones caerían a espada por lo que habría escasez de hombres. |
| 4:2-6 | Y después habría un renuevo santo, lavado por Dios con “espíritu de juicio y con espíritu de devastación” y lo protegerá. |

# 4. Análisis

Podemos ver como ciclos en la historia del pueblo de Dios ([https://www.preceden.com/timelines/867411-isaias-y-ciclos-decadencia-devastaci-n-restauraci-n](https://www.preceden.com/timelines/867411-isaias-y-ciclos-decadencia-devastaci-n-restauraci-n "https://www.preceden.com/timelines/867411-isaias-y-ciclos-decadencia-devastaci-n-restauraci-n") ):

![](/assets/images/ciclos.png)

| --- | --- | --- |
| Decadencia | Devastación | Restauración |
| Prosperidad de Judea en tiempos de Uzías y Jotam | Desolación de Judea por parte de Asiria en tiempo de Acaz | Liberación de Asiria y protección sobrenatural en tiempo de Ezequías |
| Estabilidad después de Ezequías | Fin del impero de Judea por parte de los Babilonios y exilio. | Retorno del exilio y reconstrucción del 2do templo por parte de Zorobabel en 515dC |
| Estabilidad de Israel por un tiempo | Opresión a los judios por parte del imperio greco-romano. Sufrimiento y muerte del Señor Jesús en el año 33. | Perdón de pecados por su sangre y resurrección para darnos vida. Protección mediante su presencia y su Santo Espíritu. |

Y repasar los versículos de hoy a la luz de estos:

| --- | --- |
| Versículos | Observación |
| 2 | Se refiere tanto a los de Jerusalén tras la liberación de Senaquerib, como a los judios liberados por el imperio Persa que regresaron y reconstruyeron el muro, como a nosotros los que creemos en Jesús y tenemos perdón de pecados y en fidelidad vida en Cristo. |
| 3 | Quienes recibimos y aceptamos a Jesús como Señor y Salvador podemos ser llamados santos, no por nuestras obras, sino por la sangre de Cristo que nos cubre. Apreciemos y esforcémonos por Él. |
| 4 | Fue costosa la liberación de Jerusalén en el tiempo de Ezequías (120.000 asirios muertos), así como la restauración del 2do templo (vida de muchos israelitas y su exilio por más de 70 años), así como la nuestra hoy (sangre de nuestro Señor Jesús). |
| 5 y 6 | El Señor nos ha prometido protección, una cobertura especial. Cuidemosla. |

# 

# 5. Conclusión y oración

Recordemos el precio de nuestra libertad y protección hoy por la sangre y resurrección del Señor Jesús. Personalmente creo que si lo olvido es mejor que Dios me lo recuerde con algún castigo que me ayude a arrepentirme y enderezarme en Él.

Señor gracias por Tu salvación, por la liberación del pecado y de la muerte. Por tu Santo Espíritu que nos ayuda. Ayudanos a recordar el precio que has pagado po esto. Por favor ayudanos a arrepentirnos de lo que no estemos haciendo bien, para caminar en integridad en Tí.

Por favor Señor, en lo que yo esté fallando de manera recurrente, perdoname y si es necesaria haz en mi como una devastación, que me permita arrepentirme e ir a Tí, para que me vuelvas a limpiar y ayudar a hacer cambios profundos para ser más como Tú, pido en el nombre de Jesús.

A quiénn aún no haya recibido a Jesús como Señor y Salvador pero quiera hacerlo, le invitamos a hacer una oración de Fe.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* Henry, Matthew. Commentary. [https://www.biblegateway.com/resources/matthew-henry/toc/](https://www.biblegateway.com/resources/matthew-henry/toc/ "https://www.biblegateway.com/resources/matthew-henry/toc/")
* Bruce, Dan. 2013. Sacred Chronology of the hebrew kings. The Prophecy Society. [http://www.prophecysociety.org/PDF/SC_FREE.pdf](http://www.prophecysociety.org/PDF/SC_FREE.pdf "http://www.prophecysociety.org/PDF/SC_FREE.pdf")

Imagen de dominio público de [https://www.publicdomainpictures.net/en/free-download.php?image=winter-trees-and-snow&id=31013](https://www.publicdomainpictures.net/en/free-download.php?image=winter-trees-and-snow&id=31013 "https://www.publicdomainpictures.net/en/free-download.php?image=winter-trees-and-snow&id=31013")