---
layout: post
title: "Testimonio_Flaviano"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Según {1} Jesús es mencionado por el historiador judio Flavio Josefo en Antigüedades Judías 18:63 y en Antigüedades Judias 20:200.
El tiempo de vida de Josefo fue del 37 d.C. al 100.  

18:63

|Por ese tiempo vivió Jesús, un hombre sabio, si en verdad uno debe llamarlo hombre. Porque realizó hechos extraordinarios y fue maestro de quienes aceptaron felizmente la verdad. Se ganó a muchos judíos y griegos. Era el Mesías. Cuando fue acusado por los hombres más importantes de nuestro pueblo, y Poncio Pilato lo condenó a ser crucificado, quienes originalmente habían llegado a amarlo no cesaron de hacerlo; porque se les apareció al tercer día, restaurado a la vida, como los profetas de la Deidad habían predicho esta y otras incontables maravillas acerca de él, y la tribu de los cristianos, así llamados en honor a él, no ha desaparecido hasta nuestros días. (Todas las citas de Josefo, excepto la siguiente, son de P.L. Maier, ed./trad. al inglés, Josephus -The Essential Works [Josefo: Obras Esenciales], Grand Rapids: Kregel Publications, 1994).|

20:200

|Con tal carácter ["impulsivo y temerario" por el contexto], Anano pensó que con Festo muerto y Albino aún en camino, él tendría la oportunidad adecuada. De acuerdo con los jueces del Sanedrín, trajo ante ellos al hermano de Jesús llamado el Cristo, cuyo nombre era Santiago, y a ciertos otros. Los acusó de haber transgredido la ley y los entregó para que fueran lapidados. Pero de entre los residentes de la ciudad, aquellos a quienes se les consideraba más justos y estrictos en la observancia de la ley, se ofendieron por esto. Por tanto, secretamente se pusieron en contacto con el rey [Herodes Agripa II], incitándolo a ordenar a Anano que desistiera de ese tipo de acciones, porque no tenía justificación para lo que ya había hecho. Algunos de ellos incluso fueron a ver a Albino, quien estaba de camino a Alejandría, y le informaron que Anano no tenía autoridad para convocar al Sanedrín sin su consentimiento. Convencido de estas palabras, Albino escribió en tono iracundo a Anano, amenazándolo con un castigo. Y debido a esto, el rey Agripa lo destituyó del cargo de sumo sacerdote, el cual había desempeñado durante tres meses.|


La redacción del primero hace pensar que Flavio Josefo sería cristiano, 
En 1972  Schlomo Pines dijo haber descubierto otra tradición para Antiguedades 18:63 en las obras del historiador Agapio del siglo X :

|En ese tiempo hubo un sabio llamado Jesús, y su conducta fue buena, y fue conocido por virtuoso. Muchas personas de entre los judíos y las otras naciones se convirtieron en sus discípulos. Pilatos lo condenó a morir crucificado. Pero quienes se habían convertido en sus discípulos no abandonaron su enseñanza. Informaron que él se les apareció tres días después de haber sido crucificado y que estaba vivo. Según esto, tal vez haya sido el Mesías, de quien los profetas habían informado maravillas. Y la tribu de los cristianos, llamada así en honor a él, no ha desaparecido hasta hoy.|

Otras fuentes extrabíblicas seculares del primer siglo que mencionan a Jesús según {1} son:

* Tácito (Anales 15:44)
* Suetonio (Claudio 25)
* Plinio el Joven (Carta a Trajano)

##REFERENCIAS

{1} http://www.4truth.net/site/apps/nlnet/content3.aspx?c=kiKUL4PPLvF&b=1461613&ct=2027101
