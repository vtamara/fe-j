---
layout: post
title: Caminar en la Resurrección
author: vtamara
categories:
- Prédica
image: "/assets/images/sepulcrocol.jpg"
tags: Prédica

---
La pastora Adaia me solicitó predicar en la Iglesia La Resurrección en San Nicolás, justamente sobre La Resurrección y como caminar en ella.

Además de las explicaciones de nuestro Señor en la Biblia, un par de situaciones que han ayudado para esta prédica han sido:

* Ver unos mensajes promoviendo el suicidio en la página web de una adolescente
* Los agradecimientos de niñ@s de danzas respecto a la resurrección: vida, salud, familia.

Todo esto me hace recordar como mi esperanza para vivir y todo lo que disfruto en la vida hoy es fruto de la resurrección de nuestro señor Jesucristo. Primero mi espíritu que estaba muerto, Dios lo resucitó cuando creí en Jesús (fue proceso de 2000 a 2004), después mi espíritu ha aprendido y crecido en Él (con altibajos y tropiezos, pero por su fidelidad y amor seguimos), por eso me ha permitido disfrutar nueva vida, familia, salud y hasta predicar aquí hoy Su Resurrección, y además se que mañana cuando este cuerpo terrenal muera (si es que el Señor Jesús no vuelve antes), mi espíritu seguirá vivo y que Dios Padre en su momento completará mi resurrección dandome un nuevo cuerpo celestial. Así llegamos a 1 Corintios 15, del cual por su extensión miraremos 1 al 17.

## 1. TEXTO

1 Corintios 15:1-17

    1  Además os declaro, hermanos, el evangelio que os he predicado, el cual 
       también recibisteis, en el cual también perseveráis;
    2 por el cual asimismo, si retenéis la palabra que os he predicado, sois 
      salvos, si no creísteis en vano.
    3 Porque primeramente os he enseñado lo que asimismo recibí: Que Cristo murió 
      por nuestros pecados, conforme a las Escrituras;
    4 y que fue sepultado, y que resucitó al tercer día, conforme a las 
      Escrituras;
    5 y que apareció a Cefas, y después a los doce.
    6 Después apareció a más de quinientos hermanos a la vez, de los cuales 
      muchos viven aún, y otros ya duermen.
    7 Después apareció a Jacobo; después a todos los apóstoles;
    8 y al último de todos, como a un abortivo, me apareció a mí.
    9 Porque yo soy el más pequeño de los apóstoles, que no soy digno de ser 
      llamado apóstol, porque perseguí a la iglesia de Dios.
    10 Pero por la gracia de Dios soy lo que soy; y su gracia no ha sido en vano 
      para conmigo, antes he trabajado más que todos ellos; pero no yo, sino la 
      gracia de Dios conmigo.
    11 Porque o sea yo o sean ellos, así predicamos, y así habéis creído.
    12 Pero si se predica de Cristo que resucitó de los muertos, ¿cómo dicen 
       algunos entre vosotros que no hay resurrección de muertos?
    13 Porque si no hay resurrección de muertos, tampoco Cristo resucitó.
    14 Y si Cristo no resucitó, vana es entonces nuestra predicación, vana es 
      también vuestra fe.
    15 Y somos hallados falsos testigos de Dios; porque hemos testificado de Dios 
      que él resucitó a Cristo, al cual no resucitó, si en verdad los muertos no 
      resucitan.
    16 Porque si los muertos no resucitan, tampoco Cristo resucitó;
    17 y si Cristo no resucitó, vuestra fe es vana; aún estáis en vuestros 
      pecados.
    

## 2. CONTEXTO

En la carta a los Corintios, Pablo le escribe a los miembros de la iglesia de esa ciudad una diversidad de enseñanzas que estaban necesitando, por ejemplo los capítulos cercanos al 15 tratan sobre: 13) Amor, 14) Dones en la iglesia y 16) Las ofrendas.

En el capítulo Pablo explica la resurrección de nuestro Señor Jesús, mencionando que estaba profetizado, con experiencia de personas cercanas que lo vieron resucitado y con su propio testimonio quien también lo vio resucitado. Explica como esa resurrección es para la vida en el cielo y como es justamente nuestra esperanza.

### 2.1 Casos de resurrección terrenal en las Escrituras

En Internet se encuentran varios testimonios de personas que han resucitado terrenalmente por su fe y vida espiritual en el Señor Jesús, por ejemplo un pastor nigeriano y [https://www.youtube.com/watch?v=ny6zHkevIJ8](https://www.youtube.com/watch?v=ny6zHkevIJ8 "https://www.youtube.com/watch?v=ny6zHkevIJ8"). Personalmente conozco a uno, que resucitó por la oración de su papá con fe (ambos se llaman Luis, viven en Bogotá en la Av 68 con 26). Pero concentremonos en los casos que se presentan en la Biblia

#### 2.1.1 Antiguo Testamento:

* 1 Reyes 17:17-24. Medianta Elías Dios resucita a la hija de la viuda de Sarepta. _22 Y Jehová oyó la voz de Elías, y el alma del niño volvió a él, y revivió._
* 2 Reyes 4:25-35. Mediante Eliseo Dios resucita al hijo de la Sunamita en el Carmelo. _35 Volviéndose luego, se paseó por la casa a una y otra parte, y después subió, y se tendió sobre él nuevamente, y el niño estornudó siete veces, y abrió sus ojos._
* 2 Reyes 13:20-21 Mediante los huesos de Eliseo que había muerto, Dios resucitó a un Israelita muerto que había sido lanzado de afán en el sepulcro de Eliseo _21 Y aconteció que al sepultar unos a un hombre, súbitamente vieron una banda armada, y arrojaron el cadáver en el sepulcro de Eliseo; y cuando llegó a tocar el muerto los huesos de Eliseo, revivió, y se levantó sobre sus pies._

#### 2.1.2 Nuevo Testameto

* Marcos 5:35-43.Jesús resucita a la hija de Jairo cerca al lago de Tiberias. _41 Y tomando la mano de la niña, le dijo: Talita cumi; que traducido es: Niña, a ti te digo, levántate._
* Lucas 7:11-17. Jesús resucita al hijo de la viuda de Nain. _14 Y acercándose, tocó el féretro; y los que lo llevaban se detuvieron. Y dijo: Joven, a ti te digo, levántate. 15 Entonces se incorporó el que había muerto, y comenzó a hablar. Y lo dio a su madre._
* Juan 11:1-45. Jesús resucita a a Lázaro. _42 Yo sabía que siempre me oyes; pero lo dije por causa de la multitud que está alrededor, para que crean que tú me has enviado. 43 Y habiendo dicho esto, clamó a gran voz: !!Lázaro, ven fuera! 44 Y el que había muerto salió, atadas las manos y los pies con vendas, y el rostro envuelto en un sudario. Jesús les dijo: Desatadle, y dejadle ir._
* Hechos 9:40 Mediante Pedro Dios resucita a la piadosa Tabita (o Dorcas o Gacela), ver {5}. _40 Entonces, sacando a todos, Pedro se puso de rodillas y oró; y volviéndose al cuerpo, dijo: Tabita, levántate. Y ella abrió los ojos, y al ver a Pedro, se incorporó._
* Hechos 20:9-12 10 Mediante Pablo Dios resucita al joven Eutico _Entonces descendió Pablo y se echó sobre él, y abrazándole, dijo: No os alarméis, pues está vivo._

Espero que nos dejen evidencia clara que la resurrección terrenal es un hecho , inexplicable para la ciencia pero que gracias a Dios ha ocurrido y sigue ocurriendo.

Parece simple, pero en todos estos casos, son detalles comunes:

* Muerte previa
* Dolor en familiares, amig@s, tipicamente con lagrimas y lamentos
* En diversos casos es común la oración por parte de quien Dios usa para la resurrección a solas o en compañia de un pequeño de creyentes.
* Tras la resurrección, consuelo y alegría

## 3. ANÁLISIS: LA RESURRECCIÓN DE NUESTRO SEÑOR JESUCRISTO

Aunque hay similitudes entre esos casos de resurreción terrenal con la resurrección de nuestro Señor Jesús, la de Él es diferente. En los 4 evangelios se describe de forma similar Mateo 28:1-10. Lucas 24:1-12. Marcos 16:1-8. Juan 20:1-10. Las mujeres llegan a la tumba y la encuentran vacía, a lo que llaman otros discípulos que la encuentra también vacía. El Señor posteriormente aparece a mujeres, a discípulos y a otras personas.

Los versículos 3 y 4 del texto indican que las escrituras hablabán de esa resurrección, y tan solo en profecias vemos las mismas tres características de las resurrecciones terrenales que repasamos:

1. Que moriría. De hecho muerte con muchos sufrimiento y en cruz
   * Hay bastantes profecías que describen detalles de las humillaciones y sufrimientos de Cristo justo antes de su muerte (ver {6}), sólo citaremos Isaías 53:5 quien 700 años escribió _Más el herido fue por nuestras rebeliones, molidos por nuestros pecados, castigo por nuestra paz fue sobre Él y por sus llagas fuimos nosotros curados_
   * Números 21:4-9 atribuido a Moises (unos 1400 años antes) presenta un símbolo profético sobre la muerte del Señor Jesús en la cruz y de la necesidad de mirarlo para ser salvos _8 Y Jehová dijo a Moisés: Hazte una serpiente ardiente, y ponla sobre una asta; y cualquiera que fuere mordido y mirare a ella, vivirá. 9 Y Moisés hizo una serpiente de bronce, y la puso sobre una asta; y cuando alguna serpiente mordía a alguno, miraba a la serpiente de bronce, y vivía._ esto es confirmado por el mismo señor Jesús en Juan 3:14-15 _14 Y como Moisés levantó la serpiente en el desierto, así es necesario que el Hijo del Hombre sea levantado, 15 para que todo aquel que en él cree, no se pierda, mas tenga vida eterna._
2. Lagrimas por su muerte
   * Zacarias 12:10 profetizó unos 530 años antes de Cristo _Y derramaré sobre la casa de David, y sobre los moradores de Jerusalén, espíritu de gracia y de oración; y mirarán a mí, a quien traspasaron, y llorarán como se llora por hijo unigénito, afligiéndose por él como quien se aflige por el primogénito._
3. Consuelo y alegría por la resurrección, consuelo y alegría (ver {2} y {3}):
   * Salmo 16:10 escrito por David unos 1000 años antes de Cristo _porque no dejarás mi alma en el Seol, ni permitiras que tu santo vea corrupcion_ confirmado como profecia sobre Jesús por Pedro en Hechos 2:24-27.
   * Oseas 3:2 2 unos 750 años antes _Nos dará vida después de dos días; en el tercer día nos resucitará, y viviremos delante de él._
   * Isaias unos 700 años antes de Cristio en 53:10 y 12. _Con todo eso, Jehová quiso quebrantarlo, sujetándole a padecimiento. Cuando haya puesto su vida en expiación por el pecado, verá linaje, vivirá por largos días, y la voluntad de Jehová será en su mano prosperada. ... 12 Por tanto, yo le daré parte con los grandes, y con los fuertes repartirá despojos; por cuanto derramó su vida hasta la muerte, y fue contado con los pecadores, habiendo él llevado el pecado de muchos, y orado por los transgresores._
   * Jonas 17 unos 680 años de Cristo _Pero Jehová tenía preparado un gran pez que tragase a Jonás; y estuvo Jonás en el vientre del pez tres días y tres noches._ confirmado cómo símbolo profético por Jesús en Mateo 12:40, quien profetizó sobre su propia muerte y resurrección _Porque como estuvo Jonas en el vientre del gran pez tres días y tres noches así estará el Hijo del Hombre en el corazón de la tierra tres dias y tres noches_

Pero también hay diferencias:

* El cuerpo de Jesús no está en la tumba. Los otros resucitados por Dios, dejaron su cuerpo terrenal como estaba y continuaron en el mismo, el cual años después moriría.
* El Señor Jesús resucitado ya no está sujeto a las leyes físicas la tierra. Los demás resucitados terrenales vuelven a la tierra y están sujetos a sus leyes naturales. Por ejemplo el Señor Jesús mostró a discípulos como ascendia al cielo, se apareció de un momento a otro a ciertos discípulos, como Pablo resume en los versículos 5-8, incluso al mismo Pablo. Con pena de ponerme junto a esos grandes hombres, también declaro que por ejemplo en el 2003, cuando estaba en una situación de robo, enfermedad e incluso amarrado en Sierra Leona, escuché los pasos del Señor Jesús, sentí cuando me levantó del suelo, me desamarró y organizó todo para que pudieramos salir ilesos con mis hijas Sara, Milagros y la mamá.

Y Pablo explique esto en el mismo capítulo 15:

* _40 Y hay cuerpos celestiales, y cuerpos terrenales; pero una es la gloria de los celestiales, y otra la de los terrenales... 42 Así también es la resurrección de los muertos. Se siembra en corrupción, resucitará en incorrupción._

Entonces Jesús resucitó en cuerpo celestial, esa es la diferencia con las otras resurrecciones que revisamos y esa es garantía de que sus promesas son verdad pues cumplió todas las profecias sobre Él mismo podemos confiar que cumplirá sus promesas para nosotros. Incluyendo promesas como las que nos dió hoy en el Salmo: Joel 2:23-24.

Ahora el turno de la comunidad:

* ¿Crees en la resurrección de Jesús?
* ¿Qué significa para Ti?
* ¿Estas seguro/a que tendrás resurrección?

## 4. CONCLUSIÓN Y ORACIÓN

* Señor Jesús tu resucitase para la vida eterna, fuiste el primero y por esa resurrección tenemos garantía que eres verdad, camino y vida. Por eso podemos confiar en tus promesas.
* Señor podemos confiar por ejemplo en tu promesa de que quienes creemos en Ti resucitaremos para la vida eterna después de que este cuerpo terrenal pase. Señor podemos confiar en la vida tenemos esperanza en nuestra resurrección para la vidad eterna y es garantía de las promesas que nos has hecho.
* Podemos confiar en tus promesas en esta tierra, que suplirás abundantemente nuestras necesidades, siempre que búsquemos primero el Reino y su Justicia. Hoy sin ponernos de acuerdo, la pastora Adaia quien dirigió leyó unos versículos que incluían Joel 2:23-24, por tu resurrección y nuestro arrepentiemiento de pecados confiamos en que nos darás lluvia temprana y tardía como nos dijiste y en que "las eras se henchirán de trigo, y los lagares rebosarán de vino y aceite.". Gracias Señor.
* Señor gracias por ser resurrección y vida como dices en Juan 11:25 "Le dijo Jesús: Yo soy la resurrección y la vida; el que cree en mí, aunque esté muerto, vivirá." Rogamos por los jóvenes, especialmente para que liberes a quines tiene opresión de suicidio.
* Señor resucitanos espiritualmente, haznos vivir ahora confiados y dependiendo de Ti. Gracias por la salud, la familia, la provisión, pues en Tí tenemos en abundancia y para compartir.
* Señor también podemos confiar y saber que es cierta tu promesa de que estarías todos los días con quienes creemos. Gracias Señor.
* Señor también podemos confiar en tu promesa que nos enviarías Espíritu Santo, y lo llamamos ahora para llenarnos, para recordanos el gozo de tu salvación y de la vida eterna contigo. Para que nos recuerde continuamente orar y confiar en Ti Señor, y en que por Ti llegaremos a Dios Padre.


* El nacimiento en Espíritu para mi requirió muchos años hasta que recibí al Señor Jesús como Señor y Salvador con muchas intervenciones sobrenaturales de Dios. ¿Tu ya lo recibiste?. ¿Ya miraste a Jesús crucificado, como miraron los Israelitas la serpiente de bronce en el desierto para no morir por las picaduras de las serpientes? ¿Ya crees en Jesús para no morir por el pecado, para tener perdón, reconciliación con Dios y vida eterna? Si aún no has recibido a Jesus como Señor y Salvador te invito a hacer una [Oracion de fe](https://web.archive.org/web/20160812140521/http://fe.pasosdejesus.org/?id=Oracion+de+fe)

## 5. REFERENCIAS

* {1} Biblia. Reina Valera 1960.
* {2} [http://www.reasonablefaith.org/spanish/profecias-del-antiguo-testamento-sobre-la-resurreccion-de-jesus](http://www.reasonablefaith.org/spanish/profecias-del-antiguo-testamento-sobre-la-resurreccion-de-jesus "http://www.reasonablefaith.org/spanish/profecias-del-antiguo-testamento-sobre-la-resurreccion-de-jesus")
* {3} [http://www.icr.org/article/prophecies-resurrection/](http://www.icr.org/article/prophecies-resurrection/ "http://www.icr.org/article/prophecies-resurrection/")
* {4} [http://francisco-morillo.blogspot.com/2012/02/ejemplos-de-resurreccion-en-la-biblia.html](http://francisco-morillo.blogspot.com/2012/02/ejemplos-de-resurreccion-en-la-biblia.html "http://francisco-morillo.blogspot.com/2012/02/ejemplos-de-resurreccion-en-la-biblia.html")
* {5} [http://fe.pasosdejesus.org/?id=Mujer+Misericordiosa](http://fe.pasosdejesus.org/?id=Mujer+Misericordiosa "http://fe.pasosdejesus.org/?id=Mujer+Misericordiosa")
* {6} [http://www.corazones.org/jesus/profecias_at_cumplidas_cristo.htm](http://www.corazones.org/jesus/profecias_at_cumplidas_cristo.htm "http://www.corazones.org/jesus/profecias_at_cumplidas_cristo.htm")
* {7} [http://www.abideinchrist.org/es/num21v9es.html](http://www.abideinchrist.org/es/num21v9es.html "http://www.abideinchrist.org/es/num21v9es.html")

***

Imagen puede usarse con fines no comerciales y proviene de [https://misdibujoscristianos.blogspot.com/2012/03/el-ha-resucitado-he-has-risen.html](https://misdibujoscristianos.blogspot.com/2012/03/el-ha-resucitado-he-has-risen.html "https://misdibujoscristianos.blogspot.com/2012/03/el-ha-resucitado-he-has-risen.html")