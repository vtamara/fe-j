---
layout: post
title: "Rosh_Hashanah"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
## 1 INTRODUCCIÓN

Sabemos que celebramos año nuevo el primero de Enero por nuestro calendario gregoriano (nombre dado a raíz del papa católico Gregorio XIII {Ver 4}), es un calendario solar es decir cada año cuenta una revolución completa de la tierra alrededor del sol, el primer día de este calendario fue establecido desde tiempos del calendario Juliano hacía el año 46aC (nombre del calendario en honor del emperador romano Julio Cesar).  

Sabemos que el final de un año y comienzo del siguiente es buen momento para hacer evaluación del año que pasó y hacer propósitos para el siguiente año, como se hizo en la iglesia en el servicio pasado.

Yo le pregunté a Dios como quería Él que celebrase el año nuevo y en esta prédica quiero compartir lo que me ha enseñado que terminó siendo la Pascua. El texto central será Exodo 12:1-13

## 2. TEXTO Y CONTEXTO

!Año nuevo judio

Comencé pensando en la celebración Judia, resulta que en hebreo &#1512;&#1465;&#1488;&#1513;&#1473; (Rosh) significa principio y  
&#1513;&#1464;&#1473;&#1504;&#1464;&#1492; (hashanah) significa año.   

Ver {2}.  Desde antes de Cristo y hasta nuestros dias, los judíos  celebran el año nuevo de acuerdo a su calendario solar-lunar entre Septiembre y Octubre, en parte siguiendo la tradición de la fiesta descrita en Levíticos 23:24 donde dice:
<pre>
Habla a los hijos de Israel y diles: En el mes séptimo, al primero del mes 
tendréis día de reposo, una conmemoración al son de trompetas, y una santa 
convocación.
</pre>
En este dia le dan un sentido de conmemoración a los siguientes aspectos:
* La creación del universo de la nada por parte de Dios
* La creación del ser Humano por parte de Dios
* El juicio de Dios. Respecto a este es especial el 10 día al que llaman Yom Kippur y que se describe en Levítico 23:27 así:
<pre>
A los diez días de este mes séptimo será el día de expiación; tendréis santa 
convocación, y afligiréis vuestras almas, y ofreceréis ofrenda encendida a 
Jehová.
</pre>

Las trompetas a las que se refieré Levítico 23:24 la hacen con cuernos y aún hoy es costumbre que la hagan sonar para su celebración:
[http://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Liten_askenasisk_sjofar_5380.jpg/240px-Liten_askenasisk_sjofar_5380.jpg] Imagen de {3}


! Año nuevo en la Biblia

En la Biblia las dos palabras Rosh Hashanah (principo de año) sólo aparecen en Ezequiel 40:1 (en el contexto del cautiverio de Israel en Babilonia):
<pre>
En el año veinticinco de nuestro cautiverio, al principio del año, a los
diez días del mes, a los catorce años después que la ciudad fue conquistada,
en aquel mismo día vino sobre mí la mano de Jehová, y me llevó allá.
</pre>
La celebración se explica en Ezequiel 45:18-24.  Se trata de la pascua que en el calendario Judio se celebra entre Marzo y Abril coincidiendo con lo que nosotros celebramos como Semana Santa.   De esos versículos de Ezequías y otras fuentes como {5} resumimos el siguiente orden de celebración en las casas y en el templo:

| __Día__ | __Donde__ | __Rito__ |
| 1 | Templo | Tomar un becerro sin defecto y purificar el santuario|
| 7 | Templo | Poner su sangre en puerta y otras partes del templo, en expiación por los que pecaron por error y por engaño y por expiación por la casa |
| 14 | Casas | Preparar cordero que comerían |
| 14 | Templo | Sacrificar becerro por el pueblo | 
| 15 | Casas | Comer cordero pascual | 
| 15-21 | Casas | Comer pan sin levadura |
| 15-21 | Templo | Sacrificio de un becerro, un carnero y un macho cabrio diario |

Hay quienes consideran un alteración por parte de los judios celebrar año nuevo el primer día del mes Tishri (entre Septiembre-Octubre) y no el primer día del mes Nisan (entre Marzo-Abril) (ver {5}).

La descripción de Ezequiel a su vez se basa en Exodo 12:1-13, donde dice:

<pre>
12  Habló Jehová a Moisés y a Aarón en la tierra de Egipto, diciendo:

2 Este mes os será principio de los meses; para vosotros será éste el primero 
en los meses del año.

3 Hablad a toda la congregación de Israel, diciendo: En el diez de este mes 
tómese cada uno un cordero según las familias de los padres, un cordero por 
familia.

4 Mas si la familia fuere tan pequeña que no baste para comer el cordero, 
entonces él y su vecino inmediato a su casa tomarán uno según el número de 
las personas; conforme al comer de cada hombre, haréis la cuenta sobre el 
cordero.

5 El animal será sin defecto, macho de un año; lo tomaréis de las ovejas o de 
las cabras.

6 Y lo guardaréis hasta el día catorce de este mes, y lo inmolará toda la 
congregación del pueblo de Israel entre las dos tardes.

7 Y tomarán de la sangre, y la pondrán en los dos postes y en el dintel de 
las casas en que lo han de comer.

8 Y aquella noche comerán la carne asada al fuego, y panes sin levadura; con 
hierbas amargas lo comerán.

9 Ninguna cosa comeréis de él cruda, ni cocida en agua, sino asada al fuego; 
su cabeza con sus pies y sus entrañas.

10 Ninguna cosa dejaréis de él hasta la mañana; y lo que quedare hasta la 
mañana, lo quemaréis en el fuego.

11 Y lo comeréis así: ceñidos vuestros lomos, vuestro calzado en vuestros 
pies, y vuestro bordón en vuestra mano; y lo comeréis apresuradamente; es la 
Pascua de Jehová.

12 Pues yo pasaré aquella noche por la tierra de Egipto, y heriré a todo 
primogénito en la tierra de Egipto, así de los hombres como de las bestias; y 
ejecutaré mis juicios en todos los dioses de Egipto. Yo Jehová.

13 Y la sangre os será por señal en las casas donde vosotros estéis; y veré 
la sangre y pasaré de vosotros, y no habrá en vosotros plaga de mortandad 
cuando hiera la tierra de Egipto.
</pre>


## 3. ANÁLISIS

! Año nuevo en Cristo

El éxodo y el cordero pascual eran tipo de la salvación de Cristo, de hecho Isaías había profetizado 700 años de Cristo:

<pre>
5 Mas él herido fue por nuestras rebeliones, molido por nuestros pecados; el
 castigo de nuestra paz fue sobre él, y por su llaga fuimos nosotros curados.

6 Todos nosotros nos descarriamos como ovejas, cada cual se apartó por su
 camino; mas Jehová cargó en él el pecado de todos nosotros.

7 Angustiado él, y afligido, no abrió su boca; como cordero fue llevado al 
matadero; y como oveja delante de sus trasquiladores, enmudeció, y no abrió
 su boca.
</pre>


Podemos comparar detalles dados en Exodo 12:1-13 con detalles del sacrificio de Cristo (ver {6}):

| __Exodo, Cordero y Pascua__ | __Cristo__ |
| El cordero debía ser macho y morir en plenitud de vida (Ex 12:5) | Jesús es varón que fue sacrificado hacía los 33 años | 
| Cordero debía ser sin defecto ni mancha (Ex 12:5, Lev. 22:21-22) | Jesús no tuvo pecado (1 Pe 1:19) |
| El cordero debía ser sacrificado (Ex 12:6) | Jesús fue crucificado | 
| Los huesos del cordero pascual no deben ser rotos | Salmo 34:20 lo profetizó y Juan 19:33-36 lo evidencia |
| Aplicar sangre del cordero en puertas Ex 12:7 | Creer en Cristo para tener vida eterna Juan 3:36 |
| La sangre del cordero protegía del juicio de Dios Ex 12:12-13 | Hebreos 9:28 "así también Cristo fue ofrecido una sola vez para llevar los pecados de muchos; y aparecerá por segunda vez, sin relación con el pecado, para salvar a los que le esperan." | 

Además de todas estas coincidencias, el tiempo en el que el Señor Jesús fue crucificado sería el tiempo en el que estaban preparandose para la delebración de la Cena de Pascua.  

Es decir el plan de salvación es un diseño perfecto por parte de Dios anunciado en el Exodo y a través de profetas e incluso confirmado después de la muerte de nuestro Señor Jesucristo, por ejemplo por Pablo en 1 Corintios 5:7
<pre>
7 Limpiaos, pues, de la vieja levadura, para que seáis nueva masa, sin
levadura como sois; porque nuestra pascua, que es Cristo, ya fue 
sacrificada por nosotros.
</pre>
Por Pedro en 1 Pedro 1:18-19
<pre> 
18 sabiendo que fuisteis rescatados de vuestra vana manera de vivir, la cual recibisteis de vuestros padres, no con cosas corruptibles, como oro o plata,

19 sino con la sangre preciosa de Cristo, como de un cordero sin mancha y sin contaminación,
</pre>
y muy brevemente por Juan en Juan 1:29:
<pre>
29 El siguiente día vio Juan a Jesús que venía a él, y dijo: He aquí el Cordero de Dios, que quita el pecado del mundo.
</pre>

##4. CONCLUSIÓN Y ORACIÓN

Examinemos nuestros propósitos para este año, quien tenga la hoja que iniciaron hace 8 días por favor completela teniendo en cuenta las siguientes tres preguntas, entre más concretas (por ejemplo de ser posible con fechas, mejor):

* ¿Que haré para agradecer a Cristo y conmemorar que compró mi salvación con su sangre?
* ¿Que haré para compartir con otras personas el perfecto plan de salvación para que mediante el Espíritu Santo puedan creer y ser salvas?
* ¿Que haré para alejar el pecado de mi vida y obedecer más a Cristo?

Gracias Padre eterno por el perfecto plan de salvación, pues ya no necesitamos sacrificar un cordero por familia y diariamente un cordero, un becerro y un macho cabrio en el templo durante la pascua.    

Gracias señor Jesús porque cuando te aceptamos como salvador, ya no necesitamos poner sangre en las puertas del templo en memoria de la sangre en las puertas de las casas para evitar el juicio de Dios, ni iniciar un año esperando que el juicio de Dios nos sea favorable.  Señor gracias a Tí tenemos la certeza de que recibir a Cristo da salvación y exime de Jucio.   

Por favor ayudame a cuidar la salvación que me has regalado, señor fortalece mi fe y que mi forma de vivir muestre esa fe pura en Cristo, que esa fe transforme mi vida para seguirte mejor y ser más como Tú Cristo.

Gracias por recordarnos en este año nuevo a Cristo como cordero perfecto, que se entregó par limpiar nuestros pecados, que basta que creamos en Tí como salvador. 


!REFERENCIAS

* {1} http://www.blueletterbible.org/Bible.cfm?b=Eze&c=40&t=KJV#s=t_conc_842001
* {2} http://en.wikipedia.org/wiki/Rosh_Hashanah
* {3} http://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Liten_askenasisk_sjofar_5380.jpg/240px-Liten_askenasisk_sjofar_5380.jpg
* {4} http://es.wikipedia.org/wiki/Calendario_juliano
* {5} http://www.logon.org/spanish/s/p098.html
* {6} http://www.abideinchrist.org/es/ex12v1es.html
