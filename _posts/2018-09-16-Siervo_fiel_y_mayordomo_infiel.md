---
layout: post
title: Siervo fiel y mayordomo infiel
author: vtamara
categories:
- Prédica
image: "/assets/images/laeti_mapiripan_febrero-2017.jpg"
tags: 

---
# 1. Introducción

En la serie iniciada por Jaime el 26.Ago hemos estudiado que las parábolas buscan explicar un concepto mediante otro. Por ejemplo ese día estudiamos 2 Samuel 1:25 cuando el profeta Natán le dijo una parábola al rey David sobre como una persona con muchas ovejas le robó su oveja a otro que sólo tenía una, para evidenciar cuando David tomó a la esposa de uno de sus generales y después lo hizo matar

Otras parábolas estudiadas se relacionan estrechamente con Isaías 61:1-7, texto escrito 700 años de Señor Jesús y 100 años antes del exilio a Babilonia, pero que "danza" de forma maravillosa con las parábolas estudiadas y dichas por Jesús como en 5 pasos:

1. Isaías en 61:1-7 profetizó 700 años antes como sería el ministerio de
   Jesús
2. Jesús confirma la profecía de Isaías diciendo que en Él se cumplia
   (ver Lucas 4:16-21)
3. Jeśus enseñó con parábolas, incluyendo las que hemos estudiado y la
   interpretación de esas parábolas las volvemos a encontrar en Isaías
   61:1-7
4. El cumplimiento lo vivimos hoy quienes creemos en Jesús como Rey

Detallemos un poco más:

Paso 1:Isaías profetizó como sería el ministerio de Jesús

|Isaías 61|Ministerio de Jesús|
|1 El Espíritu de Jehová el Señor está sobre mí, porque me ungió Jehová; me ha enviado a predicar buenas nuevas a los abatidos, a vendar a los quebrantados de corazón, a publicar libertad a los cautivos, y a los presos apertura de la cárcel; 2 a proclamar el año de la buena voluntad de Jehová, y el día de venganza del Dios nuestro; a consolar a todos los enlutados; 3 a ordenar que a los afligidos de Sion se les dé gloria en lugar de ceniza, óleo de gozo en lugar de luto, manto de alegría en lugar del espíritu angustiado;|Cristo significa el ungido. Él mismo es La Palabra, Él mismo anuncia y es la Buena Nueva --que hoy seguimos compartiendo. Cumplió todo lo escrito|

Paso 2. Jesús lee Isaías 61:1-3 y lo confirma (Lucas 4:16-21)

| Lucas 4:21 Y comenzó a decirles: Hoy se ha cumplido esta Escritura delante de vosotros.|

Paso 3. Varias de las parábolas que hemos estudiado tiene interpretación en Isaías 61:1-7

| Parábola de Jesús | Interpretación en Isaías 61 |
| 26.Ago - Lucas 15:11-32 Parábola del hijo pródigo, que muestra la alegría del Padre cuando vuelve un hijo, aún cuando el hijo lo haya deshonrado. No lo recibe como empleado sino con toda la honra de hijo, para volver a comer en la casa del padre.| 6. Y vosotros seréis llamados sacerdotes de Jehová, ministros de nuestro Dios seréis llamados; comeréis las riquezas de las naciones, y con su gloria seréis sublimes.|
| 9.Sep - Lucas 14:15-24 La gran cena, que nos ayuda a reconocer que aunque no somos dignos, porque por nuestros pecados somos como mancos, cojos y pobres frente a Dios Él nos lleva a su Reino mediante Jesús | 7 En lugar de vuestra doble confusión y de vuestra deshonra, os alabarán en sus heredades; por lo cual en sus tierras poseerán doble honra, y tendrán perpetuo gozo.|
|9.Sep - Mateo 13:31-32 Que compara Reino de Dios con el crecimiento de la semilla de mostaza, que empieza muy pequeña pero crece, como nuestra fe y como la iglesia | 3b y serán llamados árboles de justicia, plantío de Jehová, para gloria suya.|

Paso 4: Las promesas se cumplen en nosotros los siervos del Rey Jesús Al
menos yo si, pero evalúe cada uno desde que conoció Cristo:

|Abatidos recibimos las buenas nuevas |
|Quebrantados de corazón hemos sido remendados |
|Jesús nos ha hecho libres del cautiverio y la cárcel |
|Hemos sido consulado en el luto |
|Cuando estabamos afligidos hemos recibido gloria en lugar de ceniza |
|En lugar del loto recibimos óleo de gozo |
|En lugar de espíritu angustiado hemos recibido manto de alegría |
|No les parece una relación maravillosa, entre texto escritos con 700 años de diferencia y nuestra lectura 2000 años después? Así son los diseños de Dios. |

La cuestión es que se cumpla para quienes están comenzando la vida
cristiana, y que se mantengan para quienes ya las recibimos. Y hay
parábolas que apuntan a eso como otra estudiada el 2.Sep:

Lucas 3:3-15 Parábolas de Juan el Bautista comparando gente con víboras
y con árboles que serían cortados si no daban buenos frutos de
arrepentimiento.

O la de hoy en Lucas 12:35-48 cuando Jesús nos enseña (1) como ser
buenos siervos suyos a todos los miembros de la iglesia y (2) que no
hacer a quienes estamos sirviendo en la iglesia --entendiendo que todos
los miembros somos llamados a capacitarnos y ejercer nuestros dones para
servir en la iglesia a medida que maduramos en la vida cristiana.

# 2. Texto: Lucas 12:35-48

El siervo vigilante

    35 Estén ceñidos vuestros lomos, y vuestras lámparas encendidas;
    36 y vosotros sed semejantes a hombres que aguardan a que su señor regrese de 
    las bodas, para que cuando llegue y llame, le abran en seguida.
    37 Bienaventurados aquellos siervos a los cuales su señor, cuando venga, 
    halle velando; de cierto os digo que se ceñirá, y hará que se sienten a la 
    mesa, y vendrá a servirles.
    38 Y aunque venga a la segunda vigilia, y aunque venga a la tercera vigilia, 
    si los hallare así, bienaventurados son aquellos siervos.
    39 Pero sabed esto, que si supiese el padre de familia a qué hora el ladrón 
    había de venir, velaría ciertamente, y no dejaría minar su casa.
    40 Vosotros, pues, también, estad preparados, porque a la hora que no 
    penséis, el Hijo del Hombre vendrá.

El mayordomo infiel

    41 Entonces Pedro le dijo: Señor, ¿dices esta parábola a nosotros, o también 
    a todos?
    42 Y dijo el Señor: ¿Quién es el mayordomo fiel y prudente al cual su señor 
    pondrá sobre su casa, para que a tiempo les dé su ración?
    43 Bienaventurado aquel siervo al cual, cuando su señor venga, le halle 
    haciendo así.
    44 En verdad os digo que le pondrá sobre todos sus bienes.
    45 Mas si aquel siervo dijere en su corazón: Mi señor tarda en venir; y 
    comenzare a golpear a los criados y a las criadas, y a comer y beber y 
    embriagarse,
    46 vendrá el señor de aquel siervo en día que éste no espera, y a la hora que 
    no sabe, y le castigará duramente, y le pondrá con los infieles.
    47 Aquel siervo que conociendo la voluntad de su señor, no se preparó, ni 
    hizo conforme a su voluntad, recibirá muchos azotes.
    48 Mas el que sin conocerla hizo cosas dignas de azotes, será azotado poco; 
    porque a todo aquel a quien se haya dado mucho, mucho se le demandará; y al 
    que mucho se le haya confiado, más se le pedirá.

# 3. Contexto

Los versículos anteriores en el capítulo 12 tratan sobre:

* 1-3 La hipocresia de los fariseos
* 4-7 Temer a Dios y no a los hombres
* 8-12 Jesús confesará sobre aquellos que lo confiesen a Él
* 13-21 Parábola del rico insensato, rico para si mismo pero pobre para Dios
* 22-31 No afanarse por que comer o vestir sino por buscar el reino de Dios
* 32-34 Dar limosna para hacer tesoros en el cielo y no en la tierra

Hay un énfasis en no preocuparnos por lo material, sino por ser ricos ante Dios y el método dado por Jesús es vender lo que se tiene para darlo como limosna, esto es interpretado por el comentarista Mathew Henry así "en lugar de querer, con los bienes aliviar a quienes realmente necesitan, vende lo que tienes que es superfluo, todo lo adicional a la manutención propia y de la familia dalo a los pobre. Vende lo que tienes, si encuentras que es un obstáculo o estorbo en el servicio de Cristo"

# 4. Análisis

## 4.1 Siervo fiel

La primera parábola tiene 2 para ejemplificar lo mismo, (1) la de un siervo que espera a su amo y (2) la de un padre de familia que protege su casa.

Me parece que no es tan fácil entender la primera de estas parábolas por el cambio en las relaciones laborales entre el tiempo de Jesús y el actual, para la primera parte la imagen que tengo es:

![Contratado para esperar](https://frommadridtomadrid.files.wordpress.com/2013/02/img_3659.jpg)

Una persona por ejemplo en un hotel contratada para esperar en el aeropuerto a otra que no conoce, debe portar un letrero con el nombre y así se retrase el vuelo esperar hasta que llegue.

Otra que tal vez se acerque podría ser un guardaespaldas/chofer, conozco algunos de líderes sociales colombianos de tienen esquema de protección con carro y guardaespaldas que lo maneja, he hablado con algunos de estos guardaespaldas y se que deben acomodar sus horarios a los de quien protegen cada vez que estos salen a espacios públicos, no vuelan con sus protegidos cuando deben viajar de una ciudad a otra pero a su regreso los recogen en el aeropuerto. Si el vuelo se retrasa, si los vuelos cambian o si los planes del líder cambian el guardaespaldas tiene que re-acomodar sus tiempos y planes para garantizar estar en el aeropuerto justo en el momento que vuelva el líder.

Una última imagen es la de un acompañante internacional, por ejemplo de Brigadas de Paz, son extranjeros no armados que acompañan a líderes colombianos que no tienen recursos o se niegan a aceptar esquemas de protección armados:

![PBI espera](https://i0.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Avre-yond%C3%B3-taller-Silvia.jpg?resize=768%2C512&ssl=1)
Imágen de [https://i0.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Avre-yondó-taller-Silvia.jpg?resize=768%2C512&ssl=1](https://i0.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Avre-yond%C3%B3-taller-Silvia.jpg?resize=768%2C512&ssl=1)

Al igual que el guardaespaldas deben esperar a los líderes que acompañan y re-acomodar sus planes para asegurar estar en el aeropuerto cuando llega. Ellos también acompañan comunidades altamente victimizadas y en riesgo de seguirlo siendo y lo que hacen es convivir con la comunidad y coordinar para estar en tantos eventos comunitarios como es posible, eso lo hacen en paralelo con reportar lo que hacen y el contexto para que
otros equipos en el extranjero puedan levantar fondos para su
manutención:

![](https://i1.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Laeti_Mapiripan_Febrero-2017.jpg?resize=672%2C372&ssl=1)
Imagen de [https://i1.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Laeti_Mapiripan_Febrero-2017.jpg?resize=672%2C372&ssl=1](https://i1.wp.com/pbicolombiablog.org/wp-content/uploads/2018/06/Laeti_Mapiripan_Febrero-2017.jpg?resize=672%2C372&ssl=1)

Incluso hay acompañantes internacionales cristianos cercanos a nuestra iglesia en Magdalena Medio, los Equipos Cristianos de Acción por la Paz.

La otra parábola si la entendemos los padres de familia, si supieramos a que hora fueran a robar nuestra casa, organizariamos para reforzar puertas, estar presenetes, epdir ayuda y evitar el robo.

Así debemos esperar a Jesús.

Jesús no necesita guardaespaldas, de hecho Él obra como nuestro guardaespaldas, pero de un cristiano lo que pide es obediencia. Su mandamiento resumido ya lo conocemos "amar a Dios con todo el corazón, toda el ama, toda la mente y todas las fuerzas y al prójimo como a mi mismo," y ya vimos que el énfasis del capítulo es con bienes materiales. Ese es el trabajo de cada cristiano(a) que no debo parar de hacerse por esperar a que Jesús vuelva, sino que yo y usted debemos hacer de manera continua, viendo las señales que Dios ha provisto para evaluarme (el fruto del Espíritu) y en comunicación continúa con Jesús mediante la lectura de la palabra y la oración para poder cambiar planes y que todo el tiempo me vea haciendo lo correcto, incluso cuando vuelva. Por cierto si quiero conocer cuando Jesús vuelve en cuerpo lo mejor es preguntarle directo a Él, pero por su gracia podemos pedir que su Espíritu vuelva y llene a cada cristiana tanto como queramos.

## 4.2 Mayordomo infiel

    41 Entonces Pedro le dijo: Señor, ¿dices esta parábola a nosotros, o también a todos?
    42 Y dijo el Señor: ¿Quién es el mayordomo fiel y prudente al cual su señor pondrá sobre su casa, para que a tiempo les dé su ración?

Esta segunda es dirigida más a líderes de iglesia pues es respuesta a Pedro cuando él pregunta sobre él y los otros discípulos. La comparación es con un mayordomo, tal vez un análogo en nuestro contexto es cuando unos padres de familia dejan sus hijos al encargo de alguien, bien en un jardín o bien una persona que viene a cuidarlos en su casa. O bien un jefe que tiene a cargo persona subordinadas.

    43 Bienaventurado aquel siervo al cual, cuando su señor venga, le halle haciendo así.
    44 En verdad os digo que le pondrá sobre todos sus bienes.
    45 Mas si aquel siervo dijere en su corazón: Mi señor tarda en venir; y comenzare a golpear a los criados y a las criadas, y a comer y beber y embriagarse,
    http://cdn3.radiosantafe.com/wp-content/uploads/2018/09/Dmvf-zYU4AEOkoW.jpg http://cdn3.radiosantafe.com/wp-content/uploads/2018/09/Dmvf-zYU4AEOkoW.jpg

La foto es de una noticia del 10.Sep.2018 que entre otras dice: "En las cámaras de seguridad del jardín se observa como una de las personas que tenía a cargo el cuidado de los niños los golpeaba. “Desde que la niña está en ese lugar, siempre que llega a la casa presenta golpes en su cuerpo”, afirmó Sandra Arango, tía de la niña. Según con los padres de familia, son varios los niños que han sido maltratados en el jardín infantil por la misma mujer."
[https://www.radiosantafe.com/2018/09/10/denuncian-maltrato-a-una-nina-de-10-meses-en-un-jardin-infantil-en-cali/](https://www.radiosantafe.com/2018/09/10/denuncian-maltrato-a-una-nina-de-10-meses-en-un-jardin-infantil-en-cali/)

Pero yo predico para mí también, porque otras comparaciones son con un jefe que maltrata a sus subordinados o con un padre o madre de familia  que maltrata a su familia, y bueno en mi caso tengo subordinados y he tenido que ir a un comité de convivencia porque me he pasado con un trato verbal y de actitudes muy duras, y así mismo ya siendo cristiano hace
unos años Dios tuvo que defenderme en fiscalía por maltrato familiar.

Obviamente ese no es el fruto del Espíritu, aunque tanto mis
subordinados como familia han dicho que he venido mejorando. He
analizado que suelen ser situaciones de pereza mías o de falta de preparación mías, para mejorar he requerido más preparación, terapias y debo hacer cada mañana ejercicios de respiración y pedir la presencia del Espíritu Santo. Y también pido oración por mi carácter y personalidad para con diligencia dar el fruto del Espíritu.

    46 vendrá el señor de aquel siervo en día que éste no espera, y a la
    hora que no sabe, y le castigará duramente, y le pondrá con los
    infieles. 

En el infierno seguramente habrá líderes cristianos y pastores, y este versículo refuerza otra creencia compartida por muchos menonitas (pero no por luteranos o bautistas por ejemplo): la salvación debe cuidarse. Dios puede poner como infiel a uno de sus siervos, borrar del libro de la vida a alguien que haya hecho la oración de fe, pues la vida es un proceso y la salvación también.

    47 Aquel siervo que conociendo la voluntad de su señor, no se preparó,
    ni hizo conforme a su voluntad, recibirá muchos azotes.
    48 Mas el que sin conocerla hizo cosas dignas de azotes, será azotado
    poco; porque a todo aquel a quien se haya dado mucho, mucho se le
    demandará; y al que mucho se le haya confiado, más se le pedirá.

A quienes hemos madurado en el cristianismo Dios nos ha dado mucho, se nos han cumplido todas las promesas de Isaías 61, Dios espera de nosotros excelencia en el servicio, materializada en buenas relaciones en nuestra primera iglesia que es al familia, en la familia ampliada que es la comunidad de fe y en testimonio para poder evangelizar con testimonio.

# 5. Conclusión y oración

Señor gracias por tus planes y diseños y la revelación progresiva que nos has dado, mediante Isaías que en sus escritos dialoga con Jesús con 700 años de diferencia y hoy ambos nos enseñan con 2000 años de diferencia.

Gracias por cumplir tus promesas de Isaías 61:1-7. Señor gracias por darnos buenas nuevas cuando estamos abatidos. 

Gracias por remendar nuestro corazones quebrantados. 

Gracias por librarnos del cautiverio y de la carcel. 

Gracias por consolarnos en el luto. 

Gracias por cambiar las cenizas por gloria cuando estabamos afligidos. 

Gracias por cambiar el luto por óleo de gozo. 

Gracias por cubrirnos con manto de alegría cuando teníamos un espíritu angustiado.

Señor perdona cuando no he sido un siervo fiel tuyo, cuando he ignorado que eres Rey y no te he hecho caso. 

Perdónanos cuando quienes servimos en la iglesia hemos maltratado a nuestros herman@s. 

Perdonanos cuando como padres y madres de familia no hemos visto a nuestra familia como una iglesia y cuando hemos maltratado a nuestros hiij@s o espos@s. Perdona cuando nos hemos embriagado, comido de gula, cuando hemos sido peresozos.

Por favor ayudanos a dar frutos de arrepentimiento. Por favor ayudanos a ser diligentes.

Dios Padre quiero hacer bien labor de cristiano, de amarte sobre todo y  amar al prójimo como a mi mismo. No quiero esperar a que vuelvas en cuerpo para hacerlo, quiero que ahora y continuamente vuelvas con Tu Santo Espiritu y me llenes para hacer bien mi trabajo. Señor quiero pasar la prueba, quiero ver el fruto de tu Espíritu en mi relacionamiento con toda persona con la que me relaciono y en mi labor evangelística, por favor ayudame y guiame para ver amor, gozo, paz, paciencia, benignidad, bondad, fe, mansedumbre y templanza en el nombre de Jesús.

Señor capacitame para ser buen lider en mi casa, en mi trabajo y en la iglesia en el nombre de Jesús.

Si alguien aún no ha recibido al Señor Jesús en su corazón, y aún no tiene las promesas de Isaías 61:1-7, pero quiere vivirlas aceptando a Jesús como Rey, le invitamos a hacer una oración de fe.

# 6. Referencias

* Biblia Reina Valera 1960
* Las parábolas de Jesús 39: El siervo vigielante y el siervo infiel. Pastor Olaff Saavedra. 2016 <https://pastorolaffsaavedra.blogspot.com/2016/01/las-parabolas-de-jesus-39-el-siervo.html> Consultado 12.Sep.2018
* <https://bible.knowing-jesus.com/Espa%C3%B1al/topics/Contentamiento>
* <http://www.gracia.org/recursos.aspx?p=a&article=362>
* Matthew Henry's Commentary disponible en <http://www.biblegateway.com>

Este escrito de dominio público también está disponible com
documento OnlyOffice en línea: [http://bit.ly/2MIk2vu](http://bit.ly/2MIk2vu)