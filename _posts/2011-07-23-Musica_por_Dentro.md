---
layout: post
title: "Musica_por_Dentro"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Lily Goodman y Tercer Cielo

<pre>
Tengo música por dentro
   Lilly Goodman yeah
Yo no la puedo detener
   Tercer cielo
Me fluye desde adentro
de mi interior, de mi interior

Dios puso en mi el ritmo
El gozo de mi corazón
Como voy a esconderlo, si libre soy,
si libre soy

Yo no puedo callar mi voz, yo no puedo callar mi voz
Yo no puedo callar mi voz, yo no puedo callar mi voz
Yo no puedo callar mi voz, yo no puedo callar mi voz
Yo no puedo callar mi voz, yo no puedo callar mi voz

Siente el ritmo
Solo así 
Quiero soltar lo que hay en mi
Y en medio cielo
Lo que tengo adentro
De cantarlo yo no me arrepiento
No me critiques cuando escuches mi voz
Mejor ponte adorar que el fuego cayo
El party ya empezó
Estoy contento yo

Tengo motivos, razones que me sobran
Por eso mi alma no se cansa siempre adora
Hoy descubrí que cuando mas canto yo, mas libre soy
Canta, danza, da tu alabanza con el alma y corazón

Tengo música por dentro
Yo no la puedo detener
Me fluye desde adentro
De mi interior, de mi interior

Dios puso en mi el ritmo
El gozo de mi corazón
Como voy a esconderlo, si libre soy
si libre soy

Cuando me veas toda sonriente
Y andando por la calle
Mo es nada raro es que el gozo del Señor me invade
Asi que unete a mi
Deja tus penas salir
Olvida que diran
Dejate llevar
Empienza alabar

Abre tu corazon
Deja que entre Dios
Por que estar triste y solo
Si Él libertad puede darte

Canta, danza, da tu alabanza con el alma y corazón

Tengo música por dentro
Yo no la puedo detener
Me fluye desde adentro
De mi interior, de mi interior

Dios puso en mi el ritmo,
El gozo de mi corazón,
Como voy a esconderlo, si libre soy
Si libre soy

Yo no puedo callar mi voz, yo no puedo callar mi voz (x8)
</pre>

* Canción: http://www.youtube.com/watch?v=42j-BDWi5A8
* Letra basada en: http://www.musica.com/letras.asp?letra=1297384
