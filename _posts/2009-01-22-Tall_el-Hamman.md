---
layout: post
title: "Tall_el-Hamman"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Se trata de un sitio en al noreste del Mar Muerto en Jordania, que ha sido excavado recientemente por Associates for Biblicas Research.  Según un reporte de la misma asociación ([http://www.biblearchaeology.org/post/2009/01/Tall-el-Hammam-2008-A-Personal-Perspective.aspx]) allí se han construido ciudades, una sobre otra. 

La primera parecer ser muy antigua (antes del 3000aC) e importante --algunos proponen que se trató de Sodoma--, parece haber sido destruida e inhabitada durante unos 500 años. 

Se construyó otra en el tiempo de Salomón (alrededor del 1000aC) que pudo ser capital de uno de sus distritos, esta también fue destruida.  

Otra ciudad se construyó en el mismo lugar durante el imperio romano y tal vez existió en el tiempo de Jesús ---puede ser Abila o Livias (Julias) capital de Perea.   

Como concluye el reporte "Durante cada periodo de la historia, se mantuvo como un testigo silencioso de algunas de los personajes y eventos más grandiosos de la Biblia".
