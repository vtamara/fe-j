---
layout: post
title: Conocer amor de Dios
author: vtamara
categories:
- Prédica
image: "/assets/images/cruzcol.jpg"
tags: Prédica

---
## 1. INTRODUCCIÓN

7 niños y niñas del ministerio "Danzadores y danzadoras de Jesucristo" hicieron oración de fe para recibir a Cristo en sus corazones como Señor y Salvador el 10 de Enero de 2016 de este año en la iglesia Menonita, Caminos de Esperanza de Suba, es un gozo. El tema de la prédica de Gonzalo Bonilla fue la salvación a partir de 3 sencillas preguntas:

* ¿Quién es Jesús?
* ¿Hay un cielo y un infierno?
* ¿Si hoy mueres a donde vas?

El 17 de Enero, Sonia predicó sobre dar buen fruto, refiriéndose al fruto del Espíritu y en oposición al fruto de la carne. Y como debemos estar unidos a Cristo para darlo.

A propósito de recibir a Cristo en el corazón el Señor me regaló un pasaje muy hermoso que describe:

1\. Lo que podemos hacer quienes predicamos para esto: orar para que el Espíritu Santo los fortalezca en su interior y así Cristo efectivamente habite en sus corazones

2\. Una consecuencia de que Cristo habite en el corazón: comprender el infinito amor de Dios.

## 2. TEXTO

Efesios 3:14-21

    14 Por esta causa doblo mis rodillas ante el Padre de nuestro Señor 
       Jesucristo,
    
    15 de quien toma nombre toda familia en los cielos y en la tierra,
    
    16 para que os dé, conforme a las riquezas de su gloria, el ser fortalecidos 
       con poder en el hombre interior por su Espíritu;
    
    17 para que habite Cristo por la fe en vuestros corazones, a fin de que, 
       arraigados y cimentados en amor,
    
    18 seáis plenamente capaces de comprender con todos los santos cuál sea la 
       anchura, la longitud, la profundidad y la altura,
    
    19 y de conocer el amor de Cristo, que excede a todo conocimiento, para que 
       seáis llenos de toda la plenitud de Dios.
    
    20 Y a Aquel que es poderoso para hacer todas las cosas mucho más 
       abundantemente de lo que pedimos o entendemos, según el poder que actúa en 
       nosotros,
    
    21 a él sea gloria en la iglesia en Cristo Jesús por todas las edades, por 
       los siglos de los siglos. Amén.
    

## 3. CONTEXTO

Efesios 3:1-13 Pablo explica que Dios quiso que los gentiles también fuésemos coherederos y miembros del cuerpo de Cristo por la fe en Él.

Efesios 4:1-16 Invita a estar unidos en la fe y al crecimiento para ser de la estatura de Cristo mediante el ejercicio de la vocación y dones que Jesús le regala a cada creyente.

Aparece una secuencia entonces entre los 3 grupos de pasajes:

1\. Dios permite que los gentiles recibamos la salvación por fe en Cristo. 2. Espíritu Santo que nos fortalezca y que Jesús viva en nuestro corazones y conozcamos su amor 3. Servir con los dones y vocación

El libro Efesios fue una carta escrita por Pablo, por lo que leemos en 3:1, 4:1 y 6:20 (ver {2}), mientras estaba prisionero en Roma posiblemente hacia el año 62dC. Estaba dirigida a la iglesia ubicada en el puerto de Éfeso, iglesia que él mismo había fundado en su segundo viaje misionero (ver Hechos 18:19-21).

Éfeso se ubica en Asia Menor en la actual Turquía. En el tiempo de Pablo era un puerto romano que florecía por el comercio donde había un famoso templo dedicado a la Artemisa y por lo visto una gran comunidad judía pues había una sinagoga en la que Pablo predicó.

![https://lasteologias.files.wordpress.com/2010/08/viajes-misioneros-pablo.jpg](https://web.archive.org/web/20160812140515im_/http://fe.pasosdejesus.org/?binary=https%3A%2F%2Flasteologias.files.wordpress.com%2F2010%2F08%2Fviajes-misioneros-pablo.jpg) (Mapa de [https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/"))

[https://drive.google.com/file/d/0B8_HQfYDVFPuLUtZQkpsd2NVRTQ/view?usp=sharing](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/") Fachada de la biblioteca de Éfeso, de [https://es.wikipedia.org/wiki/%C3%89feso](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/")

Nota: Al igual que el resto del nuevo testamento la carta a los Efesios está escrito en griego que era la lengua más popular en esos tiempos y en la que Pablo predicaba a los gentiles (aunque la lengua de Jesús y que sus discípulos y el mismo Pablo debía hablar con los judíos era el arameo, pues el hebreo era la lengua de los eruditos y en la que estaba escrito el antiguo testamento que seguramente Jesús leía, ver {3}).

Nota: Políticamente el 62dC era un tiempo en el que los Judíos estaban perdiendo cada vez más autonomía frente al imperio romano. Los llamados reyes judíos (incluyendo a Herodes el grande quien pretendió matar a Jesús cuando era recién nacido, sus tres hijos Arquelao, Antipas y Filipo así como el subsiguiente reunificador Agripa) habían perdido toda autoridad para el 44dC cuando Agripa murió. Y todo el territorio que había sido de Israel era gobernado por un procurador romano. Esto condujo a una rebelión armada por parte de los judíos (se destacan los "zelotes") en el año 66 y la posterior guerra judía hasta el año 70dC cuando el ejército romano tomó a Jerusalén y destruyó el templo.

Esto explica porque en varias oportunidades Pablo dijo que era ciudadano romano evitando maltratos e injusticia por parte de autoridades romanas --siendo fuertemente perseguido por las autoridades judías. Era ciudadano romano por haber nacido en Tarso ubicado en Asi y que era romana, aún cuando su familia era judía.

## 4. ANÁLISIS

Voy a centrar nuestro interés un momento en una hermosa promesa del texto estudiado: 17 para que habite Cristo por la fe en vuestros corazones, a fin de que, arraigados y cimentados en amor, 18 seáis plenamente capaces de comprender con todos los santos cuál sea la anchura, la longitud, la profundidad y la altura, 19 y de conocer el amor de Cristo, que excede a todo conocimiento, para que seáis llenos de toda la plenitud de Dios.

Inicialmente yo me preguntaba como podía conocerse algo que excede todo conocimiento, la respuesta me la dió el Señor en 1 Cor 2:10-12:

    10 Pero Dios nos las reveló a nosotros por el Espíritu; porque el Espíritu 
      todo lo escudriña, aun lo profundo de Dios.
    11 Porque ¿quién de los hombres sabe las cosas del hombre, sino el espíritu 
      del hombre que está en él? Así tampoco nadie conoció las cosas de Dios, 
      sino el Espíritu de Dios.
    12 Y nosotros no hemos recibido el espíritu del mundo, sino el Espíritu que 
      proviene de Dios, para que sepamos lo que Dios nos ha concedido,
    

Es decir no podemos conocer en nuestras limitaciones humanas la inmensidad 
del amor de Dios, pero mediante el Espíritu Santo podemos conocerlo.

### 4.1 Ejemplo. Pablo: Testimonio de alguien que conoció más del amor de Dios

Si uno examina los viajes misioneros de Pablo se da cuenta de los sufrimientos 
que tuvo que pasar, por ejemplo parte de su primer viaje puede resumirse así:

* 13:51 Expulsados junto con Bernabe van a Iconio
* 14:6 Por riesgo de apedreamiento huyen a Listra en Licaonia
* 14:19 Apedreado en Listra hasta que creyeron muerto a Pablo quien después milagrosamente se levanta y se fue con Bernabé hacía Derbe
* 14:21 Predica en Derbe, y vuelve a Listra e Iconio animando a los que ya habían creído
* 14:21 Antioquia de Psidia
* 14:24 Panfilia
* 14:25 Perge y Atalia
* 14:26 Se embarcan de vuelta a Antioquía de Siria

El amor de Pablo por los gentiles, como nosotros, de Listra fue tal que primero les predico hasta que los envidiosos lo apedrearon hasta creerlo muerto, Dios no permitió que muriera, se fue a Derbe unos días pero unos días después volvió para animar a los que empezaron a creer en la misma Listra. ¿Quién de nosotros se arriesgaría a esto? Si así es el amor de Pablo por los gentiles de Listra, ¿cuanto mayor será el de Dios hacía ellos? Cuan bien lo conoció Pablo que llegó a obedecer y amar como lo hizo.

### 4.2 Éfeso, una iglesia madura

Hay quienes escriben que el libro a los Efesios es el más profundo respecto a la iglesia (ver {3}), y es dirigido a una iglesia madura. Uno se pregunta como fue la fundación de la iglesia de Éfeso, encuentro:

1 En su segundo viaje misionero Pablo pasó por Éfeso junto varios acompañantes incluyendo a Aquila y Priscila (con quienes había fundado la iglesia en Corinto). Como dice Hechos 1:19-21 se separó por un momento de sus acompañantes y predicó en la Sinagoga. Los judíos le pidieron quedarse pero el prometió volver posteriormente.

2 Aquila y Priscila estuvieron en Éfeso antes que Pablo volviera, tal vez sólo por momentos, mejorando la fe por ejemplo de un judío de nombre Apolo quien sin mucho conocimiento empezó a predicar sobre Jesús en la misma sinagoga de Éfeso de acuerdo a 18:24-25 y que fue mejor encaminad por Aquila y Priscila, y quienes lo enviaron a la iglesia de Corinto donde predico con valor.

3 Como se describe en Hechos 19, en su tercer viaje misionero Pablo pasó por Éfeso, bautizó en el nombre de Jesús e impuso manos con lo que descendió el Espíritu Santo y hablaron en lenguas unos 12 recientes discípulos. 3 meses predicó los sábados en la sinagoga. Como no fue aceptado por varios judíos, se fue a instruir a los gentiles en la escuela de Tirano diariamente. Y se quedó allí durante 2 años, por lo que muchas personas pudieron escuchar el mensaje del Señor. Todo confirmado mediante milagros, al punto que mandaban pañuelos de enfermos y cuando Pablo los tocaba, los enfermos sanaban o eran libres de demonios.

4 Tuvo que salir prácticamente expulsado hacía Macedonia, fue expulsado por los orfebres de Éfeso cuyo negocio de hacer imágenes de la "diosa" artemisa estaba viéndose afectado por la efectiva predicación y las conversión que el Señor hacía en ese puerto.

Aunque Pablo salió, quedó bien plantada una iglesia, a la que después dirigió esta carta, cuyas enseñanzas han servidor a todas las iglesias por generaciones.

Uno pensaría entonces que dos ingredientes centrales para la fundación de esa iglesia de Éfeso son: amor y constancia. Constancia por ejemplo para enseñar a diario durante dos años.

## 5.CONCLUSIÓN Y ORACIÓN

* Señor así como Pablo te rogaba por los nuevos creyentes, hoy te pedimos por quienes han recibido reciente o hace tiempo a Cristo, para que nos fortalezcas en nuestro interior mediante tu Santo Espíritu y así pueda Cristo habitar en nuestros corazones y conozcamos lo infinito de tu Amor.
* Señor que conociendo lo infinito de tu amor, aumente nuestro amor por Ti y por nuestro prójimo, para llegar a obedecerte como Pablo lo hacía, arriesgando hasta la vida.
* Señor que mezclemos el amor y la constancia para edificar en Ti nuestra vida, nuestras familias y nuestra iglesia.
* Señor como protegiste a Pablo, protégenos Señor, que conozcamos tu voluntad santa y perfecta.

## REFERENCIAS

* {1} Biblia Reina Valera 1960. [https://www.biblegateway.com/passage/?search=Eph+3%3A14-21&version=RVR1960](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/")
* {2} [https://es.wikipedia.org/wiki/Ep%C3%ADstola_a_los_Efesios](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/")
* {3} [http://es.aleteia.org/2014/11/11/israel-reconoce-oficialmente-el-idioma-arameo-que-hablaba-jesus/](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/")
* {4} [https://es.wikipedia.org/wiki/%C3%89feso](https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/ "https://lasteologias.wordpress.com/2010/08/08/carta-a-los-efesios-parte-1/")

***

Esta predica se compartió el 24.Ene.2016 en la Iglesia Menonita Caminos de Esperanza de Suba. Damos gloria y honra a nuestro Señor de amor infinito.

Dibujo de [https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html](https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html "https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html")
