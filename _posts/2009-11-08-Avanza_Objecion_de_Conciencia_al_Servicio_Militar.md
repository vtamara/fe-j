---
layout: post
title: "Avanza_Objecion_de_Conciencia_al_Servicio_Militar"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Transcribiendo de

[http://www.corteconstitucional.gov.co/comunicados/No.%2043%20Comunicado%2014%20de%20Octubre%20de%202009.php]


##3. EXPEDIENTE D-7685        -          SENTENCIA C-728/09

Magistrado ponente: Dr. Gabriel Eduardo Mendoza Martelo

 
!3.1.    Norma acusada

LEY 48 DE 1993

(marzo 3)

Por la cual se reglamenta el servicio de reclutamiento y movilización

 

ARTÍCULO 27. EXENCIONES EN TODO TIEMPO. Están exentos de prestar el servicio militar en todo tiempo y no pagan cuota de compensación militar:

a. Los limitados físicos y sensoriales permanentes.

b. Los indígenas que residan en su territorio y conserven su integridad cultural, social y económica.

 

!3.2.    Problema jurídico planteado

Le corresponde a la Corte resolver, si el legislador al establecer en el artículo 27 dos hipótesis en las que se está exento de prestar el servicio militar en todo tiempo, incurrió en una omisión legislativa relativa contraria a la igualdad (art. 13 C.P.), la libertad de conciencia (art. 18 C.P.) y a la libertad de cultos (art. 19 C.P.), por no incluir a los objetores de conciencia.

!3.3.    Decisión

Primero. - Declarar EXEQUIBLE, por el cargo analizado, el artículo 27 de la Ley 48 de 1993.

Segundo. - Exhortar al Congreso de la República para que, a la luz de las consideraciones de esta providencia, regule lo concerniente a la objeción de conciencia frente al servicio militar.

 

!3.4.    Fundamentos de la decisión  

La Corte decidió declarar la exequibilidad del artículo 27 de la Ley 48 de 1993, fundamentada en que contrario a lo afirmado en la demanda, de la norma acusada no cabe predicar la configuración de una omisión legislativa relativa, por cuanto allí se regulan unas exenciones a la prestación del servicio militar bajo unos presupuestos que no son predicables de los eventuales objetores de conciencia. Existen evidentes diferencias entre los sujetos a los que se refiere la norma y estos últimos, que impiden asimilarlos.  La exención alude a circunstancias objetivas presentes en los allí mencionados grupos de personas, en razón de las cuales no existe para ellos la obligación de prestar el servicio militar.  La objeción de conciencia alude a consideraciones subjetivas, por las cuales una persona se opone a prestar el servicio al que está obligado, por razones de conciencia. Se trata de dos situaciones diferentes que no tenían que haber sido reguladas en la misma norma. En ese caso, en relación con esa norma, podría predicarse la existencia de una omisión legislativa relativa.

A juicio de la Corte, no hay omisión relativa, porque la omisión pretendida por los actores no se predica de la disposición demandada. Además,  la omisión alegada podía predicarse de diversos artículos de la mencionada ley, indeterminación que obra en contravía de la procedencia del mencionado fenómeno,  el cual exige plena certidumbre de que la norma demandada sea precisamente la que debe  incluir en sus regulaciones una prescripción que desarrolle el imperativo constitucional del derecho a la objeción de conciencia. La decisión adoptada tuvo en cuenta, además, que, en principio, la regulación de los derechos fundamentales, como es el caso de la objeción de conciencia, en cuanto se oriente a desarrollar de manera específica y completa el derecho, e incluya los procedimientos y recursos para su protección, debe hacerse mediante ley estatutaria, siendo, una ley de esa naturaleza,  la llamada a regular el citado derecho. Tal situación lleva a concluir que la omisión sería absoluta lo cual conduce, a su vez, ante la necesidad de que se regule el derecho fundamental en cuestión, como también lo sugieren instrumentos internacionales, a que se exhorte al Congreso de la República para que expida la correspondiente normativa. 

No obstante desechar la alegada omisión legislativa relativa, la Corte consideró que,  atendiendo la naturaleza fundamental del derecho de objeción de conciencia, de aplicación inmediata, ésta podía hacerse valer mediante el ejercicio de la acción de tutela,  aún frente al deber de prestar el servicio militar obligatorio, sobre la base de la demostración de circunstancias excepcionalmente extremas que así lo justifiquen, indicativas de la imposibilidad irreductible que surge para el objetor de acometer algunas de las actividades inherentes al cumplimiento de dicho deber, por resultar abiertamente incompatibles con las comprobadas, serias y reales razones de conciencia que aduzcan, aspecto en relación con el cual la Corte decidió variar la jurisprudencia existente sobre el particular, opuesta a dicha postura.

 

3.5.    Los magistrados MARIA VICTORIA CALLE CORREA, JUAN CARLOS HENAO PÉREZ, JORGE IVÁN PALACIO PALACIO y LUIS ERNESTO VARGAS SILVA manifestaron su salvamento de voto en relación con la decisión de exequibilidad adoptada en esta sentencia.

Los magistrados CALLE CORREA, HENAO PEREZ y PALACIO PALACIO, si bien comparten plenamente la existencia de la objeción de conciencia en el contexto del servicio militar obligatorio en el orden constitucional vigente, tal como lo señala la mayoría de la Sala Plena en su decisión, se apartan de ésta, en cuanto también se decide que el legislador no incurrió en una omisión legislativa relativa. En consecuencia, consideran que la Corte debió declarar exequible la norma pero condicionando su constitucionalidad en el entendido de que la exención al servicio militar obligatorio también se aplica, íntegramente, a aquellas personas para las cuales prestar ese servicio supone actuar en contra de sus convicciones o creencias propias, profundas, fijas y sinceras.

En su concepto, la razón por la que se exime en todo tiempo del servicio militar obligatorio a los grupos de personas que, como los indígenas o las personas con limitaciones, selecciona la norma, es que en uno y otro caso para estas personas la prestación del servicio es incompatible con el libre desarrollo de sus vidas e identidades -por pertenecer a una cultura diferente o por tener una condición física o mental especial- y por tanto, son objeto de protección constitucional, en calidad de derechos. A su juicio, estos grupos son asimilables con el de las personas para las cuales prestar el servicio militar obligatorio implica actuar en contra de su conciencia o de sus creencias religiosas, cuando éstas son tan profundas, fijas, sinceras y determinan su obrar externo como, por ejemplo, las creencias y las convicciones propias de un indígena que hace parte de su comunidad. Las personas para la cuales prestar servicio militar obligatorio implica actuar en contra de su libertad de conciencia o de su libertad religiosa, sí son comparables a los grupos de personas contemplados en el artículo 27 de la Ley 48 de 1993. De hecho, en algunos aspectos, los grupos de objetores de conciencia son más afines a los dos grupos contemplados en dicha disposición legal (los indígenas que conservan su identidad y los limitados físicos y sensoriales permanentes) que éstos dos grupos entre sí. Para los magistrados que salvan su voto es claro que sí existe un derecho de objeción de conciencia, y existe a la vez el deber legal de regular las exenciones al servicio militar obligatorio, de manera que el no haber incluido a los objetores como un caso previsto en la Ley de reclutamiento, es una clara omisión legislativa relativa. La omisión se hace evidente en el reconocimiento que hace la Sala Plena  (i) de la necesidad de que el legislador regule la cuestión, y (ii) de que al hacerlo, lo haga a la luz de lo establecido por la sentencia.

Por su parte, el magistrado VARGAS SILVA consideró que la Corte debió declarar la exequibilidad condicionada de la norma acusada, para corregir la omisión en que incurrió el legislador al excluir a los objetores de conciencia de la prestación del  servicio militar obligatorio cuando estableció  los grupos exentos, en todo tiempo, de pertenecer a la milicia. En su criterio, no existe ninguna razón constitucional que justifique dar a este grupo de personas un trato diferente al que se prodiga a limitados físicos y sensoriales permanentes, y a los  indígenas que conserven su identidad etnocultural.  Los individuos para los cuales prestar el servicio militar obligatorio implica actuar en contra de su conciencia o de sus creencias religiosas, se enfrentan a una incompatibilidad tan manifiesta y atendible como aquella que el legislador reconoce a aquellos.

Para el magistrado VARGAS SILVA, la evidencia de que la sociedad colombiana se encuentra preparada y reclama de su orden jurídico el reconocimiento de la objeción de conciencia al servicio militar obligatorio como un derecho fundamental justiciable, se deduce palmariamente del concepto de su representante -el Procurador General de la Nación - y de la intervención de más de 400 organizaciones integrantes de  la sociedad civil, que al unísono solicitaron ante este Tribunal una decisión que posibilitara el ejercicio de esta expresión de la libertad de conciencia, mediante una sentencia condicionada que integrara a los objetores y les asignara una forma distinta de prestar el servicio militar en trabajos sociales, educativos, comunitarios. Empero, la mayoría atendió a la única entidad que pidió la exequibilidad del precepto.

No obstante el sentido del fallo, el magistrado VARGAS SILVA reconoció el avance significativo logrado, dado que la decisión mayoritaria introduce una rectificación jurisprudencial que abre la posibilidad jurídica para que, a partir de ahora los objetores de conciencia obtengan protección constitucional en los casos concretos, por la vía de la acción de tutela. El cambio de jurisprudencia quedará pues como parte de la ratio decidendi vinculante para el Juez constitucional respectivo, atendiendo las particularidades propias de cada caso.
