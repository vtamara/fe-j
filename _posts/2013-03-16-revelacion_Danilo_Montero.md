---
layout: post
title: "revelacion_Danilo_Montero"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Digno y Santo
Kari Jobe, Danilo Montero

<pre>
Digno es el Cordero Santo 
Santo Santo es Él 
Levantamos nuestra alabanza 
Al que en el trono está. 

CORO

Santo, Santo, Santo Dios Todopoderoso 
Quien fue, quien es y quien vendrá 
La creación hoy canta y damos gloria a Él 
Tú eres digno por siempre y siempre. 

Digno es el Cordero Santo 
Santo Santo es Él 
Levantamos nuestra alabanza 
Al que en el trono está. 

//CORO//

//Levantamos nuestra alabanza 
Al que en el trono esta. //

//CORO//

</pre>

* Letra basada en http://www.musica.com/letras.asp?letra=1737695
* http://www.youtube.com/watch?v=evsf6_Hm6Xw
