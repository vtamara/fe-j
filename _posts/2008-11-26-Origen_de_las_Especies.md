---
layout: post
title: "Origen_de_las_Especies"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
La versión inicial proviene del artículo de dominio público: http://fpv.gfc.edu.co/?id=edit/Origen+de+las+especies

Los siguientes son puntos problemáticos para la teoría de la evolución en lo que respecta a la diversidad de animales existentes.


## 1. La explosión cámbrica:

De acuerdo a {ec1} el periodo cámbrico abarca entre los 543 y 490 millones de años a.C.  este "periodo cámbrico marca un punto importante en la historia 
de la vida sobre la tierra; es el tiempo en el que la mayoría de los 
grandes grupos de animales aparecen primero según el registro fósil.  Este 
evento en ocasiones se denomina "Explosión Cámbrica", por el tiempo 
relativamente corto  en el cual aparece esta diversidad de formas.   En 
algún momento se pensó que las rocas cámbricas contenían los primeros y los 
más antiguos fósiles animales, pero estos ahora se han encontrado en
estratos Vendianos más antiguos." En el registro fósil de este periodo aparecen 11 phylums animales de las 20 conocidos {ec3}.  Aunque de acuerdo a la teoría de la evolución se esperaría encontrar primero animales simples, y cada vez más complejos, en este periodo aparecen no simplemente animales multicelulares, sino hasta vertebrados. También hay evidencias de extinciones masivas en las eras Vendianas y Cámbricas.

Como se describe en {ec2}, la evidencia de la diversidad de animales del periodo cámbrico se ha encontrado particularmente en 2 sitios, donde se han conservado de manera especial fósiles que muestran partes blandas de los animales del cámbrico:
* Esquiso Burger (localizado en British Columbia, Canada --de acuerdo a http://www.origins.org/articles/chien_explosionoflife.html este esquiso fue encontrado en 1909 pero sólo reportado al público en 1980.
** Muestra de restos encontrados y reconstrucciones disponible en: http://www.nmnh.si.edu/paleo/shale/index.html
** Pikaia.  Cordado. http://www.nmnh.si.edu/paleo/shale/ppikaia.htm
* Esquiso Maotianshan (localizado en China)
** The first tunicate from the Early Cambrian of South China. http://www.pnas.org/cgi/content/full/100/14/8314.  Este artículo describe el fósil cámbrico Shankoudava encontrado cerca de Kunning China.  Es un Tunicate del subfilum Urocordata de más de 543 millones de años.
** Chengjiang (localizado en Yunnan, provincia del sur de China). Hay fotos de algunos de los fósiles en: http://www.gs-rc.org/repo/repoe.htm
*** Myllokunmingia fengjiaoa.  Vertebrado.
*** Yunnanozoon lividum. Hemicordado.
*** Cathaymyrus diadexus.  Cefalocordado.
*** Xidazoon stephanus. Problematica.
** Haikouichthys
** Haikouella
* Esquiso Emu Bay (Australia)
** http://www.trilobites.info/Emu.htm

Entre las familias que se han encontrado por primera vez en este periodo están:

* Cordados
** Vertebrados
*** Myllokunmingia fengjiaoa.  http://www.gs-rc.org/repo/repoe.htm  http://www.nature.com/nature/journal/v402/n6757/fig_tab/402042a0_F2.html http://news.bbc.co.uk/1/hi/sci/tech/504776.stm
[http://news.bbc.co.uk/olmedia/505000/images/_505099_myll300.jpg]
Fosil de Myllokunmingia fengjiaoa de 2.8cm de BBC
* Artropodos:
** Xandarella http://www.trilobites.info/Emu.htm
** Primicaris http://www.trilobites.info/Emu.htm
* Moluscos 
* Anelidos 
** Emuellidae http://www.trilobites.info/Emu.htm
** Redlichiidae http://www.trilobites.info/Emu.htm
** Dorypygidae http://www.trilobites.info/Emu.htm


!Bibliografía

* {ec1} The Cambrian Period. 543 to 490 Million Years Ago. http://www.ucmp.berkeley.edu/cambrian/camb.html
* {ec2}  Los primeros vertebrados http://www.ciencias.uma.es/publicaciones/encuentros/ENCUENTROS60/vertebrados.html
* {ec3} http://en.wikipedia.org/wiki/Cambrian_Explosion

## 2. El delicado orden de las especies:

En cuanto a organización, hábitat y orden de las especies somos bastante ignorantes, por ejemplo el departamento de agricultura de los Estados Unidos continua declarando explicitamente nuestra incapacidad para detener una plaga introducida en EUA por un científico hace más de 100 años.  Se trata de una polilla natural de Europa y Asia, donde es un ser más del hábitat natural y equilibrado que el creador ha desarrollado.

A continuación traducción y adaptación de la mencionada declaración:

"Gypsy Moth In North America" http://www.fs.fed.us/ne/morgantown/4557/gmoth/

La polilla gitana,  Lymantria dispar, es una de las pestes del bosque más
desvastadoras de Norte América.  Las especies evolucionaron originalmente en  Europa y Asia ![N.T. para mi 'La especie es originaria de Europa y Asia'] y ha existido en esos lugares por miles de años. Bien en 1868 o en 1869, la polilla gitana fue introducida accidentalmente cerca a Boston, MA por E. Leopold Trouvelot.  Cerca de 10 años después de su introducción, los primeros brotes comenzaron en el vecindario de Trouvelot y en 1890 los gobiernos federal y estatal comenzaron sus intentos por erradicar la polilla gitana. Estos intentos en últimas fallaron y desde esa época, el rango de la polilla gitana ha continuado expandiéndose.  Cada año, poblaciones aisladas se descubren más allá del rango continuo de la polilla gitana pero estas poblaciones son erradicadas o desaparecen sin intervención (N.T para mi 'sin intervención humana'). Es inevitable que la polilla gitana continué expandiendo su rango en el futuro.

## 3. Errores en ubicación temporal 

Las fechas que se asignan a los fósiles (y por tanto al orden de la evolución) están en interminable cambio por parte de los evolucionistas.  Por ejemplo en {t1} se describe un hallazgo según el cual los dinosaurios tardíos convivieron con sus supuestos dinosaurios precursores pues "ahora la evidencia muestra que pudieron haber coexistido durante 15 o 20 millones de años o más"

El ''celacanto'' es una especie con fósiles datados hace 410 millones de años, que se creía había desaparecido hacía unos 80 millones de años.  Sin embargo desde 1938 se han encontrado unos pocos especímenes vivos {t3}.

[http://www.dinofish.com/cimages/TanzCoelie.jpg]
Foto de www.dinofish.com/

Justamente los métodos para datar fosiles y la tierra han sido debatidos por los creacionistas de tierra joven ({t2}).

Es típico que los artículos y libros evolucionistas afirmen con vehemencia la veracidad de sus afirmaciones, pero también es típico encontrar posteriormente otros artículos evolucionistas que refutan los primeros, y que a su vez son contradichos por investigaciones posteriores.   Un análisis de la falacia del lenguaje y los problemas de diversos artículos de biología muy recientes son publicados casi a diario por un humilde profesor de biología en {t4}

!Bibliografía

* {t1} http://www.lavanguardia.es/lv24h/20070719/53377399565.html
* {t2} http://www.answersingenesis.org
* {t3} http://en.wikipedia.org/wiki/Coelacanth
* {t4} http://darwins-god.blogspot.com/


## 4. Líneas evolutivas problemáticas

* Aves:  No hay consenso. Huxley propuso que procedían de los dinosaurios (a raíz de fósiles de Archaeopteryx que vivieron hace 147 millones de años), "pero hoy se considera que fue un camino evolutivo sin salida y sus descendientes no dieron origen a las aves modernas" {e1}. El intento por mostrar que las aves evolucionaron de los dinosaurios se ha extendido incluso hasta el 2005, así como las refutaciones de los mismos evolucionistas {e6}. Feduccia ha propuesto (desde 1995?) un origen "explosivo" y rápido de las aves al comienzo del cenozoico (posterior al cretaceo) sin especificar con claridad ancestros evolutivos, su teoría se ha denominado El 'Big-Ban' Neornithino {e4}.  Otros insisten en que aparecieron en el Cretaceo y sobrevivieron a la extinción de los dinosaurios {e5}.
* Cetaceos:  Ha habido gran variedad de explicaciones sobre su origen {e7}.
* Seres humanos como se trata más adelante

! Bibliografía

** {e1} Dinosaurs mingled with ducks, ancient kin http://www.msnbc.msn.com/id/6866337/from/RL.4/
** {e4} http://wiki.cotch.net/index.php/The_Neornithine_'Big_Bang'
** {e5} http://digimorph.org/specimens/vegavis_iaai/
** {e6} http://www.sciencedaily.com/releases/2005/10/051010085411.htm  http://colombia.indymedia.org/news/2006/10/50474.php
** {e7} http://www.trueorigin.org/whales.asp


## 5. Origen de la forma y de la información que dió lugar a las especies

La forma o estructura de los seres vivos no está codificada en el ADN sino
en la forma misma del ADN.
¿De donde salió esa forma? ¿Qué información permitió que se diera? {f1}

! Bibliografía

* {f1} Intelligent Design: The Origin of Biological Information and the Higher Taxonomic Categories.  Stephen C. Meyer. http://www.discovery.org/scripts/viewDB/index.php?command=view&id=2177&program=CSC%20-%20Scientific%20Research%20and%20Scholarship%20-%20Science



## 6. Homo sapiens sapiens

* Homo Erectus no es descendiente de Homo Habilis como se creía hasta 2007. http://www.nature.com/nature/journal/v448/n7154/full/nature05986.html
 Implications of new early Homo fossils from Ileret, east of Lake Turkana, Kenya. F. Spoor1, M. G. Leakey, P. N. Gathogo, F. H. Brown, S. C. Antón, I. !McDougall, C. Kiarie, F. K. Manthi & L. N. Leakey. Resumen: Los sitios en Africa orienta han dado luz a el inicio de una evolución del genero Homo.  Las especies hominidas tempranas mejor conocidas H. habilis y H. erectus, se han interpretado comúnmente como segmentos sucesivos de un mismo linaje evolutivo anagenético. Este caso fue fortalecido por el descubrimiento de un pequeño y temprano cráneo hominido del Pleistoceno de Dmnisi en Georgia que aparentemente provee evidencia de continuidad morfológica entre las dos taxas.  Aquí describimos dos nuevos fosiles craneales de la formación Koobi Fora al oriente del lago Turkana en Keny, que dan significaod a la relación entre las especies tempranas de Homo. Un maxilar asignado a H. Habilis demuestra con robustez que esta especie sobrevivió más tarde de lo que se había reconocido previamente, haciendo improbable una relación anagenética con H. erectus.  El descubrimiento de una caja craneal (calvarium) particularmente pequeña de H. erectus indica que este taxón se traslapó en tamaño con H. habilis, y puede mostrar un dimorfismo sexual marcado.  El nuevo fósil confirma las diferencias de H. habilis y H. erectus, independientemente del tamaño craneal, y sugiere que estas 2 taxas tempranas vivían sympatrically ![N.T no está enhttp://www.websters-online-dictionary.org/] en la misma cuenca de un lago por casi medio millón de años.
* Neanderthales no están en familia evolutiva de Homo Sapiens. Ni como especie morfológica http://www.publico.es/ciencias/077554/neandertales/homo/sapiens/solo/primos ni como especie biológica http://www.signonsandiego.com/uniontrib/20050223/news_1c23neander.html
* Lucy desmontada de la línea ancestral de los humanos. http://www.arn.org/blogs/index.php/literature/2007/0424/lucy_demoted_from_the_human_ancestral_li  Gorilla-like anatomy on Australopithecus afarensis mandibles suggests Au. afarensis link to robust australopiths Yoel Rak, Avishag Ginzburg, and Eli Geffen. Proceedings of the National Academy of Sciences USA, April 17 2007, 104: 6568-6572, doi 10.1073/pnas.0606454104

----

!Enlaces relacionados:

* http://www.prodiversitas.bioetica.org/prensa48.htm
* http://www.menteabierta.org/html/articulos/ar_probldarwin1.htm
* http://www.menteabierta.org/html/articulos/ar_probldarwin2.htm
* http://www.menteabierta.org/html/articulos/ar_probldarwin3.htm
* http://www.menteabierta.org/html/articulos/ar_probldarwin4.htm
* http://www.godandscience.org/evolution/evol1995.html
