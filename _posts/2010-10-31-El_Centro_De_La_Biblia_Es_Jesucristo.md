---
layout: post
title: "El Centro de la biblia es Jesucristo (2)"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

## 1. Introducción

En este mes de la Biblia, memoricemos unos versículos:

Mateo 7:24-27 

<pre>
24 Cualquiera, pues, que me oye estas palabras, y las hace, 
le compararé a un hombre prudente, que edificó su casa sobre la roca. 

25 Descendió lluvia, y vinieron ríos, y soplaron vientos, 
y golpearon contra aquella casa; 
y no cayó, porque estaba fundada sobre la roca. 

26 Pero cualquiera que me oye estas palabras y no las hace, 
le compararé a un hombre insensato, que edificó su casa sobre la arena; 

27 y descendió lluvia, y vinieron ríos, y soplaron vientos, 
y dieron con ímpetu contra aquella casa; 
y cayó, y fue grande su ruina.
</pre>


## 2. ¿Por qué memorizar la Biblia?

Además de servirnos de fundamento como indicaba la parábola de la introducción:

1. Es mandamiento, como enseña Deuteronomio 6:4-9. "Oye, Israel: Jehová nuestro Dios, Jehová uno es. Y amarás a Jehová tu Dios de todo tu corazón, y de toda tu alma, y con todas tus fuerzas. Y estas palabras que yo te mando hoy, estarán sobre tu corazón; y las repetirás a tus hijos, y hablarás de ellas estando en tu casa, y andando por el camino, y al acostarte, y cuando te levantes. Y las atarás como una señal en tu mano, y estarán como frontales entre tus ojos; y las escribirás en los postes de tu casa, y en tus puertas."
2. Trae muchos beneficios como enseña el Salmo 119 (el más largo): limpiar camino (9 ¿Con qué limpiará el joven su camino?     Con guardar tu palabra.), no pecar contra Dios (11 En mi corazón he guardado tus dichos,  Para no pecar contra ti), conocer maravillas (27 Hazme entender el camino de tus mandamientos, Para que medite en tus maravillas), es linterna y lumbrera en nuestro camino (105 Lámpara es a mis pies tu palabra, Y lumbrera a mi camino. ), vivifica (107 Afligido estoy en gran manera; Vivifícame, oh Jehová, conforme a tu palabra), nos evita malos caminos (101 De todo mal camino contuve mis pies, Para guardar tu palabra), nos evita humillaciones (67 Antes que fuera yo humillado, descarriado andaba; Mas ahora guardo tu palabra).  
3. Hace que todo nos prospere y salga bien como dicen el Salmo 1:1-3 "Bienaventurado el varón que no anduvo en consejo de malos, ni estuvo en camino de pecadores, ni en silla de escarnecedores se ha sentado, sino que en la ley de Jehová está su delicia y en su ley medita de día y de noche; será como árbol plantado junto a corrientes de agua que da su fruto en su tiempo y su hoja no cae, y todo lo que hace prosperará" y Josue 1:8 "Nunca se apartará de tu boca este libro de la ley, sino que de día y de noche meditarás en él, para que guardes y hagas conforme a todo lo que en él está escrito; porque entonces harás prosperar tu camino, y todo te saldrá bien." 
4. Nos alarga vida en paz como dice Proverbios 3:1-2 "Hijo mío, no te olvides de mi ley, Y tu corazón guarde mis mandamientos; Porque largura de días y años de vida Y paz te aumentarán."
5. Nos revela pensamientos e intenciones del corazón como enseña Hebreros 4:12. "Porque la palabra de Dios es viva y eficaz, y más cortante que toda espada de dos filos; y penetra hasta partir el alma y el espíritu, las coyunturas y los tuétanos, y discierne los pensamientos y las intenciones del corazón."
6. Es nuestro alimento espiritual como nos enseña Jesús mismo en Mateo 4:4. "Él respondió y dijo: Escrito está: No sólo de pan vivirá el hombre, sino de toda palabra que sale de la boca de Dios."
7. Podemos estar confiados en que estamos memorizando algo bueno y útil pues 2 Timoteo 3:16-17  dice "Toda la Escritura es inspirada por Dios, y útil para enseñar, para redargüir, para corregir, para instruir en justicia,  a fin de que el hombre de Dios sea perfecto, enteramente preparado para toda buena obra."

Según {7} un buen método para aprenderla es en familia, pues aunque es difícil memorizar, los niños disfrutan repetir lo que ya memorizaron, y es hermoso recitar en familia lo que se aprende, la idea no es generar fuente de tensión, sino proponer como ejercicio familiar que los niños ayuden a los adultos a memorizar el texto seleccionado. Ellos por imitación y su deseo de "meter la cucharada" harán lo mismo.  Entonces en nuestro devocional en familia diario búsquemos repasar lo que ya nos hemos aprendido para tenerlo fresco y memorizar nuevas porciones.


## 3. Pero el énfasis es Jesucristo

Recordemos que el propósito final de la Biblia es nuestra salvación como lo describe, entre otros, Timoteo 3:15  "y que desde la niñez has sabido las Sagradas Escrituras, las cuales te pueden hacer sabio para la salvación por la fe que es en Cristo Jesús."

Por la gracia de Dios esa salvación es por fe en Jesús, no por obras, ni por la ley, pero el mismo Señor nos llama a obedecerle en Juan 14:15-16 "15 Si me amáis, guardad mis mandamientos.  16 Y yo rogaré al Padre, y os dará otro Consolador, para que esté con vosotros para siempre," y ¿cómo vamos a obedecer sus mandamientos en todo momento, si no los conocemos de memoria, si no los hemos guardado en el corazón poniéndolos en práctica y permitiendo que el Espíritu Santo nos los recuerde cuando los necesitamos?

La importancia de esos mandamientos nos la reitera Juan 14:21 y Juan 14:23-24 "21 El que tiene mis mandamientos, y los guarda, ése es el que me ama; y el que me ama, será amado por mi Padre, y yo le amaré, y me manifestaré a él. ... 23 Respondió Jesús y le dijo: El que me ama, mi palabra guardará; y mi Padre le amará, y vendremos a él, y haremos morada con él. El que no me ama, no guarda mis palabras; y la palabra que habéis oído no es mía, sino del Padre que me envió. 

En últimas la __Palabra de Dios__ es Jesucristo mismo, pues Juan 1:1 nos dice que "En el principio era el Verbo, y el Verbo era con Dios, y el Verbo era Dios,"  siendo Verbo traducción de la palabra griega  &#955;ó&#947;&#959;&#962; que también puede traducirse como "palabra" (ver {4}) ---por cierto los libros originales del nuevo testamento se escribieron en griego entre el 50 y 100dC, mientras que los originales del antiguo testamento se escribieron en hebreo entre el 1400 y el 400aC aproximadamente.   

Las palabras que empleamos los seres humanos para comunicarnos son limitadas, no logran describir ese &#955;ó&#947;&#959;&#962; que es Jesucristo, pero nos dan ideas para aproximarnos con la interpretación correcta.  La Biblia fue escrita por humanos, inspirados por Dios, y gracias a Él, con su Santo Espíritu podemos entender más cuando la leemos.  Sin embargo la Biblia no debe ser objeto de adoración, y su interpretación requiere del Espíritu Santo.  En la teología Menonita hacemos énfasis también  en el discernimiento comunitario para entenderla y se enfatiza que la interpretación requiere mirarla con Jesús como "lupa."

Alimentemonos entonces de ese &#955;ó&#947;&#959;&#962; pues en Juan 6:35 "Jesús les dijo: Yo soy el pan de vida; el que a mí viene, nunca tendrá hambre; y el que en mí cree, no tendrá sed jamás. 


## 4. Conclusión

Por Jesucristo podemos ser salvos como dice Pablo en Colosenses 1:15-20 "El es la imagen del Dios invisible, el primogénito de toda creación.  Porque en él fueron creadas todas las cosas, las que hay en los cielos y las que hay en la tierra, visibles e invisibles; sean tronos, sean dominios, sean principados, sean potestades; todo fue creado por medio de él y para él.  Y él es antes de todas las cosas, y todas las cosas en él subsisten; y él es la cabeza del cuerpo que es la iglesia, él que es el principio, el primogénito de entre los muertos, para que en todo tenga la preeminencia; por cuanto agradó al Padre que en él habitase toda plenitud, y por medio de él reconciliar consigo todas las cosas, así las que están en la tierra como las que están en los cielos, haciendo la paz mediante la sangre de su cruz."

Jesucristo es la Palabra de Dios, y es Él mismo quien nos enseña, por ejemplo en oración y en lectura con amor individual y comunitaria de la parte de sus dichos y acciones consignados en el centro de la Biblia, en los evangelios, los libros del antiguo testamento son la introducción y el resto de libros del nuevo testamento son como el epilogo.  Guardemos sus enseñanzas en nuestro corazón poniéndolas en práctica y permitiendo que el Espíritu Santo nos las recuerde cuando lo necesitamos.

Jesucristo es la roca, es por Él que tiene sentido leer la Biblia, es por Él que vale la pena memorizarla, ¿ya te propusiste fundar tu casa sobre la roca?  ¿Ya lo aceptaste en tu corazón?  Si ahora quieres hacerlo te invitamos a hacer una [Oración de fe]


## Referencias

* {1} Biblia. Reina Valera. 1960
* {2}  http://www.reinmex.org/APAD/Radio%20Programs/Espanol/Edited%20transcripts%20for%20recording/Texto%20001-030%20UPAD/011%20Proverbios%203%201-4%20Parte%201%20-%20Lunes.doc
* {3} http://arodriguezvivas.wordpress.com/2010/03/23/construye-tu-casa-sobre-la-roca/
* {4} Logos (en griego &#955;ó&#947;&#959;&#962;) significa: la palabra en cuanto meditada, reflexionada o razonada,
* {5} http://www.maran-ata.net/exalt-esp/versos/versos2.htm
* {6} En mi corazón he guardado tus dichos. Pedro Stucky. 16.Oct.2005 http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-16oct2005.shtml
* {7} http://docs.google.com/viewer?a=v&q=cache:d53EQsGZqdwJ:www.contra-mundum.org/castellano/lindvall/Memorizar.pdf+Moises+y+memorizarlas+estas+palabras+y+las+pondras+en+las+puertas+de+tu+casa&hl=es&gl=co&pid=bl&srcid=ADGEESjEUzkRPgP3MI7mAwaqfZKHiAShGCnDMSZwTiryNRPqNkGq-03esrEP7KqZloFfzkZ0oo1SBIB8LWds-wa7tvPSv0Zc3av11xnfKGcURgVQaPLws8jsQAXbdEaahk3kA0AyYSQS&sig=AHIEtbRR1Pvbgh8s9PfXZWpMjY_56cN4rw
