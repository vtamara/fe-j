---
layout: post
title: 'Adoracion: En el trono'
author: vtamara
categories:
- Alabanza
image: "/assets/images/clouds-above-the-sky-3.jpg"
tags: 

---
Erick Linares
<pre>
_CORO_
En el trono
de mi corazón
Tu te has sentado
con Poder

//_CORO_//

_A1_
Y haz de mi
un adorador
Señor
vive en mi

_A2_
Haz de mi
un adorador
Señor
fluye en mi

_A1_
Vive en mi

//_CORO_//

_A1_
_A2_

Te amo
Señor, cuanto te amo
pues Tu eres
todo para mi

Te amo
Te necesito
Señor cuanto te amo
//pues Tu eres
todo para mi//
</pre>

* Pista 9 del CD Alabadle de Erick Linares
* Video: [https://www.youtube.com/watch?v=kMjgp5-DR1s]()
* Foto de dominio público de: [https://www.publicdomainpictures.net/en/view-image.php?image=50613&picture=clouds-above-the-sky-3](https://www.publicdomainpictures.net/en/view-image.php?image=50613&picture=clouds-above-the-sky-3 "https://www.publicdomainpictures.net/en/view-image.php?image=50613&picture=clouds-above-the-sky-3")