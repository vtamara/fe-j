---
layout: post
title: "Felipe_Evangelista"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
## Nombre

Ver [Felipe]

Llamado Felipe el evangelista en Hechos 21:8 seguramente para distinguirlo de [Felipe el Apostol ](Felipe Apostol).

[http://thebiblerevival.com/clipart/acts%208%20-%2029%20go%20near%20and%20join%20thyself%20to%20this%20chariot.jpg]

##Genealogía

| Parentezco | Persona | Referencia |
| 4 hijas profetizas | | Hechos 21:9 |

##Datos en la Biblia

Hechos 6:5 Escogido junto con Esteban, Procoro, Nicanor, Timón, Parmenas y Nicolás como diaconos.

Hechos 8:5-12 Predicó en Samaria, donde Dios obró milagros a través de él, y muchos se bautizaron.  Incluso un Simón que antes practicaba brujería y engañaba al pueblo empezó a creer.

Hechos 8:26-40 Por instrucción de un ángel en el camino de Jerusalén a Gaza, explicó las escrituras a un Etiope, comenzando con Isaias 53: , después lo bautizó.

Hechos 8:39-40 Fue transportado milagrosamente hasta Azoto desde donde también predicó la Buena Nueva por todas las ciudades hasta llegar a Cesarea.

Hechos 21:8-9  En Cesarea vivía con 4 hijas virgenes y profetizas. Fue visitado por Pablo y compañeros de este.


##Evidencias arqueológicas

##Referencias


* {1} Easton Bible Dictionary. 1897.  http://www.ccel.org/ccel/easton/ebd2.html  Dominio Público.
* {2}  http://en.wikipedia.org/wiki/Philip_the_Evangelist
* {3} http://www.evidenceofgod.com/addendums/Chapter%2036%20Addendum.pdf
