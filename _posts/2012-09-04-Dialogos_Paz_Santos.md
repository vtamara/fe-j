---
layout: post
title: "Dialogos_Paz_Santos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
El actual (2010-2014) presidente de colombia Juan Manuel Santos ha anunciado diálogos de paz con el grupo guerrillero FARC-EP (entre muchos ver la noticia por ejemplo {1}), saludamos la iniciativa e invitamos a la sociedad civil a hacerse garante de este proceso.  

Tanto la guerrilla desea la aceptación socila, como Santos desea la reelección.  Puede que para Santos sea una jugada política para presentar una propuesta radicalmente diferente a la de su predecesor Uribe Veléz, pero efectivamente es una excelente oportunidad para quienes amamos la paz.  Para quienes entendemos que la paz es fruto de la justicia y proviene de Dios.  Para quienes creemos que la guerra a muerte entre hermanos no trae la paz.  Para quienes hemos vivido toda una vida en medio del conflicto y anhelamos verdad, justicia y paz.

Si le falto incorporarse más en el esfuerzo de paz durante los diálogos en Caguan en 1998 (mandato de Pastrana), no perdamos esta oportunidad y como sociedad civil volquemonos a buscar garantías para una paz sin armas.     Esto no le va a gustar a Uribe, ni al ejército, ni a los belicitas, pero creo que le gustará a Dios.


##REFERENCIAS

* {1} http://noticias.univision.com/america-latina/colombia/article/2012-09-04/juan-manuel-santos-conversaciones-farc-acuerdo#axzz25YYEMkvw
