---
layout: post
title: "Dios_Retribuye"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
## 1. INTRODUCCIÓN

Hace casi un año, Dios me permitió predicar sobre Diezmos y Ofrendas, los veíamos como una forma de agradecer al Señor por crearnos, sustentarnos, pagar por nuestro pecado con sufrimiento, darnos salvación y amarnos de infinitas formas (ver {diezmos}).  Hoy retomamos el tema con "Dios Retribuye."

El Señor es quien retribuye los sacrificios que hacemos, en particular Él ve el sacrificio que hacemos al ofrendar en la iglesia y no lo dejará sin recompensa.   Traje algo que me lo recuerda, el vestido que tengo lo use en el matrimonio hace casi 8 meses y ahora nuevamente, más adelante explico porque.


## 2. TEXTO Y CONTEXTO

!2.1. Texto

Marcos 12:41-44 {RV1960}
<pre>
41 Estando Jesús sentado delante del arca de la ofrenda, miraba cómo el 
   pueblo echaba dinero en el arca; y muchos ricos echaban mucho.
42 Y vino una viuda pobre, y echó dos blancas, o sea un cuadrante.
43 Entonces llamando a sus discípulos, les dijo: De cierto os digo que
   esta viuda pobre echó más que todos los que han echado en el arca;
44 porque todos han echado de lo que les sobra; pero ésta, de su
   pobreza echó todo lo que tenía, todo su sustento.
</pre>

!2.2. Sobre Traducción

Sólo deseo resaltar que la palabra sustento, proviene de la palabra griega "&#946;&#943;&#959;&#962;" (ver {BLB}) en manuscritos como el Textus Receptus y se puede traducir como vida, de dos formas: el periodo que dura la vida o aquello que sostiene la vida (recursos, dinero y bienes).   Me impacta que al parecer la viuda estaba dando todo lo que le quedaba para vivir.  

!2.3 Entorno social

En {Monedas} hay llamativas fotos de las monedas del tiempo de Jesús (Griegas, Romanas y Judias), las monedas de este texto eran romanas, la principal moneda romana era el denario que equivalía a un jornal, el cuadranta era la 1/64 parte de un denario y una blanca o leptón era la 1/128 parte de un denario.   Si pensamos en un jornal hoy en dia digamos de $25.000, como si fuese un denario, un cuadrante sería como $390 y una blanca sería menos que $200.  Así que la viuda hoy habria hechado como dos moneditas de $200.

!2.3 Contexto Antes y Después en Escrituras

En Marcos esto ocurre en Jerusalén después de:
Cap 11
* 1  Entrada triunfal (que celebramos el domingo de Ramos durante semana Santa)
* 12 Maldición de la higuera que pueden interpretarse como maldición a las tradiciones judias sin corazón ni honra a Dios
* 15 Purificación del templo, cuando con ira santa saca a comerciantes del templo y recuerda que es casa de oración
* 20 Exhorta a tener fe y perdonar 
* 27 Sumos sacerdotes, escribas y anciando cuestionan su autoridad y él cuestiona su falta fe evidenciando con Juan Bautista.
Cap 12
* 1  Parábola de los viñadores malvados, para decirles a los líderes religiosos      que les daría la viña a otros
* 13 Tributo al Cesar, cuando es probado pues buscan inculparlo y responde con astucia y enseñando a soportar el mundo
* 18 Aclaración de Resurrección a Saduceos y a la humanidad.
* 28 Mandamiento más importante, cuando un escriba pregunta más para aprender que para cuestionar y efectivamente aprendemos todos
* 35 Jesús pregunta sobre el Mesías y David a los líderes, para explicarles que ellos tendrían que llamar Señor a quien no esperaban y con lo que completa estableciendo su autoridad doctrinal pues ya no le preguntan más
* 38 Anuncia sentencia más severa para letrados por aprovecharse con su estatus de los más débiles (viudas).

41-44 Ofrenda de la Viuda. 41

Cap 13
* 1  Anuncia destrucción del templo a sus discípulos
* 3  Anuncia como sería el comienzo del fin de esta era
* 14 Anuncia gran tribulación
* 24 Anuncia que retornaría entre nubes con poder y gloria
* 28 Parábola de la higuera respecto a interpretar lo que viene
* 32 El día y la hora sólo lo conoce el Padre

Cap 14
* 1 Sumos sacerdotes y escribas planean matar a Jesús
* 3 Unjido con pérfume en Betania, preparando su cuerpo y exponiendo a Judas
* 10 Traición de Judas
* 12 Preparación cena de pascua
* 17 Jesús revela a sus discípulos quien lo traicionaría
* 22 Cena del Señor
* 23 Oración en huerto de Getseman
* 43 Arresto de Jesús
* 53 "Juzgamiento" por parte de autoridades religiosas de Jesús y declarado  blasfemo por confirmar que es el Mesías e hijo de Dios
* 66 Negación de Pedro 

Esta parábola en Marcos se presenta durante lo que hoy llamamos Semana
Santa, posiblemente el miercoles, después que Jesús entrara en
Jerusalén, purificara el templo, dejará públicamente sin palabras a todos 
los líderes religiosos de su tiempo: sumos 
sacerdotes, ancianos, escribas, fariseos, saduceos, anunciara el cambio
en el liderazgo de la iglesia,  aclarara creencias y prácticas 
para el nuevo pacto que Él completaría el viernes con Su
crucificción y santificaría el domingo con Su resurrección.



! 2.4 Paralelos

Lucas 21:1-4
<pre>
1 Levantando los ojos, vio a los ricos que echaban sus ofrendas en el 
  arca de las ofrendas.
2 Vio también a una viuda muy pobre, que echaba allí dos blancas.
3 Y dijo: En verdad os digo, que esta viuda pobre echó más que todos.
4 Porque todos aquéllos echaron para las ofrendas de Dios de lo que 
  les sobra; mas ésta, de su pobreza echó todo el sustento que tenía.
</pre>

El contexto que predece y que sigue a este verículo en Lucas es el mismo
que el de Marcos.



! 2.5 Concordancias

Algo de la historia de esa "arca" para las ofrendas puede verse en 2 Reyes 12:4-16
<pre>
4 ... y todo el dinero que cada uno de su propia voluntad trae a la casa de Jehová,
5 recíbanlo los sacerdotes, cada uno de mano de sus familiares, y reparen los portillos del templo dondequiera que se hallen grietas.
...
9 Mas el sumo sacerdote Joiada tomó un arca e hizo en la tapa un agujero, y la puso junto al altar, a la mano derecha así que se entra en el templo de Jehová; y los sacerdotes que guardaban la puerta ponían allí todo el dinero que se traía a la casa de Jehová.
10 Y cuando veían que había mucho dinero en el arca, venía el secretario del rey y el sumo sacerdote, y contaban el dinero que hallaban en el templo de Jehová, y lo guardaban.
11 Y daban el dinero suficiente a los que hacían la obra, y a los que tenían a su cargo la casa de Jehová; y ellos lo gastaban en pagar a los carpinteros y maestros que reparaban la casa de Jehová,
12 y a los albañiles y canteros; y en comprar la madera y piedra de cantería para reparar las grietas de la casa de Jehová, y en todo lo que se gastaba en la casa para repararla.
...
16 El dinero por el pecado, y el dinero por la culpa, no se llevaba a la casa de Jehová; porque era de los sacerdotes.
</pre>

Entonces las ofrendas que traía el pueblo se invertían en el templo una parte y otra para el sostenimieto de los sacerdotes y sus familias.  En nuestro contexto menonita colombiano, para pasar de misión a iglesia en formación se requiere que la comunidad sostenga a su iglesia (tanto al templo como al pastor), personalmente ayudé a contar la ofrenda hace 8 dias, se recolectaron  un poco más de $40.000 ---aún tenemos camino largo por recorrer para sostener templo y pastora.

La otra forma como se ayudaba a sostener iglesias era con solidaridad entre iglesias como se ve en 2 Cor 8:1-15
<pre>
1 ASIMISMO, hermanos, os hacemos saber la gracia de Dios que ha sido dada á 
  las iglesias de Macedonia:  
2 Que en grande prueba de tribulación, la abundancia de su gozo y su 
  profunda pobreza abundaron en riquezas de su bondad.  
3 Pues de su grado han dado conforme á sus fuerzas, yo testifico, y aun 
  sobre sus fuerzas;  
... 
8 No hablo como quien manda, sino para poner á prueba, por la eficacia de 
  otros, la sinceridad también de la caridad vuestra.  
9 Porque ya sabéis la gracia de nuestro Señor Jesucristo, que por amor de 
  vosotros se hizo pobre, siendo rico; para que vosotros con su pobreza 
  fueseis enriquecidos.  
...
11 Ahora pues, llevad también á cabo el hecho, para que como estuvisteis 
  prontos á querer, así también lo estéis en cumplir conforme á lo que
  tenéis.
12 Porque si primero hay la voluntad pronta, será acepta por lo que tiene, 
  no por lo que no tiene.  
13 Porque no digo esto para que haya para otros desahogo, y para vosotros 
  apretura;  
14 Sino para que en este tiempo, con igualdad, vuestra abundancia supla 
  la falta de ellos, para que también la abundancia de ellos supla vuestra 
  falta, porque haya igualdad;  
15 Como está escrito: El que recogió mucho, no tuvo más; y el que poco, 
  no tuvo menos. 
</pre>


## 3. ANÁLISIS

Uno de los ejemplos más citados de fidelidad de Dios para retribuir  a quien administra bien lo que se le da fue el de George Müller quien en el siglo XIX no tenía nada para él pero en oración fundó  117 colegios y cuidó a más de 10.000 huerfanos, se calcula que el fluj monetario anual que manejaba era de más de 200'000.000 (ver {Muller} y {Pierson}).

Con pena por la comparación daré un ejemplo más personal, aunque deseo anotar que también predico para mi pues a ratos me ha faltado más disciplina con mis diezmos y ofrendas y tengo una deuda grande porque sólo empecé a diezmar y ofrendar cuando tenía casi 30 años.   Sin embargo testifico que desde que aprendí hace casi 10 años he procurado dar diezmo de todo lo que recibo para administrar y ofrenda donde quiera que voy a servicio.   El Señor siempre me ha suplido necesidades y me ha dado en abundancia.  Tengo montones de ejemplo pasando por provisión para familia, vivienda, educación, organizar familia internacional y este que traigo puesto, no para ostentar sino para agradecer a Dios.  Cuando, gracias a Dios lo compraba, yo quería usarlo en prédicas para servir con lo mejor que pudiera al Señor.   Para el matrimonio tenía la idea de alquilar un smoking, pero una vez supe de los US$160 que costaría por un día, preferí ir a averiguar algo que me sirviera además para predicas, un día en un almacén encontré saco  a mi medida y corbata, cuando fui a pagar resultaron con más del 50% de descuento, otro día salimos con mi suegra sabiendo que necesitaba pantalón negro, camisa y chaleco.  Los encontramos y mi suegra vio un defecto en el pantalón --uno que yo no logro encontrar hoy--, como no había más pantalones como este en esta talla el almacén hizo un gran descuento, de forma que todo salió por un valor muy cercano al del alquiler por un día.  Todo quedaba a mi medida, después del matrimonio examinando con más detalle lo que Dios había dado descubrí que el pantalón, el saco y el chaleco aunque comprados en tiendas diferentes y sin mirar marcas, son exactamente de la misma marca y de la misma colección (las mimas telas). Dios proveyó de manera perfecta y personalmente creo mi imperfecto intento por ser fiel con diezmos, ofrendas y servicio, Él lo retribuye de manera perfecta en su Reino y además acá.

(Recientemente en todos los gastos para el cambio de vivienda proveyéndome un proyecto extra en Mayo y Junio que me ayudó a completar. Otro que recuerdo de forma especial hace varios años cuando Sara y Milagros estaba en preescolar debía pagar unos vestidos en el colegio para una presentación si mal no recuerdo eran $200.000, no tenía dinero y llegó el día que debía pagarlos a las 3PM, pero yo estaba tranquilo y cuando estaba en el trabajo y a las 12M un compañero llegó y me dijo algo como "mire le doy esta donación porque usted me ayudó voluntariamente hospedando una página web" y me entregó $200.000  --dinero que yo nunca le había cobrado-- con eso pude cumplir sin retraso).

Volviendo a nuestro texto, resulta que se dió durante la semana más importante para la humanidad y muestra de manera muy práctica un detalle indisipensable del  nuevo pacto: como le doy a Dios, otros detalles del nuevo pacto en el que estamos y que se dan en este contexto:

* El templo de Dios es casa de oración, no es para hacer negocio, ni   enriquecer a los negociantes.
* Fundamental Fe y perdonar
* Líderes requieren más fe correcta y práctica en sus vidas
* Dios es quien da o quita el liderazgo en la iglesia, no son los hombres ni tradiciones
* El liderazgo es de más responsabilidad eterna y no puede basarse en explotar a los débiles.
* Vivir en el mundo pero dando al mundo sólo lo que es del mundo no concediendo lo que es de Dios
* Esperanza y fe en la resurección y en el regreso triunfal de Cristo
* Mandamiento más importante entre nosotros: parte 1) amar a Dios sobre todo
  con todo corazón, toda el alma, mente y fuerzas y 2) amar al prójimo
  como a mi mismo
* Declarar a Jesús como Señor (y así vivirlo, Él es Rey y yo subdito y
  por tanto le obedezco sólo a Él)
 
Es decir en esta parábola y su contexto se aclara como los miembros
de una iglesia debemos obrar y dar en la iglesia pero también como los 
líderes deben(mos) recibir y obrar. 

Como Arthur T. Pierson hacer notar en una enseñanza (ver {Pierson}) hay una ley de recompensa y una recompensa que vemos la viuda pobre es haber sido exaltada por Cristo hasta hoy y por siempre con su Eterna Palabra como atestiguan los evangelios, el sufrimiento de haberlo dado todo en esa ocasión bien valió la pena y además personalmente creo que Dios suplió sus necesidades terrenales en ese entonces, pues conmigo lo ha hecho cuando he dado ofrenda y he necesitado, así como muchas otras personas que han dado testimonio de la recompensa sobrenatural, espiritual y material de Dios.


## 4. CONCLUSIÓN Y ORACIÓN

* Señor gracias por seguir mirando hoy nuestros sacrificios, por seguir mirando como damos en el templo y no la cantidad sino el esfuerzo que hacemos para hacerlo.  Gracias por retribuir aquí y ahora y eternamente, gracias porque exhaltaste a la viuda que sacrificó tanto al dar y por eso hoy aprendemos de su ejemplo.
* Tu eres justo, ayudame a sacrificar cuando de, no a dar de lo que me sobra sino a dar de lo que tengo y necesito.
* Señor ayúdame a ser buen administrador de los recursos que provees.   Ayudame a dar y confiar que tu proveerás para mis necesidades familiares.
* Gracias por exaltarme, permitiendo pararme aquí a predicar aunque posiblemente hay personas más dignas escuchando, ayudame a servirte bien y ser responsable con todo lo que me has dado administrandolo para extender tu Reino.
* Señor que en las iglesias los líderes no querramos enriquecernos sobre los fieles ni sobre los débiles.  Que sirvamos y con oración recibamos tu provisión.   
* Señor que cuando en esta iglesia abunde que compartamos con otras que necesitan.
* Señor tu diste tu sangre por mi, por favor que yo pueda darte algo.  ¿Alguién aún no ha hecho la [Oración de fe]?.



## 5. REFERENCIAS
* {RV1960} http://www.biblegateway.com/passage/?search=mark%2012:41-44&version=RVR1960
* {2} RV1909
* {3} BLB.  http://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=G979&t=KJV
* {4} Predica "Diezmos y Ofrendas". Junio.2012. http://fe.pasosdejesus.org/?id=Diezmos+y+ofrendas
* {Pierson} http://www.blueletterbible.org/commentaries/comm_view.cfm?AuthorID=16&contentID=4602&commInfo=20&topic=The%20Fundamentals&ar=Mar_12_41 
* {Monedas} http://www.centrosanfrancisco.org.ar/noticias/MONEDAS.pdf
* {Müller} http://en.wikipedia.org/wiki/George_M%C3%BCller
