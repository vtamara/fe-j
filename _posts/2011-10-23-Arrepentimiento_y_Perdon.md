---
layout: post
title: "Arrepentimiento_y_Perdon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---


##1.	Introducción
Una pregunta ¿He hecho recientemente algo que no le agrada a Dios?   Pensemos un minuto y durante la prédica revisemos con los pasajes que leamos.

Estoy dañando mi relación con Dios cuando peco, ¿Qué puede pasar en mi vida si no restablezco mi relación con Dios?

¿Qué puedo hacer para restablecer nuestra relación con Él?

##2.	Texto

Jeremias 3:25 - 4:4

Yacemos en nuestra confusión y nuestra afrenta nos cubre; porque pecamos contra Jehová nuestro Dios, nosotros y nuestros padres desde nuestra juventud y hasta este día, y no hemos escuchado la voz de Jehová nuestro Dios. Si te volvieres, oh Israel, dice Jehová, vuélvete a mí. Y si quitares de delante de mí tus abominaciones y no anduvieres de acá para allá,  y jurares: Vive Jehová, en verdad, en juicio y en justicia, entonces las naciones serán benditas en Él y en Él se gloriarán.
Porque así dice Jehová a todo varón de Judá y de Jerusalén: Arad campo para vosotros y no sembréis entre espinos. 
Circuncidaos a Jehová, y quitad el prepucio de vuestro corazón, varones de Judá y moradores de Jerusalén; no sea que mi ira salga como fuego, y se encienda y no haya quien la apague, por la maldad de vuestras obras.


##3.	Contexto literario

¿En que contexto está este pasaje?

Si uno revisa unos versículos antes ve que el pueblo de Dios estaba adorando falsos dioses, estaba en idolatría.   Hoy si uno pone eso en contexto hoy en día cabe preguntarse si estoy poniendo mi confianza en algo más que en Dios, ¿en el dinero? ¿En capacidades? ¿En personas?

Unos versículos después lo que ocurre es que Jeremías profetiza que Israel y Judea serían invadidas, Dios le estaba dando oportunidad al pueblo de cambiar pero advirtiéndole que si no cambiaban habría una consecuencia.  Por ejemplo 4:14 dice  “Lava tu corazón de maldad, oh Jerusalén, para que seas salva. ¿Hasta cuando permitirás en medio de ti los pensamientos de iniquidad?”

##4.	Contexto sociológico

El libro de Jeremías describe con buen detalle la invasión de Babilonia a Israel y Judea, Babilonia se llevó cautiva a buena parte del pueblo.  Esta invasión ocurrió hacia el año 600 aC.

Las tribus de Jacob rechazaron a Dios y su protección y tuvieron que enfrentar las consecuencias durante esa invasión que dejó muchos muertos y posteriormente el exilio para muchos judíos.  Dios siguió amando a su pueblo y aún en medio de esa asolación guardó a siervos como a Jeremías -a quien los Babilonios dejaron libre cuando entraron a Jerusalén pues sus conciudadanos lo tenían en cárcel por sus profecías-  o a Daniel quien llegó a ocupar un importante cargo en el gobierno de Babilonia.  Incluso Jeremías profetizó que después de esa desolación Dios volvería a juntar a Israel y así ocurrió pues posteriormente hacía el año 550aC los Persas invadieron a los Babilonios y permitieron que los Judíos volvieran a Jersualén y reconstruyeran el templo y los muros.


El pueblo de Dios durante la cautividad se arrepintió y buscó al Señor, por lo menos en la Biblia vemos con claridad que así ocurrió con Daniel que soportó foso de leones y sus 3 amigos que soportaron ser lanzados a un horno con tal de no adorar falsos dioses.  Es decir ellos entendieron lo grave que era ese pecado e hicieron todo lo humanamente posible para evitarlo, por su parte Dios obró el resto, pues los cuido mientras estaban en llamas y en el foso para dejarlos ilesos.


##5.	Aplicación para nuestra vida

Dios ya completó el plan de salvación con Cristo, pues quienes creemos en Él como Señor y Salvador ya no estamos bajo la ley sino bajo la gracia, la sangre de Cristo es lo sobrenatural que Dios ha obrado en nosotros, es el que nos sacó y nos saca del foso de leones y del fuego cuando nos arrepentimos tras pecar, Cristo es quien nos limpia con su sangre para que el Padre nos vea justificados es decir sin mancha.

De nosotros el Señor quiere que dejemos los pecados y abominaciones y que declaremos que Él es justo:
Si te volvieres, oh Israel, dice Jehová, vuélvete a mí. Y si quitares de delante de mí tus abominaciones y no anduvieres de acá para allá,
y jurares: Vive Jehová, en verdad, en juicio y en justicia, entonces las naciones serán benditas en Él y en Él se gloriarán.

El Señor quiere que seamos misericordiosos y justos

Jer 8:23-24 Así dijo Jehová: No se alabe el sabio en su sabiduría, ni en su valentía se alabe el valiente, ni el rico se alabe en sus riquezas.  Más alabese en esto el que hubier de alabar: en entederme y conocerme, que yo soy Jehová, que hago misericordia, juicio y justicia en la tierra; porque estas cosas quiero, dice Jehová.

También quiere que nos consagremos a Él

Romanos 6:13-14 Ni tampoco presenteís vuestros miembros al pecado como instrumentos de iniquidad, sino presentaos vosotros mismos a Dios como  vivos de entre los muertos y vuestros miembros a Dios como instrumentos de justicia


Pidámosle al Espíritu Santo que nos muestre en que estamos fallando, cuales son las abominaciones que dañan nuestra relación con Dios, pues además de la Palabra, de Cristo nos ha dejado su santo Espíritu:

Romanos 2:12  Y nosotros no hemos recibido el espíritu del mundo, sino el Espíritu que proviene de Dios para que sepamos lo que Dios nos ha concedido.

Juan 16:7-8.  Pero yo os digo la verdad: Os conviene que yo me vaya; porque si no me fuera, el Consolador no vendría a vosotros; más si me fuere, os lo enviaré.  Y cuando Él venga, convencerá al mundo de pecado, de justicia y de juicio.

Una vez entienda las abominaciones que estoy haciendo, debo arrepentirme y confesar mi pecado ante Dios.   Es que como dice Juan 8:32 “Y conocereís la verdad y la verdad os hará libres” debemos reconocer nuestro pecado para ser libres. 

Gracias al Señor también hay situaciones en las que confesarlo a otra persona es necesario, claro no es a cualquiera pues puede ser una arma en nuestra contra, pero Dios nos pondrá la persona que sabrá escuchar, dar un consejo e incluso perdonar en su nombre.  Por ejemplo el bautismo de Juan era de arrepentimiento con confesión de pecados a Juan:

Marcos 1:5   Y salían a él toda la provincia de Judea, y todos los de Jerusalén; y eran bautizados por él en el río Jordán, confesando sus pecados.
Y Santiago 5:13-16 recomienda:  ¿Está alguno entre vosotros afligido? Haga oración. ¿Está alguno alegre? Cante alabanzas.14 ¿Está alguno enfermo entre vosotros? Llame a los ancianos de la iglesia, y oren por él, ungiéndole con aceite en el nombre del Señor.   15 Y la oración de fe salvará al enfermo, y el Señor lo levantará; y si hubiere cometido pecados, le serán perdonados.   16 Confesaos vuestras ofensas unos a otros, y orad unos por otros, para que seáis sanados. La oración eficaz del justo puede mucho.

La confesión es escencialmente ante el Señor por ejemplo en la Biblia vemos casos en los que el Señor perdona con el sólo anhelo de acercarse a Él de quienes pecaron:
Lucas 19:4-9  Y corriendo delante, subió a un árbol sicómoro para verle; porque había de pasar por allí.  5 Cuando Jesús llegó a aquel lugar, mirando hacia arriba, le vio, y le dijo: Zaqueo, date prisa, desciende, porque hoy es necesario que pose yo en tu casa.  6 Entonces él descendió aprisa, y le recibió gozoso.  7 Al ver esto, todos murmuraban, diciendo que había entrado a posar con un hombre peca-dor.    8 Entonces Zaqueo, puesto en pie, dijo al Señor: He aquí, Señor, la mitad de mis bienes doy a los pobres; y si en algo he defraudado a alguno, se lo devuelvo cuadruplicado.   9 Jesús le dijo: Hoy ha venido la salvación a esta casa; por cuanto él también es hijo de Abraham.
Lo que si tenemos es que es necesaria esa confesión ante Él: 1 Juan 1:7-10    7 pero si andamos en luz, como él está en luz, tenemos comunión unos con otros, y la sangre de Jesucristo su Hijo nos limpia de todo pecado.    8 Si decimos que no tenemos pecado, nos engañamos a nosotros mismos, y la verdad no está en nosotros.    9 Si confesamos nuestros pecados, él es fiel y justo para perdonar nuestros pecados, y limpiarnos de toda maldad.    10 Si decimos que no hemos pecado, le hacemos a él mentiroso, y su palabra no está en nosotros.

En ocasiones nos desgastamos en lo que no conviene, en lo que no es eterno y dejamos abandonado lo eterno, nuestro campo en el Reino de Dios.  El Señor nos invita a enfocarnos en ese campo en su Reino:
Porque así dice Jehová a todo varón de Judá y de Jerusalén: Arad campo para vosotros y no sembréis entre espinos.
Nos invita a ablandar nuestro corazón para entender:
Circuncidaos a Jehová, y quitad el prepucio de vuestro corazón,



##6.	Oración y Conclusión

* Señor quiero una buena relación contigo, quiero confesarte y declararte ante los hombres con mi testimonio y con mi boca.
* Señor hay cosas que entorpecen nuestra relación, Señor con tu Santo Espíritu revelame cuales son.
* Señor hazme un corazón blando para reconocer esos errores que me revelas, para arrepentirme, pues se que quieres que yo sea misericordioso, justo y consagrado para tí.
* Señor ayudame a confesar esos errores, primero ante Tí, Rey, pero cuando sea el caso ponme personas a quienes pueda confesarlos, que con amor y tu guianza me aconsejen.
* Señor tu eres fiel y justo para perdonarme y prefieres un corazón contrito y humillado
* Señor Je&#347;us yo te he recibido como Señor y Salvador tu moriste en la cruz por mi, por limpiar esos pecados que he cometido, vuelveme a lavar con tu sangre preciosa y quedaré limpio, ayudame con tu Santo Espíritu a no caer hoy y cada día.

Si aún no ha confesado que Cristo es su Señor y Salvador lo invitamos a hacer una [Oración de fe].

---

Predicado en iglesia Menonita La Resurrección de San Nicolas Soacha el 23.Oct.2011
