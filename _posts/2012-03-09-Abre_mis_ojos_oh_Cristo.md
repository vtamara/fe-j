---
layout: post
title: "Abre_mis_ojos_oh_Cristo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
Abre mis ojos oh Cristo,
Abre mis ojos Señor,
Yo quiero verte,
yo quiero verte


Abre mis ojos oh Cristo
Abre mis ojos Señor
Yo quiero verte,
yo quiero verte.


CORO

Y contemplar tu majestad
y el resplandor de tu gloria
Derrama tu amor y poder
mientras cantamos Santo, Santo


Santo, Santo, Santo
Santo, Santo Santo
Santo, Santo, Santo
yo quiero verte.
</pre>

* Letra basada en: http://www.tusacordes.com/secciones/ver_tema.php?id=18327
* Canción: http://www.tusacordes.com/secciones/ver_tema.php?id=18327
