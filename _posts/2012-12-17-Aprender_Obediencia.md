---
layout: post
title: "Aprender_Obediencia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
#Andrews Bridge Christian Fellowship. Grupo de Estudio en Español
##Aprender a Obedecer
16.Dic.2012. 

##1. VERSÍCULOS

Efesios 6:1-4

Hijos, obedeced en el Señor a vuestros padres, porque esto es justo.
Honra a tu padre y a tu madre, que es el primer mandamiento con promesa; para que te vaya bien, y seas de larga vida sobre la tierra.
Y vosotros, padres, no provoquéis a ira a vuestros hijos, sino criadlos en disciplina y amonestación del Señor.

Hebreos 12:1-12

12 Por tanto, nosotros también, teniendo en derredor nuestro tan grande nube de testigos, despojémonos de todo peso y del pecado que nos asedia, y corramos con paciencia la carrera que tenemos por delante,
2 puestos los ojos en Jesús, el autor y consumador de la fe, el cual por el gozo puesto delante de él sufrió la cruz, menospreciando el oprobio, y se sentó a la diestra del trono de Dios.
3 Considerad a aquel que sufrió tal contradicción de pecadores contra sí mismo, para que vuestro ánimo no se canse hasta desmayar.
4 Porque aún no habéis resistido hasta la sangre, combatiendo contra el pecado;
5 y habéis ya olvidado la exhortación que como a hijos se os dirige, diciendo:
    Hijo mío, no menosprecies la disciplina del Señor,
    Ni desmayes cuando eres reprendido por él;
6 Porque el Señor al que ama, disciplina,
Y azota a todo el que recibe por hijo. m
7 Si soportáis la disciplina, Dios os trata como a hijos; porque ¿qué hijo es aquel a quien el padre no disciplina?
8 Pero si se os deja sin disciplina, de la cual todos han sido participantes, entonces sois bastardos, y no hijos.
9 Por otra parte, tuvimos a nuestros padres terrenales que nos disciplinaban, y los venerábamos. ¿Por qué no obedeceremos mucho mejor al Padre de los espíritus, y viviremos?
10 Y aquéllos, ciertamente por pocos días nos disciplinaban como a ellos les parecía, pero éste para lo que nos es provechoso, para que participemos de su santidad.
11 Es verdad que ninguna disciplina al presente parece ser causa de gozo, sino de tristeza; pero después da fruto apacible de justicia a los que en ella han sido ejercitados.


##2. LECTURA: Aprendiendo Obediencia: El Gran Debate

http://www.crosswalk.com/family/parenting/kids/learning-obedience-the-great-debate.html
Cheri Swalwell

##3. PREGUNTAS PARA REFLEXIONAR

# ¿Siendo yo desobediente ante Dios y pecador puedo exigir obediencia a mis hijos?
# ¿Qué hacer cuando nuestro  hijo ante una orden responde con “una larga lista de razones por las que se debe hacer después, no se debe hacer del todo, o la forma en que se podría hacer mejor.”?

##4. PUESTA EN COMÚN

# Es Dios quien nos manda como hijos a obedecer a nuestros padres terrenales independiente de sus fallas como humanos.  Efesios 6:1-4
# Pedir discernimiento a Dios por anticipado y en el momento de la diferencia. Escuchar  hijos, discernir si puede haber negociación, si Dios nos habla a través de ellos para mejorar o si deben obedecer.  


##5. ORACIÓN

* Siguenos disciplinando para que te obedezcamos mejor.
* Guianos para dar ordenes correctas a nuestr@s hij@s.
* Humildad y capacidad de escuchar a nuestr@s hij@s.
* Discernimiento respecto a lo que nos responden nuestros hijos y humildad para cambiar cuando hay algo mejor.
* Guianos para disciplinar a nuestros hijos cuando es debido.

---- 

Socializado en Andrew's Bridge Christian Fellowship el 16.Dic.2012

