---
layout: post
title: "Inscripcion_de_Ecron"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

Confirma la existencia de la ciudad mencionada en Josue 13:3 y el nombre de 2 reyes.

Según {2} es uno de los 10 hallazgos arqueológicos más importantes del siglo XX.

##REFERENCIAS

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/versions/?action=getVersionInfo&vid=60
* {2} http://biblicalstudies.info/top10/schoville.htm
