---
layout: post
title: "Tengo_hambre_de_ti"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Jesús Adrián Romero

<pre>

//Por un momento en Tu presencia ![ah-ahh] 
Por un instante de Tu amor 
Por un destello de Tu gloria 
Por un minuto nada mas 
Todo daría, no importaría 
lo que tenga que pasar 
lo que tenga que esperar... //

//Tengo hambre de Ti 
de Tu presencia 
de Tu fragancia 
de Tu poder 
Hambre que duele 
Que debilita 
Que desespera 
Por Ti//

////Tengo hambre de Ti ![Oh-Hoo]
Hambre de Ti, Señor////

</pre>

----

* Canción: http://www.youtube.com/watch?v=jex-R26T46Q
* Letra basada en: http://www.musica.com/letras.asp?letra=928409
