---
layout: post
title: "Encuesta Testimonio Vladimir Tamara"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

Mi amigo Sergio Andrés Gómez del Real (sergio.g.delreal@gmail.com) me envió
unas buenas preguntas que le respondí.  Ambos hemos permitido publicar 
aquí --en el caso de Vladimir libera su testimonio al dominio público.


### S: Para mi, dios (con mayúscula o minúscula, para mi da igual) no es más que una idea, una abstracción, no muy diferente a ideas como los  números, los conjuntos, etc. Me parece curioso tu forma de concebir esta idea, que me sugiere una postura de 'verdad absoluta' sobre su representación que, en tu caso, veo que corresponde a Jesús y el  cristianismo.               
                                                  

Considero que si hay una verdad absoluta, por ejemplo los nacimientos, las
muertes, los hechos ocurridos, libres de interpretación hacen parte de
esa verdad absoluta.

Considero que una respuesta que debe ser un si o un no absoluto es si este
universo y a nosotros nos creo Dios.

Considero que otra que debe ser un si o un no absoluto, es si Jesús es o no
Dios.

Se necesita un grado de fe para poder responder si a las dos últimas, aunque
la razón también ayuda a comprobarlas.  En mi caso además de fe y razón
me ayuda la memoria porque el Señor se me ha revelado como Dios Creador y se
me ha revelado como Jesús en experiencias concretas que están en mi
memoria.

### S: Nunca había encontrado en la red a una persona que defienda estas ideas con proyectos y temas que a primera vista no tienen mucha relación.                                                        

Considero que uno le pone lo que cree a todo lo que hace.  Puede que en el caso de otras personas no sea explicito. Por ejemplo el proyecto GNU se basa en una filosofía atea que Stallman le
imprime (en algunos casos es explicito).  Yo pertenecí a ese proyecto del 2000 al 2001, entre más me metí más fui aceptando principios y formas de ver la vida atea, que me estaban llevando al ateísmo.   En la red puede encontrar cosas que hice mientras estuve en ese proyecto
buscando vtamara@gnu.org

### S: En concreto: ¿Por qué esa defensa tan marcada sobre una representación específica de una idea tan compleja como dios? 

De 1999 a 2001 también estuve metiendome en temas de yagé con varios taitas, justamente en el 2001 en un viaje que hice de Alemania a Bogotá (pues viviamos en Kaiserslautern por un estudio que yo hacía), el Señor me permitió entender en una toma que yo me estaba alejando de
Él por acercarme más a la filosofía atea del proyecto GNU.  Me salí de ese proyecto y en otra toma decidí a buscar a Dios. Por su misericordia me permitió encontrarlo con una visión interior en la que anduve por una carrilera hasta llegar a su presencia, no lo ví, pero lo escuché en mi interior, en esa oportunidad me regaló el segundo mensajito que está en la primera columna de http://vtamara.pasosdeJesus.org: "__Somos niños en una escuelita, lo que tenemos que hacer son nuestras tareas con amor, respeto, humildad y sin excusas, para así finalmente alcanzar la paz de Dios__."   En otras 2 tomas posteriores y últimas me permitió entender que el yagé no es Dios, que es como una herramienta incontrolable que puede resultar en bien o en mal porque como que lo deja a uno en un mundo espiritual en el que uno es demasiado ignorante y donde también hay espíritus malignos y los intereses de las personas que participan.

Tras esa sálida del proyecto GNU empecé a aprender más de católicos y de evangélicos y a entrar en marchas anti-guerra desde una perspectiva de fe (empecé objeción fiscal a impuestos de guerra en Alemania saliendome del trabajo con el estado que tenía, me volvieron a dar beca). Encontré el proyecto OpenBSD que aunque no es amigable con cristian@s al menos tiene una mascota que no me resultaba ofensiva (como si la de FreeBSD, ver http://fe.pasosdejesus.org/?id=Mascota+FreeBSD ).  Algunas cosas se encuentran en la red por vtamara@informatik.uni-kl.de

### S: ¿Por qué Jesús y el cristianismo?                                             

En 2003 en oración el Señor me dijo "En Sierra Leona faltan manos", posteriormente hicimos un viaje a Sierra Leona Africa buscando una adopción, el Señor permitió una prueba muy fuerte por la cual mi entonces esposa se separó, pero en la que confirmé al Señor Jesús (en quien hasta entonces creía pero no confiaba), además de confirmarme en Él ante fuertes influencias musulmanas, brujería (es fuerte en Sierra Leona) y  un estado de salud grave, cuando ya teníamos todo perdido, sentí sus  pasos, sentí que me levanto y me desamarró (pues me tenían amarrado los estafadores que se hicieron pasar por agencia de adopción), después Fatú, una señora cristiana que apenas nos había visto en el aeropuerto pero que no nos conocía, nos reclamo ante los  estafadores y nos llevó a su casa donde mi salud mejoró y pudimos después volver a Alemania, sin dinero pero con salud, con la mamá de mis hijas separada y con más fe en sus propias fuerzas  y en mi caso con más fe en Jesús como Dios y confianza en Él.

Como decidí dejar el doctorado porque no ayudaba a la paz y decidimos volver a Colombia, en el 2004 en Enero mi presupuesto era 0 y decidí no trabajar en cosas que sirvieran a la guerra, a sembrar, a hacer trueque y un tiempo ser profesor universitario.

### S: ¿La forma ideal es tomar una representación concreta  y vivir en base a eso, o estar abierto a que dios existe independientemente de las representaciones?                                   

En mi humilde opinión lo mejor es ser sincero y buscar ser consistente (no soy perfecto, veo la perfección en Jesús).

He visto que Dios trata de forma única a cada persona, se que Él ve la búsqueda que sumerce está haciendo  y le oro para que se revele a su vida,  para que le recuerde momentos en los que ya se le hubiera revelado, para que le de nuevas experiencias con Él y para que le ayude a seguir buscando verdad y si es bueno en su caso a proclamarla.    

### S: ¿Cuánto tiempo llevas en el cristianismo y defendiendo esas ideas?            

Desde mi niñez tuve una pequeña fe en el Señor Jesús pues leí los evangelios que mis padres católicos me regalaron.  Sin embargo la confianza completa en Cristo como Dios fue desde esa experiencia en Sierra Leona a mediados de 2003.

Por la hostilidad "general" de los sistemas operativos contra el  cristianismo, inicié la distribución adJ en 2004 y ese mismo año adquirí el dominio pasosdeJesus.org --cuando hubo un mínimo de dinero para   eso.

Gracias a Dios también fui conectado con ONGs de derechos humanos (como el Banco de Datos del CINEP) y encontré la Iglesia Menonita --el Señor me la mostró desde 2003-- donde he podido fusionar el deseo de paz sin  armas con mi fe y con mi trabajo.   Además como el Señor me dió promesa de seguridad anduve y  ando  confiado en Él y sin "protección de armas" en este país donde matan, torturan y amenazan a cualquiera que se oponga a la guerra o que pida justicia.  

Creo que el Señor también permite varias iglesias para que uno encuentre donde mejor puede desarrollarse.  Oro para que le ayude a encontrar una porque la experiencia comunitaria de Dios es demasiado  linda, ojala la pueda vivir.

Fue en la Iglesia Menonita donde hice mi confesión de fe pública para recibir a Cristo en mi corazón y aceptar su salvación --que yo no entendía bien hasta entonces.

### S: ¿Fue a partir de alguna experiencia particular?

Las que le he contado, además:
* El Señor me ha seguido hablando al corazón de muchas formas (oración, en detalles que ocurren, leyendo Biblia, a través de personas, sueños, con viento de su Espíritu, fuego interior que pone su Espíritu,  quebrantamiento, Su ministración que me ha permitido ver y ejercer cuando su Espíritu tumba o quebranta personas dispuestas), a Él oro para que me siga guiando.
* Me dió promesa de seguridad en una lectura bíblica comunitaria en el 2004 y me la ha cumplido aún cuando he sido amenazado.
* En el 2004 me dio promesa de provisión en otra lectura bíblica comunitaria y cada vez me la ha estado aumentando de manera que a mis hijas y a mi no nos falta y como me he propuesto limitar mis ingresos personales (no consumir tanto para poder compartir más y que otros tenga algo), desde el 2009 se han podido contratar personas en Pasos de Jesús y desde el 2010 empezamos fondo para misión en Sierra Leona con los excedentes.  Esto no es trivial  teniendo en cuenta que me niego a trabajar en bancos, para el gobierno y en entidades que no promuevan la paz sin armas.  Justamente como hay trabajo quiero ayudar a que la gente se prepare y se vincule con ONGs donde se usa o se puede usar [SIVeL](http://sivel.sf.net), [OpenBSD adJ](http://aprendiendo.pasosdeJesus.org) y [SINCODH](https://www.sincodh.org) y por eso los cursos.  Para los cursos virtuales gratuitos recientemente he encontrado y colaborado con [P2PU](http://www.p2pu.org).
* Me ha ido preparando para servirle (estoy terminando un nivel de  teología en el seminario bíblico de mi iglesia), desde 2006 en escuela dominical con niños y en comedores comunitarios, desde 2009 inicie un ministerio de Danza Alabanza en la iglesia Menonita en San Nicolás (Soacha),   desde el 2010 con jóvenes en la iglesia menonita de Teusaquillo, desde   mediados del 2010 en dirección de servicio cada mes y medio en la iglesia Menonita La Resurrección en San Nicolas Soacha, desde finales del año pasado me ha permitido   predicar en varias iglesias (más o menos cada mes y medio en La Resurrección).   Procuro publicar las alabanzas y las prédicas en <http://fe.pasosdeJesus.org>


Por cierto la oración de fe de la que hablé antes está en:
<http://fe.pasosdeJesus.org/Oracion_de_fe>
Lo invito a  verla y si el Señor toca a su corazón ábrale la puerta, pues necesitamos su Salvación.

