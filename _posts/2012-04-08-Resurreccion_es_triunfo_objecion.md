---
layout: post
title: "Resurreccion_es_triunfo_objecion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!1. Lecturas (6')

* Lectura de artículo de actualidad {2}
* Lectura de pasaje de la resurrección. Juan 20
<pre>
6 Luego llegó Simón Pedro tras él,, y entró en el sepulcro, y vio los
  lienzos puestos allí.
7 y el sudario, que había estado sobre la cabeza de Jesús, no puesto con 
  los lienzos, sino enrollado en un lugar aparte.
8 Entonces entró también el otro discípulo que había venido primero al
  sepulcro y vio y creyó.
9 Porque aun no había entendido la escritura que erá necesario que Él
  resucitase de entre los muertos.
</pre>


!2. Opiniones y puesta en común (6')

!3. Reflexión y relación con conmemoración de la resurrección de nuestro Rey (6').

Primero Dios es Dios de vida, no de muerte. Mr 12:27a
<pre>
Dios no es Dios de muertos, sino Dios de vivos...
</pre>

Es decir no aprueba que nos estemos matando unos a otros.  De hecho desde el antiguo testamento lo dejó claro, en el mandamiento de no matar  que confirmó en el nuevo testamento y que quedó del todo abolido cuando nos ordena a amar a Dios sobre todo y al prójimo como a nosotros.  Mr 12:29-31.

Y como se encadena esto con resurrección, en el triunfo de la vida, veamos el contexto en la respuesta que Jesús dio a quienes  no creían en la resurrección Mr 12:26-27
<pre>
26 Pero respecto a que los muertos resucitan, ¿no habéis leído en el libro 
   de Moisés cómo le habló Dios en la zarza, diciendo: Yo soy el Dios de 
   Abraham, el Dios de Isaac y el Dios de Jacob?
27 Dios no es Dios de muertos, sino Dios de vivos; así que vosotros mucho 
   erráis. 
</pre>

Como se encadena con objeción de conciencia: en la necesidad de oponerse a lo que representa desobediencia a Dios.   El Señor Jesús se opuso a las normas inhumanas que los religiosos de su tiempo declaraban como religión, y se proclamo Rey oponiendose a la autoridad romana que en ese entonces se proclamaba rey.   La oposición ante esos poderosos terrenales, le causo muerte terrenal, pero la verdad y la vida triunfaron con la resurrección como había sido escrito 700 años por  Isa 53:10-12
<pre>
10 Con todo eso, Jehová quiso quebrantarlo, sujetándole a padecimiento.
   Cuando haya puesto su vida en expiación por el pecado, verá linaje, 
   vivirá por largos días, y la voluntad de Jehová será en su mano prosperada.
11 Verá el fruto de la aflicción de su alma, y quedará satisfecho; por su 
   conocimiento justificará mi siervo justo a muchos, y llevará las 
   iniquidades de ellos.
12 Por tanto, yo le daré parte con los grandes, y con los fuertes repartirá 
   despojos; por cuanto derramó su vida hasta la muerte, y fue contado con 
   los pecadores, habiendo él llevado el pecado de muchos, y orado por los 
   transgresores. 
</pre>


Hoy en esta guerra contra jóvenes y niños (ver {2}) debemos hacer lo que nuestro Rey nos ordena:No matar y por el contrario Amar.  Ciertamente objetar conciencia para oponerse podría ser costoso, pero es necesariao y quienes creemos y obedecemos al Rey tenemos promesa de victoria.


##BIBLIOGRAF?A

* {1} Reina Valera 1960. http://www.biblegateway.com/passage/?search=Exo%2024&version=RVR1960
* {2} Carlos Medina Gallego.  Una guerra contra Niños, Adolescentes y Jóvenes. http://www.camega.org/inicio/index.php?option=com_content&view=article&id=572:carlos-medina-gallego&catid=40:articuloscarlos&Itemid=72


