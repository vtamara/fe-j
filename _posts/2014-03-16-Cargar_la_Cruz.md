---
layout: post
title: "Cargar_la_Cruz"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
# 1. Introducción

Cuando la pastora Adaia me solicito compartir una palabra que nos ayudara a prepararnos para Semana Santa, yo me preguntaba como se había preparado el Señor Jesús y si podríamos imitarlo de alguna forma. Casualmente llegué a Mat. 16:24 que recientemente estudiamos con niños, niñas y adolescentes en el devocional de pureza sexual de los sábados (que hacemos antes o después del ensayo de danza-alabanza).  Se trata de la primera vez de las 3 que se describen en Mateo, cuando el Señor anunció a sus discípulos su futura muerte y resurrección (que ocurrirían durante la Pascua, es decir semana Santa).  Él estaba preparando a sus discípulos, y creo que también se preparaba, pues al igual que nosotros ahora fue en la tierra 100% humano, mortal, fragil, vulnerable a la tentación, con capacidad humanas limitadas ---aún cuando  también es 100% Dios omnipotente plenamente revelado así en la resurrección. 

Hay tres preguntas, que le pido tener en mente: 
* ¿Cúal es mi cruz? 
* ¿Qué debo cambiar para llevar mejor mi cruz? 
* En semana santa este año (13-20 de Abril) ¿cómo celebraré que Jesús llevó su cruz perfectamente? 
Oro para que el Señor nos hable, nos revele o confirme cual es la cruz de cada uno(a) y nos ayude a llevarla.

¿Pensemos en una cruz de madera que características tiene?

# 2. Texto

Mt 16:24-27

<pre>
21 Desde entonces comenzó Jesús a declarar a sus discípulos que le era
   necesario ir a Jerusalén y padecer mucho de los ancianos, de los
   principales sacerdotes y de los escribas; y ser muerto, y resucitar al 
   tercer día.
22 Entonces Pedro, tomándolo aparte, comenzó a reconvenirle, diciendo: 
   Señor, ten compasión de ti; en ninguna manera esto te acontezca.
23 Pero él, volviéndose, dijo a Pedro: ##Quítate de delante de mí, Satanás!;
   me eres tropiezo, porque no pones la mira en las cosas de Dios, sino en 
   las de los hombres.
24 Entonces Jesús dijo a sus discípulos: Si alguno quiere venir en pos de mí, 
   niéguese a sí mismo, y tome su cruz, y sígame.
25 Porque todo el que quiera salvar su vida, la perderá; y todo el que pierda 
   su vida por causa de mí, la hallará.
26 Porque ¿qué aprovechará al hombre, si ganare todo el mundo, y perdiere su 
   alma? ¿O qué recompensa dará el hombre por su alma?
27 Porque el Hijo del Hombre vendrá en la gloria de su Padre con sus ángeles, 
   y entonces pagará a cada uno conforme a sus obras.
28 De cierto os digo que hay algunos de los que están aquí, que no gustarán 
   la muerte, hasta que hayan visto al Hijo del Hombre viniendo en su reino.
</pre>


# 3. Contexto

## 3.1 Tiempo y espacio

Podemos examinar el contexto de esta escritura en Mateo (ver [Contexto Mateo]) ubicando geográficament versículos cercanos a nuestro texto:

| Versículo | Sitio | ¿Qué ocurrió? |
| 15:1 | Jerusalén | Ante cuestionamiento de Fariseos y Escribas explica lo que contamina al hombre. |
| 15:21| Tiro y Sidón | Sanidad a hija de mujer cananea. |
| 15:29 | Junto al mar de Galilea | Sanó a muchos. Alimentación de 4000. |
| 15:39 | Magdala junto al mar de Galilea | Fariseos y Saduceos demandan una señal. Enseña a sus discípulos sobre levadura de fariseos. |
| 16:13 | Cesarea de Filipo | Pedro lo reconoce como Mesías. Primer anuncio de muerte y resurrección. |
| 17:1 | Monte alto (Hermón?) | Transfiguración. Al volver sana joven lunático. |
| 17:22 | Galilea | Segundo anuncio de muerte y resurrección. |
| 17:24 | Capernaum | Pagó impuestos de Él y Pedro. Enseño directo a discípulos y con Parábolas. |
| 19:1 | Judea al otro lado del Jordán | Bendice niños. Enseña. |
| 20:17 | Hacía Jerusalén | Anuncia por 3ra vez su muerte. Enseña a discípulos. Da vista a ciegos. |
| 21:1 | Betfagé | Prepara entrada a Jerusalén. |
| 21:10 | Jesuralén | Entrada. Purifica templo. Maldición de la Higuera Esteril. Enseña en Parábolas . El gran mandamiento. Advenimiento del Hijo del Hombre. Parábola de Diez Vírgenes.  Complot para prenderlo. |


[http://classic.scriptures.lds.org/es/biblemaps/map11.jpg]
Mapa de http://classic.scriptures.lds.org/es/biblemaps/11

Ahora ubiquémonos mentalmente en Cesarea de Filipo, donde por lo visto el Señor dio estas palabras, allí nace el rio Jordan, aún hoy es cristalino y hermoso allí y pueden verse peces, queda junto al monte Hermón, que es bastante alto (ver {3}).

[http://www.arcauniversal.com.ar/wp-content/uploads/2012/01/biblia-cesareia-filipe-cavepan.jpg]

[http://www.arcauniversal.com.ar/wp-content/uploads/2013/04/800px-Hermonsnow_0.jpg]

Fotos de {4}



# 4. Análisis

El Señor Jesús seguramente mediante oración,  estudio juicioso de la palabra y revelación, supo con bastante anticipación los sufrimientos por los que pasaría, sabía que esas llagas que le inflingirían eran necesarios para que por ellas fuesemos sanos, sabía que ese castigo de muerte en cruz que Él iba a sufrir era el precio de nuestra paz.  Y es que en los planes eternos de Dios Padres la resurrección está después de la cruz.

El Señor Jesús supo que ese sufrimiento y muerte sería en Jerusalén, paradojicamente ciudad donde estaba el templo para adorar al Dios vivo.  Sabía que paradojicamente sería condenado a muerte por blasfemia por los lideres religiosos de su tiempo, por los que se encargaban de ministrar en el templo y los que estudiaban la Biblia como resalta Spurgeon (ver {5}).  Sabía que lo humillarían, que lo torturarían y que le harían padecer una muerte de cruz, por ejemplo en el tercer anuncio de su muerte en Mateo 20:19 dijo "y le entregarán a los gentiles para que le escarnezcan, le azoten, y le crucifiquen; mas al tercer día resucitará,"  o de acuerdo a Marcos 10:34
"y le escarnecerán, le azotarán, y escupirán en él, y le matarán; mas al tercer día resucitará."

Sabiéndolo, inició su peregrinación hacía Jerusalen.  Preparó a sus discípulos (su grupo cercano) repitiéndoles varias veces lo que ocurriría. Y con su ejemplo mostró lo que significa "Tomar la cruz diariamente" como dice el versículo paralelo Lucas 9:23
<pre>
Y decía a todos: Si alguno quiere venir en pos de mí, niéguese a sí mismo, tome su cruz cada día, y sígame.
</pre>
Creo en la consistencia de Dios, creo que Él no nos pide hacer algo que Él no haya hecho ya.  Jesús con certeza cargó su cruz diariamente, no sólo cuando llegó el día de su crucificción en el Golgota, sino desde siempre cuando en sus planes estuvo la humanidad y su salvación.   

Los versículos 22 y 23 nos ayudan a entender mejor de que se trata la cruz.  Pedro le dice que tenga compasión de Él mismo, que renuncie a ese martirio, pero Jesús reprende al diablo que habló mediante Pedro, pues lo que hablaba no tenía como objetivo las cosas de Dios. El versículo 24 se entiende en clave de VOLUNTAD DE DIOS:

| 1. Nieguese a si mismo | Renuncie a su propia voluntad, a la voluntad del mundo y del diablo |
| 2. Tome su cruz | Tome como propia la voluntad de Dios y sus consecuencias |
| 3. Sigame | Ande en la voluntad de Dios con Jesús, con Sus reglas y haciendo lo que Él hace |

Otros versículos concordantes son 1 Pedro 4:1-2:
<pre>
1 Puesto que Cristo ha padecido por nosotros en la carne, vosotros también 
   armaos del mismo pensamiento; pues quien ha padecido en la carne, terminó 
   con el pecado,
2 para no vivir el tiempo que resta en la carne, conforme a las 
  concupiscencias de los hombres, sino conforme a la voluntad de Dios.
</pre>

Entonces para responder la pregunta central ¿Cúal es mi cruz? nos pueden ayudar algunas características de la cruz de Jesús:

* Se trata de una misión que pesa y es dificil de llevar
* Se trata de un propósito sobre el que puedo decidir dia a dia tomar o no tomar.
* Se trata de un propósito que a Dios le agrade que yo haga

Además los versículos 25 y 26 dan más pistas de como es:

* Es un propósito concordante con la fe en Jesús, puede perder la vida terrenal por mi fe en Jesús haciendo ese propósito.  Seguir ese propósito con fe en Jesús me dará vida con Él.
* Es un propósito que se opone a ganar el mundo, que se opone a los valores del mundo y de Satanas, pero que permite ganar mi alma en Cristo.

El versículo 27 nos da esperanza, pues tas la culminación de llevar la pesada y dificil cruz viene la resurrección, vendrá la gloria de Dios plenamente, con su justicia. 

Creo que el versículo 28 fue cumplido por ejemplo en el discípulo Juan, quien pudo ver en visión al señor Jesucristo y dejó testimonio en el libro de Apocalipsis, de las muchas referencias al Señor Jesús y la gloria que Juan pudo vr, por ejemplo citamos Apocalipsis 19:13 

<pre>
11 Entonces vi el cielo abierto; y he aquí un caballo blanco, y el que lo 
   montaba se llamaba Fiel y Verdadero, y con justicia juzga y pelea.
12 Sus ojos eran como llama de fuego, y había en su cabeza muchas diademas; y  
   tenía un nombre escrito que ninguno conocía sino él mismo.
13 Estaba vestido de una ropa teñida en sangre; y su nombre es: EL VERBO DE 
   DIOS.
</pre>


# 5. Conclusión y Oración

El Señor hubiera podido quedarse en los lindos manantiales de Cesarea de Filipo, observando la belleza del monte Hermón.   Pero sabía que debía llevar su cruz, sabía que eran voluntad de Dios Padre que Él fuese a Jerusalén a ser humillado, torturado y asesinado en una cruz, y así día a día camino hacía Jerusalén, hasta que se cumplió el plan de salvación, del cual hoy podemos disfrutar con sólo creer en nuestro Señor y Salvador Jesucristo.

¿Cúal es mi cruz?

Se que debo negarme a mi mismo, a los deseos de mi carne, a la gloria pasajera del mundo y de Satanas,  ¿A que debo renunciar en mi vida hoy?

Celebremos a Jesús llevando su cruz, mejorando como llevamos la nuestra. Para avanzar hacía mi Jerusalén, hacía el propósito de Dios en mi vida, hay acciones concretas que debo hacer.  ¿Qué haré en un mes en semana santa este año para llevar mejor mi cruz simultanemante siguiendo mejor a Jesús?

Señor gracias por permitirnos ver Semana Santa como un símbolo de lograr el propósito que tu tienes en nuestras vidas, y la cruz como un símbolo de avanzar en ese propósito con las dificultades y dolores que implica, pero con la esperanza y fe de la resurrección postrera.

Por favor revela y confirma cual es tu voluntad en nuestras vidas, cúal es la cruz que diariamente debo decidirme a llevar. Regalame discernimiento para no confundir con los falsos propósitos que mi carne, el mundo y Satanas ofrecen.  Guiame para avanzar diariamente hacía tu propósito,  para llevar bien mi cruz.  Dame paciencia para aguantar el peso y ayudame a cargarla por favor.   Quiero seguirte Jesús, quiero llevar mi cruz contigo, obedeciendo Tus reglas Señor, comenzando por amar a Dios sobre todo y al prójimo como a mi mismo y realizando contigo acciones como las que tu hiciste: enseñando, predicando, sanando, liberando en Tu nombre. 

Señor Jesús tu has sufrido por mi salvación, llevando la cruz de forma perfecta y por tu gracia regalándomela con sólo creer en Tí, Señor que en agradecimiento yo acepte tu voluntad diariamente en mi vida, renuncie a mi voluntad y a la que se oponga a tu voluntad y te siga.


# 6. Referencias


* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/passage/?search=Mateo+16&version=RVR1960
* {2} http://www.blueletterbible.org/Bible.cfm?b=Mat&c=16&t=KJV#s=945024
* {3} https://mujercristianaylatina.wordpress.com/2009/03/21/la-revelacion-de-cesarea-de-filipo/
* {4} http://www.arcauniversal.com.ar/lugares-de-la-biblia-cesarea-de-filipo
* {5} http://www.blueletterbible.org/Comm/spurgeon_charles/sermons/2212.cfm?a=945028
