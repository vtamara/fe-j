---
layout: post
categories:
- Prédica
title: Cristo vuelve, trabajemos
author: vtamara
image: ''

---
## CRISTO VUELVE - TRABAJEMOS

### 1. Introducción

Un misterio que Dios ha revelado después de Jesús sólo a los creyentes en Él, es que Él vuelve. Repasemoslo hoy y busquemos lo que significa eso para nuestra vida.

### 2. Texto

Hechos 3:

    14 Más vosotros negasteis al Santo y al Justo, y pedisteis que se os diese un homicida,
    15 y matasteis al Autor de la vida, a quien Dios ha resucitado de los muertos, 
    de lo cual nosotros somos testigos.
    16 Y por la fe en su nombre, a éste, que vosotros veis y conocéis, le ha 
    confirmado su nombre; y la fe que es por él ha dado a éste esta completa 
    sanidad en presencia de todos vosotros.
    17 Mas ahora, hermanos, sé que por ignorancia lo habéis hecho, como también 
    vuestros gobernantes.
    18 Pero Dios ha cumplido así lo que había antes anunciado por boca de todos 
    sus profetas, que su Cristo había de padecer.
    19 Así que, arrepentíos y convertíos, para que sean borrados vuestros pecados; 
    para que vengan de la presencia del Señor tiempos de refrigerio,
    20 y él envíe a Jesucristo, que os fue antes anunciado;
    21 a quien de cierto es necesario que el cielo reciba hasta los tiempos de la
     restauración de todas las cosas, de que habló Dios por boca de sus santos 
    profetas que han sido desde tiempo antiguo.
    22 Porque Moisés dijo a los padres: El Señor vuestro Dios os levantará profeta 
    de entre vuestros hermanos, como a mí; a él oiréis en todas las cosas que os hable;
    23 y toda alma que no oiga a aquel profeta, será desarraigada del pueblo.
    24 Y todos los profetas desde Samuel en adelante, cuantos han hablado, también 
    han anunciado estos días.
    25 Vosotros sois los hijos de los profetas, y del pacto que Dios hizo con 
    nuestros padres, diciendo a Abraham: En tu simiente serán benditas todas las 
    familias de la tierra.
    26 A vosotros primeramente, Dios, habiendo levantado a su Hijo, lo envió para 
    que os bendijese, a fin de que cada uno se convierta de su maldad.
    

### 3. Contexto

En el texto de Hechos lo que precede estos versículos es la sanidad de un hombre cojo mediante Pedro y Juan. El cojo pedía dinero en una de las puertas del templo de Jerusalén, pero en lugar de dinero, Pedro y Juan en el nombre de Jesús le dieron sanidad. Después entraron al templo con el cojo dando saltos y alabando a Dios, allí la gente reconoció al cojo y Pedro habló diciendo que quien le había dado sanidad había sido el Señor Jesús a quienes el pueblo y gobernantes habían crucificado.

En el capítulo siguiente dice que tras intervenciones como está ya erán como 5000 quienes habían creído en el Señor Jesús. Juan, Pedro y el hombre cojo que había sido sanado (quien tenía más de 40 años) fueron apresados por la guardia del templo y los líderes religiosos y dejados una noche en prisión. Al día siguiente testificaron con valor frente al concilio que esa sanidad había sido en el nombre de Jesucristo. Los líderes les prohibieron seguir predicando el nombre de Jesús, pero ellos objetaron conciencia diciendo “Juzgad si es justo delante de Dios obedecer a vosotros antes que a Dios”

Según el libro de Hechos esto ocurrió algunos días después del pentecostés (que hoy en día se celebra 50 días después de la resurrección), es decir era alrededor del año 30dC y justo después de que los apóstoles habían recibido al Espíritu Santo y estaban comenzando a formar la iglesia primitiva. Cuando aún los líderes religiosos y civiles eran los que estaban en el momento de la crucifixión y estaba apenas comenzando a formarse la iglesia sólo en Jerusalén por la predicación.

### 4. Análisis

Pedro le estaba hablando a la gente del templo en Jerusalén, allí sólo iban judíos creyentes en Jehová. Sus palabras convencen para que empiecen a creer en Jesús, tienen implícito por tanto un método evangelístico del que podemos aprender, claro comenzando con la fe de Pedro y Juan y la intervención milagros de Dios en la sanidad del cojo.

Veamos en detalle cada versículo y tratemos de encontrar relación con nuestras vidas y de ver el método evangelístico.

| --- | --- | --- |
| Versículos | Yo como cristiano que he recibido a Jesús | Otros fuera de la iglesia (método evangelístico) |
| 14,15 | He pecado aún después de recibir a Jesús | Han pecado |
| 15 | Creo en resurrección | Proclamar la resurrección de Jesús |
| 16 | Dios me ha sanado, he visto sanidades de Dios | Doy testimonio de mis sanidades y las de otros |
| 17 | Reconocer que he sido ignorante | No conocen |
| 18 | e.g Isaías 53:5. Reconocer diseño y autoridad de la Biblia | Explicar diseño de la Biblia y su autoridad |
| 19 | Pilas: De mis nuevos o continuos pecados arrepentirme y pedir nueva limpieza a Jesús. Promesa de descanso. No basta decir Señor, Señor. | Enseñar arrepentimiento para que Jesús borre pecados. Promesa de descanso |
| 20 | Creer que Jesús vuelve | Enseñar que Jesús vuelve |
| 21 | Esperar en Él. | En un tiempo y vuelve a restaurar. e.g Salmo 1:5 |
| 22-23 | Oír a Jesús es obedecerle, fe sin obras es muerta (¿como salva?). Es posible ser borrado del libro de la vida (Apo. 3:5) | Autoridad de Biblía y obedecer a Jesús. Exodo 18:15, 18-19 |
| 24 | Autoridad de Biblia. | Autoridad de Biblia. Zac 9:9 |
| 25 | Somos hijos de Abraham (Gal 3:7) | Promesa Ser hijo de Dios (Juan 1:12) |
| 26 | Bendición de Jesús mientras yo me convierta de mi maldad | Bendición de Jesús mientras yo me convierta de maldad |

Tengo entonces aquí bastantes puntos para aplicar a mi vida, hoy, pues Cristo puede volver hoy mismo. Una nemotécnica es AABEE (doble A, B, doble E):

* Amar a Jesús es creerle y obedecerle (con hechos)
* Arrepentirme de mis nuevos pecados. Rogar limpieza a Jesús, ayuda para no recaer y voluntad.
* Biblia: Estudiarla, memorizarla, es autoridad.
* Espíritu Santo: dejar que me use en sanidades y como Él disponga
* Evangelizar, empezando por familia y siguiendo con entorno como Dios me indique, llamar a otros a recibir a Cristo, a bautizarse como paso de fe y a crecer como cristiano

Dependiendo del tiempo e interés hoy podemos profundizar en la B del AABEE. Podemos estudiar más sobre la Parusía o el segundo advenimiento de Cristo.

Este misterio del regreso del Señor Jesús les fue anunciado a Pedro, Juan junto con otros discípulos por el mismo Jesús antes de su muerte y después de su resurrección. Veamos los pasajes antes de la resurrección

Mateo 16

      27 Porque el Hijo del Hombre vendrá en la gloria de su Padre con sus 
      ángeles, y entonces pagará a cada uno conforme a sus obras.
    

Mateo 24

      3  Y estando él sentado en el monte de los Olivos, los discípulos se le 
      acercaron aparte, diciendo: Dinos, ¿cuándo serán estas cosas, y qué señal 
      habrá de tu venida, y del fin del siglo? 
    …
      13  Mas el que persevere hasta el fin, éste será salvo.
      14  Y será predicado este evangelio del reino en todo el mundo, para 
      testimonio a todas las naciones; y entonces vendrá el fin. 
    …
      27  Porque como el relámpago que sale del oriente y se muestra hasta el 
      occidente, así será también la venida del Hijo del Hombre.
    …
      30  Entonces aparecerá la señal del Hijo del Hombre en el cielo; y entonces 
      lamentarán todas las tribus de la tierra, y verán al Hijo del Hombre 
      viniendo sobre las nubes del cielo, con poder y gran gloria. 
    …
      42  Velad, pues, porque no sabéis a qué hora ha de venir vuestro Señor. 
    

Mateo 25

     
      31 Cuando el Hijo del Hombre venga en su gloria, y todos los santos ángeles 
      con él, entonces se sentará en su trono de gloria,
    

Marcos 8

      38 Porque el que se avergonzare de mí y de mis palabras en esta generación 
      adúltera y pecadora, el Hijo del Hombre se avergonzará también de él, 
      cuando venga en la gloria de su Padre con los santos ángeles.
    

Marcos 13

      26 Entonces verán al Hijo del Hombre, que vendrá en las nubes con gran 
      poder y gloria.
      27 Y entonces enviará sus ángeles, y juntará a sus escogidos de los cuatro 
      vientos, desde el extremo de la tierra hasta el extremo del cielo.
    

Lucas 9

      26 Porque el que se avergonzare de mí y de mis palabras, de éste se 
      avergonzará el Hijo del Hombre cuando venga en su gloria, y en la del 
      Padre, y de los santos ángeles.
    

Lucas 17

      24 Porque como el relámpago que al fulgurar resplandece desde un extremo 
      del cielo hasta el otro, así también será el Hijo del Hombre en su día.
    …
      26 Como fue en los días de Noé, así también será en los días del Hijo del 
      Hombre.
    …
      34 Os digo que en aquella noche estarán dos en una cama; el uno será 
      tomado, y el otro será dejado.
      35 Dos mujeres estarán moliendo juntas; la una será tomada, y la otra 
      dejada.
    

Lucas 18

      8 Os digo que pronto les hará justicia. Pero cuando venga el Hijo del 
      Hombre, ¿hallará fe en la tierra?
    

Lucas 21

      34 Mirad también por vosotros mismos, que vuestros corazones no se carguen 
      de glotonería y embriaguez y de los afanes de esta vida, y venga de repente 
      sobre vosotros aquel día.
      35 Porque como un lazo vendrá sobre todos los que habitan sobre la faz de 
      toda la tierra.
      36 Velad, pues, en todo tiempo orando que seáis tenidos por dignos de  
      escapar de todas estas cosas que vendrán, y de estar en pie delante del 
      Hijo del Hombre.
    

Juan 14

      2 En la casa de mi Padre muchas moradas hay; si así no fuera, yo os lo 
      hubiera dicho; voy, pues, a preparar lugar para vosotros.
      3 Y si me fuere y os preparare lugar, vendré otra vez, y os tomaré a mí 
      mismo, para que donde yo estoy, vosotros también estéis.
    

Y el pasaje donde les repite lo mismo después de su resurrección en Hechos 1:

      8 pero recibiréis poder, cuando haya venido sobre vosotros el Espíritu 
      Santo, y me seréis testigos en Jerusalén, en toda Judea, en Samaria, y 
      hasta lo último de la tierra.
      9 Y habiendo dicho estas cosas, viéndolo ellos, fue alzado, y le recibió 
      una nube que le ocultó de sus ojos.
      10 Y estando ellos con los ojos puestos en el cielo, entre tanto que él se 
      iba, he aquí se pusieron junto a ellos dos varones con vestiduras blancas,
      11 los cuales también les dijeron: Varones galileos, ¿por qué estáis 
      mirando al cielo? Este mismo Jesús, que ha sido tomado de vosotros al 
      cielo, así vendrá como le habéis visto ir al cielo.
    

En el antiguo testamento hay bastantes profecías sobre el advenimiento del Mesías y su sufrimiento, pero es difícil encontrar que se iría y volvería después una segunda vez, una que he encontrado más o menos clara es:

Zacarías 14

      3 Después saldrá Jehová y peleará con aquellas naciones, como peleó en el 
      día de la batalla. 
    

Por el contrario el nuevo testamento está repleto de esta revelación casi en todos sus libros. Lo encontré en 19 de los 27 libros. Está en 4/4 evangelios, 1/1 hechos, 9/14 cartas paulinas, 4/7 cartas no paulinas y apocalipsis entero.

Estos versículos hablan de eventos futuros, sabemos que es difícil entender nuestro presente, aún más el pasado y mucho más el futuro. Llamamos escatología al estudio de esos versículos de eventos futuros. Hay diversas clasificaciones, al menos prácticamente todas coinciden en que Cristo volverá. La manera, los eventos alrededor de ese regreso y sus tiempos varían, pero vuelve porque vuelve.

Sabemos que el evangelista Juan emplea símbolos, por ejemplo en la estructura de su evangelio hay 7 milagros y 7 veces que Jesús define “Yo soy”, posiblemente porque el 7 representa perfección. También hay símbolos en el libro de Apocalipsis, pero donde pedimos guianza al Espíritu Santo es en discernir que es símbolo y que es literal. Las diferencias en interpretación parecen depender de que elementos del libro de apocalipsis y de las otras citas escatológicas se tomen como simbólicos y cuales como literales. Una clasificación de las interpretaciones es:

* Preterismo: Todos los eventos de apocalipsis ya se cumplieron, consideran que el milenio es simbólico y que estamos en el milenio, el cual comenzó cuando Cristo venció a Satanás en el calvario. Por ejemplo las langostas que salen del abismo descritas en Apocalipsis 9:1-11 fueron hordas demoníacas que azotaron Israel durante el asedio a Jerusalén en los años 66-70 d.C.
* Historicismo: Varios eventos ya se cumplieron desde el tiempo de los profetas. Por ejemplo las langostas fueron los árabes musulmanes que recorrieron el norte de África, Oriente Medio y España en los siglos 6 y 8.
* Futurismo: En general son eventos futuros. Por ejemplo las langostas serán hordas demoníacas que se liberarán sobre la tierra al final del mundo.
* Idealismo: Prácticamente todo lo descrito en Apocalipsis es simbólico.

Comparto una línea de tiempo futurista (dispensacional), que se basa especialmente en el comentario de Cliff Truman, pidiendo a Dios que Él mediante su santo Espíritu la corrija en cada uno lo que no llegue a ser preciso:

* Tribulación menor con las señales que Cristo dio en los evangelios estando la iglesia en la tierra.
* Regreso de Cristo en las nubes con trompetas. No toca tierra.
* Resurrección de los muertos en Cristo y rapto o arrebatamiento de la iglesia. Llevados en cuerpo al cielo.
* Gran tribulación en la tierra. 7 años caracterizados por ira de Dios sobre incrédulos de la tierra y reinado del anticristo --posibilidad de conversión pero sin Espíritu Santo
* Regreso del Señor Jesús tocando tierra. Batalla de Armagedón. Vence a Satanás y lo ata por mil años.
* Mil años Cristo Reina en la tierra con iglesia resucitada, convertidos de la tribulación que queden y resucitados y aún con sobrevivientes del armagedón aún no convertidos pero de quienes se espera conversión mientras no hay demonios.
* Tras mil años Satanás soltado para tentar y hacer nuevamente batalla contra Cristo y resultar derrotado de manera definitiva y lanzado al lago de fuego con anticristo y falso profeta.
* Juicio. Para los que no han recibido a Jesús exposición de pecados y condena. Tribunal: Para los que han recibido a Jesús exposición de buenas obras y recompensa acorde.
* Reino de Dios eterno con la humanidad

A continuación transcribo los versículos del nuevo testamento donde he encontrado que Cristo vuelve de forma explícita:

Hechos 1

      10 Y estando ellos con los ojos puestos en el cielo, entre tanto que él se 
      iba, he aquí se pusieron junto a ellos dos varones con vestiduras blancas,
      11 los cuales también les dijeron: Varones galileos, ¿por qué estáis 
      mirando al cielo? Este mismo Jesús, que ha sido tomado de vosotros al 
      cielo, así vendrá como le habéis visto ir al cielo.
    

Hechos 3

      19 Así que, arrepentíos y convertíos, para que sean borrados vuestros 
      pecados; para que vengan de la presencia del Señor tiempos de refrigerio,
      20 y él envíe a Jesucristo, que os fue antes anunciado;
      21 a quien de cierto es necesario que el cielo reciba hasta los tiempos de 
      la restauración de todas las cosas, de que habló Dios por boca de sus 
      santos profetas que han sido desde tiempo antiguo.
    

1 Corintios 1

      7 de tal manera que nada os falta en ningún don, esperando la manifestación 
      de nuestro Señor Jesucristo;
      8 el cual también os confirmará hasta el fin, para que seáis irreprensibles 
      en el día de nuestro Señor Jesucristo.
    

1 Corintios 15

      52 en un momento, en un abrir y cerrar de ojos, a la final trompeta; porque 
      se tocará la trompeta, y los muertos serán resucitados incorruptibles, y 
      nosotros seremos transformados.
    

Filipenses 1

      6 estando persuadido de esto, que el que comenzó en vosotros la buena obra, 
      la perfeccionará hasta el día de Jesucristo;
    

Filipenses 4

      5 Vuestra gentileza sea conocida de todos los hombres. El Señor está cerca.
    

Colosenses 3

      4 Cuando Cristo, vuestra vida, se manifieste, entonces vosotros también 
      seréis manifestados con él en gloria.
    

1 Tesalonicenses 1

      10 y esperar de los cielos a su Hijo, al cual resucitó de los muertos, a 
      Jesús, quien nos libra de la ira venidera.
    

1 Tesalonicenses 2

      19 Porque ¿cuál es nuestra esperanza, o gozo, o corona de que me gloríe? 
      ¿No lo sois vosotros, delante de nuestro Señor Jesucristo, en su venida?
    

1 Tesalonicenses 3

      13 para que sean afirmados vuestros corazones, irreprensibles en santidad 
      delante de Dios nuestro Padre, en la venida de nuestro Señor Jesucristo con 
      todos sus santos.
    

1 Tesalonicenses 4

      17  Luego nosotros los que vivimos, los que hayamos quedado, seremos 
      arrebatados juntamente con ellos en las nubes para recibir al Señor en el   
      aire, y así estaremos siempre con el Señor.
    

1 Tesalonicenses 5

      2 Porque vosotros sabéis perfectamente que el día del Señor vendrá así como 
      ladrón en la noche;
    

2 Tesalonicenses 1

      7  y a vosotros que sois atribulados, daros reposo con nosotros, cuando se 
      manifieste el Señor Jesús desde el cielo con los ángeles de su poder, 
      8  en llama de fuego, para dar retribución a los que no conocieron a Dios, 
      ni obedecen al evangelio de nuestro Señor Jesucristo; 
    

2 Tesalonicenses 2:1-12

      1. Pero con respecto a la venida de nuestro Señor Jesucristo, y nuestra 
      reunión con él, os rogamos, hermanos,
    

1 Timoteo 6

      14 que guardes el mandamiento sin mácula ni reprensión, hasta la aparición 
      de nuestro Señor Jesucristo,
      15 la cual a su tiempo mostrará el bienaventurado y solo Soberano, Rey de 
      reyes, y Señor de señores,
    

2 Timoteo 4

      1 Te encarezco delante de Dios y del Señor Jesucristo, que juzgará a los 
      vivos y a los muertos en su manifestación y en su reino,
      ...
      8 Por lo demás, me está guardada la corona de justicia, la cual me dará el 
      Señor, juez justo, en aquel día; y no sólo a mí, sino también a todos los 
      que aman su venida.
    

Tito 2

      13 aguardando la esperanza bienaventurada y la manifestación gloriosa de 
      nuestro gran Dios y Salvador Jesucristo,
    

Hebreos 9

      28 así también Cristo fue ofrecido una sola vez para llevar los pecados de 
      muchos; y aparecerá por segunda vez, sin relación con el pecado, para 
      salvar a los que le esperan.
    

Hebreos 10

      25 no dejando de congregarnos, como algunos tienen por costumbre, sino 
      exhortándonos; y tanto más, cuanto veis que aquel día se acerca.
    

Santiago 5

      7 Por tanto, hermanos, tened paciencia hasta la venida del Señor. Mirad 
      cómo el labrador espera el precioso fruto de la tierra, aguardando con 
      paciencia hasta que reciba la lluvia temprana y la tardía.
      8 Tened también vosotros paciencia, y afirmad vuestros corazones; porque la 
      venida del Señor se acerca.
    

1 Pedro 1

      7 para que sometida a prueba vuestra fe, mucho más preciosa que el oro, el 
      cual aunque perecedero se prueba con fuego, sea hallada en alabanza, gloria 
      y honra cuando sea manifestado Jesucristo,
    

1 Pedro 4

      7 Mas el fin de todas las cosas se acerca; sed, pues, sobrios, y velad en 
      oración.
    

2 Pedro 3

      10 Pero el día del Señor vendrá como ladrón en la noche; en el cual los 
      cielos pasarán con grande estruendo, y los elementos ardiendo serán 
      deshechos, y la tierra y las obras que en ella hay serán quemadas.
    

1 Juan 2

      28 Y ahora, hijitos, permaneced en él, para que cuando se manifieste, 
      tengamos confianza, para que en su venida no nos alejemos de él 
      avergonzados.
    

Apocalipsis 1

      7 He aquí que viene con las nubes, y todo ojo le verá, y los que le 
      traspasaron; y todos los linajes de la tierra harán lamentación por él. Sí, 
      amén.
      8 Yo soy el Alfa y la Omega, principio y fin, dice el Señor, el que es y 
      que era y que ha de venir, el Todopoderoso.
    

Apocalipsis 22

      12 He aquí yo vengo pronto, y mi galardón conmigo, para recompensar a cada 
      uno según sea su obra.
    

Apocalipsis 22

      20 El que da testimonio de estas cosas dice: Ciertamente vengo en breve. 
      Amén; sí, ven, Señor Jesús.
    

### 5. Conclusiones y oración

Esperemos a cristo trabajando con AABEE :

* Amar a Jesús es creerle y obedecerle (con hechos)
* Arrepentirme de mis nuevos pecados y pedir ayuda para vencerlos con Espíritu Santo y voluntad. Rogar limpieza a Jesús
* Biblia: Estudiarla, memorizarla pues es autoridad.
* Espíritu Santo: dejar que me use en sanidades y como Él disponga
* Evangelizar, llamar a otros a recibir a Cristo, a bautizarse como paso de fe y a crecer como cristianos

Oremos por cada uno.

### 6. Referencias

* {1} Biblia Reina Valera 1960
* {2} [http://www.versiculosbiblia.org/versiculos-de-la-biblia-sobre-la-segunda-venida-de-jesus/](http://www.versiculosbiblia.org/versiculos-de-la-biblia-sobre-la-segunda-venida-de-jesus/ "http://www.versiculosbiblia.org/versiculos-de-la-biblia-sobre-la-segunda-venida-de-jesus/")
* {3} [http://www.bibleinfo.com/es/topics/segunda-venida-de-cristo](http://www.bibleinfo.com/es/topics/segunda-venida-de-cristo "http://www.bibleinfo.com/es/topics/segunda-venida-de-cristo")
* {4} [https://www.bibliatodo.com/versiculos-biblicos/pdf/jesus/la-segunda-venida-de-cristo](https://www.bibliatodo.com/versiculos-biblicos/pdf/jesus/la-segunda-venida-de-cristo "https://www.bibliatodo.com/versiculos-biblicos/pdf/jesus/la-segunda-venida-de-cristo")
* {5} [http://www.tiemposprofeticos.org/rapto-venida-jesus/](http://www.tiemposprofeticos.org/rapto-venida-jesus/ "http://www.tiemposprofeticos.org/rapto-venida-jesus/")
* {6} Comentario a apocalipsis. Cliff Truman.
* {7} Sugel Michelen. Escatología. El Reino Milenario. [https://www.youtube.com/watch?v=Krp6WrkJogk](https://www.youtube.com/watch?v=Krp6WrkJogk "https://www.youtube.com/watch?v=Krp6WrkJogk")
* {8} [https://en.wikipedia.org/wiki/Christian_eschatology](https://en.wikipedia.org/wiki/Christian_eschatology "https://en.wikipedia.org/wiki/Christian_eschatology")
* {9} Comentario Apocalipsis. Cliff Truman. [https://es.scribd.com/doc/81055752/Comentario-Apocalipsis-Cliff-Truman](https://es.scribd.com/doc/81055752/Comentario-Apocalipsis-Cliff-Truman "https://es.scribd.com/doc/81055752/Comentario-Apocalipsis-Cliff-Truman")

***

Este escrito se cede al dominio público. Vladimir Támara Patiño. 2017.

Gracias a Dios partes se pudieron compartir y consolidar con:

* Iglesia La Resurrección 2017-06-30 - San Nicolás, Soacha
* Iglesia Monte Horeb - 2017-05-07 - Rincón del Lago, Cazuca

También disponsible en [https://docs.google.com/document/d/1ggvGNB2dQgZMPqVg0D217_4HqPeN1mGnM6e3HYGsGk0/edit?usp=sharing](https://docs.google.com/document/d/1ggvGNB2dQgZMPqVg0D217_4HqPeN1mGnM6e3HYGsGk0/edit?usp=sharing "https://docs.google.com/document/d/1ggvGNB2dQgZMPqVg0D217_4HqPeN1mGnM6e3HYGsGk0/edit?usp=sharing")