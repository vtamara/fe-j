---
layout: post
title: Bruederlich vereinigung etlicher Kinder Gottes
author: vtamara
categories: []
image: "/assets/images/4556.jpg"
tags: Artículo

---
El museo de la ciudad de Schleitheim en Suiza tiene un escaneo del Acuerdo o Confesión de Schleitheim, transcripción de cada página y una traducción a aleman moderno (ver [https://www.museum-schleitheim.ch/taeufer_bekenntnis1.htm](https://www.museum-schleitheim.ch/taeufer_bekenntnis1.htm "https://www.museum-schleitheim.ch/taeufer_bekenntnis1.htm")).

Para preservarlo (como hace Wayback Machine, por ejemplo [https://web.archive.org/web/20120313130304/http://www.museum-schleitheim.ch/bekenntnis/artikel1.htm](https://www.museum-schleitheim.ch/taeufer_bekenntnis1.htm "https://www.museum-schleitheim.ch/taeufer_bekenntnis1.htm")), aquí mantenemos otra copia:

***

z![](/assets/images/4556.jpg)

Bereits 1527 (spätestens 1529) erschien die erste gedruckte Fassung der "Brüderlichen vereynigung" bei Peter Schöffer dem Jüngeren in Worms. Von diesem Erstdruck sind in der Literatur bis anhin nur 2 Exemplare beschrieben worden. Beide befinden sich im Besitz der Bayrischen Staatsbibliothek München. 1533 erschien der gleiche Text, ergänzt um ein Traktat über die Ehescheidung, bei Jacob Cammerlander in Strassburg. Eine dritte deutsche Ausgabe erschien um weitere kurze Texte angereichert in einem kleinen, handlichen Format um 1550. Von den 2 bekannten Exemplaren, beide finden sich mit der "Concordanz und Zeiger der namhafftigsten Sprüch aller Biblischen Bücher alts und news Testament" in einem Band zusammengebunden, befindet sich eines in der Mennonit Historical Library in Goshen und das zweite, hier abgebildete, im Ortsmuseum Schleitheim.

***

0 

![](/assets/images/4570.jpg)

Brüderlich vereinigung etlicher Kinder Gottes / sieben artikel betreffend. Item, ein Sendbrieff Michaels Sattlers / an ein gemein Gottes / sampt kurzem / doch wahrhafftigen anzeig / wie er seine leer zu Rottenburg am Necker / mit seinem blut bezeuget hat |

1

![](/assets/images/bekenntnis_1.jpg) 

Freud / fried / und barmherzigkeit / von unserem Vater / durch die vereinigung des blutes Christi Jesu / mit sampt den gaben des Geistes / der vom Vater gesend wird / allen Gleubigen zu sterke und trost / und bestendigkeit in aller trübsal / bis an das ende / Amen. Sey mit allen liebabern Gottes / und Kindern des Liechtes / welche zerstrewt sind / allenthalben / wo sie von Gott unserem Vater verordnet sind / wo sie versamlet seind einmütiglich / inn einem Gott unnd Vater unser aller / Genade und fried im Herzen sey mit euch allen / Amen.

Lieben in dem Herren / Brüder und Schwestern / Uns ist alwege zum ersten und fürnemsten angelegen / Ewer trost |

2 

![](/assets/images/bekenntnis_2.jpg)

trost und versicherung eweres Gewissens / welches etwan verwirret was / da mit ir nicht immer als die auslendigen von uns gesondert würden / und schier vast ausgeschlossen nach billigkeit / sondern das ihr euch widerumb wenden möchten zu den waren eingepflanzten gliedern Christi / die da gerüstet werden durch gedültigkeit / und erkennung sein selbs / und also widerumb mit uns vereinbart würden in der krafft eines Göttlichen Christlichen Geists und eifers nach Gott.

Es ist auch offenbar / mit was tausent listigkeit der Teuffel uns abgewendet hab / damit er inen das werck Gottes / welches inn uns zum theyl barmherziglich und gnediglich angehebt ist

3 

![](/assets/images/4563.jpg) 

worden / zerstöre unnd zu boden richt. Aber der trewe Hirt unser Seelen Christus / der solchs angehabé hat in uns / der wird dasselbe bis an das ende richten und leren / zu seiner ehr und unserm heil / Amen.

Lieben Brüder und Schwestern / wir / die da versamlet sind gewesen im Herrn / zu Schlaten am Randen / mit einander in stücken und artickeln / thun kunt allen liebabern Gottes / das wir vereinigt sind worden / So uns betreffen im Herre zu halten / als die gehorsamen Gottes Kinder und Söne und Töchtern / die da abgesondert sind / und sollen sein von der welt / in alweg thun und lassen / und Gott sey einig preis un lob / ohne aller Brüder widersprechen

4 

![](/assets/images/4564.jpg)

ganz wol zufrieden. In solchem haben wir gespüret die einigkeit des Vatters / und unsers gemeinen Christi / mit irem geist mit uns gewesen sein / Dann der Herr ist der Herr des friedes und und nit des zancks / wie Paulus anzeygt, Das ir aber verstanden /in was artickeln solchs geschehen sey /sollen ir mercken und versthon.

Es ist von etlichen falschen brüdern unter uns grosse ergernis ein gefürt worden / das sich etlich von dem Glauben abgewendedt haben / in dem sie vermeint haben / die freyheit des Geystes unnd Christi / sich uben unnd brauchen / solche aber haben gefehlet der wahrheit / unnd seindt ergeben worden (zu irem urtheil) der

5 

![](/assets/images/4559.jpg)

geylheyt unnd freyheyt des fleisches / und haben geachtet / der glaub und lieb mög es alles thun und leiden / und inen nichts schaden noch verdamlich sein / dieweil sie also glaubig seyen.

Merckent ir glieder Gottes in Christo Jesu / der glaub an himelischen Vater /durch Jesum Christum ist nicht also gestalt /wircket und handelt nicht solche ding / so diese falsche brüder und schwestern handelen und leren / hüten euch und seint gemant vor solchen / dann sie dienen nicht unserm Vater /sonder irem Vater dem Teuffel.

Aber ir nicht also / dann die da Christi seint / die haben ir fleisch gecreuziget mit sampt allen gelüsten und begierden / ir verstan mich wol / unn die brüder

6 

![](/assets/images/4560.jpg)

welche wir meynen / Absonderteuch von inen / denn die sie sind verkehrt. Bittend den Herren umb ire erkantnus zur busse / und uns umb bestendigkeit / den angegriffnen weg führt zu wandeln / nach der Ehr Gottes / und seines Sons Christi / Amen.

Die artickel so wir gehandelt haben/ unnd in denen wir eins worden / sein diese.

1\.Tauff. 2.Ban. 3. Brechung des Brots. Absunderung von greweln. 5. Hirten in der gemeyn. 6. Schwert. und der 7. Eyd.

Zum ersten / So mercked von dem Tauffe.

Der Tauff folg geben werden allen denen so gelernt seind die bus unnd enderung

7 

![](/assets/images/4561.jpg)

des lebens / unnd glauben in der warheit / das ire sünd durch Christum hinweggenomen seyen / un allen denen / so wöllen wandeln in der aufferstehung Jesu Christi / auff das sie mit im auffestehn mögen /und allen denen so es in solcher meynung von uns begeren und fordern / durch sich selbs. Mit dem werden ausgschlossen alle kinder tauff / des Bapsts höchste und erste grewel. Solches habt ir grund unnd zeugnus der Schrift / un brauch der Apostel / Matt. 27. Marc.16. Art.2. 8. 16. 19. Des wöllen wir uns eynfaltiglich / doch festiglich / halten und versichert seyn.

Zu andern / Seind wir vereiniget worden von dem Bann / also / Der Bann

8 

![](/assets/images/4562.jpg)

sol gebrauch werden mit allen denen / so sich dem Herrn ergeben haben / nachzuwandeln in seinen geboten / und mit allen denen / die in einem leib Christi getaufft sein worden / und sich lassen brüder oder schwester nennen / und doch etwan entschlipffen / unnd fallen in ein fal und sünd / und unwissentlich uber eilt werden. Die selben sollen vermant werden zum andernmal heymlich / und zum drittemal offentlich / vor aller gemeyn getrafft oder gebant werden nach dem befehl Christi / Matth. 18. Solches aber sol geschehen nach ordenung des Geist Gottes vor dem brotbrechen / damit wor einmütiglich und in einer liebe von eynem brotbrechen unnd essen mögen / unnd von eynem

9 

![](/assets/images/4565.jpg)

Kelch trincken.

Zum dritten / In dem Brodbrechen seind wir eynes worden und vereinbaret / Alle die ein Brodbrechen wollen / zur gedechtnus des brochnen Leibes Christi / Unnd alle die von einem tranck trincken wollen zu einer gedechtnus / des vergossenen blutes Christi / die sollen vorhin vereiniget sein in einem leibe Christi / das ist / in die Gemeine Gottes / auff welchem Christus das Haupt ist / nemlichen /durch den Tauffe / Dann wie Paulus anzeiget / so mögen wir nicht auff einmal teilhaftig sein des HERREN Tisch unnd der Teuffel Tisch / Wir mögen auch nicht auff einmal theilhfftig sein / und trincken von des HERRN Kelch / und des Teufels

10 

![](/assets/images/4566.jpg)

kelch / das ist alle die gemeinschaft haben mit den todten wercken der finsternus / die haben kein theil am liecht /also alle die dem Teuffel folgen und der welt / die haben kein theil mit denen die zu Gott aus der Welt beruffen seyn / alle die in dem argen ligen / die haben kein theil an dem guten. Also auch sol und mus seyn / welchen nicht hat die beruffung eins Gottes zu einem glauben / zu einem Tauff / zu einem Geist / zu einem Leib / mit allen kindern Gottes gemein / der mag auch nit mit ein ein Brot gemacht werden / wie denn seyn mus / wo man das Brot in der warheit / nach dem befelch Christi brechen will.

Zum vierden / seind wir vereyniget

11 

![](/assets/images/4889.jpg)

worden von der absunderung. Sol geschehen von dem bösen und dem argen / das der Teuffel in der Welt gepflanzt hat / also /allein das wir nicht gemeinschaft mit inen haben / unnd mit inen lauffen in die gemenge irer greuweln / das ist also/ Dieweil alle (die nit getretten seyn / in die gehorsam des glaubens / und die gehorsam des glaubens / und die sich nicht vereynigt haben mit Gott / as sie seinen willen thn wollen) ein grosser grewel vor Gott sein / so kann und mag anders nicht von inen wachsen oder entspringen dann grewlich ding. Nun ist je nichts anders in aller creatur /dann guts und böses / glaubig unnd unglaubig finsternus unnd liecht / Welt / und die aus der Welt sind / Tempel Gottes und die Götzen / Christus

12 

![](/assets/images/4890.jpg)

Christus und Belial / und keynes maf mit dem andern theyl haben.

Nu ist uns auch das gebot des Herrn offenbar / in welchem er uns heist abgesundert sein und werden von dem bösen / so wöl er unser Gott sein / und werden wir seine Söne und töchter sein..

Weiternvermant er uns daurmb von Babylon / und dem irdischen Egypto auszugon / das wir nicht auch theyhafftig werden irer qual / und leiden / so der Herr uber sie füren würt..

Aus dem allem sollen wir lerne das alles / was it mit unserm Gott unnd Christo vereyngt ist nichts anders sei / Dann die grewel / welche wir meiden sollen und fliehen. In dem werden vermeint alle Bepstlich unnd widerbepstliche

13 

![](/assets/images/4891.jpg)

werck / und Gottes dienste / versamelung / kirchgang / Weinheuser / Burgerschafften / und verpflichtung des unglaubens / unnd andere mehr der gleichen / die dann die welt für hoch helt / und doch stracks wider den befelch Gottes gehandelt werden / nach der mas aller ungerechtigkeiten / die in der welt ist. Von diesem allem sollen wir abgesundert werden / und kein theil mit solchen haben / dann es sein eitel grewel / die uns verhasset machen vor unserem Christo Jesu / welcher uns entledigt hat von der dienstbarkeit des Fleisches / unnd uns gschickt gemacht dem dienst Gottes durch den Geist / welchen er uns geben hat. Allso werden nu auch von uns ange-

14 

![](/assets/images/4896.jpg)

zweifelt die unchristlichen / auch teuffelischen waffen des gewalts fallen / als da seint Schwert / Harnasch /und dergleichen / und aller irer brauch für freunde / oder wider die Feind / in krafft des worts Christi / Ir söllend dem ubel nit widerstan.

Zum fünften / seind wir vereyniger worden von den Hirten in der gemein Gottes also Der Hirt in der gemein Gottes sol einer sein nach der ordnung Pauli / ganz und gar der ein gut zeugnus hab / von denen die ausser dem glauben seind. Solches ampt sol sein lesen / vermanen und leren / manen / straffen / bannen / in der gemein / und allen Brüdern und Schwestern zur besserung vorbeten / das Brot anheben zu brechen /

15 

![](/assets/images/4897.jpg)

und in allen dingen des Leibs Christi acht haben / das er gebawr unnd gebessert wird / und dem lesterer der Mund werde verstopfft.

Dieser aber sol erhalten werden / wo er mangel haben würd / von der gemein welche in erwehlt hat / damit welcher dem Evangelio dienet / von demselben auch lebe / wie der Herr verordnet hat. So aber ein Hirt etwas handlen würd / das zu straffen were / soll mit im nichts gehandelt werden on zween oder dreyen zeugen / Und so sie sünden / sollen sie vor allen gestrafft werden /damit die anderen forcht haben.

So aber dieser Hirt vertrieben / oder durch das creuz dem Herrn hingefürt würd / soll von stundan ein anderer an

16 

![](/assets/images/4892-1.jpg)

die stat verordnet werden / damit das Völcklein un heufflein Gottes nich zerstört werden.

Zum sechsten sein wir vereiniget worden von dem Schwert /also / Das Schwert ist ein Gottes ordnung / ausserhalb der volkomenheit Christi / welches den Bösen straffet unnd tödtet / und den Guten schützet und schirmet. Im Gesatz wird das Schwert geordnet / uber die Bösen zur Straffe / und zum todt / und dasselbige zubrauchen / sind geordnet die weltlichen Oberkeiten.

In der volkomenheit Christi aber / wird der Bann gebraucht / allein zu einer manung und ausschliessung des der gesündet hat on todt des fleisches /

17 

![](/assets/images/4893.jpg)

allein durch die manung und den befelh / nicht mehr zu sündigen. Nu wird gefragt von vielen / die nicht erkennen den willen Christi / die nicht erkennen den willen Christi möge oder sol das Schwert brauchen / gegen dem bösen / umb des guten Schutz und schirm willen / oder umb der liebe willen:

Antwort ist offenbart einmütiglich also /

Christus lert und befilcht uns / ds wir von im lernen sollen / dann er sey milt / und von herzen demütig / und so werden wir rhu finden unser Selen.. Nun saget Christus zum Heidnischen Weiblin / das im Ehebruch begriffen worden was / nicht das man es versteinigen solt / nach dem Gesatze

18 

![](/assets/images/4894.jpg)

seines Vaters (und er doch saget / Wie mir der Vater befohlen hat / also thu ich ) sonder der barmherzigkeit und verzeihung / und manung / nicht mehr zu sünden / und sprich / gang hin / unnd sünde nit mehr / Solches sollen wir uns genzlich auch halten / nach der Regel des Bannes.

Zum andern wirt gefragt / des Schwerts halben / Ob ein Christ sol urtheil sprechen in weltlichen zanck und spen / so die ungleubigen mit einander haben: Ist das die eynige antwort Christus hat nicht wöllen entscheiden oder urtheilen zwischen Brüder des erbtheils halben / sonder hat sich desselben gewidert / also sollen wir ihm auch thun.

Zum dritten / wird gefragt des Schwerts

19 

![](/assets/images/4895.jpg)

halben / Soll das ein Oberkeit sein / so einer dazu erwehlt wirt: Dem wirt also geantwort / Christus hat sollen gemacht werden zu einem König / und er ist geflohen / und hat nicht angesehen die ordnung seines Vaters / also sollen wir im auch thun / und im nachlauffen / so werden wir nicht in der finsternus wandeln/ dann er sagt selbs / welcher / nach mir kommen wöl / der verleugne sich selbs / und neme sein creuz auff sich und folge mir nach. Auch verbeut er selbst / den gewalt des schwerts und sagt / Die weltlichen Fürsten herrschen / etc. ir aber nit also. Weiter sagt Paulus / Welche Gott versehen hat / die hat es auch verordnet / das sie gleichherrig sein sollen dem ebenbild seins Sons / etc. Auch

20 

![](/assets/images/4898.jpg)

sagt Petrus / Christus hat gelitten / nit geherschet / und hat uns ein ebenbild gelassen/ das ir solt nachfolgen seinen fusstapffen.

Zu letzt wird gemerkt /das es dem Christen nicht mag zimen ein Oberkeit zu sein in den stücken / Der Obrer regiment ist nach dem fleisch / so ist der Christ nach dem Geist / ir heuser und wonung ist leiblich / in dieser welt / so ist der Christen im himel / ir burgerschafft ist in dieser welt / so ist der Christen burgerschaff im himmel / ihres streits und kriegs waffen sein fleischlich / unnd allein wider das Fleisch / der Christen waffen aber seind geistlich / wider die befestigunge des Teuffels / Die weltlichen werden gewapnet mit stahel und eisen /

21 

![](/assets/images/5120.jpg)

aber die Christen send gewapnet mit dem harnisch Gottes / mit warheit / gerechtigkeit / fried / glauben / heil / und mit dem wort Gottes. In summa / Was Chistus unser heupt auff uns gesinnet ist / das alles sollen die glieder des leibe Christi durch in gesinnet sein / damit keine spaltung in dem leib sey / dardurch er zerstöret werde / denn ein igliches reich das in im selbs zerteilt ist / wird zerstöret werden. So nun Chrisus also ist / wie von im geschrieben stehet / so müssen die glieder auch also sein / damit sein leib ganz und einig bleib / zu seiner selbes besserung und erbawung.

Zum siebenden / seind wir vereiniget worden von dem eyd /also / Der eyd ist ein befestigung unter denen / die da

22 

![](/assets/images/5121.jpg)

zancken / oder verheissen / und ist im Gesatz geheissen worden / das er sol geschehen / bey dem namen Gottes / allein war hafftig / und nit falsch. Christus der die vollkommenheit des Gesatzes leret / verbeut den seinen alles schweren / weder reche noch falsch / weder beym Himel / noch bey dem Erdreich / noch bey Jerusalem / noch bey unserm Haupt und das umb der ursach willen / wie er bald hernacher spricht / Dann ir mögen nicht ein haar weis oder schwarz machen. Sehend zu / darumb ist alles schweren verbotten / dan wir mögen nicht / das in dem schwerenverheissen wirt / erstatten / dieweil wir das aller geringst an uns / nit mögen endern. Nun sind etlich / die dem eynfeltigen

23 

![](/assets/images/5122.jpg)

gebot Gottes nicht glauben geben / sonder sie sagen unnd fragen also / Ey nun hat Gott dem Abraham geschworen / durch sich selbs / dieweil er Gott was / (da er im verhies / das er im wol wölt / und wölt sein Gott sein / so er seine gebot hielt ) warumb solt ich dann nicht auch schweren / so ich einem etwas verhies: Antwort / Höre was die Schrifft sagt / Gott da er wolt den erben der verheissung uberschwencklich beweisen / das sein rath nicht wancket / hat er einen Eyde darzwischen gelegt / auff das wir durch zwey unwanckliche ding ( dardurch es unmöglich ist / das Gott liege) ein starken trost haben. Merck den Verstand dieser geschrifft / Gott hat gewalt zu thun das er dir verbeut / dann es ist

24 

![](/assets/images/5127.jpg)

im alles müglich. Gott hat dem Abraham geschworen einen eid / sagt die Schrifft / darumb das er beweise / das sein rhat nicht wancket / das ist / Es mocht im niemand seinem willen understahn und hindern / und darumb mocht er den eid halten / Wir aber mögen nichts / wie droben von Christo gesaget ist / das wie den halten oder leisten / darumb sollen wir nichts schweren.

Nu sagen etlich weiter also / Es sey nit bey Gott verbotten zu schweren / in dem newen Testament / und doch im alten gebotten / Sonder sey allein bey den Himel / Erdrich / Jerusalem / und bey unserm Haupt verbotten zu schweren. Antwort / höre die Schrifft / Wer da schweret bey dem Himel / der schweret

25 

![](/assets/images/5128.jpg)

beydem stul Gottes / und bey dem der darauff sizet. Merck / schweren bey dem Himel / ist verbotten / der nu ein stul Gottes ist / wie viel mehr ist verbotten bey Gott selbs? Ir narren und blinden / was ist grösser / der Stul oder der darauff sizet? Noch sagen etlich / wann nu das unrecht ist / wann man Gott zu der warheit braucht / so habe Apostel Petrus und Paulus auch geschworen. Antwort / Petrus und Paulus zeuge allein das / welches von Gott dem Abraham durch den eid verheissen was / und sie selbs verheissen nichts / als die exempel klar anzeigen. Zeugen aber und schweren ist zweierley / Dann so man schweret / so verheisst man erst künfftig ding / wie

26 

![](/assets/images/5123.jpg)

dem Abrahe Christus verheissen ist worden / welchen wir lange zeit hernach entpfangen haben. So man aber zeuget / so zeyget man an das gegenwertig / ob es gut oder bös sey / wie der Simeon von Christo zu Maria sprach unnd zeuget / Sihe / dieser wirt gesezt zu einem fall und aufferstehung vieler in Israel / und zu einem zeichen dem widersprochen wirt.

Dergleichen hat uns Christus auch gelert / da er sagt / Ewer rede sol sein ja / ja / und nein / nein / dann was uber das ist / ist von argem. Er sagt / Ewer rede oder wort sol sein ja und nein Das man nit verstahn wölle / das er die meinung zugelassen hab. Christus ist eynfeltig ja und nein / unnd alle die in eynfeltig suchen

27 

![](/assets/images/5124.jpg)

/ werden sein Wort verstahn/ Amen.

Liebe Brüder und Schwestern im Herren / das sind die Artickel / die etlich Brüder bisher irrig / und den waren verstand ungleich verstanden haben / unnd damit viel schwacher gewissen verwirret / dadurch der nam Gottes gar gröslich verlestert ist worden / darumb dann not ist gewesen / das wir vereynigt sind worden im herrn / Gor sey lob und preis / wie dann geschehen ist.

Nu / dieweil ir reichlich verstanden habt / den willen Gottes jez mal durch uns geoffenbart sein / wirt not sein / das ir den erkannten willen Gottes harriglich / unabgewelzt vollnbringen / Dann ir wisset wl / was dem Knecht zu lohn

28 

![](/assets/images/5125.jpg)

gehört der da wissentlich sündet. Alles was is unwissentlich gethan und bekannt / haben unrecht gehandelt / das ist euch verzihen durch das gleubig Gebet / welches in uns in der versammlung verbracht ist für unser aller fehle und Schuld / durch die genedige verzeihung Gottes / und durch das blut Jesu Christi / Amen.

Habent acht auff alle die nicht wandlen nach der einfaltikeit Göttlicher warheit / die in diesem brieffe begriffen ist von uns in der versammlung / damit jedermann geregieret werde unter uns / durch die regel des Bans / und furohin verhüt werde dee falschen Brüder und Schwestern zugang unter uns.

Sondert ab von euch was bös ist /

29 

![](/assets/images/5126.jpg)

so wilder Herr ewer Gott sein / und ir wedent seine Sne und Töchter sein. Lieben Brüder seid eingedenck was Paulus seinen Titum vermanet / Er spricht also / Die heilsam gnad Gottes ist erschienenallen / und züchtiget uns /das wir sollen verleugnen das ungöttlich wesen / und die weltlichen lüste / und züchtigt / gerecht / und Gott selig leben in dieser Welt / und warten auff diesebige Hoffnung und erscheinunge der herrligkeit de grossen Gottes / und unsers heilandes Jhesu Christi / der sich selbs für uns gegeben hat / auff das er uns erlöset von aller Ungerechtigkeit / unnd reiniget ihm selbs ein Volck zum eigenthumb / das da eifferig were zu guten Wercken. Das bedencket / unnd

30 

![](/assets/images/5129.jpg)

seient des geübet / so wird der Herr des friedens mit euch sein.

Der nam Gottes sei ewig gebenedeyet und hochgelobet / Amen. Der Herr gebe euch seinen frieden / Amen.

Acta Schlatten am Randen auff Matthie / Anno M.D.XXVII.

***

Übersetzung

Brüderliche Vereinigung etlicher Kinder Gottes, sieben Artikel betreffend Freude, Friede und Barmherzigkeit von unserm Vater durch die Gemeinschaft des Blutes Jesu Christi, mitsamt den Gaben des Geistes, der vom Vater gesendet wird, allen Gläubigen zur Stärkung, zum Trost und zur Beständigkeit in aller Trübsal bis ans Ende. Amen.

Das wünschen wir allen Liebhabern Gottes und Kindern des Lichtes, welche zerstreut sind allenthalben, wohin sie von Gott unserem Vater verordnet und wo sie einmütiglich in einem Gott und Vater unser aller versammelt sind. Gnade und Friede im Herzen sei mit Euch allen. Amen.

Liebe Brüder und Schwestern in dem Herrn!

Uns liegt zuerst und vor allem daran. Euch zu trösten und Euer Gewissen, das eine Weile verwirrt war, zu stärken, damit Ihr nicht für immer als Heiden von uns abgesondert und mit Recht fast ganz ausgeschlossen werdet, sondern Euch wieder den wahren, eingepflanzten Gliedern Christi, die mit Geduld und Erkenntnis Christi ausgerüstet werden, zuwendet und so wieder mit uns vereinigt werdet in der Kraft eines göttlichen, christlichen Geistes und Eifers zu Gott.

Es ist offenkundig, mit welcher Tausendlistigkeit der Teufel uns hintergangen hat, damit er bei ihnen das Werk Gottes, das unter uns eine Zeitlang barmherzig und gnädig begonnen worden ist, zerstöre und zu Grunde richte. Aber der treue Hirte unserer Seele, Christus, der solches in uns angefangen hat, der wird es bis ans Ende führen und lehren zu seiner Ehre und unserm Heil. Amen.

Liebe Brüder und Schwestern! Wir, die wir zu Schleitheim am Randen im Herrn versammelt gewesen sind, tun allen Liebhabern Gottes kund, dass wir in den Stücken und Artikeln übereingekommen sind, die wir im Herrn halten sollen, wenn wir gehorsame Kinder, Söhne und Töchter Gottes sein wollen, die abgesondert von der Welt in allem Tun und Lassen sind und sein wollen. Gott allein sei Preis und Lob, dass es ohne den Widerspruch irgendeines Bruders und in voller Zufriedenheit geschehen ist. In dem allem haben wir gespürt, dass die Einigkeit des Vaters und des uns alle verbindenden Christus samt ihrem Geist mit uns gewesen ist. Denn der Herr ist der Herr des Friedens und nicht des Zankes, wie Paulus sagt. Damit Ihr aber versteht, in welchen Punkten das geschehen ist, sollt Ihr aufmerken und verstehen.

Es ist von einigen falschen Brüdern unter uns ein sehr großes Ärgernis erregt worden. Es haben sich einige vom Glauben abgewandt, indem sie meinten, sie übten und gebrauchten die Freiheit des Geistes und Christi. Aber sie haben die Wahrheit verfehlt und haben sich (sich selbst zum Gericht) der Geilheit und Freiheit des Fleisches ergeben und haben gedacht, der Glaube und die Liebe könnten alles tun und dulden und nichts könne ihnen schaden oder verwerflich sein, weil sie doch gläubig seien.

Merkt auf, ihr Glieder Gottes in Jesus Christus: Der Glaube an den himmlischen Vater durch Jesus Christus ist nicht so gestaltet, wirkt und handelt nicht solche Dinge, wie diese falschen Brüder und Schwester sie tun und lehren. Hütet Euch und seid gewarnt vor solchen! Denn sie dienen nicht unserm Vater, sondern ihrem Vater, dem Teufel.

Ihr aber nicht so! Denn die zu Christus gehören, die haben ihr Fleisch gekreuzigt mitsamt allen Lüsten und Begierden. Ihr versteht mich wohl und (wisst), welche Brüder wir meinen. Sondert Euch von ihnen ab! Denn sie sind verkehrt. Bittet den Herrn, dass sie zur Erkenntnis und zur Busse kommen und dass wir beständig sind, den begonnenen Weg weiterzugehen nach der Ehre Gottes und seines Sohnes Christus. Amen.

Die Punkte, die wir behandelt haben und in denen wir eins geworden sind, das sind diese:

1\. Taufe, 2. Bann, 3. Brechung des Brotes, 4. Absonderung von Greueln, 5. Hirten in der Gemeinde, 6. Schwert, 7. Eid.

Zum ersten merkt Euch über die Taufe: Die Taufe soll allen denen gegeben werden, die über die Busse und Änderung des Lebens belehrt worden sind und wahrhaftig glauben, dass ihre Sünden durch Christus hinweggenommen sind, und allen denen, die wandeln wollen in der Auferstehung Jesu Christi und mit ihm in den Tod begraben sein wollen, auf dass sie mit ihm auferstehen mögen, und allen denen, die es in solcher Meinung von uns begehren und von sich selbst aus fordern. Damit wird jede Kindertaufe ausgeschlossen, des Papstes höchster und erster Greuel. Dafür habt Ihr Beweise und Zeugnisse in der Schrift und Beispiele bei den Aposteln Matth. 27. Mark. 16. Apg. 2. 8. 16. 19. Dabei wollen wir einfältig, aber doch fest und mit Gewissheit bleiben.

Zum zweiten haben wir uns folgendermaßen über den Bann geeinigt: Der Bann soll bei allen denen Anwendung finden, die sich dem Herrn ergeben haben, seinen Geboten nachzuwandeln, und bei allen denen, die in den einen Leib Christi getauft worden sind, sich Brüder oder Schwestern nennen lassen und doch zuweilen ausgleiten, in einen Irrtum und eine Sünde fallen und unversehens überrascht werden. Dieselben sollen zweimal heimlich ermahnt und beim dritten Mal öffentlich vor der ganzen Gemeinde zurechtgewiesen oder gebannt werden nach dem Befehl Christi. Das aber soll nach der Anordnung des Geistes Gottes vor dem Brotbrechen geschehen, damit wir alle einmütig und in einer Liebe von einem Brot brechen und essen können und von einem Kelch trinken.

Zum dritten, was das Brotbrechen anlangt, sind wir uns einig geworden und haben folgendes vereinbart: Alle, die ein Brot brechen wollen zum Gedächtnis des gebrochenen Leibes Christi, und alle, die von einem Trank trinken wollen zum Gedächtnis des vergossenen Blutes Christi, die sollen vorher vereinigt sein zu einem Leib Christi, das ist zur Gemeinde Gottes, an welcher Christus das Haupt ist, nämlich durch die Taufe. Denn wie Paulus sagt, können wir nicht zugleich teilhaftig sein des Tisches des Herrn und des Tisches der Teufel. Wir können auch nicht zugleich teilhaftig sein und trinken des Herren Kelch und der Teufel Kelch. Das heißt: Alle, die Gemeinschaft haben mit den toten Werken der Finsternis, die haben kein Teil am Licht, also alle, die dem Teufel folgen und der Welt, die haben kein Teil mit denen, die aus der Welt zu Gott berufen sind. Alle, die dem Bösen verfallen sind, haben kein Teil am Guten. So soll und muss es auch sein: Wer nicht die Berufung eines Gottes zu einem Glauben, zu einer Taufe, zu einem Leib zusammen mit allen Kindern Gottes hat, der kann auch nicht mit ihnen zu einem Brot werden, wie es doch sein muss, wo man das Brot in der Wahrheit nach dem Befehl Christi brechen will.

Zum vierten haben wir uns über die Absonderung geeinigt: Sie soll geschehen von den Bösen und vom Argen, das der Teufel in der Welt gepflanzt hat, damit wir ja nicht Gemeinschaft mit ihnen haben und mit ihnen in Gemeinschaft mit ihren Greueln laufen. Das heißt, weil alle, die nicht in den Gehorsam des Glaubens getreten sind und die sich nicht mit Gott vereinigt haben, dass sie seinen Willen tun wollen, ein großer Greuel vor Gott sind, so kann und mag nichts anderes aus ihnen wachsen oder entspringen als greuliche Dinge. Nun gibt es nie etwas anderes in der Welt und in der ganzen Schöpfung als Gutes und Böses, gläubig und ungläubig, Finsternis und Licht, Welt und solche, die die Welt verlassen haben, Tempel Gottes und die Götzen, Christus und Belial, und keins kann mit dem ändern Gemeinschaft haben.

Nun ist uns auch das Gebot des Herrn offenbar, in welchem er uns befiehlt, abgesondert zu sein und abgesondert zu werden vom Bösen; dann wolle er unser Gott sein und wir würden seine Söhne und Töchter sein. Weiter ermahnt er uns, Babylon und das irdische Ägypten zu verlassen, damit wir nicht auch ihrer Qualen und Leiden teilhaftig werden, die der Herr über sie herbeiführen wird. Aus dem allen sollen wir lernen, dass alles, was nicht mit unserem Gott und mit Christus vereinigt ist, nichts anderes ist als die Greuel, die wir meiden und fliehen sollen. Damit sind gemeint alle päpstlichen und widerpäpstlichen Werke und Gottesdienste, Versammlungen, Kirchenbesuche, Weinhäuser, Bündnisse und Verträge des Unglaubens und anderes dergleichen mehr, was die Welt für hoch hält und was doch stracks wider den Befehl Gottes durchgeführt wird, gemäss all der Ungerechtigkeit, die in der Welt ist. Von all diesem sollen wir abgesondert werden und kein Teil mit solchen haben. Denn es sind eitel Greuel, die uns verhasst machen vor unserm Jesus Christus, welcher uns befreit hat von der Dienstbarkeit des Fleisches und fähig gemacht hat zum Dienst Gottes durch den Geist, welchen er uns gegeben hat. So werden dann auch zweifellos die unchristlichen, ja teuflischen Waffen der Gewalt von uns fallen, als da sind Schwert, Harnisch und dergleichen und jede Anwendung davon, sei es für Freunde oder gegen die Feinde - kraft des Wortes Christi: Ihr sollt dem Übel nicht widerstehen.

Zum fünften haben wir uns über die Hirten in der Gemeinde folgendermaßen geeinigt: Der Hirte in der Gemeinde Gottes soll ganz und gar nach der Ordnung von Paulus einer sein, der einen guten Leumund von denen hat, die außerhalb des Glaubens sind. Sein Amt soll sein Lesen und Ermahnen und Lehren, Mahnen, Zurechtweisen, Bannen in der Gemeinde und allen Brüdern und Schwestern zur Besserung vorbeten, das Brot anfangen zu brechen und in allen Dingen des Leibes Christi Acht haben, dass er gebaut und gebessert und dem Lästerer der Mund verstopft wird. Er soll aber von der Gemeinde, welche ihn erwählt hat, unterhalten werden, wenn er Mangel haben sollte. Denn wer dem Evangelium dient, soll auch davon leben, wie der Herr verordnet. Wenn aber ein Hirte etwas tun sollte, was der Zurechtweisung bedarf, soll mit ihm nur vor zwei oder drei Zeugen gehandelt werden. Und wenn sie sündigen, sollen sie vor allen zurechtgewiesen werden, damit die ändern Furcht haben. Wenn aber dieser Hirte vertrieben oder durch das Kreuz zum Herrn hingeführt werden sollte, soll von Stund an ein anderer an seine Stelle verordnet werden, damit das Völklein und Häuflein Gottes nicht zerstört, sondern durch die Mahnung erhalten und getröstet wird.

Zum sechsten haben wir uns über das Schwert folgendermaßen geeinigt: Das Schwert ist eine Gottesordnung außerhalb der Vollkommenheit Christi. Es straft und tötet den Bösen und schützt und schirmt den Guten. Im Gesetz wird das Schwert über die Bösen zur Strafe und zum Tode verordnet. Es zu gebrauchen, sind die weltlichen Obrigkeiten eingesetzt. In der Vollkommenheit Christi aber wird der Bann gebraucht allein zur Mahnung und Ausschließung dessen, der gesündigt hat, nicht durch Tötung des Fleisches, sondern allein durch die Mahnung und den Befehl, nicht mehr zu sündigen. Nun wird von vielen, die den Willen Christi uns gegenüber nicht erkennen, gefragt, ob auch ein Christ das Schwert gegen den Bösen zum Schutz und Schirm des Guten und um der Liebe willen führen könne und solle. Die Antwort ist einmütig folgendermaßen geoffenbart. Christus lehrt und befiehlt uns, dass wir von ihm lernen sollen; denn er sei milde und von Herzen demütig, und so würden wir Ruhe finden für unsere Seelen. Nun sagt Christus zum heidnischen Weiblein, das im Ehebruch ergriffen worden war, nicht, dass man es steinigen solle nach dem Gesetz seines Vaters -- obgleich er sagt: wie mir der Vater befohlen hat, so tue -, sondern spricht (nach dem Gesetz) der Barmherzigkeit und Verzeihung und Mahnung, nicht mehr zu sündigen: "Gehe hin und sündige nicht mehr“. Zweitens wird wegen des Schwertes gefragt, ob ein Christ Urteil sprechen soll in weltlichem Zank und Streit, den die Ungläubigen miteinander haben. Die Antwort ist diese: Christus hat nicht entscheiden oder urteilen wollen zwischen Bruder und Bruder des Erbteils wegen, sondern hat sich dem widersetzt. So sollen wir es auch tun. Drittens wird des Schwertes halber gefragt, ob der Christ Obrigkeit sein soll, wenn er dazu gewählt wird. Dem wird so geantwortet: Christus sollte zum König gemacht werden, ist aber geflohen und hat die Ordnung seines Vaters nicht berücksichtigt. So sollen wir es auch tun und ihm nachlaufen. Wir werden dann nicht in der Finsternis wandeln. Denn er sagt selbst: "Wer mir nachfolgen will, der verleugne sich selbst und nehme sein Kreuz auf sich und folge mir nach“. Auch verbietet er selbst die Gewalt des Schwertes und sagt: "Die weltlichen Fürsten, die herrschen" usw.; "ihr aber nicht also". Weiter sagt Paulus: "Welche Gott zuvor ersehen hat, die hat er auch verordnet, dass sie gleichförmig sein sollen dem Ebenbild seines Sohnes" usw. Auch sagt Petrus: "Christus hat gelitten, nicht geherrscht und hat uns ein Vorbild gelassen, dass ihr seinen Fußstapfen nachfolgen sollt". Zum letzten stellt man fest, dass es dem Christen aus folgenden Gründen nicht ziemen kann, eine Obrigkeit zu sein: Das Regiment der Obrigkeit ist nach dem Fleisch, das der Christen nach dem Geist. Ihre Häuser und Wohnung sind mit dieser Welt verwachsen; die der Christen sind im Himmel. Ihre Bürgerschaft ist in dieser Welt; die Bürgerschaft der Christen ist im Himmel. Die Waffen ihres Streits und Krieges sind fleischlich und allein wider das Fleisch; die Waffen der Christen aber sind geistlich wider die Befestigung des Teufels. Die Weltlichen werden gewappnet mit Stachel und Eisen; die Christen aber sind gewappnet mit dem Harnisch Gottes, mit Wahrheit, Gerechtigkeit, Friede, Glaube, Heil und mit dem Wort Gottes. In summa: Wie Christus, unser Haupt über uns, gesinnt ist, so sollen in allem die Glieder des Leibes Christi durch ihn gesinnt sein, damit keine Spaltung im Leib ist, durch die er zerstört wird. Denn ein jedes Reich, das in sich selbst zerteilt ist, wird zerstört werden. Da nun Christus so ist, wie von ihm geschrieben steht, so müssen die Glieder auch so sein, damit sein Leib ganz und einig bleibt zu seiner eigenen Besserung und Erbauung.

Zum siebten haben wir uns über den Eid folgendermaßen geeinigt: Der Eid ist eine Bekräftigung unter denen, die zanken oder Versprechungen machen, und es ist im Gesetz befohlen, dass er im Namen Gottes allein wahrhaftig und nicht falsch geleistet werden soll. Christus, der die Erfüllung des Gesetzes lehrt, der verbietet den Seinen alles Schwören, sowohl recht als auch falsch, sowohl beim Himmel als auch beim Erdreich, bei Jerusalem oder bei unserm Haupt, und das aus dem Grund, den er gleich darauf ausspricht: "Denn ihr könnt nicht ein Haar weiß oder schwarz machen". Sehet zu! Darum ist alles Schwören verboten. Denn wir können nichts von dem garantieren, was beim Schwören versprochen wird, weil wir an uns nicht das Geringste ändern können. Nun sind einige, die dem einfältigen Gebot Gottes nicht Glauben schenken, sondern sagen und fragen so: Ei, nun hat Gott dem Abraham bei sich selbst geschworen, weil er Gott war (als er ihm nämlich versprach, dass er ihm wohl wollte und dass er sein Gott sein wollte, wenn er seine Gebote hielte); warum sollte ich nicht auch schwören, wenn ich einem etwas verspreche? Antwort: Höre, was die Schrift sagt: "Als Gott den Erben der Verheißung auf überschwängliche Art beweisen wollte, dass sein Ratschluss nicht wankt, legte er einen Eid ab, damit wir durch zwei unerschütterliche Dinge (wodurch es unmöglich war, dass Gott lügen könnte) einen starken Trost haben". Merke die Bedeutung dieser Schriftstelle: Gott hat Gewalt zu tun, was er dir verbietet. Denn es ist ihm alles möglich. Gott hat dem Abraham einen Eid geschworen - sagt die Schrift -, um zu beweisen, dass sein Rat nicht wankt. Das heißt: Es kann niemand seinem Willen widerstehen und hinderlich werden. Darum konnte er den Eid halten. Wir aber vermögen es nicht, wie es oben von Christus ausgesprochen ist, dass wir den Eid halten oder leisten. Darum sollen wir nicht schwören. Nun sagen weiter einige so: Es ist im Neuen Testament nicht verboten, bei Gott zu schwören, und im Alten sogar geboten. Dagegen sei lediglich verboten, beim Himmel, Erdreich, bei Jerusalem und bei unserm Haupt zu schwören. Antwort. Höre die Schrift: "Wer da schwört beim Himmel, der schwört beim Stuhl Gottes und bei dem, der darauf sitzt". Merke: Schwören beim Himmel, der ein Stuhl Gottes ist, ist verboten. Wie viel mehr ist es bei Gott selbst verboten! Ihr Narren und Blinden, was ist größer, der Stuhl oder der darauf sitzt? Auch sagen einige so: Wenn es nun unrecht ist, dass man Gott zur Wahrheit gebraucht, so haben die Apostel Petrus und Paulus auch geschworen. Antwort: Petrus und Paulus bezeugen allein das, was von Gott Abraham durch den Eid verheißen war, und sie selbst verheißen nichts, wie die Beispiele klar zeigen. Aber Zeugen und Schwören ist zweierlei. Denn wenn man schwört, so verheißt man Dinge, die noch in der Zukunft liegen, wie dem Abraham Christus verheißen wurde, den wir lange Zeit hernach empfangen haben. Wenn man aber zeugt, dann bezeugt man das Gegenwärtige, ob es gut ist oder böse, wie der Simeon zu Maria von Christus sprach und ihr bezeugte: "Dieser wird gesetzt zu einem Fall und einer Auferstehung vieler in Israel und zu einem Zeichen, dem widersprochen wird". Dasselbe hat uns auch Christus gelehrt, als er sagte: "Eure Rede soll sein ja ja und nein nein; denn was darüber ist, ist vom Argen. Er sagt: Eure Rede oder euer Wort soll sein ja und nein, was man nicht so verstehen kann, als ob er den Eid zugelassen habe. Christus ist einfältig ja und nein, und alle, die ihn einfältig suchen, werden sein Wort verstehen. Amen.

Liebe Brüder und Schwestern im Herrn! Das sind die Artikel, die einige Brüder bisher falsch und dem wahren Sinn zuwider verstanden haben. Sie haben damit viele schwache Gewissen verwirrt, wodurch der Name Gottes sehr schwer gelästert worden ist. Darum ist es notwendig gewesen, dass wir im Herrn übereingekommen sind, wie es auch geschehen ist. Gott sei Lob und Preis. Weil Ihr nun den Willen Gottes reichlich verstanden habt, wie er jetzt durch uns offenbart ist, wird es notwendig sein, dass Ihr den erkannten Willen Gottes beharrlich und ohne Aufschub vollbringt. Denn Ihr wisst wohl, was dem Knecht an Lohn gehört, der wissentlich sündigt.

Alles, was Ihr unwissentlich getan habt und was Ihr bekannt habt, unrecht gehandelt zu haben, das ist Euch verziehen durch das gläubige Gebet, das in uns in der Versammlung vollbracht ist für unser aller Verfehlung und Schuld, durch die gnädige Verzeihung Gottes und durch das Blut Jesu Christi. Amen.

Habt acht auf alle, die nicht nach der Einfältigkeit göttlicher Wahrheit wandeln, die in diesem Brief von uns in der Versammlung zusammengefasst ist, damit jedermann unter uns regiert werde durch die Regel des Banns und forthin der Zugang der falschen Brüder und Schwestern unter uns verhütet werde.

Sondert ab von Euch, was böse ist, so will der Herr Euer Gott sein und Ihr werdet seine Söhne und Töchter sein.

Liebe Brüder, seid eingedenk, mit was Paulus seinen Titus ermahnt. Er spricht so: "Die heilsame Gnade Gottes ist erschienen allen und züchtigt uns, dass wir sollen verleugnen das ungöttliche Wesen und die weltlichen Lüste und züchtig, gerecht und gottselig leben in dieser Welt und warten auf dieselbe Hoffnung und Erscheinung der Herrlichkeit des großen Gottes unseres Heilands Jesus Christus, der sich selbst für uns gegeben hat, auf dass er uns erlöste von aller Ungerechtigkeit und reinigte sich selbst ein Volk zum Eigentum, das da eifrig wäre zu guten Werken. Das bedenkt und übt Euch darin, so wird der Herr des Friedens mit Euch sein.

Der Namen Gottes sei ewig gebenedeit und hoch gelobt. Amen.

Der Herr gebe Euch seinen Frieden. Amen.

Geschehen in Schleitheim am Randen, auf Matthiae (24. Febr.), Anno 1527.