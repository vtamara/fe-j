---
layout: post
title: Contrato arrendamiento con opcion de compra
author: vtamara
categories:
- Artículo
image: "/assets/images/98-01.jpg"
tags: Artículo

---
He reflexionado que los arriendo están bien para personas que no pueden trabajar y que en otro caso se quedarían sin otras formas de ingreso. En el momento no me parece adecuado para personas con salud, posibilidades de trabajar y/o ingresos suficientes para satisfacer las necesidades básicas de su familia.

Me parece mejor vender a cuotas o financiar la compra, para evitar mala fe de quien compra, me parece que el arrendamiento con opción de compra, con la garantía de devolver lo pagado en arriendo en caso de incumplimiento (menos gastos) es una buena forma de vender un predio a alguien que no logra tener lo suficiente para un financiamiento típico.

Respecto a intereses Éxodo 22:25 dice "Cuando prestares dinero a uno de mi pueblo, al pobre que está contigo, no te portarás con él como logrero, ni le impondrás usura" (RV1960). Interpretándolo de manera literal y queriendo ayudar a financiar una vivienda a un hermano(a) en Cristo, ¿Qué tal el siguiente modelo de Contrato de arrendamiento con opción de compra?

[https://docs.google.com/document/d/18UxjXz75qFRldeaa96Tlw5hGM77m-sZ9VFEAtYV6W0A/edit?usp=sharing](https://docs.google.com/document/d/18UxjXz75qFRldeaa96Tlw5hGM77m-sZ9VFEAtYV6W0A/edit?usp=sharing "https://docs.google.com/document/d/18UxjXz75qFRldeaa96Tlw5hGM77m-sZ9VFEAtYV6W0A/edit?usp=sharing")

Me ha resultado interesante encontrar que Habitat, por su fe, no cobra intereses: [http://www.habitat.org/lc/theforum/spanish/finanzas/Las_consecuencias.aspx](http://www.habitat.org/lc/theforum/spanish/finanzas/Las_consecuencias.aspx "http://www.habitat.org/lc/theforum/spanish/finanzas/Las_consecuencias.aspx")