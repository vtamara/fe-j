---
layout: post
title: "Promesa_de_Sanidad"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
#1. INTRODUCCIÓN

Recibamos hoy como promesa que Él es nuestro sanador. Oramos pidiendo Espíritu Santo para que nos guie y nos permita recibirla, nos permita creer y clamar.   Se trata de una promesa que el Señor hizo viva en mi, y que por eso hoy comparto clamando a Él que la haga viva en ti.  Oramos pidiendo fe, la fe correcta en Jesucristo para recibirla, creerla y vivirla.

#2. TEXTO
Exodo 15:22-27 El agua amarga de Mara
<pre>
22 E hizo Moisés que partiese Israel del Mar Rojo, y salieron al desierto de 
   Shur; y anduvieron tres días por el desierto sin hallar agua.
23 Y llegaron a Mara, y no pudieron beber las aguas de Mara, porque eran 
   amargas; por eso le pusieron el nombre de Mara.[a]
24 Entonces el pueblo murmuró contra Moisés, y dijo: ¿Qué hemos de beber?
25 Y Moisés clamó a Jehová, y Jehová le mostró un árbol; y lo echó en las 
   aguas, y las aguas se endulzaron. Allí les dio estatutos y ordenanzas, y 
   allí los probó;
26 y dijo: Si oyeres atentamente la voz de Jehová tu Dios, e hicieres lo 
   recto delante de sus ojos, y dieres oído a sus mandamientos, y guardares 
   todos sus estatutos, ninguna enfermedad de las que envié a los egipcios 
   te enviaré a ti; porque yo soy Jehová tu sanador.
27 Y llegaron a Elim, donde había doce fuentes de aguas, y setenta palmeras; 
   y acamparon allí junto a las aguas. 
</pre>


##2.1 Contexto cronológico y geográfico

En una cronología bíblica típica (ver {5}) podemos ubicar a Moisés hacía el año 1400aC, los hechos narrados ocurren después de la salida de Israel de Egipto, justo después de que el Señor había permitido pasar al pueblo en medio del Mar Rojo y había hundido allí al ejército del faraon, sin requerir violencia de parte de los israelitas que acababan de salir de Egipto.   El siguiente mapa muestra una posible ruta, note Elim que se menciona en el versículo 27, Mara debe estar un poco al norte.

[http://i2.squidoocdn.com/resize/squidoo_images/590/draft_lens4095382module27805792photo_1309931209Red_Sea_MAP_Exodus_Rephid]


Por cierto, Moises en Ex 15:13-18 oró para que los Israelitas no tuvieran que derramar sangre para entrar a Cannan, sino que el Señor sacará a los pueblos que ocupaban llenos de temor,  entendemos que no derramar sangre humana es la voluntad del Señor dada antes en Gen 9:6, por eso también creemos que el Señor no quería que los israelitas emplearan violencia para la toma de Canaan que seguiría; sin embargo creemos que por el corazón duro de los israelitas, por ejemplo visto en el versículo 24 de nuestro texto donde el pueblo murmura contra Moisés en lugar de clamar a Dios y confiar en Él y lo mismo en Ex 16:2 y Ex 17:3, por eso tuvieron que empezar a derramar sangre en la primera batalla contra los amalecitas descrita en Ex 17:8-16. 



##2.2 Detalles  del texto
En hebreo la palabra &#1502;&#1463;&#1512; (mar) se traduce como amargo, las aguas de ese sitio eran así y por es el nombre Mara (en hebreo &#1502;&#1512;&#1492;).
En el texto hebreo de este pasaje al final se habla de  &#1497;&#1456;&#1492;&#1493;&#1464;&#1492; &#1512;&#1465;&#1508;&#1456;&#1488;&#1462;&#1469;&#1498;&#1464;  Jehova Rafa.  Rafa es sanador o médico, sanar, ser sanado, restaurar o reparar.  Y todo esto es Dios para nosotros, como nos lo dice hoy.


#3. ANÁLISIS

##3.1 Testimonio

Junto con la ruptura del tendón de Aquiles sufrí una trombosis venosa profunda el 24 de Junio del 2012, doppler el 26:

[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/c1.png]

En el segundo doppler el 22.Jul casi un mes después  no había mucha esperanza en que pudieran canalizar las venas con trombos pues todo estaba igual tras un mes.  En algunas personas una TVP puede canalizar naturalmente pero no es común, respecto a tratamientos  cuando es fresca (6 horas después del trombo en {3}, o según {4} hasta 8 días después ) puede ser tratada con un método reciente pero muy tedioso --pudiendo llegar hasta las 4:30 horas de intervención-- (ver {2}), consiste en poner catéteres en las venas con trombos e inyectar por el cateter químicos que ayudan a disolver trombos frescos. Este método sin embargo no permite disolver trombos en venas pequeñas como las de la pantorrilla y no es efectivo con trombos que se han endurecido --solo trombos recientes.

[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/c2.png]

En una posterior cita con una doctora, me dijo que los trombos quedarían allí de por vida, lo mismo me dijo el doctor que me atendió esta semana tras mostrarle los 2 primeros diagnósticos, sin embargo le dije al doctor, que eso ya me lo habían dicho otra doctora, pero que yo le había orado a Dios y que en un tercer doppler que me efectuaron más de 2 meses después, el 28 de Septiembre resultó que no tenía trombo alguno --posteriormente el doctor dijo que alrededor 1 de cada 5 trombosis se diluye naturalmente (me pregunto de donde sacó esa estadística y si tuvo en cuenta que el trombo en mi caso ya se había endurecido pues casi un mes después tenía la misma posición y tamaño y no había canalizado).

Yo había estado clamando por canalización del trombo tras el segundo examen en julio -de hecho le pedí al pastor Jaime Ramirez precisamente eso, canalización del trombo, y el había orado en el nombre de Jesús por esto-- y había recibido unos días antes de ese examen esta palabra de hoy.  Pero el Señor hizo más, a la doctora que me hizo examen el 28.Sep le dije que había sufrido TVP, ella dijo que examinaría ambas piernas, comenzó con la pierna derecha donde había estado el trombo,  tras terminar está pierna preguntó, el trombo está en la otra pierna verdad?  Es que en la derecha no encontró trombos, repitió varias veces buscando en las venas donde se habían alojado y descritas en el primer doppler, pero no encontró. 

[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/c3.png]

Hoy veo y creo que el Señor hizo más de lo que pedí para que no me quedara duda de que Él es mi sanador, mi Rafa.    Desde el 1400 antes de Cristo hasta hoy, desde esos desiertos cerca a Egipto y Canaan, hasta hoy en donde tu estas, el Señor se agrada y quiere ser tu Rafa. 

El Señor es soberano y solo Él lo es y sana a quien, cuando quiere y como Él quiere, pero estudiemos como lo ha hecho según la Biblia, primero tengamos fe en esa promesa de Exodo 15:26 de que el es Rafa, reiterada por ejemplo en Jeremias 33:6:
<pre>
He aquí que yo les traeré sanidad y medicina; y los curaré, y les revelaré 
abundancia de paz y de verdad.
</pre>

##3.2 Fe y por esta obedecer sus estatutos

Por Exodo 15:26, que se da después de las maravillosas demostraciones del poder del Señor, que aumentaban la fe, se ve algo que el Señor la quiere de nosotros: Si oyeres atentamente la voz de Jehová tu Dios, e hicieres lo recto delante de sus ojos, y dieres oído a sus mandamientos, y guardares todos sus estatutos, ninguna enfermedad de las que envié a los egipcios te enviaré a ti;
El quiere sanar a su pueblo santo, también por esto la sanidad del cuerpo va de precedida del perdón, como dice el Salmo 103:3
<pre>
El es quien perdona todas tus iniquidades,
El que sana todas tus dolencias;
</pre>

##3.3 Buscar primero al Señor en particular en caso de enfermedad
No despreciamos los esfuerzos humanos, pero primero en casos de enfermedad debemos buscar al Señor, veamos:
Buscar a Jehova es indispensable como confirma 2 Cro 16:12-13
<pre>
En el año treinta y nueve de su reinado, Asa enfermó gravemente de los pies, 
y en su enfermedad no buscó a Jehová, sino a los médicos.
Y durmió Asa con sus padres, y murió en el año cuarenta y uno de su reinado.
</pre>

##3.4 Clamar a Jehova
Busquemolo con clamor profundo como dice el Sal  6:2
<pre>
  Ten misericordia de mí, oh Jehová, porque estoy enfermo;
</pre>
Sáname, oh Jehová, porque mis huesos se estremecen.
Nuestro clamor que vaya con alabanza como dice Jeremias 17:14
<pre>
14 Sáname, oh Jehová, y seré sano; sálvame, y seré salvo; porque tú eres mi alabanza.
</pre>
2 Reyes 20:5 
<pre>
Vuelve, y di a Ezequías, príncipe de mi pueblo: Así dice Jehová, el Dios de 
David tu padre: Yo he oído tu oración, y he visto tus lágrimas; he aquí que 
yo te sano; al tercer día subirás a la casa de Jehová.
</pre>
La oración y lagrimas de Ezequías se ve en 2 Reyes 20:1-7
<pre>
1 En aquellos días Ezequías cayó enfermo de muerte. Y vino a él el profeta 
Isaías hijo de Amoz, y le dijo: Jehová dice así: Ordena tu casa, porque 
morirás, y no vivirás.
2 Entonces él volvió su rostro a la pared, y oró a Jehová y dijo:
3 Te ruego, oh Jehová, te ruego que hagas memoria de que he andado delante 
de ti en verdad y con íntegro corazón, y que he hecho las cosas que te 
agradan. Y lloró Ezequías con gran lloro.
</pre>

##3.5 Escuchar sus instrucciones  y obedecerlas
Hay sanidades instantáneas que no requieren más, pero hay sanidades que también provienen del Señor y donde se emplea algo más:
En el caso de Ezequías, 2 Rey 20:7 dice: 
<pre>Y dijo Isaías: Tomad masa de higos. Y tomándola, la pusieron sobre la 
llaga, y sanó.
</pre>
El mismo Señor Jesús en ocasiones empleó materiales además de su presencia y palabra:
Juan 9:5-7 dice
<pre>
5 Entre tanto que estoy en el mundo, luz soy del mundo.
6 Dicho esto, escupió en tierra, e hizo lodo con la saliva, y untó con el 
  lodo los ojos del ciego,
7 y le dijo: Ve a lavarte en el estanque de Siloé (que traducido es, 
  Enviado). Fue entonces, y se lavó, y regresó viendo.
</pre>

##3.6 En el nombre de Jesús

Es Jesucristo quien da la sanidad no el pastor, ni nadie más: Hechos 9:33-34 lo confirma:
<pre>
33 Y halló allí a uno que se llamaba Eneas, que hacía ocho años que estaba 
   en cama, pues era paralítico.
34 Y le dijo Pedro: Eneas, Jesucristo te sana; levántate, y haz tu cama. Y 
   en seguida se levantó.
</pre>

Pues fue Él quien pagó el precio de nuestro pecado y por sus llagas somos sanos como dice Isaias 53:5.  Por eso cuando ministremos sanidad hagámoslo en el nombre de Jesús como nos enseña por ejemplo en Col 3:17 Y todo lo que hagan, de palabra o de obra, háganlo en el nombre del Señor Jesús, dando gracias a Dios el Padre por medio de él.  
Y confirma en Hechos 3:16 -
<pre>
Y por la fe en su nombre, a éste, que vosotros veis y conocéis, le ha 
confirmado su nombre; y la fe que es por él ha dado a éste esta completa 
sanidad en presencia de todos vosotros.
</pre>
He visto casos de sanidad también hechando fuera toda enfermedad y toda obra del enemigo. Hagámoslo.

##3.7 Gracias
Cuando el Señor nos sane o sane a alguien demos gracias como enseña el Salmo 30:2
<pre>
Jehová Dios mío,A ti clamé, y me sanaste.
</pre>

Demos testimonio, pues nuestro testimonio ayuda a edificar y permitirá seguir declarando que Jehova es Rafa:
Isaias 19:22 
<pre>
Y herirá Jehová a Egipto; herirá y sanará, y se convertirán a Jehová, y les 
será clemente y los sanará.
</pre>

#4. RESUMEN Y ORACIÓN

* Jehova es mi sanador.
* Fe y Obediencia a sus estatutos
* Buscar primero al Señor en particular en caso de enfermedad
* Clamar a Jehova
* Escuchar sus instrucciones y seguirlas
* Ministrar sanidad en el nombre de Jesús
* Dar gracias.


#5. BIBLIOGRAFÍA
* {1} http://www.biblegateway.com/passage/?search=exodo%2015:22-27&version=RVR1960 
* {2} http://www.noblood.org/medical-conditions-treatments/4261-cure-deep-vein-thrombosis/#.UHkr-xJEjp4
*  {3} http://www.entornomedico.org/enfermedadesdelaalaz/index.php?option=com_content&view=article&id=481:trombosis&catid=54:enfermedades-con-t&Itemid=491 
* {4}http://cirugiavascularactual.blogspot.com/2007/07/tratamiento-anticoagulante-y-terapia.html
* {5} http://fe.pasosdejesus.org/?id=Historia

----

Gracias a Dios esta prédica de dominio público fue dada por Vladimir Támara el 14.Oct.2012 en la Iglesia Menonita La Resurrección en San Nicolas Soacha.
