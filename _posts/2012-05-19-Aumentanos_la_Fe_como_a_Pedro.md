---
layout: post
title: Aumentanos la Fe como a Pedro
author: vtamara
categories:
- Prédica
image: "/assets/images/pescador_col.jpg"
tags: 

---
# 1. Introducción y Texto

* ¿Quien ha experimentado el amor de Dios?
* ¿Quien ha experimentado muchisimo amor de Dios?
* ¿Quien ha visualizado el amor de Dios como agua pura que baña desde la cabeza hacia los pies o como un río para sumergirse?

Aumentar la fe correcta nos lleva a vivir más el amor de Dios.

En Lucas 17:5-6 encontramos:

    5 Dijeron los apóstoles al Señor: Auméntanos la fe.
    6 Entonces el Señor dijo: Si tuvierais fe como un
    grano de mostaza, podríais decir a este sicómoro:
    Desarráigate, y plántate en el mar; y os obedecería.

Pedro seguramente era uno de los discípulos haciendo esa petición, examinemos como su fe (πίστις - pistis en griego) lo llevó cada vez más al amor de Dios y llegó a ser como la de un grano de mostaza.

Mientras lo hacemos examinemos el tamaño de nuestra fe y oremos para que por el amor del Señor aumente la fe de quien escucha/lee.

# 2. Análisis

## 2.1. Fe correcta

En otros momentos han podido ver la definición de fe de Hebreos 11:1 (Es pues la fe la certeza de lo que se espera, la convicción de lo que no se ve), pero con esa definición hasta podemos decir que los ateos tiene fe, pues esperan que no existiera Dios convencidos por razonamientos humanos incompletos e imperfectos, razonamientos que tampoco pueden ver.

Queremos que el Señor aumente la fe correcta, la cual el mismo Pedro encontró y describió con una pista de como aumentarla en 2 Pedro 1:1-2

    1 Simón Pedro, siervo y apóstol de Jesucristo, a los que habéis alcanzado,
    por la justicia de nuestro Dios y Salvador Jesucristo, una fe igualmente
    preciosa que la nuestra:
    2 Gracia y paz os sean multiplicadas, en el conocimiento de Dios y de
    nuestro Señor Jesús.

Para los cristianos el centro de la fe correcta es la fe en Jesucristo como "nuestro Dios" y en Jesucristo como nuestro Salvador.

Justamente de las 228 veces que aparece la palabra pistis en el nuevo testamento en griego {2} (por cierto todo parece indicar que escrito originalmente en griego), gran parte se refiere a la fe en Jesucristo como Dios y Salvador, que consideramos la fe correcta.

## 2.2. Plantando fe

En el evangelio de Lucas encontramos la primera referencia a Pedro en 4:38-39

    38 Entonces Jesús se levantó y salió de la sinagoga, y entró en casa de
    Simón. La suegra de Simón tenía una gran fiebre; y le rogaron por ella.
    39 E inclinándose hacia ella, reprendió a la fiebre; y la fiebre la dejó,
    y levantándose ella al instante, les servía.

Según Lucas, esto ocurrió después de que Jesús había salido de enseñar en una sinagoga y de liberar un muchacho endemoniado y esto hacía que se aumentara la fama del Señor.

Eventualmente Pedro habría estado en la sinagoga donde Jesús había predicado o habría oído de la fama de Jesús por lo que le permitió entrar a su casa, un acto de confianza frente al amor que había visto en Jesús.
Seguramente presencio la sanidad en su suegra lo que aumentó en él su amor por Jesús y su confianza en Él.  En estos eventos seguramente fue sembrada la semilla de la fe correcta en la vida de Pedro.

En mi caso empecé a tener fe en que Jesús es Dios en la adolescencia entendiendo el amor de los evangelios.

¿En su caso cuando y como fue plantada la semilla de que Jesús es amor y Dios?

## 2.3. Creerle a Jesús, confiar y actuar

Examinemos lo que Pedro hace en Lucas 5:1-11

    1 Aconteció que estando Jesús junto al lago de Genesaret, el gentío se
    agolpaba sobre él para oír la palabra de Dios.
    2 Y vio dos barcas que estaban cerca de la orilla del lago; y los  
    pescadores, habiendo descendido de ellas, lavaban sus redes.
    3 Y entrando en una de aquellas barcas, la cual era de Simón, le rogó que
    la apartase de tierra un poco; y sentándose, enseñaba desde la barca a
    la multitud.
    4 Cuando terminó de hablar, dijo a Simón: Boga mar adentro, y echad vuestras
    redes para pescar.
    5 Respondiendo Simón, le dijo: Maestro, toda la noche hemos estado
    trabajando, y nada hemos pescado; mas en tu palabra echaré la red.
    6 Y habiéndolo hecho, encerraron gran cantidad de peces, y su red se rompía.
    7 Entonces hicieron señas a los compañeros que estaban en la otra barca,
    para que viniesen a ayudarles; y vinieron, y llenaron ambas barcas,
    de tal manera que se hundían.
    8 Viendo esto Simón Pedro, cayó de rodillas ante Jesús, diciendo: Apártate
    de mí, Señor, porque soy hombre pecador.
    9 Porque por la pesca que habían hecho, el temor se había apoderado de él,
    y de todos los que estaban con él,
    10 y asimismo de Jacobo y Juan, hijos de Zebedeo, que eran compañeros de
    Simón. Pero Jesús dijo a Simón: No temas; desde ahora serás pescador
    de hombres.
    11 Y cuando trajeron a tierra las barcas, dejándolo todo, le siguieron.

Pedro era pescador, pero en la noche no había logrado pescar nada, aún así el amor que ya tenía al Señor lo impulsaba a quedarse cerca a Él y a servirle, prestandole su propia barca y seguramente escuchando sus enseñanzas.  Aún cuando sabe que Jesús no es pescador Pedro confia en Él para decir "Más en tu palabra echaré la red".  Le creyó a Jesús, tuvo fe y con confianza en lo que le decía, actuó e hizo lo que Jesús le decía.

En mi caso personal empecé a escuchar a Jesús leyendo Biblia diariamente y a confiar y hacerle caso por su amor en el 2001 tras revelación del Padre.

¿En su caso cuando y como empezó a leer Biblia a diario para escuchar a Jesús y para aprender a amrlo y hacerle caso?

Pedro reconoce el Poder del Señor y lo sigue "dejándolo todo."   La suegra de Pedro tras la sanación le sirve al Señor.  La fe conlleva a confianza y esta a acciones.

En el caso de Pedro pasar de ser pescador a ser pescador de hombres dejandolo todo es un cambio radical en el proyecto de vida.

En mi caso hice un cambio radical en el proyecto de vida para servir mejor al Señor a los 31 años, cuando el Señor me llamo a poner mis manos a su servicio.

¿Cuando replanteó su proyecto de vida por llamamiento del Señor?

Sin embargo el Señor puede requerir no sólo grandes cambios en proyecto de vida completo, sino pequeñas acciones en el día a día por ejemplo cuando le dice a Pedro que vuelva a lanzar la red, o cuando Elias le dice a Nahaman que se lave en el rio Jordan 7 veces tras lo cual queda sano de lepra (2 Reyes 5), o cuando Jesús le dice a un ciego que se lave en una piscina tras lo cual recupera la vista:

    5 Entre tanto que estoy en el mundo, luz soy del mundo.
    6 Dicho esto, escupió en tierra, e hizo lodo con la saliva, y untó con el
    lodo los ojos del ciego,
    7 y le dijo: Ve a lavarte en el estanque de Siloé (que traducido es,
    Enviado). Fue entonces, y se lavó, y regresó viendo.

Eventualmente de manera especial el Señor nos pide que leamos Biblia o que le hablemos a alguien o que realicemos cierta acción que en últimas afianzará nuestra fe y aún más alla nuestro amor por Él.

Que importante es poder escucharlo, discernir su voz, confiar en Él y hacerle caso.

Pero ojo, no se trata de actuar por actuar, pretender hacer muchas obras pero sin fe, las obras deben ser consecuencias de la fe, como dice Santiago 2:18

    Pero alguno dirá: Tú tienes fe, y yo tengo obras. Muéstrame tu fe sin tus obras, y yo te mostraré mi fe por mis obras.

## 2.3  Dios da la fe correcta

Mateo 6:13-17

    13 Viniendo Jesús a la región de Cesarea de Filipo, preguntó a sus discípulos, diciendo: ¿Quién dicen los hombres que es el Hijo del Hombre?
    14 Ellos dijeron: Unos, Juan el Bautista; otros, Elías; y otros, Jeremías, o alguno de los profetas.
    15 El les dijo: Y vosotros, ¿quién decís que soy yo?
    16 Respondiendo Simón Pedro, dijo: Tú eres el Cristo, el Hijo del Dios viviente.
    17 Entonces le respondió Jesús: Bienaventurado eres, Simón, hijo de Jonás, porque no te lo reveló carne ni sangre, sino mi Padre que está en los cielos.

La fe correcta la revela Dios mismo.   Ahora que tenemos Espíritu Santo es el Espíritu Santo quien la da como dice Galatas 5:22-23 es parte del fruto del Espíritu Santo:

    22 Mas el fruto del Espíritu es amor, gozo, paz, paciencia, benignidad,
    bondad, fe,
    23 mansedumbre, templanza; contra tales cosas no hay ley.

## 2.4. Orar pidiendo más fe

Entonces hagamos lo que hicieron los discípulos por amor oremos pidiendo más fe, e incluso fe para otros como nos enseño el Señor en Lucas 22:31-32

    31 Dijo también el Señor: Simón, Simón, he aquí Satanás os ha pedido para zarandearos como a trigo;
    32 pero yo he rogado por ti, que tu fe no falte; y tú, una vez vuelto, confirma a tus hermanos.

## 2.5. No rendirnos

No rendirnos, asi nuestra fe flaquee.  El amor de nuestro Señor nos dará fuerzas para continuar, el mismo Pedro flaqueó como de antemano lo sabía nuestro Señor y quedó en Lucas 22:33-34

    33 El le dijo: Señor, dispuesto estoy a ir contigo no sólo a la cárcel, sino también a la muerte.
    34 Y él le dijo: Pedro, te digo que el gallo no cantará hoy antes que tú niegues tres veces que me conoces.

El Señor sabe que necesitamos fe para disfrutar de su amor, y es de tal medida su amor que permite que personas oren por nosotros para que tegamos más fe.  Es tal su amor que con Jesucristo abolió su propia ley para cambiarla por la gracia que es por fe como nos recuerda Galatas 5:6

    porque en Cristo Jesús ni la circuncisión vale algo, ni la incircuncisión, sino la fe que obra por el amor.

## 2.6 El grano de mostaza

Entonces Pedro pidio más fe y el Señor quería que fuera como la de un gran de mostaza, creo que así ocurrió con Pedro, y cuando su fe fue como un grano de mostaza, él predicaba y miles se convertian (Hechos 3:41 Así que, los que recibieron su palabra fueron bautizados; y se añadieron aquel día como tres mil personas) y el Señor lo liberaba de la carcel,  lo usaba en milagros, en enseñanza, en organizar, en sanidad de manera poderosa como por ejemplo se describe en Hechos 3:2-8

    2 Y era traído un hombre cojo de nacimiento, a quien ponían cada día a la
    puerta del templo que se llama la Hermosa, para que pidiese limosna de los
    que entraban en el templo.
    3 Este, cuando vio a Pedro y a Juan que iban a entrar en el templo, les
    rogaba que le diesen limosna.
    4 Pedro, con Juan, fijando en él los ojos, le dijo: Míranos.
    5 Entonces él les estuvo atento, esperando recibir de ellos algo.
    6 Mas Pedro dijo: No tengo plata ni oro, pero lo que tengo te doy; en el
    nombre de Jesucristo de Nazaret, levántate y anda.
    7 Y tomándole por la mano derecha le levantó; y al momento se le afirmaron
    los pies y tobillos;
    8 y saltando, se puso en pie y anduvo; y entró con ellos en el templo,
    andando, y saltando, y alabando a Dios.

Esa semilla de fe plantada en Pedro por el Señor, que el mismo Señor cuido con amor había crecido y daba fruto.  Pedro disfruto el amor del Señor por esas pequeñas y grandes decisiones de fe, el pescador se hizo gracias a Dios pescador de hombres  --nuevamente el Señor cumplió su promesa.

## 2.7 Como aumentar fe

Según Sprungeon en {3}:

* La escencia de la fe salvadora debe ser la confianza en Cristo,
* Creamos que la fe puede crecer siempre, anhelemos con pasión que crezca.
* Hay un único medio para obtener fe: Espiritu Santo. Entonces oremos, pidamos que nos aumente la fe, escudriñemos la palabra, vivamos en comunión con santos --cristianos y cristianas desde la médula--, experimentemos y testifiquemos de lo que Dios da.
* Evitemos guardarnos los ministerios, alejarnos de Dios o de su palabra.

'Les digo en el nombre de Cristo, lo mismo que Cristo le dijo al pobre leproso agradecido: "Tu fe te ha salvado, vé en paz." Aunque tu vida pasada haya sido muy vil, y hayas venido aquí sin Dios, y sin esperanza, sin embargo, si ahora crees en Jesucristo y confías únicamente en Él, ninguno de tus pecados será mencionado en tu contra nunca más para siempre. "Yo deshice como una nube tus rebeliones, y como niebla tus pecados."' {3}

# 3. Oración/Conclusión

Terminemos repasando 1 Cor 13:2 y 1 Cor 13:13

    2 Y si tuviese profecía, y entendiese todos los misterios y toda ciencia,
    y si tuviese toda la fe, de tal manera que trasladase los montes, y no
    tengo amor, nada soy.
    13 Y ahora permanecen la fe, la esperanza y el amor, estos tres; pero
    el mayor de ellos es el amor.

Y anhelemos ante todo el amor de Dios, sabiendo que requerimos fe para alcanzarlo.  Hoy quiero decirles que hay más, que el rio del amor del Señor es más grande de lo que podemos imaginar, y que apenas estamos en la orilla.

Quiero decirles que el Señor quiere que vayamos más hacia el centro, que disfrutemos más de ese amor que es el mismo, el mismo señor Jesús, pero es la fe la que nos guiará y por eso la necesitamos y necesitamos aumentarla.

Oremos:

* Gracias por tu amor, por querernoslo brindar con pasión y por la fe como herramienta para vivirlo.
* Gracias por haber sembrado fe correcta en nosotros, por revelarnos tu amor Señor Jesús que nos da confianza en ti.
* Por favor que escuchemos tu voz, enseñanos a reconocerla a discernir cuando Tu nos habla.   Quitanos la pereza para hacerte caso, tu nos conoces y quieres y sabes que es lo mejor para cada uno.  Que actuemos cuando nos hables.
* Señor aumentanos la fe.  Con tu Santo Espíritu danos fe y continuamente recuerdanos orar, aprender de tu palabra, pedirte más fe, buscar personas que te amen y ser y dar testimonio.
* Señor cuando lleguen pruebas que hagan flaquear la fe de mis hermanos y hermanas, renuevala te ruego de antemano como tu hiciste con Pedro.
* Señor quiero servirte, regalame fe, revelame acciones pequeñas que debo hacer y replanteame el proyecto de vida para que sea el que tu sueñas. Quiero que mi fe llegue a ser como una semilla de mostaza o mejor más grande para que me uses en sanidad, liberación, para predicar tu palabra de forma que de fruto abundate y de requerirse pueda dar mi vida por amor a Tí como Pedro lo hizo.
* Señor Jesús gracias por haber cambiado la ley por gracia y por hacerla tan fácil de alcanzar con fe en ti como Salvador y Dios.  Si alguien no ha hecho aún su oración de fe y confianza en Jesús hoy puede: \[Oración de fe\].

# 4. Bibliografía

* {1} Biblia. Reina Valera 1960. [http://www.biblegateway.com/passage/?search=lucas%2017:5-6&version=RVR1960]()
* {2} [http://www.blueletterbible.org/lang/lexicon/lexicon.cfm?strongs=G4102&t=KJV&page=1]()
* {3} [http://www.spurgeon.com.mx/sermon3384.html]()

***

El dibujo puede usarse sin fines comerciales y proviene de [http://misdibujoscristianos.blogspot.com/2012/01/pescador-de-hombres.html](http://misdibujoscristianos.blogspot.com/2012/01/pescador-de-hombres.html "http://misdibujoscristianos.blogspot.com/2012/01/pescador-de-hombres.html")