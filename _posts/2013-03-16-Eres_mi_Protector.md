---
layout: post
title: "Eres_mi_Protector"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
<pre>
Eres mi Protector
Llenas mi corazón 
con cánticos de liberación
de angustias me guardarás

//Confiaré en Tí//
El débil dirá fuerte soy con poder del Señor
</pre>

* Letra basada en: http://acordes.lacuerda.net/mus_catolica/eres__mi__protector
* http://www.youtube.com/watch?v=pwvHoDyru_o
