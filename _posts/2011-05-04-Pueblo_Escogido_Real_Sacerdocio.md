---
layout: post
title: "Pueblo_Escogido_Real_Sacerdocio"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Somos un pueblo escogido por el Señor, no por nuestras obras sino por la infinita misericordia de Dios, por eso de lo insignificante nos ha hecho reyes y sacerdotes.

| Exodo 19:5-6 | Ahora, pues, si diereis oído a mi voz, y guardareis mi pacto, vosotros seréis mi especial tesoro sobre todos los pueblos; porque mía es toda la tierra.   Y vosotros me seréis un reino de sacerdotes, y gente santa. Estas son las palabras que dirás a los hijos de Israel. |
|Deuteronomio 7:6-10|Porque tú eres pueblo santo para Jehová tu Dios; Jehová tu Dios te ha escogido para serle un pueblo especial, más que todos los pueblos que están sobre la tierra.  No por ser vosotros más que todos los pueblos os ha querido Jehová y os ha escogido, pues vosotros erais el más insignificante de todos los pueblos; sino por cuanto Jehová os amó, y quiso guardar el juramento que juró a vuestros padres, os ha sacado Jehová con mano poderosa, y os ha rescatado de servidumbre, de la mano de Faraón rey de Egipto.   Conoce, pues, que Jehová tu Dios es Dios, Dios fiel, que guarda el pacto y la misericordia a los que le aman y guardan sus mandamientos, hasta mil generaciones;  y que da el pago en persona al que le aborrece, destruyéndolo; y no se demora con el que le odia, en persona le dará el pago.  Guarda, por tanto, los mandamientos, estatutos y decretos que yo te mando hoy que cumplas.  |
|1 Pedro 2:9|Mas vosotros sois linaje escogido, real sacerdocio, nación santa, pueblo adquirido por Dios, para que anunciéis las virtudes de aquel que os llamó de las tinieblas a su luz admirable;  vosotros que en otro tiempo no erais pueblo, pero que ahora sois pueblo de Dios; que en otro tiempo no habíais alcanzado misericordia, pero ahora habéis alcanzado misericordia.  |
|Deuteronomio 4:20| Pero a vosotros Jehová os tomó, y os ha sacado del horno de hierro, de Egipto, para que seáis el pueblo de su heredad como en este día. |

Parte de un estudio de Luis.  Él y Beatriz se congregan en la iglesia Bethesda y se dedican a predicar el evangelio y a ventas ambulantes.    El Señor ha dado sanidad a varias personas por las que Luis ha orado.  Pueden ubicarse en Bogotá en el puente de la Avenida 26 con Carrera 68.
