---
layout: post
title: Altar_de_Josue
author: vtamara
categories: 
image: assets/images/home.jpg
tags: 

---
Descubierto por Adam Zertal{3} en 1982 en el monte Ebal (940mt s.n.m).

Referenciado en Deut.27:4-8 y Josué 8:30-35 {4}

Moisés profetizó que sería construido, Josué lo construyó después de la victoria sobre los de Hai, que a su vez ocurrió después de cruzar el Jordán --sobrenatural y análogo al cruce del Mar Rojo.

Moisés ordenó maldecir y bendecir sobre ese monte, como Josué hizo, así como escribir la ley.

|Vista aerea de los montes Gerizim y Ebal.  |
|![](/assets/images/bible-archeology-altar-of-joshua-two-gerizims-ebal.jpg)|
|Foto de [https://www.bible.ca/archeology/bible-archeology-altar-of-joshua.htm](https://www.bible.ca/archeology/bible-archeology-altar-of-joshua.htm) |

Entre los dos montes está ubicada la actual Nablus y antigua Siquem.  Abraham acampó en Siquem donde Dios se le apareció  mientras era habitada por cananeos Gen 12:6-7. Jacob acampó allí, sanó y salvó, en Siquem y compró una parcela a los hijos de Hamor --descrito como padre de Siquem-- Gen. 33:18-20. El monte Gerizim era el de las bendiciones, mientras que el Ebal el de las maldiciones  (Deut. 27-28). Los huesos de José fueron enterrados en Siquem en la parcela comprada por Jacob (Josue 24:32). {7}

|Vista aérea de los restos encontrados por el arqueologo Zertal en 1982 tras búsqueda sistemática desde 1980.  |
|![](/assets/images/ihm_-_-_.jpeg)|
|Foto de [https://en.wikipedia.org/wiki/Mount_Ebal_site](https://en.wikipedia.org/wiki/Mount_Ebal_site "https://en.wikipedia.org/wiki/Mount_Ebal_site")|

Alrededor del sitio se encontraron restos de  alfarería israelita datada entre los siglos 13aC y 12aC.  Está en tierra de Samaria --el monte Gerizim es lugar de culto para los samaritanos según {9}-- no hay otras estructuras similares alrededor.

|![](/assets/images/fallow_deer_arp.jpg)  |
|Foto de vendados de dominio público disponible en [http://en.wikipedia.org/wiki/File:Fallow_deer_ar_.jpg](http://en.wikipedia.org/wiki/File:Fallow_deer_ar_.jpg) |

Zertal también encontró cenizas y huesos de animales, los cuales tras analisis resultaron ser de cabras, ovejas, vacas y ciervos (que antiguamente ocupaban esa zona) varones de 1 año aproximadamente, tal como era ordenado en Levitico 1:1-3.

|Diagrama del altar según Zertal.|
|![](/assets/images/bible-archeology-altar-of-joshua-drawing.jpg)|

Tendría una rampa en lugar de escaleras por ley Exodo 2:23.  Por su ubicación, datación y naturaleza ha sido identificado por Zertal y otros como altar Israelita.

|Otra estructura debajo del altar cuadrado|
|![](/assets/images/bible-archeology-altar-of-joshua-drawing-with-stones-xray.jpg)|
|Imagen de <https://www.bible.ca/archeology/bible-archeology-altar-of-joshua.htm>|

Zertal describe otra estructura debajo de este altar que también tenía huesos de los mismos animales y que dató en el 1300aC. En {10} se propone que el altar rectangular es del 1250aC (epoca de Debora en  jueces 4) mientras que el que esta debajo sería circular del 1400aC como otro altar circular descubierto en Gilgal.

|Manualidad típica de  Egipto {2}.|
|![](/assets/images/bible-archeology-altar-of-joshua-scarabs-cartouche-tuthmose-iii.jpg)|

Al parecer en relación con la estructura antigua, también fueron encontrada en este sitio manualidades típicas de Egipto, se han relacionado con los faraone Tutmosis III (144 aC) y Ramses II según {10}.

Diversos historiadores y arqueologos (como Israel Finkelstein) han puesto en duda la conquista de Canaan como la describe la Biblia, sin embargo han tenido que aceptar que los restos encontrados por Zertal corresponden a un sitio de culto.  Aunque no lo aceptan debe ser un lugar de culto israelita, pues los huesos corresponden a animales puros en la tradición mosaica (Deut 14:4-5) y la alfarería es típica de los hebreos de la época.  Otros pueblos cercanos en espacio y tiempo empleaban otros animales (como cerdos en el caso de filisteos y caballos que no podían ser sacrificados en tradición judia), incluso en una estela fenicia (estela Incirni) parece haber descripción de sacrificios de recien nacidos a sus dioses {11} tal como los describe la Biblia.

También se han venido encontrando pruebas de los asentamientos israelitas en esta región datados antes del 1100aC.   Por ejemplo a lo largo del valle del Jordan ya se han encontrado 5 asentamientos en forma de pie humano que se han asociado a asentamientos israelitas.

| Asentamiento en forma de pie descubierto en Abr.2009 |
| ![](/assets/images/13357_web.jpg)|
| Imagen de [https://www.eurekalert.org/news-releases/845400](https://www.eurekalert.org/news-releases/845400 "https://www.eurekalert.org/news-releases/845400") |

Con respecto a los asentamientos descubiertos desde 2002 el mismo Zertal dice que el 'pie' tiene gran significado como simbolo de posesión de un territorio, control sobre el enemibo, conección de la gente con la tierra y presencia de la Deidad {12}.



!REFERENCIAS

* {2} http://members.bib-arch.org/publication.asp?PubID=BSBA&Volume=11&Issue=1&ArticleID=15
* {3} http://research.haifa.ac.il/\~archlgy/staff/cv_zertal.html
* {4} http://www.oldhamwoodschurch.com/doc_files/stones_of_israel.doc
* {5} http://ebal.haifa.ac.il/
* {6} Everythin history of the Bible Book. http://books.google.com.co/books?id=rWO-UjdH0PEC&pg=PA95&lpg=PA95&dq=mount+ebal+altar&source=bl&ots=SievBAK171&sig=FK6Ll1iai_6dJ0meqygPVXMmCvw&hl=es&ei=KF9VSpCABIjyMbqB6dAC&sa=X&oi=book_result&ct=result&resnum=6
* {7} http://scriptures.lds.org/en/biblephotos/17
* {8} http://ebal.haifa.ac.il/ebal01.html
* {9} http://en.wikipedia.org/wiki/Mount_Ebal
* {10} http://www.bible.ca/archeology/bible-archeology-altar-of-joshua.htm
* {11} http://www.humnet.ucla.edu/humnet/nelc/stelasite/zuck.html
* {12} http://www.newswise.com/articles/exceptional-archaeological-foot-discovery-in-jordan-valley