---
layout: post
title: "El_Fruto_que_da_Fruto"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

¿Qué parabola de Cristo nos motiva a dar fruto y que significa esto? Rta: El sembrador por ejemplo Lucas 8:4-15

¿Quieres dar fruto en tu vida? 

Como queremos dar fruto abundante para nuestro Señor, necesitamos primero el carácter de Cristo en nuestras vidas.  Necesitamos el fruto del Espíritu en nosotros.

Un poco extraño, pero es la consigna para recordar: para dar fruto necesitamos el Fruto del Espíritu o necesitamos el fruto que da fruto.  Veamos como obtenerlo.


##2. Texto:  Galatas 5:16- 6:4 Las obras de la carne y el fruto del Espíritu

<pre>
Digo, pues: Andad en el Espíritu, y no satisfagáis los deseos de la carne.
Porque el deseo de la carne es contra el Espíritu, y el del Espíritu es 
contra la carne; y éstos se oponen entre sí, para que no hagáis lo que 
quisiereis.
Pero si sois guiados por el Espíritu, no estáis bajo la ley. Y manifiestas 
son las obras de la carne, que son: adulterio, fornicación, inmundicia, 
lascivia, idolatría, hechicerías, enemistades, pleitos, celos, iras, 
contiendas, disensiones, herejías, envidias, homicidios, borracheras, 
orgías, y cosas semejantes a estas; acerca de las cuales os amonesto, 
como ya os lo he dicho antes, que los que practican tales cosas no 
heredarán el reino de Dios.
Mas el fruto del Espíritu es amor, gozo, paz, paciencia, benignidad, 
bondad, fe, mansedumbre, templanza; contra tales cosas no hay ley.
Pero los que son de Cristo han crucificado la carne con sus pasiones y
deseos.  Si vivimos por el Espíritu, andemos también por el Espíritu.
No nos hagamos vanagloriosos, irritándonos unos a otros, envidiándonos
unos a otros.
Hermanos, si alguno fuere sorprendido en alguna falta, vosotros que sois 
espirituales, restauradle con espíritu de mansedumbre, considerándote a ti 
mismo,  no sea que tú también seas tentado.
Sobrellevad los unos las cargas de los otros, y cumplid así la ley de Cristo.
Porque el que se cree ser algo, no siendo nada, a sí mismo se engaña.
Así que, cada uno someta a prueba su propia obra, y entonces tendrá 
motivo de gloriarse sólo respecto de sí mismo, y no en otro;
</pre>


Según {5} benignidad y bondad son similares se relacionan con misericordia, pero benignidad es una cualidad interior que cuando se manifista en el exterior se hace bondad.    Es decir Benignidad es sentir deseo de hacer el bien y bondad es actuar para hacer el bien.  Por ejemplo si voy por la calle y veo una persona con hambre y en el corazón siento deseo de ayudarle eso es benignidad, si le compro un alimento y se lo entrego eso es bondad.

Templanza es como sinónimo de dominio propio es poder dominar mis pasiones y deseos.

Mansedumbre se refiere a humildad, según {5} es la forma humilde y serena con la que reaccionamos a las fallas de los otros.

Aprendamonos entonces los componentes de ese fruto que queremos: Amor, gozo, paz, paciencia, benignidad, bondad, fe, mansedumbre y templanza.

Nos puede servir: __Primero Amor y Gozo, doble P, doble B y de una Fe, Mansedumbre y Tamplanza __

¿Queremos ese fruto multiforme para dar fruto?  Siiii##

Entonces hagamos lo que el Señor le enseñó a Pablo: no satisfacer los deseos de la carne y no hacer las obras de la carne.

##3. Análisis

Bueno entonces como le dijo el Señor en Josue 1:9, esforcemonos y seamos valientes  para no satisfacer los deseos de la carne y listo.... Aunque es necesario, no basta pues como dice Pablo no hay ni un justo, y estas luchas nadie en esta tierra puede decir que ya las tiene ganadas, aprendemos del Salmo 127:1 si Dios no edifica la casa en vano trabajan los constructores.   Vamos a trabajar en 5 frentes: FEGOA como quien recuerda Feijoa, pero sin la I y con G en lugar de J.

* Fe y gracia
* Esfuerzo personal, 
* Guerra espiritual, 
* Oración y ayuno, 
* Amor

Aseguremonos de entender primero bien cuales van a ser las obras de la carne por superar, unas  de estas no están aturdiendo más en este tiempo así que cada quien identifique contra la que más va a luchar.   Hay tres grupos grandes que se relacionan con nuestras pasiones y emociones: sexual, confianza en Dios y contra herman@s: 
* Sexual. En general el matrimonio es como un paraiso para la pareja, pero quebrantarlo o no esperar es obra de la carne:
** adulterio (&#956;&#959;&#953;&#967;&#949;&#8055;&#945; griego de {2}): relaciones de caracter sexual que rompen el matrimonio --incluso malos pensamientos como enseñó el Señor Jesús
** fornicación (&#960;&#959;&#961;&#957;&#949;&#8055;&#945;): actos sexuales inmorales antes o fuera del matrimonio. Incluye tener relaciones sexuales sin la bendición del matrimonio, prostitución, etc.
** inmundicia (&#7936;&#954;&#945;&#952;&#945;&#961;&#963;&#8055;&#945; y &#7936;&#954;&#8049;&#952;&#945;&#961;&#964;&#959;&#962;: impureza tanto física como moral o de pensamiento, por ejemplo actos con animales; masturbación, manoseos o palabras para incitar fuera del matrimonio.
** lascivia (&#7936;&#963;&#8051;&#955;&#947;&#949;&#953;&#945;):  Propensión a los deleites carnales (según {4}), esto incluye ver pornografía.
* Falta de confianza en Dios: 
** idolatría: tener falsos dioses, fetiches, dinero, moda, fama
** hechicerías: acudir a espíritus malignos
** herejías: conociendo una verdad sobre Dios negarla y proclamar lo contrario.
* Faltas contra hermanos y hermanas: 
** enemistades, pleitos, celos, iras (no controlarla), contiendas, disensiones (dividir por dividir o por intereses vanos), envidias, homicidios, borracheras, 
** orgías: así lo traduce RV1960, pero en griego es &#954;&#8182;&#956;&#959;&#962;, que segun Thayer se refiere a un procesión nocturna de semi-borrachos que tras banquetes salian a las calles con antorcha y música en honor a Baco (deidad griega del vino) o de alguna otra deidad, y cantaba y tocaban frente a casas de sus amigos y amigas; así que se usaba generalmente para fiestas de alcoholo y fiestas hata tarde en la noche indulgentes en revelry
** y las completadas en Efesios 5:4 ni palabras deshonestas, ni necedades, ni truhanerías, que no convienen, sino antes bien acciones de gracias. 

!3.1 Fe y Gracia

Nuestro texto está enmarcado en una discusión sobre la salvación por fe, pues justamente para quienes tenemos fe en Cristo no estamos bajo la ley de Moises --aunque si debemos cumplir el resumen de esos mandamientos que ordenó Jesús (Marcos 12:28-34): amar a Dios sobre todo y al prójimo como a mi mismo.

El contexto puede resumirse en Galatas 5:5-5:6
<pre>
Pues nosotros por el Espíritu aguardamos por fe la esperanza de la justicia;
porque en Cristo Jesús ni la circuncisión vale algo, ni la incircuncisión, 
sino la fe que obra por el amor.
</pre>

que es concordante con  Galatas 6.15-16
<pre>
Porque en Cristo Jesús ni la circuncisión vale nada, ni la incircuncisión, sino una nueva creación.  Y a todos los que anden conforme a esta regla, paz y misericordia sea a ellos, 
y al Israel de Dios.
</pre>

Tenemos entoneces como la siguiente identificación:

| Gracia |   opuesta a | Ley |
| Libertad | opuesta a | Esclavitud |
| Sara | opuesta a | Agar |

¿Entendemos entonces la magnitud de la gracia de nuestro Señor con nosotros?


Veamos Lucas 7:28  
<pre>
Os digo que entre los nacidos de mujeres no hay mayor profeta que Juan el Bautista, 
pero el más pequeño en el Reino de Dios es mayor que él.
</pre>

Esa gracia  nos ha hecho herederos del Reino, mayores que los más esforzados cumplidores de la ley mosaica.   ¿Lo podemos creer?

Para aumentar la fe, además de pedirle al Señor leamos con frecuencia la Biblia, es necesario hacer devocional personal en la mañana al despertar y en la noche antes de acostarme.  

! 3.2 Esfuerzo personal

Ciertamente debemos hacer lo que está en nuestras fuerzas para alejarnos de esos desos de la carne, entre otras, como dice {2} ¿Piense en el problema que más está atacandolo en este tiempo, como puede aplicar los siguientes principios?


* Crucificar (poner a muerte, hacer morir) la carne, Gál. 5:24; Rom. 6:6; Col. 3:5.
* Negar o renunciar a los deseos mundanos, Tito 2:12.
* No proveer para los de?seos de la carne, Rom. 13:14.
* Huir de los deseos de la carne, 2 Tim. 2:22 (pasiones juveniles), 1 Cor. 6:18 (la fornicación), 1 Cor. 10:14 (la idolatría), 2 Ped. 1:4 (la corrupción a causa de la concupiscencia).



!3.3 Guerra espiritual

También es posible que en nuestras vidas existan ataduras, que hayan cadenas generacionales o que hayamos abierto puertas al enemigo.   Clamemos al Señor Jesucristo que es más Poderoso, que ya venció todo y al reconocerlo nos libera y sana.  Acostumbremonos a reprender con frecuencia, repasemos la Armadura de Efesios 6. También cuando estemos cercanos a esas pruebas reprendamos y oremos continuamente.


!3.4 Oración y ayuno.

El Señor nos quiere ayudar, pero quiere que le pidamos, que le hablemos que lo búsquemos en oración insistente, en todo momento, pero también pagando el pequeño precio del ayuno.

¿Cuando voy a hacer el próximo ayuno?  Programemos uno estas semana.

También intercedamos por otros y otras, oremos por otras personas que sabemos están luchando para superar obras de la carne.


!3.5 Amor

Debemos entonces luchar con un arma: amor como dice Efesios 5:2
<pre>
Y andad en amor, como también Cristo nos amó, y se entregó a sí mismo por nosotros, ofrenda y sacrificio a Dios en olor fragante.
</pre>

Completemos el círculo de lo pararójico, para obtener el fruto que da fruto, una de cuyas partes es el amor, debemos dar amor.

Además como la fuente del amor es el Señor, pidamosle más amor hacía Él, que lo ponga en nuestros corazones y más amor hacía esas personas con quienes estamos fallando.

##4. Conclusión y Oración

* Señor te agradecemos por la gracia de tu salvación, te rogamos nos aumentes la fe para entenderla y aceptarla, no porque nosotros seamos buenos sino porque Tú Eres Bueno.

* Señor con tu Santo Espíritu recuerdanos ser esforzados para dominarnos y superar las obras de la carne, Señor ayudanos a huir de pasiones, a no proveer para estas, a rechazarlas.

* Señor rechazamos las obras de la carne en nuestras vidas.   Por las Victoria de Cristo en la Cruz tenemos Victoria sobre estas.  Se rompen ataduras generacionales en el nombre de Jesús, se sanan nuestras heridas emocionales en el nombre de Jesús.  Rechazamos toda acta de decreto en contra de nosotros y toda maldición es cortada en el nombre de nuestro Señor Jesucristo. Declaramos libertad para seguirte Jesús, pues a Ti, uno solo puede seguirte en libertad.

* Espíritu Santo ayudanos a orar, nosotros no sabemos como, pero Tú si, ayudanos en este momento, fluye con poder, pero también cuando estemos sólos, pon palabras en nuestra boca, ponnos ansia de ayunar de llenar nuestra vida de tu preciosa palabra, de tu presencia y amor cuando ayunamos.

* Señor pone en nosotros tu amor sobrenatural, que no entendemos, que nos abarca, que es más grande que todo lo que podemos conocer y enteder.  Ese ponnos más amor por Tí, más amor por nuestros prójimos, que aprovechemos esta breve vida para amar con pasión a nuestra familia, a  nuestra comunidad de fe, a nuestros vecinos, a tu Creación porque Tu la hiciste.

* Señor recuerdanos esto FEGOA esta semana de manera especial para que superemos especialmente esta semana las obras de la carne y recuerdanos darte gracias cuanod lo hagamos y recibamos Tu Fruto que da Fruto pues es lo que anhelamos.

Si aún no ha aceptado al Señor Jesús como su salvador suficiente le invitamos a hacer una [Oración de fe].

##5. Bibliografía

* {1} Biblia Reina Valera 1960. 
* {2} Wayne Partain.  ¿Qué hacer con la carne? Sermon. http://www.waynepartain.com/Sermones/s705.html705.html
* {3} http://www.blueletterbible.org/Bible.cfm?b=Gal&c=5&v=1&t=KJV#conc/19
* {4} Diccionario RAE. http://buscon.rae.es/draeI/SrvltConsulta?TIPO_BUS=3&LEMA=fornicar
* {5}  Pastor Walter Valle.  TEMA: "Los Frutos del Espíritu Santo (Benignidad y Bondad)” Parte 4. http://bosquejoscristianos.blogspot.com/2009/01/los-frutos-del-espritu-santo-benignidad.html

----
Predica de dominio público, gracias al Señor presentada por Vladimir Támara Patiño (vtamara@pasosdeJesus.org) el 7.Ago.2011 en la Iglesia Menonita La Resurección de San Nicolas, Soacha.
