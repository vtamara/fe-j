---
layout: post
title: "NG_David_y_Salomon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En su edición de Diciembre 2010 National Geographic tiene un especial sobre David y Salomon, escrito por Robert Draper y titulado "David y Salomon: Reyes de controversia."  Está disponible en inglés en:

http://ngm.nationalgeographic.com/2010/12/david-and-solomon/draper-text

Presenta la controversia respecto a David y Salomón por parte de arqueólogos que debaten si fueron reyes de reinos organizados o lideres de un grupo de pastores.   Explica el interés político de grupos zionistas por ratificar la narrativa bíblica, pero también las reacciones "científicas" erradas.

Da por sentada la existencia de David puesta en duda por investigadores de la Universidad de Copenhagen en la decada de los 1980's.

Favorece existencia de Edom hacía el 1000aC, aunque según Amihai Mazar "Todos los investigadores que trataron con Edom en las dos últimas generaciones clamaron que Edom no existió como estado antes del siglo 8 a.C"

En el debate sobre las características del pueblo liderado por David pone como evidencias por interpretar: 
* Estructura encontrada en Jerusalen data por Eliat Mazar en el siglo 10aC  y que ella hipotetisa se trataba del Palacio del Rey David, 
* Ciudad fortificada encontrando por Yosef Garfinkel del siglo 10aC con características hebreas (ausencia de huesos de cerdo, arquitectura, ostracon con escritura al parecer hebrea --la más antigua hasta ahora encontrada), 
* Minas de cobre cercanas al siglo 10aC encontradas por Thomas Levy que según él posiblemente eran de Salomón.

Para la discusión da argumentos de:

| __No hubo Reino__ | __Hubo Reino__ |
| David Ilan del Hebrew Union College  | Eilat Mazar del Centro Shalem |
|Israel Finkelstein, Universidad de Tel Aviv | Yosef Garfinkel de la Universidad Hebrea |
| Norma Franklin Universidad de Tel Aviv |Thomas Levy de la Universidad de California, San Diego |
| | Amihai Mazar |

Y dice "La propuesta de que una sociedad compleja del siglo 10 a.C haya existido a algún lado del río Jordán ha lanzado la visión de Finkelstein a la defensiva. Sus muchos artículos en respuesta y su tono sarcástico refleja esa defensiva, y sus argumentos en ocasiones parecen un poco desesperados."

Incluye también explicitamente la postura de Finkelstein con respecto a Salomón "Ahora Salomón, pienso que yo destruí a Salomón, por así decrlo.  Lo lamento por eso!  Pero tome a Salomón, hagale una disección.  Tome la gran visita de la reina de Saba --una arabe--, trayendo toda clase de comodidades exóticas a Jerusalén.  Esta es una historia que es imposible de pensar antes del 732a.C, antes del comienzo del comercio Arabe bajo la dominación Asiria.  Tome la historia de Salomón como el gran, usted sabe, entrenador de caballos, y carros y grandes ejercitos y así.  El mundo detras de Salomón es el mundo del siglo Asirio."




