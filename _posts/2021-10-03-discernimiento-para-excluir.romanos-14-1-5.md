---
layout: post
categories:
- Prédica
title: Discernimiento para excluir. Romanos 14:1-5
author: vtamara
image: "/assets/images/294324.png"

---
# Discernimiento para excluir. Romanos 14:1-15

## Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 3.Oct.2021

# 1 Introducción

Entre diferentes iglesias cristianas hay diferencias, por ejemplo:

* Los adventistas suelen no comer carne de cerdo, de hecho varios son vegetarianos, promueven alimentación y hábitos saludables, promueven el uso de falda por parte de las mujeres y dicen que los cultos deben ser los sábados --siguiendo la tradición judía-- en lugar de los domingos.
* Los menonitas tradicionales no oran con personas que no sean de su misma iglesia y sólo emplean carros color negro.
* La gran mayoría de los Amish (que también son anabautistas como los menonitas), procuran no crear lazos con el mundo, y por eso en general no emplean electricidad.
* Hay iglesias cristianas donde pastores y/o líderes de alabanza usan tatuajes o piercings.

¿Son diferencias como estas fundamentales para excluir de la iglesia a una persona?

Recientemente el Señor me ha estado mostrando mediante el libro de Romanos sobre diferencias entre cristianos que no son fundamentales cuando se basan en una fe sincera.  Y en el mismo libro muestra otras diferencias que si son fundamentales --cuando se basan en un empeño consciente de engañar.

# 2. Texto: Romanos 14:1-15

1 Recibid al débil en la fe, pero no para contender sobre opiniones.

2 Porque uno cree que se ha de comer de todo; otro, que es débil, come legumbres.

3 El que come, no menosprecie al que no come, y el que no come, no juzgue al que come; porque Dios le ha recibido.

4 ¿Tú quién eres, que juzgas al criado ajeno? Para su propio señor está en pie, o cae; pero estará firme, porque poderoso es el Señor para hacerle estar firme.

5 Uno hace diferencia entre día y día; otro juzga iguales todos los días. Cada uno esté plenamente convencido en su propia mente.

6 El que hace caso del día, lo hace para el Señor; y el que no hace caso del día, para el Señor no lo hace. El que come, para el Señor come, porque da gracias a Dios; y el que no come, para el Señor no come, y da gracias a Dios.

7 Porque ninguno de nosotros vive para sí, y ninguno muere para sí.

8 Pues si vivimos, para el Señor vivimos; y si morimos, para el Señor morimos. Así pues, sea que vivamos, o que muramos, del Señor somos.

9 Porque Cristo para esto murió y resucitó, y volvió a vivir, para ser Señor así de los muertos como de los que viven.

10 Pero tú, ¿por qué juzgas a tu hermano? O tú también, ¿por qué menosprecias a tu hermano? Porque todos compareceremos ante el tribunal de Cristo.

11 Porque escrito está:

Vivo yo, dice el Señor, que ante mí se doblará toda rodilla,

Y toda lengua confesará a Dios.

12 De manera que cada uno de nosotros dará a Dios cuenta de sí.

13 Así que, ya no nos juzguemos más los unos a los otros, sino más bien decidid no poner tropiezo u ocasión de caer al hermano.

14 Yo sé, y confío en el Señor Jesús, que nada es inmundo en sí mismo; mas para el que piensa que algo es inmundo, para él lo es.

15 Pero si por causa de la comida tu hermano es contristado, ya no andas conforme al amor. No hagas que por la comida tuya se pierda aquel por quien Cristo murió.

# 3. Contexto.

El libro a los Romanos, es una carta que el apóstol Pablo le escribió a la iglesia en Roma que por lo visto estaba integrada por judios convertidos al cristianismo y gentiles. Se calcula que Pablo la escribió hacia el año 55 durante su tercer viaje misionero.

En esta carta Pablo explica ampliamente la salvación y justificación mediante nuestro Señor Jesucristo, pero también da enseñanzas prácticas, algunas duales y complementarias como lo que vimos del capítulo 14 con un par de versículos del capítulo 16:

> _17 Mas os ruego, hermanos, que os fijéis en los que causan divisiones y tropiezos en contra de la doctrina que vosotros habéis aprendido, y que os apartéis de ellos._
>
> _18 Porque tales personas no sirven a nuestro Señor Jesucristo, sino a sus propios vientres, y con suaves palabras y lisonjas engañan los corazones de los ingenuos._

Lo que he entendido es que necesitamos discernimiento para diferenciar entre una fe genuina de unos (aún cuando sea incorrecta) y el deseo de enseñar doctrinas falsas de otros para favorecer intereses propios.

La fe cristiana fundamental es creer en Jesús como Dios, Salvador y Señor de mi vida.

Debemos evitar que enseñanzas contrarias entren a la iglesia y por el contrario busquemos evangelizar a quienes las hayan recibido. Por ejemplo enseñanzas que niegan que Jesús es Dios (los Testigos de Jehova Jesús dicen que es un ángel y los musulmanes dicen que es un profeta, pero niegan que sea Dios, sus enseñanzas se respetan pero no se aceptan en la iglesia, requieren nuestro esfuerzo evangelístico con amor).

# 4. Análisis

| Versículos | Observación |
| --- | --- |
| 1 | En principio no contender respecto a opiniones o prácticas basadas en fe sincera (aun cuando pueda ser por ignorancia). |
| 2-3 | Particularmente entre cristianos alrededor de temas como la alimentación pero hoy también sobre piercing, tatuajes, forma de vestir, etc. |
| 4 | La fe en Cristo da salvación incluso ante algunas prácticas que no entendemos. |
| 5-6 | Tampoco es fundamental una diferencia en el día del culto (aunque para los Adventistas si lo sea). |
| 7-9 | Tengamos una práctica de vida sincera para con Dios, entendiendo que por ignorancia podríamos estar equivocados en algunos temas, pero abiertos a entenderlo de Dios y cambiar. Y así mismo esperemos esto de otros. |
| 10-13 | Cuidémonos de no juzgar (pero con tiempo pidamos discernimiento). |
| 14 | Notemos que contrista al Espíritu de Dios que vive en cada uno y no hagamos eso. Evitemos llamar algo inmundo. Oremos para hablarle con amor a quien tiene una práctica que consideramos errada respecto a la Biblia. |
| 15 | Hay temas respecto a la forma de vestir o lo que se usa, o la forma de vivir, con los que podemos alejar a los creyentes de la iglesia. Tratemoslos con amor, oración, respeto y diálogos diligentes. |

Sujetemonos a la autoridad de Cristo fundamentalmente, y después a quienes Él ha puesto en autoridad en la iglesia y en otros espacios. Si con guianza de Dios discernimos que un líder falla, tras oración digamosle en privado con amor, si no nos escucha acudamos a su líder, en el caso de la iglesia la última instancia es salirse de la iglesia. Así, cuando tengamos un ministerio (ser padre/madre o un cargo de liderazgo en la iglesia) como servidores, sin abusar, dentro de la esfera del ministerio, en amor y en rectitud frente a Cristo, podremos pedir a quienes servimos que se nos sujeten.

# 5. Conclusión y oración

Señor enseñanos más de ti, para cimentar nuestra fe en tu verdad y tu palabra y no en ideas de otros. Por favor corrígenos cuando pongamos nuestra fe en algo que no es. Que seamos sensibles a tu Santo Espíritu para no entristecerlo.

Señor por favor danos discernimiento para diferenciar entre quienes se acerquen con una fe sincera de quienes quieran engañar enseñando falsedades por intereses propios.

Señor con quienes tengan una fe genuina, pero errada, ayúdanos a ser pacientes y danos de Tu palabra que les ayude a cambiar y con tu Santo Espíritu ayúdanos a compartirles con amor.

Pedimos con acción de gracias en el nombre de Jesús.

Si aún no ha recibido a Jesús como salvador y Señor de su vida lo invito a conocerlo y a hacer una [oración de Fe](https://fe.pasosdejesus.org/Oracion_de_fe/).

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* Pablo. [https://fe.pasosdejesus.org/Pablo/](https://fe.pasosdejesus.org/Pablo/ "https://fe.pasosdejesus.org/Pablo/")

----

Duplicado en drive: <https://docs.google.com/document/d/1hXYV9WfYWDvoufYgZKIHnnTQNJuZJcZxYaaOMOl4WcQ/edit?usp=sharing>

Imagen de dominio público de [https://openclipart.org/detail/294324/students-partner-work-two-males](https://openclipart.org/detail/294324/students-partner-work-two-males)