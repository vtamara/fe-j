---
layout: post
title: "Apocalipsis"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
A lo largo de la Biblia se encuentran diversas profecias sobre el fin de este tiempo.  Hay diversas interpretaciones cristianos y otras no cristianas (e.g. de la nueva era).  El evento central es el segundo advenimiento de nuestro Señor Jesucristo. 

Veamos que dice el mismo Señor Jesucristo según 3 de sus evangelistas:


Lucas 21:25-38 

| 25 Entonces habrá señales en el sol, en la luna y en las estrellas, y en la tierra angustia de las gentes, confundidas a causa del bramido del mar y de las olas;     26 desfalleciendo los hombres por el temor y la expectación de las cosas que sobrevendrán en la tierra; porque las potencias de los cielos serán conmovidas.     27 Entonces verán al Hijo del Hombre, que vendrá en una nube con poder y gran gloria.    28 Cuando estas cosas comiencen a suceder, erguíos y levantad vuestra cabeza, porque vuestra redención está cerca.    29 También les dijo una parábola: Mirad la higuera y todos los árboles.    30 Cuando ya brotan, viéndolo, sabéis por vosotros mismos que el verano está ya cerca.     31 Así también vosotros, cuando veáis que suceden estas cosas, sabed que está cerca el reino de Dios.    32 De cierto os digo, que no pasará esta generación hasta que todo esto acontezca.    33 El cielo y la tierra pasarán, pero mis palabras no pasarán.    34 Mirad también por vosotros mismos, que vuestros corazones no se carguen de glotonería y embriaguez y de los afanes de esta vida, y venga de repente sobre vosotros aquel día.    35 Porque como un lazo vendrá sobre todos los que habitan sobre la faz de toda la tierra.    36 Velad, pues, en todo tiempo orando que seáis tenidos por dignos de escapar de todas estas cosas que vendrán, y de estar en pie delante del Hijo del Hombre.    37 Y enseñaba de día en el templo; y de noche, saliendo, se estaba en el monte que se llama de los Olivos.    38 Y todo el pueblo venía a él por la mañana, para oírle en el templo. |



Marcos 13:24-37

| 24 Pero en aquellos días, después de aquella tribulación, el sol se oscurecerá, y la luna no dará su resplandor,    25 y las estrellas caerán del cielo, y las potencias que están en los cielos serán conmovidas.     26 Entonces verán al Hijo del Hombre, que vendrá en las nubes con gran poder y gloria.    27 Y entonces enviará sus ángeles, y juntará a sus escogidos de los cuatro vientos, desde el extremo de la tierra hasta el extremo del cielo.     28 De la higuera aprended la parábola: Cuando ya su rama está tierna, y brotan las hojas, sabéis que el verano está cerca.     29 Así también vosotros, cuando veáis que suceden estas cosas, conoced que está cerca, a las puertas.     30 De cierto os digo, que no pasará esta generación hasta que todo esto acontezca.     31 El cielo y la tierra pasarán, pero mis palabras no pasarán.     32 Pero de aquel día y de la hora nadie sabe, ni aun los ángeles que están en el cielo, ni el Hijo, sino el Padre.     33 Mirad, velad y orad; porque no sabéis cuándo será el tiempo.     34 Es como el hombre que yéndose lejos, dejó su casa, y dio autoridad a sus siervos, y a cada uno su obra, y al portero mandó que velase.      35 Velad, pues, porque no sabéis cuándo vendrá el señor de la casa; si al anochecer, o a la medianoche, o al canto del gallo, o a la mañana;     36 para que cuando venga de repente, no os halle durmiendo.     37 Y lo que a vosotros digo, a todos lo digo: Velad. |


Mateo 24:29-35 


|  29 E inmediatamente después de la tribulación de aquellos días, el sol se oscurecerá, y la luna no dará su resplandor, y las estrellas caerán del cielo, y las potencias de los cielos serán conmovidas.     30 Entonces aparecerá la señal del Hijo del Hombre en el cielo; y entonces lamentarán todas las tribus de la tierra, y verán al Hijo del Hombre viniendo sobre las nubes del cielo, con poder y gran gloria.     31 Y enviará sus ángeles con gran voz de trompeta, y juntarán a sus escogidos, de los cuatro vientos, desde un extremo del cielo hasta el otro.     32 De la higuera aprended la parábola: Cuando ya su rama está tierna, y brotan las hojas, sabéis que el verano está cerca.    33 Así también vosotros, cuando veáis todas estas cosas, conoced que está cerca, a las puertas.     34 De cierto os digo, que no pasará esta generación hasta que todo esto acontezca.     35 El cielo y la tierra pasarán, pero mis palabras no pasarán. |

El otro evangelista (Juan) no dedicó una parte de su evangelio a este tema, sino un libro completo que reitera el advenimiento de Cristo y complementa otros detalles de profecias por ejemplo de Isaias y Daniel.

La fecha precisa de ese segundo advenimiento como Jesús mismo lo dijo, sólo la conoce el Padre, varias personas se han equivocado a lo largo de la historia al estimarla (incluyendo testigos de Jehova {2}, mormones {3}).  ¿Cómo entonces discernir cual información es correcta?  

# Pedirla a Dios en oración discernimiento
# Poner a prueba, por ejemplo 1 Juan 4:1-2 `Amados, no creáis a todo espíritu, sino probad los espíritus si son de Dios; porque muchos falsos profetas han salido por el mundo. En esto conoced el Espíritu de Dios: Todo espíritu que confiesa que Jesucristo ha venido en carne, es de Dios.'
# Discernir en la comunidad de fe con sinceridad y valor, pidiendo guianza del Espíritu Santo.

Una referencia por excelencia es la Biblia.    Algunos eventos que he encontrado y que pueden servir como guía de lectura son:


* Señales anteriores y tribulación. Lucas 21:7-24. Mateo 24:3-28.  Marcos 13:3-23
* Señales en el cielo. Sol, luna y estrellas.  Mateo 24:29. Marcos 13:24-25. Lucas 21:25. Isaías 13:10. Ezequiel 32:7. Joel 2:31.  Apocalipsis 6:13-14.
* Jesús llega entre nubes.
* Dios aparta a sus elegidos.  Hay otros elegidos ---tal vez a lo largo de la historia--- que ya había apartado.
* Batalla. Dios gana.
* Satanas encadenado 1000 años.   Reinado en la tierra de Cristo esos 1000 años con algunas resucitados (e.g quienes habían muerto por el testimonio).
* Satanas soltado por breve tiempo como prueba final.
* Juicio a quienes habían muerto.
* Los que estén en el libro de la vida van al Reino de Dios, los que no van al lago de fuego.



Este retorno de nuestro Salvador es un evento maravilloso y hermoso, lo anhelamos quienes lo hemos recibido como Señor y Salvador, aunque como Él mismo nos enseña debemos velar, mantenernos en oración, manteniendonos libres de pecado, pidiendole perdón por los pecados que aún nos falte declararle o los que se cometan.   

Los eventos narrados coinciden
que hayen el mejor esfuerzo por no pecar y obedecerlo, si pecamos pronto, pronto pedirle perdón y ayuda para no pecar más.  Su amor es infinito por eso nos ayuda, pero su justicia también es infinita por eso tenemos que temerle sólo a Él y obedecerle con amor. 


!REFERENCIAS

{1} Biblia. Reina Valera 1960. http://www.biblegateway.com/passage/?search=Marcos 13:24-37;&version=60;

{2} Falsas Profecías de los Testigos de Jehová. Christian Apologetics and Research Ministry. http://www.carm.org/espanol/testigos/test_profecia.htm

{3} False prophecies of Joseph Smith. Christian Apologetics and Research Ministry. http://www.carm.org/lds/js_false_prophecies.htm
