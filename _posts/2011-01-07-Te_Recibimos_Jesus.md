---
layout: post
title: "Te_Recibimos_Jesus"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
* En el corazón como Señor y salvador
* Recibimos tu amor eterno y lo correspondemos gracias a Tí.
* En cada visitación y cuando nos hablas te atendemos y obedecemos

Lamentamos el asesinato de .. El arracacho.   Hay quienes la justifican por presuntos robos de su parte, pero no nos corresponde juzgar sus errores, lo que si nos corresponde es recordar que la vida es sagrada, analizar en que contexto ocurrió este asesinato y ubicarnos en perspectiva de protección por parte de nuestro Señor.


Paramilitarismo en Bogotá

* 31.May.2008 Alerta temprana de la defensoría "Un factor que agudiza el riesgo para la población civil es la existencia de contradicciones entre las propias estructuras paramilitares y las disputas entre grupos y facciones rivales por detentar y/o ampliar el poder, el reacomodamiento de cuadros medios y bajos, la lucha por los territorios y corredores estratégicos, por las fuentes de financiación y en los ajustes de cuentas que provocan enfrentamientos entre grupos grandes y pequeños" http://www.nuevoarcoiris.org.co/sac/?q=node/443
* Jun.2008 Informe de la Personería. Debate en el concejo de Bogotá. “Los grupos paramilitares hacen presencia en Bogotá de variadas formas”, indica un informe de la Personería que dio a conocer el viernes el concejal del Polo Democrático Jaime Caycedo Turriago. Esta realidad contradice una vez más “los pronunciamientos oficiales sobre la inexistencia de estos grupos ilegales en la ciudad”, dice el estudio. http://www.elespectador.com/impreso/articuloimpreso-herencia-de-auc-bogota
* Sep.2008 "El senador Héctor Helí Rojas, del Partido Liberal, denunció la presencia de paramilitares en barrios de Bogotá y pidió al Alcalde, Samuel Moreno, que encare el debate en el Congreso, sobre la seguridad de la ciudad. (Vea el video de sus declaraciones)" http://www.semana.com/wf_InfoArticulo.aspx?IdArt=115377
* Jun.2009 Debate en el concejo de Bogotá.  "Este es un nuevo paramilitarismo politizado y sin mayores vínculos con el tejido social que ataca y amenaza a quienes se animan a denunciar abusos y a exigir verdades dentro del proceso de Justicia y Paz", dijo el concejal Carlos Vicente de Roux... En el debate los concejales dieron como ejemplo, el caso específico de las "Águilas Negras", un grupo emergente de paramilitares que hacen presencia en varias localidades, dónde imponen horarios para la circulación de la gente y reclutan a jóvenes para engrosar sus filas.  http://www.nuevoarcoiris.org.co/sac/?q=node/443
