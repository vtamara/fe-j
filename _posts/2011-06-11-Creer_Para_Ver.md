---
layout: post
title: "Creer_Para_Ver"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!1. Introducción

La envidia no nos permite aumentar la fe correcta, erradiquemosla de nuestra vida y con extrema urgencia en nuestras relaciones con cristianos y cristianas.

En humildad el Espíritu Santo nos puede revelar la fe correcta de muchas formas por su multiforme gracia.  Rompamos ligaduras de orgullo en el nombre del Señor Jesucristo.

Con fe podremos ver las obras poderosas del Señor en nuestras vidas.  Así que en lugar de "Ver para Creer" hoy intentemos "Creer para Ver," claro creer en lo correcto, pues hay muchas cosas que se pueden creer pero no todo es cierto.


!2. Pasaje

<pre>
1 Salió Jesús de allí y vino a su tierra, y le seguían sus discípulos.
2 Y llegado el día de reposo, comenzó a enseñar en la sinagoga; y muchos, oyéndole, 
se admiraban, y decían: ¿De dónde tiene éste estas cosas? ¿Y qué sabiduría es esta 
que le es dada, y estos milagros que por sus manos son hechos?
3 ¿No es éste el carpintero, hijo de María, hermano de Jacobo, de José, de Judas y de 
Simón? ¿No están también aquí con nosotros sus hermanas? Y se escandalizaban de él.
4 Mas Jesús les decía: No hay profeta sin honra sino en su propia tierra, y entre sus parientes, 
y en su casa.
5 Y no pudo hacer allí ningún milagro, salvo que sanó a unos pocos enfermos, poniendo sobre
 ellos las manos.
6 Y estaba asombrado de la incredulidad de ellos. Y recorría las aldeas de alrededor, enseñando.
</pre>

!3. Contexto

El capítulo anterior describe enseñanzas del Señor, liberación y milagros como la sanidad de la mujer que llevaba 12 años con un flujo de sangre y la resurrección de la hija de Jairo quien tenía 12 años.

Seguramente la fama del Señor era grande, ¿como podía ignorarse una resurrección?   Incluso en el pasaje sus coterraneos hablan de "estos milagros que por sus manos son hechos."

El pasaje termina indicando que Jesús se fue a enseñar a otras aldeas, y el siguiente pasaje describe el envío de sus discíulos quienes dependían plenamente de Dios y expulsaban demonios y ungían con aceite y Dios daba sanidad por intermedio de ellos.

Sus discípulos tenían fe y por eso fueron a ponerla en práctica, por el contrario sus escandalizados coterraneos no tuvieron fe.   Sus corazones endurecidos no permitió crecer la semilla que el Señor sembró con su enseñanza.

¿Hoy cómo quien queremos ser?  Como los coterraneos sin fe y que hoy recordamos tristemente por haber rechazado al Rey de Reyes, o como los discípulos y los que les siguieron con fe, quienes gracias a Dios hicieron obras poderosas y siguiendo la enseñanza de nuestro Maestro, enseñaron y transmitieron su palabra que hoy estamos recibiendo?

La fe de esos discípulos seguramente se fortaleció viendo los milagros que hacía el Señor, pero también escuchando su enseñanza y obedeciéndole cuando los envió a predicar.  

<pre>
   Aprender                        Predicar/Enseñar
   Milagros ->           Fe     -> Milagros
   Obedecer
</pre>

!4. Análisis

!4.1 Sin envídia

En los versículos 2 y 3 veo envidia de parte de sus coterraneos, como lo habían conocido desde su infancia, lo habían visto crecer entre ellos y sus hijos no podían entender como Dios le había dado poder y sabiduría a Él pero a ellos no.

Es natural no entender, finalmente sólo Dios lo sabe todo, lo que es extraño es supeditar la fe a la capacidad de entender.   Ciertamente la fe correcta no está en contravía de la razón correcta pues ambas se fundamentan en la verdad que es una: Cristo como se afirma en Juan 14:6
<pre>
 Jesús le dijo: Yo soy el camino, y la verdad, y la vida; nadie viene al Padre, sino por mí.
</pre>
Sin embargo también es cierto que la fe es más abarcante y llega más lejos de lo que llega la razón. 

¿Entonces porque querer que la capacidad de entender sea lo que designe que es cierto de lo que no se puede entender?  En mi humilde opinión es la vieja treta de Satanas expuesta en Genesis 3:5, decir que se puede ser como Dios sabiendo más.  Quien cree que su razón es la que domina todo en esa trampa cae.

Entonces esos coterraneos aún cuando veían se negaban a creer.  Su necesidad de ser más, no les permitió reconocer al Mesias.  La envidia los cegó.

Entonces hoy esperemos que de nuestros conocidos, de los más pequeños y humildes entonces surgan poderosos hijos de Dios, no nos extrañemos cuando ocurra y apoyemoslos sin envídia, para que mañana podamos ver y reconocer al Rey. 

Pidamos al Señor que nos haga humildes para reconocer que no sabemos todo, y para poder aceptar cuando el Señor de algo especial a una persona conocida de manera que cuando eso ocurra glorifiquemos el nombre del Señor en lugar de abatirnos y errar.

!4.2 Con la fe correcta

Hay otro aspecto por resaltar en el versículo 4 se habla de 4 hermanos de Jesús y de hermanas.   

Marcos es un evangelio escrito originalmente en griego, entre los manuscritos más antiguos que se conocen están:
| __Manuscrito__ | __Siglo__ |
| 1 - Sinaiticus, |  IV  |
| 2 - Alejandrino|  V |
| 3 - Vaticano|  IV |
| P45 | III |

En todos estos dice en griego  "&#945;&#948;&#949;&#955;&#966;&#959;&#962;" (ver {2}) que traduce hermanos, así que cabe preguntarse si la virgen María tuvo más hijos después de Jesús, a raíz de este versículo.   

Aunque no hay parte alguna en la Biblia donde diga que María, la madre de nuestro Señor Jesuscristo, se mantuvo virgen después de dar a luz al Señor (ver {4}),  por tradiciones católicas, esa fe defiende con diversos argumentos que no tuvo más hijos y dan interpretaciones acordes a eso para este versículo y su palabra griega "&#945;&#948;&#949;&#955;&#966;&#959;&#962;".  

Aunque pueden tenerse en cuenta diversos elementos, uno más  es el osuario de Santiago:

[http://o.aolcdn.com/photo-hub/news_gallery/6/8/688822/1286231226318.JPEG]

se trata de un osuario del siglo I encontrado  que dice "Santiago hijo de José, hermano de Jesús."    

Oled Golam un coleccionista de antiguedades Israeli que lo dio a conocer está en juicio en su país desde 2004 por la supuesta falsificación de la inscripción de este elemento (pues se reconoce como probado que el Osuario es del siglo I).  Sin embargo sus acusadores de la Autoridad Arqueológica de Israel no han logrado dar pruebas convincentes, pues entre otras pruebas de la defensa, no se conoce procedimiento humano para crear la patina que cubre la inscripción, y por declaraciones recientes del juez del caso (ver {3}) parece que fallará a favor de Golam.

Oramos entonces para que salga la verdad en ese caso pronto (pues ya lleva 7 años de juicio) y también respecto a nuestra fe.

Aun si esa pieza arqueológica resulta auténtica, podría no referirse a nuestro Señor Jesús, pero parece probable que si se refiera a Él. Con respecto a la palabra hermano puede que se use en sentido metafórico, pero por la tradición en Palestina de la epoca es bien probable que fuera un Santiago hermano de sangre de un Jesús.

Ciertamente la fe no está limitada por la razón, pero tampoco se contradicen cuando son correctas.  Probemos nuestra fe, como decía Pablo,  no para armar guerras  con católicos, pero para que el Espíritu Santo ilumine nuestra razón desde la humildad y nos de discernimiento de la  fe correcta.


!5. Conclusión y Oración

Oremos pidiendo al Señor que nos revele la fe correcta por su Santo Espíritu, que nos de discernimiento para rechazar lo falso y valor para quedarnos con lo verdadero y bueno.

Oremos pidiendo humildad para que no caigamos en trampas del orgullo que lleguen a cegar nuestra razón.

Que nos aumente la fe correcta para realizar las obras que Jesús hizo como dice su palabría que ocurriría a quienes crean.

Que levante siervos y siervas con fe y poder en nuestras comunidades, que los veamos crecer con amor y los y las ayudemos a crecer en la fe correcta.    

Rechazamos de nuestras vidas el orgullo.  En el nombre de nuestr Señor Jesucristo nos declaramos humilde como Él.

Que el Señor nos revele donde está obrando, con quien está obrando para poder trabajar allí y apoyar a quienes estén allí sin envidia, sino con el objetivo común de proclamar la verdad y anunciar el evangelio como el Señor nos ha mandado y nos ha enseñado.



!6. Bibliografía

* {1} Biblia. Reina Valera. 1960. http://www.biblegateway.com/passage/?search=Marcos+6&version=RVR1960
* {2} http://nttranscripts.uni-muenster.de/AnaServer?NTtranscripts+0+start.anv
* {3} http://www.bib-arch.org/
* {4} http://www.antesdelfin.com/maria.html
