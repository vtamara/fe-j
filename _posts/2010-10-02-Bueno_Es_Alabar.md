---
layout: post
title: "Bueno_Es_Alabar"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
//Bueno es alabar oh Señor,
Tu Nombre.
Darte gloria honra y honor,
por siempre.
Bueno es alabarte Jesús 
y gozarme en tu Poder//

//Porque grande eres Tu,
grandes son tus obras.
Porque grande eres Tu,
grande es tu amor, 
grande es tu gloria//

//Bueno es alabar oh Señor,
Tu Nombre.
Darte gloria honra y honor,
por siempre.
Bueno es alabarte Jesús 
y gozarme en tu Poder//

//Porque grande eres Tu,
grandes son tus obras.
Porque grande eres Tu,
grande es tu amor, 
grande es tu gloria//

Bueno es alabarte Señor...

</pre>



* Letra: http://www.letrasymas.com/letra.php?p=danilo-montero-bueno-es-alabar
* Video de Danilo Montero interpretandola (sonido desfasado) http://www.youtube.com/watch?v=QZfOreQJTFw
* Karaoke con coros cantados: http://www.youtube.com/watch?v=bqpOONPeKJE

