---
layout: post
title: "Cierran_Investigacion_Duque_vs_Giraldo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Cierran investigación contra padre Javier Giraldo

Viernes 5 de agosto de 2011, de El Espectador

PRENSA - La justicia les dio la razón al padre Javier Giraldo y a los defensores de derechos humanos Elkin Ramírez y Miguel Afanador, y cerró la investigación que se adelantó en su contra por los delitos de injuria, calumnia y falsa denuncia.

El teniente coronel (r) Néstor Duque los denunció en 2005 por sus quejas ante la Corte Interamericana de Derechos Humanos (CIDH) y la Presidencia de la República, en las que lo señalaron por su presunta participación en varios crímenes mientras comandaba el batallón Bejarano Muñoz, de la Séptima Brigada del Ejército, con sede en el municipio de Carepa (Antioquia).

Los denunciados señalaron a Duque de haber participado en la tortura de dos campesinos de la región, que ocurrió el 22 de diciembre de 2004. El uniformado consideró que con estas afirmaciones se faltaba a la verdad, que eran imputaciones falsas y que, por ello, interpuso la denuncia.

Sin embargo, la justicia consideró que los denunciados no incurrieron en los delitos señalados debido a que sus afirmaciones no fueron hechas bajo la gravedad del juramento ni con la intención de denunciar a Duque, sino la de informar al presidente y a la Corte sobre la participación de uniformados en varios delitos ocurridos en el Urabá antioqueño.

Agregó que, además, hubo un cambio en la declaración de uno de los testigos, quien confesó que Duque lo torturó para implicar a los denunciados. Estos hechos motivaron la preclusión de la investigación.

http://www.elespectador.com/impreso/judicial/articulo-289553-cierran-investigacion-contra-padre-javier-giraldo
