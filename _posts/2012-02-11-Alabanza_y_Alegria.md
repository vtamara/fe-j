---
layout: post
title: "Alabanza_y_Alegria"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##INTRODUCCIÓN

Deseo preguntar ¿quiénes hicieron el ejercicio de alabar al Señor a solas?   Lo pregunto con una explicación, anhelamos que más personas participen en la alabanza y adoración durante el servicio, pero eso vendrá siendo fruto de un esfuerzo personal y del Santo Espíritu que nos guie y a quien oramos pidiendo para que nos ayude a alabar más y mejor.

Hay quienes sugieren Dios nos creó con el propósito de alabarlo (ver por ejemplo  {4}) pues eso pueden entenderse de Efesios 1:3-6
<pre>
 3 Bendito sea el Dios y Padre de nuestro Señor Jesucristo, 
   que nos bendijo con toda bendición espiritual en los lugares celestiales en Cristo,
 4 según nos escogió en él antes de la fundación del mundo, 
    para que fuésemos santos y sin mancha delante de él,
 5 en amor habiéndonos predestinado para ser adoptados hijos suyos
    por medio de Jesucristo, según el puro afecto de su voluntad,
 6 para alabanza de la gloria de su gracia, con la cual nos hizo aceptos en el Amado,
</pre>

No se trata de egoismo o presunción de Dios.  Él simplemente Es como lo dice Ex 3:14.  Dios no necesita aparentar ni presumir.

Personalmente he experimentado que Dios me creo por amor y me ha hecho libre y en esa libertad me he enamorado de Él y por eso lo alabo.   A las personas que alaban al Señor (por ejemplo a los niños del ministerio de danza) que les he preguntado coincidimos en sentir felicidad y gozo, por su santo Espíritu nos unimos más a Él durante la alabanza y en esa unión nos sentimos completos y creo que Dios también se complace en nosotros  y hasta nos honra.

Uno puede preguntarse si acaso en el cielo se alaba con cantos al Señor y según Apocalipsis 5:9 así es:
<pre>
9 y cantaban un nuevo cántico, diciendo: Digno eres de tomar el libro y de abrir sus sellos; porque tú fuiste inmolado, y con tu sangre nos has redimido para Dios, de todo linaje y lengua y pueblo y nación;
10 y nos has hecho para nuestro Dios reyes y sacerdotes, y reinaremos sobre la tierra.
</pre>

Entonces la alabanza es demasiado importante, de hecho es continúa en los cielos (y ojala lo sea aquí en la tierra) como dice Apocalipsis 4:8 "y no esaban día y noche de decir: Santo, santo, santo es el Señor Dios Todopoderoso, el que era, el que es y que ha de venir"

De hecho la alabanza continúa es la invitación que nos hace el Salmo 34:
<pre>
1 Bendeciré a Jehová en todo tiempo; 
    Su alabanza estará de continuo en mi boca. 
2 En Jehová se gloriará mi alma; 
    Lo oirán los mansos, y se alegrarán.
3 Engrandeced a Jehová conmigo, 
    Y exaltemos a una su nombre.
4 Busqué a Jehová, y él me oyó, 
    Y me libró de todos mis temores.
</pre>

Y es que cualquier oportunidad es buena para alabar al Señor:

!Para dar gracias

Isaias 12:1-6
<pre>
1 En aquel día dirás: Cantaré a ti, oh Jehová; pues aunque te enojaste
   contra mí, tu indignación se apartó, y me has consolado.
2 He aquí Dios es salvación mía; me aseguraré y no temeré; 
   porque mi fortaleza y mi canción es JAH Jehová, quien ha sido salvación para mí.
3 Sacaréis con gozo aguas de las fuentes de la salvación.
4 Y diréis en aquel día: Cantad a Jehová, aclamad su nombre, haced 
   célebres en los pueblos sus obras, recordad que su nombre es engrandecido.
5 Cantad salmos a Jehová, porque ha hecho cosas magníficas; 
   sea sabido esto por toda la tierra.
6 Regocíjate y canta, oh moradora de Sion; porque grande es en 
   medio de ti el Santo de Israel.
</pre>


!Para expresar nuestra alegría

Santiago 5:13
<pre>
¿Está alguno entre vosotros afligido? Haga oración. 
¿Está alguno alegre? Cante alabanzas.
</pre>


!En medio de la adversidad pues el Señor libra

Este principio lo ejemplifica Hechos 16 (como lo resalta {3}):
<pre>
    23 Después de haberles azotado mucho, los echaron en la cárcel, mandando al carcelero que los guardase con seguridad.
    24 El cual, recibido este mandato, los metió en el calabozo de más adentro, y les aseguró los pies en el cepo.
    25 Pero a medianoche, orando Pablo y Silas, cantaban himnos a Dios; y los presos los oían.
    26 Entonces sobrevino de repente un gran terremoto, de tal manera que los cimientos de la cárcel se sacudían; y al instante se abrieron todas las puertas, y las cadenas de todos se soltaron.
</pre>

Y con nuevos cánticos:
Apo 5:9

Salmo 40

Trompetas
Apo 8:6
 6 Y los siete ángeles que tenían las siete trompetas se dispusieron a tocarlas.

ap 15

Numeros 10
Jerico -> para destruir fortalezas del enemigo.



##Bibliografía

* {1} RV1960
* {2} Blue Letter Bible.http://www.blueletterbible.org/Bible.cfm?b=Psa&c=34&v=1&t=KJV#conc/1
* {3} Hay Liberacion En La Alabanza. Pablo Roman. 2011. http://cristianobook.com/profiles/blogs/hay-liberacion-en-la-alabanza
* {4} La Alabanza como Llave para Liberar el Poder Milagroso de Yahweh. José Levi Machado. 2006. http://www.adoradores.com/node/53
* {5} Alabanza y Adoración. http://www.escuelasbiblicas.org/material/109-AlabanzaYAdoracion.pdf
* {6} Alabando al Señor en todo tiempo. http://www.restorationnations.com/es/mostrar-articulos.asp?artID=57&pageSz=1045
