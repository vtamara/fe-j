---
layout: post
title: "CaracteristicasConciencia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!Encuesta 1.

Aplicada verbalmente el 24.Jul.2010 a grupo de jóvenes que se preparan para objetar servicio militar por razones de conciencia de la Iglesia Menonita de Teusaquillo

|Pregunta:| Si | No | No responde |
|¿Controla su conciencia? | 0 | 3 | 0 |

!Encuesta 2

Aplicada el 29.Jul.2010 en Iglesia Cristiana Menonita de Teusaquillo a miembros de la iglesia durante consulta sobre participación de la iglesia en proyecto de ley sobre Objeción de Conciencia

| Pregunta | Si | No | No responde |
| ¿Alguna vez he sentido a mi conciencia indicandome que algo que hacía estaba mal? | 11 | 0 | 1 |
| ¿Alguna vez cuando iba a hacer algo que estaba mal supe que estaba mal sin que otra persona me lo dijera? | 10 | 0 | 2 |
| ¿Alguna vez sin que fuera mi voluntad, me senti mal por algo incorrecto que hice hasta que corregí? | 12 | 0 | 0 |
| ¿Alguna vez por mi conciencia evité hacer algo placentero o lucrativo? | 12 | 0 | 0 |
| ¿Hay actos que encuentro malos aunque sean legales? | 12 | 0 | 0 |
