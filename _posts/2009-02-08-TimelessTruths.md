---
layout: post
title: "TimelessTruths"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Se trata de una librería en línea en inglés, cuyos contenidos en su mayoría son de dominio público.

Tiene un hermoso, simple y funcional diseño para publicar:
* La biblia (Versión del rey James KJV)
* Textos
** 2 publicaciones eriadas: Foundation Truth, Treasures of the Kingdom
** Archivo de otras publicaciones seriadas: Dear Princess
** Libros
** Sermones
** Artículos
** Tratados
* Música
