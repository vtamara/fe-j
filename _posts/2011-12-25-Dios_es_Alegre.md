---
layout: post
title: "Dios_es_Alegre"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Aline Barros

Aline Barros

<pre>
CORO

Nuestro Dios es Dios alegre, mi Dios es alegre,
Sofonías 3:17 me dice que El se regocija en mi.
Nuestro Dios es Dios alegre Oh, mi Dios es alegre Oh,
Sofonías 3:17 me dice que El se regocija en mi.

INTRO

El consuela al que llora, su ayuda no demora,
El abraza al rechazado y lo trae a su lado
para dar su amor, El levanta al abatido
restaura el alma del herido, El exalta el loor
del pueblo que busca conocer al Señor//

(CORO)
(INTRO)
//(CORO)//

Dios es alegre (eco) cantemos con gozo (eco)
Dios es alegre (eco) con gritos de júbilo.

Nuestro Dios es Dios alegre, mi Dios es alegre,
Sofonías 3:17 me dice que El se regocija en mi
Nuestro Dios es Dios alegre Oh, mi Dios es alegre Oh,
//Sofonías 3:17 me dice que El se regocija en mi.//
</pre>

* Video: http://www.youtube.com/watch?v=QYZuIRXyzjQ
* Letra basada en: http://www.musica.com/letras.asp?letra=1144632
