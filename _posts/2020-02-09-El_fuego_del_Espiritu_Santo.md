---
title: El fuego del Espíritu Santo
author: vtamara
categories:
- Prédica
image: "/assets/images/fogueira-em-acampamento.jpg"

---
# El fuego del Espíritu Santo

### Comunidad Menonita de Suba, Caminos de Esperanza.

# 1. INTRODUCCIÓN

Estamos viendo una serie de prédicas sobre el Fuego del Espíritu Santo y una de las enseñanzas clave del pastor Jaime Ramírez en este tema es que intentemos lo que otros hacen con Espíritu Santo. Repasemos hoy algo que hicieron los primeros discípulos e intentemos hacer lo mismo.

Antes de entrar al texto deseo introducir que los sucesos ocurrieron hacia el año 33dC en el templo de Jerusalén, algunos días después de la resurrección de Jesús y de que los discípulos habían recibido al Espíritu Santo que Jesús había prometido. Juan y Pedro fueron al templo, y por intermedio de ellos Dios le dio sanidad a un cojo de nacimiento que pedía en una de las puertas del templo. Gran señal mediante el don de sanidad.

![Pedro sana a un cojo](https://www.jhonnyquinde.com/wp-content/uploads/2015/05/pedro-sana-un-cojo.jpg)
Imagen de [https://www.jhonnyquinde.com/wp-content/uploads/2015/05/pedro-sana-un-cojo.jpg](https://www.jhonnyquinde.com/wp-content/uploads/2015/05/pedro-sana-un-cojo.jpg)

# 2. TEXTO: Hechos 3:11-26

Discurso de Pedro en el Pórtico de Salomón

    11 Y teniendo asidos a Pedro y a Juan el cojo que había sido sanado, 
       todo el pueblo, atónito, concurrió a ellos al pórtico que se llama de
       Salomón.
    12 Viendo esto Pedro, respondió al pueblo: Varones israelitas, ¿por qué
       os maravilláis de esto? ¿o por qué ponéis los ojos en nosotros, como
       si por nuestro poder o piedad hubiésemos hecho andar a éste?
    13 El Dios de Abraham, de Isaac y de Jacob, el Dios de nuestros padres, 
       ha glorificado a su Hijo Jesús, a quien vosotros entregasteis y 
       negasteis delante de Pilato, cuando éste había resuelto ponerle en 
       libertad.
    14 Mas vosotros negasteis al Santo y al Justo, y pedisteis que se os 
       diese un homicida,
    15 y matasteis al Autor de la vida, a quien Dios ha resucitado de los 
       muertos, de lo cual nosotros somos testigos.
    16 Y por la fe en su nombre, a éste, que vosotros veis y conocéis, le ha 
       confirmado su nombre; y la fe que es por él ha dado a éste esta 
       completa sanidad en presencia de todos vosotros.
    17 Mas ahora, hermanos, sé que por ignorancia lo habéis hecho, como 
       también vuestros gobernantes.
    18 Pero Dios ha cumplido así lo que había antes anunciado por boca de 
       todos sus profetas, que su Cristo había de padecer.
    19 Así que, arrepentíos y convertíos, para que sean borrados vuestros
       pecados; para que vengan de la presencia del Señor tiempos de 
       refrigerio,
    20 y él envíe a Jesucristo, que os fue antes anunciado;
    21 a quien de cierto es necesario que el cielo reciba hasta los tiempos
       de la restauración de todas las cosas, de que habló Dios por boca de 
       sus santos profetas que han sido desde tiempo antiguo.
    22 Porque Moisés dijo a los padres: El Señor vuestro Dios os levantará
       profeta de entre vuestros hermanos, como a mí; a él oiréis en todas 
       las cosas que os hable;
    23 y toda alma que no oiga a aquel profeta, será desarraigada del 
       pueblo.
    24 Y todos los profetas desde Samuel en adelante, cuantos han hablado,
       también han anunciado estos días.
    25 Vosotros sois los hijos de los profetas, y del pacto que Dios hizo 
       con nuestros padres, diciendo a Abraham: En tu simiente serán 
       benditas todas las familias de la tierra.
    26 A vosotros primeramente, Dios, habiendo levantado a su Hijo, lo envió
       para que os bendijese, a fin de que cada uno se convierta de su 
       maldad.

# 3. CONTEXTO

Los versículos anteriores y siguientes a este son:

| Versículos | Tema según RV1960 | Acción por parte de discípulos con Espíritu Santo |
| --- | --- | --- |
| 2:1-13 | Venida del Espíritu Santo | Bautismo |
| 2:14-42 | Primer discurso de Pedro | Compartir buena nueva |
| 2:43-47 | Vida de los primeros cristianos | Cambiar forma de vivir |
| 3:1-10 | Curación de un cojo | Prodigios, señales y milagros |
| 3:11-26 | Discurso de Pedro en el Pórtico de Salomón | Compartir buenas nuevas |
| 4:1-22 | Pedro y Juan ante el concilio | Defender la posibilidad de compartir buenas nuevas |
| 4:23-31 | Los creyentes piden confianza y valor | Orar |
| 4:32-37 | Todas las cosas en común | Cambiar forma de vivir |

Varias acciones con el Espíritu Santo, pueden también verse en la vida del Señor Jesucristo (excepto Cambiar forma de vivir porque es Santo y sin Pecado) y en otras partes que nos motivan a cambiar forma de vivir para obeder mejor a Jesús:

| Acción con Espíritu Santo | Versículo |
| --- | --- |
| Bautismo | Lucas 3:22. y descendió el Espíritu Santo sobre él en forma corporal, como paloma, y vino una voz del cielo que decía: Tú eres mi Hijo amado; en ti tengo complacencia. |
| Llenura (y fruto) | Lucas 4:1. Jesús, lleno del Espíritu Santo, volvió del Jordán, y fue llevado por el Espíritu al desierto. Galatas 5:22-23 Más el fruto del Espíritu es Amor, Gozo, Paz, Paciencia, Benignidad, Bondad, Fe, Mansedumbre y Templanza; contra tales cosas no hay Ley. |
| Poder y dones | Lucas 4:14-15. Y Jesús volvió en el poder del Espíritu a Galilea, y se difundió su fama por toda la tierra de alrededor. 15 Y enseñaba en las sinagogas de ellos, y era glorificado por todos. 1 Corintios 12:8-9. Porque a éste es dada por el Espíritu palabra de sabiduría; a otro, palabra de ciencia según el mismo Espíritu;a otro, fe por el mismo Espíritu; y a otro, dones de sanidades por el mismo Espíritu. |
| Unción para dar buenas nuevas | Lucas 4:18-19. El Espíritu del Señor está sobre mí, Por cuanto me ha ungido para dar buenas nuevas a los pobres; Me ha enviado a sanar a los quebrantados de corazón; A pregonar libertad a los cautivos, Y vista a los ciegos; A poner en libertad a los oprimidos; 19 A predicar el año agradable del Señor. |

# 4. ANÁLISIS

Cómo hace notar Matthew Henry en su comentario sobre estos versículos:

* 12: Pedro dirige la atención que la gente estaba poniendo en ellos por el milagro, que no era mayor que los que había hecho Jeśus ni su resurrección, hacía Cristo. Pues Pedro y Juan eran pecadores, pero esa sanidad fue hecha en el nombre del que no tenía mancha: Jesucristo.
* 13: Les predica con valentía sobre Jesús como Hijo de Dios, asumiendo todo el riesgo por cuanto Jesús fue crucificado por ser Hijo de Dios.
* 13-15: Les enrostra su culpa por haberlo crucificado, de haber pretendido matar al príncipe y dador de la vida.
* 16-18: Pero les da esperanza de misericordia aún antes tan grave pecado.
* 19-20: Los exhorta a hacerse cristianos, que es el propósito de su predicación: arrepintiendose, convirtiendose y  escuchando a Cristo. Les dice lo que han de esperar: perdón y el consuelo de Cristo.
* 21-24 Justifica con las Escrituras y Ley de los Judios que Jesús había sido profetizado desde Moises por muchos profetas.
* 25-26 Le explica que como Israelitas tienen primera oportunidad de salvación.

## 4.1 Compartiendo un sueño que interpreto relacionado con el Espíritu Santo

Esta semana soñé que yo tenía algo que debía llevar, lo tenía enrollado y en el sueño el pastor Jaime me decía cómo debía usarlo, que debía extenderlo, cuando lo extendió era como un amplia ruana para cubrir. Si bien el Espíritu Santo nos puede cubrir y ungir, también entiendo este sueño como la necesidad de compartir la buena nueva con quienes aún no la conocen y de poner en práctica con otros los dones espirituales que Dios ha dado a cada quien. De lo que he encontrado, cuando se ponen en práctica los dones, el fin último es que uno de testimonio de vida en Cristo y que comparta la buena nueva de salvación, invitando creer en el Señor Jesús mediante arrepentimiento, conversión y obediencia a Él.

## 4.2 Caso de predicación del evangelio con Fuego del Espíritu

Justamente hoy se está velando el cuerpo de la señora Maria Stucky. Ella junto con su esposo Gerardo Stucky, quien había fallecido en 1988, iniciaron la obra menonita en Colombia.

Ambos se prepararon para la misión en Estados Unidos. Justamente se conocieron estudiando en el Seminario de Nueva York, y se casaron en 1943, y respondieron al llamado de Dios de servir en Colombia, donde terminaron radicados y donde tuvieron tres de sus cuatro hijos.

A continuación detallamos parte de la obra misionera de esta pareja, tomando de [https://fe.pasosdejesus.org/En_Memoria_de_Mary_Hope_Wood_Stucky/](https://fe.pasosdejesus.org/En_Memoria_de_Mary_Hope_Wood_Stucky/ "https://fe.pasosdejesus.org/En_Memoria_de_Mary_Hope_Wood_Stucky/") 

| Año | Obra | Tiempo |
| --- | --- | --- |
| 1945 | Inician obra menonita en Colombia con: (1) colegio internado para hijos de papá con Lepra y que huían de la violencia, (2) pastores de la iglesia en la finca menonita | 20 años |
| 1965 | Regresan a EEUU donde pastorean Iglesia Menonita de Berne, Indiana | 8 años |
| 1973 | Regresan a Colombia, ella inicia Hogar Cristiano La paz para atender personas de la tercera edad, y ambos inician grupo de estudio en su apartamento en Chapinero que se convertiría en la Iglesia Menonita de Teusaquillo que pastorearon hasta la muerte de Gerardo en 1988 | 15 años |
| 1988 - 2020 | Doña María se queda en Colombia donde sigue apoyando en la Iglesia de Teusaquillo, voluntaria en el Instituto Nacional de Cancerología y sirviendo con comité de educación que orientaba colegios menonitas | 32 años |

Doña María y don Gerardo Stucky dejaron su tierra y familia en Estados Unidos desde 1945 para plantar la semilla del evangelio en Colombia, predicando la salvación en Cristo con los ingredientes menonitas de servicio y paz radicales. Su amplio servicio desinteresado fue desde el humilde cambio de nombres (Mary y Gerald), pasando por hacer obras para niños, para ancianos, para enfermos, para marginados, así como pastoreando y aconsejando; hasta dejar sus huesos en este país (don Gerardo en 1988 y doña María en 2020). Todo por amor a Dios y al prójimo, particularmente amor a l@s colombian@s.

# 5. CONCLUSIÓN Y ORACIÓN

En conclusión el fuego del Espíritu Santo tiene por fin transformarnos más para predicar con más eficiencia las buenas nuevas de salvación en Jesús.

Después de ese discurso de Pedro, él y Juan fueron apresados, dieron testimonio del señor Jesús y no aceptaron la prohibición de predicar en el nombre de Jesús, fueron liberados finalmente con amenazas y volvieron donde los demás discípulos y parte de su oración es la siguiente que hoy hago por nosotros:

Hechos 4:29-31

    29 Y ahora, Señor, mira sus amenazas, y concede a tus siervos que con 
       todo denuedo hablen tu palabra,
    30 mientras extiendes tu mano para que se hagan sanidades y señales y
       prodigios mediante el nombre de tu santo Hijo Jesús.
    31 Cuando hubieron orado, el lugar en que estaban congregados tembló; y 
       todos fueron llenos del Espíritu Santo, y hablaban con denuedo la 
       palabra de Dios.

Señor que compartamos con denuedo la buena nueva como hizo Pedro, el que había pecado negándote, así como nosotros, pero que pudo hacerlo con fuego de Tu Santo Espíritu. Que invitemos a creer en el Señor Jesús mediante arrepentimiento, conversión y obediencia a Cristo.

Señor prepáranos y dirígenos para compartir la buena nueva, comenzando  con oración y nuestras vidas para que sean cada vez mejor testimonio de arrepentimiento de la vida pasada, conversión y obediencia a Jesucristo.

Señor que no guardemos lo que el Espíritu Santo nos recuerda y enseña, ni Su Fruto, ni los dones que mediante Él tenemos, ni Su unción, ni Su presencia, sino que los extendamos ampliamente a todos los conocemos y los que tu pongas.

Señor te agradecemos por don Gerardo y hoy especialmente doña María y por su buen ejemplo en la predicación de las buenas nuevas de salvación acompañada de testimonio de vida, servicio y paz radicales con el Fuego del Espíritu Santo.

Oramos por consuelo para la familia Stucky y para toda la iglesia Menonita en Colombia y para que podamos recordarlos y seguir mejor su ejemplo de seguimiento a Jesucristo con pureza y rectitud menonitas, de manera especial en los liderazgos de la Iglesia de Teusaquillo, de las iglesias hijas y de la Iglesia Menonita de Colombia.

Pedimos en el nombre de Jesús.

Si alguien aún no ha hecho Oración de fe, y hoy desea arrepentirse e iniciar una vida de conversión y obediencia a Cristo, para asegurar perdón de pecados y salvación, lo invitamos a hacer públicamente ea oración de fe.