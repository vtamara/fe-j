---
layout: post
title: "Sentado_en_su_trono"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
//Sentado en su trono
rodeado de luz
a la diestra del Padre
gobierna Jesús
con ojos de fuego
con rostro de sol
cuando abre Su boca
es trueno su voz//

CORO
//Poderoso, en majestad y Reino
Poderoso, en potestad e imperio//

Un gran arco iris
corona su ser,
Él es el cordero
que pudo vencer.

Él es el primero
Él es el postrer
y arrojan coronas
delante de Él.

CORO

///Poderoso///

</pre>

* Letra con base en: http://www.musica.com/letras.asp?letra=872839
* Canción: http://www.youtube.com/watch?v=__S2dNWXH6k
