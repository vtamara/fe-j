---
layout: post
title: "Bote_de_Galilea"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

Según {2} es uno de los 10 hallazgos arqueológicos más importantes del siglo XX.

##REFERENCIAS

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/versions/?action=getVersionInfo&vid=60
* {2} http://biblicalstudies.info/top10/schoville.htm
