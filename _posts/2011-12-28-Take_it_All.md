---
layout: post
title: "Take_it_All"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Hillsong

<pre>
Searching the world
the lost will be found
In freedom we live
As one we cry out
You carried the cross
You died and rose again
My God I'll only ever give my all

CORO
Jesus, we're living for your name
We'll never be ashamed of you
Wo, oh, oh
Our praise and all we're today
Take, take, take it all

(2)
You sent Your Son
From heaven to earth
You delivered to us all
It's eternally heard
I searched for truth
and all I found was you, my God
I'll only ever give my all

CORO
(2)
CORO (x2)

//Running to the one who heals the blind
following the shining light
In you hands the power to save the world 
and my life//

CORO (x4)
</pre>

* Video con subtitulos (en los cuales se basa esta letra): http://www.youtube.com/watch?v=6QGIVpL5GdQ
