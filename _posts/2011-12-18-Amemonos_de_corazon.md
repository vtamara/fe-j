---
layout: post
title: "Amemonos_de_corazon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
<pre>
/Amémonos de corazón,
no de labios ni de oídos,/ (bis)
/para cuando Cristo venga,
para cuando Cristo venga
nos encuentre bien unidos./ (bis)

/¿Cómo puedes tú orar
enojado con tu hermano?/ (bis)
/Dios no escucha la oración,
Dios no escucha la oración
si no estás reconciliado./ (bis)
 
/Cristo dijo, perdonad,
si queréis ser perdonados,/ (bis)
/porque aquel que no perdona,
porque aquel que no perdona
no podrá ser perdonado./ (bis)
 
/Mandamiento nuevo os doy:
que os améis unos a otros/ (bis)
/como yo os he amado,
como yo os he amado
os améis también vosotros./ (bis)
</pre>

* Letra basada en la de: http://www.obispadogchu.org.ar/cancionero/07cuaresma/172ComopuedesTuOrar.htm 

