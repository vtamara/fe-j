---
layout: post
title: "ArticuloInformacionFuncionalYEntropiaEnSistemasVivos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Escrito por Andy McIntosh en 2006, está disponible en http://www.heveliusforum.org/Artykuly/Func_Information.pdf

Sostiene que materia y energía no bastan para lograr una disminución en la entropia (i.e más orden).  Por el contrario se requiere energía e información para lograr una disminución local en la entropía.

"La autoorganización sólo ocurre cuando información existente ya es inherente al sistema y no vice versa.   En un sistema abierto, la energía (como la del sol) puede incrementar las diferencias locales de temperatura  (y así aumentar el potencial de trabajo útil que podría realizarse localmente), pero sin una máquina (es decir un dispositivo hecho o programado para usar la energía disponible) no hay posibilidad de la auto-organización de la materia.  Teiene que haber información previamente escrita o orden (conmunmente llamado 'teleonomia') para que químicos pasivos y sin vida respondan y se vuelvan activos"   


"Lo que está muerto (como una rama o una hoja de un árbol) no tiene información o teleonomía en si para convertir la energía del sol en trabajo útil.  De hecho simplemente se calentará y la entropía aumentará.  Sin embargo una planta viviente tiene información en si misma, de forma que la energía del sol es absorvida (junto con oxido de carbono y agua) por sus hojas, por fotosíntesis. La clorofila de la hoja permite que tal reacción bioquímica ocurra."
