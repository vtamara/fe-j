require 'csv'
def ew_a_md(ew)
  md = ew.gsub("==", '```')
  md.gsub!("!!!", "#")
  md.gsub!("!!", "##")
  md.gsub!("^\!", "###")
  return md
end

CSV.foreach "/tmp/e" do |row|
  t = row[0].gsub(/[^0-9A-Za-z.\-]/, '_')
  f = row[1]
  n = f + "-" + t + ".md"
  puts n
  io = open n, "w"
  io.puts "---"
  io.puts "layout: post"
  io.puts "title: \"#{t}\""
  io.puts "author: vtamara"
  io.puts "categories:"
  io.puts "image: assets/images/home.jpg"
  io.puts "tags:"
  io.puts "---"
  io.puts ew_a_md row[3]
  io.close
end
