---
layout: post
title: "Mascota_FreeBSD"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
La mascota del sistema operativo FreeBSD es considerada ofensiva por algunos cristianos, ver:

* http://www.politechbot.com/2005/04/01/religious-leaders-warn/
* http://seclists.org/politech/2005/Apr/6

Una de las páginas del proyecto FreeBSD http://www.freebsd.org/copyright/daemon.html intenta justificar que no es un demonio ("demon" en inglés), sino un "daemon" que proviene del griego y se aplicaba a espíritus buenos y malos.   Sin embargo tal explicación no es consistente con la mascota, pues si el origen fuese griego ¿Por qué no se empleó una figura griega que representara ese concepto?  Por el contrario se trata de la representación típica de un demonio para los cristianos (y también para satanistas).

Por su parte !McKusick no da rodeos con el griego.  En su página http://www.mckusick.com/beastie/  aclara que él es propietario de los derechos de reproducción de ese logo y que el nombre es beastie (en español sería algo como "bestiecita").

Es entonces una representación "tierna" del demonio.

Cabe entonces preguntarse ¿Por qué !McKusick mantiene y defiende tal "mascota"? pues en su página no da razones, sólo aclara el nombre y que él mantiene los derechos de reproducción --con permisos generosos.

Desde el 4 de Marzo de 2010, expuse que era un logo y nombre ofensivos para cristianos y le pregunté por correo a !McKusik (ahora es [carta abierta](CartaMcKusik)) la razón por la que escogió ese logo, pero no obtuve respuesta.

Sin duda !McKusick tiene una asombrosa habilidad técnica,  que ha permitido que un logo ofensivo hacia los cristianos sea ampliamente usado.  De forma similiar Richard Stallman ha aplicado su inteligencia en difundir el ateísmo junto con el proyecto GNU (bastante asociado con Linux), del cual salí en el 2001 justamente para evitar esa influencia.  En el contexto de Unix también se hizo tradición desde sus albores el uso de la palabra demonio para referirse a ciertos tipos de programas (ver http://aprendiendo.pasosdejesus.org/?id=Renombrando+Daemon+por+Service).

Son lamentables las ofensas y burlas a grupos religiosos por sus creencias, aún más de parte de personas tan inteligentes como !McKusick y Stallman.  

Tal vez esto ha promovido algo de hostilidad hacía cristianos en los círculos de los proyectos BSD (FreeBSD, NetBSD, OpenBSD) y Linux (ver por ejemplo http://www.mail-archive.com/ports@openbsd.org/msg15475.html y http://www.mail-archive.com/ports@openbsd.org/msg27449.html). Sin embargo la solidaridad en el licenciamiento, al compartir lo que se conoce, en entregar lo que se hace para todos, es punto común con el cristianismo, esa posibilidad de dar aún con creencias y posturas tan opuestamente diferentes es un increíble punto común (más que increíble es sobrenatural).   Solidaridad es otro de los nombres del amor.   Veo entonces, que es inherente a nosotros el amor, es algo con lo que ya venimos, que trasciende tiempo, espacio, credos, lenguas, razas y todo tipo de fronteras.  Al amor ni el demonio, ni nada lo puede vencer, incluso muchas páginas web reconocen que "es la fuerza más poderosa del universo", de hecho según 1 Juan 4:16 "Dios es amor, y quien permanece en el amor permanece en Dios y Dios en él."  Entonces el amor es más poderoso de lo que podemos imaginar o entender.

Mientras vamos descubriendo nuestra escencia de amor, quienes no deseen arriesgar su fe en Cristo  --quien es Dios mismo  para los cristianos-- en ambientes hostiles, o quienes deseen, Dios mediante, ganar, renovar o aumentar su Fe están invitad@s a emplear:
* [Ubuntu Christian Edition](http://www.ubuntuce.com), distribución de Linux
* [Aprendiendo de Jesús](https://aprendiendo.pasosdeJesus.org) distribución de OpenBSD para hispanohablantes.

También invitamos a orar por !McKusick, Stallman y todas las personas involucradas en proyectos tecnológicos que han sufrido hostilidad de parte de cristian@s o que han sido hostiles con cristian@s, pues según 1 Juan 4:16 por el amor que demuestran permanentemente al dar lo mejor de si a los demás, Dios sigue estando en ellos y ellas, y aquí y ahora cuando tenemos oportunidad estando vivos en esta tierra los queremos del lado de Dios, para promover el NOMBRE SOBRE TODO NOMBRE también desde la tecnología.

-----

Escrito por Vladimir Támara Patiño vtamara@pasosdeJesus.org liberado al dominio público y dedicado a Dios por su amor incluyente.
