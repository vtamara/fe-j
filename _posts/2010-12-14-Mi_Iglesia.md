---
layout: post
title: "Mi_Iglesia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Lilly Goodman

<pre>
Mi pueblo escuchame lo que voy a decirte,
Solo será un minuto si tienes que irte,
No quiero digas nada simplemente escucha,
Perdona si en un momento te interrumpa.
Hace dos mil años fui crucificado,
Mi cuerpo escupido y ensangrentado,
Llevé todas tus culpas sobre mi costado, 
Para darte la vida eterna como un regalo.

Y hoy soy quien pregunta ¿por qué no me escuchas?
¿Por qué no me miras?, ¿por qué no me abrazas?,
La hipocresía ha cegado tu mente  
y dices que me amas.
Te has hecho muy fuerte en tus razonamientos,
Has cambiado mi gloria por tus sentimientos,
A veces por las noches vengo y te despierto,
Pero ya no te importa hablarme un momento.
Te pasas todo el tiempo hablando cosas vanas, 
si la televisión las modas o la fama,
Has perdido la santidad que en ti brillaba,
sabes mas de novelas que de mi palabra.
Mi anhelo es usarte que muestres mi gloria,
Llenarte de unción y que rebose tu copa, 
Que cambies al mundo cada vez que hables 
pues el  tiempo se agota, 
se agota.

Recuerda cuando no estabas en mis brazos,
Llorabas como un niño hambriento y descalzo,
En cámara lenta tu y yo nos juntamos,
Las lágrimas caían bailando en un charco.
Te di una nueva vida  te abrí nuevas puertas,
Llené tu corazón borrando la tristeza,
Hoy dices que ya tienes hechas tus maletas,
Te  vas al mundo no importa que suceda.
No puedes negar siempre estuve contigo,
Ojalá que si vuelves yo no me hay ido,
Entonces será demasiado tarde, 
Y no podrás encontrarme.

//Tus mejillas se gastarán de 
llorar por mi,
Cuando recuerdes los momentos 
junto a ti//
////Regresa a mi, 
a mi////
</pre>

* Canción: http://www.youtube.com/watch?v=0ePQTKKrPpU
* Karaoke: http://www.youtube.com/watch?v=VduYnuns_gc
* A Capella: http://www.youtube.com/watch?v=haS2Xu8AiWE
* Coreografía: http://www.youtube.com/watch?v=0s4ATRhf5AQ 
