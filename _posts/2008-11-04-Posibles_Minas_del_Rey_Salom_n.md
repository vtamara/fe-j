---
layout: post
title: "Posibles_Minas_del_Rey_Salom_n"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
http://www.foxnews.com/story/0,2933,444286,00.html

 
Martes, 28 Octubre 2008.
Associated Press

WASHINGTON  -  Las ficticias Minas del Rey Salomón tenían un tesoro de oro y diamantes, pero los arqueólogos dicen que las minas reales pudieron haber suplido al antiguo rey con cobre.

Investigadores dirigidos por Thomas Levy de la Universidad de California, San Diego, y Mohammad Najjar de Amigos de la Arqueología de Jordán, descubrieron un centro de producción de cobre al sur del Jordán, que data del siglo 10 a.C., el tiempo del reinado del rey Salomón.

El descubrimiento ocurrió en Khirbat en-Nahas, que significa en Árabe "ruinas de cobre".

Localizado al sur del Mar Muerto, la región era conocida en el Antiguo Testamento como Edom.

Investigaciones en el sitio en 1970 y 1980 indicaban que habían comenzando trabajo con metales allí en el siglo 7 a.C. bastante después de Salomón.

Pero Levy y Najjar cavaron más profundo y pudieron datar materiales como semillas y palos del siglo 10 a.C.

"No podemos creen todo lo que nos dicen los escritos antiguos," dijo Levy en un pronunciamiento. "Pero esta investigación representa una  confluencia entre los datos arqueológicos y científicos y la Biblia."

Su hallazgo fue reportado en la edición del martes de "Proceedings of the National Academy of Sciences".


-----


King Solomon's Mines Possibly Found in Jordan
Tuesday, October 28, 2008

WASHINGTON  -  The fictional King Solomon's Mines held a treasure of gold and diamonds, but archaeologists say the real mines may have supplied the ancient king with copper.

Researchers led by Thomas Levy of the University of California, San Diego, and Mohammad Najjar of Jordan's Friends of Archaeology, discovered a copper-production center in southern Jordan that dates to the 10th century B.C., the time of Solomon's reign.

The discovery occurred at Khirbat en-Nahas, which means "ruins of copper" in Arabic.

Located south of the Dead Sea, the region was known in the Old Testament as Edom.

Research at the site in the 1970s and 1980s indicated that metalworking began there in the 7th century B.C., long after Solomon.

But Levy and Najjar dug deeper and were able to date materials such as seeds and sticks to the 10th century B.C.

"We can't believe everything ancient writings tell us," Levy said in a statement. "But this research represents a confluence between the archaeological and scientific data and the Bible."
Related

Their findings are reported in Tuesday's issue of Proceedings of the National Academy of Sciences.
