---
layout: post
title: "El_Siervo_Rey"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Del cielo vino mortal%%%
Al mundo con su cruz dejo%%%
Para servir con amor%%%
Su vida dió y se entregó%%%

__CORO__

Es nuestro Dios el Siervo Rey%%%
Nos llama hoy a andar con Él%%%
Día tras día, nuestra vida ofrecer%%%
adoración al Siervo Rey%%%

De lágrimas el jardín%%%
Y cargó Él mi soledad%%%
Su corazón en dolor%%%
Su voluntad, si, yo haré%%%

CORO

Mira sus manos y pies %%%
Del sacrificio cicatriz%%%
Manos que dan creación%%%
El clavo fue el transgresor%%%

CORO


!Coro con vídeo

Parte de la letra viene de este vídeo, otra parte del corario de la Iglesia Menonita de Teusaquillo

http://www.youtube.com/watch?v=B1COc7s67ro

!Partitura

La letra es diferente a la presentada
http://www.meseguer-pradas.com/TALLERFICHEROSPDF/046-ElSiervoRey.pdf



!Pista

Incluida por ejemplo en Adoración Instrumental 4. Colección de Adoración Instrumental. Producido por Cristopher Norton.
