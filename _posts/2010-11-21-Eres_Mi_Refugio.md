---
layout: post
title: "Eres_Mi_Refugio"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Paulina Aguirre


|__Paulina Aguirre__ | __Coro__ | __Voces Masculinas__ |
|Yo no me perderé, | | |
|estoy segura venceré | | |
|Tu eres mi fuerza | | |
|Aunque afligida este,  | | |
|seguro me levantaré | | |
|Mi fortaleza | | |
| | | |
|En medio del dolor,  | | |
|pude escuchar Tu voz | | |
|Y hoy meditaré,  | | |
|en Tu Ley, seguiré | | |
|No me alejaré,  | | |
|pues confiada voy | | |
|Tu me salvaste mi Dios | | |
| | | |
| | Erés Tu mi escudo,Tu | |
| | Erés mi refugio, Tu | |
| | Erés mi meditación | |
| | Y sólo en Ti me gozaré | |
| | | |
|Eres Tu mi escudo, Tu | ...Eres Tu mi escudo | |
|Eres mi refugio, Tu | ...Mi refugio has sido Tu | |
|Eres mi meditación | | |
|Y sólo en Ti me gozaré, Yee | | |
| | | |
| | |No te olvidaré, |
| | |nadie podrá enfrentarte, no |
| | |Tu eres mi sierva |
| | |Yo te prosperaré |
| | |en todo lo que vas a hacer |
| | |Pondré mi fuerza en ti |
| | | |
| | |Lord you are my strength, |
| | |you are my Savior |
| | |I put my trust in your Word |
| | |Tú eres mi refugio |
| | |You,re my refugee in the time of storm |
| | |Your  name is a strong tower |
| | |The righteous run in|
| | |and they are saved |
| | |In You will I put my trust |
| | |Porque Tu eres mi todo |
| | | |
|In the midst of my pain | | In the midst of my pain|
|I hear your voice so clear | | I hear your voice so clear|
|And I will meditate on your word,| | And I will meditate on your word,|
|Oh my God | | Oh my God|
| | |and no I will not stray |
| | |For I am confident |
| That you will save me, Oh Lord |That you will save me, Oh Lord |That you will save me, Oh Lord|
| | | |
| |Erés Tu mi escudo, Tu | |
| |Erés mi refugio, Tu | |
| |Erés mi meditación | |
| |Pues sólo en ti me gozaré | |
| | | |
|Eres Tu mi escudo, Tu | ...Eres Tu mi escudo | |
|Eres mi refugio, Tu | ...Mi refugio has sido Tu | |
|Eres mi meditación | ...Erés Tu mi meditación | |
|Y sólo en Ti me gozaré, Yee | | |
| | | Eres tu |
| -Mi refugio | | |
| | | Mi roca y refugio erés tu |
| | Mi refugio | |
| Yea | | |
| | Eres Tú | |
| | | Mmm |
| Mi roca y refugio erés Tu | | |
| | -Mi roca y refugio erés Tu | |
|Mi refugioooo| | |
| | -Eres Tú | |
| | | Eres Tú |
| |-Mi refugio| |
| Yoooo | | |
| |Mi refugio| |
| | Eres Tú | MMM |
| | | Mi roca y refugio erés Tu |
| | Mi roca y refugio erés Tu | |
|-Mi refugio | | |
| | Eres Tú | |
| |Mi refugio| |
| | |Mi refugio|
| | Mi refugio| |
| Yea | | |
| | Eres Tú | |
| Mi roca y refugio erés Tu | |
| | -Mi roca y refugio erés Tu | |
|Mi refugio | | |
| | Eres Tú | |
| | Mi refugio| |
| Yaaaaao | | |


* Letra adptada de: http://www.musiconcristiano.com/2009/10/17/video-y-letra-eres-mi-refugio-paulina-aguirre/
* Video: http://www.youtube.com/watch?v=0Fav2Bbv5Uw
