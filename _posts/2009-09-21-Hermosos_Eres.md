---
layout: post
title: "Hermosos_Eres"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Hermoso Eres

Marcos Witt

<pre>
En mi corazón hay una canción... que demuestra mi pasión
para mi Rey y mi Señor... para aquél que me amó.

Hermoso Eres, mi Señor
Hermoso Eres Tú, amado mío
Tú Eres la fuente de mi vida
y el anhelo de mi corazón

Hermoso Eres, mi Señor
Hermoso Eres Tú, amado mío.
Tú Eres la fuente de mi vida
y el anhelo de mi corazón<
</pre>
http://vagalume.uol.com.br/marcos-witt/hermoso-eres.html


Puede verse una presentación con fondo del autor original en
http://www.youtube.com/watch?v=oGO-JbNTsFg
o de un joven interprete con unción en:
http://www.youtube.com/watch?v=HRgDzlx_8Ss
