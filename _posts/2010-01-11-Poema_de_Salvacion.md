---
layout: post
title: "Poema_de_Salvacion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Se trata del testimonio del rockero Pablo Olivares en pelicula.  Su paso de las tinieblas a la luz admirable.

Fue lanzada a mediados de 2009 pero aún no está en Colombia, hay un grupo que sondea interés por parte de colombian@s en [http://www.facebook.com/group.php?gid=221280027824]

Hay unos cortos en:
http://www.youtube.com/watch?v=YLvf6EJ3OY8

Marcos Witt lo introduce como un excelente recurso evangelistico en:
http://www.youtube.com/watch?v=qYgSI5ZzT0w

Del mismo Pablo Olivares, una hermosa canción que ministra:
Yo quiero amarte más - http://www.youtube.com/watch?v=jUaHfiyxo3o
