---
layout: post
title: "Paramilitares_Soacha_2013"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Respecto al asesinato de la líder del barrio Los Robles, Isnelda Gutierrez, el 9 de Abril de 2013, la fuerza pública inicialmente manifestó que no tenía relación con su actividad comunitaria (ver {1}), sin embargo posteriormente cuando comunicaron la captura de 9 integrantes de la banda "Samuel" presuntamente liderada por el también capturado Luis Samuel Cardona alias el ?Pollo (ver {3}), la policia informó que a esta banda, denunciada por Isnelda, se le atribuía el asesinato de 18 personas incluyendo a la líder Isnelda y  que estaba en puja por el control del microtráfico de drogas.   Respecto al mismo sujeto informaron que  es "exintegrante de las autodefensas que nunca se desmovilizó y conformó la organización criminal." Dada la contradicción en la información suministrada por fuerza pública, ¿como discernir cuando la fiscalía (ver {8}) y el gobernador de Cundinamarca (ver {7}) se pronuncian diciendo que en el 2013 no hay paramilitares o guerrilla en Soacha, mientras que organizaciones como Arco Iris (ver {2}) o informes de la misma Defensoria del Pueblo (ver {4}) dicen que si hay? De hecho dan cuenta en el 2013 de:
* Presencia de las bandas paramilitares Los Pascuales y Los Tarazona al menos en Soacha, Yacopi y La Palma
* Presencia y accionar en Soacha de Las Aguilas Negras - Bloque Capital.
* Según el defensor también se utilizan pandillas barriales para la ejecución de crímenes. 

De lo anterior y el contexto nacional (ver {9}) puede plantearse como hipótesis que la mayoría de la producción y macrotráfico de droga a nivel nacional es manejado por guerrilla y estructuras paramilitares grandes que delegan microtráfico y eventualmente algunas actividades de transporte, por ejemplo en Soacha y alrededores, a bandas ciudadanas menores en casos lideradas por paramilitares no desmovilizados o re-movilizados y a pandillas de barrio.

Esta caótica situación es en buena parte resultado de la supuesta "desmovilización paramilitar" presentada por el ex-presidente Uribe Velez, acción mediatica sin verdad, ni justicia, ni reparación que no afectó la producción, transporte y tráfico de drogas y que dejó libres de procesos judiciales a miles de paramilitares.

Oramos para que el Señor toque conciencia de quienes delinquen, por protección para los líderes y familiares amenazados este año por la guerrilla y paramilitares, y rogamos por consuelo para los familiares de los líderes comunitarios asesinados en Soacha recientemente, incluyendo a:
* José Luis Roa líder de San Mateo asesinado en Agosto de 2012
* Julio Cesar Castro líder en Villa Mercedes asesinado el 5 de Enero de 2013
* Jairo Rojas esposo de una líder de ciudadela Sucre asesinado el 6 de marzo de 2013
* Isnelda Gutierrez líder del barrio Los Robles asesinada el 9 de Abril de 2013.

En todos estos casos se mantiene la impunidad, sin confensiones ni resultados judiciales pero los líderes de otros sectores de Soacha (ver {5}) presumen que los asesinatos se relacionan con las denuncias respecto a las bandas, pandillas y microtráfico.   También los mismos líderes han manifestado la dificultad para denunciar ante autoridades policiales tanto por el excesivo tiempo que los trámites requieren como por temor fundado en casos de complicidad entre policia y criminales (ver por ejemplo {5}). Seguimos orando por protección del Todopoderos y valor para no callarnos ante lo que sigue ocurriendo, pues sólo la verdad guiará una buena transformación social y nos hará libres (Juan 8:32, ver {6}).

##REFERENCIAS

* {1} http://periodismopublico.com/Autoridades-toman-medidas-a-raiz
* {2} http://www.periodismopublico.com/Corporacion-Nuevo-Arco-Iris
* {3} http://www.periodismopublico.com/Desmantelada-peligrosa-banda
* {4} http://periodismopublico.com/Defensor-del-Pueblo-alerta-sobre
* {5} http://periodismopublico.com/Tension-en-la-comuna-cuatro-de
* {6} http://www.biblegateway.com/passage/?search=juan%208:32&version=RVR1960
* {7} http://www.larepublica.co/economia/%E2%80%9Cno-puede-haber-una-paz-duradera-si-no-existe-una-etapa-post-conflicto%E2%80%9D-cruz_39982
* {8} http://www.periodismopublico.com/Cuestionan-reclutamiento-forzado
* {9} http://www.nocheyniebla.org
