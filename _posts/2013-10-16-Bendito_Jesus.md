---
layout: post
title: "Bendito_Jesus"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
Por Ti es mi clamor
Y si tengo temor
En Ti puedo confiar
Bendito Jesús

Hoy puedo escuchar
Tu voz de sanidad
Restaurando mi ser
Bendito Jesús

CORO

Mi deseo eres Tú 
Mi momento de amor
La alegría de vivir
Bendito Jesús

Eres mi fuente de amor
Eres aire y eres sol
El latir del corazón
Bendito Jesús

Bendito amor
De mi Señor


Tu gracia y favor
Conmigo puedo ver
Llenando hoy mi ser
Bendito Jesús

Y hoy mi voluntad
se rinde a Tu verdad
Tu amor y Tu bondad
Bendito Jesús

//CORO//
</pre>


* Video: http://www.youtube.com/watch?v=oBbjJAqLQvk
* Karaoke: http://www.youtube.com/watch?v=VqxSe3hCWNo

