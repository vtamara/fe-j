---
layout: post
title: "Con_mis_manos_levantadas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
Con mis manos levantadas hacia el cielo
Me presento ante ti hoy mi Señor
Para recibir de ti la fuerza y el poder
Para vivir junto a ti
Llenas hoy mi corazón con tu presencia

Llenas de alegría y paz todo mi ser
De cualquier necesidad tu me responderás
Por que me amas me amas
Por que me amas me amas
de cualquier necesidad....
</pre>

* Video: http://www.youtube.com/watch?v=r2vAYR7xFLs
* Karaoke: http://www.youtube.com/watch?v=p8wk0auyl-4
* Letra basada en la de: http://www.musica.com/letras.asp?letra=1206042
