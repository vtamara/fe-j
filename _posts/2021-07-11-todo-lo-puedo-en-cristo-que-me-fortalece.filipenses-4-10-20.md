---
layout: post
categories:
- prédica
title: Todo lo puedo en Cristo que me Fortalece. Filipenses 4:10-20
author: vtamara
image: "/assets/images/pasted-image-0.png"

---
# Todo lo puedo en Cristo que me fortalece. Filipenses 4:10-20

## Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 11.Jul.2021

# 1 Introducción

El tatuaje sobre el lado izquierdo de su Tórax decía **_Todo lo puedo en Cristo que me fortalece_**. Este versículo que encontramos en Filipenses 4:13 y que hoy estudiaremos en su pasaje, estaba escrito sobre el cuerpo sin vida del joven identificado como Juan David Realpe de 20 años encontrado en Cali el 25 de Junio de este año. Su familia informó que se dedicaba a la construcción en Cali y que vivía en un barrio de invasión.

La fotografía de Juan David que está de portada fue tomada de [https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/](https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/ "https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/")

Su cuerpo apareció en el río Cañaveralejo con disparos de arma de fuego y signos de tortura según \[QUEHUBO\].

Esto acontece en el contexto del paro nacional que comenzó el 28 de Abril de este año y aún continúa con intensidad más baja. En este contexto, en redes sociales ha quedado la evidencia fotográfica y fílmica de otros cuerpos de jóvenes especialmente reportados en ríos de Cauca y Valle del Cauca, también ha quedado evidencia fílmica de hombres que lanzan estos cuerpos al río desde caminos. Algunos de estos cuerpos quizás también torturados como el de Juan David pero varios quizas aún no han sido identificados, y por lo mismo no aparecen en el "Listado de 74 víctimas fatales hasta el 28 de Junio en el marco del paro" (ver \[INDEPAZ\]). De hecho Juan David Realpe tampoco está en ese listado. En ese listado la fuerza pública (Ejército, CTI, ESMAD, GOES, Policía, Policía Antinarcóticos) es presunta responsable de la mayoría de los casos (i.e 44 o 59%).

# 2. Texto: Filipenses 4:10-20

10 En gran manera me gocé en el Señor de que ya al fin habéis revivido vuestro cuidado de mí; de lo cual también estabais solícitos, pero os faltaba la oportunidad.

11 No lo digo porque tenga escasez, pues he aprendido a contentarme, cualquiera que sea mi situación.

12 Sé vivir humildemente, y sé tener abundancia; en todo y por todo estoy enseñado, así para estar saciado como para tener hambre, así para tener abundancia como para padecer necesidad.

13 Todo lo puedo en Cristo que me fortalece.

14 Sin embargo, bien hicisteis en participar conmigo en mi tribulación.

15 Y sabéis también vosotros, oh filipenses, que al principio de la predicación del evangelio, cuando partí de Macedonia, ninguna iglesia participó conmigo en razón de dar y recibir, sino vosotros solos;

16 pues aun a Tesalónica me enviasteis una y otra vez para mis necesidades.

17 No es que busque dádivas, sino que busco fruto que abunde en vuestra cuenta.

18 Pero todo lo he recibido, y tengo abundancia; estoy lleno, habiendo recibido de Epafrodito lo que enviasteis; olor fragante, sacrificio acepto, agradable a Dios.

19 Mi Dios, pues, suplirá todo lo que os falta conforme a sus riquezas en gloria en Cristo Jesús.

20 Al Dios y Padre nuestro sea gloria por los siglos de los siglos. Amén

# 3. Contexto

Algunos momentos de la vida del apostol Pablo extraidos de la Biblia, \[Wilson, Alyssial\] y Wikipedia son:

* \~5 Nació en Tarso, hijo de Judios y ciudadano Romano
* En Jerusalén, educado por Gamaliel para ser Fariseo
* En Jerusalén perseguidor de Cristianos
* \~31/36 Camino a Damasco, conversión por aparición de Jesús
* 46-48 Primer viaje misionero. Hechos 13 y 14
* 49-52 Segundo viaje misionero. Hechos 16 a 18
* 53-57 Tercer viaje misionero. Hechos 18:23-21:14
* 58-62/64? Viaje bajo custodia romana
  * 58 Apresado en Jerusalen
  * 58-60 En prisión en Cesarea
  * 60 Viaja como prisionero a Roma con naufragio incluido
  * 60-62/64 En prisión domiciliaria en Roma
  * 64 Ejecución por parte de Nerón

Fue mientras estaba en prisión bajo custodia romana que escribió las epístolas a los Efesios, a los Filipenses, a los Colosenses y a Filemón.

La carta a los filipenses fue dirigida a la iglesia en Filipos, que era una ciudad en Macedonia, hoy Grecia, donde Pablo inició una comunidad de fe durante su segundo viaje misionero

![](https://lh6.googleusercontent.com/8CRYwY8Pn2o7MWYgBbnXCKXXyfOGMFRCEQNPHw2qAPpNHAoX-HAJgNKNv5dZQB6qvO4Om2PuaJ5C5SMCuSFzOoreQSMFOa49Qaf_3h6Rdco-GYzg6KUGz5uuxwuJQ7WGRnBwo6wZ =279x231)Imagen de Wikipedia.

![](https://lh5.googleusercontent.com/PUNQobsLtOssfE0unda9t14o6L96yo6tKgt8Zf8qnKKJXkyHqOoeyNNkCEuNs9Whp_ZqCEo_6g7ASMiRqCPExa81gsWy3bu3ByWG0IECd22icebgHd2Q8sEmZ7a_iB-cZ5arKtHU =499x340)Foto de las ruinas de Filipo de [https://upload.wikimedia.org/wikipedia/commons/5/5c/Philippi_city_center.jpg](https://upload.wikimedia.org/wikipedia/commons/5/5c/Philippi_city_center.jpg)

En Hechos 16 se indica que Dios dirigió en sueños a Pablo hacia Macedonia, donde fue con Silas, para quedarse muchos días en aquella ciudad. Enseñaban en la sinagoga y se dieron conversiones como la de Lidia.

En Filipos, ocurre el incidente con una joven adivinadora y esclava que los seguía, de quien Pablo expulsó el espíritu de adivinación por lo que los amos de la joven enviaron a prisión a Pablo y Silas. Allí un terremoto sobrenatural a media noche los dejó libres. A raíz de esto ocurre la conversión del carcelero y son liberados al día siguiente.

Algunos temas de la carta a los Filipenses en títulos de pasajes especialmente tomados de \[RV1960\] son:

| Capítulo | Temas |
| --- | --- |
| 1 | Saludo. Oración por Creyentes. Para Mi el Vivir es Cristo. |
| 2 | Humillación y exaltación de Cristo. Luminares en el Mundo. Timoteo y Epafrodito enviados. |
| 3 | Prosigo al blanco. |
| 4 | Regocijaos en el Señor siempre. En esto pensad. Dadivas de los filipenses. Salutaciones finales. |

# 4. Análisis

Pablo fundó la iglesia en Filipo hacia el año 50 (cuando él tenía 45 años) y escribió la carta posiblemente unos 10 años después (cuando tenía 55 años) mientras estaba en prisión.

| Versículos | Observación |
| --- | --- |
| 10 | Tal vez el carcelero de Filipo y Lida seguían en esa iglesia y posiblemente ellos junto con otros creyentes en esos 10 años, no dejaron de enviarle ofrendas a Pablo. Por lo visto ante un envío a su prisión en Roma, Pablo respondió con la epístola a los Filipenses. |
| 11-12 | Definición de contentamiento, que debemos aprender y vivir |
| 13 | Es como una expresión de aguante o resiliencia. Tener gozo aún ante dificultades. Seguramente Juan David que vivía en un barrio de invasión entendía esto. |
| 14 | La solidaridad en particular con nuestros pastores. |
| 15 | Por lo visto los filipenses empezaron a aportarle a Pablo desde su conformación como comunidad de fe --seguramente inicialmente poco porque eran pocos, pero no dejaron de dar desde el comienzo. |
| 16 | No se trataba de enriquecer a su pastor, sino de aportar para las necesidades de él, para que pudiera realizar su trabajo, pues sin duda esos viajes misioneros fueron costosos en todo sentido. |
| 17 | Las ofrendas de los filipenses eran fruto de su conversión a Cristo y el uso que Pablo les daba reflejaba a Cristo. |
| 18 | Tal vez Juan David, hoy tenga abundancia y esté lleno en presencia de Cristo. |
| 19 | Confiemos en que Dios nos suplirá |
| 20 | Al Dios y Padre nuestro sea gloria por los siglos de los siglos. Amén |

# 5. Conclusión y oración

Como en el versículo 18, Dios sabe si Juan David, hoy tenga abundancia y esté lleno en la presencia de Cristo. Seguramente conocía que Jesús es el camino la verdad y la vida, como el tatuaje que se hizo lo sugiere.

No estoy diciendo que debamos tatuarnos en la piel un versículo para mostrar que somos cristianos, ni tampoco estoy condenando a Juan David por haberlo hecho (aunque de Levítico 19:28 puede interpretarse como prohibición de tatuarse, por lo que considero que no conviene hacerlo).

A lo que sí invito es a tatuarnos la solidaridad en el corazón: como la de Pablo que daba todo lo que tenía para enseñar de Cristo, como la de los Filipenses, que daban de sus recursos para ayudar al sostenimiento de Pablo.

Seamos solidarios con las víctimas y sus familias hoy, con los jóvenes que protestan pacíficamente, diferenciemoslos de quienes vandalizan, no condenemos ni permitamos que se condenen los bloqueos de manera generalizada, como sugiere el documento de la CIDH, pero pidamos a sus organizadores que sean acotados para no afectar la vida y aguantemos todos con solidaridad.

* Papito Dios gracias por cuidar de nosotros, por enseñarnos y darnos la posibilidad de compartir de lo que tu nos das.
* Que seamos solidarios, que podamos compartir tu Palabra con los jóvenes que participan en las protestas, con la policía, con los gobernantes, para que haya respeto a la vida y temor de Tí.
* Que seamos solidarios en la iglesia y ofrendemos como los filipenses para ayudar a suplir las necesidades de nuestra pareja pastoral.
* Te pedimos por la familia de Juan David, dale tu paz que sobrepasa todo entendimiento.
* Si alguién aún no ha hecho la oración de fe, para recibir a Jesús en su corazón como Señor y Salvador, [lo invitamos a hacerla hoy](https://fe.pasosdejesus.org/Oracion_de_fe/) y acercarse a una comunidad de fe sana, en particular a [la nuestra](https://www.facebook.com/groups/menonitacaminosdeesperanza).

Y como decía Pablo “Al Dios y Padre nuestro sea gloria por los siglos de los siglos. Amén.”

# 6. Referencias Bibliográficas y Créditos

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* Cali 24 Horas. 27.Jun.2021. Hallan cuerpo sin vida en el río Cañaveralejo: esto es lo que se sabe. [https://www.cali24horas.com/hallan-cuerpo-sin-vida-en-el-rio-canaveralejo-esto-es-lo-que-se-sabe/](https://www.cali24horas.com/hallan-cuerpo-sin-vida-en-el-rio-canaveralejo-esto-es-lo-que-se-sabe/ "https://www.cali24horas.com/hallan-cuerpo-sin-vida-en-el-rio-canaveralejo-esto-es-lo-que-se-sabe/")
* QUEHUBO Cali. 30.Jun.2021. Juan David el Jóven hallado muerto en el río Cañaveralejo. [https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/](https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/ "https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/")
* INDEPAZ. 28.Jun.2021. Listado de las 74 víctimas de violencia homicida en el marco del Paro Nacional al 28 de junio [http://www.indepaz.org.co/victimas-de-violencia-homicida-en-el-marco-del-paro-nacional/](http://www.indepaz.org.co/victimas-de-violencia-homicida-en-el-marco-del-paro-nacional/ "http://www.indepaz.org.co/victimas-de-violencia-homicida-en-el-marco-del-paro-nacional/")
* Wilson, Alyssial. A Breakdown of Paul’s Four Missionary Journeys [https://bethanygu.edu/news/missionary-journey/](https://bethanygu.edu/news/missionary-journey/ "https://bethanygu.edu/news/missionary-journey/") . Consultado en Jul.2021
* Zondervan Academic. Who was Paul? His early life, and why it matters. [https://zondervanacademic.com/blog/who-was-paul-his-early-life-and-why-it-matters](https://zondervanacademic.com/blog/who-was-paul-his-early-life-and-why-it-matters "https://zondervanacademic.com/blog/who-was-paul-his-early-life-and-why-it-matters"). Consultado en Jul.2021
* Wikipedia. Paul the Apostle. [https://en.wikipedia.org/wiki/Paul_the_Apostle](https://en.wikipedia.org/wiki/Paul_the_Apostle "https://en.wikipedia.org/wiki/Paul_the_Apostle")
* Támara, Vladimir. 2020. Mapa-Historia Segundo viaje misionero de Pablo. [https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html](https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html "https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html")

***

La imagen de portada es de Juan David Realpe. Fue publicada por el diario Que Hubo Cali. [https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/](https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/ "https://www.qhubocali.com/judiciales/juan-david-el-joven-hallado-muerto-en-el-rio-canaveralejo/")

Este documento también está disponible en:
[https://docs.google.com/document/d/1ZVpicVaQF9VJabBIm0JgNwDdI1KNTDhDa8Lk7f1W1sI/edit?usp=sharing](https://docs.google.com/document/d/1ZVpicVaQF9VJabBIm0JgNwDdI1KNTDhDa8Lk7f1W1sI/edit?usp=sharing)

Esta prédica se cede al dominio público de acuerdo a la legislación colombiana.
