---
layout: post
title: "ADiosAlabare"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!A Dios Alabaré
Letra: Pedro Stucky

A Dios Alabaré %%%
De todo corazón %%%
Y proclamaré  %%%
sus maravillas %%%
su nombre cantaré %%%

Señor tealabaré %%%
de todo corazón %%%
Tu eres Señor %%%
el gozo para mí %%%
Ale-elu-uya 

Dios, El Señor es Rey %%%
Él permanecerá %%%
y juzgará al mundo con verdad %%%
justicia sentará 

Dios el Señor es Rey %%%
su reino es eternal %%%
Los pueblos verán  %%%
justicia y rectitud %%%
Ale-elu-uya 

Al pobre cuidará %%%
Y le protejerá %%%
Refugio es Él %%%
en tiempo de aflicción %%%
Su nombre es Salvador 

Al oprimido ve,  %%%
y le defenderá %%%%
El salvador, %%%
el santo de Israel %%%
Ale-elu-uya 

Gloria al Padre Dios, %%%
Loor a Cristo el Rey %%%
Al Santo Espíritu %%%
te bendecirá %%%
Su nombre alabaré 

Honra al Creador %%%
Loor al Slavador %%%
Al Santo Espíritu  %%%
Consolador %%%
A dios alabo hoy
