---
layout: post
title: "Eclesiastes"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Libro del Antiguo Testamento atribuido a [Salomon].   

En este libro el autor narra su experiencia de vida, su búsqueda de felicidad en placeres, en conocimiento y concluye en sus 3 últimos versículos (de {2}):
<pre>
Ahora, hijo mio, a más de esto, sé amonestado.  No hay fin de hacer muchos libros; y el mucho estudio es fatiga de la carne. El fin de todo el discurso oído es este: Teme a Dios, y guarda sus mandamientos; porque esto es el todo del hombre.  Porque Dios traerá toda obra a juicio, juntamente con toda cosa encubierta, sea buena o sea mala.
</pre>

##Evidencias de autoría

* Evidencia interna:
** El autor se identifica como "el hijo de David, rey de Jerusalén" (1:1) {1}
** Sabiduría sin rival del escritor (1:16) {1}
** Riquezas extremas (2:7) {1}
** Oportunidades de placeres (2:3) {1}
** Actividades de construcción extensas (2:4-6) {1}
** No hay otro descendiente de David que tenga todas estas características fuera de Salomón. {1}
** En 12:9 dice que el autor compuso muchos proverbios, --libro atribuido a Salomón.


*Evidencia  externa
** La tradición judia atribuye el libro a Salomón


## Eclesiastes 5:10-20

##REFERENCIAS

* {1} http://www.bibleinsong.com/Song_Pages/Ecclesiastes/Ecclesiastes12/Ecclesiastes12.htm
* {2} RV 1960.
