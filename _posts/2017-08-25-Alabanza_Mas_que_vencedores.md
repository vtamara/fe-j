---
layout: post
categories:
- Alabanza
title: Alabanza Más que vencedores
author: vtamara
image: ''

---
## 

Richi Ray y Bobby Cruz

    --Volvieron las Bestias
    
    --Huye
    
    Nadie conoce al cazador si no tiene escopeta
    Nadie conoce al tiburón si no le da en su aleta
    Y el cristiano por su espada siempre se da a conocer
    Escucha, escucha
    El poder de la palabra hace la tierra temblar
    
    Y todos tus enemigos a tus pies se rendirán
    
    Firme mi hermano no heche pa'tras
    que en la Roca victoria tendrá
    
    (Ea, no temas hermano que en la Roca vencerás)
    Por más fuerte que sea el enemigo en la roca vencerás
    (Ea, no temas hermano que en la Roca vencerás)
    Cristo es la roca de Horeb que está brotando agua viva para usted
    
    
    
    (Ea, no temas hermano que en la Roca vencerás)
    Hecha pa'lante muchacho y no temas que en la roca vencerás
    (Ea, no temas hermano que en la Roca vencerás)
    Que de todos los enemigos, Su poder nos salvará
    (Ea, no temas hermano que en la Roca vencerás)
    Aunque tiemble la tierra y el cielo se opaque la victoria nos dará
    
    
    
    (Ya somos más que vencedores)
    Por aquel que nos amó y perdóno nuestros errores
    (Ya somos más que vencedores)
    Por eso hecha pa'lante muchacho y no llores
    (Ya somos más que vencedores)
    Que Él es la roca, que Él es la Roca de Horeb Que está brotando
    (Ya somos más que vencedores)
    Agua, Agua de vida para ti y para mi
    
    --Que hable el yuyu
    
    --Palito
    
    
    ///(Ya somos más que vencedores)
    Hecha pa'lante muchacho y no llores
    (Ya somos más que vencedores)
    Que ya, Que ya somos más que vencedores
    (Ya somos más que vencedores)
    Por aquel que nos amó y nos vistió de loores
    (Ya somos más que vencedores)
    Él es la roca, Él es la roca de Horeb que está brotando
    (Ya somos más que vencedores)
    Agua, auga, Agua de vida para ti y para mi
    (Ya somos más que vencedores)
    Qe ya somos más que vencedores
    (Ya somos más que vencedores)
    Cristo es la Roca, Cristo es la Roca

* Audio: [https://www.youtube.com/watch?v=udGKImYTK-Y](https://www.youtube.com/watch?v=udGKImYTK-Y "https://www.youtube.com/watch?v=udGKImYTK-Y")
* Historia de como sus compositores le cambiaron la letra a Sonido Bestial: [http://cristianos.com/%C2%ABsalsa-cristiana-para-honrar-a-dios%C2%BB-grabaran-richie-ray-y-bobby-cruz/](http://cristianos.com/%C2%ABsalsa-cristiana-para-honrar-a-dios%C2%BB-grabaran-richie-ray-y-bobby-cruz/ "http://cristianos.com/%C2%ABsalsa-cristiana-para-honrar-a-dios%C2%BB-grabaran-richie-ray-y-bobby-cruz/")