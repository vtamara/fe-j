---
layout: post
title: "En_Momentos_Asi"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##En Momentos Asi
David Graham

En momentos así%%%
Levanto mi voz%%%
Levanto mi canto a Cristo%%%

En momentos así%%%
Levanto mi ser%%%
Levanto mis manos a Él%%%

Cuanto te amo Dios%%%
Cuanto te amo Dios%%%
Cuanto te amo%%%
Dios te amo%%%

En momentos así%%%
Levanto mi voz%%%
Levanto mi canto a Cristo%%%

En momentos así%%%
Levanto mi ser%%%
Levanto mis manos a Él%%%

//Cuanto te amo Dios%%%
Cuanto te amo Dios%%%
Cuanto te amo%%%
Dios te amo//

Según {1} el autor es David Graham 

!Canción

http://www.youtube.com/watch?v=EgCkae9VwOQ


!Midi

http://www.midisaya.com/dispcanto.asp?ID=MIDIS01/EnMomentosAsi.txt

!Pista

Incluida por ejemplo en Adoración Instrumental 4. Colección de Adoración Instrumental. Producido por Cristopher Norton.



!Referencias

{1} http://www.cantamos.es/canta/slletter/s0111l.htm
