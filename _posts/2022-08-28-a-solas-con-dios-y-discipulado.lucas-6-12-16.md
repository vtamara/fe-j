---
layout: post
categories:
- Prédica
title: A solas con Dios y Discipulado. Lucas 6:12-16
author: vtamara
image: "/assets/images/last-supper.jpg"

---
# **1. Introducción**

De unos pocos versículos breves el Señor nos enseña sobre el discipulado.

# **2. Texto**

12 En aquellos días él fue al monte a orar, y pasó la noche orando a Dios. 13 Y cuando era de día, llamó a sus discípulos, y escogió a doce de ellos, a los cuales también llamó apóstoles: 14 a Simón, a quien también llamó Pedro, a Andrés su hermano, Jacobo y Juan, Felipe y Bartolomé, 15 Mateo, Tomás, Jacobo hijo de Alfeo, Simón llamado Zelote, 16 Judas hermano de Jacobo, y Judas Iscariote, que llegó a ser el traidor.

# **3. Contexto**

## **3.1 v12-13.**

Solo el evangelio de Lucas, presenta retiros de Cristo para orar en secreto, varios de ellos ante decisiones importantes (quizás Dios le reveló cómo decidir durante la oración (?)).

| Versículos | Dónde | Cuándo/Cuánto tiempo | Después |
| --- | --- | --- | --- |
| 4:1-2 | Desierto para ayunar | 40 días | Tentado y después inicia el ministerio |
| 4:42-44 | Lugar solitario cerca de la casa de Simón en Galilea para orar | Amanecer | 43: “Pero él les dijo: Es necesario que también a otras ciudades anuncie el evangelio del reino de Dios; porque para esto he sido enviado.” |
| 5:16 “Mas él se apartaba a lugares desiertos, y oraba”. | Desierto. Lugares solitarios para orar |  |  |
| 6:12-13 | Montaña para orar | Toda la noche en oración | Selección de 12 apóstoles |
| 22:41-42 | Monte de los olivos, a cierta distancia de los discípulos. Oraba “diciendo: Padre, si quieres, pasa de mí esta copa; pero no se haga mi voluntad, sino la tuya.” |  | Antes de ser arrestado, sufrimiento, muerte y resurrección |

## **3.2 v14-16**

Nombres de los apóstoles según diversos versículos:

| Lc 6:12-16 | Mt 10:2-4 | Mc 3:16- 19 | Hch 1:13 |
| --- | --- | --- | --- |
| Simón (Pedro) \[1\] | Simón (Pedro) \[1\] | Simón (Pedro) \[1\] | Pedro \[1\] |
| Andrés (hno. Simón) \[2\] | Andrés (hno. Simón) \[2\] | Andrés \[4\] | Andrés \[4\] |
| Santiago \[3\] | Santiago (hijo. Zebedeo) \[3\] | Santiago (hijo. Zebedeo) \[2\] | Santiago \[2\] |
| Juan \[4\] | Juan (hno. Santiago) \[4\] | Juan (hno. Santiago, Boanerges --hijos del Trueno) \[3\] | Juan \[3\] |
| Felipe \[ 5\] | Felipe \[5\] | Felipe \[5\] | Felipe \[5\] |
| Bartolomé \[6\] | Bartolomé \[6\] | Bartolomé \[6\] | Bartolomé \[7\] |
| Mateo \[7\] | Mateo (cobrador imp. ) \[8\] | Mateo \[7\] | Mateo \[8\] |
| Tomás \[8\] | Tomás \[7\] | Tomás \[8\] | Tomás \[6\] |
| Santiago (hijo de Alfeo) \[9\] | Santiago \[hijo de Alfeo\] \[9\] | Santiago (hijo de Alfeo) \[9\] | Santiago (hijo de Alfeo) \[9\] |
| Simón (celote) \[ 10\] | Simón (cananeo) \[11\] | Simón (cananeo) \[11\] | Simón (celote) \[10\] |
| Judas (hijo de Santiago) \[11\] | Lebeo Tadeo \[10\] | Tadeo \[10\] | Judas (hijo de Santiago) \[11\] |
| Judas Iscariote\] \[12\] | Judas Iscariote \[12\] | Judas Iscariote \[12\] |  |

# **3.3 Más contexto de cada apóstol**

# **3.3.1 Simón**

![](/assets/images/pedro.png)Icono siglo 6. Monasterio de Santa Catalina

Nacimiento: Betsaida

Ocupación antes: Pescador

Padres: Jonas y Joanna

Hermano: Andrés

Vivió: Cafarnaum con suegra (y esposa). Según Clemente Alejandrino tenía hijos y su esposa también sufrió martirio.

Llamamiento: Caminando por la ribera del mar de Galilea vio a dos hermanos, Simón, llamado Pedro, y su hermano Andrés, echando la red en el mar, pues eran pescadores, y les dice: «Venid conmigo, y os haré pescadores de hombres.» Y ellos al instante, dejando las redes, le siguieron.

Mateo 4, 18-20

Martirio: Según fuentes extrabíblibcas, crucificado por Nerón, pidió ser crucificado boca abajo

## **3.3.2 Andrés**

![](/assets/images/andres.png) Icono del siglo XIV.

Nacimiento: Betsaida

Ocupación antes: Pescador

Padres: Jonas y Joanna

Hermano: Simón

Vivó: Cafarnaúm.

Llamamiento: Caminando por la ribera del mar de Galilea vio a dos hermanos, Simón, llamado Pedro, y su hermano Andrés, echando la red en el mar, pues eran pescadores, y les dice: «Venid conmigo, y os haré pescadores de hombres.» Y ellos al instante, dejando las redes, le siguieron.

Mateo 4, 18-20

## **3.3.3 Santiago (el mayor) o Jacobo**

![](/assets/images/santiagomayor.png)Cuadro de Guido Reni. Siglo XVI

Nacimiento: Betsaida

Papa: Zebedeo y Salomé

Hermano: Juan

Vivó: Cafarnaúm.

Ocupación antes: Pescador

Llamamiento: “Pasando de allí, vio a otros dos hermanos, Jacobo hijo de Zebedeo, y Juan su hermano, en la barca con Zebedeo su padre, que remendaban sus redes; y los llamó. 22 Y ellos, dejando al instante la barca y a su padre, le siguieron.” Mat 4:21-22

Martirio: Asesinado por el rey Herodes Hechos 12 :1

## **3.3.4 Juan**

![](/assets/images/juan.png)San Juan el Evangelista (1600), por El Greco.

Nacimiento: Betsaida

Papa: Zebedeo y Salome

Hermano: Santiago (el mayor)

Vivó: Cafarnaúm.

Ocupación antes: Pescador

Llamamiento: “Pasando de allí, vio a otros dos hermanos, Jacobo hijo de Zebedeo, y Juan su hermano, en la barca con Zebedeo su padre, que remendaban sus redes; y los llamó. 22 Y ellos, dejando al instante la barca y a su padre, le siguieron.” Mat 4:21-22

Martirio: Fuentes extrabíblicas dicen que sobrevivió al agua hirviendo, exiliado en la isla de Patmos

## **3.3.5 Felipe**

![](/assets/images/felipe.png)San Felipe, de Peter Paul Rubens, c. 1611.

Nacimiento: Betsaida

Llamamiento: “43 El siguiente día quiso Jesús ir a Galilea, y halló a Felipe, y le dijo: Sígueme. 44 Y Felipe era de Betsaida, la ciudad de Andrés y Pedro. ” Juan 1:43-44

Martirio: Ahorcado contra un pilar en Hierápolis en Frigia

## **3.3.6 Bartolomé (o Natanael)**

![](/assets/images/bartolome.png)Lienzo de José de Ribera, 1643

Nacimiento: Caná

Llamamiento: “45 Felipe halló a Natanael, y le dijo: Hemos hallado a aquel de quien escribió Moisés en la ley, así como los profetas: a Jesús, el hijo de José, de Nazaret.” Juan 1:43-44

Martirio: Desollado vivo por Astiages rey de Armenia

## **3.3.7 Mateo**

![](/assets/images/mateo.png)La inspiración de San Mateo, por Caravaggio

Nacimiento: Judea

Padres: Alfeo

Profesión antes: Recaudador de impuestos

Llamamiento: Mateo 9:9-13

Martirio: Asesinado en Etiopía

## **3.3.8 Tomás o Judas Tomás Didimo**

![](/assets/images/tomas.png)Santo Tomás, por Diego Velázquez

Nacimiento: Galilea

Martirio: Atravesado con una lanza en las Indias Orientales

## **3.3.9 Santiago (hijo de Alfeo)**

![](/assets/images/santiagomenor.png)El apóstol Santiago el Menor, por El Greco (h. 1609).

Nacimiento: ?

Padres: Alfeo o Cleofas y María

Martirio: Arrojado del pináculo del templo y golpeado hasta la muerte

## **3.3.10 Simón el cananeo (zelote)**

![](/assets/images/simonzelote.png)Simón el Cananeo. José de Ribera, c. 1630.

Nacimiento: Caná

Martirio: ?

## **3.3.11. Judas o Judas Tadeo (hijo de Santiago)**

![](/assets/images/judastadeo.png)San Judas Tadeo (ca. 1609-1610), óleo sobre lienzo de José de Ribera.

Nacimiento: Caná

Martirio: Epístola de Judá. Muerto a tiros con flechas.

## **3.3.12. Judas Iscariote**

![](/assets/images/judasiscariote.png)Remordimiento de Judas, de José Ferraz de Almeida Júnior, 1880.

Nacimiento: Keirot

No fue martirizado, se suicidó.

Remplazado con Matias

![](/assets/images/matias.png)

Nacimiento: Judea

Llamamiento: “21 Es necesario, pues, que de estos hombres que han estado juntos con nosotros todo el tiempo que el Señor Jesús entraba y salía entre nosotros, 22 comenzando desde el bautismo de Juan hasta el día en que de entre nosotros fue recibido arriba, uno sea hecho testigo con nosotros, de su resurrección. 23 Y señalaron a dos: a José, llamado Barsabás, que tenía por sobrenombre Justo, y a Matías. 24 Y orando, dijeron: Tú, Señor, que conoces los corazones de todos, muestra cuál de estos dos has escogido, 25 para que tome la parte de este ministerio y apostolado, de que cayó Judas por transgresión, para irse a su propio lugar. 26 Y les echaron suertes, y la suerte cayó sobre Matías; y fue contado con los once apóstoles.” Hechos 1:21-26

Martirio: Lapidación y crucifixión

# **4. Análisis**

* Significado de apóstol “enviado”
* Es interesante que Jesús haya elegido 12 discípulos. Que las tribús de Juda fueran 12 y los siguientes pasajes (hay muchas otras menciones de 12 [https://misionesonline.net/2012/12/13/el-n-mero-12-es-usado-187-veces-en-la-biblia/](https://misionesonline.net/2012/12/13/el-n-mero-12-es-usado-187-veces-en-la-biblia/ "https://misionesonline.net/2012/12/13/el-n-mero-12-es-usado-187-veces-en-la-biblia/") ):
  * Josue 4:5-7:5 “Y les dijo Josué: Pasad delante del arca de Jehová vuestro Dios a la mitad del Jordán, y cada uno de vosotros tome una piedra sobre su hombro, conforme al número de las tribus de los hijos de Israel, 6 para que esto sea señal entre vosotros; y cuando vuestros hijos preguntaren a sus padres mañana, diciendo: ¿Qué significan estas piedras? 7 les responderéis: Que las aguas del Jordán fueron divididas delante del arca del pacto de Jehová; cuando ella pasó el Jordán, las aguas del Jordán se dividieron; y estas piedras servirán de monumento conmemorativo a los hijos de Israel para siempre.”
  * 1 Reyes 18:31-32 “31 Y tomando Elías doce piedras, conforme al número de las tribus de los hijos de Jacob, al cual había sido dada palabra de Jehová diciendo, Israel será tu nombre, 32 edificó con las piedras un altar en el nombre de Jehová;”
  * Por lo visto con doce piedras se puede armar una buena estructura para un altar.
  * Mt 16:15-18 15 Él les dijo: Y vosotros, ¿quién decís que soy yo? 16 Respondiendo Simón Pedro, dijo: Tú eres el Cristo, el Hijo del Dios viviente. 17 Entonces le respondió Jesús: Bienaventurado eres, Simón, hijo de Jonás, porque no te lo reveló carne ni sangre, sino mi Padre que está en los cielos. 18 Y yo también te digo, que tú eres Pedro, y sobre esta roca edificaré mi iglesia; y las puertas del Hades no prevalecerán contra ella.
  * 1 Pedro 2:5 “vosotros también, como piedras vivas, sed edificados como casa espiritual y sacerdocio santo, para ofrecer sacrificios espirituales aceptables a Dios por medio de Jesucristo.”
  * Por lo visto 12 personas es buen número para un discipulado que es como un altar para alaba a Dios, conformando iglesia para alabar a Dios sobre la piedra fundamental que es Jesucristo.
* Cuál es el costo de ser discípulo. “El discipulado cuesta todo lo que tenemos” Niégate a ti mismo Mc 8,34-38. Cambio de estilo de vida para convertirse en “sal de la tierra” y “luz del mundo” \[Heckman2014\]

¿Diferencias entre el discipulado de Jesús y el discipulado de los fariseos?

* Los discípulos conocieron a Jesús y vieron sus milagros antes de ser llamados y aceptar seguirlo y dejarlo todo. \[Bruce\]
* Los discípulos fariseos esperaban la aprobación de los hombres. Los discípulos de Jesús esperan la aprobación de Dios. \[Ernie\]
* Falso discipulado de fariseos que eran hipócritas e imponían reglas a los hombres. Se llamaban a sí mismos puros e impuros los que no seguían sus reglas. \[Bruce\]
* Los discípulos deben mudarse a cualquier región/nación “sin temor a que sus vidas sean contaminadas o desagradables a Dios”. \[Bruce\]
* Jesús envió a sus discípulos y les dio el Espíritu Santo. Los fariseos no podían.

# **5. Aplicación personal y oración**

Necesito orar más como Jesús. Especialmente antes de decisiones importantes.

Cómo hacer discípulos hoy

* Dos puntos importantes \[Bruce\]
  * ¿Qué quiere Dios de esta persona?
  * Conoce antecedentes.
* Enseñar a seguir a Jesús y enfocarnos en el Señor no en nosotros mismos.
* Dios escogió a los débiles. Prácticamente todos de Galilea, muchos pescadores. De lo humilde. \[Phil\]
* Es un honor ser discípulo de Jesús.
* No descalificarme aunque este lejos de ser perfecto --hasta Pedro negó 3 veces a Jesús, pero Jesús no dejó de esperar en él y lo restauró.
* Preguntar a Dios qué quieres que haga
* Para hacer buenos discípulos primero necesito ser un buen discípulo. La organización de la iglesia con Jesús como roca fundamental, 12 discípulos que son piedras, 72 y todos los que quieran unirse, es un orden para alcanzar más, no para que la mayor autoridad de unos sea para oprimir a otros.

Señor ayudame a ser buena piedra viva en tu iglesia, sendo discípulo sujeto y cuando sea el momento discipulando.

Ayudame a ser humile y usame, eres Tu quien hace el milagro de la conversón, nosotros sólo compartimos lo que has hecho.

Por favor perdoname lo que no he hecho bien como discípulo o discipulando, enseñame como hacerlo bien en el nombre de Jesús.

Si quieres ser discípulo de Jesús y aún no has hecho una oración de Fe, es buen momento de hacerla: [https://fe.pasosdejesus.org/Oracion_de_fe/](https://fe.pasosdejesus.org/Oracion_de_fe/ "https://fe.pasosdejesus.org/Oracion_de_fe/")

# **6. Referencias**

* Biblia. Reina Valera 1960.
* Bruce Heckman 5 Principios de los estudios bíblicos de descubrimiento [https://docs.google.com/document/d/1UyBLhfA7bR17bA3Mw9lSwTNmk7-WHvk5GpYqjK6DJu4/edit?usp=sharing](https://docs.google.com/document/d/1UyBLhfA7bR17bA3Mw9lSwTNmk7-WHvk5GpYqjK6DJu4/edit?usp=sharing "https://docs.google.com/document/d/1UyBLhfA7bR17bA3Mw9lSwTNmk7-WHvk5GpYqjK6DJu4/edit?usp=sharing")
* Comentario de Matthew Henry
* [https://www1.cbn.com/onlinediscipleship/resurrection-proof-the-disciples-sacrifice](https://www1.cbn.com/onlinediscipleship/resurrection-proof-the-disciples-sacrifice "https://www1.cbn.com/onlinediscipleship/resurrection-proof-the-disciples-sacrifice")
* [https://www.gotquestions.org/Nathanael-in-the-Bible.html](https://www.gotquestions.org/Nathanael-in-the-Bible.html "https://www.gotquestions.org/Nathanael-in-the-Bible.html")
* Bruce Heckman. 2016. Corazón y mandato de Dios por la misión. Capítulo 22. Discipulado en el Reino de Dios.
* Ideas de [https://docs.google.com/document/d/1y8oTHO2ZRQ_8kM13Fu6DoWQL4Svm4HufHCZ-2xdLfdM/edit?usp=sharing](https://docs.google.com/document/d/1y8oTHO2ZRQ_8kM13Fu6DoWQL4Svm4HufHCZ-2xdLfdM/edit?usp=sharing "https://docs.google.com/document/d/1y8oTHO2ZRQ_8kM13Fu6DoWQL4Svm4HufHCZ-2xdLfdM/edit?usp=sharing")
* [https://es.wikipedia.org/wiki/Ap%C3%B3stol](https://es.wikipedia.org/wiki/Ap%C3%B3stol "https://es.wikipedia.org/wiki/Ap%C3%B3stol")

***

Gracias a Dios [vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org) compartió esta prédica el 28.Ago.2022 en la Iglesia Menonita de Suba.

La pintura introductoria es "La última cena" de Carl Heinrich Bloch (1834-1890). [https://public-domain-images.blogspot.com/2010/10/carl-heinrich-bloch-last-supper.html](https://public-domain-images.blogspot.com/2010/10/carl-heinrich-bloch-last-supper.html "https://public-domain-images.blogspot.com/2010/10/carl-heinrich-bloch-last-supper.html")