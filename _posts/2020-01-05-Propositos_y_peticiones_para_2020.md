---
layout: post
title: Propósitos y peticiones para 2020
author: vtamara
categories:
- Prédica
image: "/assets/images/propositos_y_peticiones_2020.png"
tags: Prédica

---
# Propósitos y peticiones para 2020

# Iglesia Menonita de Suba, Caminos de Esperanza

# 1. Introducción

Con motivo del inicio del 2020, he propuesto predicar sobre propósitos y peticiones para el 2020.

# 2. Texto: Jeremias 29:10-14

10 Porque así dijo Jehová: Cuando en Babilonia se cumplan los setenta años, yo os visitaré, y despertaré sobre vosotros mi buena palabra, para haceros volver a este lugar.

11 Porque yo sé los pensamientos que tengo acerca de vosotros, dice Jehová, pensamientos de paz, y no de mal, para daros el fin que esperáis.

12 Entonces me invocaréis, y vendréis y oraréis a mí, y yo os oiré;

13 y me buscaréis y me hallaréis, porque me buscaréis de todo vuestro corazón.

14 Y seré hallado por vosotros, dice Jehová, y haré volver vuestra cautividad, y os reuniré de todas las naciones y de todos los lugares adonde os arrojé, dice Jehová; y os haré volver al lugar de donde os hice llevar.

# 3. Contexto

Estas palabras son parte de una carta que el profeta Jeremías les envió a los israelitas que habían sido deportados a Babilonia hacia el año 600aC.

Antes de la deportación, Jeremías les decía que se convirtieran al Señor y dejaran la idolatría y la desobediencia. Jeremías les había anunciado que Israel sería derrotado por Babilonia y que serían llevados cautivos, y aunque no lo escucharon a tiempo, se cumplió esa profecía, y Jeremías que había sido encarcelado por los Israelitas por sus profecias, fue liberado por los Babilonios durante la toma a Jerusalén (como ilustración ver video de 1.5 minutos [https://www.youtube.com/watch?v=HjqItNhJZvU&list=PLMBjSzDl9ktdn-_rveBuLrVF8Pyyq2IGk&index=30&t=0s](https://www.youtube.com/watch?v=HjqItNhJZvU&list=PLMBjSzDl9ktdn-_rveBuLrVF8Pyyq2IGk&index=30&t=0s "https://www.youtube.com/watch?v=HjqItNhJZvU&list=PLMBjSzDl9ktdn-_rveBuLrVF8Pyyq2IGk&index=30&t=0s")). Seguramente los Judios cautivos en Babilonia ya prestaron atención a Jeremías.

¿Está pasando por un momento dificil en la vida? ¿Cual es su Babilonia en este momento?

El Señor quiere restaurarnos y hacernos pasar la prueba, pero requiere que lo búsquemos de TODO corazón.

En los versículos 5 a 7 del mismo capítulo, y aún cuando estaban en terrible prueba, el Señor les dijo a los Israelitas cautivos:

5 Edificad casas, y habitadlas; y plantad huertos, y comed del fruto de ellos.

6 Casaos, y engendrad hijos e hijas; dad mujeres a vuestros hijos, y dad maridos a vuestras hijas, para que tengan hijos e hijas; y multiplicaos ahí, y no os disminuyáis.

7 Y procurad la paz de la ciudad a la cual os hice transportar, y rogad por ella a Jehová; porque en su paz tendréis vosotros paz.

Así que hoy, estés o no en prueba, el Señor te repite esas palabras, hoy te invita a planear con fe, confianza y optimismo este año 2020.

Les propongo emplear los esquemas anexos, para planear y pedir lo que anhelamos en el 2020 [/assets/images/propositos_y_peticiones_2020.pdf], mientras agradecemos con el mismo esquema las bendiciones del 2019 [/assets/images/bendiciones_2019.pdf].

![](/assets/images/bendiciones_2019.png)

![](/assets/images/propositos_y_peticiones_2020.png)

# 4. Análisis

Mientras cada quien avanza en su diagrama de bendiciones 2019 y propósitos y peticiones 2020, vamos a leer algunos versículos, uno cada minuto.

Salmo 37:4-5 Deléitate asimismo en Jehová, Y él te concederá las peticiones de tu corazón. Encomienda a Jehová tu camino, Y confía en él; y él hará.

Salmo 138:9 Jehová cumplirá su propósito en mí; Tu misericordia, oh Jehová, es para siempre; No desampares la obra de tus manos.

Proverbios 16:3 Encomienda a Jehová tus obras, Y tus pensamientos serán afirmados.

Malaquias 3:10-12 10 Traed todos los diezmos al alfolí y haya alimento en mi casa; y probadme ahora en esto, dice Jehová de los ejércitos, si no os abriré las ventanas de los cielos, y derramaré sobre vosotros bendición hasta que sobreabunde.

11 Reprenderé también por vosotros al devorador, y no os destruirá el fruto de la tierra, ni vuestra vid en el campo será estéril, dice Jehová de los ejércitos.

12 Y todas las naciones os dirán bienaventurados; porque seréis tierra deseable, dice Jehová de los ejércitos.

Ezequiel 36:26-28 Os daré corazón nuevo, y pondré espíritu nuevo dentro de vosotros; y quitaré de vuestra carne el corazón de piedra, y os daré un corazón de carne.

27 Y pondré dentro de vosotros mi Espíritu, y haré que andéis en mis estatutos, y guardéis mis preceptos, y los pongáis por obra.

28 Habitaréis en la tierra que di a vuestros padres, y vosotros me seréis por pueblo, y yo seré a vosotros por Dios.

# 5. Conclusión y Oración

Señor gracias por las bendiciones del 2019. Por acompañarnos, cuidarnos, sanarnos, proveernos, enseñarnos, guiarnos.

Señor ponemos nuestros corazones en tus manos, para que los moldees de acuerdo a tu voluntad, haznos nuevos este año 2020, para que pidamos conforme a tus propósitos que son buenos.

Por favor ayúdanos a dejar la pereza y la indiferencia, ayúdanos a plantear propósitos y peticiones para este 2020, a planearlos y a llevar a cabo nuestra parte y confiar en que Tu harás la tuya.

Por favor ayúdanos a traer este año los diezmos, ofrendas y primicias y a confiar en ti, que nos proveerás.

Señor Jesús que este año más personas conozcan de Ti y que te reciban como Señor y Salvador. Que en esta iglesia hagamos nuestra parte de esa gran comisión compartiendo tu palabra, invitando a la iglesia, acogiendo, siendo solidarios y confiando en que Tu Santo Espíritu convencerá en el nombre de Jesús.

***

Estra predica también está disponible en [https://docs.google.com/document/d/1WfTEQg5Si3nLEybbj7AWGoQW7rcdb7rE3xOYMv5abx0/edit?usp=sharing](https://docs.google.com/document/d/1WfTEQg5Si3nLEybbj7AWGoQW7rcdb7rE3xOYMv5abx0/edit?usp=sharing "https://docs.google.com/document/d/1WfTEQg5Si3nLEybbj7AWGoQW7rcdb7rE3xOYMv5abx0/edit?usp=sharing")