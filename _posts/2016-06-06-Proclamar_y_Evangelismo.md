---
layout: post
title: Proclamar y Evangelismo
author: vtamara
categories:
- Prédica
image: "/assets/images/98-01.jpg"
tags: Prédica

---

1. INTRODUCCIÓN

Me llenó de esperanza leer en 2 Timoteo 4:7 "He peleado la buena batalla, he
acabado la carrera, he guardado la fe." Pablo compara guardar la fe con una
batalla o una carrera, lo mismo en 1 Timoteo 6:12 (Pelea la buena batalla de la
fe, echa mano de la vida eterna, a la cual asimismo fuiste llamado, habiendo
hecho la buena profesión delante de muchos testigos). Anhelo esa certeza de
haber hecho bien las cosas que Pablo expresa, estudiemos que es lo que Pablo
recomienda al respecto. 
