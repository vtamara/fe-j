---
layout: post
title: "El_sonido_del_silencio"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Alex Campos


<pre>
El Sonido del silencio, el que no quiero escuchar
Es aquella noche fría en la que quiero evitar, el sentirme
Descubierto cuando el sol me quemara.

Para que seguir riendo cuando siento que no estas,
Para que quiero los mares si mi barco se hundirá, para que
Seguir viviendo si a lo lejos tu estas.

Que las fuerzas se me agotan mi alma esta por comenzar
Otro día en silencio el que acaba de pasar,  pasa y pasan
Los minutos en mi oscura soledad,

CORO

Soledad que se alimenta
Del silencio de tu boca, esa boca que sonríe pronunciando así
Mi nombre, aquel nombre que me diste diciendo
Que si mi amor, es mi amor que en ti espera y que siempre
Esperara, es tu amor que me condena a esta eterna libertad,
Y aunque pasen mil silencios, pronto se que me hablaras...aaaa
Pronto se que me hablaras?..aaaaaaa

El sonido del silencio donde se que escucharas, el susurro
De mi canto y el grito de mi llamar, el llamado de mi alma
Pidiendo tú libertad.

Yo quiero seguir riendo aunque el llanto aquí esta,
Aunque el barco se me hunda se que yo podre nadar, la
Corriente de este rio a tu amor me llevará.

Tu eres mi fortaleza, Mi escudo y mi lanza, eres todo lo
Que tengo cuando siento que no estas, eres tu mi compañía
En esta oscura soledad, 

CORO

Vuelve, vuelve el momento, de escucharte, en tu silencio
“”Vuelve, vuelve el momento, de escucharte, en tu silencio””

CORO
</pre>

* Letra basada en:
* Video:
