---
layout: post
title: "Drogas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En la Biblia no se menciona explicitamente las drogas ni del cigarrillo como se conocen hoy, pero si se habla de otra sustancia adictiva: el alcohol.

Rom 13:12-14
<pre>
12 La noche está avanzada, y se acerca el día. Desechemos, pues, las obras
   de las tinieblas, y vistámonos las armas de la luz.
13 Andemos como de día, honestamente; no en glotonerías y borracheras, 
   no en lujurias y lascivias, no en contiendas y envidia,
14 sino vestíos del Señor Jesucristo, y no proveáis para los deseos de 
   la carne.
</pre>

Gal 5:19-21
<pre>
19 Y manifiestas son las obras de la carne, que son: adulterio, fornicación, 
inmundicia, lascivia,

20 idolatría, hechicerías, enemistades, pleitos, celos, iras, contiendas, 
disensiones, herejías,

21 envidias, homicidios, borracheras, orgías, y cosas semejantes a estas; 
acerca de las cuales os amonesto, como ya os lo he dicho antes, que los que 
practican tales cosas no heredarán el reino de Dios.
</pre>

1 Cor 6:9-11

<pre>
9 ¿No sabéis que los injustos no heredarán el reino de Dios? No erréis; ni 
los fornicarios, ni los idólatras, ni los adúlteros, ni los afeminados, ni 
los que se echan con varones,
10 ni los ladrones, ni los avaros, ni los borrachos, ni los maldicientes, ni 
los estafadores, heredarán el reino de Dios.
11 Y esto erais algunos; mas ya habéis sido lavados, ya habéis sido 
santificados, ya habéis sido justificados en el nombre del Señor Jesús, y por 
el Espíritu de nuestro Dios.
</pre>


!Referencias

* {1} Biblia. RV1960.
* {2} http://predicandounmensajedesalvacion.over-blog.com/article-las-drogas-en-la-biblia-79066023.html
