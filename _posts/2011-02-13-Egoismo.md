---
layout: post
title: "Egoismo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Como resalta Hector Mondragon en una prédica (ver {2}), los pecados sexuales son graves, debemos evitarlo como dice 2 Timoteo 2:22, sin embargo también son muy graves otros pecados poco promovidos por los poderosos: la avaricia, el egoísmo y no compartir con los pobres.

!Egoismo 
1 Timoteo 6:9 porque los que quieren enriquecerse caen en tentación y lazo, y en muchas codicias necias y dañosas, que hunden a los hombres en destrucción y perdición; 

1 Fil 2:3-4 
|3 Nada hagáis por contienda o por vanagloria; antes bien con humildad, estimando cada uno a los demás como superiores a él mismo;|
|4 no mirando cada uno por lo suyo propio, sino cada cual también por lo de los otros. |

Como dice {3} nuestro remedio es la humildad y recordar a Mat. 16:24, "niéguese a sí mismo, y tome su cruz, y sígame".

!Despojar a los vulnerables 

Isaías 1:13-17

Veamos un ejemplo reciente de como los poderosos anteponen sus intereses económicos sobre la vida y el bien común por ejemplo en {4}.





## REFERENCIAS

* {1} Biblia. Reina Valera. 1960
* {2} http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-22oct2006.shtml Lo que dice la Biblia. Hector Mondragón. Predica en Iglesia Menonita de Teusaquillo. 22 de octubre de 2006
* {3} http://www.amigoval.com/WP/Sermones/s708.html
* {4} http://justiciaypazcolombia.com/Wikileaks-revela-el-modus-operandi
