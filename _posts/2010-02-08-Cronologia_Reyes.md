---
layout: post
title: Cronologia de los Reyes de Juda e Israel
author: vtamara
categories:
- Biblia
image: "/assets/images/two-kings.jpg"
tags: 

---
# 1. Introducción

No es fácil establecer una cronología para los reyes de Israel y Juda con base en los escritos bíblicos. Como lo indica Rodgers es como armar un rompecabezas lógico con más de 120 pistas.   La tarea sin embargo, según Rodgers, ya ha sido completada y resulta concordante con eventos narrados en registros antiguos de Babilonia y Asiria (ver {2}).

La cronología propuesta por Rodgers (basada en la de Thiele) puede verse en {1} donde también hay artículos que detallan como se derivó.

Por su parte Leslie McFall también ha publicado una cronología concordante con la de Rodgers (ver {3}).

Otra cronología que demuestra bastante esfuerzo es la de Dan Bruce (ver
{DAN}).

A continuación se traducen dos tablas de {1}, una explicación sobre la notación empleada y la tabla de {3}.  Los nombres de los reyes son tomados de Reina Valera 1960.

## Explicación de la notación "Nisan/Tishri"

931n	El año que comienza en el primero de Nisan (un mes de primavera) del 931 aC.  Los años de  reinado fueron contados desde Nisan 1 en Asiria, Babilonia e Israel (i.e Reino del Norte o Samaria).

931t	El año que comienza en el primero de Tishri de 931 aC.  Tishri es un mes de otoño, 6 meses después de Nisan.  Los años de reinado fueron contados desde el 1 de Tishri en Judea.  Los años sabáticos comenzaban  el 1 de Tishri. Los años de Jubileo comenzaban el 10 de Tishri.

931n/931t	El periodo de seis meses que comienza en Nisan 1 de 931 aC.

931t/930n	El periodo de seís meses que comienza en Tishri 1 de 931 aC.

# Reyes de Israel (Rodgers)

| Rey | Reinos traslapados | Comienzo de reinado solo | Finalización | Inicio y fin oficial | Años que reinó |
| --- | --- | --- | --- | --- | --- |
| Jeroboam I |  | 931n | 910t/909n | 931n - 910n | 22 (21) |
| Nadab |  | 910t/909n | 909t/908n | 910n - 909n | 2 (1) |
| Baasa |  | 909t/908n | 886t/885n | 909n - 886n | 24 (23) |
| Ela |  | 886t/885n | 885t/884n | 886n - 885n | 2 (1) |
| Zimri |  | 885t/884n | 885t/884n | 885n | 7 días |
| Tibni |  | 885t/884n | 880n/880t | 885n - 880n | (no establecido) |
| Omri | 885t/884n | 880n/880t | 874t/873n | 885n - 874n | 12 (11) |
| Acab |  | 874t/873n | 853n/853t | 874n - 853n | 22 (21) |
| Ocozías |  | 853n/853t | 852n/852t | 853n - 852n | 2 (1) |
| Joram |  | 852n/852t | 841n/841t | 852n - 841n | 12 (11) |
| Jehu |  | 841n/841t | 814t/813n | 841n - 814n | 28 (27) |
| Joacaz |  | 814t/831n | 798n/798t | 814n - 798n | 17 (16) |
| Joás |  | 798n/798t | 782t/781n | 798n - 782n | 16 |
| Jeroboam II | 793n | 782t/781n | 753,  Elul (Sep) | 793n - 753n | 41 (40) |
| Zacarías |  | 753, Elul | 752, Adar (Mar) | 753n - 753n | 6 meses |
| Salum |  | 752, Adar | 752, Nisan (Abr) | 753n - 752n | 1 mes |
| Manahem |  | 752, Nisan | 742t/741n | 752n - 742n | 10 |
| Pekaía |  | 742t/741n | 740t/739n | 742n - 740n | 2 |
| Peka | 752, Nisan | 740t/739n | 732t/731n | 752n - 732n | 20 |
| Oseas |  | 732t/731n | 723n/723t | 732n - 723n | 9 |

La quinta columna tiene las fechas inicial y final del reinado (en notación Nisan/Tishri) que fueron usadas por los escribas de la corte para determinar la longitud de los reinados.  La Columna 6 tiene los años transcurridos basados en la fecha inicial y final.  Cuando hay dos figuras en la columna 6, esto indica que la longitud del reinado dada en la Escritura (la primera figura) es por un conteo por no-accesión.  La comparación entre la columna 5 con las fechas iniciales y finales y con la columna 6 muestra que los escribas de la corte de Israel siempre tenían en mente el año oficial de inicio de un reinado y fueron exactos en todas las representaciones de las longitudes de sus reinados.

## Reyes de Judá (Rodgers)

| Rey | Comienzo de coreino | Comienzo de reinado solo | Finalización | Inicio y fin oficial | Años que reinó |
| --- | --- | --- | --- | --- | --- |
| Saul |  | 1051t? | 1009t? | 1051t-1009t? | 42? |
| David |  | 1009t? | 969t? | 1009t-969t? | 40 |
| Salomón | 971t | 969t? | 932t | 971t-932t | 40(39) |
| Roboam |  | 932t | 914n/914t | 932t-915t | 17 |
| Abiam |  | 914n/914t | 912t/911n | 915t-912t | 3 |
| Asa |  | 912t/911n | 871t/870n | 912t-871t | 41 |
| Josafat | 873t | 871t/870n | 848n/848t | 873t-849t | 25(24) |
| Joram | 854t | 848n/848t | 841n/841t | 849t-842t | 8(7) |
| Ocozías |  | 841n/841t | 841n/841t | 842t-842t | 1(0) |
| Atalía |  | 841n/841t | 835n/835t | 842t-836t | 7(6) |
| Joas |  | 835n/835t | 796n/796t | 836t-797t | 40(39) |
| Amasías |  | 796n/796t | 767n/767t | 797t-768t | 29 |
| Uzias/Azarías | 791t | 767n/767t | 740t | 791t-740t | 52(51) |
| Jotam | 750n/750t | 740t | (735n/735t) 732t | 751t-736t | 16(15) |
| Acaz | 735n/735t | 732t | 716t/715n | 732t-716t | 16 |
| Ezequías | 729t/728n | 716t/715n | 687t | 716t-687t | 29 |
| Manasés | 697t | 687t | 643t | 697t-643t | 55(54) |
| Amón |  | 643t | 641t | 643t-641t | 2 |
| Josías |  | 641t | 609 Tammuz (Jul) | 641t-610t | 31 |
| Joacaz |  | 609 Tammuz | 609 Tishri (Oct) | 610t-609t | 3 meses |
| Joacim |  | 609 Tishri | 598 21 Heshvan (cerca 9 Dic 598) | 609t-598t | 11 |
| Joaquín |  | 598 21 Heshvan | 597 2 Adar, (Mar 17) | 598t | 3 meses 10 d. |
| Sedequías |  | 597 2 Adar | 587 9 Tammuz (Jul 27) | 598t-588t | 11(10) |

Algunas longitudes de reinados se miden desde el comienzo de una correinado. Los 16 (15) años de Jotam terminaron cuando su hijo Acaz fue instalado por la facción pro-Asiría en Judá, en 735n/735t, aunque algunos lo consideran el dirigente hasta su muerte en 732t, dandole así los veinte años mencionados en 2 Reyes 15:30.  La comparación de la columna 5 con las fechas de inicio y fin y con la columna 6 muestra que los escribas de la corte de Juda siempre tenían en mente el año oficial de inicio de un rey y fueron exactos en todas sus representaciones de la longitud del reinado.

## Reyes de Juda e Israel (McFall)

En la siguiente tabla los reyes de Juda aparecen en mayúsculas mientras que los reyes de Israel están en minúsculas.

| Secuencia Bíblica | Juda e Israel | Correinado (comienzo) | Reinado (comienzo o reinado solo) | Muerte (o último año) |
| --- | --- | --- | --- | --- |
| 1 Samuel 10 | _SAUL_ |  | SEP 1051  | SEP 1009 |
| 1 Reyes 1:1-2:11 | _DAVID_ |  | ABR 1010 | ABR 971 - ABR 970 |
| 1 Reyes 2:11-11:43. | _SALOMÓN_ |  | ABR 970 | ABR 931 - SEPT 931 |
| 1 Reyes 12:1-14:20 | Jeroboam I |  | Sept 931 - Abr 930 | Sept 910 - Abr 909 |
| 1 Reyes 14:21-31. | _ROBOAM_ |  | ABR 931 - SEPT 931 | ABR - SEPT 914 |
| 1 Reyes 15:1-8 | _ABIAM_ |  | ABR - SEPT 914 | SEPT 912 - ABR 911 |
| 1 Reyes 15:9-24 | _ASA_ |  | SEPT 912 - ABR 911 | SEPT 871 - ABR 870 |
| 1 Reyes 15:25-32 | Nadab |  | Sept 910 - Abr 909 | Sept 909 - Abr 908 |
| 1 Reyes 15:33-16:7 | Baasa |  | Sept 909 - Abr 908 | Sept 886 - Abr 885 |
| 1 Reyes 16:8-14 | Ela |  | Sept 886 - Abr 885 | Sept 885 - Abr 884 |
| 1 Reyes 16:15-20 | Zimri |  | Sept 885 - Abr 884 | Sept 885 - Abr 884 |
| 1 Reyes 16:21-22 | Tibni | Sept 885 - Abr 884 |  | Abr 880 - Sept 880 |
| 1 Reyes 16:23-28 | Omri | Sept 885 - Abr 884 | Abr 880 - Sept 880 | Sept 874 - Abr 873 |
| 1 Reyes 16:29-22:40 | Acab |  | Sept 874 - Abr 873 | Abr - Sept 853 |
| 1 Reyes 22:41-51 | _JOSAFAT_ | SEPT 873 - | SEPT 871 - ABR 870 | ABR - SEPT 848 |
| 1 Reyes 22:40. | Ocozías |  | Abr - Sept 853 | Abr - Sept 852 |
| 2 Reyes 3:1-8:15 | Joram |  | Abr - Sept 852 | Abr - Sept 841 |
| 2 Reyes 8:16-24 | _JORAM_ | SEPT 854 - | ABR - SEPT 848 | ABR - SEPT 841 |
| 2 Reyes 8:25-9:29 | _OCOZÍAS_ | SEPT 842 - | ABR - SEPT 841 | ABR - SEPT 841 |
| 2 Reyes 9:30-10:36. | Jehu |  | Abr - Sept 841 | Sept 814 - Abr 813 |
| 2 Reyes 11:1-21 | _ATALÍA_ |  | ABR - SEPT 841 | ABR - SEPT 835 |
| 2 Reyes 12:1-21 | _JOAS_ |  | ABR - SEPT 835 | ABR - SEPT 796 |
| 2 Reyes 13:1-10 | Joacaz |  | Sept 814 - Abr 813 | Sept 798 - Abr 797 |
| 2 Reyes 13:11-25 | Joás | Abr 799 - | Sept 798 - Abr 797 | Sept 782 - Abr 781 |
| 2 Reyes 14:1-22 | _AMASÍAS_ |  | ABR - SEPT 796 | ABR - SEPT 767 |
| 2 Reyes 14:23-29 | Jeroboam II | Abr 793 - | Sept 782 - Abr 781 | Ago/Sept 753 |
| 2 Reyes 15:1-7 | _AZARÍAS o UZÍAS_ | SEPT 791 - | ABR - SEPT 767 | ABR - SEPT 739 |
| 2 Reyes 15:8-12 | Zacarías |  | Ago/Sept 753 | Mar 752 |
| 2 Reyes 15:13-15 | Salum |  | Mar 752 | final de Abr 752 |
| 2 Reyes 15:16-22 | Manahem | final Abr 752 - | (divisón con Peka) | Sept 742 - Abr 741 |
| 2 Reyes 15:23-26 | Pekaía |  | Sept 742 - Abr 741 | Sept 740 - Abr 739 |
| 2 Reyes 15:27-31 | Peka | final Abr 752 - | Sept 740 - Abr 739 | Sept 732 - Abr 731 |
| 2 Reyes 15:32-38. | _JOTAM_ | ABR- SEPT 750 - | ABR - SEPT 739 TO | SEPT 732 - SEPT 731 |
| 2 Reyes 16:1-20. | _ACAZ_ | SEPT 735 - | SEPT 732 - SEPT 731 | C. MAR 715 |
| 2 Reyes 17:1-41 | Oseas |  | Sept 732 - Abr 731 | Abr - Sept 723 |
| 2 Reyes 18:1-20:21 | _EZEQUÍAS_ | SEPT 729 - | C. MAR 715 | SEPT 687 - SEPT 686 |
| 2 Reyes 21:1-18 | _MANASÉS_ | SEPT 697 - | SEPT 697 - SEPT 686 | SEPT 643 - SEPT 642 |
| 2 Reyes 21:19-26 | _AMÓN_ |  | SEPT 643 - SEPT 642 | SEPT 641 - SEPT 640 |
| 2 Reyes 22:1-23:30 | _JOSÍAS_ |  | SEPT 641 - SEPT 640 | C. JUL 609 |
| 2 Reyes 23:31-35 | _JOACAZ_ |  | JUL 609 | c. OCT 609 |
| 2 Reyes 23:36-24:7 | _JOACIM_ |  | OCT 609 | 9 DIC 598 |
| 2 Reyes 24:8-17 | _JOAQUIN_ | SEPT 608 - | DIC 598 - ABR 597 | AFTER ABR 561 |
| 2 Reyes 24:18-25:7 | _SEDEQUÍAS_ | ABR 597 - AGO 586 | C. AGO 586 |  |

# Referencias

* {1} [http://home.swbell.net/rcyoung8/CTables/frame4.html](http://home.swbell.net/rcyoung8/CTables/frame4.html)
* {2} [http://home.swbell.net/rcyoung8/Unexpected1.pdf]()
* {3} [http://www.btinternet.com/\~lmf12/](http://www.btinternet.com/\~lmf12/)
* {4} Biblia.  Reina Valera 1960. <https://www.biblegateway.com/versions/Reina-Valera-1960-RVR1960-Biblia/>
* {DAN} BRUCE, Dane. Sacred Chronology: of the hebrew Kings. http://www.prophecysociety.org/PDF/SC_FREE.pdf

***

Foto de dominio público de [https://www.publicdomainpictures.net/en/view-image.php?image=55681&picture=two-kings](https://www.publicdomainpictures.net/en/view-image.php?image=55681&picture=two-kings "https://www.publicdomainpictures.net/en/view-image.php?image=55681&picture=two-kings")
