---
layout: post
title: "Oh_Moradora_de_Sion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Jaime Murrel

<pre>
Oh moradora de Sion alaba a Jehová.
Grandes son sus maravillas.

Sobre los montes y collados a mi amado buscaré.
Grande es su hermosura.

Y al estar en su presencia gozoso danzaré,
al Señor alabaré.

Grande es Jehova
digno de alabar.

Tu eres grande Jehova
grande es tu nombre.
</pre>


* Canción: http://www.youtube.com/watch?v=p0Q7rJXAFPU
* Letra basada en: http://www.musica.com/letras.asp?letra=1184955
