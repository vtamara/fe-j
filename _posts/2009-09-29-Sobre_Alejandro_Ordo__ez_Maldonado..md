---
layout: post
title: Sobre Alejandro Ordoñez Maldonado.
author: vtamara
categories: 
image: assets/images/home.jpg
tags: 

---
Alejandro Ordóñez Maldonado es el actual Procurador General de Colombia, infortunadamente ha beneficiado a políticos involucrados en parapolítica y ha perjudicado a las víctimas ayudando a mantener la verdad oculta, pero afortunadamente se ha opuesto al aborto abiertamente.

Tal ambivalencia ha sido aprovechada particularmente por la periodista María Jimena Duzán (ver {2}) para promover clínicas donde se practica el aborto, exaltar la ley de los hombres por encima de la de Dios e incluso burlarse del Procurador por ser devoto de Cristo Rey.

Para aclarar, el Señor Jesús (Cristo, Rey y Dios trino) nos enseña a proclamar con valor la verdad, por ejemplo en Mateo 10:26-27 (tomado de {1}) 

```
Así que, no los temáis; porque nada hay encubierto, 
que no haya de ser manifestado; ni oculto, que no 
haya de saberse. Lo que os digo en tinieblas, decidlo
en la luz; y lo que oís al oído, proclamadlo desde las azoteas.
```
o similar en Lucas 12:2-9

Ciertamente, el actual Procurador ha entorpecido investigaciones de parapolítica y chuza-DAS por lo menos así:

* Al ingresar a la Procuraduría absolvió al actual ministro Diego Palacio y el exministro Sabas Pretel que resultaron implicados en el caso de Yidis Medina quien favorecío la reelección de Uribe por prebendas.  Para absolverlos cambió por completo el concepto que el procurador anterior Maya Villazón había iniciado ---pero que el mismo Maya no quizo hacer público antes de salir del cargo.  Al respecto la Corte Suprema inició investigación en Septiembre (ver {3}).
* Amenazó a los investigadores de la Corte Suprema de Justicia por  entrevistar a los extraditados que están en carceles de Estados Unidos sin presencia de Ministerio Público. (ver {4}).
* Cerró una sala de interceptaciones de la Procuraduría, legal según el anterior procurador, justamente en el tiempo que las intercepciones ilegales del DAS ocupan titulares de prensa (ver {5}).
* Solicitó a la Corte que la investigación del ex-senador Álvaro García volviera a justicia ordinaria, tras decisión de la Corte de tomar todos los casos de ex-senadores que renunciaron a su fuero para no ser investigado por esa instancia (ver {7}).
* Archivó investigación contra senador Luis Fernando Velasco por vínculos con paramiltares, aunque públicamente se conoció evidencia de apoyo (ver {8}).

El señor Ordoñez para varias de estas decisiones argumenta los derechos de los procesados, y aunque efectivamente debe velarse porque se respeten, diversas decisiones y solicitudes que ha hecho parecen evitar investigaciones alrededor del Sr. Uribe Velez.  Por eso sería mejor otro Procurador que no sea cuota política del mismo.

Sin embargo, con respecto al aborto es el mismo Cristo Rey, quien a su vez es Dios, el que nos ha regalado la vida y la libertad.  Como para ejercer la libertad es necesario estar vivo, no puede argumentarse que la libertad esté por encima del derecho a la vida, como pretenden los proabortistas que proclaman falsamente el derecho de la mujer a decidir respecto a su cuerpo por encima del derecho a la vida del bebé (a quien además le niegan el derecho de decidir sobre su cuerpecito).

Aunque se supone que en Colombia el aborto es legal sólo en 3 casos, en la práctica se ha legalizado para todos los casos, pues cualquier mujer embarazado podría argumentar peligro de enfermedad mental en caso de no  interrumpir el embarazo (otros argumentos en {9}).

Puede que los cristianos fallemos en muchas cosas, que nos salgamos de las leyes de hombres o de Dios (por ejemplo al mentir, ocultar la verdad o favorecer a criminales), pero no es a Dios a quien debe culparse por nuestros pecados y el mal uso de nuestra libertad.  María Jimena hace bien en denunciar el encubrimiento del Procurador Ordeñez a delincuentes, pero hace mal al denigrar la Ley de Dios favoreciendo clínicas abortivas y al burlarse de la fe en Cristo Rey.

# Referencias

* {1} Biblia. Reina Valera, 1960. http://www.biblegateway.com/passage/?search=Mateo%2010&version=RVR1960
* {2} Mariá Jimena Duzán.  ¿En que orilla está Ordóñez? http://www.semana.com/noticias-opinion/orilla-esta-ordonez/129266.aspx
* {3} http://www.semana.com/noticias-justicia/corte-suprema-abre-indagacion-preliminar-contra-procurador/129225.aspx
* {4} http://www.elespectador.com/noticias/judicial/articulo163159-procurador-anuncia-acciones-contra-magistrados-entrevisten-paras-ex
* {5} http://www.eltiempo.com/colombia/justicia/al-menos-369-lineas-fijas-y-52-moviles-intercepto-la-procuraduria-desde-1997_6188370-1
* {6} http://www.eje21.com.co/index.php?option=com_content&task=view&id=16274&Itemid=1
* {7} http://www.verdadabierta.com/web31/component/content/article/54-generales/1701-procuraduria-le-pide-a-la-corte-que-devuelva-caso-de-alvaro-garcia-a-juzgados
* {8} http://www.verdadabierta.com/web31/component/content/article/54-generales/1693-procuraduria-archiva-investigacion-contra-el-senador-velasco
* {9} http://www.aciprensa.com/noticia.php?n=24543