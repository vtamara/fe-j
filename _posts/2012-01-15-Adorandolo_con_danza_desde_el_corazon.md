---
layout: post
title: Adorandolo con danza desde el corazon
author: vtamara
categories:
- Prédica
image: "/assets/images/77121271_112803006850842_435791768676466688_n.jpg"
tags: 

---
## 1. Introducción

Hay quienes critican el baile, con razón, pues se ha usado de mala manera (por ejemplo en relación  con la muerte de Juan el Bautista ver Mat 14:6),  pero la danza para alabar al Señor es necesaria según el Salmo 150.

Aunque desearimos abordar el tema con una mirada histórica, sociológica y cultural es un tema tan amplio que nos concentraremos en pasajes de la Biblia, indicando antes que la danza para Dios y para expresar alegría han acompañado al pueblo de Dios por lo menos desde que hay memoria escrita y hoy vive  tanto en el  tradicional Israel con danzas tradicionales como en el  Israel amplio de Cristo en multitud de formas por la también multiforme gracia de Dios.

## 2. Mensaje: Salmo 150.

```
1 Alabad a Dios en su santuario;
  Alabadle en la magnificencia de su firmamento.
2 Alabadle por sus proezas;
  Alabadle conforme a la muchedumbre de su grandeza.
3 Alabadle a son de bocina;
  Alabadle con salterio y arpa.
4 Alabadle con pandero y danza;
  Alabadle con cuerdas y flautas.
5 Alabadle con címbalos resonantes;
  Alabadle con címbalos de júbilo.
6 Todo lo que respira alabe a JAH.
  Aleluya.
```

## 3. ¿Cómo danzar para el Señor?

Aunque ya sabemos que es un requerimiento según el Salmo 150, veamos como hacerlo estudiando en la Biblia la palabra danzar y las situaciones en las que ha ocurrido.

### 3.1 Primero términos: ¿Debemos decir danzar o bailar?

Las palabras danzar y bailar aparecen en las Biblias en español de diversas formas dependiendo de la traducción, vamos a tomar como referencia la Reina Valera 1960 que es una hermosa y buena traducción.

Con la raiz bailar encontramos las siguientes 5 citas:

1. Jueces 21:21 y estad atentos; y cuando veáis salir a las hijas de Silo a bailar en corros, salid de las viñas, y arrebatad cada uno mujer para sí de las hijas de Silo, e idos a tierra de Benjamín.

2. Salmos 30:11 Has cambiado mi lamento en baile; Desataste mi cilicio, y me ceñiste de alegría.

3. Eclesiastés 3:4 tiempo de llorar, y tiempo de reír; tiempo de endechar, y tiempo de bailar;

4. Mateo 11:17 diciendo: Os tocamos flauta, y no bailasteis; os endechamos, y no lamentasteis.

5. Lucas 7:32 Semejantes son a los muchachos sentados en la plaza, que dan voces unos a otros y dicen: Os tocamos flauta, y no bailasteis; os endechamos, y no llorasteis.

Con la raiz danzar encontramos las siguientes 22:

1. Éxodo 15:20  Y María la profetisa, hermana de Aarón, tomó un pandero en su mano, y todas las mujeres salieron en pos de ella con panderos y danzas.

2. Éxodo 32:19  Y aconteció que cuando él llegó al campamento, y vio el becerro y las danzas, ardió la ira de Moisés, y arrojó las tablas de sus manos, y las quebró al pie del monte.

3. Jueces 11:34  Entonces volvió Jefté a Mizpa, a su casa; y he aquí su hija que salía a recibirle con panderos y danzas, y ella era sola, su hija única; no tenía fuera de ella hijo ni hija.

4. Jueces 21:23  Y los hijos de Benjamín lo hicieron así; y tomaron mujeres conforme a su número, robándolas de entre las que danzaban; y se fueron, y volvieron a su heredad, y reedificaron las ciudades, y habitaron en ellas.

5. 1 Samuel 18:6  Aconteció que cuando volvían ellos, cuando David volvió de matar al filisteo, salieron las mujeres de todas las ciudades de Israel cantando y danzando, para recibir al rey Saúl, con panderos, con cánticos de alegría y con instrumentos de música.

6. 1 Samuel 18:7  Y cantaban las mujeres que danzaban, y decían: Saúl hirió a sus miles, Y David a sus diez miles.

7. 1 Samuel 21:1  Y los siervos de Aquis le dijeron: ¿No es éste David, el rey de la tierra? ¿no es éste de quien cantaban en las danzas, diciendo: Hirió Saúl a sus miles, Y David a sus diez miles?

8. 1 Samuel 29:5 ¿No es éste David, de quien cantaban en las danzas, diciendo: Saúl hirió a sus miles, Y David a sus diez miles?

9. 2 Samuel 6:5  Y David y toda la casa de Israel danzaban delante de Jehová con toda clase de instrumentos de madera de haya; con arpas, salterios, panderos, flautas y címbalos.

10. 2 Samuel 6:14  Y David danzaba con toda su fuerza delante de Jehová; y estaba David vestido con un efod de lino.

11. 2 Samuel 6:16  Cuando el arca de Jehová llegó a la ciudad de David, aconteció que Mical hija de Saúl miró desde una ventana, y vio al rey David que saltaba y danzaba delante de Jehová; y le menospreció en su corazón.

12. 2 Samuel 6:21  Entonces David respondió a Mical: Fue delante de Jehová, quien me eligió en preferencia a tu padre y a toda tu casa, para constituirme por príncipe sobre el pueblo de Jehová, sobre Israel. Por tanto, danzaré delante de Jehová.

13. 1 Crónicas 15:29  Pero cuando el arca del pacto de Jehová llegó a la ciudad de David, Mical, hija de Saúl, mirando por una ventana, vio al rey David que saltaba y danzaba; y lo menospreció en su corazón.

14. Salmos 149:3  Alaben su nombre con danza; Con pandero y arpa a él canten.

15. Salmos 150:4  Alabadle con pandero y danza; Alabadle con cuerdas y flautas.

16. Isaías 3:16 Asimismo dice Jehová: Por cuanto las hijas de Sion se ensoberbecen, y andan con cuello erguido y con ojos desvergonzados; cuando andan van danzando, y haciendo son con los pies;

17. Jeremías 31:4  Aún te edificaré, y serás edificada, oh virgen de Israel; todavía serás adornada con tus panderos, y saldrás en alegres danzas.

18. Jeremías 31:13  Entonces la virgen se alegrará en la danza, los jóvenes y los viejos juntamente; y cambiaré su lloro en gozo, y los consolaré, y los alegraré de su dolor.

19. Lamentaciones 5:15  Cesó el gozo de nuestro corazón; Nuestra danza se cambió en luto.

20. Mateo 14:6  Pero cuando se celebraba el cumpleaños de Herodes, la hija de Herodías danzó en medio, y agradó a Herodes,

21. Marcos 6:22  entrando la hija de Herodías, danzó, y agradó a Herodes y a los que estaban con él a la mesa; y el rey dijo a la muchacha: Pídeme lo que quieras, y yo te lo daré.

22. Lucas 15:25 Y su hijo mayor estaba en el campo; y cuando vino, y llegó cerca de la casa, oyó la música y las danzas;

Aunque algunas personas hacen una distinción entre danzar y bailar, en esta Biblia los dos términos se usan como sinónimos (por ejemplo se habla de danza para Dios en Ex 15:20 pero también danza para ídolos en Ex 32:19, y de baile para Dios en Jueces 21:21 pero baile por alegría en Sal 30:11).

Hay quienes argumentan que el problema es de traducción y que danzar es bueno o no dependiendo del termino que se emplee en las lenguas originales de  la Biblia, recordemos que el AT fue escrito en hebreo mientras que el NT en griego, que los manuscritos más antiguos del AT que se conocen hasta ahora y desde 1945 son los rollos de Qumram del año 100aC y los más antiguos del nuevo son anteriores al año 100dC.

Revisando en una Biblia con concordancia Strong como <http://blb.org>, vemos que las palabras hebreas que en la Reina Valera se han traducido como bailar o danzar en los 21 pasajes del AT son:

* מחלה mĕchowlah - H4246 en Ex 15:20, 32:19, Jue 11:34, Jue 21:21, 1 Sam 18:6 (1 Sam 18:7), 21:11 y 29:5
* חוּל chuwl - H2342 en Jueces 21:21 y 21:23 (Jue 21:21 emplea esta y la anterior).
* שָׂחַק  sachaq  - H7832  en 2 Sam 6:5, 6:21
* כָּרַר karar - H3769 en 2 Sam 6:14 y 6:16
* מחול machowl - H4234  en Salmo 30:11, 149:3, 150:4, Jer 31:4, 31:13,  Lam 5:15
* רקד raqad - H7540 en Ecl 3:4,  1 Cro 15:29
* טָפַף taphaph - H 2952 en Isa 3:16

Y las palabras griegas empleadas en el nuevo testamento son:

* χορός choros - G5525  en Luc 15:25
* ὀρχέομαι orcheomai - G3738 en Mat 11:17, Luc 7:32, Mat 14:6, Mar 6:22

Examinemos las motivaciones para danzar que se ven en la Biblia son variadas:

* Para alabar a Dios (8).  En el primer versículo donde se habla de danza Ex 15:20 y en 2 Sam 6:5, 6:14, 6:16, 6:21, 1 Cr 15:29, 149:3, 150:4
* Como contraposición a la tristeza (7):  Sal 30:11, Ec 3:4, Mat 11:17, Luc 7:32, Jer 31:4, 31:13,  Lam 5:15
* Para celebrar un acontecimiento feliz (1): Jue 11:34, 1 Sam 18:6, 18:7, 21:11, 29:5, Luc 15:25
* Para celebrarle a una persona y talvez seducir (2): Mat 14:6, Mar 6:22
* Para ufanarse (1): Isa 3:16
* Para adorar ídolos (1): Ex 32:19

Pueden cruzarse términos y motivaciones:

| |Alabar a Dios | Contraposición a tristeza | Celebrar acontecimiento feliz | Celebrar a un a  persona o seducir | Ufanarse | Adorar ídolos |
|-|--------------|---------------------------|-------------------------------|------------------------------------|----------|---------------|
| מחלה mĕchowlah | Ex 15:20,  Jue 21:21| | Jue 11:34, 1 Sam 18:6,(7),  21:11, 29:5 | | | Ex 32:19 |
| מחול machowl | Salmo  149:3, 150:4, | Sal 30:11,  Jer 31:4,13,   Lam 5:15 | | | | |
| שָׂחַק sachaq | 2 Sam 6:5, 6:21 | | | | | | |
| כָּרַר karar| 2 Sam 6:14, 6:16 | | | | | | |
|רקד raqad | Cro 15:29 | Ecl 3:4,  1 | | | | | |
|טָפַף taphaph | | | | |Isa 3:16| |
|חוּל chuwl |Jue 21:21, 23 | | | | | | |
|χορός choros | | | Luc 15:25 | | | | |
|ὀρχέομαι orcheomai | | Mat 11:17, Luc 7:32, | | Mat 14:6, Mar 6:22 | | |

Vemos entonces que no hay una única palabra que pueda emplearse para describir la danza para alabar al Señor, se usan muchas,incluso la más usada (מחלה mĕchowlah) también se uso en el contexto de adorar ídolos.

### 3.2 La intención

Parece que se trata de algo que trasciende los términos, veamos entonces las intencionalidades de la danza:

* Definitivamente no debe danzarse para adorar idolos
* Danzar para ufanarse no sirve, de lo único que uno puede como “ufanarse” según  Jeremias 9:23-24 es de conocer de Dios “Así dijo Jehová: No se alabe el sabio en su sabiduría, ni en su valentía se alabe el valiente, ni el rico se alabe en sus riquezas. Mas alábese en esto el que se hubiere de alabar: en entenderme y conocerme, que yo soy Jehová, que hago misericordia, juicio y justicia en la tierra; porque estas cosas quiero, dice Jehová.”
* Danzar para seducir fuera del matrimonio es errado.   Entre esposos es permitido e importante seducirse mutuamente dentro de los límites generales del matrimonio.
* Danzar para celebrar un acontecimiento feliz puede ser delicado (como le ocurrió a Jefte que tuvo que ser sacrificada por su papá por ser la primera que salió a recibirlo danzando),  pero en Lucas 15:25 en el contexto de la parábola del hijo prodigo, que el padre celebra el regreso de su hijo con una fiesta y danza, y su interpretación como la alegría de Dios cuando un hijo retorna a  Él lo pone a uno a pensar si la fiesta que se hace en el cielo en un caso así incluye danza de los ángeles, o si los ángeles danzan para el Señor ---todos sabremos con certeza cuando allí estemos.
* Danzar para expresar alegría es como típico en los niñitos pequeños, que bueno aprender de ellos y poderlo hacer.
* Finalmente danzar para el Señor es históricamente bueno.

Así que el punto central de la danza para Dios es que yo la haga con intención de alabarlo a Él.

### 3.3. Elementos para tener en cuenta para danzar para Dios

En la danza hay música con todas sus características y además movimientos del cuerpo por lo que todos y cada uno de estos elementos debe tener la intencionalidad de alabar al Señor.   Por ejemplo no podemos danzar para el Señor con una música cuya letra adore ídolos.

Sin embargo tampoco podemos prejuzgar los ritmos por ejemplo declarar que el reaggeton u otro ritmo es para ídolos, examinemos la intención de quien interpreta, y pidamose  en cada caso al Señor que revele.

Con respecto a instrumentos en el salmo 150 encontramos el mandamiento de alabar al Señor con gran variedad de instrumentos, desde los simples (como la pandereta, la bocina, los címbalos) hasta los que reqieren más experticia (flautas, cuerdas, salterio, arpa).    Es decir todos debemos alabar con instrumentos (por lo menos palmas) con intencionalidad de alabar.

Respecto al momento y el sitio para alabarlo con danza, si bien Jueces 21:21 y 21:23 muestra que en unas fiestas anuales para Jehova las doncellas danzaban en el sitio de la fiesta para Él, David danzó mientras transportaba el arca según los versículos de 2 Sam y 1 Cr, y Myriam lo hizo recién el Señor los libró de los egipcios justo al lado del mar Rojo.  Todo tiempo y lugar es bueno para alabar al Señor pero en espíritu y verdad como dice Juan 4:23: “Mas la hora viene, y ahora es, cuando los verdaderos adoradores adorarán al Padre en espíritu y en verdad; porque también el Padre tales adoradores busca que le adoren.”

Respecto a quienes lo deben alabar con danza no hay limitación, en Ex 15:20 danzaron TODAS las mujeres sin discrimación por edad o por experiencia en baile, y los versículos de 2 Sam muestran que David y por tanto los hombres también danzan para el Señor.   El Salmo 148 es un buen resumen de donde y quienes deben alabar al Señor: todos en todas partes, o aún más breve en el final del Salmo 150 “Todo lo que respira alabe a JAH“

## 4. Conclusión

El Señor quiere que en todo tiempo, todo lugar y de todas las formas lo alabemos en Espíritu y en Verdad.

Para esta prédica el Señor me recordó que Él mira el corazón de quien danza, de quien lo alaba. Al Señor no le interesa tanto que uno cante con el tono y tiempo perfecto o que dance para Él con perfecta coordinación o la vestimenta o escenografía, pero que el corazón este lleno de amor hacía Él.

Me lo recordó de varias maneras, una palabra de una persona y esta humilde pero hermosa  canción que me quebrantó increiblemente, la cual los invito a escuchar mientras oramos:
http://www.youtube.com/watch?v=4iPnXm6GtNA&feature=fvst

Mientras lloraba porque el Señor me recordaba como se goza de un corazón humilde a Él, la dancé para el Él a solas y ahora le pregunto a cada uno y cada una, ¿a solas alguna vez ha danzado para Dios?  Si la respuesta es no, lo y la invito a hacerlo pronto, y al Creador le ruego que permita sentir su Santo Espíritu a quienes lo hagan.  No importa si sabe o no sabe, si ha practicado nada, poco o mucho, deje que el Espíritu de Dios lo guie en sus movimientos, Él se enseñará pasos para alabar mejor al Señor.

Al Señor agradezco el ministerio de danza que me permitió iniciar en la Iglesia Menonita de San Nicolás (Soacha) y que me mostró mientras yo estaba en Sierra Leona en peligro mortal, como me permitió ver como se calmaban las personas que me veían adorando al Creador en danza.  Le agradezco poder sembrar aquí esa semilla de adoración a Él y que esté dando fruto y a Él oro para que me permita seguirlo sembrando donde Él disponga y como disponga.

Señor recibe nuestra alabanza, enseñanos cada día como alabarte más y mejor, siempre con amor sincero porque allí donde está nuestro corazón está nuestro tesoro en tí en tu Santo Espíritu.


---

La imagen es del ministerio Danzadores y danzadoras de Jesucristo como era en 2019.