---
layout: post
title: "TejidosBlandosEnFosiles"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Soft tissue and cellular preservation in vertebrate skeletal elements from the Cretaceous to the present
Mary Higby Schweitzer Jennifer L Wittmeyer, and John R Horner

http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=1685849
