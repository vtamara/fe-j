---
layout: post
categories: []
title: Ofrendas de Paz
author: vtamara
image: "/assets/images/9006.png"

---
# 1 Introducción

Como indica \[PYP2011\] “las naciones han puesto su confianza en los ejércitos y las armas… “, pero nosotros confiamos en Dios y su protección y obedecemos a Jesús quien nos ordena “no matar” (Exo 20:13), amar a nuestros enemigos y orar por ellos (Mat 5:44), así mismo nos llama a ser pacificadores (Mat 5:9), que entendemos por ejemplo liderando la reconciliación.

  
Por mandato de la ONU, el 21 de Septiembre se celebra el día internacional de la paz, la no-violencia y el cese al fuego. Los menonitas en Colombia hemos aprovechado ese día para celebrar “Pan y Paz” con diversos eventos que buscan fomentar una cultura de paz como la entendemos de Jesús y que se ha resumido en 7 puntos de una declaración (ver \[PYP2011\]), (he actualizado punto 4 y minimamente el 5):

1. En seguimiento a Jesús, Hijo de Dios, a quien reconocemos como Dios soberano, afirmamos nuestra convicción bíblica e histórica de que el camino a la Paz es la no-violencia activa y el amor a los prójimos, en especial a los débiles, marginados, pobres y a los enemigos.
2. No tomaremos partido por propuestas armadas en la búsqueda de la paz. Consideramos que el esfuerzo de obligar a toda la población a tomar partido en el conflicto armado tanto por parte del Estado como por parte de los grupos armados ilegales es una alternativa falsa porque ambos utilizan la misma lógica de las armas y la violencia.
3. Nos oponemos a participar en grupos armados y a pagar para que otros lo hagan. Nos oponemos a la militarización de la economía y abogamos para todos el derecho de objeción de conciencia a todo servicio armado.
4. Celebramos la propuesta de Paz Total del actual gobierno que prioriza caminos de diálogo con todos y cada uno de los actores armados.
5. Animamos a los funcionarios estatales, a los grupos armados y a los medios de comunicación a deponer las actitudes de guerra y embarcarse en conversaciones y acciones de paz donde se hagan concesiones sustanciales y fundamentales en la construcción de un nuevo país, con plenas garantías para la dignidad humana dentro de la justicia social y legal que incluya vivienda, empleo, tierra, seguridad, educación, salud y libertades democráticas.
6. Hacemos un llamado a la comunidad internacional, y en especial a los ciudadanos de Estados Unidos a frenar sus contribuciones en finanzas, armas y asesoría para la guerra en Colombia, como también a contribuir al bienestar del mundo apoyando y aprobando la participación en la Corte Penal Internacional.
7. Invitamos a los movimientos de paz y no-violencia en Colombia a deponer sus divisiones y asumir un papel más activo por la paz en nuestro país.

También celebro de la actual propuesta de presupuesto del gobierno (\[PRES23\]) el incremento en rubros para vivienda de interés social, agricultura, educación y salud. Aún cuando anhelamos una disminución del presupuesto para la guerra.

Por experiencia personal, fallando especialmente a nivel familiar y especialmente en mi primer matrimonio, entiendo que es necesario transmitir este mensaje de manera consistente con paz en nuestra relación con Dios, en las relaciones familiares, en relaciones con personas de la iglesia y en relaciones con personas fuera de la iglesia.

Respecto a relaciones familiares, comienzan con la relación de pareja, \[LAR2004\] indica que de manera particular el esposo está llamado a ser “Constructor de Paz” en la relación con la esposa: “De alguna manera puede ser lo más importante. Si hay un disgusto, un conflicto, incluso una sensación de tensión, usted y su esposa no estarán plenamente en paz, y, por tanto, no se podrán sentir realmente conectados. Sin paz en su relación, ella no se sentirá cercana, ella no sentirá apertura de su parte, y ciertamente ella pensará que usted no la entiende.”

Pero el centro de la prédica hoy es paz en nuestra relación con Dios.

# 2. Texto: Éxodo 20:22-26

22 Y Jehová dijo a Moisés: Así dirás a los hijos de Israel: Vosotros habéis visto que he hablado desde el cielo con vosotros. 23 No hagáis conmigo dioses de plata, ni dioses de oro os haréis. 24 Altar de tierra harás para mí, y sacrificarás sobre él tus holocaustos y tus ofrendas de paz, tus ovejas y tus vacas; en todo lugar donde yo hiciere que esté la memoria de mi nombre, vendré a ti y te bendeciré. 25 Y si me hicieres altar de piedras, no las labres de cantería; porque si alzares herramienta sobre él, lo profanarás. 26 No subirás por gradas a mi altar, para que tu desnudez no se descubra junto a él.

# 3. Contexto

El contexto inmediato de estos versículos fue Dios hablándole desde una montaña directamente al pueblo de Israel que estaba en el desierto, los 10 mandamientos en medio de estruendo, relampagos y con la montaña humeando. El pueblo aterrorizado prefirió pedirle a Moisés que él hablara con Dios y les transmitiera lo que Dios decía, y así lo hizo y estos versículos muestran lo que Dios entonces le dijo a Moisés.

En el Antiguo Testamento la palabra paz aparece especialmente en relación con las ofrendas de paz, citadas por primera vez en el pasaje de hoy.

Los capítulos 20 a 23 del libro de Éxodo escrito por Moisés, son una serie de leyes que comienzan con los 10 mandamientos. Es interesante que la primera referencia a ofrendas de paz es en este contexto justo después de los 10 mandamientos, pues resalta la importancia de este tipo de ofrendas.

Según \[5OFR\] los 5 principales tipos de ofrendas se describen en los 5 primeros libros de Levítico. Aunque hay otros tipos de ofrendas pueden considerarse como subtipos de estas. Algunas características que notamos de estas son:

| --- | --- | --- | --- | --- | --- | --- |
| Libro | Tipo | Frec | Volunt | Qué | Quien comía | Cómo |
| Lev 1 | Holocausto | 197 | Si | Becerro, oveja, cabra, tórtola o palomino | Nadie | Degollados, dividios y quemados |
| Lev 2 | Oblación (ofrenda sin carne) | 123 | Si | Flor de harina, Tortas cocidas en horno o en cazueala, | Una parte se quemaba otra para el sacerdote y su familia. | Si hace falta al fuego con aceite y sal. Pero sin levadura. |
| Lev 3 + Lev 7:11-21 | Paz | 119 | Si | Vaca o toro sin defecto, Oveja sin defecto, Cabra sin defecto. Tortas sin levadura. | Una parte se quemaba, otra para sacerdote, otra para persona que la llevaba y otra para el pueblo. | Degollados, divididos, grosura y sangre se quemaban. |
| Lev 4 | Pecado | 43 | No | Becerro sin defecto, si pecó sacerdote o el pueblo. Macho cabrío si pecó jefe. Cabra o cordero hembra si pecó otro por yerro (otro denuncia que pecó). | Nadie(?) | Degollados, divididos, una parte quemada en altar y otra fuera del campamento. La persona o pueblo eran perdonados por Dios. |
| Lev 5 | Expiación | 36 | No | Para algunas faltas confiesa y puede llevar cabra o cordero hembra, Si no tiene 2 tórtolas o 2 palominos, si no alcanza harina. Otras faltas contra cosas santas un carnero y reposición monetaria. Otros pecados un carnero. | Al menos en caso de harina, una parte para sacerdote. | Degollado, dividido, quemado. La persona era perdonada por Dios. |

Como hace notar \[GOTQ2022\] hay una explicación más completa de como son las ofrendas de paz en Levitico 7:11-21:

Y esta es la ley del sacrificio de paz que se ofrecerá a Jehová: 12 Si se ofreciere en acción de gracias, ofrecerá por sacrificio de acción de gracias tortas sin levadura amasadas con aceite, y hojaldres sin levadura untadas con aceite, y flor de harina frita en tortas amasadas con aceite. 13 Con tortas de pan leudo presentará su ofrenda en el sacrificio de acciones de gracias de paz. 14 Y de toda la ofrenda presentará una parte por ofrenda elevada a Jehová, y será del sacerdote que rociare la sangre de los sacrificios de paz. 15 Y la carne del sacrificio de paz en acción de gracias se comerá en el día que fuere ofrecida; no dejarán de ella nada para otro día. 16 Mas si el sacrificio de su ofrenda fuere voto, o voluntario, será comido en el día que ofreciere su sacrificio, y lo que de él quedare, lo comerán al día siguiente; 17 y lo que quedare de la carne del sacrificio hasta el tercer día, será quemado en el fuego. 18 Si se comiere de la carne del sacrificio de paz al tercer día, el que lo ofreciere no será acepto, ni le será contado; abominación será, y la persona que de él comiere llevará su pecado. 19 Y la carne que tocare alguna cosa inmunda, no se comerá; al fuego será quemada. Toda persona limpia podrá comer la carne; 20 pero la persona que comiere la carne del sacrificio de paz, el cual es de Jehová, estando inmunda, aquella persona será cortada de entre su pueblo. 21 Además, la persona que tocare alguna cosa inmunda, inmundicia de hombre, o animal inmundo, o cualquier abominación inmunda, y comiere la carne del sacrificio de paz, el cual es de Jehová, aquella persona será cortada de entre su pueblo.

Estas ofrendas de paz --a diferencia de las demás-- eran comidas en parte por quien las ofrendaba, otra parte por los sacerdotes y sólo la grasa se quemaba como sacrificio a Jehova, incluso según \[ENCJ\] de esas ofrendas también se repartía para otras personas ritualmente limpias, incluyendo pobres, esclavos, extranjeros en un ambiente de fiesta. Había prácticamente dos razones para hacer una ofrenda de paz: por agradecimiento y por voto.

1. Las ofrendas de paz por agradecimiento a Dios eran una forma de agradecerle y alabar a Dios por Su bondad (tortas sin levadura y un animal que debía ser sacrificado por el sacerdote, su sangre esparcida como era costumbre y debía comerse el mismo día)
2. Voto voluntario que según \[GOTQ2022\] era para expresar paz hacía Dios después de haber cumplido un voto, dice “Un buen ejemplo de este fue cuando Ana cumplió su voto ante Dios al llevar a Samuel al templo \[bien niño\]; en esa ocasión también llevó una ofrenda de paz para expresar paz en su corazón hacía Dios con respecto al sacrificio --era como decir “No tengo resentimiento; no tenga nada en contra al pagar mi voto” (podría comerse en 2 días, lo que quedara para el tercer día debía ser quemado --seguramente ya no sería apto para consumo y por eso se declaraba inmundo).

Noto que el objetivo era agradecer y estar en paz con Dios y que requería un sacrificio y esfuerzo real de parte de quien la ofrecía.

Dios no necesita ser aplacado o algo así, pues para perdonarnos basta que vayamos a Él en oración arrepentidos y que con un cristiano(a) maduro que confesemos. La ofrenda de paz es más para expresar que no tenemos rencor con Dios.

# 4. Análisis

| --- | --- |
| Versículos | Observación |
| 22 | El pueblo se había alejado aterrorizado del monte desde el cual Dios les había hablado, pero Dios sigue hablándole a Moisés que quizo entrar en su presencia. Hoy sigue escuchando y hablándole al que lo busca. |
| 23 | Resalta repitiendo el primer mandamiento Exodo 20:3-6: “3 No tendrás dioses ajenos delante de mí. 4 No te harás imagen, ni ninguna semejanza de lo que esté arriba en el cielo, ni abajo en la tierra, ni en las aguas debajo de la tierra. 5 No te inclinarás a ellas, ni las honrarás; porque yo soy Jehová tu Dios, fuerte, celoso, que visito la maldad de los padres sobre los hijos hasta la tercera y cuarta generación de los que me aborrecen, 6 y hago misericordia a millares, a los que me aman y guardan mis mandamientos”. ¿Hoy estoy cumpliendolo? ¿Qué es lo que estoy honrando? ¿Ante que me inclino? La ciencia, el dinero, los famosos. ¿Donde estoy poniendo mi confianza? |
| 24 | Para hacer ofrenda a Dios, él no requirió suntuosidad o gastos en el altar, bastaba uno de tierra o piedra pura. Hoy no necesitamos lujosos templos, ni ceremonias especiales para ofrendar de lo que tenemos a Dios, sólo un corazón agradecido. |
| 25 | En caso de que fuese un altar más elaborado, de piedra, era en todo caso sencillo y Él dio las instrucciones precisas de como debía ser. ¿Estoy permitiendo que Dios me guíe respecto a mis ofrendas, el lugar y la forma? incluso ¿donde vivo y como vivo? |
| 26 | Este versículo me muestra el carácter público de las ofrendas en el tiempo de Moises. Varias personas estarían cerca al altar, y su concentración debía ser en arrepentirse de los pecados en los que estuvieran y agradeciendo por la bondad de nuestro Señor. Dios diseña correctamente, evitando distracciones innecesarias. ¿Estoy analizando lo que hago y confesando pecados? ¿Cuando veo el pecado de otro estoy actuando acorde a Mateo 18:15-22? ¿Estoy organizando mi tiempo y recursos para no distraerme y desenfocarme de Dios y para evitarme pecar? (personalmente debo diseñar mejor mi horario para no desenfocarme en Internet) |

# 5. Conclusión y oración

Señor, por favor ayúdanos a organizar nuestro tiempo y recursos para vivir de una manera que te de gloria, que nos permita enfocarnos en Tí, que nos haga difícil desobedecerte y nos facilite obedecerte y cuando pequemos buscarte, confesar pecados y reparar.

Gracias por haberte dado completo como sacrificio último por nuestros pecados, por haber dado tu sangre que hace innecesarios el derramamiento de más sangre en sacrificio y por sellar nuestra relación con Tu Santo Espíritu. Señor tu perdón y salvación son el regalo más precioso, que lo reconozcamos, agradezcamos y compartamos.

Señor reconozco que eres el único Dios Todopoderoso, Justo, Creador y que me amas como nadie. Gracias por cada bendición, eres la fuente de lo bueno en mi vida y en la de todos. Perdoname cuando te he desobedecido, muestrame para arrepentirme y tener paz contigo.

Ayudame a organizarme y a esforzarme para darte ofrendas, son voluntarias porque tu ya pagaste por las obligatorias (aunque cuando peco debo ir a ti arrepentido y confesar). Que dando ayuda a quienes necesitan tengamos tesoro en el cielo como has prometido, que ayudando con ofrendas en la iglesia ayudemos al sostenimiento del pastor y su familia y la expansión de Tu Reino.

Señor, aún cuando temamos tu voz, por favor háblanos directamente, para entender que una buena relación contigo es más importante y es la base para la paz personal, familiar, en la iglesia y nuestra sociedad.

Gracias por el deseo del actual gobierno de una paz total, por el deseo de que haya pan y trabajo para todos, por favor guía a nuestros gobernantes hacía esto, hablales directamente y a través nuestro como iglesia de paz pues sin Ti no hay paz porque eres el Príncipe de la Paz.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* \[GOTQ2022\] [https://www.gotquestions.org/peace-offering.html](https://www.gotquestions.org/peace-offering.html "https://www.gotquestions.org/peace-offering.html"). Consultado Sep.2022
* \[JENC2022\] [https://www.jewishencyclopedia.com/articles/11966-peace-offering](https://www.jewishencyclopedia.com/articles/11966-peace-offering "https://www.jewishencyclopedia.com/articles/11966-peace-offering") Consultado Sep.2022
* \[PYP2011\] 2011 [https://fe.pasosdejesus.org/assets/images/cartillapanypaz2011.pdf](https://fe.pasosdejesus.org/assets/images/cartillapanypaz2011.pdf "https://fe.pasosdejesus.org/assets/images/cartillapanypaz2011.pdf") Consutado Sep.2022
* \[PRES23\] [https://www.elpais.com.co/economia/conozca-en-detalle-los-cambios-que-tendra-el-presupuesto-general-de-la-nacion-en-2023.html](https://www.elpais.com.co/economia/conozca-en-detalle-los-cambios-que-tendra-el-presupuesto-general-de-la-nacion-en-2023.html "https://www.elpais.com.co/economia/conozca-en-detalle-los-cambios-que-tendra-el-presupuesto-general-de-la-nacion-en-2023.html")
* \[5OFR\] \][https://www.learnthebible.org/sermon-outlines/the-five-major-offerings.html](https://www.learnthebible.org/sermon-outlines/the-five-major-offerings.html "https://www.learnthebible.org/sermon-outlines/the-five-major-offerings.html")

***

Esta prédica se compartió con la Comunidad Menonita de Suba el  25.Sep.2022.

La imagen es de dominio público tomada de [https://openclipart.org/detail/9006/sheep](https://openclipart.org/detail/9006/sheep "https://openclipart.org/detail/9006/sheep")