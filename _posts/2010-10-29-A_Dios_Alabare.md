---
layout: post
title: "A_Dios_Alabare"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
1

A Dios alabaré, de todo corazón. %%%
Y proclamaré sus maravillas %%%
Su nombre cantaré. %%%

Señor te alabaré, de todo corazón.%%%
Tu eres el Señor, el gozo para mí, %%%
Aleluya. %%%

2

Dios, el Señor es Rey, El permanecerá %%%
Y juzgará al mundo con verdad,%%%
justicia sentará %%%

Dios el Señor es Rey, su reino es eternal %%%
Los pueblos verán justicia y rectitud, %%%
Aleluya. %%%

3

Al pobre cuidará, y le protegerá %%%
Refugio es Él en tiempo de aflicción %%%
Su nombre es Salvador %%%

Al oprimido ve, y le defenderá %%%
El salvará, el santo Dios es El, %%%
Aleluya. %%%



4

Gloria al Padre Dios, Loor a Cristo el Rey, %%%
Al Santo Espíritu la bendición %%%
Su nombre alabaré; %%%

Gloria al Creador, Loor al Salvador, %%%
Al Santo Espíritu Consolador %%%
A Dios alabo hoy. %%%

----

Coro de una melodia francesa con letra por Pedro Stucky (pastor de la iglesia Menonita de Teusaquillo) con base en Sal. 9-1,7-9, 

Pista de dominio público interpretada y editada por Omar Baracaldo
* [Formato ogg ](http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/A_Dios_Alabare.ogg), [Formato mp3 ](http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/A_Dios_Alabare.mp3)
* [Fuente comprimida para Garage Band ](http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/A_Dios_Alabare.band.tar.gz )
