---
layout: post
title: "Fortaleza_en_Ashdod_Era_Hierro"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
[http://lh5.ggpht.com/-nujjcg23GHg/TzPp0p57u5I/AAAAAAAACoE/nzcGiLcH4Ls/s1600-h/DSCN4502%25255B4%25255D.jpg]
(imagen de http://blog.bibleplaces.com/2012/02/iron-age-fortress-excavated-in-ashdod.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed:+BiblePlacesBlog+(BiblePlaces+Blog) )

Se trata de una fortaleza con paredes de hasta 1m de ancho que se ha datado entre finales del siglo 8ac y comienzos del 6aC.   Se presume que era bien Asiria o bien del rey judio Josias (640-609aC).  La excavación fue dirigida por Dmitri Egorov.    
