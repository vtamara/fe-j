---
layout: post
title: Promesas de Dios por diezmos y ofrendas
author: vtamara
categories:
- Prédica
image: "/assets/images/hungarian-fruit-and-vegetables.jpg"
tags: Prédica

---

# Promesas de Dios por diezmos y ofrendas

## 1. Introducción

Le he estado orando a Dios por esta prédica, en dos amaneceres me recordó una canción que más adelante pondré y cuando la escuché, entendí que el tema central es la promesa que acompaña al diezmo y las ofrendas. En predicas anteriores hemos aclarado que el diezmo no es obligatorio en la Iglesia Menonita, pero la medida de la decima parte de mis ingresos para ofrendar en la iglesia es un diseño de Dios desde el antiguo testamento.

## 2. Texto

Malaquias 3:6-12

    6 Porque yo Jehová no cambio; por esto, hijos de Jacob, no habéis sido 
    consumidos.
    7 Desde los días de vuestros padres os habéis apartado de mis leyes, y no las 
    guardasteis. Volveos a mí, y yo me volveré a vosotros, ha dicho Jehová de los 
    ejércitos. Mas dijisteis: ¿En qué hemos de volvernos?
    8 ¿Robará el hombre a Dios? Pues vosotros me habéis robado. Y dijisteis: ¿En 
    qué te hemos robado? En vuestros diezmos y ofrendas.
    9 Malditos sois con maldición, porque vosotros, la nación toda, me habéis 
    robado.
    10 Traed todos los diezmos al alfolí y haya alimento en mi casa; y probadme 
    ahora en esto, dice Jehová de los ejércitos, si no os abriré las ventanas de 
    los cielos, y derramaré sobre vosotros bendición hasta que sobreabunde.
    11 Reprenderé también por vosotros al devorador, y no os destruirá el fruto 
    de la tierra, ni vuestra vid en el campo será estéril, dice Jehová de los 
    ejércitos.
    12 Y todas las naciones os dirán bienaventurados; porque seréis tierra 
    deseable, dice qqqJehová de los ejércitos.
    

## 3. Contexto

En el pasaje anterior Malaquias profetizaba sobre el regreso del Señor Jesús, en los versículos siguientes sobre la diferencia entre el que sirve a Jehová y el que no, el primer justo y el segundo no.

Elegí este pasaje porque en los versículos 10 y 11 tiene las promesas conectadas con el diezmo. Al que diezma Dios le abrirá las ventanas de los cielos y derramará bendición hasta que sobreabunde. Y reprenderá por esa persona al devorador, de forma que no destruirá el fruto de su tierra ni será esteril su vid.

La primera mención de ofrenda en la Biblia, es la que hicieron Caín y Abel en Gen 4:3-4, y en una predica anterior con el pastor Jaime estudiamos la primera mención de diezmo entre Abraham y Melquisedec en Gen 14:18-20, Jaime explicaba que Abraham diezmo de puro agradecimiento no por obligación de una ley (pues la ley vino después con Moises).

Respecto a la primera ofrenda, como explica {3}, la de Abel fue agradable a Dios porque se basó en el primer sacrificio que Dios mismo hizo. Después de la desobediencia de Adán y Eva, Genesis 3:21 dice “Y Jehová Dios hizo al hombre y a su mujer túnicas de pieles, y los vistió” que implicaría que Dios sacrificó animales y les quito las pieles para ponérselas como nuevo vestido a Adán y Eva y hacerlos presentables. Símbolo del posterior sacrificio del Señor Jesús y de como nos cubre con su sangre a quienes lo aceptamos para ser presentables ante Dios.

Hay por lo menos dos palabras en hebreo que se traducen como diezmar, ambas están presentes en Deut 26:12

      12 Cuando acabes de diezmar (6237) todo el diezmo (4634) de tus frutos en 
    el año tercero, el año del diezmo, darás también al levita, al extranjero, al 
    huérfano y a la viuda; y comerán en tus aldeas, y se saciarán.
    

La primera que es el verbo diezmar con número Strong 6237 en hebreo es עָשַׂר y se lee como aser.

La segunda que es el sustantivo diezmo con número Strong 4643 en hebreo es מַעֲשֵׂר que se lee como majaser.

* aser: diezmar, dar o recibir un decimo. Dar o recibir diezmo.
* majaser: un decimo, especialmente un diezmo.

La primera es una raíz y al segunda una forma derivada. La primera es casi identica a otra palabra hebrea (en la escritura hebrea sólo se diferencian por la ubicación del acento en la letra del medio):

| --- | --- |
| aser | asher |
| עָשַׂר | עָשַׁר |

Esta última es ganar riquezas. Una interesante coincidencia pero que coincide con la promesa de Miqueas 3:10, “bendición hasta que sobreabunde”. Pero OJO, Dios lo hace, lo cumple y bendice hasta que sobreabunde, pero no olvidemos que el Señor Jesús dijó que entraría más fácil un camello por el ojo de una aguja que un rico en el Reino de los Cielos.

## 4. Análisis

Como conté en una predica anterior, en mi caso le creí a Dios su promesa de que nada me faltaría en una lectura comunitaria, repasemos el salmo 23 y creamos las promesas allí escritas:

Salmo 23

    1 Jehová es mi pastor; nada me faltará.
    2 En lugares de delicados me hará descansar.  Junto a aguas de reposos me 
      pastoreará
    3 Confortará mi alma, me guiará por sendas de justicia por amor a Su nombre. 
    4 Aunque ande en valles de sombra de muerte no temeré mal alguno, porque Tu 
      irás conmigo. Tu vara y tu callado me infundirán aliento
    5 Aderezas mesa delante de mi, en presencia de mis angustiadores. Unges mi 
      cabeza con aceite, mi copa está rebozante
    6 Ciertamente el bien y la misericordia me seguirá todos los días de mi 
      vida. Y en la casa de Jehová moraré por largos días.
    

Pero vuelvo a recordarme y recordarnos que la bendición que Dios da, no puede ser para alimentar el egoismo, debe ser para compartir, para dar, no para olvidar el primer gran mandamiento de Amar a Dios sobre todas las cosas, con toda la mente, todo el corazón y todas las fuerzas y el segundo igual de importante de AMAR AL PROJIMO COMO A MI MISMO. El mismo pasaje que citamos Deut 6:12 me lo recuerda, pues además de dar el diezmo en el templo es necesario dar "al extranjero, al huérfano y a la viuda; y comerán en tus aldeas, y se saciarán"

En la prédica pasada Jaime recordó que mi projimo es toda persona sobre esta tierra, si limito a mi familia, a mi iglesia o incluso a mi país, creo que no se está cumpliendo este mandamiento.

Invito cada quien a preguntarse ¿Estoy Listo para el “asher” que viene con el “aser”? ¿Estoy listo para compartir la sobreabundacia una vez aprenda a diezmar? ¿Quien está listo(a) para que la bendición lo(a) persiga?

Escuchemos una canción al respecto: [https://www.youtube.com/watch?v=VBPNn9gdNmg](https://www.youtube.com/watch?v=VBPNn9gdNmg "https://www.youtube.com/watch?v=VBPNn9gdNmg")

¿Le cree usted esa promesa a Dios? ¿Cree que nada le faltará, y que Dios lo sobreabundará de tal manera que la bendición lo perseguirá?

Lo invito a hacer la prueba, cuando reciba un salario sabiendo que hay tantas obligaciones que no alcanza (como es usual), aparte la décima parte y confíe en que Dios le proveerá, delo bien en la iglesia o bien en ofrendas para necesitados (fuera de su círculo de responsabilidad) o parte y parte. Vea por si mismo si Dios le responde como dice Malaquias 3:10-11.

## 5. Conclusión y oración

Es buen momento para reiterar que las finanzas de la iglesia requieren el compromiso de todos con sus diezmos y/o ofrendas.

El Señor sobreabundará, a quienes ofrenden y diezmen, como si la bendición persiguiera.

Señor gracias por tus promesas, por favor aumentanos la fe y el amor a ti y por los demás para poder obedecerte por amor y confianza y no por temor.

Señor Jesús gracias por ofrendarte en sacrificio para que quienes creemos pudiésemos ser reconciliados con Dios y presentarnos recubiertos por Tu sangre preciosa.

Si alguién aún no ha aceptado a Jesús como Señor y Salvador lo invitamos a hacer una oración de fe.

## 6. Referencias:

* {1} Biblia. Reina Valera 1960.
* {2} [https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=H4643&t=KJV](https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=H4643&t=KJV "https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=H4643&t=KJV")
* {3} [http://www.abideinchrist.org/es/gen3v21es.html](http://www.abideinchrist.org/es/gen3v21es.html "http://www.abideinchrist.org/es/gen3v21es.html")

***

Basado en predica del 7.Oct.2018. 

Imagen de dominio público de [https://www.publicdomainpictures.net/en/view-image.php?image=237606&picture=hungarian-fruit-and-vegetables](https://www.publicdomainpictures.net/en/view-image.php?image=237606&picture=hungarian-fruit-and-vegetables "https://www.publicdomainpictures.net/en/view-image.php?image=237606&picture=hungarian-fruit-and-vegetables")