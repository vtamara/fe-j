---
layout: post
title: "Parejas_que_hacen_perder"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Hay bastantes ejemplos de matrimonios entre el pueblo de Dios y personas de fuera del pueblo que fueron perdición para el los del pueblo de Dios:

* Salomon

1 Reyes 11:1-6

<pre>
1 Pero el rey Salomón amó, además de la hija de Faraón, a muchas mujeres 
extranjeras; a las de Moab, a las de Amón, a las de Edom, a las de Sidón, y
a las heteas;
2 gentes de las cuales Jehová había dicho a los hijos de Israel: No os 
llegaréis a ellas, ni ellas se llegarán a vosotros; porque ciertamente harán 
inclinar vuestros corazones tras sus dioses. A éstas, pues, se juntó Salomón 
con amor.
3 Y tuvo setecientas mujeres reinas y trescientas concubinas; y sus mujeres 
desviaron su corazón.
4 Y cuando Salomón era ya viejo, sus mujeres inclinaron su corazón tras 
dioses ajenos, y su corazón no era perfecto con Jehová su Dios, como el 
corazón de su padre David.
5 Porque Salomón siguió a Astoret, diosa de los sidonios, y a Milcom, ídolo 
abominable de los amonitas.
6 E hizo Salomón lo malo ante los ojos de Jehová, y no siguió cumplidamente a 
Jehová como David su padre.
</pre>


Hay un ejemplo de un matrimonio entre una persona del pueblo de Dios con una extranjera que terminó siendo de bendición por cuanto la mujer extranjera 
aceptó a Dios como Dios:

Rut 1:1-8, 15-16 
<pre>
Aconteció en los días que gobernaban los jueces, que hubo hambre en la tierra. Y un varón de Belén de Judá fue a morar en los campos de Moab, él y su mujer, y dos hijos suyos.

2 El nombre de aquel varón era Elimelec, y el de su mujer, Noemí; y los nombres de sus hijos eran Mahlón y Quelión, efrateos de Belén de Judá. Llegaron, pues, a los campos de Moab, y se quedaron allí.

3 Y murió Elimelec, marido de Noemí, y quedó ella con sus dos hijos,

4 los cuales tomaron para sí mujeres moabitas; el nombre de una era Orfa, y el nombre de la otra, Rut; y habitaron allí unos diez años.

5 Y murieron también los dos, Mahlón y Quelión, quedando así la mujer desamparada de sus dos hijos y de su marido.

6 Entonces se levantó con sus nueras, y regresó de los campos de Moab; porque oyó en el campo de Moab que Jehová había visitado a su pueblo para darles pan.

7 Salió, pues, del lugar donde había estado, y con ella sus dos nueras, y comenzaron a caminar para volverse a la tierra de Judá.

8 Y Noemí dijo a sus dos nueras: Andad, volveos cada una a la casa de su madre; Jehová haga con vosotras misericordia, como la habéis hecho con los muertos y conmigo.

9 Os conceda Jehová que halléis descanso, cada una en casa de su marido. Luego las besó, y ellas alzaron su voz y lloraron,

10 y le dijeron: Ciertamente nosotras iremos contigo a tu pueblo.

11 Y Noemí respondió: Volveos, hijas mías; ¿para qué habéis de ir conmigo? ¿Tengo yo más hijos en el vientre, que puedan ser vuestros maridos?

12 Volveos, hijas mías, e idos; porque yo ya soy vieja para tener marido. Y aunque dijese: Esperanza tengo, y esta noche estuviese con marido, y aun diese a luz hijos,

13 ¿habíais vosotras de esperarlos hasta que fuesen grandes? ¿Habíais de quedaros sin casar por amor a ellos? No, hijas mías; que mayor amargura tengo yo que vosotras, pues la mano de Jehová ha salido contra mí.

14 Y ellas alzaron otra vez su voz y lloraron; y Orfa besó a su suegra, mas Rut se quedó con ella.

15 Y Noemí dijo: He aquí tu cuñada se ha vuelto a su pueblo y a sus dioses; vuélvete tú tras ella.

16 Respondió Rut: No me ruegues que te deje, y me aparte de ti; porque a dondequiera que tú fueres, iré yo, y dondequiera que vivieres, viviré. Tu pueblo será mi pueblo, y tu Dios mi Dios.
</pre>
