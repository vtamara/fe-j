---
layout: post
title: "Poema_De_Salvacion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Por fin se estrenó en Colombia esta película sobre la vida de Pablo Olivares, se está presentando desde el 25 de Febrero de 2010 en Bogotá en el Cinemark de Cafam Floresta (al parecer también se presentó en Medellín {2}).   Para el 11 de Abril se presenta en 2 funciones en la noche. Dios quiera que llegue a muchos otros teatros y funciones.


!Resumen 
(Tomado de {1})

Poema de salvación

Drama - Argentina - 80 minutos - 15 años Texto

Pablo Olivares es un niño talentoso e inquieto, nacido en una familia cristiana. Carmen, su madre, dedica su tiempo a educarlo conforme a los principios bíblicos y cultiva en él desde pequeño el amor por la música. Roberto, su padre, se concentra principalmente en sus negocios, lo que lo mantiene distante de la vida de Pablo.

Ante la ausencia emocional de su padre, Pablo se junta con amigos que lo introducen al mundo del rock'n'roll y comienza a sentirse atraído hacia el ocultismo. Su habilidad para la música se hace evidente y aunque Carmen cree en su talento, el rock es un asunto que ella no apoyará.

El dolor de la ausencia paterna y el rechazo de su madre ante sus sueños, fermentan en el joven un odio que se torna lentamente hacia Carmen y la religión que ella profesa. Llevado por su ambición de triunfar en la música, Pablo decide pactar con el diablo.

Carmen intenta todo para reestablecer la relación con su hijo, y fiel a sus principios, ora incesantemente por él durante catorce años. La confrontación constante entre Pablo y su madre pronto deja de ser un asunto meramente familiar y se convierte en un campo de batalla espiritual por el alma del joven. Este drama está inspirado en una historia real y filmado en Buenos Aires, Argentina.

!Para ver

* Canción Yo quiero amarte más de Pablo Olivares http://www.youtube.com/watch?v=jUaHfiyxo3o&feature=related
* Corto promocional de la película http://www.youtube.com/watch?v=YLvf6EJ3OY8
* Sitio de la película. http://www.poemadesalvacion.com.ar/
 
!REFERENCIAS

* {1} http://bogota.vive.in/cine/bogota/articulos_cine/febrero2010/ARTICULO-WEB-NOTA_INTERIOR_VIVEIN-7308167.html
* {2} http://feaktivatv.blogspot.com/2010/01/poema-de-salvacion-se-estrena-en.html
