---
layout: post
title: Sembrar paz sin envidia ni egoísmo
author: vtamara
categories:
- Prédicas
image: assets/images/320578-openclipart-x200.png
tags: 

---
# 1. INTRODUCCIÓN

En una preparación para un ayuno, el Señor le regaló a mi esposa un sueño en el que nos recuerda que la salvación requiere que nos mantengamos vigilantes y despiertos en obediencia al Señor, en el mismo sueño le dió los versículos Santiago 1:16 y 1 Corintios 14:33.   El primero dice:
<pre>
Porque donde hay celos y contención, allí hay perturbación y toda obra   perversa.
</pre>
y el segundo
<pre>
pues Dios no es Dios de confusión, sino de paz.  Como en todas las iglesias de los santos,
</pre>

Deseo entonces profundizar en un pasaje que incluye el primero (versículos 13 a 18 de la carta de Santiago), y oro para que el Señor guie y revele un mensaje de edificación para la iglesia.


# 2. TEXTO

En la traducción {1}:
<pre>
13 ¿Quién es sabio y entendido entre vosotros? Muestre por la buena conducta
   sus obras en sabia mansedumbre.
14 Pero si tenéis celos amargos y contención en vuestro corazón, no os 
   jactéis, ni mintáis contra la verdad;
15 porque esta sabiduría no es la que desciende de lo alto, sino terrenal, 
   animal, diabólica.
16 Porque donde hay celos y contención, allí hay perturbación y toda obra 
   perversa.
17 Pero la sabiduría que es de lo alto es primeramente pura, después 
   pacífica, amable, benigna, llena de misericordia y de buenos frutos, 
   sin incertidumbre ni hipocresía.
18 Y el fruto de justicia se siembra en paz para aquellos que hacen la paz.
</pre>



# 3. CONTEXTO

## 3.1 ¿Qué dicen los pasajes que preceden y que siguen a este texto?

División de carta de Santiago de acuerdo a RV1960:

| Versículos | Título en {1} |
| 1:1 |  Salutación |
| 1:2-11 | La sabiduria que viene de Dios |
| 1:12-8 | Soportando las pruebas |
| 1:19-27 | Hacedores de la palabra |
| 2:1-13 | Amonestación contra la parcialidad |
| 2:14-26 | La fe sin obras es muerta |
| 3:1-12 | La lengua |
| 3:13-18 | La sabiduría de lo alto |
| 4:1-10 | La amistad con el mundo |
| 4:11-12 | Juzgando al hermano |
| 4:13-17 | No os gloriéis del dia de mañana |
| 5:1-6 | Contra los ricos opresores |
| 5:7-20 | Sed pacientes y orad |


## 3.2 ¿Quien escribió este pasaje?

En el nuevo testamento se habla de 3 Santiagos (Jacobo en algunas traducciones, &#7992;&#940;&#954;&#969;&#946;&#959;&#962; en griego y &#1497;&#1506;&#1511;&#1489;, en arameo). Dos fueron discípulos de Jesús como lo confirman Mateo, Marcos y Lucas, por ejemplo Mateo 10:2-4
<pre>
2 Los nombres de los doce apóstoles son estos: primero Simón, llamado Pedro, 
  y Andrés su hermano; Jacobo hijo de Zebedeo, y Juan su hermano;
3 Felipe, Bartolomé, Tomás, Mateo el publicano, Jacobo hijo de Alfeo, Lebeo,
  por sobrenombre Tadeo,
4 Simón el cananista, y Judas Iscariote, el que también le entregó.
</pre>
El tercero es un hermano de Jesús como dicen Marcos 6:3
<pre>
¿No es éste el carpintero, hijo de María, hermano de Jacobo, de José, de
Judas y de Simón? ¿No están también aquí con nosotros sus hermanas? Y se escandalizaban de él.
</pre>
Gálatas 1:18-19
<pre>
18 Después, pasados tres años, subí a Jerusalén para ver a Pedro, y 
   permanecí con él quince días;
19 pero no vi a ningún otro de los apóstoles, sino a Jacobo el hermano del 
   Señor.
</pre>
y cuya muerte (hacía el año 62 dC) es relatada en un escrito extra-bíblico del historiador judio Flavio Josefo (traducción de Antigüedades de los Judios, libro 20, capítulo 9, de {4}):
<pre>
Ananias ... pensaba que tenía una oportunidad apropiada [para ejercer su
 autoridad].  Festo había muerto, y Albino estaba en camino; así que convocó
 al sanedrín de jueces, y trajo ante ellos al hermano de Jesús, al que llaman
 Cristo, cuyo  nombre era Santiago, y a algunos  otros, [o algunos de sus 
acompañantes]; y cuando los había acusado como quebrantadores de la ley, los
 mandó a apedrear.
</pre>
Del mismo Santiago el historiador Hegesipo escribió que le decían el justo, que oraba constantemente (hasta tener callos en las rodillas), y que tenía un excelente testimonio por el cual muchos judíos creyeron en Jesús (ver {4}).

Entonces los Santiagos posibles son:

* Santiago el mayor. Uno de los 12 discípulos, hermano de Juan otro discípulo, hijos de Zebedeo y Salomé.  Muerto en el año 44dC por orden de Agripa (ver Hechos 12:2 y {7}).
* Santiago el menor. Otro de los 12 discípulos, hijo de Alfeo y María.  Tal parece hermano de otro de Judas Tadeo otro de los 12 discípulos.
* Santiago el justo.  Hermano de sangre de Jesús, pero que no fue discípulo.  Ejecutado en el año 62.

Típicamente la tradición cristiana no-católica atribuye al tercer Santiago la epístola de Santiago y suele identificarlo como obispo de la iglesia en Jerusalén.  La tradición católica suele discrepar en esto e identificarlo con  Santiago el menor.

Desde la tradición cristiana no-católica, revisando un poco de la vida del Santiago hermano de Jesús, vemos que inicialmente no creía en Jesús, Juan 7:5 nos dice 
<pre>
Porque ni aun sus hermanos creían en él.
</pre>
y Marcos 3:20-21:
<pre>
20 Y se agolpó de nuevo la gente, de modo que ellos ni aun podían comer pan.
21 Cuando lo oyeron los suyos, vinieron para prenderle; porque decían: 
   Está fuera de sí.
</pre>

Tras la resurrección Jesús se apareció a un Santiago --de acuerdo a 1 Corintios 15:7-- y cabe pensar que se trata de él.
<pre>
7 Después apareció a Jacobo; después a todos los apóstoles;
</pre>

También un Santiago es una de las 3 columnas de la iglesia primitiva mencionado por Pablo en Galatas 6:9 (Se ha descartado que sea Santiago el Mayor quen murió en el 44 dC, mientras que la carta a los Galatas se escribió entre el 50 y 56.)
<pre>
y reconociendo la gracia que me había sido dada, Jacobo, Cefas y Juan, que eran considerados como columnas, nos dieron a mí y a Bernabé la diestra en señal de compañerismo, para que nosotros fuésemos a los gentiles, y ellos a la circuncisión.
</pre>

La evidencia arqueológica que se tiene en el momento es un cofre datado en el año 63d.C que tiene la inscripción "Santiago, hijo de José, hermano de Jesús."   El cofre es propiedad del israelí Oded Golan, quien fue inculpado de falsificación por parte de la Autoridad de Arqueología de Israel, lo mantuvieron en prisión domiciliaria, sin juicio, casi 2 años desde 2003, después afrontó  un juicio que duró 7 años desde el 2005 hasta el 2012.  Durante el juicio quedo claro que el osario es del año 63dC, que proviene del área de Jerusalén y que es autentica de esa epoca la inscripción "Santiago hijo de José."  En el juicio lo acusaban de haber falsificado o mandado falsificar la parte de la inscripción que dice "hermano de Jesús." Sin embargo tras daños hecho por la policía Israelí a esa parte de la inscripción y argumentos en favor y en contra de 126 estudiosos de renombre mundial en diversas áreas como la arqueología, química y fisica, no logró demostrarse que se tratara de una falsificación, por lo que el juez tuvo que absolver a Golan.  La autoridad de arqueología de Israel sólo devolvió el cofre con daños a su propietario en Noviembre del año pasado y él planea ponerlo en exhibición en un museo de Israel (ver {7}).

Por cierto doy testimonio que yo supe de este caso, por lo menos desde el 2008 y desde entonces estuve orando para la verdad saliera y por el israelit Oded Golan.  Doy gracias a Dios que lo mantuvo firme en un juicio tan largo, y que la verdad ha brillado.

Por estas evidencias vamos a considerar que quien escribió ese pasaje, es Santiago el hermano de sangre de Jesús, hijo de María y José. Inicialmente no creyente, pero que creyó tras la resurrección del Señor y llegó a ser obispo de la iglesia de Jerusalén, siendo apedreado por su fe en el año 62d.C.


## 3.3 ¿A quién se la escribió?

Cómo obispo de Jerusalén le escribió a todas las iglesias de su epoca, como dice el versículo 1:1  "Santiago, siervo de Dios y del Señor Jesucristo, a las doce tribus que están en la dispersión: Salud."


# 4. ANÁLISIS

Veamos el versículo 3:16 en detalle.

Dios Habla Hoy
<pre>
Donde hay envidias y rivalidades, hay también desorden y toda clase de maldad;
</pre>

Traducción en lenguaje actual
<pre>
y produce celos, peleas, problemas y todo tipo de maldad.
</pre>

Nueva Biblia Latinoamericana de Hoy
<pre>
Porque donde hay celos y ambición personal[a], allí hay confusión y toda cosa mala.
</pre>

Del Textus Receptus griego, en el cual se basa la traducción Reina Valera dice:
<pre>
3:16  &#8005;&#960;&#959;&#965; &#947;&#8048;&#961; &#950;&#8134;&#955;&#959;&#962; &#954;&#945;&#8054; &#7952;&#961;&#953;&#952;&#949;&#8055;&#945; &#7952;&#954;&#949;&#8150; &#7936;&#954;&#945;&#964;&#945;&#963;&#964;&#945;&#963;&#8055;&#945; &#954;&#945;&#8054; &#960;&#8118;&#957; &#966;&#945;&#8166;&#955;&#959;&#957; &#960;&#961;&#8118;&#947;&#956;&#945;
</pre>

Revisemos 4 palabras:

* &#950;&#8134;&#955;&#959;&#962; z&#275;los: ardor, fervor de espíritu (1) celo por defender algo, (2) indignación, (3) rivalidad contenciosa y envidiosa, (4) celos.
* &#7952;&#961;&#953;&#952;&#949;&#8055;&#945; eritheia: deseo de ponerse delante de los otros, partidismo o parcialidad, clientelismo.
* &#7936;&#954;&#945;&#964;&#945;&#963;&#964;&#945;&#963;&#8055;&#945; akatastasia: inestabilidad, estado de desorden, perturbación o confusión.
* &#966;&#945;&#8166;&#955;&#959;&#962; phaulos: eticamente malo o corrupto
* &#960;&#961;&#8118;&#947;&#956;&#945; pragma: lo que se logra, obra, hecho logrado


El versículo 14 emplea también las palabras &#950;&#8134;&#955;&#959;&#962; y &#7952;&#961;&#953;&#952;&#949;&#8055;&#945;.   Aclara más el sentido de celos en este pasaje poniéndole el adjetivo &#960;&#953;&#954;&#961;&#8057;&#962; pikros que signfica amargos.  Entonces se habla de
* Unos celos amargos o envidia 
* Un egoísmo que sin imparcialidad alguna busca alianzas o partidos con otr@s.
* Un estado de confusión o desorden
* Obras perversas 

Este versículo describe una situación a la que se llega cuando se emplea la sabiduria del mundo, que finalmente es diabólica, no de Dios.   Es esa sabiduria o inteligencia terrenal que busca y en ocasiones alcanza objetivos perversos --pueden ser objetivos dificiles de alcanzar por ejemplo relacionados con riquezas o con poder.

Pienso que pudo ser dificil para Santiago reconocer a Cristo en su hermano de sangre Jesús, con quien había compartido posiblemente desde su niñez. Pero a diferencia de los líderes religiosos de su tiempo, tras la resurrección del Señor pudo reconocerlo y en su epistola nos enseña que requirió y que nosotros requerimos la sabiduria de Dios.  Que nuestras fuerzas e inteligencia no bastan, pues pueden ser usadas o guiadas por Satanas.  Pues naturalmente en nuestro corazón hay lo mismo que lo guió a él:
* Envidia
* Orgullo y egoísmo
   
Pero el resto de este texto nos ayuda a ver que hacer:

* Pedir sabiduria de lo alto.  La sabiduría de lo alto viene acompañada de humildad y reconocer que dependemos de Dios y no de nuestra cabeza. Santiago 3:17 "Pero la sabiduría que es de lo alto es primeramente pura, después pacífica, amable, benigna, llena de misericordia y de buenos frutos, sin incertidumbre ni hipocresía."
* Más que hablar mucho, que nuestras obras den cuenta de nuestra fe.  Por ejemplo Santiago 3:7-8 dice "7 Porque toda naturaleza de bestias, y de aves, y de serpientes, y de seres del mar, se doma y ha sido domada por la naturaleza humana; 8 pero ningún hombre puede domar la lengua, que es un mal que no puede ser refrenado, llena de veneno mortal."  y Santiago 2:24 y 26 "24 Vosotros veis, pues, que el hombre es justificado por las obras, y no solamente por la fe... 26 Porque como el cuerpo sin espíritu está muerto, así también la fe sin obras está muerta.
* ¿Tenemos celos amargos y contención en nuestro corazón?, más bien no nos enorgullezcamos, ni mintamos.  No nos parcialicemos con otras personas, como en partidos políticos, para ir contra alguien, no hagamos acepción de personas.   Santiago 2:8-9 dice "Si en verdad cumplís la ley real, conforme a la Escritura: Amarás a tu prójimo como a ti mismo, bien hacéis;  pero si hacéis acepción de personas, cometéis pecado, y quedáis convictos por la ley como transgresores."
* ¿Queremos justicia?  Sembremosla con paz.


# 5. CONCLUSIÓN Y ORACIÓN

Un versículo nos ha permitido escudriñar más sobre Santiago, su testimonio y su carta, oramos que esas partes que nos hayan servido para nuestra situación las apliquemos en el nombre de Jesús.

* Señor gracias por Tu sabiduría, regalanos de ella junto con Tu presencia para que nos nos desviemos.  Ayudanos a discernir cuando no estemos confiando en Ti sino en ídolos.
* Señor ayudanos a mostrar nuestra fe por las obras, que antes de predicar obremos de acuerdo a lo que predicamos y nuestra vida sea la prédica.
* Señor por favor quita de nuestro corazón la envidia y el egoísmo, ayudanos a concentrarnos en Ti, a agradecerte por todo lo que nos has dado y por lo que les has dado a los demás y a reconocer que sólo Tu eres Dios.
* Señor por favor ayudanos a discernir de que hablar y con quien, que no búsquemos beneficio y poder, sino a Ti.  Que no nos aliemos con otras personas para maltratar o menospreciar a otras.
* Por favor mantenos en la verdad, aún cuando vengan dificultades.
* Señor limpia nuestro corazón de envia y egoísmo.  Que podamos verlo y cambiar.  
* Que sembremos Tu paz que sobrepasa todo entendimiento, para que florezca justicia.
* Señor Jesús Tu eres la verdad ayúdanos a reconocerte, a verte y entender que Te necesitamos.   Si alguien no lo ha hecho está invitad@ a hacer una [Oracion de fe] en el Señor Jesucristo.


# 6. REFERENCIAS

* {1} RV1960
* {2} http://es.wikipedia.org/wiki/Santiago_el_Menor
* {3} http://es.wikipedia.org/wiki/Santiago_el_Mayor
* {4} http://es.wikipedia.org/wiki/Santiago_el_Justo
* {5} http://www.biblestudytools.com/history/flavius-josephus/antiquities-jews/book-20/chapter-9.html
* {6} http://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=G2385&t=KJV
* {7} http://jamesossuarytrial.blogspot.com/

----
Vladimir Támara cede este escrito al dominio público y agradece a Dios la oportunidad de predicarlo en la Iglesia Menonita de Suba el 17 de Agosto de 2014.

Imagen de dominio público de https://openclipart.org/image/400px/320578