---
layout: post
title: "Transformacion_y_Resolucion_de_Conflictos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Los conflictos ocurren, el objetivo para los cristianos es resolverlos y por eso el centro de los mismos es el perdón y lo que Jesús nos enseñó al respecto:

* Mateo 18:15-20

Jesús mismo práctico el método que enseñó:

* Lucas 9:

Nuestras creencias subyacen en la forma de resolver conflictos, una particular nuestra autoestima pues de ella depende en gran medida la forma en la que responderemos.  Como puede verse en  Gen 1:26-31, tenemos atributos de Dios: inteligencia, creatividad; además nos ama como nadie y nos ayuda.

* Enseñanza de Maria Helena Arango. San Nicolas. 

---------
La siguiente cartilla de dominio público fue copiada el 11.May.2010 de:
http://fpv.gfc.edu.co/?id=Cartilla+Resolucion+de+Conflictos

----

# Introducción
# Testimonios
# ¿Por qué y para qué aprender sobre resolución de conflictos?
# ¿Cómo resolver conflictos?
## Importancia de creencias
## Requerimientos generales para la resolución
## Conducto
## Asertividad
## Mediación
## Resolución de conflictos en el aula
# Conclusión
# Créditos y Términos
# Bibliografía

#1. Introducción

''Amad a vuestros enemigos y orad por los que os persiguen, para que seáis hijos de vuestro Padre que está en los cielos; porque El hace salir su sol sobre malos y buenos, y llover sobre justos e injustos.'' Mat 5:44

Por solicitud de padres de familia, para la tercera escuela de padres 
del Gimnasio Fidel Cano de 2006, abordamos la resolución de conflictos.  

Esta cartilla busca sintetizar aprendizajes colectivos que desarrollamos 
con estudiantes, padres, profesores y colaboradores del colegio.   
Creemos indispensable prevenir los conflictos (por ejemplo con oración y 
diálogo), pero para tratar los conflictos existentes o los inevitables 
pidamos valor y guianza.

El enfoque desarrollado no es el único pero pedimos a nuestro Creador que así como en las reuniones preparatorias que hicimos, aquí quede algo de Su amor y destellos de Su sabiduría para resolver nuestros conflictos y mediar en conflictos con herman@s.

Cada situación como se mencionaba anteriormente, tiene muchas ópticas para ser abordada. Sin embargo es importante tener en cuenta los siguientes tips a fin de realizar la elección adecuada en la resolución pacífica del conflicto:
* El conflicto NUNCA es la persona, es simplemente una situación que junto con el otro, se debe resolver.
* El conflicto o la diferencia es fundamental en la construcción del tejido social. De éste surge siempre un aprendizaje que nos permite crecer en sociedad y también como individuos.
*Procuremos siempre resolver "el problema" directametne con quien está involucrado en él, pero también generemos la conciencia de que aveces también resulta pertinente recurrir a terceros para que medien en la solución del conflicto siempre y cuando estén capacitados para ello.
*No olvidemos el poder de la palabra ya que con ella se puede destruir a un oponente pero también CONSTRUIR para la sociedad.



#2. Testimonios


Los siguientes son testimonios de integrantes de la comunidad educativa del Gimnasio Fidel Cano.
* Tuve un empleador supremamente agresivo, él llegó a tirarme papeles en la cara mientras me decía que si quería que me fuera.  Típicamente cuando hay una parte agresiva, la otra es pasiva.  Cuando no se resuelve un problema queda pendiente, y suele sacarse tiempo después.
* Mi esposo es muy agresivo. Intelectualmente es muy inteligente, pero emotivamente no.  Tomó la decisión de hacer una compra para ayudar a un hermano, pero sin consultarme como esposa.  Tuve que hacer un esfuerzo grande, pero me quedé en silencio, pensé "Dios mio" y lo mire por el lado bueno.  Finalmente el hermano no aceptó la ayuda.
* En medio de una separación, ayer estaba visitando a mis hijas, una tenía una tarea y yo quería ayudarle a hacerla.  Pedí un lápiz prestado a la mamá quien me dijo de mala forma que no, que yo ni eso llevaba, que ni para eso servía.   Me quedé en silencio un instante y después dije que me respetara, y aún más en presencia de las niñas, ella dijo que antes yo le había dicho ciertas cosas en presencia de ellas, le dije que no lo había vuelto a hacer recientemente y que me disculpara por haberlo hecho.  Un rato después me prestó el lápiz y pudimos avanzar en la tarea.
* Con una persona teníamos diferentes puntos de vista, disentíamos en un tema. Llegamos a durar hasta 8 días sin dirigirnos la palabra ni saludarnos. Opté por saludar normalmente y logré que a pesar de las diferencias profesionales se mantuviera una norma de cordialidad y respeto de saludar y no crear rencores.
* Un sobrino que convivía con mi familia a los 14 años ingresó a una secta satánica, gracias a Dios y al apoyo de la familia logró salir, pero a los 16 años entró al mundo de las drogas donde estuvo hasta los 19 años.  En esta época tenía que ser físicamente levantado del piso y la familia no esperaba ya nada de él, era sólo un problema. Tuvo que iniciar undécimo 5 veces.   Cuando terminó bachillerato no quería estudiar, y prefirió ayudar en la cocina hasta que decidió que quería estudiar filosofía, lo cual hizo y hoy gracias a Dios está por graduarse.    El cambio se logró con ayuda de Dios, oración, tiempo, paciencia, diálogo, amor, fe y esperanza, porque en particular yo, aunque toda la familia no daba más que malos augurios sobre él, lo animaba diciéndole que él saldría de eso y le ayudaba físicamente.
* La historia del actual grado 10 del colegio ha sido de conflictos, los conflictos interpersonales han escalado tanto y las dificultades de convivencia llegaron este año a ser tan extremas que diseñaron un sistema de tarjetas para expresarse aprobación o desaprobación.  Cuando se presente la tarjeta de desaprobación todos los estudiantes detienen la actividad que estén realizando.  Su buena idea ha sido socializada a todos los estamentos del colegio.
* Los conflictos familiares se reflejan en el colegio.  Por ejemplo los problemas de divorcio se notan en los estudiantes, así como la soledad que algunos estudiantes viven en casa.


#3. ¿Por qué y para qué aprender sobre resolución de conflictos?

''Y cuando estéis orando, perdonad si tenéis algo contra alguien, para que también vuestro Padre que está en los cielos os perdone vuestras transgresiones.''  Marcos 11:25

Vivimos una realidad de violencia a todos los niveles, aunque es dificil
debemos mirarla con ayuda de Dios para entender hacía donde dirigirnos:
* Violencia política: En el 2005 en {5} se documentan  702 personas ajenas al conflicto político asesinadas por causa de este (191 por parte de  fuerzas regulares del estado, 377 por parte de paramilitares y 174 por parte de grupos guerrilleros). Para el mismo periodo se documentan 1104 miembros de grupos armados  asesinados.
* Violencia social: Para el decenio pasado en {6} se mencionan más de 30.000 víctimas fatales de violencia social al año.  En esta se incluye la violencia intrafamiliar, a la cual no somos ajenos {8}.

Redescubrimos que la violencia no resuelve los problemas, mientras seguimos soñando con una sociedad incluyente donde se fomenten valores, se respete la dignidad humana y la vida.  Queremos ser ejemplo para nuestros hijos, una convivencia sana y vivir dignamente.  Nuestras familias y el colegio son pequeñas comunidades que desde el obrar de sus individuos ayuda a moldear la sociedad. Ya sabemos que no queremos la paz presentada en medios masivos, queremos una paz que muchos entendemos como  de Dios, una paz con justicia, verdad, amor, humildad, conciencia, perdón, gratitud y tolerancia {7}.

Sin embargo nuestra práctica cotidiana nos aleja de ese sueño de sociedad, en
particular por nuestra forma de enfrentar los conflictos.
El conflicto es algo propio de nuestra realidad y nos servirá para construir sociedad si lo miramos como una oportunidad.  

En el centro del conflicto está LA DIFERENCIA, que posibilita la construcción de una sociedad cuando se relaciona con EL RESPETO.

__El conflicto es entonces una oportunidad de hacer una construcción propositiva que logre transformaciones individuales y colectivas__

#4. ¿Cómo resolver conflictos?
''Y vosotros, maridos, igualmente, convivid de manera comprensiva con vuestras mujeres, como con un vaso más frágil, puesto que es mujer, dándole honor como a coheredera de la gracia de la vida, para que vuestras oraciones no sean estorbadas.'' 1 Pedro 3:7


Consideramos un conflicto resuelto con éxito cuando se establece la verdad, con perdón y solidaridad se garantiza justicia reparativa/restaurativa y el/los agresor(es) reparan sus agresiones y acuerdan pactos para prevenir que se repita. Este proceso se cierra apropiadamente con una celebración {9} y {10}. 

La justicia reparativa/restaurativa es diferente a la tradicional justicia retributiva ---en cuyo marco por ejemplo un ofensor paga con cárcel su ofensa.  En la justicia reparativa/restaurativa el ofensor, el ofendido y la comunidad afectada son los actores centrales en el proceso de la justicia, mientras que las autoridades son facilitadores `de un sistema enfocado hacia la rendición de cuentas del ofensor, la reparación que éste hace a la víctima y la participación plena de los tres actores.' {11}


##4.1 Importancia de Creencias
''Y vosotros, padres, no provoquéis a ira a vuestros hijos, sino criadlos en la disciplina e instrucción del Señor.'' Ef 6:4


|__Problema externo__| Aquello con lo que estamos o no de acuerdo|
|__Motivaciones no percibidas__| La razón por la que estamos o no de acuerdo y si estamos a favor o en contra el uno del otro|
|__Convicciones subyacentes__| Lo que creemos acerca de Dios, nosotros mismos y nuestras circunstancias.  |

Las ``capas'' presentadas tomadas de {1} explican la relación entre 
conflictos y creencias.  Las convicciones subyacentes o creencias moldean, 
no sólo el motivo del desacuerdo, sino también la forma en que disentimos y
en la que buscamos solución.


##4.2 ¿Requerimientos generales para la resolución de conflictos?
''Honra a tu padre y a tu madre, para que tus días sean prolongados en la tierra que el SEÑOR tu Dios te da.'' Ex. 20.12

Estudiando algunos pasajes bíblicos (Génesis 13:1-18, Éxodo 20:17, Santiago 3:13-16, Lucas 4:23-28, Jueces 6:1,16; 7:19-23) con padres de familia hemos llegado a los siguientes requerimientos:

* Orar pidiendo guianza y ayuda a Dios, con fe y obedeciendo a las respuestas que nos da.  Su sabiduría, amor y conocimiento de la realidad de cada uno lo hacen el mediador perfecto.  Buscar imitarlo más y mejor, conocerlo más, buscar diariamente su palabra (los cristianos creemos que se encuentra en la Biblia).
* El amor es el centro. Nos ayuda a ser humildes y trabajar para la resolución. Nos permite vencer al mal con el bien (Rm 12:21).  Nos permite, con guianza de Dios, hacer sacrificios para resolver conflictos.
* Verdad como base.  Conocer bien el conflicto.  Ser sinceros. 
* Buena comunicación: diálogo lo más directo posible cuando se logre un tiempo de calma (esperarlo de requerirse). Expresar sentimientos. "La suave respuesta quita la ira, pero la palabra áspera aumenta el furor (Prov 15:1)"
* Ser creativo. Dar tiempo y trabajar cuando es el momento para resolver el conflicto.
* No renunciar a lo que la Dios y la conciencia manden (por ejemplo aún ante la inminencia de la muerte por el conflicto con autoridades de su momento Jesús no renunció a su misión Mat 26:38-39).
* Aprender a perdonar (Mat 18:21). No guardarse resentimientos. Reconocer cualidades y defectos del otr@.
* Discernir lo que está bien y lo que está mal, pero sin condenar. No juzgar para no ser juzgado (Lc 6:37)
* De requerirse, buscar una buena mediación (sugerimos una cristiana, por ejemplo en el colegio, los centros ESPERE {10} o en Justapaz).
* Prevenir que se repitan situaciones de vulneración, pero sin cerrar las posibilidades para que el/la otr@ cambie.  Buscar señales de arrepentimiento (por ejemplo si el ofensor comienza a decir la verdad o busca al ofendido para pedirle perdón).


##4.3 Conducto

Cómo en el caso de violencia intrafamiliar {8} proponemos seguir un conducto mediado por los requerimientos recién presentados.  El conducto es el propuesto por Jesús de acuerdo a Mateo 18:15-17

''Y si tu hermano peca, ve y repréndelo a solas; si te escucha, has ganado a tu hermano. Pero si no te escucha, lleva contigo a uno o a dos más, para que toda palabra sea confirmada por boca de dos o tres testigos. Y si rehúsa escucharlos, dilo a la iglesia; y si también rehúsa escuchar a la iglesia, sea para ti como el gentil y el recaudador de impuesto.'' 

Jesús en su tiempo trató con amor especial a gentiles y recaudadores de impuestos, de hecho llamó a Levi a su ministerio.  Por esto interpretamos 'gentil/recaudador de impuesto' como alguien que requiere más amor, más con nuestra actitud le hacemos saber y sentir que no aprobamos algo de su actuar como se lo hemos dicho en privado, frente a mediadores (más que sólo testigos) y frente a una comunidad afectada.  En todo caso l@ tratamos sin crueldad y con respeto,  pues 'para "estar por ellos" debemos estar "contra ellos" en su pecado.' {2}   Consideramos que en algunos casos, si no hay arrepentimiento del ofensor el conducto debe salir del entorno familiar (e.g en caso de violencia intrafamiliar como presentamos en {8}).

El testimonio del sobrino que acudió a las drogas (ver sección Testimonios), muestra un caso en el que la familia tuvo que llegar a tratarlo como a un "gentil". En otros casos en esta misma situación a la persona eventualmente la habrían echado de la casa, aquí por amor le dieron amplia oportunidad pero con restricciones justas y que posibilitaron hasta la convivencia por 3 años, mientras completaba su recapacitación.

Note que la primera "sanción" --que se hace después de hablar con el ofensor y que este no responda-- es contar el caso a uno o dos mediadores, quienes también mantendrán reserva, excepto si deben pasar a la etapa siguiente de socializar a la comunidad afectada.

##4.4 Asertividad

''Venid a mi todos los que estáis cargados y cansados y yo os haré descansar'' Mateo 11:28. Entreguemosle nuestros sentimientos a El y si hay resentimiento pidamosle que lo cambie por amor, bendición hacia quienes nos han ofendido y oremos por ellos. 


!Formas de responder

En una discusión (por ejemplo en el primer paso del conducto presentado) se 
puede responder de varias formas, por ejemplo:
* Indiferencia: como quien no tiene que ver.
* Huida permanente: aunque en ocasiones retirarse momentáneamente puede ser buena opción, huir reiterativamente para evitar el conflicto sólo lo aumenta.
* Agresividad: Expresa nuestra arrogancia y en muchos casos machismo o feminismo.  Puede ser resultado de nuestra historia, de la forma como fuimos educados.  Pidamos a Dios que sane nuestras heridas, que borre los golpes, las lágrimas de dolor y ofrezcamoslas si vuelven a brotar.   Si no logramos sanarnos, con seguridad maltrataremos a nuestro prójimo (también a nuestros hijos y espos@).
* Capitulación: la persona decide rendirse y usar una máscara "todo esta bien" pero con el tiempo estalla.
* Chisme, Ironía y Sarcasmos: Es equivalente a la agresividad y daña nuestra personalidad.  No es fácil renunciar a la agresividad, pidamos perdón a Dios por las veces que hemos sido agresivos, pues Él es justo, fiel y misericordioso para perdonarnos.
* Asertivamente: expresando sentimientos y deseos positivos y negativos de una forma eficaz sin negar o desconsiderar los de los demás y sin crear o sentir vergüenza. 

La respuesta agresiva produce en el receptor cólera, humillación y dolor, en el emisor produce sentimientos de superioridad, desprecio y posteriormente culpa.

La respuesta pasiva (indiferencia, capitulación, huir) es emocionalmente deshonesta, en quien la da produce ansiedad, frustración, cólera, dolor y en el receptor puede producir  culpa o superioridad e irritación o pena contra quien da la respuesta pasiva.

La respuesta asertiva es apropiada en contextos en los que la expresión personal es importante (por ejemplo en la familia) y ofrece ventajas:  Edifica la autoestima del otr@ y la mía.  Es directa. Así que ayuda a resolver los conflictos en los tiempos apropiados.  Produce satisfacción.  Es honesta, promueve un pensamiento crítico y reduce ansiedad.  Promueve buenas relaciones interpersonales y mejora compresión.  Incrementa auto respeto. Fomenta la toma de decisiones propias.  El receptor se siente respetado, valorado y apreciado.

!La respuesta asertiva
Libremonos de resentimientos  y tomemos nuestro tiempo para clarificar el mensaje.  Al expresar sentimientos, verbalicemos muy bien lo que queremos decir  siendo explícitos en:
# Manifestar primero las cualidades del otr@
# Mencionar la situación que nos ha molestado
# Expresar como nos sentimos. La expresión de sentimientos debe ser clara, directa, honesta, haciéndonos responsables de nuestros sentimientos y no culpando a los demás por estos. Cambiemos frases como : "ME HICISTE SENTIR ..." por "YO ME SIENTO ... CUANDO TU ..."
# Expresar consecuencias positivas para el otro inicialmente y de requerirse pasar a las consecuencias negativas.

!Ayudas para una comunicación asertiva
Una comunicación asertiva requiere cuidado a la comunicación no verbal:%%%
__Postura__:  cabeza y cuerpo derecho, distancia prudente. %%%
__Contacto visual__: mirar de frente y a los ojos de la otra persona %%%
__Gestos del cuerpo__: relajado y moderado %%%
__Expresión facial__: de acuerdo a lo que siente y expresa %%
__Tono y volumen de la voz__: pausado, firme, de acuerdo a lo que quiere comunicar

Una vez comienza la comunicación, eventualmente, puede mejorarse con alguna de las siguientes técnicas:
* Tiempo fuera de juego : interrumpir discusión si no hay claridad
* Disco rayado:  no responder a nada que se salga del tema de discusión
* Repetir o parafrasear lo que el otro dice para verificar que se entiende.
* Inversión: no usar "estás muy desordenad@" sino primero lo positivo y después con precisión lo que quiere que haga.
* Fraccionar Interrupciones: no defender las acciones hasta que el mensaje que vamos a dar sea totalmente claro.
* Privación de cólera:  si la otra persona esta de mal genio proporcionarle mínima información no muy explicita y luego utilizar la técnica de disco rayado. 
* Banco de Niebla:  cuando es cierto aceptar que el otro tiene razón pero mostrar una causa mayor que implica cambio {3}.

Para posibilitar la comunicación asertiva, debemos cambiar algunos pensamientos erróneos:
| Puedo decir no | Puedo aceptar un no como respuesta |
| No soy perfecto  | Puedo cuestionar autoridades |
| Puedo enojarme (sin agredir) | No tengo que ser admirado por tod@s |


##4.5 Mediación en conflictos
''Y vosotros, padres, no provoquéis a ira a vuestros hijos, 
sino criadlos en la disciplina e instrucción del Señor.'' Efesios 6:4 

En caso de obrar como mediador en un conflicto, además de los requerimientos generales antes descritos (consideramos especialmente importante el amor, el perdón y la guianza de Dios), pueden tenerse en cuenta:
* Verificar realización del paso anterior, apoyando a los involucrados.
* Buscar verdad analizando el conflicto y sin parcializarse.
* Sintamos junto con cada involucrado el dolor del otro.  Podría acudirse a un juego de roles que permita ponerse en la posición del otro.  
* Buscar un punto de arreglo, en el que se asuman responsabilidades y consecuencias, con sanciones y compromisos para que no se repita.
* Pedir disculpas si el conflicto obliga a tratar temas de la intimidad de alguna de los involucrados que puedan causar vergüenza.

Para establecer verdad en el paso de análisis del conflicto puede acudirse al
siguiente triángulo:
<pre>
     Persona

Problema    Proceso
</pre>

(1) Persona: definir todos los involucrados en el conflicto; (2) Problema: definir en que consiste el conflicto y cuales son sus causas; (3) Proceso: saber como se desarrolla el conflicto {4}

Estos vertices pueden construirse escuchando ambas partes y eventualmente a testigos. Las partes deben colaborar dando la información que el mediador solicite.

##4.6 Resolución de conflictos en el aula

Testimonio de la profesora Mónica Casadiego:

Ella notó conflictos en tercero, dio espacio para que se se expresaran en 10 minutos de la clase,  "En vez de decirles algo, los escuché".  Anota otros puntos importantes para resolver conflictos en el aula {12}:
* Crear un ambiente favorable para los niños
* Calmarse para escuchar. Observar . Esperar. No ponerles etiquetes.
* Que el miedo a ceder no me cierre los oídos.
* Repetirse: lo que me tenga que decir es importante
* La confianza abre corazones.
* Mirar las cosas desde los ojos del otro. Perspectiva de ellos. No necesariamente estar de acuerdo pero entenderlos.
* Resistir tentación de juzgar.
* Escucharse a si mismo. Revisarse.



#5. Conclusión

Las herramientas presentadas en esta cartilla pueden aplicarse a conflictos en diversos ámbitos (incluso nacional).  En el contexto familiar, como espos@, padres y/o hijos nos dirigimos a nuestra pareja, a nuestros hijos o a nuestros padres por ejemplo tras una ofensa para hacer peticiones y de requerirse aplicar sanciones (no físicas) buscando que cambien algunas cosas y sean mejores cada día, aprovechando el conflicto y las diferencias para construir:
# Oramos a nuestro Padre pidiendo guianza y ayuda.
# Tomamos tiempo para pensar con sinceridad en el mensaje antes de decirlo:
## Buscamos los aspectos positivos por resaltar, verbalizamos la situación así como nuestros sentimientos y planteamos de forma específica los cambios que deseamos y de requerirse la reparación que esperamos.
## Identificamos las consecuencias positivas del cambio y la reparación primero para la persona y luego para otros (también las negativas si no hay cambio).
# Como ofendido le hablamos en privado, acordando que se mantiene reserva si se llega a conciliación.  Empleamos asertividad. Manifestamos el afecto, amor y aprecio que sentimos.  Escuchamos con sensibilidad su versión y sentimientos.  Aseguramos verdad para continuar.
## Manteniendo autoridad pero con misericordia, abrimos espacio a la conciliación de sanciones y formas de reparación. Dejamos que el aire del perdón fluya para que sane heridas cuando el ofensor lo pida y/o el ofendido lo conceda y si el esfuerzo lo amerita, alivie sanciones al ofensor.
## Celebramos la reconciliación ojala comenzando con un abrazo.
# Si no se establece verdad o no atiende hablando a solas, hacerlo en compañía de otro(s) miembro(s) de la familia. Oramos en grupo, aplicamos herramientas de mediación para buscar verdad, conciliación de sanciones y reparación para llegar a celebrar si se logra.
# Si no se establece verdad o no atiende y es posible, poner una sanción que le haga notar el desacuerdo de la familia entera o si el caso lo requiere pasar a otra instancia, e.g. mediadores preferiblemente cristianos que compartan nuestro enfoque (e.g algunos psicólogos, colegio, centros ESPERE, Justapaz).


#6. Créditos y Términos

Han aportado para esta cartilla:
* Padres de familia: Betty Castro, Bernarda Bernal (7), Adriana Torres con Santiago (3), Eliza Maldonado (8), Sonia Gómez (7), Gonzálo Gonzáles y Nancy Abril (3), Gilma Carvajal (4), Fabiola Charry y Carlos Estupiñan (8), Enrique Mora (6), Amparo de Vargas (9), Fernando Sarmiento (2 y 3).  Profesores: Amanda Arias, Maria Victoria Bermudez, Illya González, Hernan Quishpe.  Invitados y Colaboradores: Rosario Diaz de U. Santo Tomás, Jesús Vargas del centro de conciliación Justapaz. Edición y diseño: Vladimir Támara
* Impresión financiada por la Asociación de Padres de Familia del Gimnasio Fidel Cano.

Esta obra se cede al dominio público.  Por favor cite como fuente en
Internet http://fpv.gfc.edu.co sitio donde encontrará la versión más
actualizada.

#7. Bibliografía

* {1} Biblia de las Américas
* {2} ¿Cómo podemos resolver nuestras diferencias?  Serie Discovery. 1997.
* {3} Presentación. Comunicación Asertiva: Para la expresión y desarrollo de la Autoestima. Juan Valdés Lazo. 2004 http://titulov.uprutuado.edu/titulovnew/consejeria/Presentaciones/Comunicacion%20Asertiva.ppt
* {4} Guía de Formación para la Vida de 8.   Maria Victoria Bermudez. Gimnasio Fidel Cano.
* {5} Revista Noche y Niebla. No 32 y 31. 2005. Banco de Datos de Violencia Política, Derechos Humanos y Derechos Internacional Humanitario. CINEP. http://www.nocheyniebla.org/
* {6} El sufrimiento de Colombia. Macarena Aguilar. Revista Internacional de la Cruz Roja y de la Media Luna. http://www.redcross.int/ES/mag/magazine2000_4/Colombia.html
* {7} Segunda reunión de Análisis de Información. Abr.2006. Gimnasio Fidel Cano.  http://fpv.gfc.edu.co/?id=2006%2C+Reuni%F3n+An%E1lisis+2
* {8} Violencia intrafamiliar. Segunda Escuela de Padres en el Gimnasio Fidel Cano. 2006. http://fpv.gfc.edu.co/?id=Escuela+de+Padres.+28.Abr.2006
* {9} Verdad, Justicia y Reparación.  Una propuesta bíblica de iglesias cristianas evangélicas de Colombia.
* {10} Descripción de Escuelas de Perdón y Reconciliación. ESPERE. Leonel Narvaez. http://drclas.fas.harvard.edu/revista/?article_id=539
* {11} ¿Qué es la Justicia Restaurativa? Dan Van Ness. http://www.justicia-restaurativa-colombia.org
* {12} Estudio de la UNICEF. Guía para docentes una forma de resolver conflictos.
