---
layout: post
title: Señor eres fiel
author: vtamara
categories:
- Alabanza
image: assets/images/home.jpg
tags: 

---
Marcos Barrientos 

```
Señor eres fiel y tu misericordia eterna
Gente de toda lengua y nación
De generación a generación

//Te adoramos hoy, aleluya, aleluya
Te adoramos hoy, eres Señor//
Eres fiel

Señor eres fiel y tu misericordia eterna
Gente de toda lengua y nación
De generación a generación

//Te adoramos hoy, aleluya, aleluya
Te adoramos hoy, eres Señor//

We worship you, aleluya, aleluya
We worship you, for who you are

Te adoramos hoy, aleluya, aleluya
Te adoramos hoy, eres Señor
Eres fiel

////Eres fiel, siempre fiel
Siempre fiel, eres fiel////
```

* Canción: <http://www.youtube.com/watch?v=cbn-Lh5C7qo>
* Letra basada en:  <http://www.musica.co/letras.asp?letra=989533>