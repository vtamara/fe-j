---
layout: post
title: "Peregrinacion_Grace_2010"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Los miembros de la comunidad de Paz San José de Apartado y de la comunidad de Tamera que realizan esta peregrinación, hoy 4 de Noviembre estarán en el colegio claretiano de Bosa realizando diversas actividades.

De 8:30AM a 10:30AM Recorrido por varios sitios de Bosa saliendo y regresando al colegio claretiano.

De 2:00PM a 4:00PM foro y taller en el colegio claretiano.

De 4:00PM a 5:00PM obra teatral

De 5:00PM a 6:00PM celebración eucarística

De 6:00PM a 8:00PM muestra cultural 
