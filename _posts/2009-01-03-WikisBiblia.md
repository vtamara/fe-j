---
layout: post
title: WikisBiblia
author: vtamara
categories: 
image: assets/images/home.jpg
tags: 

---
Hay varios wikis que intentan hacer un comentario de la biblia:

* [http://bible.tmtm.com/wiki/Main_Page GFDL]()
* [http://www.wikichristian.org/ GFDL Graham Grove]()
* [http://www.theopedia.com/ ]()Dominio Público, con su contraparte en español [http://www.teopedia.com]()
* [http://www.reformedword.org/Home]() CC atribución, compartir igual.

Que buscan ser enciclopedias bíblicas:

* [http://orthodoxwiki.org/ ]()GFDL y CC
* [http://christianity.wikia.com/wiki/Welcome]() GFDL

Enciclopedias creacionistas

* [http://creationwiki.org/Main_Page]() GFDL

Enciclopedias generales pero desde el punto de vista cristiano:

* [http://www.conservapedia.com/Main_Page]() con licencia contradictoria entre dominio público y todos los derechos reservados

Recursos para evangelizar:

* [http://en.jesus-wiki.org/wiki/Main_Page]()

Incluso hay espacio para discutir wikis cristianos:

* [http://www.wikichristian.org/index.php/Talk:Christian_wikis]()