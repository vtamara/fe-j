---
layout: post
categories:
- Predica
title: En 2023 confiemos en Dios más que en nosotros mismos. 2 Cor 1:8-14
author: vtamara
image: "/assets/images/advise-lightbulb.png"

---
# En 2023 confiemos en Dios más que en nosotros mismos. 2 Cor 1:8-14

Iglesia Menonita de Suba

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 25.Abr.2021

# 1 Introducción

Para comenzar este 2023 el Señor me ha dado el privilegio de compartir de su Palabra y  me habló en 2 Cor 1:9 “Pero tuvimos en nosotros mismos sentencia de muerte, para que no confiásemos en nosotros mismos, sino en Dios que resucita a los muertos;”.

# 2. Texto: 2 Cor 1:8-14

8 Porque hermanos, no queremos que ignoréis acerca de nuestra tribulación que nos sobrevino en Asia; pues fuimos abrumados sobremanera más allá de nuestras fuerzas, de tal modo que aun perdimos la esperanza de conservar la vida. 9 Pero tuvimos en nosotros mismos sentencia de muerte, para que no confiásemos en nosotros mismos, sino en Dios que resucita a los muertos; 10 el cual nos libró, y nos libra, y en quien esperamos que aún nos librará, de tan gran muerte; 11 cooperando también vosotros a favor nuestro con la oración, para que por muchas personas sean dadas gracias a favor nuestro por el don concedido a nosotros por medio de muchos.

12 Porque nuestra gloria es esta: el testimonio de nuestra conciencia, que con sencillez y sinceridad de Dios, no con sabiduría humana, sino con la gracia de Dios, nos hemos conducido en el mundo, y mucho más con vosotros. 13 Porque no os escribimos otras cosas de las que leéis, o también entendéis; y espero que hasta el fin las entenderéis; 14 como también en parte habéis entendido que somos vuestra gloria, así como también vosotros la nuestra, para el día del Señor Jesús.

# 3. Contexto

Hechos 18:1-17 indica que Pablo fundó la iglesia en el puerto de Corinto (en Grecia) durante su segundo viaje misionero, a donde llegó posiblemente hacia finales del año 49 y donde guiado en visión por Dios se quedó un año y medio estableciendo la iglesia. Salió cuando los judios lo llevaron al tribunal del procónsul Galión --quien se negó a escucharlos porque no había delito y los expulsó. Tras esto Pablo salió de Corinto para continuar su viaje de regreso a Jerusalén.

  
![](/assets/images/viaje2.png)

Segundo viaje misionero. Años 49-51. [https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html](https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html "https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/segundo-viaje-misionero-de-pablo/index.html")

La primera carta a los corintios fue escrita desde Efeso según 1 Cor 16:8. Esto debió ocurrir durante el tercer viaje misionero de Pablo cuando se quedó en Efeso por dos años como indica Hechos 19:10. Según Davis, C. 2022. la segunda carta a los Corintios también habría sido escrita tiempo después en la misma estadía en Efeso. Empleando la datación de McCallum, D. 2022, Pablo habría escrito esa segunda carta a los Corintios aproximadamente en el año 54.

![](/assets/images/tercer.png)

Tercer viaje misionero. Años 52-55. [https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/tercer-viaje-misionero-de-pablo/draft.html](https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/tercer-viaje-misionero-de-pablo/draft.html "https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/tercer-viaje-misionero-de-pablo/draft.html")

# 4. Análisis

  
Entonces el versículo 2 Cor 1:8 debe hablar de alguno de los eventos que le ocurrieron a Pablo en Asia antes de escribir la carta a los Corintios durante su estadía en Efeso en el 54. Veamos algunas posibilidades:

* Cuando Pablo fue apedreado en Listra durante su primer viaje misionero aprox. en el año 48. Hechos 14:19-20

19 Pero vinieron algunos judíos de Antioquía y de Iconio, y habiendo persuadido a la multitud, apedrearon a Pablo y lo arrastraron fuera de la ciudad, pensando que estaba muerto. 20 Pero mientras los discípulos lo rodeaban, él se levantó y entró en la ciudad. Y al día siguiente partió con Bernabé a Derbe.

![](/assets/images/primer.png)

Primer viaje misionero. Año 48. [https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/primer-viaje-misionero-de-pablo/draft.html](https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/primer-viaje-misionero-de-pablo/draft.html "https://uploads.knightlab.com/storymapjs/7da0d4ce8dd20bff4bda2a6776201836/primer-viaje-misionero-de-pablo/draft.html")

* Cuando fue librado por sus hermanos de iglesia en Efeso de los plateros que hacían filigrana en honor de su diosa Diana y se fueron contra los cristianos de esa ciudad durante el tercer viaje misionero de Pablo antes de escribir la 2da carta pero tal vez en el mismo año (aprox. 54). Hechos 19:28-31

28 Cuando oyeron esto, se llenaron de ira, y gritaban, diciendo: ¡Grande es Diana de los efesios! 29 Y la ciudad se llenó de confusión, y a una se precipitaron en el teatro, arrastrando consigo a Gayo y a Aristarco, los compañeros de viaje de Pablo, que eran de Macedonia. 30 Cuando Pablo quiso ir a la asamblea, los discípulos no se lo permitieron. 31 También algunos de los asiarcas, que eran amigos de Pablo, enviaron a él y repetidamente le rogaron que no se aventurara a presentarse en el teatro.

* O en alguna otra de las diversas oportunidades que menciona en 2 Cor 11:23-28:

23 ¿Son ministros de Cristo? (Como si estuviera loco hablo.) Yo más; en trabajos más abundante; en azotes sin número; en cárceles más; en peligros de muerte muchas veces. 24 De los judíos cinco veces he recibido cuarenta azotes menos uno. 25 Tres veces he sido azotado con varas; una vez apedreado; tres veces he padecido naufragio; una noche y un día he estado como náufrago en alta mar; 26 en caminos muchas veces; en peligros de ríos, peligros de ladrones, peligros de los de mi nación, peligros de los gentiles, peligros en la ciudad, peligros en el desierto, peligros en el mar, peligros entre falsos hermanos; 27 en trabajo y fatiga, en muchos desvelos, en hambre y sed, en muchos ayunos, en frío y en desnudez; 28 y además de otras cosas, lo que sobre mí se agolpa cada día, la preocupación por todas las iglesias.

* Incluso en 1 Cor 15:32 Pablo dice “Si por motivos humanos luché contra fieras en Efeso, ¿de qué me aprovecha? Si los muertos no resucitan, comamos y bebamos, que mañana moriremos.” que algunos interpretan como que fue puesto en un circo romano ante leones, pero que según Matthew Henry simboliza el enfrentamiento con los plateros de Efeso.

  
  
Personalmente considero que la tribulación de la que habla 2 Cor 1:8 fue el apedreamiento en Listra, pues efectivamente recibió una sentencia de muerte, que fue ejecutada, pero de la cual Dios milagrosamente lo libró o incluso lo resucitó, como se describe en el versículo 9, donde está el mensaje principal que Dios me ha dado para comenzar este 2023: “**que no confiásemos en nosotros mismos, sino en Dios que resucita a los muertos**;” en el Dios de la vida, único, suficiente y todopoderoso.

  
Respecto al versículo 10, Pablo recuerda como Dios lo libró en el pasado, en el presente (tal vez del tumulto de los plateros de Efeso que debió ocurrir cerca a la escritura de 2 Corintios) y confiaba en que volvería a librarlo en el futuro, como ocurrió por ejemplo en el naufragio del buque que lo llevaba a prisión en Roma descrito en Hechos 27 y que ocurrió aproximadamente en el año 57.

22 Pero ahora os exhorto a tener buen ánimo, pues no habrá ninguna pérdida de vida entre vosotros, sino solamente de la nave. 23 Porque esta noche ha estado conmigo el ángel del Dios de quien soy y a quien sirvo,

  
  
  
Recuerdo ahora, dos oportunidades en las que Dios me libró de la muerte:

1. Cuando era niño tal vez de 8 años y jugaba en un vagón de tren de un parque y no alcance a sostenerme de las barandas e iba cayendo con mi cabeza contra las escaleras de ladrillo construidas para subir al vagón. Sin mi voluntad mi pie se fue hacia adelante y tocó primero la escalera haciéndome rebotar y dar una vuelta en el aire para aterrizar a salvo en pasto que estaba más alla de las escaleras.
2. En Sierra Leona en 2003 cuando estábamos con mi ex-esposa e hija bajo el control de estafadores que nos engañaron haciéndose pasar por agencia de adopción, que nos robaron varios miles de dólares y que buscaban que nuestros familiares en Colombia nos enviaran más dinero para robarlo. Yo estaba enfermo, delirando por una medicina contra la malaria, haciendo locuras (incluso me tiré por un balcón e intentaba irme a no se donde) por lo que me mantenían atado y encerrado, pero reconocí a Jesús como Dios y a Él clamé y vino (escuche sus pasos), me desamarró, me puso sobre la cama y después nos libró de los estafadores mediante Fatu, una cristiana con quien nos habíamos saludado en el aeropuerto al llegar a Freetown.

Y tú, ¿qué oportunidades recuerdas en las que Jesús te libró de la muerte?

Las liberaciones de la muerte de Pablo, según indica en el versículo 11 se debieron a la oración de muchos, recordemos por ejemplo que el inició sus viajes misioneros fue enviado explícitamente por la iglesia de Antioquía como se describe en Hechos 13:1-3

**1** Había entonces en la iglesia que estaba en Antioquía, profetas y maestros: Bernabé, Simón el que se llamaba Niger, Lucio de Cirene, Manaén el que se había criado junto con Herodes el tetrarca, y Saulo. **2** Ministrando estos al Señor, y ayunando, dijo el Espíritu Santo: Apartadme a Bernabé y a Saulo para la obra a que los he llamado. **3** Entonces, habiendo ayunado y orado, les impusieron las manos y los despidieron.

Así que había una comunidad que los cubría y seguramente oraba por ellos, además de las diversas iglesias que Pablo fundó a lo largo de sus viajes.

Por eso oremos unos por otros continuamente, así como por nuestra pareja pastoral, tanto de manera individual como comunitaria durante las peticiones de oración en el servicio. Y cuando Dios responda a alguno, tod@s demos gracias al Dios de la vida.

Según el comentario de Matthew Henry en estos versículo 12 a 14 empiezan una defensa de Pablo frente a calumnias que los Corintios habían escuchado contra Pablo de parte de unos falsos profetas. Posiblemente sea introducción al capítulo 11 de la misma carta donde los versículos 13 a 15 dicen:

**13** Porque estos son falsos apóstoles, obreros fraudulentos, que se disfrazan como apóstoles de Cristo. 14 Y no es maravilla, porque el mismo Satanás se disfraza como ángel de luz. 15 Así que, no es extraño si también sus ministros se disfrazan como ministros de justicia; cuyo fin será conforme a sus obras.

Así que cuidémonos de lo que escuchamos como enseñanza respecto a Jesús, conozcamos la Palabra de Dios directamente y pidamos discernimiento mediante el Espíritu Santo para distinguir que enseñanzas son acordes y cuales no.

El versículo 12 nos da otro punto a tener en cuenta: dar buen testimonio comportandonos a conciencia.

El versículo 13 me recuerda que tenemos a nuestra disposición estos escritos de Pablo inspirados por Dios y en general la Palabra de Dios escrita en la Biblia, y que debemos aprovecharla y leerla a diario.

Finalmente el versículo 14 nos recuerda que al evangelizar e invitar a alguien a creer en Jesús tendremos gloria de parte de Dios por esa(s) persona(s). Oremos también por esas personas que hemos ayudado a evangelizar y por quienes, pues en todos Dios se glorifica.

# 

# 5. Conclusión y oración

Confiemos más en Dios que en nosotros y para eso a manera de resumen: **CONSULTE**.

| --- | --- |
| **C** | Consultemos a Dios antes de emprender algo e incluso consultemosle que emprender y hagamosle caso. |
| **O** | Oremos continuamente unos por otros, individual y comunitariamente. |
| **N** | No seamos sabios en la opinión de cada quien, sino que busquemos consejo de Dios. |
| **S** | Seamos agradecidos. Agradezcamos individual y comunitariamente cuando Dios nos responda oración. |
| **U** | Unción del Espíritu Santo. Pidamosla para que nos recuerde lo que nos ha enseñado y nos permita discernir otras enseñanzas que alguien pretenda darnos. |
| **L** | Leamos Su Palabra diariamente para escucharlo allí. |
| **T** | Testimonio, demos buen testimonio a conciencia según nos ha enseñado Jesucristo. |
| **E** | Evangelicemos. Compartamos de Cristo, oremos por las personas a las que les hemos compartido y oremos por quienes nos han compartido. |


Oremos:

* Señor por favor ayúdanos a confiar más en Ti que en nosotros mismos en el 
  2023 y consultarte todo lo que hagamos.
* Aprovechamos este momento para orar unos por otros por este año 2023, 
  para que nos guíes y libres de muerte, para que nos libres de peligros, 
  de malos pasos, de malos negocios, de tentaciones y nos des victoria en Tí. 
  De manera especial oramos por nuestra pareja pastoral Jaime Ramírez y 
  Mavi, para que les des salud, vida y amor para Ti, su familia y la obra en el 
  nombre de Jesús.

* Por favor recuerdanos leer tu Palabra a diario, llénanos con tu Santo 
  Espíritu en el nombre de Jesús para recordarnos tu Palabra, como interpretarla
  y para discernir cuando recibamos una enseñanza.
* Ayudanos a dar buen testimonio obrando a conciencia según nos has enseñado.
* Señor por favor respaldanos cuando compartamos tu Palabra y ayudanos a 
  hacerlo. Te agradecemos por quienes nos compartieron de Cristo y te pedimos 
  para que los mantengas y te rogamos por las personas a quienes les hemos 
  compartido de Cristo para que tu Palabra viva y crezca en ell@s, en el 
  nombre de Jesús.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. 
  [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* McCallum, Dennis. A Chronological Study of Paul's Ministry 
  [https://dwellcc.org/learning/essays/chronological-study-pauls-ministry](https://dwellcc.org/learning/essays/chronological-study-pauls-ministry "https://dwellcc.org/learning/essays/chronological-study-pauls-ministry") . 
  Consultado en Dic.2022
* Guzik, David. Comentario Bíblico. 
  [https://www.blueletterbible.org/Comm/guzik_david/spanish/StudyGuide_2Co/2Co_11.cfm](https://www.blueletterbible.org/Comm/guzik_david/spanish/StudyGuide_2Co/2Co_11.cfm "https://www.blueletterbible.org/Comm/guzik_david/spanish/StudyGuide_2Co/2Co_11.cfm")
* Davis, Craig. [http://www.datingthenewtestament.com/](http://www.datingthenewtestament.com/ "http://www.datingthenewtestament.com/"). Consultado Dic.2022.
* Henry, Matthew. 1706. Complete Commentary. 
  [https://www.biblestudytools.com/commentaries/matthew-henry-complete/](https://www.biblestudytools.com/commentaries/matthew-henry-complete/ "https://www.biblestudytools.com/commentaries/matthew-henry-complete/") Consulatado Dic. 2022

Imagen basada en [https://openclipart.org/detail/321461/advise-lightbulb](https://openclipart.org/detail/321461/advise-lightbulb "https://openclipart.org/detail/321461/advise-lightbulb")
