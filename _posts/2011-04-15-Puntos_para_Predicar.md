---
layout: post
title: "Puntos_para_Predicar"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

Esta predica tiene dos puntos centrales respecto a la predicación:
* El método del Señor es altisimamente práctico, pongamos en práctica, la medida será nuevas personas que reconozcan a Cristo como Rey y en la iglesia.
* Un camino para abrir la puerta a la predicación es el servicio.  Pues el servicio permite crear relaciones y a partir de una relación personal es más fácil establecer un diálogo sobre la fe.

Recordemos ese ciclo de creer por el oír, oír posiblemente de un predicador que se ha preparado, se ha preparado porque ha sido enviado, ha sido enviado porque cree y es obediente y cree porque oyó y creyó.   Y recordemos Romanos 10:9 'Que si confesares con tu boca que Jesús es el Señor, y creyeres en tu corazón que Dios lo levanto de los muertos, será salvo.'   ¿Qué hemos hecho las últimas semanas para que alguien conozca del Señor o para invitarl@ a la iglesia o para ayudarle a volver?

En Marcos 16:26 Jesús nos dice a todos los que creemos `Id por todo el mundo y predicad el evangelio a toda criatura.  El que creyere y fuere bautizado será salvo, más el que no creyere, será condenado.  Y estas señales seguirán a los que creen: en mi nombre echarán fuera demonios, hablarán nuevas lenguajes; tomaran en las manos serpientes y si bebieren cosa mortífera, no les hará daños, sobre los enfermos pondrán las manos y sanarán.'

La pregunta del como siempre tendrá más respuestas del Señor, porque Él es creativo, santo y ama a su pueblo y le muestra cada día nuevos amaneceres y nuevas formas de conocerlo. Hoy examinemos uno de los pasajes que describen como envío Jesús a 70 seguidores, esto ocurrió después de haber enviado a sus 12 con instrucciones similares y antes de enviarnos a todos los creyentes.

Los versículos que leeremos están en un contexto de enseñanza teórico-práctica por parte de Jesús  a los discípulos que envió:
* 8:4-8:21: Enseñanzas
* 8:26-8:56 Milagros: Calma tempestad, Libera, Sanidad, Resurrección
* 9:1-5 Envía 12 discípulos.
* 9:7-9 Hasta sus enemigos reconocen obras poderosas en Jesús
* 9:18-45 Más milagros que confirman que Jesús es el Señor
* 9:46-62 Enseñanzas
* 10:1-24 Envío de 70 y resultados
* 10:25-13:9 Enseñanzas

Es decir esos 70 habían podido escuchar directamente la predicación y enseñanza de Jesús, habían visto los milagros que realizaba, veían sus testimonio de amor diario y veían los frutos.  Entonces creían y se dispusieron para el servicio.


##2. Jesús da instrucciones a quienes envía. Lucas 10:1-12

<pre>
1 Después de estas cosas, designó el Señor también a otros setenta, a
quienes envió de dos en dos delante de él a toda ciudad y lugar adonde él
había de ir.
2 Y les decía: La mies a la verdad es mucha, mas los obreros pocos; por 
tanto, rogad al Señor de la mies que envíe obreros a su mies.
3 Id; he aquí yo os envío como corderos en medio de lobos.
4 No llevéis bolsa, ni alforja, ni calzado; y a nadie saludéis por el camino.
5 En cualquier casa donde entréis, primeramente decid: Paz sea a esta casa.
6 Y si hubiere allí algún hijo de paz, vuestra paz reposará sobre él; y si 
no, se volverá a vosotros.
7 Y posad en aquella misma casa, comiendo y bebiendo lo que os den; porque 
el obrero es digno de su salario. No os paséis de casa en casa.
8 En cualquier ciudad donde entréis, y os reciban, comed lo que os pongan
delante;
9 y sanad a los enfermos que en ella haya, y decidles: Se ha acercado a 
vosotros el reino de Dios.
10 Mas en cualquier ciudad donde entréis, y no os reciban, saliendo por sus 
calles, decid:
11 Aun el polvo de vuestra ciudad, que se ha pegado a nuestros pies, lo 
sacudimos contra vosotros. Pero esto sabed, que el reino de Dios se ha acercado a vosotros.
12 Y os digo que en aquel día será más tolerable el castigo para Sodoma, 
que para aquella ciudad.
</pre>
 
##3.  Puntos que extraemos

!3.1 Después de estas cosas, designó el Señor también a otros setenta, a quienes envió de dos en dos delante de él a toda ciudad y lugar adonde él había de ir.

En Lucas 9:1 vemos como había enviado antes a sus 12 discípulos --parece que de uno en uno-- pero agrega explicitamente el poder y autoridad que les dio y que nos da a quienes creemos: 
<pre>
   1 Habiendo reunido a sus doce discípulos, les dio poder y autoridad sobre 
   todos los demonios, y para sanar enfermedades.
</pre>

Además podemos ir confiados porque Él irá allí donde nos envía y Él hará la obra por intermedio nuestro, basta disponernos.

!3.2 Y les decía: La mies a la verdad es mucha, mas los obreros pocos; por tanto, rogad al Señor de la mies que envíe obreros a su mies.

Debemos orar constantemente.  Por esta petición particular sabemos que nos encontraremos con otros trabajadores de Él. Alegrémonos al encontrarlos, oremos por ellos y ellas y procuremos trabajar juntos.   En unos casos serán trabajadores de nuestra misma denominación menonita y en otros serán de otras denominaciones, pero en todos los casos lo importante y lo que nos permite discernir son sus frutos.

!3.3 Id; he aquí yo os envío como corderos en medio de lobos.

Requerimos entender el contexto del sitio donde prediquemos, saber que no es fácil, que puede resultar peligroso, medir hasta donde alcanzo con la guianza del Señor en oración.   Poner mis talentos y mente para buscar la manera de moverse y de avanzar en medio de esos lobos que el Señor quiere transformar en ovejas (y además  con nuestra ayuda predicando).

!3.4 No llevéis bolsa, ni alforja, ni calzado; y a nadie saludéis por el camino.

Personalmente me tomé este versículo y otros concordantes de manera muy apresurada, por ejemplo en Enero de 2004 llegue a estar prácticamente en ceros y sin trabajo por decisiones que fundamenté en el evangelio.  El Señor en su misericordia ha sostenido a mis hijas y a mi, y por su bondad me fue aumentando provisión de manera sorprendente año tras año para llegar a un tope, para poder compartir y darnos más estabilidad económica.  

El propósito del Señor con esta ordenanza lo da en Lucas 22:35:
<pre>
35 Y a ellos dijo: Cuando os envié sin bolsa, sin alforja, y sin calzado, ¿os faltó algo? Ellos dijeron: Nada. 
</pre>

Es decir Él quiere que dependamos de Él, que entendamos que Él es quien nos provee, que nos quitemos la preocupación del tener y nos concentremos en su Reino y Él añadirá lo demás.  Pongamos a disposición del Señor nuestro tiempo, nuestras habilidades, dones y recursos materiales completos y pidámosle en oración que nos muestre con que debemos ir en cada caso a predicar.

!3.5 En cualquier casa donde entréis, primeramente decid: Paz sea a esta casa. 6 Y si hubiere allí algún hijo de paz, vuestra paz reposará sobre él; y si no, se volverá a vosotros.

Deseemos con nuestros labios paz a cada sitio donde entremos.   Cuando el Señor nos de la oportunidad de solicitar posada, hagámoslo y deseemos paz a quien nos hospede o reciba.    Pero creo que podemos extender un poco, como una metodología muy menonita de predicar: desde el servicio.

La hospitalidad, bondad y servicio del otro:
# Búsquemos la hospitalidad y bondad de las personas.  Puede ser das hospedaje, pero también otros detalles más simples, que nos lleven una maleta en un bus, que nos den espacio para pasar, que nos den instrucciones sobre una dirección, etc.
# A quien sea hospitalario/bondadoso deseemosle paz y bendigamos.

Mi servicio.
# Siendo bondadoso, caballero/dama, hospitalori@.  
# Poniendo dones, habilidades y recursos para servir a otr@s.
# Deseemos paz y bendigamos a quienes servimos.

Es entonces el servicio bien de los otros o el mio hacía los demás un medio para relacionarme con otras personas (ver {3}), y de esa relación le pedimos al Señor que vaya abriendo espacio para poder predicarles del Señor Jesucristo a aquell@s con los que nos relacionamos.

!3.7 Y posad en aquella misma casa, comiendo y bebiendo lo que os den; porque el obrero es digno de su salario.  No os paséis de casa en casa.  8 En cualquier ciudad donde entréis, y os reciban, comed lo que os pongan delante;

No seamos gravosos con aquellos que nos dan hospitalidad, en lo que digamos, hagamos, pidamos o rechacemos ellos verán nuestro testimonio.    

Recibamos la hospitalidad que nos den y entendamos que es como "pago" por el servicio que hacemos a Dios.   Sin embargo no creamos que nos tienen que dar cada vez más "salario"  o que espiritualmente ascendemos juntamente con ascensos económicos.  El Señor pone límites cuando dice no pasar de casa en casa, sino quedarse donde nos reciben primero.   Si al ir de misión, me reciben en un sitio humilde y después me ofrecen en un sitio lujoso, no he de pasarme porque merezco más.  Podría hacer sentir menos al que me ofreció primero y a quien ya le recibí la hospitalidad, en cambio es más fácil decirle al que me está ofreciendo algo más lujoso que ya estoy ubicado en un sitio gracias a Dios.     Por otra parte si me recibieron en un sitio lujoso, pues bueno agradecer al Señor pues como decía Pablo "Sé vivir humildemene y sé tener abundancia; en todo y por todo estoy enseñado, así para estar saciado como para tener hambre, así para tener abundancia como para padecer necesidad"  Filipenses 4:12 {2}


!3.9 y sanad a los enfermos que en ella haya, y decidles: Se ha acercado a vosotros el reino de Dios.

Poner en práctica el poder y autoridad que el Señor nos ha dado y todo esto para decirles "se ha acercado a vosotros el Reino de Dios"

!3.10 Mas en cualquier ciudad donde entréis, y no os reciban, saliendo por sus calles, decid  11 Aun el polvo de vuestra ciudad, que se ha pegado a nuestros pies, lo sacudimos contra vosotros. Pero esto sabed, que el reino de Dios se ha acercado a vosotros. 12 Y os digo que en aquel día será más tolerable el castigo para Sodoma, que para aquella ciudad.

Aún donde no sean hospitalarios conmigo declaro "que el Reino de Dios se ha acercado a vosotros," la vergüenza no es para mí, por no haber sido recibido para predicar, sino para quienes no me hayan recibido.

##4. Conclusión

El método del Señor sigue estando vivo en nuestra comunidad hoy:
* Nos está enseñando
* Está haciendo milagros que podemos ver
* Nos está dando autoridad para liberar y sanar

Todo esto con el fin de que podamos decirle a la gente que Él y su Reino se han acercado.  Sólo para que la gente que no lo conoce crea y los que lo conocemos fortalezcamos nuestra fe. 

Un método que enfatizamos porque lo consideramos muy menonita es predicar desde el servicio, el servicio que permite crear relaciones, relaciones personales a partir de las cuales se puede hablar de la fe, se puede mostrar la fe propia e invitar al otro a poner su confianza en el Rey de Reyes y Señor de Señores.


Los resultados de nuestra predicación los podemos ver:
* Esperanza del Reino en nuevos corazones y salvación
* Nuestros nombre quedarán escritos en el libro de la vida como dice Luc 10:20 "Pero no os regocijéis de que los espíritus se os sujetan, sino regocijaos de que vuestros nombres están escritos en los cielos."
* Veremos la gloria de Dios cuando ocurran sanidades, cuando liberemos en el nombre del Señor. 
* Jesús se regocijará como dice Lucas 10:21 "1 En aquella misma hora Jesús se regocijó en el Espíritu, y dijo: Yo te alabo, oh Padre, Señor del cielo y de la tierra, porque escondiste estas cosas de los sabios y entendidos, y las has revelado a los niños. Sí, Padre, porque así te agradó."


##5. Biografía

* {1} Biblia. Reina Valera 1960. 
* {2} http://www.creeleadiosministerios.org/aprendiendo/vivirporfe.html
* {3} El Evangelio Encarnado. Elizabeth Soto Albrecth.  Julio 2010. 15 Asamblea Menonita en Asunción Paraguay. 
