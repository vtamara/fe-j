---
layout: post
title: "Bernabe"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Nombre

Bernabe


##Genealogía

| Parentezco | Persona | Referencia |

Levita nacido en Chipre.  Acompañante de los apóstoles.  De nombre José pero llamado Bernabe por Pablo según {2}. 

[http://gardenofpraise.com/images/acts14b.jpg]

Imagen de {3} donde también se encuentra un lección bíblica para niños sobre Bernabe.

##Referencias

* {1} Easton Bible Dictionary. 1897. http://www.ccel.org/ccel/easton/ebd2.html   Dominio Público.
* {2} Smit's Bible Dictionary. http://www.ccel.org/ccel/smith_w/bibledict.html?term=barnabas
* {3} http://gardenofpraise.com/bibl60s.htm
