---
layout: post
title: Espiritu Santo en nuestra vida
author: vtamara
categories:
- Prédica
image: "/assets/images/98-01.jpg"
tags: Prédica

---

El Espíritu Santo Acompañando nuestra vida: ¿Qué tengo que hacer yo?
1. Introducción

Personalmente doy testimonio de como siento la presencia del Espíritu Santo,
tanto en su cuidado continuo y de manera especial en una especie de
estremecimiento interior cuando le pido llenarme en el nombre de Jesús. Me
ocurre siempre y por eso también le doy gracias a Dios, pues tengo la certeza de
que siempre está conmigo.

También soy testimonio de la sanidad, pues me sanó de un trombo que tenía
