---
layout: post
title: Alabanza_Levantate_y_resplandece
author: vtamara
categories:
- Alabanza
image: assets/images/home.jpg
tags: 

---
Marcos Barrientos
<pre>
Abre tus puertas por que tu amanecer llego 
tu sol hoy brilla, tu luz nunca se apagara 
soy tu salvador soy tu redentor quien te da seguridad 
eres fuerte en mi 

Un sonido nunca oído un estruendo celestial 

Levantate y Resplandece hoy 
con mi luz mi poder te cubrira 
Resplandece en las naciones 
sin temor brillaras levantate 

Levantate
</pre>

* Letra basada en http://www.musica.com/letras.asp?letra=1027545
* Video: https://www.youtube.com/watch?v=rRurvm-a4_E