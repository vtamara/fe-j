---
layout: post
title: "El_Poderoso_de_Israel"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Juan Carlos Alvarado

Y los ojos de los ciegos se abrirán  y ellos verán %%%
Los oídos de los sordos oirán %%%
El cojo saltará, con el arpa danzará %%%
La lengua de los mudos, cantará %%%

__CORO__ %%%
//Él es el poderoso de Israel, %%%
el poderoso de Israel %%%
Su voz se oirá nadie lo detendrá %%%
Al poderoso de Israel// %%%

Y de noche cantaremos celebrando su poder %%%
con alegría de corazón %%%
como el que va con la flauta al monte de Jehová
celebraremos su poder %%%

__CORO__ 

Y los ojos de los ciegos se abrirán  y ellos verán %%%
Los oídos de los sordos oirán %%%
El cojo saltará, con el arpa danzará %%%
La lengua de los mudos, cantará %%%

__CORO__ 

----

* Video: http://www.youtube.com/watch?v=JROF12jPECE
