---
layout: post
title: "Al_Final"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Lilly Goodman

<pre>
0:17 Yo he visto el dolor, acercarse a mi
0:21 causarme heridas, golpearme así
0:23 y  hasta llegué a preguntarme
0:26 ¿donde estabas tu?

0:31 He hecho preguntas en mi aflicción
0:34 buscando respuestas sin contestación
0:37 y hasta dude por instantes 
0:40 de tu compasión

0:44 Y aprendí
0:46 que en la vida todo tiene un sentido
0:51 y descrubí 
0:53 que todo obra para bien

CORO
0:58 Y que al final será 
1:01 mucho mejor lo que vendrá
1:05 es parte de un propósito
1:09 y todo bien saldrá
1:11 siempre has estado aquí
1:15 tu palabra no ha fallado
1:19 y nunca me has dejado
1:22 descansa mi confianza sobre ti

1:33 Yo he estado entre la espada y la pared
1:36 rodeada de insomnios sin saber que hacer
1:40 pidiendo a gritos 
1:42 tu interverción
1:46 a veces me hablaste de una vez
1:50 en otras tu silencio sólo escuché
1:53 que interesante 
1:56 tu forma de responder

1:59 Y aprendi que lo que pasa bajo el cielo
2:06 conoces tu que todo tiene una razón

2:13 CORO

3:02 Ohhhh

3:08 CORO
3:42 sobre ti...
3:49 ahhohhh
</pre>

----

Tiempo acorde con:

* Video: http://www.youtube.com/watch?v=8ZfOqa40WbY
* Karaoke: http://www.youtube.com/watch?v=5foCl_xbfVk&feature=related
