---
layout: post
categories:
- Predica
title: Cuidemos la salvación. Efesios 5:1-20
author: vtamara
image: "/assets/images/Jesus_lava_pies-bronce-texas.jpg"

---

# Cuidemos la salvación. Efesios 5:1-20

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 2024-02-07


## 1. Introducción

El Señor me ha enfatizado el pasaje de hoy por mi perdida de tiempo en redes sociales e incluso estar cerca de ser tentado por contenidos sexuales.  Predicó comenzando para mí y orando para que sea de bendición para muchos.


## 2. Texto: Efesios 5:1-20

<sup>1</sup> Sed, pues, imitadores de Dios como hijos amados. <sup>2</sup> Y andad en amor, como también Cristo nos amó, y se entregó a sí mismo por nosotros, ofrenda y sacrificio a Dios en olor fragante.

<sup>3</sup> Pero fornicación y toda inmundicia, o avaricia, ni aun se nombre entre vosotros, como conviene a santos; <sup>4</sup> ni palabras deshonestas, ni necedades, ni truhanerías, que no convienen, sino antes bien acciones de gracias. <sup>5</sup> Porque sabéis esto, que ningún fornicario, o inmundo, o avaro, que es idólatra, tiene herencia en el reino de Cristo y de Dios. <sup>6</sup> Nadie os engañe con palabras vanas, porque por estas cosas viene la ira de Dios sobre los hijos de desobediencia. <sup>7</sup> No seáis, pues, partícipes con ellos. <sup>8</sup> Porque en otro tiempo erais tinieblas, mas ahora sois luz en el Señor; andad como hijos de luz <sup>9</sup> (porque el fruto del Espíritu es en toda bondad, justicia y verdad), <sup>10</sup> comprobando lo que es agradable al Señor. <sup>11</sup> Y no participéis en las obras infructuosas de las tinieblas, sino más bien reprendedlas; <sup>12</sup> porque vergonzoso es aun hablar de lo que ellos hacen en secreto. <sup>13</sup> Mas todas las cosas, cuando son puestas en evidencia por la luz, son hechas manifiestas; porque la luz es lo que manifiesta todo. <sup>14</sup> Por lo cual dice:

Despiértate, tú que duermes,

Y levántate de los muertos,

Y te alumbrará Cristo.

<sup>15</sup> Mirad, pues, con diligencia cómo andéis, no como necios sino como sabios, <sup>16</sup> aprovechando bien el tiempo, porque los días son malos. <sup>17</sup> Por tanto, no seáis insensatos, sino entendidos de cuál sea la voluntad del Señor. <sup>18</sup> No os embriaguéis con vino, en lo cual hay disolución; antes bien sed llenos del Espíritu, <sup>19</sup> hablando entre vosotros con salmos, con himnos y cánticos espirituales, cantando y alabando al Señor en vuestros corazones; <sup>20</sup> dando siempre gracias por todo al Dios y Padre, en el nombre de nuestro Señor Jesucristo.


## 3. Contexto

Recordemos (especialmente de {2}) que:



* La carta a los Efesios fue escrita por Pablo hacía el año 62 mientras era prisionero en Roma. La dirigió a los miembros de la iglesia de Éfeso, con enseñanzas prácticas para cristianos maduros.
* Éfeso es un puerto sobre el mediterraneo donde hoy es Turquía. En el tiempo de Pablo era un puerto romano comercialmente muy importante y había un templo a la falsa-diosa Artemisa así como una comunidad judía pues Pablo predicó en la sinagoga.
* Pablo fundó la iglesia de Éfeso durante su segundo viaje misionero hacia el año 51 en compañía de Priscila y Aquila, se quedó allí 2 años predicando y enseñando a diario. Tuvo que salir por amenazas de algunos orfebres cuyo emprendimiento de hacer piezas con Artemisa estaba cayéndose por la efectiva predicación de Pablo.



![Fachada de la biblioteca de Celso](/assets/images/celso.jpg)

Librería de Celso en Efeso [https://es.wikipedia.org/wiki/%C3%89feso#/media/Archivo:Ephesus_Celsus_Library_Fa%C3%A7ade.jpg](https://es.wikipedia.org/wiki/%C3%89feso#/media/Archivo:Ephesus_Celsus_Library_Fa%C3%A7ade.jpg) 



Como contexto de la lectura de hoy veamos los pasajes que le preceden y que le siguen:

<table>
  <tr>
   <td><strong>Versículos</strong>
   </td>
   <td><strong>Título del pasaje (en RVR1960)</strong>
   </td>
   <td><strong>A quien se dirige</strong>
   </td>
  </tr>
  <tr>
   <td>4:1-16
   </td>
   <td>La unidad del Espíritu
   </td>
   <td>Creyentes
   </td>
  </tr>
  <tr>
   <td>4:17-32
   </td>
   <td>La nueva vida en Cristo
   </td>
   <td>Creyentes
   </td>
  </tr>
  <tr>
   <td>5:1-20
   </td>
   <td>Andad como hijos de luz
   </td>
   <td>Creyentes
   </td>
  </tr>
  <tr>
   <td>5:21-33
   </td>
   <td>Someteos los unos a los otros
   </td>
   <td>Parejas
   </td>
  </tr>
</table>


Tanto el pasaje anterior 4:17-32 como el que hoy estudiamos 5:1-20 son similares en cuanto a que instan a cristianos maduros a no devolverse a caminos gentiles.


## 4. Análisis

Podemos hacer una tabla con lo que los cristianos si deben y no deben hacer:

<table>
  <tr>
   <td><strong>Si hacer</strong>
   </td>
   <td><strong>No hacer</strong>
   </td>
  </tr>
  <tr>
   <td>1 Imitadores de Dios como hijos amados
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>2 Andar en amor como Cristo (se entregó por mi)
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>3 No fornicar, ni andar en inmundicia o avaricia
   </td>
  </tr>
  <tr>
   <td>4 Acciones de gracias 

    No simplemente dar gracias sino hacer acciones
   </td>
   <td>4 No decir palabras deshonestas, necedades, ni truhanerías
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>5 No tiene herencia en el Reino de Dios.
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>6 No ser engañados por palabras vanas

      No ser hijos de desobediencia.
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>7 No participes con ellos. 

Son palabras dirigidas a cristianos maduros, la salvación no es por obras, pero aquí nos recuerda que debemos cuidarla.
   </td>
  </tr>
  <tr>
   <td>8 Luz en el Señor, andar como hijos de luz
   </td>
   <td> 
   </td>
  </tr>
  <tr>
   <td>9 Fruto del espíritu: bondad, justicia y verdad
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>10 Comprobar lo agradable a Dios
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>11 Reprender obras de las tinieblas
   </td>
   <td>11 No participes en obras de las tinieblas
   </td>
  </tr>
  <tr>
   <td>12 Hasta vergonzoso hablar de lo que hacen
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>13 La luz revela todo
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>14 Despiértate, tú que duermes,
<p> Y levántate de los muertos,</p>
<p> Y te alumbrará Cristo. </p>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>15 Andar con diligencia como sabios
   </td>
   <td>15 No andar como necios
   </td>
  </tr>
  <tr>
   <td>16 Aprovechar el tiempo, porque los días son malos
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>17 Entendidos de la voluntad de Dios
   </td>
   <td>17 No ser insensatos
   </td>
  </tr>
  <tr>
   <td>18 Llenos del Espíritu
   </td>
   <td>18 No embriagarse
   </td>
  </tr>
  <tr>
   <td>19 Hablar entre nosotros con salmos, himnos y cánticos espirituales, alabar de corazón.
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>20 Dar gracias a Dios y Padre en el nombre de nuestro Señor Jesucristo
   </td>
   <td>
   </td>
  </tr>
</table>



## 5. Conclusión y oración

Señor gracias por recordarnos cuan puro eres, y que no cambias.  Lo que requieres de nosotros ha sido siempre santidad gracias a Tu amor.

Señor perdónanos cuando hemos hecho lo que no debíamos y sabiéndolo como cristianos maduros: perder el tiempo, embriagarse, ser insensatos, andar como necios, dormirnos y no alumbrar con Cristo, dejarnos engañar de falsas palabras, mentir, hacer chanzas inapropiadas, ser avaros, andar en inmundicias o fornicación.

Reprendemos las obras de las tinieblas en el nombre de Jesús.

Señor transfórmanos para que seamos luz con Cristo, con Tu Santo Espíritu que demos frutos de justicia, verdad y bondad, que empleemos nuestro tiempo en oración, alabarte con cantos, en compartir tu palabra, orar, ayunar, leer tu palabra, en ser más como Tu Cristo.

Pedimos en tu nombre poderoso Jesús.


## 6. Bibliografía

* {1} Biblia, Reina Valera 1960.
* {2} Támara, Vladimir. 2016. Conocer amor de Dios. [https://fe.pasosdejesus.org/Conocer_amor_de_Dios/](https://fe.pasosdejesus.org/Conocer_amor_de_Dios/) 
* {3} Viajes misioneros de Pablo. [https://storymap.knightlab.com/edit/?id=segundo-viaje-misionero-de-pablo](https://storymap.knightlab.com/edit/?id=segundo-viaje-misionero-de-pablo) 

    [https://storymap.knightlab.com/edit/?id=tercer-viaje-misionero-de-pablo](https://storymap.knightlab.com/edit/?id=tercer-viaje-misionero-de-pablo) 

-----

La imagen de portada es de 
<https://picryl.com/media/bronze-statue-of-jesus-washing-the-disciple-peters-feet-in-witness-park-outside> 
donde dice:

* "'Divine Servant' por Max Greiner, 1990."
* "Estatua de bronce de Jesús lavando los pies del discípulo Pedro.
Está ubicada en Witness Parkd fuera de la 'Torre de la Oración'" en Pittsburg,
Texas.

Agradezco al pastor Jaime la oportunidad de compartir en estudio bíblico de
la Iglesia Menonita de Suba el miércoles 7 de Febrero de 2024.
