---
layout: post
title: "Busqueda_de_las_Minas_de_Salomon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
PBS y Nova presentaron el 23 de noviembre un documental sobre las posibles minas del Rey Salomon:

http://www.pbs.org/wgbh/nova/ancient/quest-solomons-mines.html

Presentan la controversia respecto a Salomón y David entre arqueologos, dejando mejor ubicados a los que opinan que David y Salomón si existieron como reyes de una nación organizada con grandes ciudades y estructura política y económica.
