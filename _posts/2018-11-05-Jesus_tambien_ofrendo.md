---
layout: post
title: Jesús también ofrendó
author: vtamara
categories:
- Prédica
image: "/assets/images/cruzcol.jpg"
tags: Prédica

---
# Jesús también ofrendó

Iglesia Menonita de Suba. 4.Nov.2018. vtamara@pasosdeJesus.org

## 1. Introducción

En la prédicación de hace 15 días Jaime trató entre otros Filipenses 4:19 “Mi Dios, pues, suplirá todo lo que os falta conforme á sus riquezas en gloria en Cristo Jesús” recordando la importancia de ofrendar con alegría. Hace 8 días trató, entre otros, Lucas 8:1-3 “... y otras muchas que le servían de sus bienes.” para explicar que una forma de adorar es con bienes y que adorar con mis bienes es un principio de sabiduría. Explicó que cuando no hay bienes, como con jóvenes, se puede adorar con tiempo. y que es importante dar con alegría. Hoy continuaremos con el tema de dar diezmos y ofrendas en el templo con el ejempo supremo, el de Jesús.

## 2. Texto: Mateo 17:24-27

24 Cuando llegaron a Capernaum, vinieron a Pedro los que cobraban las dos dracmas, y le dijeron: ¿Vuestro Maestro no paga las dos dracmas?

25 El dijo: Sí. Y al entrar él en casa, Jesús le habló primero, diciendo: ¿Qué te parece, Simón? Los reyes de la tierra, ¿de quiénes cobran los tributos o los impuestos? ¿De sus hijos, o de los extraños?

26 Pedro le respondió: De los extraños. Jesús le dijo: Luego los hijos están exentos.

27 Sin embargo, para no ofenderles, ve al mar, y echa el anzuelo, y el primer pez que saques, tómalo, y al abrirle la boca, hallarás un estatero; tómalo, y dáselo por mí y por ti.

## 3. Contexto

Este pasaje cierra el capítulo 17, los textos anteriores y posteriores son (títulos de \[RV1960\]):

* 17:1-13 La transfiguración
* 17:14-21 Jesús sana a un muchacho lunático
* 17:22-23 Jesús anuncia otra vez su muerte
* 17:24-27 Pago del impuesto del templo
* 18:1-5 Quien es el mayor
* 18:6-9 Ocasiones de caer
* 18:10-14 Parábola de la oveja perdida
* 18:15-22 Cómo perdonar al hermano

Así que ocurre en medio de enseñanzas de Jesús, que típicamente se intercalaban con milagros pero después de la transfiguración que a su vez es posterior al reconocimiento por parte de Pedro de que Jesús es el Mesía, el Hijo de Dios Dios.

El contexto para los judios y el cobro que le estaban haciendo los líderes religiosos a Pedro puede verse en Exodo 30:11-16 en medio de los instructivos de Dios a Moisés para construir y mantener el tabernáculo —que era como el templo antes de que se construyera el primero en la epoca de Salomón.:

11 Habló también Jehová a Moisés, diciendo:

12 Cuando tomes el número de los hijos de Israel conforme a la cuenta de ellos, cada uno dará a Jehová el rescate de su persona, cuando los cuentes, para que no haya en ellos mortandad cuando los hayas contado.

13 Esto dará todo aquel que sea contado; medio siclo, conforme al siclo del santuario. El siclo es de veinte geras. La mitad de un siclo será la ofrenda a Jehová.

14 Todo el que sea contado, de veinte años arriba, dará la ofrenda a Jehová.

15 Ni el rico aumentará, ni el pobre disminuirá del medio siclo, cuando dieren la ofrenda a Jehová para hacer expiación por vuestras personas.

16 Y tomarás de los hijos de Israel el dinero de las expiaciones, y lo darás para el servicio del tabernáculo de reunión; y será por memorial a los hijos de Israel delante de Jehová, para hacer expiación por vuestras personas.

Es decir en el tiempo de Jesús, era una ordenanza de más de 1300 años para el mantenimiento del templo, que cada Judio debía dar medio siclo anualmente.

Respecto a etimología, algunas palabras en griego usadas en el nuevo testamento para dar en el templo son:

| --- | --- | --- | --- |
| Griego | Español | FrecEvangelios | Ejemplo de versículos en los evangelios que lo usan |
| τελέω teleo | Pagar | 12 | Mateo 17:24 |
| ἀποδεκατόω apodekatou | Diezmo | 3 | Mateo 23:23, Lucas 11:42, Lucas 18:11-12 Mateo 23:23!!Ay de vosotros, escribas y fariseos, hipócritas! porque diezmáis la menta y el eneldo y el comino, y dejáis lo más importante de la ley: la justicia, la misericordia y la fe. Esto era necesario hacer, sin dejar de hacer aquello. |
| βάλλω ballo | Hechar(Dar) | 77 | Mateo 27:6, Marcos 12:41, 12:43Marcos 12:43 Entonces llamando a sus discípulos, les dijo: De cierto os digo que esta viuda pobre echó más que todos los que han echado en el arca; |
| δίδωμι dedomi | Dar | 206 | Mat 17:27Mar 4:11 Dios da interpretación de parábolas a discípulos.Mar 6:2 Dios da sabiduría a Jesús (y a quien le pida)Mar 6:7 Jesús da autoridad sobre espíritus inpurosMar 8:6 Jesus dió gracias, partió siete panes y dio a 5000Mar 10:21 Dar a pobres es hacer tesoro en el cieloMar 11:28 Dios da autoridad Mar 13:11 El Espíritu Santo dará lo que se ha de decirMar 14:22-23 Jesús dió pan y vino simbolizando su cuerpo y sangre |
| ἀποδίδωμιapodidomi | Entregar | 26 | Mat 22:21 Le dijeron: De César. Y les dijo: Dad, pues, a César lo que es de César, y a Dios lo que es de Dios. |

Algunas monedas empleadas en el templo de Jesús y su valor equivalente al denario se presentan a continuación (basado en \[SM\]):

| --- | --- | --- | --- |
| Nombre de la moneda | Equivalencia en denarios | Comentario |  |
| Shékel o SicloSiclo de TiroEstatero de Atenas | 4 | Equivalía más o menos a una semana de jornales.El siclo de Tiro era acuñado por los judios (con más porcentaje de plata y permiso del imperio). |  |
| Medio siclo | 2 | Lo que debía pagar anualmente una persona para el templo, 2 jornales de trabajo |  |
| DenarioDracma | 1 | 1 jornal de trabajo. |  |
| Leptón | 1/32 | La viuda que no tenía mucho dinero pero dió más que los ricos, hecho en el cofre del templo dos de estas monedas (Mr 12:43) |  |

## 4. Análisis

### 4.1 Versículo 24

_Cuando llegaron a Capernaum, vinieron a Pedro los que cobraban las dos dracmas, y le dijeron: ¿Vuestro Maestro no paga las dos dracmas?_

Según el comentarista bíblico Matthew Henry, los encargados del templo mostraron modestia al preguntarle a Pedro y no directamente a Jesús, pues era él y su familia quienes vivían en Capernaum, y seguramente Jesús solía quedarse en casa de Pedro, así que era natural que pagara la ofrenda anual para el templo en esa ciudad.

### 4.2 Versículo 25

_El dijo: Sí. Y al entrar él en casa, Jesús le habló primero, diciendo: ¿Qué te parece, Simón? Los reyes de la tierra, ¿de quiénes cobran los tributos o los impuestos? ¿De sus hijos, o de los extraños?_

Pedro pudo contestar “Si” porque conocía a Jesús y sabía que no era rebelde sino que por el contrario cumplió toda la ley y toda justicia, como dicen Mat 3:15 “_Pero Jesús le respondió: Deja ahora, porque así conviene que cumplamos toda justicia. Entonces le dejó._”.

Al adelantarse con su pregunta, cuando Pedro llega a su casa, Jesús confirma su omnisciencia.

### 4.3 Versíulo 26

_Pedro le respondió: De los extraños. Jesús le dijo: Luego los hijos están exentos._

Pedro entendía que Jesús era el Hijo de Dios y realmente estaba exento.

Pero Jesús pagó aunque realmente estaba exento por (1) ser hijo de Dios y (2) porque era un tributo por expiación y Jesús no tenía pecados por expiar.

### 4.4 Versículo 27

_Sin embargo, para no ofenderles, ve al mar, y echa el anzuelo, y el primer pez que saques, tómalo, y al abrirle la boca, hallarás un estatero; tómalo, y dáselo por mí y por ti._

Él no reclamó su derecho para estar exento por no ofender a las autoridades del templo (y aún cuando la mayoría parecía en contra de Jesús). En ocasiones los cristianos no debemos exigir nuestros derechos para no ofender a otros —claro cada caso con la guianza de Dios. Pero no podemos ignorar nuestras obligaciones.

Por lo visto en este versículo y en el resto de los evangelios, Él no tenía dinero, pues “se hizo pobre por causa de nosotros para que por Su pobreza fuesemos ricos” (2 Corintios 8:9). Posiblemente hubiera podido pedirle a Judas que tomara de la bolsa de limosnas que tenían, pero era una bolsa común y ese tributo era personal. Su provición llegó de manera sobrenatural, por Su omniciencia y omnipotencia.

Personalmente veo que hoy sigue dandome de manera sobrenatural, creo y veo por esfuerzo y anhelo de ofrendar en la iglesia (para el templo y quienes sirven en el templo). Creo que eso es extensivo para tods.

Pero no olvidemos que ofrendar en la iglesia, por ejempo y como sugiere la Biblia la decima parte, es necesario para los cristianos, pero no es todo. Cristo nos recuerda que son necesarias la justicia, la misericordia y la fe. Además ayer en devocional repasabamos que la obediencia a Dios es más importante que las ofrendas en 1 Samuel 15:22

_22 Y Samuel dijo: ¿Se complace Jehová tanto en los holocaustos y víctimas, como en que se obedezca a las palabras de Jehová? Ciertamente el obedecer es mejor que los sacrificios, y el prestar atención que la grosura de los carneros._

# 5. Conclusiones y oración

Aunque Cristo estaba exento del tributo por expiación al templo por ser el Hijo de Dios y por no haber cometido pecado, Él pago para no ofender a los líderes, (y aún cuando muchas no eran buenos líderes como Él lo denunció) y para darnos ejemplo de obediencia. Además no pago lo de Él y lo de Pedro de la bolsa común con la que se sotenía la comunidad que formaron con los discípulos, sino de otra provisión que Dios les dió.

Pagar diezmos y ofrendas en la iglesia es necesario para el sostenimiento de quienes sirven, para el mantenimiento/construcción del templo y para que la obra continúe y se expanda, pero no basta, pues Dios requiere de cada uno de nosotros justicia, misericordia, fe y obediencia a Él.

El misterio de dar es demasiado profundo, pero una conclusión la encuentro en Marcos 10:45:

_Porque el Hijo del Hombre no vino para ser servido, sino para servir, y para dar su vida en rescate por muchos._

El Señor Jesús no solo ofrendo en el templo, sino que se ofrendó por nosotros. Es el mismo que se agradaba del esfuerzo de la viuda por dar de lo que tenía y el mismo que hoy se agrada cuando nosotros damos, pero sin olvidar la justicia, la misericordia, la fe y la obediencia a Él.

Velemos porque los fondos se usen bien en la iglesia, y demos con alegría y confiados, pues ese Jesús que dió alimento y pagó ofrenda al templo de formas sobrenaturales, es el mismo que hoy está con nosotros y que cumple sus promesas.

Señor tu nos amas y expresas Tu amor, entre otras formas, dando, Señor que aprendamos a amar y dar como Tu nos amas y nos das. Señor Tu te agradas cuando aprendemos y amamos y damos, y nos esforzamos a dar como la viuda. Señor Tu nos has mandado a amar y a dar entre otros a nuestros enemigos, a los pobres y a quienes sirven en el templo y para el templo, que te obedezcamos bien y entonces podamos pedirte confiados y recibir más de ti de acuerdo a Tus promesas: Fe, interpretación de Tu palabra, sabiduría, autoridad sobre espíritus impuros, para multiplicar panes, dones, tesoros en el cielo, provisión sobreabundante en esta tierra, presencia y palabra de Tu Santo Espíritu, tu perdón por el cuerpo y sangre de nuestro Señor Jesucristo.

Si alguien quiere conocer más al Señor Jesús y recibir su promesa mayor de perdón, reconciliación y vida eterna, le invitamos a hacer una oración de fe.

# 6. Referencias

* \[RV1960\] Biblia. Reina Valera 1960.
* \[BLB\] Blue Letter Bible. www.blb.org
* \[SM\] Jan Herka. Sistema monetario y coste de vida en el tiempo de Jesús. [https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/.](https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/. "https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/.") 2009. Revisado en Nov.2018
* \[MH\] Matthew Henry’s Commentary. [https://www.biblegateway.com/passage/?search=Mateo+17&version=RVR1960](https://www.biblegateway.com/passage/?search=Mateo+17&version=RVR1960 "https://www.biblegateway.com/passage/?search=Mateo+17&version=RVR1960")

[Imágenes de siclo y medio siclo tomadas de ](_ftnref1)[https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/](https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/ "https://buscandoajesus.wordpress.com/articulos/sistema-monetario-y-coste-de-la-vida-en-tiempos-de-jesus/")

[Posible denario con imagen del emperador Tiberio tomado de ](_ftnref2)[https://www.todocoleccion.net/monedas-imperio-romano/raro-denario-tiberio-14-37-d-c-denario-tributo-jesus-jesucristo-peso-34-gramos\~x105987178](https://www.todocoleccion.net/monedas-imperio-romano/raro-denario-tiberio-14-37-d-c-denario-tributo-jesus-jesucristo-peso-34-gramos\~x105987178 "https://www.todocoleccion.net/monedas-imperio-romano/raro-denario-tiberio-14-37-d-c-denario-tributo-jesus-jesucristo-peso-34-gramos~x105987178")

[Leptón tomado de ](_ftnref3)[https://wol.jw.org/en/wol/mp/r1/lp-e/nwt/2013/142](https://wol.jw.org/en/wol/mp/r1/lp-e/nwt/2013/142 "https://wol.jw.org/en/wol/mp/r1/lp-e/nwt/2013/142")

Compartido en onlyoffice: [https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=5908122&doc=cW9Kd0RFaHZRVk1VaHBZa2x1MDcwUHQzMkNpbDIydzYrYVJmRG1ZU0U0MD0_IjU5MDgxMjIi0](https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=5908122&doc=cW9Kd0RFaHZRVk1VaHBZa2x1MDcwUHQzMkNpbDIydzYrYVJmRG1ZU0U0MD0_IjU5MDgxMjIi0 "https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=5908122&doc=cW9Kd0RFaHZRVk1VaHBZa2x1MDcwUHQzMkNpbDIydzYrYVJmRG1ZU0U0MD0_IjU5MDgxMjIi0")

***               

Dibujo de [https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html](https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html "https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html")