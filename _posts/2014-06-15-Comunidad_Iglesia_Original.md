---
layout: post
title: Comunidad Iglesia Original
author: vtamara
categories:
- Prédica
image: "/assets/images/195898.png"
tags: 

---
# 1. INTRODUCCIÓN

Alrededor de los temas "Comunidad de fe" y de "Pobreza/riqueza en la iglesia", el Señor me ha dado varios versículos que me han cuestionado en la forma de vida y que me retan de manera personal.   Comparto a continuación insistiendo que también predico para mí.


# 2. TEXTO Y CONTEXTO

Hechos 2:43
<pre>
43 Y sobrevino temor a toda persona; y muchas maravillas y señales eran
   hechas por los apóstoles.
44 Todos los que habían creído estaban juntos, y tenían en común todas las
   cosas;
45 y vendían sus propiedades y sus bienes, y lo repartían a todos según la 
   necesidad de cada uno.
46 Y perseverando unánimes cada día en el templo, y partiendo el pan en las 
   casas, comían juntos con alegría y sencillez de corazón,
47 alabando a Dios, y teniendo favor con todo el pueblo. Y el Señor añadía 
   cada día a la iglesia los que habían de ser salvos.
</pre>

## 2.1 Contexto Anterior
* Hechos 1:1-11 Jesús resucitado enseña a sus discípulos, promete Espíritu Santo y asciende al cielo.
* Hechos 1:12-26 Elección de otro apóstol que remplazara a Judas
* Hechos 2:1-13 Pentecostes --por cierto celebrado el domingo anterior-- Jesús cumplió su promesa y envió Espíritu Santo que les hizo hablar en nuevas lenguas.
* Hechos 2:14-42 Describe el Discurso de Pedro tras el cual se unieron 3000 creyentes nuevos, dice "42  Y perseveraban en la doctrina de los apóstoles, en la comunión unos con otros, en el partimiento del pan y en las oraciones."

## 2.2 Contexto Posterior
* Hechos 3:1-10 Curación de un cojo de nacimiento por medio de Pedro y Juan
* Hechos 3:11-26 Pedro habla en el pórtico de Salomón "19 Así que, arrepentíos y convertíos, para que sean borrados vuestros pecados; para que vengan de la presencia del Señor tiempos de refrigerio."

Para ubicarlo en espacio y tiempo, esto ocurre hacía el año 30,  en Jerusalén que entonces era colonia romana. Roma compartía autoridad para algunos aspectos civiles y religiosos con las autoridades judías.  Igual que ahora la gente tenía necesidades y como sustentaré más adelante, los miembros de esta iglesia eran débiles en los estándares del mundo y pobres. 

En ese momento esas autoridades Judías daban a Jesús por muerto e imaginaban que las torturas y muerte que le infligieron esparcirían a sus seguidores, pero no fue así:

Hechos 1: 12-15a
<pre>
12 Entonces volvieron a Jerusalén desde el monte que se llama del Olivar, el cual está cerca de Jerusalén, camino de un día de reposo.[a]
13 Y entrados, subieron al aposento alto, donde moraban Pedro y Jacobo, Juan, Andrés, Felipe, Tomás, Bartolomé, Mateo, Jacobo hijo de Alfeo, Simón el Zelote y Judas hermano de Jacobo.
14 Todos éstos perseveraban unánimes en oración y ruego, con las mujeres, y con María la madre de Jesús, y con sus hermanos.
15 En aquellos días Pedro se levantó en medio de los hermanos (y los reunidos eran como ciento veinte en número), ...
</pre>

Con este grupo comenzó la iglesia original con la llegada del Espíritu Santo unos 10 días después de la Ascensión, la cual según Lucas --quien escribió Hechos-- ocurrió  40 días después de la muerte y posterior Resurrección de Jesús --actualmente celebramos Pentecostés 50 días después de la Resurrección {3}.


# 3. ANÁLISIS

Pregunta de reflexión: Del texto y el contexto ¿qué características ve en esa naciente iglesia y que podemos aplicar para la nuestra hoy?

Deseo destacar 7 características de esa iglesia original (3 tomadas de {3}) orando para que podamos aplicarlas a nuestra comunidad:
1. **Perseveraban** en doctrina, comunión, partimiento del pan, oración y ruego
2. La **unidad** de la  iglesia la dio el Espíritu Santo 
3. El Espíritu Santo les dio **poder** 
4. El Espíritu Santo les dio **valor** 
5. El Espíritu Santo les dio **conocimiento**
6. Muchos de los miembros de la iglesia eran **materialmente pobres**.
7. Guiados por el Espíritu **compartieron** lo que tenían

## 3.1 Perseverar

Hechos 2:42:  
<pre>
Y perseveraban en la doctrina de los apóstoles, en la comunión unos con otros, en el partimiento del pan y en las oraciones.
</pre>

Perseverar en la comunión unos con otros es difícil, incluso los 12 apóstoles antes de recibir el Espíritu Santo, convivieron juntos en un aposento alto como dice Hechos 1:13:
<pre>
"Y entrados, subieron al aposento alto, donde moraban Pedro y Jacobo, Juan, Andrés, Felipe, Tomás, Bartolomé, Mateo, Jacobo hijo de Alfeo, Simón el Zelote y Judas hermano de Jacobo."
</pre>

Con seguridad tenían problemas de convivencia pues además de que algunos tenían familia (como Pedro), lo natural es que en un grupo haya conflictos porque gracias a Dios todos somos diferentes, pero no se separaban ante sus diferencias sino perseveraban y encontraban acuerdos que les permitían convivir.  Creo que:
* No hablaban mal unos de otros, pues  Santiago 3:5 dice "De una misma boca proceden bendición y maldición. Hermanos míos, esto no debe ser así" y Levítico 19:16 "No andarás chismeando entre tu pueblo."
* No se tenían envidia pues el 10 mandamiento mencionado en Deuteronomio 5:21 dice: "No codiciarás la mujer de tu prójimo, ni desearás la casa de tu prójimo, ni su tierra, ni su siervo, ni su sierva, ni su buey, ni su asno, ni cosa alguna de tu prójimo."
* En caso de conflicto hacían lo que Jesús mandó en Mateo 18:15-35  1- hablar directamente con la persona que ofende, si no basta para que se arrepienta, 2- hablar con uno o dos para discernir entre varios, si no hay arrepentimiento, 3- decírselo a la iglesia y que para la iglesia sea como un pagano; y 4- claro está perdonaban de corazón.


## 3.2 Unidad dada por el Espíritu

En {2} se dan varias pistas para lograr unidad en la familia, deseo mencionarlas aquí para que extrapolemos lo posible con ayuda de Dios al caso de la comunidad de fe:
* Tener una misión conjunta
* Reír tanto como podamos (sin burlarnos del otro sino siendo espontáneos y divertidos)
* Comer juntos que en buena parte es resaltado en Hechos 2:46-47:
<pre>
46 Y perseverando unánimes cada día en el templo, y partiendo el pan en las 
   casas, comían juntos con alegría y sencillez de corazón,
47 alabando a Dios, y teniendo favor con todo el pueblo. Y el Señor añadía 
   cada día a la iglesia los que habían de ser salvos.
</pre>
* Cada uno ame a Dios sobre todo y seamos fieles a Él.  En Lucas 7:36-50 Jesús habla de una mujer que lo amó mucho y por eso le perdonó sus muchos pecados.  La mujer que llevó un jarro de alabastro (muy costoso según Mateo 26:7) con perfume, y se humilló lavando los pies del Señor con sus lagrimas, secándolos con su cabello, besándolos y ungiéndolos con el perfume. Claro se humilló pero poder tocar y besar los pies del Señor es LA FELICIDAD.  Esta mujer dio sus cosas materiales para el cuerpo de Jesús, se humillo en público y ante Él, y besaba sus pies.  El cuerpo del Señor que tenemos hoy es su iglesia, así que besemos a sus miembros y demonos lo mejor que tenemos de nuestros bienes materiales.  También el Señor dijo que las personas en necesidad son Él (Mateo 25:35-35), así que demos a Jesús dando a quienes tienen necesidad.


## 3.3 Poder dado por el Espíritu

Hechos 2:43 
<pre>
Y sobrevino temor a toda persona; y muchas maravillas y señales eran
hechas por los apóstoles.
</pre>

Un caso detallado se presenta en Hechos 3:6-8, cuando Dios por intermedio de Pedro y Juan sanaron al hombre de 40 años que era cojo de nacimiento:
<pre>
6 Mas Pedro dijo: No tengo plata ni oro, pero lo que tengo te doy; en el 
  nombre de Jesucristo de Nazaret, levántate y anda.
7 Y tomándole por la mano derecha le levantó; y al momento se le afirmaron 
  los pies y tobillos;
8 y saltando, se puso en pie y anduvo; y entró con ellos en el templo, 
  andando, y saltando, y alabando a Dios.
</pre>

Los dones que el Señor ha dado a cada uno son para usarlos en la comunidad.  Gracias a Dios en esta iglesia esto se ve en las sanidades, liberaciones y en el poder del Espíritu que se mueve entre otros a través del pastor Jaime Ramírez y que se evidencia en las reuniones.

## 3.4 Valor dado por el Espíritu

O cuando Pedro y Juan tras la sanidad del cojo fueron arrestados por las autoridades Judías por predicar a Cristo y ante la orden que les daban de no predicarlo más les contestaron (Hechos 4:19b):
<pre>
Juzgad si es justo delante de Dios obedecer a vosotros antes que a Dios;
</pre>


## 3.5 Conocimiento dado por el Espíritu

Apenas recibieron el Espíritu Santo el día de Pentecostes empezaron a hablar en lenguas humanas pero que no conocía antes, hablaban las maravillas de Dios.  El Señor en situaciones extremas como esa da conocimiento inmediato, pero nosotros que tenemos el tiempo, preparémonos y el Espíritu Santo nos ayudará a entender y aplicar lo aprendido.


## 3.6 Iglesia con muchos pobres pero trabajadores y diligentes

Al respecto del trabajo de los miembros de la iglesia el apostol Pablo es muy claro en 2 Tesalonicenses 3:8-11
<pre>
8 ni comimos de balde el pan de nadie, sino que trabajamos con afán y fatiga
  día y noche, para no ser gravosos a ninguno de vosotros;
9 no porque no tuviésemos derecho, sino por daros nosotros mismos un ejemplo
  para que nos imitaseis.
10 Porque también cuando estábamos con vosotros, os ordenábamos esto: Si 
  alguno no quiere trabajar, tampoco coma.
11 Porque oímos que algunos de entre vosotros andan desordenadamente, no 
  trabajando en nada, sino entremetiéndose en lo ajeno.
12 A los tales mandamos y exhortamos por nuestro Señor Jesucristo, que 
  trabajando sosegadamente, coman su propio pan.
</pre>

Como Pablo, el pastor Jaime Ramírez nos ha dado ejemplo de trabajar por su cuenta para no serle gravoso a ninguno de nosotros, sin embargo reconocemos sus calidades para el trabajo eclesial y que la iglesia requiere más (y cada vez más) de su tiempo, por lo que lógicamente la iglesia debería llegar un día a pagarle un salario completo.

Nosotros como miembros entonces debemos esforzarnos en nuestro trabajo para poder dar mayores diezmos y ofrendas. Si nos preparamos y un día el Señor nos requiere para servir en la iglesia, como María Macana y otras personas hacen ahora, él nos retribuirá desde la iglesia misma.

Ahora que la iglesia original era principalmente conformada por personas pobres vamos a detallarlo.
Comenzando por decir que el Señor no nos llama a la misería ni a la ruina, que debemos esforzarnos para cubrir bien nuestras necesidades básicas que incluyen: nuestra relación con Él, salud, educación, vivienda, alimentación y recreación.  También aclaramos que la pobreza no da virtud, se puede ser pecador y rico, pero también pecador pobre como muchos casos que conocemos de ambas situaciones.  También añadiendo como dice {5} que en juicio debemos ser imparciales sin favorecer a los pobres como lo expresa Éxodo 23:2-3 (versión Dios Habla Hoy):
<pre>
No sigas a la mayoría en su maldad. Cuando hagas declaraciones en un caso 
legal, no te dejes llevar por la  mayoría, inclinándote por lo que no es 
justo; 3 pero tampoco favorezcas indebidamente las demandas del pobre.
</pre>
O Levítico 19:15:
<pre>
No harás injusticia en el juicio, ni favoreciendo al pobre ni complaciendo 
al grande; con justicia juzgarás a tu prójimo.
</pre>


### 3.6.1 El Señor Jesús fue materialmente pobre

Lo dice explicito en 2 Corintios 8:9 
<pre>
Porque ya conocéis la gracia de nuestro Señor Jesucristo, que por amor a 
vosotros se hizo pobre, siendo rico, para que vosotros con su pobreza fueseis 
enriquecidos.
</pre>
Pero como esto tiene una parte de metáfora, veamos detalles de su vida:

* En su nacimiento que tuvo que ser en un establo (Lucas 2:7)
* También recién nacido como dice Lucas 2:22-24, José y María cumplieron la ley de Moisés para purificación de María con 2 tórtolas que era lo reglamentario para personas pobres y no para personas con más dinero que debían ofrecer un cordero, Lev 12:8
<pre>
6 Cuando los días de su purificación fueren cumplidos, por hijo o por hija, 
  traerá un cordero de un año para holocausto, y un palomino o una tórtola 
  para expiación, a la puerta del tabernáculo de reunión, al sacerdote;
7 y él los ofrecerá delante de Jehová, y hará expiación por ella, y será 
  limpia del flujo de su sangre. Esta es la ley para la que diere a luz hijo 
  o hija.
8 Y si no tiene lo suficiente para un cordero, tomará entonces dos tórtolas 
  o dos palominos, uno para holocausto y otro para expiación; y el sacerdote 
  hará expiación por ella, y será limpia.
</pre>
* Antes de su ministerio público vivió humildemente con un oficio manual como el de su padre: carpintero,  como lo reconocían los habitantes de su pueblo en Marcos 6:3:
<pre>
¿No es éste el carpintero, hijo de María, hermano de Jacobo, de José, de Judas y de Simón? ¿No están también aquí con nosotros sus hermanas? Y se escandalizaban de él.
</pre>
* Durante su ministerio no tenía una casa donde recostar la cabeza, sino que iba de un sitio a otro (Mat 8:20), seguramente viviendo humildemente y quedándose como les enseñó a sus discípulos --en la primera casa que los recibieran.  Marcos 6:8-10
<pre>
8 Les ordenó que no llevaran nada para el camino, fuera de un bastón: ni pan,
  ni morral, ni dinero; 
9 que llevaran calzado corriente y un solo manto. 
10 Y les decía: &laquo;Quédense en la primera casa en que les den 
  alojamiento, hasta que se vayan de ese sitio. 
</pre>
* No se describe una fuente de ingresos --ni siquiera carpintería-- durante su ministerio de 24 hora (pues solía orar en lugar de dormir), excepto que (1) algunas personas le daba "ofrendas" como describe Lucas 8:1-3 y (2) en un caso excepcional indicó a Pedro sacar una moneda del primer pez que pescara para pagar el impuesto del templo de Pedro y de Él (Mat 17:24-27).
<pre>
1 Aconteció después, que Jesús iba por todas las ciudades y aldeas, 
  predicando y anunciando el evangelio del reino de Dios, y los doce con él,
2 y algunas mujeres que habían sido sanadas de espíritus malos y de 
  enfermedades: María, que se llamaba Magdalena, de la que habían salido 
  siete demonios,
3 Juana, mujer de Chuza intendente de Herodes, y Susana, y otras muchas que 
  le servían de sus bienes.
</pre>


### 3.6.2 Escogió personas pobres como apóstoles.
Sabemos que 4 eran pescadores (Mato 4:18-21) y que Mateo era recolector de impuestos pero que dejó todo para seguirlo (). 

### 3.6.3 Tras la muerte y resurrección de Jesús continuaron los discípulos continuaron siendo pobres.
Que los 12 apóstoles vivieran en un aposento alto es indicio. 

### 3.6.4 La iglesia continuo siendo conformada principalmente por pobres diligentes en los primero siglos
Además de continuar leyendo hechos, podemos revisar lo que los no creyentes de la iglesia en sus albores decían de la misma (como hace {6} en excelente detalle). Por ejemplo Celso  que era un filosofo griego del Siglo II quien escribió contra los cristianos, como lo cita Origenes en el 3er libro Contra Celso capítulo 55 dijo que eran:
"trabajadores de la lana y del cuero, y lavadores y personas del carácter más rústico y sin instrucción."


Así que en lugar de enriquecernos debemos compartir, es un imperativo por ejemplo examinando Juan:
<pre>
16 En esto hemos conocido el amor, en que él puso su vida por nosotros; también nosotros debemos poner nuestras vidas por los hermanos.
17 Pero el que tiene bienes de este mundo y ve a su hermano tener necesidad, y cierra contra él su corazón, ¿cómo mora el amor de Dios en él?
18 Hijitos míos, no amemos de palabra ni de lengua, sino de hecho y en verdad.
</pre>


## 3.7 Decidieron compartir

<pre>
44 Todos los que habían creído estaban juntos, y tenían en común todas las
   cosas;
45 y vendían sus propiedades y sus bienes, y lo repartían a todos según la 
   necesidad de cada uno.
</pre>


Y no sólo aquí se relata esta parte que es difícil, pero posible en la medida que reconocemos que Dios ha escogido a los débiles, rechazados y pobres --pero diligentes-- para conformar su iglesia.  Así que trabajemos con esfuerzo pero no para hacernos ricos, dejemos el egoísmo y compartamos con la iglesia y los necesitados.

Hechos 4:
<pre>
32 Y la multitud de los que habían creído era de un corazón y un alma; y 
   ninguno decía ser suyo propio nada de lo que poseía, sino que tenían todas 
   las cosas en común.
33 Y con gran poder los apóstoles daban testimonio de la resurrección del 
   Señor Jesús, y abundante gracia era sobre todos ellos.
34 Así que no había entre ellos ningún necesitado; porque todos los que 
   poseían heredades o casas, las vendían, y traían el precio de lo vendido,
35 y lo ponían a los pies de los apóstoles; y se repartía a cada uno según su 
   necesidad.
36 Entonces José, a quien los apóstoles pusieron por sobrenombre Bernabé (que 
   traducido es, Hijo de consolación), levita, natural de Chipre,
37 como tenía una heredad, la vendió y trajo el precio y lo puso a los pies 
   de los apóstoles.
</pre>

Esta iglesia original se construyó así, y el compromiso de quienes la conformaron fue completo, Dios así lo exigió como se revela con Ananias y Safira:

Hechos 5:1-
<pre>
1  Pero cierto hombre llamado Ananías, con Safira su mujer, vendió una 
   heredad,
2 y sustrajo del precio, sabiéndolo también su mujer; y trayendo sólo una 
  parte, la puso a los pies de los apóstoles.
3 Y dijo Pedro: Ananías, ¿por qué llenó Satanás tu corazón para que mintieses 
  al Espíritu Santo, y sustrajeses del precio de la heredad?
4 Reteniéndola, ¿no se te quedaba a ti? y vendida, ¿no estaba en tu poder?
  ¿Por qué pusiste esto en tu corazón? No has mentido a los hombres, sino a 
  Dios.
5 Al oír Ananías estas palabras, cayó y expiró. Y vino un gran temor sobre 
  todos los que lo oyeron.
6 Y levantándose los jóvenes, lo envolvieron, y sacándolo, lo sepultaron.
7 Pasado un lapso como de tres horas, sucedió que entró su mujer, no sabiendo 
  lo que había acontecido.
8 Entonces Pedro le dijo: Dime, ¿vendisteis en tanto la heredad? Y ella dijo: 
  Sí, en tanto.
9 Y Pedro le dijo: ¿Por qué convinisteis en tentar al Espíritu del Señor? He 
  aquí a la puerta los pies de los que han sepultado a tu marido, y te 
  sacarán a ti.
10 Al instante ella cayó a los pies de él, y expiró; y cuando entraron los 
   jóvenes, la hallaron muerta; y la sacaron, y la sepultaron junto a su 
   marido.
11 Y vino gran temor sobre toda la iglesia, y sobre todos los que oyeron 
   estas cosas.
</pre>



# 4. CONCLUSIÓN Y ORACIÓN

* Señor ayúdanos a vencer el egoísmo, Señor que cada uno compartamos lo que tenemos con la iglesia y los necesitados. Que lo hagamos con compromiso y sinceridad.
* Te agradecemos Señor porque has conformado Tu iglesia con los simples, débiles y viles como nosotros.
<pre>
26 Pues mirad, hermanos, vuestra vocación, que no sois muchos sabios según la 
   carne, ni muchos poderosos, ni muchos nobles;
27 sino que lo necio del mundo escogió Dios, para avergonzar a los sabios; y 
   lo débil del mundo escogió Dios, para avergonzar a lo fuerte;
28 y lo vil del mundo y lo menospreciado escogió Dios, y lo que no es, para 
   deshacer lo que es,
29 a fin de que nadie se jacte en su presencia.
</pre>
Señor que no nos creamos ni anhelemos ser poderosos, ni ricos, ni sabios en el mudo.  Ayúdanos a ser humildes, y por favor ayúdanos a vencer la pereza, ayúdanos a ser diligentes 
para poder compartir más y mejor.
* Señor queremos conocerte, enséñanos más de Ti, prepáranos para servirte bien.
* Señor con tu Espíritu Santo danos valor para predicar y vivir lo que predicamos.
* Señor danos poder, poder para dar testimonios con milagros de que Tu eres nuestro Rey.  Que ejerzamos bien nuestros dones.
* Señor danos unidad mediante tu Santo Espíritu, unidad alrededor de Ti Jesús.
* Señor que seamos perseverantes en oración, en comer juntos, en compartir, en asistir al templo, en alabarte.
* Pedimos todo esto en el nombre de nuestro Señor Jesucristo, quien es nuestro rey y salvador --si Jesús aún no es tu rey y salvador te invitamos a hacer una [Oracion de fe].




# 5. REFERENCIAS

* {1} <http://www.biblegateway.com/passage/?search=acts+2&version=RVR1960>
* {2} Building Family ties with Faith, Love & Laughter. Dave Stone. 2012. Editorial Thomas Nelson.
* {3} <http://es.wikipedia.org/wiki/Pentecost%C3%A9s>
* {5} <http://en.wikipedia.org/wiki/Option_for_the_poor>
* {6} <http://servicioskoinonia.org/relat/274.htm>
* {7} <http://www.newadvent.org/fathers/04163.htm>



----
Gracias a Dios se predico el 15.Jun.2014 en la Iglesia Menonita de Suba.

Dominio público. 2014. <vtamara@pasosdeJesus.org>

Dibujo de dominio público de <https://openclipart.org/detail/195898/choir-singing>