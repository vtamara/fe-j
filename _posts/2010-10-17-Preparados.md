---
layout: post
title: "Preparados"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

¿Estamos preparados para lo importante?

[http://thebiblerevival.com/clipart/math%2025%20-%201%20the%20tale%20of%20the%2010%20virgins.jpg]
(Ver {6})

!1.1 Adivinanza

Entre otras cosas para las cuales debemos prepararnos, adivinemos una que  se describe en diversos pasajes de los evangelios, fuera de parábolas, así:
# Desafía el tiempo, pues en unas parábolas se menciona en tiempo pasado (Mt 13:24), en otras en presente (en Mt 13:31) y en otras en futuro (Mt 25:1, Mr 9:1) --tanto en la traducción que empleamos (ver {1}) como en manuscrito griego en el que se basa (ver {2}).  --En griego &#8041;&#956;&#959;&#953;&#8061;&#952;&#951; Aoristo, pasivo, indicativo por ejemplo en 13:24, &#8009;&#956;&#959;&#8055;&#945; es adjetivo en Mat 13:31 acompañando verbo "es" en presente, es &#8001;&#956;&#959;&#953;&#8057;&#969; futuro en Mt 25:1
# No es de este mundo (Jn 18:36)
# Fue preparado desde la fundación del mundo para sus moradores (Mt 25:34), y es grato para el Padre entregarlo (Lc 12:32)
# Estaba entre los escribas que interrogaban a Jesús (Lc 17:20-21)
# Es algo que algunos esperaban como José de Arimatea (Mr 15:43)
# Sus misterios les son dados a los discípulos de Jesucristo (Lc 8:10)
# Es algo a lo que la gente se puede acercar o alejar (Mr 12:34).  Pero además puede venir a la gente (Lc 11:20)
# Fue predicado por Jesús  y para predicarlo fue enviado (Mr 1:14, Lc 4:43)
# Debemos buscarlo y lo demás se nos dará por añadidura (Lc 12:31)
# Le pertenece a los niños (Mr 10:14), le pertenece a los pobres (Lc 6:20) , le pertenece a los que son perseguidos por causa de la justicia (Mt 5:10)
# Se puede entrar o llegar, allí están Abraham y los profetas (Lc 13:28), llegaron de todos puntos cardinales allí (Lc 13:29).  Sin embargo la entrada es reservada. Entre las condiciones de ingreso están:
** Hay que recibirlo como niño (Lc 18:17)
** Hay que nacer en agua y espíritu (Juan 3:5)
** Difícilmente los ricos entrarán (Mr 10:23)
# Vendrá (Lc 22:18) y Jesús estará allí (Lc 22:30) pues no beberá vino con sus discípulos sino hasta cuando llegue allí.
# Su predicación típicamente ha ido acompañada de sanidades (Lc 9:2)

!1.2 Solución

 En los evangelios lo llaman Reino de los Cielos (Mt 25:1-13), Reino de Dios (Mr. 1:15) y Jesús también lo llama el Reino de mi Padre (Mt 26:29).  Debemos prepararnos para ese Reino como enseña la parábola central que estudiaremos.

!1.3 Oración por sanidad

Señor que tu Reino se manifieste en nuestra sanidad física. Imponemos nuestras manos para sanidad como discípulos de Cristo. Ordenamos a la enfermedad que salga de los cuerpos.


## 2. Preparados para el Reino de Dios

!2.1 Parábola de las 10 vírgenes 

Mateo 25:1-12

<pre>
1 Entonces el reino de los cielos será semejante a diez vírgenes que 
  tomando sus lámparas, salieron a recibir al esposo.
2 Cinco de ellas eran prudentes y cinco insensatas.
3 Las insensatas, tomando sus lámparas, no tomaron consigo aceite;
4 mas las prudentes tomaron aceite en sus vasijas, juntamente con sus
  lámparas.
5 Y tardándose el esposo, cabecearon todas y se durmieron.
6 Y a la medianoche se oyó un clamor: ##Aquí viene el esposo; salid a
  recibirle!
7 Entonces todas aquellas vírgenes se levantaron, y arreglaron sus 
  lámparas.
8 Y las insensatas dijeron a las prudentes: Dadnos de vuestro aceite; 
  porque nuestras lámparas se apagan.
9 Mas las prudentes respondieron diciendo: Para que no nos falte a 
  nosotras y a vosotras, id más bien a los que venden, y comprad para
  vosotras mismas.
10 Pero mientras ellas iban a comprar, vino el esposo; y las que 
   estaban preparadas entraron con él a las bodas; y se cerró la puerta.
11 Después vinieron también las otras vírgenes, diciendo: ##Señor, 
   señor, ábrenos!
12 Mas él, respondiendo, dijo: De cierto os digo, que no os conozco.
13 Velad, pues, porque no sabéis el día ni la hora en que el Hijo 
   del Hombre ha de venir.
</pre>

Una parábola busca dar un mensaje haciendo analogía con una historia diferente, requiriendo interpretación de la historia para entender el mensaje y fundamentar fe.  

Como lo indica ella misma, esta parábola busca explicar algo sobre el Reino de Dios, aunque no es la única sobre este tema, veamos:

| __Pasaje__ | __Resumen__ |
|Mt 13:24-30 | Es como hombre que siembra buena semilla de trigo, pero el enemigo siembra cizaña que posteriormente durante la cosecha debe ser separada y quemada.|
|Mt 13:31-32; Mr 4:30-32; Lc 13:18-19 | Es como una semilla de mostaza que de pequeña crece hasta ser árbol donde anidan aves. |
|Mr 4:26-29| Es como un hombre que siembra una semilla que crece aunque el sembrador no sabe como |
|Mt 13:33; Lc 13:20-21 | Es como levadura que se esconde en masa y la leuda toda |
|Mt 13:44 | Es como tesoro escondido en un campo por el cual se vende todo para comprarlo |
|Mt 13:45 | Es como mercader que halla perla preciosa por la que vende todo |
|Mt 13:47 | Es como red que recoge muchos peces que deben ser escogidos | 
|Mt 20:1-16 | Es como empleador que llamo trabajadores a su viña, unos primero que a otros, pero todos disfrutaron su pago justo |
|Mt 22:1-14 | Es como rey que organiza una fiesta de boda a la que fueron invitados unos, pero por indignos otros tuvieron que ser invitados |
|Mt 25:14-30 | Es como hombre que da  a sus 3 siervos talentos para que los hagan producir, 2 si lo hacen y uno no, el cual  es echado a tinieblas. |
|Mt 25:1-13 | Será como esta historia de 10 vírgenes, 5 de las cuales fueron prudentes o previsivas y se prepararon, mientras que otras 5 no lo fueron. |

!2.2 Interpretación

Algunos teólogos consideran que el Reino de Dios es algo en esta tierra, que se busca con justicia social (aunque el Señor dijo que no era de este mundo).  Es necesaria la justicia social para cumplir lo ordenado por nuestro Creador (Mat 25:31-46) y acercarnos a ese Reino en nuestro diario vivir, pero personalmente creo que es más que eso, gracias al Señor, creo, que Él me permitió sentir Su Reino en una visión en Marzo de 2001, cuando me volví a sentir como niño, maravillado en un "mundo" hiper-perfecto, hiper-real.  

Pero, veamos interpretaciones registradas en Mateo hechas por el mismo Señor Jesús:
| __Parábola__ | __Interpretación__ | __Resumen Interpretación__ |
|Cizaña y Trigo. Mt 13:24-30 | Mt 13:36-43 | El sembrador es Jesús mismo, el campo es el mundo, la buena semilla son hijos del Reino, la cizaña son hijos del malo, el enemigo es el diablo, la siega es fin del siglo, los segadores son ángeles, 41-43 `Enviará el hijo del hombre a sus ángeles y recogerán de su reino a todos los que sirven de tropiezo, y a los que hacen iniquidad, y los echarán en el horno de fuego; allí será el lloro y el crujir de dientes. Entonces los justos resplandecerán como el sol en el reino de su Padre. El que tiene oídos para oír, oiga.'|
| Peces que se seleccionan. Mt 13:47-48 | Mt 13:49-50 | `Así será al fin del siglo: saldrán los ángeles, y apartarán a los malos de entre los justos. Y los echarán en el horno de fuego; allí será el lloro y el crujir de dientes.' |

Entonces de lo que hemos repasado, podemos decir que se trata del __Reino de Jesús.__ Como dice el crucificado con Je&#347;us en Lc 23:42 "Y dijo a Jesús: Acuerdate de mí cuando vengas en tu reino." __Un Reino donde Él es Rey, que fue, es, será, que ya vino y que volverá__

Como la entrada es reservada, y habrá juicio, cabe preguntarnos ¿Cómo puedo entrar? 

Con respecto al aceite, en pasajes del antiguo testamento el aceite es usado por el Señor para sostener (por ejemplo la vasija de aceite que no mermaba de la viuda que compartió con Elías y la vasija cuyo aceite se multiplicó por intermediación de Eliseo para sustentar a otra viuda) ---por cierto recientemente en la iglesia Menonita de Teusaquillo escuché un testimonio de generación de aceite en manos de personas durante servicios de sanidad (en Internet se encuentran videos sobre este tema  y aunque no puedo garantizar que sean fidedignos, se que el Señor obra milagros cuando Él lo desea). 

Luis y su esposa buenos amigos, con quienes oramos,  interpretan como otros hermanos de fe el aceite como unción del Espíritu Santo.  Algunas sugerencias de {6} para mantener la unción:
* Debes, constantemente, permitir que el aceite fluya sobre ti, refrescando tu vida espiritual. Esto se hace por medio de la oración, una íntima comunión con Dios (hablo de comunión personal, no de ir a un templo una o dos veces por semana) y la lectura de la Palabra de Dios.
* Entonces, atención: no permitas que los huecos de la amargura, el resentimiento, la lástima propia (que es egolatría), y otras cosas semejantes se infiltren en tu vida. Porque es allí, entonces, donde el precioso aceite del Espíritu se saldrá y te sentirás vacío de adentro, que es el peor de los lugares para sentirse vacíos.
* Además de la oración y el estudio bíblico (que son indispensables) tú necesitas oír a hombres y mujeres de Dios. Además de tu congregación local y el mensaje de quien allí predique, (O, incluso, pese a ella), es importante contar con la palabra de otros siervos ungidos en diferentes escalas: apóstoles, profetas, evangelistas, pastores, maestros
* Sigue hacia delante deja que el aceite fresco de la unción sacerdotal (la segunda), sea derramada sobre ti diariamente, trayéndote a la comunión y a la íntima relación con el Espíritu Santo. Pasa tiempo en su presencia y permítele llenarte de Sí mismo y con Su poder. Entonces es cuando te moverás a un lugar más alto y entrarás en la unción regia, la máxima, y el imbatible poder sobre Satanás que la acompaña. 
* El consejo resumido del principio básico de este pasaje, es: guarda cuidadosamente la unción. Recuerda, no puedes operar dependiendo de glorias pasadas, tratando de sobrevivir con el aceite de ayer. Las reservas de Dios nunca se agotan. Así que, no te dejes poner rancio, ni te complazcas conmigo mismo. 
* Dios siempre está mirando para ver si vos estás guardando lo que ya tienes
* Antes de darte más, Dios mira para ver qué has hecho con lo que ya te ha dado.
* tus asociaciones son importantes. Asóciate con personas ungidas, porque la unción de ellos se "pegará" a tu vida. Ellos te influenciarán, y esto producirá maravillosos efectos.


Las viudas son interpretadas en {5} como la iglesia, las viudas preparadas como la buena iglesia, mientras que las que no se prepararon, como una iglesia sin fe o con fe y prácticas erradas.

Vivamos a Jesús como Rey, es decir seamos sus siervos y buenos trabajadores. Ahora no lo vemos, más por su Sano Espíritu lo sentimos, vivámoslo, anunciemoslo y un día por su gracia veremos al Rey junto con otros que ayudemos a verlo como Rey ahora. La celebración ese día solo la podemos imaginar como la mejor fiesta.

Al vivenciar ese Reino en nuestro diario vivir como dice {4} "Y no lo hacemos solos. Jesus va adelante y su Espíritu nos acompaña, nos da aliento, y nos confirma que vamos por buen camino."


!3. Conclusiones

!3.1 Para mi vida

Observando a las vírgenes de la parábola podemos aprender más sobre lo que debemos hacer para vivir a Jesús como Rey y estar en su Reino.

* Seamos puros y esperándolo en pureza como hacían las vírgenes.  Es puro quien es sincero, quien se esfuerza por obedecerlo como consecuencia de amarlo sobre todo y de amar a los demás como a sí mismo (Mt 22:34-40).  Pidiendo perdón continuamente para ser purificados, recibiendo perdón por gracia, porque no es por ley sino por Su gracia que resultamos limpíos.
* No basta la pureza, no basta pedirle al Señor perdón y esperarlo, hubo 5 vírgenes que no entraron, sólo 5 si que se habían preparado para entrar, incluso debo prepararme para entrar a esa fiesta.
* No es cualquier preparación, las 5 vírgenes que no entraron si habían preparado sus lámparas, y fueron a esperar, pero les falto prepararse más. Les faltó preveer, ser precavidas, pensar más y mejor lo que vendría o tal vez comunicarse con las otras vírgenes para caer en cuenta que posiblemente requerirían aceite extra y que tenían que consegurilo con anticipación.
* Renovar la unción del Espíritu Santo diariamente con oración, lectura de su palabra, adoración.  No quedarse en simples tradiciones, buscarlo con amor y de corazón. Buscar todo espacio donde pueda aumentarse esa unción: vigilias, servicios. 
* ¿Cual es el propósito de Dios en mi vida?  ¿Ya puse mi trabajo, estrategías y esfuerzos para ese propósito?  ¿Estoy dedicando tiempo periodico para evaluar? ¿Estoy tratando de visualizarme en ese propósito? ¿Estoy viendo que necesitaré y evaluando que ya tengo y que me falta?
* Ese propósito ¿incluye predicar el Reino y vivenciarlo?  ¿Cómo me estoy preparando para predicarlo y como lo estoy predicando? ¿Cómo me preparo para vivirlo y como lo vivo?


!3.2 Ampliando interpretación a familia

Del Señor aprendemos como ser mejores papás/mamás

* Como padres/madres de familia, ¿estamos siendo precavidos con nuestros hijos, estamos preparándolos para ese Reino?  
* La entrada es reservada. ¿Enseñamos esto a nuestros hijos?  ¿Les ponemos límites?
* En ocasiones nuestro amor es asfixiante y no alcanza a proyectarse en perspectiva de Inmortalidad.  ¿El amor que les estoy dando a mis hijos los está preparando para esa inmortalidad con el Señor, donde obviamente yo también lucho por estar?
* Hay mamás/papás que por amor a sus hijos alcahuetean pereza y actitudes nocivas o sin límites, como también hay papás que desean que sus hijos hagan lo que ellos piensan que es mejor.  Sin embargo Dios es soberano y es Él quien da vida, pone talentos y pone propósitos en la vida de cada quien, propósitos que inicialmente Él conoce y que va revelando a la persona (no necesariamente a los papás, pero rogémosle para entender).  Dios no quiere el pecado, como papás ayudemos a que nuestros hijos no anden en pecado, con nuestro ejemplo primero.
* ¿Estoy enseñando a mi hijo con ejemplo a buscar y renovar diariamente la unción del Espíritu Santo?

##REFERENCIAS

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/passage/?search=Mateo%2025&version=RVR1960
* {2} Blue Letter Bible. http://www.blueletterbible.org/Bible.cfm?b=Mat&c=25&v=1&t=KJV#conc/1
* {3} http://thebiblerevival.com/clipart/math%2025%20-%201%20the%20tale%20of%20the%2010%20virgins.jpg
* {4} http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-3oct2010.shtml
* {5}   Chuck Smith. Matthew 25-26 (C2000 Series). http://www.blueletterbible.org/commentaries/comm_view.cfm?AuthorID=1&contentID=7128&commInfo=25&topic=Matthew&ar=Mat_25_1
* {6} El Aceite de la Unción. http://www.tiempodevictoria.com.ar/estudios/crecimiento/291
