---
layout: post
title: Oración de Fe
author: vtamara
categories:
- Oración
image: assets/images/home.jpg
tags: featured

---
Señor Jesús en Ti confio, reconozco que soy pecador, te pido perdón por mis pecados, te pido que por favor me limpies.

Reconozco que moriste por mi en la cruz del calvario,
para justificarme ante el Padre y que resucitaste.

Abro mi corazón para que entres, te acepto como mi
único y suficiente Salvador, como mi Rey y Señor.

Por favor escribe mi nombre en el libro de la vida y
no lo borres de allí.

***

Romanos 10:9  _que si confesares con tu boca que Jesús es el Señor y creyeres en tu corazón que Dios le levantó de los muertos, serás salvo_.'

Si hizo esta oración y desea hablar con personas que buscan ser mejores siervos de Jesús, ore pidiendo una iglesia. El Señor le mostrará.  Quien escribe aquí, asiste a la iglesia Menonita de Suba y también quiere se mejor siervo de Jesús y sería feliz de hablar con quien haga esta oración: [vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org "vtamara@pasosdeJesus.org")
