---
layout: post
title: Yo y mi casa serviremos a Jehová
author: vtamara
categories: 
image: "/assets/images/285111.png"
tags: 

---
# Yo y mi casa serviremos a Jehová

# 1. Introducción

Josué 24: 14, 15

| Ahora, pues, temed a Jehová, y servidle con integridad y en verdad; y quitad de entre vosotros los dioses a los cuales sirvieron vuestros padres al otro lado del río, y en Egipto; y servid a Jehová. Y si mal os parece servir a Jehová, escogeos hoy a quién sirváis; si a los dioses a quienes sirvieron vuestros padres, cuando estuvieron al otro lado del río, o a los dioses de los amorreos en cuya tierra habitáis; pero yo y mi casa serviremos a Jehová |

Estas palabras, particularmente **Pero yo y mi casa serviremos a Jehová** fueron dichas por Josué aproximadamente en el 1200aC. Hace unos 3200 años.

¿Deseamos esto para nosotros hoy? Requerimos mejorar en muchos aspectos, uno particular es nuestro ejemplo ante hijo y la confianza en las promesas del Señor y en particular una relacionada con nuestro pecado y nuestros hijos.

# 2. Una corrección terrible del Señor

Durante ese tiempo de Josué y antes desde el 400aC aproximadamente en tiempos de [Moisés](Moises), el Señor habló lo consignado en Éxodo 34:6-7

| Y pasando Jehová por delante de él, proclamó: ##Jehová! ##Jehová! fuerte, misericordioso y piadoso; tardo para la ira, y grande en misericordia y verdad; que guarda misericordia a millares, que perdona la iniquidad, la rebelión y el pecado, y que de ningún modo tendrá por inocente al malvado; que visita la iniquidad de los padres sobre los hijos y sobre los hijos de los hijos, hasta la tercera y cuarta generación.|

El Señor es misericordioso, por eso nos corrige y todos somos de Él, es quien más nos ama a cada uno y en su soberanía eligió corregir con hijos, nietos, bisnietos y hasta tataranietos los pecados de los papás.  No lo entendemos pero el Señor sabe que es lo mejor.  Más pidamos perdón para que borre nuestras iniquidades.

Un ejemplo de uno de estos castigos (ver {2}) se relata en 2 Samuel 12:14 hacía el año 1000aC, le ocurrió a alguien que tenía su corazón conforme al señor, al valiente que venció a Goliat, que logró reinar y mantener unido a Israel hacía el año 1000aC, el que compuso gran parte de los salmos y con fuerza lucho  contra filisteos  y  los  pueblos  vecinos  de  Israel  para  poner  en  jaque  a  los  historiadores  actuales  sobre  el  poderío  de  su  Reino: el rey \[David\].  Veamos 2 Samuel  12:13-14 y 12:23

| 13 Entonces dijo David a Natán: Pequé contra Jehová. Y Natán dijo a David: También Jehová ha remitido tu pecado; no morirás.  14 Mas por cuanto con este asunto hiciste blasfemar a los enemigos de Jehová, el hijo que te ha nacido ciertamente morirá ...  23 Mas ahora que ha muerto, ¿para qué he de ayunar? ¿Podré yo hacerle volver? Yo voy a él, mas él no volverá a mí.   |

David tenía un problema de faldas. En este caso con Betsabé esposa de uno de sus soldados: Urías.  Al verla desnuda desde su ventana no resistió la tentación y se acostó con ella, unión de la cual nacería un bebé y por lo que mandó matar a Urías como escondiendo su pecado.  Sin embargo Dios le habló por el profeta Natán evidenciando su pecado y mostrándole el castigo que recibiría.  David el ser humano siguió amando al Señor, incluso con ocasión de esto se dice que compuso el salmo 51, del cual los invito a leer en oración algunos versículos 51:7-11:

| Purifícame con hisopo, y seré limpio; Lávame, y seré más blanco que la nieve. 8 Hazme oír gozo y alegría, Y se recrearán los huesos que has abatido. 9 Esconde tu rostro de mis pecados,  Y borra todas mis maldades. 10 Crea en mí, oh Dios, un corazón limpio, Y renueva un espíritu recto dentro de mí. 11 No me eches de delante de ti,Y no quites de mí tu santo Espíritu.|

Siendo David espiritualmente tan grande, si le paso esto con su hijo ¿qué podemos esperar nosotros?

# 3. Una promesa para nosotros hoy

Aproximadamente en el 580aC, cuando el Señor permitió el exilio del pueblo Judio a Babilonia, y permitió la destrucción de Jerusalén y el templo, el por su misericordia cambió la dura sentencia:

\[Ezequiel\] 18:1-20

|Vino a mí palabra de Jehová, diciendo:  2 ¿Qué pensáis vosotros, los que usáis este refrán sobre la tierra de Israel, que dice: Los padres comieron las uvas agrias, y los dientes de los hijos tienen la dentera? 3 Vivo yo, dice Jehová el Señor, que nunca más tendréis por qué usar este refrán en Israel.  4 He aquí que todas las almas son mías; como el alma del padre, así el alma del hijo es mía; el alma que pecare, esa morirá.   5 Y el hombre que fuere justo, e hiciere según el derecho y la justicia;  6 que no comiere sobre los montes, ni alzare sus ojos a los ídolos de la casa de Israel, ni violare la mujer de su prójimo, ni se llegare a la mujer menstruosa, 7 ni oprimiere a ninguno; que al deudor devolviere su prenda, que no cometiere robo, y que diere de su pan al hambriento y cubriere al desnudo con vestido,  8 que no prestare a interés ni tomare usura; que de la maldad retrajere su mano, e hiciere juicio verdadero entre hombre y hombre,  9 en mis ordenanzas caminare, y guardare mis decretos para hacer rectamente, éste es justo; éste vivirá,(B) dice Jehová el Señor. 10 Mas si engendrare hijo ladrón, derramador de sangre, o que haga alguna cosa de estas, 11 y que no haga las otras, sino que comiere sobre los montes, o violare la mujer de su prójimo, 12 al pobre y menesteroso oprimiere, cometiere robos, no devolviere la prenda, o alzare sus ojos a los ídolos e hiciere abominación, 13 prestare a interés y tomare usura; ¿vivirá éste? No vivirá. Todas estas abominaciones hizo; de cierto morirá, su sangre será sobre él.  14 Pero si éste engendrare hijo, el cual viere todos los pecados que su padre hizo, y viéndolos no hiciere según ellos; 15 no comiere sobre los montes, ni alzare sus ojos a los ídolos de la casa de Israel; la mujer de su prójimo no violare, 16 ni oprimiere a nadie, la prenda no retuviere, ni cometiere robos; al hambriento diere de su pan, y cubriere con vestido al desnudo; 17 apartare su mano del pobre, interés y usura no recibiere; guardare mis decretos y anduviere en mis ordenanzas; éste no morirá por la maldad de su padre; de cierto vivirá. 18 Su padre, por cuanto hizo agravio, despojó violentamente al hermano, e hizo en medio de su pueblo lo que no es bueno, he aquí que él morirá por su maldad.  19 Y si dijereis: ¿Por qué el hijo no llevará el pecado de su padre? Porque el hijo hizo según el derecho y la justicia, guardó todos mis estatutos y los cumplió, de cierto vivirá. 20 El alma que pecare, esa morirá; el hijo no llevará el pecado del padre, ni el padre llevará el pecado del hijo; la justicia del justo será sobre él, y la impiedad del impío será sobre él.  |

Dentera se refiere a la sensación áspera y desagradable en dientes o encías por alimentos ácidos, tacto áspero y ruido chirriante (ver {6}).

Es decir podemos estar tranquilos mientras cumplamos las condiciones:

Vs 3  Es una promesa para Israel. Hoy en Espíritu el pueblo de Dios, Israel, es el pueblo de sus hijos que voluntariamente recibieron la adopción, cuyos pecados han sido lavados por la sangre de Jesús, recibiéndolo en el corazón permitiendo que Él viviera en cada uno, manteniendo sus estatutos de amor y pidiendo y recibiendo perdón continuamente.

Vs 14. Es difícil porque el hijo viendo errores de su papá o mamá tendrá que decidir no hacer lo mismo. Necesita discernimiento que pedimos al Señor y voluntad.

Vs 16 y 17. Estamos dando de comer al hambriento y vistiendo al desnudo? Estamos fijándonos en los desplazados?

# 4. Nuestro esfuerzo

El ejemplo es la mejor lección para nuestros hijos, veamos dos casos:

* En mi caso personal me siento bastante mal cuando escucho que una de mis hijas trata mal a la otra, y noto en su tono el tono con el que yo en ocasiones me dirijo a ellas.   Es decir al matratarlas les he enseñado a maltratar a otros.
* Hacía 1945 llegó al Cesar entre la Granja y Tamalameque el señor Alberto Marulanda Grillo, (ver {3}) Compró 6000 hectáreas de tierra y desplazo con amenazas y masacres a campesinos de los alrededores para llegar a tener 12000 hectáreas de su finca 'Bellacruz' (irónico nombre).  sus desplazados se agruparon en Playa, presionaron al gobierno que terminó comprándole al señor Alberto 2600 hectáreas para titular a los desplazados, fueron avaluados en 300 millones pero vendidos en 700 millones y la titulación no se completó.  Tras la muerte del señor Alberto los desplazados veían con esperanza que su hijo Carlos Alberto Marulanda tomara el control de la finca, pues había nacido en nueva York, había estudiado economía en Harvard y Cambridge, fue ministro de desarrollo durante el gobierno de Virgilio Barco, representante a la cámara y embajador ante la unión Europea entre 1995 y 1996.  Sin embargo como quedó consignado en la sentencia T-227/1997 resuelta en tercera instancia por la Corte Constitucional en 1997: el INCORA en 1990 intentó clarificar propiedad y titulación, pero ni los funcionarios ni los topografos pudieron completar el estudio por amenazas que recibieron de paramilitares e incluso le despellejaron la espalda a uno de los funcionarios.  Algunos de los desplazados de Bellacruz tuvieron que trasladarse a Bogotá en 1996. Allí fueron estigmatizados hasta por la gobernadora de Cundinamarca, varios fueron re-ubicados en Tolima en ese año.  Carlos. A. Marulanda fue extraditado a Colombia desde España (tras años de protección Europea) en 2001, acusado como autor intelectual del homicidio de 40 campesinos por parte de Salvatore Mancuso. Pero fue liberado en Noviembre de 2002 a pesar de la evidencia que lo vinculaba con los grupos paramilitares.

Infortunadamente el hijo aprendió del papá que valía más la tierra que la vida humana y que cualquier método valía para conseguirla.  Ni el estudio, ni lo trabajos cambiaron el mal ejemplo, ni le ayudaron a discernir y corregir.

Hoy los desplazados en Colombia se cuentan por los 4'000.000 en los últimos 25 años (ver {5}) .  De acuerdo a {4}  entre las principales causas de desplazamiento está principalmente la amenaza directa 43.7% seguida de asesinato de familiares, de vecinos o amigos, masacres, torturas, desaparición forzada, secuestro y asesinato.  Los grupos guerrilleros, según esa fuente, son responsables del 39% de los desplazamientos, mientras que grupos paramilitares del 32.6%.  El tamaño promedio del hogar colombiano es de 3.9 personas, mientras que el de hogares desplazados es de 5 personas. La mayoría de desplazados son niños menores de 14 años (44.5%). Tras el desplazamiento un 30.2% de los desplazados se vincula a grupos religiosos.

Sin duda cada uno tiene errores y pecados que está mostrando a hijos. Pidamos ayuda al Creador para corregir y poder darles buen ejemplo. Busquemos la justicia expresada en Ezequiel, demos al que necesita, ayudemos a nuestros desplazados. Ayudemos a forjar una sociedad mejor para nuestros hijos, y enseñemosles con nuestro ejemplo a forjarla.  Desde la iglesia apoyemos al que necesita.

# 5. Oración

* Por los desplazados de la hacienda Bellacruz, por la familia Marulanda, por las víctimas y los victimarios para que haya cambios que traigan verdad, justicia y Tu paz.
* Por nuestros hijos, dales discernimiento de lo que está bien y lo que está mal, y voluntad para no seguir el pecado así se los enseñe la TV, sus compañeros, los dirigentes o aún sus propios padres.  Ayúdalos para que sigan Tu Camino.
* Señor como papás y mamás de familia de pedimos perdón por nuestras faltas, nos acogemos a tu misericordia con nosotros, nuestros hijos y generaciones venideras. Revélanos pecados por los que debemos trabajar y perdonanos.  Haznos oir tu gozo. Lava nuestras faltas con tu sangre preciosa Señor Jesús, vive en nosotros, en cada uno y en nuestras familias.
* Cortamos en el nombre del Señor Jesús toda cadena de pecado en nosotros proveniente de nuestros padres, abuelos, bisabuelos o tatarabuelos.
* Ayúdanos a cambiar nuestros errores para que no sean mal ejemplo para nuestros hijos.
* Señor recibimos hoy tu promesa que ni nuestros hijos, ni nuestras generaciones venideras pagarán nuestro pecado.
* Señor ayúdanos para cumplir lo dicho por Josue "**yo y mi caso serviremos a Jehova**"

Todo lo pedimos en el nombre de Jesús.

# REFERENCIAS

* {1} Biblia. Reina Valera 1960.
* {2} http://maranathalife.com/spanish/teaching/bad-dad.htm. Como Ser un Mal Padre. Ricardo Murphy. Revisado en 2010.
* {3} Un País donde todo ocurre, Alexandra Cardona Restrepo. http://books.google.com.co/books?id=srm-0n-ue6kC&lpg=PP1&ots=0OGV7UAPeN&dq=un%20Pais%20donde%20todo%20ocurre%20Alexandra&pg=PP1#v=onepage&q&f=false
* {4} El Reto.Informe de la Comisión de seguimiento a la política pública sobre desplazamiento forzado de Luis Jorge Garay 2009.
* {5} CODHES informa. Boletín 75. 2010. CODHES. http://www.codhes.org/images/stories/pdf/codhes%20informa%20final%20n%C2%BA%2076.pdf
* {6} http://www.wordreference.com/definicion/dentera

***

Predicado en Iglesia Cuadrangular del barrio El Codito el 27.Jun.2010. Vladimir Támara P. vtamara@pasosdeJesus.org. Dominio Público.

Imagén de dominio público de [https://openclipart.org/image/400px/285111](https://openclipart.org/image/400px/285111 "https://openclipart.org/image/400px/285111")
