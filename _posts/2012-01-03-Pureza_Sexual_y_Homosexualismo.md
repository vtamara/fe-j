---
layout: post
title: "Pureza_Sexual_y_Homosexualismo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción
El viernes 2.Dic.2011 iba con 4 niños en transmilenio, se subieron 2 jóvenes de unos 18 años que se trataban entre si de loca y mari..., uno tenía maquillaje y cabello un poco largo como alisado.  No les oí hablar de Dios, no se como era su relación con Él.  Uno hablaba de haber sido maltratado por la policia.

Entre los menonitas hay movimientos que están abogando por matrimonio homosexual e incluso ordenación de homosexuales, tanto a nivel mundial como local -el tema se trató durante el segundo semestre de 2011 en una sesión de “Fe y Café” de la iglesia Menonita de Teusaquillo con líderes de esta iglesia que apoyaban la aceptación del matrimonio homosexual en la iglesia.  

Por ejemplo entre las metas de Pink Menno (ver {2}) dice (traducción libre): “Pink Menno apoya la inclusión de individuos LGBTQ en matrimonios, ordenación y en la comunidad de amor de la congregación cristiana dentro de la Iglesia Menonita.”

He sabido de personas homosexuales que son muy amorosas y humanas, que trabajan por la paz al igual que nosotros.

Sin embargo en el aspecto sexual, personalmente cuestiono que el movimiento LGBT y en particular la homosexualidad sea ejemplo de pureza y es justamente a pureza sexual que nos llama el Señor.

##2. Mensaje

Marcos 7:14-23  (tomado de {1}).
<pre>
14 Y llamando a sí a toda la multitud, les dijo: Oídme todos, y entended:
15 Nada hay fuera del hombre que entre en él, que le pueda contaminar; 
pero lo que sale de él, eso es lo que contamina al hombre.
16 Si alguno tiene oídos para oír, oiga.
17 Cuando se alejó de la multitud y entró en casa, le preguntaron sus 
discípulos sobre la parábola.
18 El les dijo: ¿También vosotros estáis así sin entendimiento? 
¿No entendéis que todo lo de fuera que entra en el hombre, no le puede contaminar,
19 porque no entra en su corazón, sino en el vientre, y sale a la letrina? 
Esto decía, haciendo limpios todos los alimentos.
20 Pero decía, que lo que del hombre sale, eso contamina al hombre.
21 Porque de dentro, del corazón de los hombres, salen los malos pensamientos,
los adulterios, las fornicaciones, los homicidios,
22 los hurtos, las avaricias, las maldades, el engaño, la lascivia, la envidia, 
la maledicencia, la soberbia, la insensatez.
23 Todas estas maldades de dentro salen, y contaminan al hombre.
</pre>


##3. Análisis

!3.1 Contexto
Este pasaje es en respuesta del Señor a fariseos que cuestionan a sus discípulos por sentarse a comer sin lavar las manos como era ritual (7:1-13).    Los versículos que le preceden describen milagros que realizó el Señor (multiplicación de alimentos a 5000 6:30-44, caminar sobre el agua 6:45-51 y sanación 6:53-56), los versículos que le siguen también describen milagros  (sanidad a hija de mujer cananea 7:24-30, sanidad de un sordomudeo 7:31-37, multiplicación de alimentos a 4000 8:1-10).   En su momento y ahora esos milagros que enmarcan este pasaje refuerzan la autoridad de la enseñanza.

En griego la palabra del versículo 21 que se traduce como fornicación es &#960;&#959;&#961;&#957;&#949;&#943;&#945; que también puede traducirse como inmoralidad sexual, que según {7} incluye "adulterio, fornicación, homosexualidad, lesbianismo, relaciones con animales, relaciones entre parientes".

!3.2 Histórico
Históricamente hay evidencia de homosexualismo por ejemplo desde la antigua Grecia, durante el imperio romano, posiblemente en antiguo Egipto y posiblemente en Canaan (ver referencias en {2}).   

Desde la decada de 1960 tras el éxito de unos disturbios en Stonewell, Nueva York por enfrentamientos entre homosexuales y policia, los homosexuales han exigido no sólo aceptación sino “reconocimiento social, la integración y equiparación de derechos completa”.

!3.3 Científico
Desde el punto de vista científico reciente, el homosexualismo se consideraba un desorden mental hasta 1973 cuando fue retirado de la lista (DSM) por la Asociación de Psiquiatría de América y en 1975 por la Asociación de Psicología de América - APA.    Sin embargo, aún hoy cuando el establecimiento científico lo considera normal y promueve explorar relaciones sexuales homosexuales para definir la orientación sexual1, hay psiquiatras, psicólogos y organizaciones que siguen investigando y considerandolo un desorden con posibilidad de tratamiento (por ejemplo la Asociación Nacional de Investigación y Terapia de la Homosexualidad).  También siguen produciendose artículos que demuestran los problemas del homosexualismo (mayor riesgo de suicidio entre adolescentes homosexuales 28%, riesgo de cancer anal aumentado en 4000%, mayor riesgo de enfermedad mental, mayor prevalencia de HIV, mayor riesgo de contraer gonorrea rectal y sífilis,  hijas de parejas lesbianas son más propensas a involucrarse en comportamientos homosexuales y a autodefinirse como bisexuales, ver referencias en {2}).  

Respecto a genética, hay bastante investigación pero no consenso frente a causas genéticas para el homosexualismo (se descarta la posibilidad de un gen gay porque hay gemelos uno de los cuales es homosexual mientras que el otro heterosexual), se ha especulado respecto al ambiente en el utero materno, a causas hormonales así como en el desarrollo cerebral (ver referencias en {2}).
 
La controversia en el campo científico entonces no parece cerca de concluir, por ejemplo  según Nicholas Cummings, ex-presidente de APA lo que llevó a la APA a desclasificar la homosexualidad como desorden mental fueron más presiones políticas que evidencia científica.

Otro caso que evidencia como las presiones políticas influencian las investigaciones cientifícias fue el intento de PETA por evitar investigaciones sobre homosexualimo en ovejas (única especie animal en la que se ha identificado un pequeño porcentaje de machos que exhiben atracción exclusiva hacia el mismo sexo) por parte de  Charles E. Roselli de la Universidad de Oregon, cuyos estudios tienden a mostrar causas del homosexualismo en el desarrollo prenatal (ver [13] y [14]).

!3.4 Civil y Social

En el campo civil Holanda fue el primer país en permitir matrimonios homosexuales en 2001, y hoy en día en Colombia el tema se debate.  Respecto a adopción de hijos por parte de parejas homosexuales es legal en 14 paises (ver referencias en {2}).

Con respecto a religión, hay iglesias cristianas que aceptan plenamente el homosexualismo como la Iglesia Episcopal (parte de la anglicana) desde 2003, -que cuenta con obispos homosexuales desde 2005-, la iglesia Unida de Cristo y algunas iglesias de paises nórdicos.   Así mismo hay iglesias que lo consideran un pecado como la Católica, la Ortodoxa y varias protestantes. En el caso de los cuáqueros no hay consenso y algunos aceptan mientras que otros no (ver {9}).

Las personas LGBT siguen sufriendo también discriminación, por ejemplo en 8 paises el homosexualismo es condenado con pena de muerte y los asesinatos de homosexuales en Brasil alcanzaron 122 en 2007, así mismo hay discriminación laboral y en el servicio militar. 

En nuestro país estas tendencias mundiales facilitan que se vean hoy en los buses parejas del mismo sexo, e incluso en colegios que niños y niñas menores vean a mayores del mismo sexo besandose o que en sus juegos ahora se incluya besarse indiscriminadamente con niños y niñas.  Justamente son los jóvenes quienes necesitan mejor orientación en este tema (ver [Homosexualismo] en este Wiki).

En Colombia hay ministerios cristianos dedicados a la liberación de personas homosexuales, como es “El Corazón del Padre” dirigida por Omar Castilblanco (ver {8}).  


!3.5 Bíblico

A lo largo de la Biblia el matrimonio es para parejas heterosexuales, así que una relación homosexual es un acto de fornicación.  La fornicación está ampliamente proscrita a lo largo de la Biblia, incluso explicitamente por el Señor Jesús en Mateo 15:19 y su concordante Marcos 7:21 “Porque de dentro, del corazón de los hombres, salen los malos pensamientos, los adulterios, las fornicaciones, los homicidios” (este y los textos siguientes son tomados de [20])  Otras partes del nuevo testamento donde se prohibe son:
* 1 Corintios 6:18 “Huid de la fornicación. Cualquier otro pecado que el hombre cometa, está fuera del cuerpo; mas el que fornica, contra su propio cuerpo peca”.
* 2 Tesalonicenses 4:3 “pues la voluntad de Dios es vuestra santificación; que os apartéis de fornicación;”
* 1 Corintios 6:9-10 “¿No sabéis que los injustos no heredarán el reino de Dios? No erréis; ni los fornicarios, ni los idólatras, ni los adúlteros, ni los afeminados, ni los que se echan con varones, ni los ladrones, ni los avaros, ni los borrachos, ni los maldicientes, ni los estafadores, heredarán el reino de Dios.”
* De acuerdo a Galatas 5:16-25, la lascivia y la lujuría, entre otras son obras de la carne que tiene que evitarse.
Respecto al homosexualismo  aunque el Señor Jesús no se refiere al mismo explicitamente hay otros versículos donde se habla del tema, siempre proscribiendolo (respecto a Lev 20:13 en la tradición menonita se nos invita a no matar ni ejercer violencia):
* Levítico 20:13 “Si alguno se ayuntare con varón como con mujer, abominación hicieron; ambos han de ser muertos; sobre ellos será su sangre.”
* Levitico 18:22 “No te echarás con varón como con mujer; es abominación. ”
* Romanos 1:24-27 “Por lo cual también Dios los entregó a la inmundicia, en las concupiscencias de sus corazones, de modo que deshonraron entre sí sus propios cuerpos, ya que cambiaron la verdad de Dios por la mentira, honrando y dando culto a las criaturas antes que al Creador, el cual es bendito por los siglos. Amén.  Por esto Dios los entregó a pasiones vergonzosas; pues aun sus mujeres cambiaron el uso natural por el que es contra naturaleza,  y de igual modo también los hombres, dejando el uso natural de la mujer, se encendieron en su lascivia unos con otros, cometiendo hechos vergonzosos hombres con hombres, y recibiendo en sí mismos la retribución debida a su extravío.”

Hay algunos análisis de estos pasajes que buscan interpretarlos de manera que el homosexualismo sea aceptable, los que se han examinado (ver {2}) nos parece que fuerzan la interpretación o condiciones históricas, pues de lo que hemos podido estudiar el homosexualismo aunque existía no era visto como correcto para los judios del Antiguo Testamento, ni para Jesús, ni para la iglesia primitiva, ni para los primeros anabautistas.

##4. Conclusiones y Oración

* Aún dentro de la más laxa interpretación de la Biblia y del significado de hombre y mujer, el bisexualismo no resulta conciliable con los mandatos de pureza sexual (no fornicar, no a la lujuría ni la lacivia).  El bisexualismo hace parte integral del movimiento LGBT, que incita a la fornicación, junto con la APA y la "Gay Mennonite League" cuándo llama a la juventud a "explorar" para determinar su orientación sexual.  Creemos que incitar a la fornicación es contrario a las palabras de Jesús nuestro Señor y Salvador. 
* A quienes hayamos caido en fornicación (en mi caso antes del matrimonio en 2001) n@s invitamos a orar pidiendo perdón al Señor, a pedirle un cambio en nuestras vidas para que nos mantengamos en pureza sexual con su amor y ayuda.  Él no desprecia un corazón humillado y arrepentido.
* A las personas con dudas respecto a su orientación sexual las invitamos a (1) evitar pensamientos, acciones o relaciones sexuales fuera del matrimonio, (2) a orar al Creador para que les muestre cual es su voluntad y el plan que tiene para sus vidas, y para que nos muestre a la comunidad como ayudar en cada caso a que se cumpla Su voluntad, sin discriminar y con amor.
* Son los jóvenes y los niños quienes quedan más expuestos ante  las invitaciones a una sexualidad irracional.  Las consecuencias de una sexualidad desordenada en ellos son aún más duras y conducen más a la muerte -por ejemplo en casos de aborto pues ha venido aumentando sistemáticamente en menores de 15 años por ejemplo en Bélgica, Bulgaria, República  Checa y España ver [31].
* Desde una ética mesiánica, el Señor Jesús nos llama a ser como niños para entrar en su Reino, sexualmente a ser puros como ellos y como Él, quien invitó a no fornicar y a edificar matrimonios responsables entre hombre y mujer --si esto debe cambiarse oramos al Señor para que le muestre a la comunidad entera.
* En el contexto de comunidades de fe menonitas, no resultaría ético cambiar una interpretación de la Biblia por presiones externas, en lugar de ser fruto de un discernimiento comunitario nacido desde la voluntad de Dios como creemos ocurrió con nuestra visión de no-violencia y paz en los 1500.  Para que se tengan en cuenta como antecedentes, en la Iglesia Menonita de Teusaquillo recibí un seminario de varias semanas de duración por parte del pastor Omar Castilblanco en el 2006 donde se enfatizaba la necesidad de liberarse del homosexualismo y otros pecados sexuales {5}, y se ha predicado que el homosexualismo es un pecado sexual {6}.   Por esto los fieles de la iglesia han entendido al interpretación clásica de la biblia respecto a homosexualismo como la correcta en esta iglesia madre.
* Las estructuras sociales que están acallando los problemas del homosexualismo y de una sexualidad desordenada deben denunciarse, debe permitirse la investigación en estos campos y el acceso a información completa especialmente por parte de jóvenes.
* No hagamos vano el sacrificio de nuestro Señor por cada uno, no para entregarnos a nuestros deseos, sino para buscar santidad, la fornicación, la lascivia y la lujuría nos alejan.   El ejemplo que vemos de Él no es de sexualidad desordenada sino de amor puro hacía todos, sin discriminar, pero denunciando lo que es pecado y perdonando al pecador arrepentido; tampoco lo encontramos a Él ni a la iglesia primitiva avalando o bendiciendo uniones homosexuales -aún cuando en ese tiempo había homosexuales.
* Oramos por guianza para nuestra comunidad de fe respecto a estos temas, pues Dios nos ha enseñado y nos ha ayudado a  nadar contracorriente en el tema de la no-violencia y hoy la corriente de la sociedad (guiada por la "ciencia humana") invita a la lujuría, a la lascivia y a la fornicación.  Dios todopoderoso, por favor enseñanos y ayudanos a nadar con pureza contra esa corriente.

Agradezco al Creador por su Santo Espíritu y la oportunida de dar una versión inicial de esta predica el 4 de Diciembre de 2011 en la Iglesia de la Resurrección de Soacha, San Nicolas.


##5. Bibliografía

* {1} Biblia. Reina Valera. 1960.  http://www.biblegateway.com/passage/?search=Marcos%207&version=RVR1960
* {2} ¿Es ético aceptar el homosexualismo como correcto en la iglesia Menonita? Vladimir Támara. Ensayo para curso de Teología. 2011.
* {3} Pederastia en la Antigua Grecia, Consultada en Nov.2011, http://es.wikipedia.org/wiki/Pederastia_en_la_Antigua_Grecia
* {4} Abortos por pais, grupo de edad y periodo, Consultado Nov.2011,  http://www.ine.es/jaxi/tabla.do
* {5} Boletín de la Iglesia Menonita de Teusaquillo. 15.Oct.2006, 2006. Consultado Nov.2011, http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/boletin-15oct2006.shtml
* {6} Hector Mondragon, LO QUE DICE LA BIBLIA, 2006. Consultado Nov.2011, http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-22oct2006.shtml
* {7} Blue Letter Bible. http://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=G4202&t=KJV
* {8} El Corazón del Padre., Mi experiencia., Consultado en Nov.2011, http://www.elcorazondelpadre.org/
* {9} Homosexualidad y cristianismo, Consultado en Nov.2011, http://es.wikipedia.org/wiki/Homosexualidad_y_cristianismo
