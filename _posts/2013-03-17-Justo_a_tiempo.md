---
layout: post
title: "Justo_a_tiempo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Funky
<pre>
Tal vez se sorprenda que te haya llamado 
pero no soporto esta situación.
He tratado pero no he logrado 
encontrar alivio pa' mi condición,
reconozco que soy responsable 
y por culpa mía estoy en donde estoy.
Se que lo podía evitar 
pero yo mismo ya no se quien soy.

Te llame porque me siento solo 
y me esta matando esta soledad,
la gente me menciono tu nombre
y me recordaron que tu eres real. 
Me arrepiento por no haber actuado 
cuando mis amigos me hablaron de ti 
se que nunca te supe apreciar,
sin embargo hoy estas aquí 

CORO: Tantas veces que te rechacé 
que me hablabas y yo te ignoraba 
pero cuando te necesité 
tu llegaste justo a tiempo.
Cuando ya no quería vivir 
y sentía que estaba perdido 
cuando estaba apunto de morir 
tu llegaste justo a tiempo (hasta mi).

En tus brazos hoy quiero entregarme 
no quiero alejarme de tu corazón 
tu llegaste a mi para salvarme 
y a mi vida darle una nueva razón.
Me pregunto que hubiese pasado 
si no hubieras tu llegado hasta aquí,
todavía no puedo creer 
que llegaras y estés junto a mi.

CORO

Reconozco que yo he sido un tonto 
y que me estaba hundiendo por mi actitud 
pero ahora que te tengo cerca,
me has librado de mi esclavitud. 
Me doy cuenta que lo que tu has hecho 
nadie en este mundo lo podía hacer,
es por eso que agradezco que llegaras hasta aquí 
pues me libraste con tu amor y tu poder.

//CORO//

</pre>

* Letra basada en http://www.musica.com/letras.asp?letra=1814861
* http://www.youtube.com/watch?v=gFExIdOVZDQ
* Agradecimiento a Daniela, Vanessa y Laura
