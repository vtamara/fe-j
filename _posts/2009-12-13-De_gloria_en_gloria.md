---
layout: post
title: "De_gloria_en_gloria"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##De gloria en gloria te veo
Marcos Witt

<pre>
De gloria en gloria te veo,
cuanto más te conozco,
quiero saber más de ti

Mi Dios, cual buen alfarero
quebrantament, transformame,
modeame a tu imagen Señor

Quiero ser más como tu
ver la vida, como tu
saturarme de tu Espíritu
y reflejar al mundo tu amor.

Quiero ser más como tu
ver la vida, como tu
saturarme de tu Espíritu
y reflejar al mundo tu amor
y reflejar al mundo tu amor
y reflejar al mundo tu amor.
</pre>



Acordes

<pre>

D             A         Bm
De gloria en gloria te veo
G                F#m
Cuanto mas te conozco
E                    A    D.
Quiero saber mas de ti

(coro)
G        A
Quiero ser
F#m       Bm
mas como tu
G          A - G F#m
Ver la vida
F#m     Bm
Como tu
G                      A 
Saturarme de tu espiritu
G            A           D
Reflejar al mundo tu amor
</pre>

!Cancion

http://www.youtube.com/watch?v=DiTDsqwvztQ&feature=related

!Video para aprenderla en Piano

http://www.youtube.com/watch?v=-A_Ca4Lyne4&feature=related


! Referencias

* {1} http://www.iglesiadelamor.com/music%5Cpiano%5Cchords/De%20gloria%20en%20gloria%20te%20veo.html
* {2} Acordes de  http://www.10acordes.com.ar/para-imprimir-de-gloria-en-gloria-marcos-witt-9779. Colaboración de Luis eduardo villa Quijano
villaquijano@yahoo.com

