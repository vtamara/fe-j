---
layout: post
categories: []
title: Dios quiere perdonar los pecados que reconozcamos. Isaías 43:22-28
author: vtamara
image: "/assets/images/1062.png"

---

El Señor me ha recordado su poder para perdonar pecados y como Él quiere olvidar mi pecado.

De mi parte especialmente antes de conocer a Jesús cometí tantos pecados, pasando por la idolatría, pecados sexuales, mentir, etc. que Él, Dios, me perdono y eso me permite estar más agradecido y amarlo más. Incluso después de ser cristiano he pecado, y he necesitado y necesito continuamente el perdón y limpieza de parte del Señor. Mi testimonio es que Dios me ha perdonado y por lo gran pecador que he sido, grande agradecimiento tengo.

Veamos un resumen del pasaje de hoy acorde a \[P2005\]

**ARREPENTIRSE GENUINAMENTE**. Dios dice que el fracaso de Israel en dar los sacrificios prescritos se debía a que no llamaban a Dios ni se habían apartado de sus pecados. Les recuerda que Él es quien puede borrar sus transgresiones y dejar sus pecados atrás. Les ordena que vengan y le confiesen su caso para que puedan ser absueltos. ¡A diferencia de la ley del hombre, una persona arrepentida que se declara culpable es perdonada y absuelta en la abogacía de Dios! ¡Qué Dios misericordioso, amable y generoso tenemos!



# 2. Texto: Isaías 43:22-28

22 Y no me invocaste a mí, oh Jacob, sino que de mí te cansaste, oh Israel. 23 No me trajiste a mí los animales de tus holocaustos, ni a mí me honraste con tus sacrificios; no te hice servir con ofrenda, ni te hice fatigar con incienso. 24 No compraste para mí caña aromática por dinero, ni me saciaste con la grosura de tus sacrificios, sino pusiste sobre mí la carga de tus pecados, me fatigaste con tus maldades.

25 Yo, yo soy el que borro tus rebeliones por amor de mí mismo, y no me acordaré de tus pecados. 26 Hazme recordar, entremos en juicio juntamente; habla tú para justificarte. 27 Tu primer padre pecó, y tus enseñadores prevaricaron contra mí. 28 Por tanto, yo profané los príncipes del santuario, y puse por anatema a Jacob y por oprobio a Israel.



# 3. Contexto

Recordemos que Isaías vivió hacía el año 700aC, es decir 100 años antes del cautiverio en Babilonia, entre otros profetizó sobre ese cautiverio futuro, así como del siguiente bajo el imperio Persa y sobre el Mesías que vendría 700 años después.

Veamos que textos hay en la Biblia alrededor de los versículos que estudiamos hoy:

| Vers. | Tema | Promesa o versículo destacado |
| --- | --- | --- |
| 42:1-9 | Profecía sobre el Mesías venidero | 4 “No se cansará ni desmayará, hasta que establezca en la tierra justicia; y las costas esperarán su ley.” |
| 42:10-17 | Alabanza por la liberación poderosa de Jehová | 16 “Y guiaré a los ciegos por camino que no sabían, les haré andar por sendas que no habían conocido; delante de ellos cambiaré las tinieblas en luz, y lo escabroso en llanura. Estas cosas les haré, y no los desampararé.” |
| 42:18-25 | Israel no aprende de la disciplina | 21 “Jehová se complació por amor de su justicia en magnificar la ley y engrandecerla. “ |
| 43:1-7 | Dios es formador de Israel | 2 “Cuando pases por las aguas, yo estaré contigo; y si por los ríos, no te anegarán. Cuando pases por el fuego, no te quemarás, ni la llama arderá en ti.” |
| 43:8-13 | Sólo Dios es salvador | 11 “ Yo, yo Jehová, y fuera de mí no hay quien salve.” |
| 43:14-21 | Profecía sobre el retorno de Babilonia | 18-19 “No os acordéis de las cosas pasadas, ni traigáis a memoria las cosas antiguas. He aquí que yo hago cosa nueva; pronto saldrá a luz; ¿no la conoceréis? Otra vez abriré camino en el desierto, y ríos en la soledad.” |
| 43:22-28 | Anhelo de perdonarnos | 25 “Yo, yo soy el que borro tus rebeliones por amor de mí mismo, y no me acordaré de tus pecados.” |
| 44:1-8 | Jehova es el único Dios | 3 “Porque yo derramaré aguas sobre el sequedal, y ríos sobre la tierra árida; mi Espíritu derramaré sobre tu generación, y mi bendición sobre tus renuevos;” |
| 44:9-20 | La insensatez de la idolatría | 19 No discurre para consigo, no tiene sentido ni entendimiento para decir: Parte de esto quemé en el fuego, y sobre sus brasas cocí pan, asé carne, y la comí. ¿Haré del resto de él una abominación? ¿Me postraré delante de un tronco de árbol? |
| 44:21-28 | Jehová es el Redentor de Israel | 22 Yo deshice como una nube tus rebeliones, y como niebla tus pecados; vuélvete a mí, porque yo te redimí.…28 que dice de Ciro: Es mi pastor, y cumplirá todo lo que yo quiero, al decir a Jerusalén: Serás edificada; y al templo: Serás fundado. |

Estos versículos del libro de Isaías mezclan la justificación de Dios para los castigos que tendría que dar --con el futuro cautiverio en Babilonia-- con promesas y más profecía sobre más bien que vendría después del castigo. Me parece dirigido a varias generaciones:

* A los contemporáneos de Isaías advirtiendoles para que cambiarán y evitaran el juicio.
* A los cautivos en Babilonia (como el profeta Daniel) recordándoles la razón de su situación, animándolos a buscar a Dios y dandoles esperanza sobre el futuro en libertad
* A nosotros instruyendonos, advirtiéndonos para que cambiemos y dándonos esperanza.



# 4. Análisis

Con base en comentario de Matthew Henry

| Versículos | Observación |
| --- | --- |
| 22 | Cómo se indica en el versículo 21 del mismo capítulo, Dios nos ha creado para Él para que proclamemos alabanzas a Él. Israel no había cumplido esa expectativa. Se ven 5 problemas: (1) Había dejado de buscar a Dios, habían dejado de orar, (2) se habían cansado de servir a Dios |
| 23 | (3) habían dejado de dar lo establecido en el antiguo pacto y lo consideraba una carga aunque no era pesada |
| 24 | (4) cuando ofrendaban no lo hacían con lo mejor y de corazón, y (5) no estaban ofrendando con gozo y placer sino lo veían como una carga. Al prevalecer en el pecado, abusando de la gracia de Dios, se convertían en algo fatigante para Dios --que parece paradójico pues Dios es Todopoderoso pero si conocemos personas necias entenderemos que son sencillamente extenuantes y no queda otra opción que mantenerse a distancia. Así somos para Dios cuando nos mantenemos en el pecado. |
| 25 | Sólo Dios puede perdonar pecados y Él quiere hacerlo con quien está arrepentido y lo busca de corazón. Perdona borrando la deuda y olvidando el pecado. |
| 26 | Debemos decirle a Dios nuestros pecados, reconocerlos, Él quiere perdonarnos. |
| 27 | Eran agravantes para el pecado de Israel que los antepasados habían pecado, y que habían tenido profesores y líderes que pecaron y enseñaron a pecar. |
| 28 | Dios tuvo que castigar a la iglesia de entonces y al Estado completo de Israel. |

 

# 5. Conclusión y oración

Dios es misericordioso y justo, es su naturaleza, vemos un pasaje del antiguo testamento en el que expresa su anhelo de olvidar los pecados de la gente, la carga entonces era liviana (entre las muchas fiestas de Israel sólo una anual --la Pascua-- requería sacrificar animales). Hoy después de la obra salvífica de Cristo es aún más liviana, pues sólo necesitamos reconocer que Cristo es Dios y Señor en nuestras vida y reconocer con corazón arrepentido nuestros pecados ante Él --la confesión ante cristianos maduros es muestra de arrepentimiento y debe ser ayuda para no recaer.

Nosotros y nuestros antepasados hemos pecado contra Dios, tomemos un minuto para recordar internamente y ante Dios los pecados que hayamos cometido. Por la sangre de Cristo Él no quiere recordarlos, pero el perdón que Dios nos ha otorgado por todos y cada uno, son más razones para amarlo más, alabarlo y agradecerle.

Repasamos 5 puntos en los que había fallado el pueblo de Israel del tiempo de Isaías, aseguremos cambio si estamos fallando en alguno:

1. Falta de oración
2. Cansarse de servir a Dios
3. No ofrendar a Dios
4. Al ofrendar no hacerlo de corazón
5. Ofrendar sin gozo

Dios gracias Señor por haber olvidado los pecados que cometí, por perdonarlos, por amarme con un amor eterno, y pagar con tu propia sangre por ese perdón.

Podríamos estar ahora en algún pecado, Dios quiere perdonarlo, pero debemos reconocerlo e incluso pedir ayuda para verlo. Señor ayúdanos por favor a ver y reconocer nuestro pecado presente. Ayúdanos a cambiar para no cometerlo más, pido en el nombre de Jesús.

Cuánto anhela Dios perdonar mi pecado, ¿yo estoy queriendo perdonar a quienes me han ofendido? Señor Tú eres misericordioso, ayúdame a ser misericordioso y perdonar a quienes me han ofendido. Ayúdame a ser buen ejemplo ante mi familia y el mundo, a no enseñar a pecar sino a perdonar.

Oramos por Gustavo Petro quien asume hoy la presidencia de Colombia y por el equipo de trabajo que está eligiendo, para que te busquen, que te pidan consejo en oración, que te teman y tengan sabiduría que provenga de Ti únicamente en el nombre de Jesús.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* \[PANORAM2005\] The Bible Panorama. Copyright. 2005 Day One Publications
* Matthew Henry's Commentary. [https://www.biblegateway.com/resources/matthew-henry/toc](https://www.biblegateway.com/resources/matthew-henry/toc "https://www.biblegateway.com/resources/matthew-henry/toc")

La imagen es de dominio público de [https://openclipart.org/image/400px/1062](https://openclipart.org/image/400px/1062 "https://openclipart.org/image/400px/1062")
