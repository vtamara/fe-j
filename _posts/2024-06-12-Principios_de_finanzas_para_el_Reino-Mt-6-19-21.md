---
layout: post
categories:
- Predica
title: Principios de finanzas para el Reino. Mt. 6:19-21
author: vtamara
image: "/assets/images/dar.jpg"

---
# Principios de finanzas para el Reino. Mt. 6:19-21

## Iglesia Menonita de Suba. Estudio Bíblico.
[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 12.Jun.2024.
Dominio público

[Imagen de dominio públido de Khidir Rahim](https://www.pexels.com/photo/men-giving-money-25931056)

## 1 Introducción

En el contexto de un trabajo que estoy realizando he encontrado palabra de Dios
que me anima, se trata de unos principios para manejar las finanzas:


* Trabajar honestamente
* Ofrendar
* Presupuestar
* Ahorrar para dar
* Dar para hacer tesoros en el cielo
* Poner corazón en la expansión del Reino y no en las riquezas

Cómo lo expresó el pastor Andy Hostteter el dinero siempre está buscando la
forma de colarse en el corazón de cada quien, para que confiemos más en él que
en Dios, por eso el principio fundamental es poner el corazón en la expansión
del Reino y no en las riquezas, no hacer tesoros en la tierra sino en el cielo
--hacemos tesoros en el cielo cuando damos desinteresadamente a quien necesita.


## 2. Texto: Mt. 6:19-21

<blockquote>
<sup>19</sup> No os hagáis tesoros en la tierra, donde la polilla y el orín
corrompen, y donde ladrones minan y hurtan; <sup>20</sup> sino haceos tesoros en
el cielo, donde ni la polilla ni el orín corrompen, y donde ladrones no minan ni
hurtan. <sup>21</sup> Porque donde esté vuestro tesoro, allí estará también
vuestro corazón.
</blockquote>


## 3. Contexto


### 3.1 Pasajes alrededor con textos relacionados

<table>
  <tr>
   <td><strong>Vers.</strong>
   </td>
   <td><strong>Tema segun RV1960</strong>
   </td>
   <td><strong>Versículos relacionados</strong>
   </td>
  </tr>
  <tr>
   <td>6:1-4
   </td>
   <td>Jesús y la limosna
   </td>
   <td>3 Mas cuando tú des limosna, no sepa tu izquierda lo que hace tu derecha,
4 para que sea tu limosna en secreto; y tu Padre que ve en lo secreto te
recompensará en público.
   </td>
  </tr>
  <tr>
   <td>6:5-15
   </td>
   <td>Jesús y la oración (en secreto, Padre nuestro)
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>6:16-18
   </td>
   <td>Jesús y el ayuno (en secreto)
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>6:19-21
   </td>
   <td>Tesoros en el cielo
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>6:22-23
   </td>
   <td>La lámpara del cuerpo (ojos)
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>6:24
   </td>
   <td>Dios y las riquezas
   </td>
   <td><sup>24</sup> Ninguno puede servir a dos señores; porque o aborrecerá al
uno y amará al otro, o estimará al uno y menospreciará al otro. No podéis servir
a Dios y a las riquezas.
   </td>
  </tr>
  <tr>
   <td>6:25-34
   </td>
   <td>El afán y la ansiedad
   </td>
   <td><sup>31</sup> No os afanéis, pues, diciendo: ¿Qué comeremos, o qué
beberemos, o qué vestiremos? <sup>32</sup> Porque los gentiles buscan todas
estas cosas; pero vuestro Padre celestial sabe que tenéis necesidad de todas
estas cosas. <sup>33</sup> Mas buscad primeramente el reino de Dios y su
justicia, y todas estas cosas os serán añadidas.
   </td>
  </tr>
</table>



### 3.2 Texto concordante. Lc. 12:32-35

<blockquote>
<sup>32</sup> No temáis, manada pequeña, porque a vuestro Padre le ha
placido daros el reino. <sup>33</sup> Vended lo que poseéis, y dad limosna;
haceos bolsas que no se envejezcan, tesoro en los cielos que no se agote, donde
ladrón no llega, ni polilla destruye. <sup>34</sup> Porque donde está vuestro
tesoro, allí estará también vuestro corazón
</blockquote>


## 4. Análisis

### 4.1 Versículos que resaltan otros principios


**Efesios 4:28** 
<blockquote>
El que hurtaba, no hurte más, sino trabaje, haciendo con sus
manos lo que es bueno, para que tenga qué compartir con el que padece necesidad.
</blockquote>

**Malaquias 3:7-12**

<blockquote>
<sup>7</sup> Desde los días de vuestros padres os habéis apartado de mis leyes,
y no las guardasteis. Volveos a mí, y yo me volveré a vosotros, ha dicho Jehová
de los ejércitos. Mas dijisteis: ¿En qué hemos de volvernos? <sup>8</sup>
¿Robará el hombre a Dios? Pues vosotros me habéis robado. Y dijisteis: ¿En qué
te hemos robado? En vuestros diezmos y ofrendas.<sup>9</sup> Malditos sois con
maldición, porque vosotros, la nación toda, me habéis Robado. <sup>10</sup>
Traed todos los diezmos al alfolí y haya alimento en mi casa; y probadme ahora
en esto, dice Jehová de los ejércitos, si no os abriré las ventanas de los
cielos, y derramaré sobre vosotros bendición hasta que sobreabunde.
<sup>11</sup> Reprenderé también por vosotros al devorador, y no os destruirá el
fruto de la tierra, ni vuestra vid en el campo será estéril, dice Jehová de los
Ejércitos. <sup>12</sup> Y todas las naciones os dirán bienaventurados; porque
seréis tierra deseable, dice Jehová de los ejércitos.
</blockquote>

**Mt. 17:27** 

<blockquote>
Sin embargo, para no ofenderles, ve al mar, y echa el anzuelo, y el primer pez
que saques, tómalo, y al abrirle la boca, hallarás un estatero; tómalo, y dáselo
por mí y por ti.
</blockquote>

**Proverbios 21**


<table>
  <tr>
   <td><strong>Versículo</strong>
   </td>
   <td><strong>RVR1960</strong>
   </td>
   <td><strong>DHH</strong>
   </td>
  </tr>
  <tr>
   <td>5
   </td>
   <td>Los pensamientos del diligente ciertamente tienden a la abundancia; Mas
todo el que se apresura alocadamente, de cierto va a la pobreza.
   </td>
   <td>Los planes bien meditados dan buen resultado;  los que se hacen a la
ligera causan la ruina.
   </td>
  </tr>
  <tr>
   <td>13
   </td>
   <td>El que cierra su oído al clamor del pobre, También él clamará, y no será oído.
   </td>
   <td>El que no atiende a los ruegos del pobre
tampoco obtendrá respuesta cuando pida ayuda.
   </td>
  </tr>
  <tr>
   <td>17
   </td>
   <td>Hombre necesitado será el que ama el deleite, Y el que ama el vino y los
ungüentos no se enriquecerá.
   </td>
   <td>El que se entrega al placer, el vino y los perfumes, terminará en la
pobreza.
   </td>
  </tr>
  <tr>
   <td>20
   </td>
   <td>Tesoro precioso y aceite hay en la casa del sabio; Mas el hombre
insensato todo lo disipa.
   </td>
   <td>En casa del sabio hay riquezas y perfumes, pero el necio gasta todo lo
que tiene.
   </td>
  </tr>
</table>


### 4.2 Relación entre los versículos y los principios

<table>
  <tr>
   <td>
   </td>
   <td>Trabajar honestamente
   </td>
   <td>Ofrendar
   </td>
   <td>Presupuestar
   </td>
   <td>Ahorrar para dar
   </td>
   <td>Dar para hacer tesoros en el cielo
   </td>
   <td>Corazón en el avance del Reino y no en las riquezas
   </td>
  </tr>
  <tr>
   <td>Mt. 6:19-21
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Mt. 6:1-4
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Mt. 6:24
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Mt. 6:25-34
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Ef. 4:28
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>X
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Malaq. 3:7-1</td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Mt. 17:27</td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Pr 21:5
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Pr 21:13
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Pr 21:17
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Pr 21:20.
Ojo vida sencilla
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>X
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>


## 5. Conclusión y oración

Es importante buscar una forma de vida sencilla (es decir sin lujos), trabajar
con esfuerzo, disciplina y honestidad y organizarse con un presupuesto para: (1)
pagar los gastos familiares y obligaciones (incluidos impuestos, seguro de
salud, pensión y caja de compensación), (2)  diezmar y ofrendar, (3) ahorrar
para dar, pues es dando que hacemos tesoros en el cielo.

Señor gracias por la provisión que nos has dado, por el trabajo, por la salud,
examínanos por favor y revélanos en que estamos poniendo más atención y tiempo
que en Ti, ayúdanos a derribar los ídolos de nuestra vida para que Tu, Cristo,
reines continuamente, pedimos en el nombre de Jesús.

Por favor ayúdanos a organizarnos con presupuesto con disciplina para trabajar y
pagar nuestras obligaciones, diezmar y ofrendar y ahorrar para dar a quien
necesita, pedimos en el nombre de Jesús.

Si Jesús aún no es el Señor y Salvador de tu vida, te invitamos a que lo sea
haciendo una [oración de fe](https://fe.pasosdejesus.org/Oracion_de_fe/).


## 6. Referencias Bibliográficas



* [RV1960] Biblia. Traducción Reina Valera 1960.
  [https://biblegateway.com](https://biblegateway.com) 
* [DHH] Biblia. Traducción Dios Habla Hoy 1996.
  [https://biblegateway.com](https://biblegateway.com) 
* Cagle, Chris. 2018. 7 principios bíblicos para ahorrar e invertir tu dinero.
  [https://www.coalicionporelevangelio.org/articulo/7-principios-biblicos-ahorrar-e-invertir-dinero/](https://www.coalicionporelevangelio.org/articulo/7-principios-biblicos-ahorrar-e-invertir-dinero/)
