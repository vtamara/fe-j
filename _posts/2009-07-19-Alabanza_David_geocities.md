---
layout: post
title: "Alabanza_David_geocities"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Por el próximo cierre de geocities, transcribimos a continuación el artículo disponible en:
   http://www.geocities.com/guy_muse/danza.html
Atribuido al final a:  consejo@gu.pro.ec 

##La Alabanza, la Danza y la Oración del Rey David       
Texto 2da.de SAMUEL 6:1 al 23                  

Que hermoso es el precioso mover del Glorioso ESPÍRITU SANTO en la alabanza. Indudablemente, nosotros no hemos vivido la gloria que nuestros antepasados vivieron en  la alabanza, tan sólo tenemos referencia de ella por la Palabra de DIOS y por la historia.Sin embargo, no damos cuenta de la renovación y el avivamiento que EL ESPÍRITU nos trae en cada tiempo.

Dos actitudes aprendemos a través de este texto bíblico sobre la adoración. Dos actitudes que deben presentarse siempre en forma combinada para evitar el peligro de la "religiosidad o de la emoción durante la adoración y la alabanza." Y son:

# EL REGOCIJO y
# LA REVERENCIA.
  
Los sonidos y el movimiento no son tan importantes, como sí lo son las actitudes que deben motivar nuestro tiempo de adoración y alabanza, y estas actitudes son: la santidad, el amor y la reverencia.

Esto es lo que podemos observar a través del estudio  de este texto bíblico.

Cuando los filisteos derrotaron a los israelitas Las Escrituras dicen lo siguiente: "Aconteció que cuando el arca del pacto de Jehová llegó al campamento,todo Israel gritó con tan gran júbilo que la tierra tembló. Cuando los filisteos oyeron la voz de júbilo, dijeron:¿Qué voz de gran júbilo es esta en el campamento de los hebreos ? Y supieron que el arca de Jehová había sido traída al campamento.Y los filisteos tuvieron miedo, porque decían:Ha venido DIOS al campamento. Y dijeron: ¡Ay de nosotros ! ¿Quién nos librará de la mano de estos dioses poderosos? Estos son los dioses que hirieron a Egipto con toda plaga en el desierto. Esforzaos,oh filisteos, y sed hombres, y pelead. Pelearon, pues, los filisteos, e Israel fue vencido, y huyeron cada cual a sus tiendas; y fue hecha muy grande mortandad, pues cayeron de Israel treinta mil hombres de a pie. Y el arca de DIOS fue tomada, y muertos los dos hijos de Elí, Ofni y Finees.(1ra.de Samuel 4:5 al 11)

Los israelitas tenían la impresión, de que el Arca del Pacto era una especie de talismán o amuleto de la buena suerte. Cada vez que estaban en duros conflictos traían el Arca hasta el lugar de su aflicción. Esto me lleva a reflexionar en el hecho de que ellos habían caído en la religiosidad con algo tan Supremo, tan divino y tan Celestial. Que sin lugar a dudas estaba lejos de su realidad.

Cuando el arca llegó hasta  tierra de los filisteos, extraños fenómenos comenzaron a suceder: "Y tomaron los filisteos el arca de DIOS, y la metieron en la casa de Dagón, y la pusieron junto a Dagón." (1ra.de Samuel 5:2). Aquel ídolo y el arca de DIOS quedaron enfrentado durante la noche en aquel recinto, y a la mañana siguiente cuando los sacerdotes filisteos entraron hasta su templo de adoración, quedaron atónitos!" Y cuando al siguiente día los de Asdod se levantaron de mañana, he aquí Dagón postrado en tierra delante del arca de Jehová; y tomaron a Dagón y lo volvieron a su lugar." (1ra de Samuel 5:3). Consternados por aquel incidente los fieles sacerdotes de Dagón, tomaron a su dios y lo volvieron a poner en su habitual lugar. Pero a la mañana siguiente se volvieron a perturbar en gran manera al encontrar a su dios no sólo tumbado delante del arca sino hecho pedazos. Pues su cabeza y sus manos estaban cortadas.

Asdod era la ciudad filistea donde estaba ubicado el templo de Dagón y durante los meses siguientes, los habitantes de la ciudad tuvieron la desgracia de sufrir diversas enfermedades, y plagas de ratas.Como consecuenciasde aquellos sucesos, los sacerdotes y los filisteos tomaron una serie de medidas:
# No se volvió a pisar nunca más el umbral del Templo, "hasta hoy." (1ra de Samuel 5:5) - Esa aniquilación de su dios, puso en el corazón de los filisteos, un respeto al DIOS de los hebreos.
# Mandaron el arca desde Asdod hasta Gat tratando de evadir el castigo desatado por mano del DIOS VIVO. vv.7 y 8
# Ahora el arca es llevada desde Gat a Ecrón. v. 9. Y así por las cinco ciudades más importantes de Filistea.
# El temor y la desesperación era grande entre los habitantes de Filistea,esto los llevó a consultar a sacerdotes y adivinos (1ra.de Samuel 6:2).
# Pagaron un rescate, y aquí es interesante comprender que las cinco ciudades filisteas fueron tocadas por la mano de DIOS " porque una misma plaga ha afligido a todos vosotros y a vuestros príncipes.) 1ra.de Samuel 6:4. En este mismo versículo aparece algo muy curioso: "Y ellos dijeron: ¿ Y que será la expiación que le pagaremos? Ellos respondieron: Conforme al número de los príncipes de los filisteos,cinco tumores de oro, y cinco ratones de oro, . . . "

Se desconoce la fuente tan extrañas instrucciones. Y surge inmediatamente aquí una pregunta: ¿Qué proceso llevó a los sacerdotes y adivinos a tan curiosa conclusión?

Sin embargo, en la medida que el proceso se cumplía, el milagro de la sanidad aparecía. "Haréis, pues, figura de vuestros tumores, y de vuestros ratones que destruyen la tierra, y daréis gloria al DIOS de Israel; quizá aliviará su mano de sobre vosotros y de sobre vuestros dioses, y de sobre vuestra tierra."  v.5  Seguramente acto seguido hubo una especie de cambios de opiniones en tonos muy elevados por el alto costo de la expiación es lo que podemos ver en el v.6. Luego en el v.7 se asume una curiosa forma de limpieza para la transportación del arca, además cinco príncipes de los filisteos y un número no precisado de los mismo estuvieron presentes durante los siguientes eventos de la transportación del arca hasta Israel.

El DIOS invisible había elegido vivir y reunirse con su pueblo allí en el sitio donde se guardaba la sagrada memoria de su Voluntad revelada y escrita. DIOS no necesitaba ser rescatado por su pueblo, ÉL había vuelto por sí mismo, trayendo una prueba más de su gran poder.DIOS  traía a sus impactados secuestradores como prisioneros en un desfile de victoria. vv. 10 al 16.

Había quedado demostrado para la perpetuidad, que el arca no era un talisman para la buena suerte, quedo demostrado que el protector divino podía ser Juez en cualquier parte de la Tierra.

El arca fue llevada hasta la casa de Abinadab en Quiriat-jearim, allí "santificaron a su hijo Eleazar para que guardase el arca de Jehová." 7:1 Por veinte años estuvo en aquel lugar el arca de DIOS. " y toda la casa de Israel lamentaba en pos de Jehová." 7:2

Todo lo que Abinadab tocaba o hacía, era bendecido ricamente por el SEÑOR, esto inquieto a David.Porque la otra pregunta que cabe aquí es:
¿Qué habían hecho todo ese largo tiempo en Jerusalén sin el ARCA?

Durante veinte años se lamentaron, y siguieron por todo ese tiempo sin entender el por qué estaba allí el arca en un lugar que no era de adoración. Pero finalmente los lamentos cesaron pues David y el pueblo se dispusieron a traer el arca de vuelta hacia Jerusalén.

¿Qué espíritu movió al rey David, para sentir lo que sintió por las ricas bendiciones que JEHOVA estaba derramando en la casa de Abinadab?
2da.de Samuel 6:1 al 33.
           
Hubo un cambio en la actitud de el rey David, ahora era diferente, la música, el canto, la risa y la danza llenaban el aire durante todo el trayecto.

Muchas veces estuvo el rey David combatiendo la religiosidad en la adoración y la alabanza, y ahora él caía en lo que había combatido con todo fervor.

Y aunque todo era expresión de alabanza y alegría delante del DIOS y SEÑOR de toda la tierra, cómo se podría pensar desde nuestra cultura, desde nuestra formación y en nuestra concepto convencional, que todo esto esta  cabe en la oración. Y es que,no podemos evitar este otro importante perfil dentro de la adoración y la alabanza. La oración puede manifestarse en un llanto ahogado, en una danza alegre. Puede ser un sollozo como en un canto espíritual. En la oración podemos preguntar, podemos usar nuestro entendimiento, podemos suplicar, y hasta inclinarnos por el sillencio delante del Rey de toda la tierra. En el caso que nos ocupa, me estoy refiriendo en forma específica a la oración como danza, con sonidos de cuerdas, de tamboriles, con cantos y con expresiones de júbilo.

Ahora, como que la historia se volvía a repetir, David en gran desfile conducía la caravana que transportaba el arca del pacto, tal cual como fue, hacía ya veinte años atrás. Ahora la conmoción inicial se transformaba en silencio, " Cuando llegaron a la era de Nacón, Uza extendió su mano al arca  de DIOS, y la sostuvo; porque los bueyes tropezaban. Y el furor de Jehová se encendió contra Uza, y lo hirió allí DIOS por aquella temeridad, y cayó allí muerto junto al arca de DIOS."
2da.de Samuel 6:6 y 7.

David se entristeció, pero también estaba asustado, ya no sintió deseos de danzar, su gran proyecto había terminado en tragedia y humillación. vv. 8 y 9. Entonces David hizo llevar el arca hasta la casa de un hombre llamado; Obed-edom, el cual era geteo. En tanto que todos los festivos procesantes volvieron hasta Jerusalén con  las manos vacías. Y cuando el pueblo se alejaba de la casa de Obed-edom, éste comenzó a experimentar una alegría una prosperidad totalmente desconocida para él. Y durante tres meses se vió favorecido por la mano de Jehová.

Y creo que seguramente la noticia de prosperidad que se había desatado en la casa de Obed-edom, hicieron pensar una vez más a David y proyectar un segundo intento de traer el arca a Jerusalén.¿Pero qué garantías tenía David de que lo anterior no volviera a suceder ? Por eso David hizo esta vez  grandes cambios, primeramente en su actitud, en su amor y en su reverencia hacia el DIOS de su salvación. Y el gram cambio consistía en quitar primeramente la religiosidad que había puesto la primera vez que intentó llevar el Arca hasta Jerusalén. Exodo 25:1 al 15 cfs. Josué 3:1 al 13.

¿Cómo habían llevado los filisteos el Arca hasta Israel ?

¿Y cómo prentendió llevar David el Arca desde la casa de Abibadab?
Sabe? Me da la impresión de que el pueblo hebreo se equivocó profundamente en cuanto a la adoración y a la alabanza en este período específico de la historia del pueblo israelita. Que el Arca era considerada como un talismán, como un objeto para la buena suerte. Porque cada vez que estaban en guerra o en problemas hacían traer el arca y emitían luego fervorosos gritos de júbilo.

v.13 " Y cuando los que llevaban el arca de DIOS habían andado seis pasos,él sacrificó un  buey y un carnero engordado."

Usted seguramente se preguntará, ¿Y no habían también sacrificado antes en Bet-semes? y sin embargo murieron setenta hombres.(1ra. De Samuel capítulo 6). La diferencia significativa, radicaba que esta vez el arca iba a ser cargada sobre los hombros de los levitas y no conducida en una carreta como si fuera rumbo a una feria. 2da.de Samuel 6:13. ¿Por qué era esto importante? Porque el arca NO fue hecha para ser llevada sobre una carreta, y sobre este asunto las leyes levíticas eran estrictas y contenían instrucciones claras y precisas. Recuerde que el arca tenía unas varas recubiertas con oro, en las cuales calzaban unos anillos en las esquinas inferiores del arca, lo que nos da la idea, de que el arca había sido diseñada para ser portada en hombros por los levitas, y no transportada. Hubo aquí un significativo cambio de actitud. El Arca era soberana y no podía darsele el trato que se la había estado dando.

En el libro de Josué en el capítulo 3:1 al 6 se nos muestra claramente que los levitas cargaron en sus hombros el arca del pacto de Jehová a través del Jordán, y mientras esto sucedía, el agua se abría para dejar un camino por donde pasó Josué y el pueblo de DIOS para tomar posesión de la tierra Prometida. Fue un pequeño gran cambio que enseñó la diferencia en la relación entre DIOS y los hombres. DIOS no podía ser conducido, Él gobernaba y por lo tanto ÉL guiaba a sus siervos. Que había aprendido entonces David en todo esto y qué aprendemos nosotros hoy día. La lección de la "reverencia." Ahora entendía que el regocijo tenía que ir de la mano de la reverencia.

De modo que esta segunda vez la música volvió a sonar, los adoradores se derramaron delante de DIOS con cantos de Alabanza y exaltación, mientras tanto que David condujo la danza desde la casa de Obed-edom hasta Jersusalén. Mientras los címbalos y las trompetas sonaban exhuberantes proclamando que El Rey de toda la tierra venía, Mical, esposa de David, se sentía humillada porque el rey, su esposo, danzaba; "y estaba David vestido con un efod de lino." Todo el pueblo se reunió alrededor del arca danzando y cantando, cada vez se hacían más y más ofrendas,se distribuyó comida y bebidas. Seguramente Mical estaba juzgando a David y el pueblo por toda aquella demostración de júbilo, que desde su punto de vista, nada tenía que ver con la adoración y la oración.

Pero.¿Tenía razón ? ¿Qué pudo haber pensado David ? Fue delante de Jehová,  SEÑOR  DIOS: "Por tanto, danzaré y me gozaré delante de Jehová." Seguramente todo esto pudo No haber sido compatible con la idea de Mical y de muchas otras personas, acerca de lo que ellos entendían por dignidad cuando estamos delante del DIOS TODOPODEROSO. Pero es la dignidad de DIOS lo que más debe importarnos, y Su dignidad consiste en Su Majestad y todo el valor inherente.

La ira de DIOS no fue desatada por la danza y el júbilo expresado por el pueblo delante de DIOS. La ira llevaba en si es una enseñanza, y es que cada vez que nos acerquemos a ÉL, debemos hacerlo en base a dos importantes actitudes:
# La Reverencia y
# El Regocijo.

Ambos son compatibles. La reverencia empieza por el temor. El temor es una puerta que nos lleva hacia la reverencia, sin embargo debemos tener mucho cuidado de NO confundirnos. En los pasajes estudiados, vimos que el temor se produjo como una reacción a la ira de DIOS. Por tanto, la reverencia nos trae rendidos hasta los pies de DIOS. Muchas veces nos desagrada que tengamos que aprender la reverencia por medio del temor y nos turba cuando esta viene de la ira de DIOS. Pero nuestra turbación se debe a que muchas veces creemos que la ira de DIOS es como nuestra ira. Su ira viene acompañada de los atributos propios y exclusivos de ÉL, como, Su Amor, Su misericordia, Su consolación, Su bendición etc.

El temor es entonces el escalón hacia nuestro enriquecimiento espiritual
Sin el temor estamos expuestos a peligros por falta de conocimiento

Israel aprendió después de todo esto, que DIOS estaba por encima de todo, y esa debe ser también nuestra aprendizaje-enseñanza. DIOS permitió que sus justos juicios fueran una advertencia a Israel y lo son también hoy para nosotros. Bendiciones !

            -- consejo@gu.pro.ec 
