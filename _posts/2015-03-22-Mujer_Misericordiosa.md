---
layout: post
title: Mujer Misericordiosa
author: vtamara
categories:
- Prédica
image: "/assets/images/erfkiziw4aadeth.jpeg"
tags: Prédica

---
## [Mujer Misericordiosa](https://web.archive.org/web/20160812134426/http://fe.pasosdejesus.org/?id=links/Mujer+Misericordiosa)

## 1. INTRODUCCIÓN

Recientemente el Señor me ha recordado la necesidad de hacer misericordia de varías maneras, como el pastor Jaime me invitó a predicar en el marco del tema del mes "La mujer", pregunté a mi esposa por una mujer misericordiosa de la Biblia y ella me orientó y recordó a una registrada en el libro de Hechos.

## 2. TEXTO Y CONTEXTO

    Hechos 9:36-42
    
    36 Había entonces en Jope una discípula llamada Tabita, que traducido quiere 
       decir, Dorcas. Esta abundaba en buenas obras y en limosnas que hacía.
    37 Y aconteció que en aquellos días enfermó y murió. Después de lavada, la 
       pusieron en una sala.
    38 Y como Lida estaba cerca de Jope, los discípulos, oyendo que Pedro estaba 
       allí, le enviaron dos hombres, a rogarle: No tardes en venir a nosotros.
    39 Levantándose entonces Pedro, fue con ellos; y cuando llegó, le llevaron a 
       la sala, donde le rodearon todas las viudas, llorando y mostrando las 
       túnicas y los vestidos que Dorcas hacía cuando estaba con ellas.
    40 Entonces, sacando a todos, Pedro se puso de rodillas y oró; y volviéndose 
       al cuerpo, dijo: Tabita, levántate. Y ella abrió los ojos, y al ver a 
       Pedro, se incorporó.
    41 Y él, dándole la mano, la levantó; entonces, llamando a los santos y a las 
       viudas, la presentó viva.
    42 Esto fue notorio en toda Jope, y muchos creyeron en el Señor.
    43 Y aconteció que se quedó muchos días en Jope en casa de un cierto Simón, 
       curtidor.
    

Esta traducción es de {1}

Deseo citar otra traducción del versículo 36, de la traducción de {4}

    36 Había en Jope una mujer creyente llamada Tabita, nombre que significa
       "Gacela". Se dedicaba por entero a hacer buenas obras y a socorrer a los 
       necesitados"
    

### 2.1 Contexto literario

Este texto está en medio de la descripción de la persecución a la iglesia (en particular por parte de Pablo antes de su conversión) y como esa persecución ayudó a la expansión de la iglesia, pues los creyentes se fueron alejando de Jerusalén y a su paso fueron predicando y ganando almas para Dios.

Más en particular se inscribe en la descripción de la actividad misionera de Pedro, el texto inmediatamente anterior describe como el apostol Pedro había salido por un tiempo de Jerusalén y estaba en Lida donde Dios lo había usado para darle sanidad a un paralítico.

Los textos que le siguen describen la revelación que Dios le dió a Pedro de que toda su creación había sido purificada y le permitió predicar la salvación a los no judios.

### 2.2 Contexto geográfico/histórico

El relato transcurre en Jope que hoy en día también se conoce como Jafa (ver {11}), se trata de un puerto de los más antiguos del mundo, que está sobre el mar mediterraneo a unos 56Kms al noroeste de Jerusalén. Hoy en dia está junto a Tel-aviv (capital del actual Israel). ![http://upload.wikimedia.org/wikipedia/commons/b/be/Yaffo_air.jpg](https://web.archive.org/web/20160812134426im_/http://fe.pasosdejesus.org/?binary=http%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fb%2Fbe%2FYaffo_air.jpg) Foto de ![](http://upload.wikimedia.org/wikipedia/commons/b/be/Yaffo_air.jpg)

Pedro se encontraba en Lida (hoy en dia Lod o Lud ver {12}) que queda a unos 17 kilometros al sureste de Jope

[http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")

Lida queda entre Jerusalén y Jafa, como se muestra en el siguiente mapa:

![http://www.encinardemamre.com/imagenes/mapas/Lida.jpg](https://web.archive.org/web/20160812134426im_/http://fe.pasosdejesus.org/?binary=http%3A%2F%2Fwww.encinardemamre.com%2Fimagenes%2Fmapas%2FLida.jpg) Mapa de [http://www.encinardemamre.com/Mapas-de-la-Biblia.html](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")

## 3. ANÁLISIS

El Señor está mostrando su gloria por intermedio de Pedro, en la debilidad de la perseguida iglesia empieza a ampliarla hasta los no judios, por lo que hoy podemos pertenecer a ella.

Y en ese pequeño puerto de Jope que hoy en día tiene calles angostas y donde nunca han llegado barcos grandes por sus aguas poco profundas, allí resucita a una mujer.

### 3.1 RESURRECCIÓN

Esta resurrección tiene paralelos con una que hizo el Señor Jesús, como se describe en Marcos 5, algunas similitudes son:

* Llaman a Jesús a visitar a la hija de Jairo, aunque en el camino le dice que no es necesario que vaya pues muere.
* Muchas personas lloraban en la casa de la niña muerta
* Jesús sacó a las demás personas se quedó sólo con la niña, sus padres y los 3 discípulos que lo acompañaban.
* La forma de llamar a la niña es similar:

    41 Y tomando la mano de la niña, le dijo: Talita cumi; que traducido es: Niña, a ti te digo, levántate.
    

Así que podemos aprender varios aspecto sobre la resurrección y como Dios obra:

* Hacer esfuerzo por ir donde se nos necesita así toque caminar 17 kilometros como hizo Pedro
* Quedarse con pocas personas y que realmente ayuden
* Orar de rodillas como Pedro hizo --un comentario que leí sugiere que Dios habló a Pedro entonces indicandole como continuar, {2}
* Llamar a la persona y ordenarle que se levante

La Biblia incluye otros testimonios de resurrecciones, incluyendo la más importante, la de Nuestro Señor Jesucristo, pues fue por su resurrección que quienes creemos en Él como Señor tenemos vida eterna. Fuera de la Biblia hay registro de otras resurrecciones e incluso en Internet hay varios testimonios, uno bastante documentado es el del pastor nigeriano o por ejemplo [https://www.youtube.com/watch?v=ny6zHkevIJ8](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG") . Personalmente conozco a un hombre de fe llamado Luis quien vive en la 68 con 26 en Bogotá y a su hijo, Luis en una situación oró por resurrección de su hijo y el Señor se lo concedió.

### 3.2 MISERICORDIA

Volviendo a la traducción del versículo 36, en el Textus Receptus (versión en griego de la Biblia en la cual se basan muchas de las traducciones incluyendo Reina Valera, ver {5}), el nombre de la mujer es Ταβιθά (número Strong 5000), que se interpreta como Δορκάς (número Strong 1393). Según BLB {5} que se basa en el lexicón de Thayer, la palabra Ταβιθά proviene de la palabra aramea צְבִיָּה (con número strong Hebreo 6646), que significa gacela y aparece sólo 2 veces en el antiguo testamento, curiosamente en el libro más "sensual" de la Biblia, Cantares 4:5 y Cantares 7:3 en ambos se comparan los senos de la esposa con dos crías gemela de "Gacela." Un contexto que sin duda le celebra a la mujer y particularmente a la esposa.

Pero lo que quería concluir es que es mejor traducción la de {4}

    36 Había en Jope una mujer creyente llamada Tabita, nombre que significa 
       "Gacela". Se dedicaba por entero a hacer buenas obras y a socorrer a los 
       necesitados"
    

#### 3.2.1 Gacela

![http://deanimalia.com/images/full/desierto/gacela1.jpg](https://web.archive.org/web/20160812134426im_/http://fe.pasosdejesus.org/?binary=http%3A%2F%2Fdeanimalia.com%2Fimages%2Ffull%2Fdesierto%2Fgacela1.jpg) ![http://deanimalia.com/images/full/desierto/gacela3.jpg](https://web.archive.org/web/20160812134426im_/http://fe.pasosdejesus.org/?binary=http%3A%2F%2Fdeanimalia.com%2Fimages%2Ffull%2Fdesierto%2Fgacela3.jpg)

Fotos de {6}

Como para completar el significado de Dorcas, la gacela mostrada en la foto es seguramente el tipo de gacela al que se refiere el Cantar de Cantares, escrito por Salomón, pues según {6} es una Gacela del desierto que antes era común en oriente medio (donde está y estaba ubicado Israel en tiempos de Salomón) y en Africa, pero hoy sólo queda en desiertos del norte de Africa y se considera vulnerable. Bueno el nombre científico de este animal es "Gazella dorcas" como para que no nos quepa duda de la traducción. Algunas características de {6}, {7} y {8}:

* "Puede pasar toda su vida sin beber agua, obteniendo el líquido que necesita de las plantas que ingiere."
* Es más bien pequeña pues su altura llega hasta las 60cm y su longitud hasta 110cm y su peso hasta 20kg
* En español la palabra viene del persa "Ghazal," que significa "elegante y rápida". "Pueden alcanzar velocidades de 97 km/h, y mantener una velocidad de 56 km/h por un periodo prolongado. "
* Viven en grandes rebaños (de más de mil individuos) y son animales herbívoros.
* Para sobrevivir en campo abierto, deben estar atentos todo el tiempo.
* Aunque a menudo se echan, no duermen más de una hora al día, en cortos períodos de cinco minutos o menos.
* Salomón la conocía muy bien pues la Gacela estaba entre la provisión diaria de viveres para él junto con aves de corral, corzos, ciervos, corderos y reses (ver 1 Reyes 5:3). Además en {9} se nos recuerda que estaba entre los animales puros de acuerdo al antiguo testamento así que podía comerse.
* En la Biblia también se describe con ágil y con gracia

#### 3.2 ¿Que significa "hacer buenas obras y socorrer a los necesitados"?

Una de las obras de Dorcas se describe en el versículo 39, se reunía con viudas y hacían vestidos y túnicas, seguramente para dar a l@s necesitad@s.

Una explicación de Jesús está en Mateo 25:34-40:

    34 Entonces el Rey dirá a los de su derecha: Venid, benditos de mi Padre, 
       heredad el reino preparado para vosotros desde la fundación del mundo.
    35 Porque tuve hambre, y me disteis de comer; tuve sed, y me disteis de 
       beber; fui forastero, y me recogisteis;
    36 estuve desnudo, y me cubristeis; enfermo, y me visitasteis; en la cárcel, 
       y vinisteis a mí.
    37 Entonces los justos le responderán diciendo: Señor, ¿cuándo te vimos 
       hambriento, y te sustentamos, o sediento, y te dimos de beber?
    38 ¿Y cuándo te vimos forastero, y te recogimos, o desnudo, y te cubrimos?
    39 ¿O cuándo te vimos enfermo, o en la cárcel, y vinimos a ti?
    40 Y respondiendo el Rey, les dirá: De cierto os digo que en cuanto lo 
       hicisteis a uno de estos mis hermanos más pequeños, a mí lo hicisteis.
    

Pero esto no es lo único que podemos hacer, pues se trata de tener misericordia que es algo mucho más extenso (ver {10}), es amar, es dar al que necesita sin esperar a cambio como el ejemplo Perfecto de Misericordia que recibimos de nuestro Creador resumido en Romanos 5:8:

    Mas Dios muestra su amor para con nosotros, en que siendo aún pecadores, 
    Cristo murió por nosotros.
    

Sin embargo debemos ser cuidados de los engaños de nuestra carne o del enemigo para que confundamos lo que es amor. 1 Juan 4:8 dice que Dios es Amor y es por tanto Él quien nos puede enseñar que es amor y que es pecado. Nuestros deseos pueden oponerse a la voluntad perfecta de Dios y podemos creer que ciertas cosas son amor. De mi testimonio recuerdo que en mi juventud cuando no era cristiano tuve una novia y en una ocasión empecé a seducir a una amiga que tenía novio como por molestar, sin embargo ella después quería que nos besaramos --aunque sabiamos que teníamos novios-- y cuando me alejé empezó a llorar. Yo sentí como pesar y en mi errada mente pensé que sería como misericordioso besarla y eso hice, comenzando una infidelidad que me hizo después terminar con esa ex-novia, posteriormente perder la amiga, ella perdió a su entonces novio y todos quedamos más sucios espiritualmente, lamento decir que no se si hoy esa amiga o la ex-novia pudieron conocer o Cristo o no y tener la oportunidad de que la perdonara como a mi.

Relacionado con la errada misericordia, no deseo juzgar, pero tampoco silenciarme, hoy creo que es errada la visión de diversas iglesias cristianas , que incluyen la Iglesia de la Ciudad de San Francisco que en carta de los ancianos de la semana pasada (ver {11}) acepta el "matrimonio homosexual" y que las parejas homosexuales tengan relaciones sexuales después del matrimonio homosexual.

Entonces amemos a nuestro prójimo tanto como podamos con actos concreto, entregando nuestros recursos, tiempo y habilidades para esto, pero con la guianza de Dios para no caer o hacer caer en pecado.

#### 3.3 ¿Cómo podemos ser como gacelas "dedicada(o) por entero en hacer buenas obras y socorrer a los necesitados"?

| --- | --- | --- |
|  | Actual | Deseado |
| ¿Cuanto tiempo dedico semanalmente a hacer buenas obras y dar a necesitados? |  |  |
| ¿Qué recursos y habilidades que Dios me dió estoy usando en buenas obras y dar a los necesitados? |  |  |
| ¿Qué buenas obras realizo y como socorro a los necesitados semanalmente? |  |  |
| ¿Qué buenas obras realizamos en mi familia y como socorremos a los necesitados semanalmente? |  |  |
| ¿Qué buenas obras realizamos en mi trabajo y como socorremos a los necesitados semanalmente? |  |  |
| ¿Qué buenas obras realizamos en nuestra iglesia y como socorremos a los necesitados semanalmente? |  |  |

Comparación con una gacela en este contexto:

| --- | --- | --- |
|  | Situación actual | Deseado |
| Llevo una vida simple |  |  |
| Tengo gracia con la gente |  |  |
| Soy agil |  |  |
| Trabajo en grupo |  |  |
| Estoy atento/vigilante |  |  |

### 4. CONCLUSIONES Y ORACIÓN

* Señor hazme con gácela en en buenas obras y socorrer a los necesitados, que todo mi tiempo y el de nuestra familia en últimas sea para esto.
* Ayudame a estar atento y vigilante en mi trabajo, a ser eficiente, a trabajar en grupo, a mostrar Tu gracia en Todo lo que haga y a llevar una vida simple para que simplemente otros puedan vivir.
* Ayudanos a hacer un ministerio en nuestra familia comenzando con las necesidades de la familia, pero extendiendonos a la iglesia.
* Señor por favor enseñanos a dicernir entre el pecado y la misericordia. Que ni nuestros deseos ni el enemigo puedan engañarnos y alejarnos de Tu Voluntad Perfecta.
* Señor usanos de acuerdo a Tu voluntad, si quieres en resurrecciones o con las señales de Tus discípulos en sanidades, en liberaciones y predicando Tu Palabra.
* Señor Jesús gracias por reconciliarnos con Dios Padre con tu muerte y por darnos la oportunidad de salvación a quienes creemos y nos mantenemos en Ti con Tu resurrección.

Si aún no ha confesado su fe en Jesús y desea recibirlo en su corazón para tener salvación y comenzar vida cristiana l@ invitamos a hacer una **Oracion de Fe**[?](https://web.archive.org/web/20160812134426/http://fe.pasosdejesus.org/?id=Oracion+de+Fe)

### 5. REFERENCIAS

* {1} Biblia RV1960. [https://www.biblegateway.com/passage/?search=hechos+9&version=RVR1960](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")


* {2} [http://estudiobiblia.blogspot.com/2014/05/hechos-932-43-pedro-en-lida.html](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {3} [http://www.goisrael.es/Tourism_Spa/Tourist%20Information/Christian%20Themes/Details/Paginas/Lydda.aspx](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {4} Biblia "La Palabra". El mensaje de Dios para mi. Enciclopedia Ilustrada." 2da Edición. 2011. Sociedad Bíblica de España.
* {5} Concordancia Strong entre Biblia KJV y Textus Receptus. [http://www.blueletterbible.org/Bible.cfm?b=Act&c=9&t=KJV#s=t_conc_1027036](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {6} [http://deanimalia.com/desiertogacela.html](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {7} [http://es.wikipedia.org/wiki/Gazella_dorcas](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {8} [http://es.wikipedia.org/wiki/Gazella](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {9} [http://www.wikicristiano.org/diccionario-biblico/1966/gacela/](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {10} [http://www.jba.gr/es/La-misericordia-del-Senor.htm](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {11} [http://www.citychurchsf.org/A-Letter-From-The-Elder-Board](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {12} [http://es.wikipedia.org/wiki/Lod](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")
* {13} [http://www.encinardemamre.com/Mapas-de-la-Biblia.html](http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG "http://upload.wikimedia.org/wikipedia/commons/d/dd/Downtown_area_of_Lod%2C_Israel_00262.JPG")

***

Gracias a Dios predicado el 22 de Marzo de 2015 en la Iglesia Menonita de Suba.