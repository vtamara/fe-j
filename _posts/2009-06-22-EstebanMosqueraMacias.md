---
layout: post
title: "EstebanMosqueraMacias"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Esteban Mosquera Macías tenía una semana de nacido cuando fue asesinado.

[http://www.vanguardia.com/images/stories/2009/jun/17/judicial/16JUDIC06B085_BIG.jpg]
(ver {5})

Lo que le ocurrió ha venido conociendose en los últimos días gracias a Dios y a la oración.

##FAMILIA

|Mamá |Johana Andrea Macías | Educadora de Biología en un colegio de Piedecuesta. 24 años|
|Papá| Orlando Mosquera| Educador de primaria en un colegio rural. 26 años|
|Hermana| N.N.| Bebé. 16 meses |

Vivían en Piedecuesta Santander.

##CRONOLOGÍA

* 3.Jun.2009 La mamá denunció que 2 hombres y 1 mujer le habían secuestrado al hijo de 7 días de nacido. {4}
* 7.Jun.2009 4000 piedecuesteños acompañaron a la familia pidiendo el rescate del bebé.  Se iniciaron cadenas de oración {3}.
* Entre las llamadas de solidaridad que recibió la policía, uno lo "hizo desde Bogotá y dijo ser miembro de una comunidad cristiana. En ese relato el vidente describió algunas circunstancias de tipo familiar que vivía la pareja, las cuales según los investigadores fueron acertadas", el otro "desde una vereda ubicada en el municipio de Santander y en su comunicación describió un perfil sicológico de Johana Macías, el cual, en efecto, coincidía con el que ya las autoridades habían elaborado" {1} 
* 16.Jun.2009 La policia interroga a la mamá, quien cambia la versión de los hechos. Con indicaciones de la mamá y ayuda de Dios encuentran el cuerpo del bebé semienterrado "a la altura del kilómetro 52 vía Bucaramanga-San Gil (Santander) en el sector de Pescadero" {7}.  La mamá confesó entonces que ella había sido quien lo había hecho {1}:
<pre>
Llama la atención un detalle, hasta ahora desconocido, sobre el agente 
que encontró la bolsa con el cuerpo del bebé. Al llegar al lugar apegado
a su fe oró y le pidió a Dios que si en ese sitio había alguna pista se
la mostrara.
Fue entonces cuando la brisa, ausente hasta ese momento, levantó un 
penetrante y nauseabundo olor, que fue percibido hasta por la mamá de 
Esteban.
La brisa siguió y de paso trajo consigo la respuesta a la oración del 
investigador quien observó una bolsa naranja ligeramente tapada con arena, 
donde estaba el cuerpo sin vida de Esteban.
Ante el hallazgo, Johana Macías no pudo ocultar más su patraña, agarró 
fuertemente las manos de un investigador y con lágrimas en sus ojos repitió 
varias veces esta frase que aún retumba en las mentes de quienes estaban 
allí: “Sí yo lo hice, fui yo, yo lo hice”.
</pre>
* 18.Jun.2009 sepultado el cuerpo de Esteban {6}. Creo (junto con diversas denominaciones cristianas) que el espíritu de Estebán está  con Dios.
* 18.Jun.2008 en el proceso contra Johana se maneja la hipótesis de un crimen premeditado para ocultar paternidad del bebé "La hipótesis que manejan los investigadores, y que es investigada a fondo, es que el niño no sería hijo de Orlando Mosquera, por lo que su progenitora, al verse presionada por la revelación de la paternidad, habría optado por asesinar a su hijo y luego fingir que lo habían secuestrado." {10}

##FILICIDIO

Se trata del asesinato de un hijo por parte de su papá o de su mamá, el caso de Esteban es reciente y muy sonado, pero sólo del año pasado (2008) el Instituto de Medicina Legal reportó 31 casos, 25 asesinados por sus mamás y 6 por sus papás {2}, {8}.

##ORACIÓN

Oramos por la familia de Esteban y de tant@s otr@s bebés nacidos y no nacidos cuyas vidas han sido interrumpidas por sus mamás o papás, para que el Señor les de consuelo y les revele que están perfectamente junto a Él.

Oramos por Johana para que alcance misericordia de Dios por medio de Jesucristo y cambie, al igual que l@s filicidas, l@s abusador@s sexuales de menores, l@s promotor@s de homosexualismo, pornografía, grosería, violencia, desobediencia entre niñ@s y por quienes lo han permitido. 

Oramos por la hermanita de Esteban para que sea luz y testimonio en esta tierra del amor de Dios por Esteban y por los niños.

Oramos por los niños y niñas, para que nuestro Creador los proteja, pues además de llenar de alegría y pureza los espacios y momentos, son nuestro ejemplo aquí y ahora:

| Llevaron unos niños a Jesús para que les impusiera las manos y orara por ellos, pero los discípulos reprendían a quienes los llevaban. Jesús dijo: "Dejen que los niños vengan a mí, y no se lo impidan, porque el Reino de los Cielos es de quienes son como ellos.? |
||
|Mateo 19:13-14 {9}|

También oramos para que ni los papás y mamás, ni los jueces, ni psicológos, ni nadie impida que los niñ@s vayan al Señor Jesús.

Estimad@ lector@, si tiene necesidad en este momento de entregar su vida al Señor Jesús, si requiere que Él l@ limpie y viva en su corazón, por favor repita a conciencia una sencilla [Oración de fe] y pida al Creador que l@ guíe a una buena iglesia.


##REFERENCIAS

* {1} Así se descubrió que Esteban no estaba secuestrado sino muerto 19 de Junio de 2009. Vanguardia Liberal. Judicial. http://www.vanguardia.com/judicial/69-judicial/31232-asi-se-descubrio-que-esteban-no-estaba-secuestrado-sino-muerto
* {2} http://www.caracoltv.com/noticias/nacion/articulo142739-madre-de-esteban-alejandro-mosquera-confeso-haberlo-asesinado
* {3} http://www.elespectador.com/noticias/nacional/articulo146122-madre-del-bebe-asesinado-santander-confeso-su-crimen
* {4} http://www.vanguardia.com/judicial/69-judicial/30667-nuevos-detalles-del-secuestro-del-pequeno-esteban-alejandro
* {5} http://www.vanguardia.com/judicial/69/31000
* {6} http://www.vanguardia.com/judicial/69-judicial/31154--ampliacion-esteban-alejandro-ya-fue-sepultado
* {7} http://www.eje21.com.co/index.php?option=com_content&task=view&id=13697&Itemid=8816
* {8} Casos de madres que matan a sus hijos hay dos veces al mes. ¿Qué las lleva a volverse asesinas?. El Tiempo. http://www.eltiempo.com/colombia/justicia/ARTICULO-PRINTER_FRIENDLY-PLANTILLA_PRINTER_FRIENDL-5493947.html
* {9} http://www.biblegateway.com/passage/?search=Lc18:15-17&version=42
* {10} http://www.elespectador.com/noticias/nacional/articulo146430-johanna-macias-permanece-recluida-carcel-de-bucaramanga
