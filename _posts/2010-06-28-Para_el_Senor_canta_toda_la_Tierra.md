---
layout: post
title: "Para_el_Senor_canta_toda_la_Tierra"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
PARA EL SEÑOR CANTA TODA LA TIERRA


Jesús, Dios nuestro, %%%
Señor nadie es como tú. %%%
Desde aquí decimos hoy: %%%
Cuan grande es tu poder y amor. 

Consuelo, refugio, %%%
Amas justicia y verdad. %%%
La creación, tu pueblo aquí,%%%
Celebramos en tu honor. %%%


Para el Señor canta toda la tierra,%%%
La majestad y el poder suyos son. %%%
Tiemblan los montes, los mares se %%%
Encrespan al sonido de su voz. %%%


Ven celebremos sus obras, sus hechos; %%%
Permanecemos, pues él vive hoy. %%%
Nuestra esperanza descansa en nuestro Dios. %%%



!Referencias

{1} Letra en http://mizaqva.blogspot.com/2010/01/canciones-ageup.html
