---
layout: post
title: "Romanos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Carta a los Romanos

Escrita por Pablo, no hay controversia al respecto (ver {3}), se data entre los años 51 y 58 dC.  Intentando sincronizar las diversas cartas paulinas y hechos puede decirse que posiblemente escribió esta carta durante su tercer viaje misionero mientras estaba en la ciudad de Corintio (ver {4}).

Se han encontrado 5 manuscritos en griego anteriores al siglo IVdC que lo incluyen (ver {7}), los más antiguos son aprox. del 250dC.


De acuerdo a las Obras de Ambrosio (iii, 373) del siglo IV citado en {3} (traducción libre):
"Está establecido que había Judios viviendo en Roma en los tiempos de los Apostoles, y que esos Judios que habían creido ![en Cristo] pasaron a los romanos la tradición que debían profesar a Cristo, pero guardar la ley ![Torah]... No se debería condenar a los romanos, sino alabar su fe, porque sin ver señales, ni milagros y sin ver a ninguno de los apostoles, sin embargo aceptaron la fe en Cristo, aunque de acuerdo al rito Judio"

En esta epistola Pablo exhorta a la comunidad cristiana romana a no discriminar a los gentiles, pues si bien no eran del pueblo de Israel por sangre, por fe y la gracia de nuestro Señor tenían justificación y salvación.


##REFERENCIAS

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/passage/?search=Romanos+10&version=RVR1960
* {2} Easton Bible Dictionary. 1897.  http://www.ccel.org/ccel/easton/ebd2.html  Dominio Público.
* {3} http://en.wikipedia.org/wiki/Epistle_to_the_Romans
* {4} http://en.wikipedia.org/wiki/List_of_New_Testament_papyri
