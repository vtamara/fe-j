---
layout: post
title: "Rollos_de_plata"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Se trata de 2 pequeños rollos de plata (97 x 27 mm y 39 x 11 mm) que contienen la bendición sacerdotal de Números 6:24-26 (ver {1}).

[http://es.catholic.net/catholic_db/imagenes_db/turismo_religioso/jerplacas.jpg] (incluido de {1})

Fueron hallados por Gabriel Barkay en 1979 (ver {2}) mientras excavaba cerca de Jerusalén, aunque sólo pudieron ser desenrollados y traducidos 3 años después. Se han datado en el año 700aC, constituyendose en los escritos bíblicos más antiguos conocidos hasta ahora y en la mención escrita más antigua de YAHWE.  Traduciendo de {2} dicen:

<pre>
El SEÑOR te bendiga; el SEÑOR haga brillar Su rostro sobre tí y sea 
misericordioso contigo; el SEÑOR deje su favor sobre tí, y te de paz."
</pre>

Según {2} el descubrimiento es significativo porque ayuda a establecer la historicidad y la edad de la escritura del Antiguo Testamento.  A finales de 1800 la crítica superior comenzó a cuestionar la datación del Pentateuco (los primeros cinco libros del Antiguo Testamento) que se compuso en forma escrita.  Muchos de estos críticos argumentaron que esta sección de la Biblia fue escrita algún tiempo después del exilio de Babilonia.

De acuerdo a Barkay, el descubrimiento de esta inscripción bíblica temprana es parte importante del argumento para una datación temprana del Antiguo Testamento.  Él reconoce que el hallazgo no prueba que el Pentateuco estuviera escrito hacía el siglo 7. Sin embargo es una evidencia fuerte de esa  posición.

"Al menos puedo decir que estos versos existieron en el siglo 7 ... el tiempo del profeta Jeremias y el tiempo del Rey Josias", dijo Barkay.

Según {4}  el método de inscripción se describe en Jer 17:1 y este hallazgo "demuele la hipótesis documental."

Según {5} es uno de los 10 hallazgos arqueológicos más importantes del siglo XX.

!Bibliografía

* {1} Exhibit #15: (1) Silver Scrolls - oldest Biblical texts. Rob and Jacoba VandeWeghe. Windmill Ministries. http://www.windmillministries.org/frames/CH14-2A.htm
* {2} 'Silver scrolls' are oldest O.T. scripture, archaeologist says. 2004. Baptist Press.  http://www.bpnews.net/bpnews.asp?ID=17741
* {3} Jerusalem: Placas de Plata Inscritas con la Bendicion Sacerdotal Bíblica. http://www.es.catholic.net/turismoreligioso/702/2159/articulo.php?id=19670 
* {4} http://www.oldhamwoodschurch.com/doc_files/stones_of_israel.doc
* {5} http://biblicalstudies.info/top10/schoville.htm
