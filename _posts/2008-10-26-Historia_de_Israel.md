---
layout: post
title: "Historia_de_Israel"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Más o menos hasta el año 850aC hoy en día hay en general consenso en la historicidad de la Biblia.   Algunas evidencias arqueológicas son:

* Primer mención al pueblo de Israel en la estela Merneptha {4}.


* Estela de Mesha {2} o piedra Moabita, descubierta en 1868, escrita por un rey de Moab hacía el 850 aC confirma existencia del pueblo de Israel, del rey Omri y lucha entre Moabitas e Israelitas (algunos estudiosos también deducen mención a la dinastía de David). Describe eventos de 2 Reyes 3:4-27, aunque da conclusión diferente.
[http://upload.wikimedia.org/wikipedia/commons/1/18/Mesha_stele.jpg]
* Prisma de Senaquerib (ver [Ezequias]), asquirido en 1920, describe asedio de los Asirios a Israel y Judea tal como se describe en 2 Reyes 18, 19; Crónicas 32 e Isaias 36 a 37.  Como en el caso de la estela de Mesha la conclusión de la victoria difiere con la Biblia.
[http://oi.uchicago.edu/i/prism.jpg]
* Cilindro de Ciro {3}. Descubierto en 1878, debió escribirse hacía 530aC. Narra invasión de Ciro a Babilonia y liberación de los cautivos.
[http://upload.wikimedia.org/wikipedia/commons/0/03/Cyrus_cilinder.jpg]

Por diversas fuentes (incluyendo Biblia) hoy en día se datan los imperios antiguos:

|Imperio|Inicio|Final|
| Babilonia | 605 |539 aC| Ciro derrota a los Babilonios |
| Persia | 539 | 331 aC| Batalla de Arbela. Alejandro Magno |
| Grecia | 331 | 168 aC| Batalla de Pidna |
| Roma | 168aC | 476 dC| |

Con respecto a Ciro, Isaias habría predicho 150 años antes que un persa llamado Ciro derrotaría el imperio Babilonio en Isaias 4:28 y 5:1.

Los eventos anteriores al 850 son disputados, por ejemplo Patriarcas (ver [Abraham], [Isaac] y [Jacob]), [Exodo], [Conquista de Canaan] y reinados de [David] y Salomón.

Hay en general dos perspectivas en tensión:
** Maximalista: Cuya fuente principal es la Biblia --o la Tora que son los primeros libros, compartidos con la religión Judia.
** Minimalista: Que rechaza la Biblia como fuente histórica. 

El actual gobierno Israeli sería maximalista
http://www.mfa.gov.il/MFAES/Facts+About+Israel/HISTORIA-+Era+Bblica.htm
Entre los historiadores minimalistas, hay justamente uno israeli (Israel Finkelstein) que rechaza desde los reinados de David y Salomón hacía atras, aún con las evidencias arqueológicas que se han encontrando recientemente (ver [David]).


##Referencias

* {1} http://adventistdiscovery.adventistconnect.org//index.php?url=http%3A%2F%2Flink.adventistconnect.org%2Fdiscovery%2FFOP%2FFOP-1.html&option=com_na_wrapper&Itemid=13
* {2} http://es.wikipedia.org/wiki/La_estela_de_Mesha
* {3} http://es.wikipedia.org/wiki/Cilindro_de_Ciro
* {4} http://en.wikipedia.org/wiki/Merneptah_Stele
