---
layout: post
title: Pornografia Esclaviza
author: vtamara
categories:
- Artículo
image: "/assets/images/254918.png"
tags: 

---
Un reciente estudio científico confirma lo que dice la Biblia en Juan 8:34:

<pre>
Jesús les respondió: De cierto, de cierto os digo,
que todo aquel que hace pecado, esclavo es del pecado.
</pre>

La noticia del popular medio "El Tiempo" (ver {1}) relata resultados de un estudio reciente de investigadores del Institudo Max Planck respecto a pornografía y cerebro: "cuanto mayor era el consumo de imágenes pornográficas, más se deterioraban las conexiones entre el cuerpo estriado y la corteza prefrontal, que es la capa externa del cerebro a cargo del comportamiento y la toma de decisiones."

Esto me ayuda a entender muy literalmente como el pecado va atando a la persona, impidiendole tomar decisiones.  Incluso con un cerebro modificado no es posible entender que se es esclavo, todo parece normal.  También el contexto del versículo presentado lo revela, Jesús estaba discutiendo con Judios y les dijo:
<pre>
32 y conoceréis la verdad, y la verdad os hará libres.
</pre>

Pero los judios con un cerebro modificado por el pecado no entendían que eran esclavos:
<pre>
33 Le respondieron: Linaje de Abraham somos, y jamás hemos sido esclavos de nadie.
¿Cómo dices tú: Seréis libres?
</pre>

Y Jesús entonces explica:
<pre>
34 Jesús les respondió:
De cierto, de cierto os digo, que todo aquel que hace pecado,
esclavo es del pecado.
</pre>

Pero Jesús no quedo allí, los científicos que adelantaron esa investigación pueden ver como ese pecado sexual de ver pornografía deteriora al cerebro, cómo va quitando libertad y entre más se ejerce más lo deteriora como en un círculo vicioso, sin embargo ellos no pueden dar una solución para quienes ya tiene un cerebro deteriorado por el pecado.  Jesús si tiene una solución, una cura para conocer la verdad y que seamos verdaderamente libres, la explica en los dos siguientes versículos:

<pre>
35 Y el esclavo no queda en la casa para siempre; el hijo sí queda para siempre.
36 Así que, si el Hijo os libertare, seréis verdaderamente libres.
</pre>

Él mismo Jesús es la medicina, es Él quien verdaderamente nos libera del pecado, pero no sólo eso pues también sólo siendo libertados por Él moraremos con Él eternamente en casa del Padre.

Lo invito hoy a ir a Jesús, es fácil basta orar, hay una oración especial en la que invita a Jesús a vivir en su corazón si aún no la ha hecho o si reconoce que el pecado lo ha vuelto a atar o aún si su cerebro (modificado por el pecado) le dice que usted no ha pecado y que ya es libre, por favor haga la \[Oracion de fe\].

## REFERENCIAS

* {1} http://www.eltiempo.com/estilo-de-vida/salud/pornografia-puede-ser-nefasta-para-el-cerebro/14052401