---
layout: post
title: "Eres_Todopoderoso"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
La única razón de mi adoración eres tu mi Jesús,
mi único motivo para vivir eres tu mi Señor,
mi única verdad esta en ti, eres mi luz y mi salvación,
mi único amor eres tu Señor y por siempre te alabaré.

(CORO)
Tú eres Todopoderoso eres grande y majestuoso,
eres fuerte, invencible y no hay nadie como Tú.
Tú eres Todopoderoso eres grande y majestuoso,
eres fuerte, invencible y no hay nadie como Tú.

La unica razon de mi adoración eres tu mi Jesús,
mi unico motivo para vivir eres tu mi señor,
mi unica verdad esta en ti eres mi luz y mi salvación,
mi unico amor eres tu Señor y por siempre te alabaré.

(CORO)
</pre>

* Letra basada en la de: http://www.musica.com/letras.asp?letra=868118
* Video con letra: http://www.youtube.com/watch?v=hGgGN-a0kZM
