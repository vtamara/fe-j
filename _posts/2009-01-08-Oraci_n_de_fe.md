---
layout: post
title: "Oraci_n_de_fe"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Romanos 9:9 ''que si confesares con tu boca que Jesús es el Señor, y creyeres en tu corazón que Dios le levantó de los muertos, serás salvo.''

----

Señor Jesús en Tí confio, reconozco que soy pecador, te pido perdón
por mis pecados, te pido que me limpies.  

Reconozco que moriste por mí en la cruz del calvario,
para justificarme ante el Padre y que resucitaste.

Abro mi corazón para que entres, te acepto como mi 
único y suficiente Salvador, como mi Rey y Señor.

Por favor escribe mi nombre en el libro de la vida y
no lo borres de allí.
