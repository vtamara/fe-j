---
layout: post
title: "Ciudad_de_David"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---


Su historia tiene profundas implicaciones políticas.


Hallazgos arquológicos que la evidencia de acuerdo a {2}.

Primera referencia a Jerusalén, siglo 14 a.C.

* Fortaleza de Zion. II Sam 5:7
* Edificios Administrativos
* 51 bulas, muchas documentando personajes bíblicos
* Joab,s water shaft, II Sam.5:8.
* Manantiales de Gihón, II Chron.32:30.
* Guard towers.
* Solomon,s coronation site, I Kings 1:33.
* Pool of Melchizedek? (In-filled by Hezekiah).
* Hezekiah,s tunnel, II Chro.32:30; II Kings 20:20.
* Tower of Siloam, Luke 13:4.
* Hezekiah,s Wall.
* Rebuilt wall of Nehemiah, Neh.2:17; 3:8.
* Tombs of House of David, II Kings 2:10.
* Walls surrounding Herod,s temple.
* Stairs down to pool of Siloam, Neh.3:15; Acts 3:1-2; Mk.13;1-2.
* Pool of Siloam, Neh.3:15; John 9:7.
* Palace of David, II Sam.5:11, 17.


##REFERENCIAS

* {2} http://www.oldhamwoodschurch.com/doc_files/stones_of_israel.doc
