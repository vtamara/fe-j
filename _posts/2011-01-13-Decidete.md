---
layout: post
title: "Decidete"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Funky

<pre>
 CORO
//Decídete-te, y agúzate-te
Tu no puedes seguir al garete-te
Analízate-te y examínate-te
Ya no te muevas mas tranquilízate-te//

Oye Bartolo ¿o te peinas o te haces rolos?
Dime ¿A ti te gusta el blanco o el rojo?
Reacciona es tiempo de que tu abras las ojos
Mi hermano y dejes de andar como el cojo
Tu te motivas pero eso es por periodos
Un día eres oveja y otras veces eres lobo
Broky perdona si con esto te incomodo
Pero flojo allá arriba ya te tienen en remojo

   CORO
¡Alerta roja! Que por ahí andan los anfibios
los que hoy están caliente y mañana están tibios
perdonen si los fastidio
pero mañana van a estar pidiendo auxilio
porque entran y salen, salen y entran
realmente no saben donde se encuentran
no tienen en cuenta que mientras ustedes experimentan
no saben a lo que enfrentan

Si hoy estas frió y mañana caliente
Socio vas a confundir a la gente
Un día caliente y otro día frió
Si tus sigues así vas a terminar en lió
si hoy estas frió y mañana caliente
Socio vas a confundir a la gente
Un día caliente y otro día frió
Te metiste en tremendo lió por eso?

CORO

Oye Raymundo
¿Cómo quieres rescatar al mundo
si tu mismo no tienes ni rumbo?
Después que predicas un mensaje profundo
Entonces vienes con la misma actitud del mundo
Deja que te explique ¿quieres que el mundo se identifique?
Tienes que vivir lo que prediques
No te justifiques cuando testifiques
Simplemente vive la vida y no te compliques
No juego con fuego pues me voy a pique
Que no valla a ser que eso me descalifique
Sigo haciendo música que edifique
Y a través de ella el Señor se glorifique.

CORO
</pre>


* Letra basada en: http://www.musica.com/letras.asp?letra=976133
* Música: http://www.youtube.com/watch?v=SaMClgyvhyQ
