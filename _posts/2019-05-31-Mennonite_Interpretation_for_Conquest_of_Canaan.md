---
layout: post
title: Mennonite Interpretation for Conquest of Canaan
author: vtamara
categories:
- Prédica
image: "/assets/images/nabluspanorama2.jpg"
tags: Prédica

---
## CONTEXT FOR JOSHUA 24:1-24 AND MENNONITE INTERPETATION FOR CONQUEST OF CANAAN

### MECHANIC GROOVE MENNONITE CHURCH

### SUNDAY SCHOOL. 9.Dec.2018. 

Shechem was a city initially built by Caananites before year 1800BC. Today it is called Nablus and is a Palestinian city of around 214.000 inhabitants, most of them Muslims that live with Christian and Jewish minorities and in recent years it has been scenario of combats between Israel and Palestine (see {5}).

![https://upload.wikimedia.org/wikipedia/commons/6/66/NablusPanorama2.jpg](https://upload.wikimedia.org/wikipedia/commons/6/66/NablusPanorama2.jpg) Picture from <https://commons.wikimedia.org/wiki/File:NablusPanorama2.jpg>

Neblus is among mountains. One of them is the mount Ebal (the one to the right in the picture) where according to Joshua 8:30-32, Joshua built an altar. Just recently in 1980, this altar could be discovered by the israeli archeologist Adam Zertal, as plenty of evidence show (see {6}). It was built around 1250BC and it is an important testimony of the narrative of the conquest of Canaan written in the Bible (very disputed topic by scholars today).

![http://www.setapartpeople.com/wp-content/uploads/2012/10/Joshua-Altar-6.jpg](http://www.setapartpeople.com/wp-content/uploads/2012/10/Joshua-Altar-6.jpg) Picture or the altar on top of Ebal mount from <http://www.setapartpeople.com/wp-content/uploads/2012/10/Joshua-Altar-6.jpg>

![http://ebal.haifa.ac.il/image1.jpg](http://ebal.haifa.ac.il/image1.jpg) Structure of altar from <http://ebal.haifa.ac.il/image1.jpg>

For the quarter we are working on the NIV Standard Lesson, possibly the most important verse about love: 37 Jesus replied: “‘Love the Lord your God with all your heart and with all your soul and with all your mind.’ 38 This is the first and greatest commandment. 39 And the second is like it: ‘Love your neighbor as yourself.’ Matthew 22:37-39 According to {2} “For the Christian people, the important thing must be the way Jesus interprets the Old Testament. If we look at how God delivers Israel from the armies of the pharaoh, we will notice that he did it in the sea, without any battle ... Although the people had a long pilgrimage in the desert in the midst of wars and conflicts and entered Canaan after bloody battles, this was not God's original plan since he wanted people to practice justice and peace; Definitely disobedience and lack of trust in God were the origin of violence. The Israelite people, despite the miraculous deliverance through the Red Sea, did not trust that God would walk with them (Exodus 23:23 and Deut 1: 26-27).”

According to {3} these wars reveal like through a mirror, the opposite of what God wants, they are like a type of Christ suffering going to the cross and dying. Things got in order after Jesus death, when the sacrifising love of God was fully revealed.

According to {4} events of violence leaded by Joshua like the killing of the inhabitants of Jericho are part of the history of salvation, independent of how we can interpret but they shouldn’t be the focus for christians today, but what Jesus did in Jericho “resisted the temptation to use political and military power (Luke 4:5-8). The first Joshua came to Jericho with a sword. The second came to heal and forgive.”

Before the fall, the people of God (Adam and Eve) didn’t have blood in their hands, after the fall they had blood because of sacrifice of animals, this sacrifice of animals became law and recurring practice since Moses time for forgiveness, and Joshua had even to have human blood in his hands commanded by God because of the sins of God’s people and the nations that were in Canaan. As Hebrew 10:18 says for the people of God whose sins have been forgiven through Jesus “sacrifce for sin is no longer necessary” How much killing of humans is not necessary for we the church, current God’s people today, since He is our protector and the one who fights for us. Jesus confirms clearly the commandment of not killing in Mathew 5:21-22 “You have heard that it was said to the people long ago, ‘You shall not murder, and anyone who murders will be subject to judgment.’ 22 But I tell you that anyone who is angry with a brother or sister will be subject to judgment. Again, anyone who says to a brother or sister, ‘Raca,’ is answerable to the court. And anyone who says, ‘You fool!’ will be in danger of the fire of hell.

Joshua was born in Egypt as slave, he was not a man of war, but he had to become one to obey God. So serving God for mennonites is not going to war but obeying God even in things we don’t like.

Related to Canaan in present, Nelson Kraybill the current president of the Mennonite World Congress will guide a tour of 12 days in September 2019 to Jordan, Palestine and Israel, see [https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#)!

Rererencias

* {1} Bible. NIV.
* {2} La objeción de Conciencia como Ejercicio de la Noviolencia en la Construcción de Paz. Manuales de capacitación en construcción de paz. Pablo Stucky. Serie Justapaz. 2004.
* {3} Book Revis: Cross Vision. [http://mennoworld.org/2018/01/29/columns/book-review-cross-vision/](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/# "https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#")
* {4} [http://mennoworld.org/2016/10/10/columns/kraybill-the-two-joshuas-of-jericho/](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/# "https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#")
* {5} [https://en.wikipedia.org/wiki/Nablus](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/# "https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#")
* {6} [http://ebal.haifa.ac.il/ebal06.html](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/# "https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#")
* {7} [http://www.setapartpeople.com/joshua-altar-found-on-mount-ebal](https://www.tourmagination.com/tour/2019-jordan-palestine-israel/# "https://www.tourmagination.com/tour/2019-jordan-palestine-israel/#")