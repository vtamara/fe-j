---
layout: post
title: Yo seré tu sol
author: vtamara
categories:
- Alabanza
image: assets/images/home.jpg
tags: 

---
Tercer Cielo y Redimi2

    CORO. 
    Cuando la tormenta del dolor 
    Llueva en tu corazón 
    Yo seré tu sol 
    Yo seré tu abrigo 
    Yo seré tu sol 
    Yo siempre estaré contigo 
    Yo seré tu sol 
    
    RAP (REDIMI2) 
    Ven, déjame ser el aliento 
    Déjame ser el amigo que te calme el sufrimiento 
    Quiero que sepas que estoy atento 
    Te ayudaré a sobrepasar este mal momento 
    Cuenta conmigo, permíteme ayudarte 
    Caíste pero con mi mano puedes levantarte 
    Habla conmigo ábreme tu corazón 
    No voy a dejarte solo en medio de esta situación 
    Aunque la tormenta nuble tu cielo 
    Siempre estaré dispuesto a darte consuelo 
    Quiero ser tu sol, la luz que te guía 
    Déjame devolverte la alegría 
    
    
    CORO. 
    Cuando la tormenta del dolor 
    Llueva en tu corazón (Llueva en tu corazón) 
    Yo seré tu sol 
    Yo seré tu abrigo 
    Yo seré tu sol 
    Yo siempre estaré contigo 
    Yo seré tu sol 
    
    RAP (REDIMI2) 
    Quiero ser el amigo en quien confías 
    Prometo estar contigo todos los días 
    Ja, te amo por si no lo sabias 
    Siempre te he cuidado por si no lo sabias 
    El tiempo de la tormenta será temporario 
    Y algunas veces el dolor será necesario 
    Y será para escalar otro peldaño 
    Pero no dejaré que el proceso te haga daño 
    Conmigo estas seguro 
    Te lo aseguro 
    Conozco tu pasado, tu presente, tu futuro 
    No te fallaré, seré tu amigo sincero 
    Seré tu sol, seré tu fiel compañero 
    
    CORO. 
    Cuando la tormenta del dolor 
    Llueva en tu corazón (Llueva en tu corazón) 
    Yo seré tu sol 
    Yo seré tu abrigo 
    Yo seré tu sol 
    Yo siempre estaré contigo 
    Yo seré tu sol 
    
    Cuando sientas frio te daré calor 
    En la soledad te cantaré una canción 
    Ya no habrán noches desiertas 
    Brillará otra vez tu estrella 
    Atrévete a mirarme a los ojos 
    Verás que yo nada escondo 
    Aunque suene imposible lo que prometeré 
    Puedes confiar que yo lo cumpliré 
    No te dejaré ni un segundo 
    Te devolveré lo que es tuyo 
    No lo que te ha robado el mundo 
    Todo lo que habías perdido 
    
    CORO. 
    Cuando la tormenta del dolor 
    Llueva en tu corazón (Llueva en tu corazón) 
    Yo seré tu sol 
    Yo seré tu abrigo 
    Yo seré tu sol 
    Yo siempre estaré contigo 
    Yo seré tu sol 
    Yo seré tu abrigo 
    Yo seré tu sol 
    Yo siempre estaré contigo 
    Yo seré tu sol 
    
    Esto es el Phenomenon Edition 
    Es Redimi2 men junto a Tercer Cielo 

* Letra basada en [http://jetlyrics.com/viewlyrics.php?id=1945724](http://jetlyrics.com/viewlyrics.php?id=1945724)
* [http://www.youtube.com/watch?v=4DMLrdRqXwo](http://www.youtube.com/watch?v=4DMLrdRqXwo)
* Agradecimiento a Tatiana