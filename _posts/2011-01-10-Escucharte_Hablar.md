---
layout: post
title: "Escucharte_Hablar"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Marcos Witt

<pre>
Quiero escuchar tu dulce voz
rompiendo el silencio en mi ser
se que me haría estremecer
me haría llorar o reir
y caería rendido ante Tí

Y no podría estar ante Tí
escuchandote hablar sin llorar como un niño
y pasaría el tiempo así
sin querer nada mas
nada mas que escucharte hablar

Quiero escuchar tu dulce voz
rompiendo el silencio en mi ser
se que me haría estremecer
me haría llorar o reir
y caería rendido ante Tí

//Y no podria estar ante Tí
escuchandote hablar sin llorar como un niño
y pasaria el tiempo asi
sin querer nada mas
nada mas que escucharte hablar//

</pre>

* Letra de http://www.musica.com/letras.asp?letra=842698
* Interpretación de Julissa con video: http://www.youtube.com/watch?v=mRIYiZtTbsA
* Interpretaci&#324;o de Marcos Witt http://www.youtube.com/watch?v=mRIYiZtTbsA
* Instrumental con guitarra http://www.youtube.com/watch?v=oaP1xZIrnPY
* Midi: http://www.destellodesugloria.org/destello/midis-cristianos/midis-cristianos-para-descargar/
