---
layout: post
title: La iglesia en los acuerdos de paz entre Gobierno y FARC-EP 2016
author: vtamara
categories:
- Prédica
image: "/assets/images/98-01.jpg"
tags: Prédica

---

1. Introducción

CEDECOL que agrupa a iglesias cristianas de colombia invitó a predicar sobre la
paz (ver {11}) pues como iglesia cuerpo de Cristo, celebramos el respeto a la
dignidad humana y a la vida que se manifiesta en el "Acuerdo sobre cese al fuego
y de hostilidades bilateral y definitivo y dejación de las armas entre el
gobierno nacional y las FARC-EP", firmados el pasado jueves 23 de Junio de 2016.

En la iglesia sabemos que la paz la traerá nuestro Señor Jesucristo con su
regreso, es la p
