---
layout: post
title: Alabarlo para Ser
author: vtamara
categories:
- Prédica
image: assets/images/home.jpg
tags: 

---
# 1. INTRODUCCIÓN

¿Quiénes han hecho el ejercicio de alabar al Señor con danza a solas?    (ver <https://fe.pasosdejesus.org/Adorandolo_con_danza_desde_el_corazon> )

Anhelamos que más personas participen en la alabanza y adoración durante el servicio, pero eso vendrá siendo fruto de un esfuerzo personal y del Santo Espíritu que nos guie y a quien oramos pidiendo para que nos ayude a alabar más y mejor.

Sigamos entendiendo para qué, por qué y cómo alabar al Señor, y aunque la alabanza puede ser más amplia que cantar y danza, pues debe ser adoración con una vida en obediencia y santidad, para este estudio nos concentramos en el canto.

# 2. TEXTO

Hay quienes sugieren Dios nos creó con el propósito de alabarlo (ver por ejemplo  {4}) pues eso pueden entenderse de Efesios 1:3-6

    3 Bendito sea el Dios y Padre de nuestro Señor Jesucristo,
    que nos bendijo con toda bendición espiritual en los lugares
    celestiales en Cristo,
    4 según nos escogió en él antes de la fundación del mundo,
    para que fuésemos santos y sin mancha delante de él,
    5 en amor habiéndonos predestinado para ser adoptados
    hijos suyos por medio de Jesucristo, según el puro afecto
    de su voluntad,
    6 para alabanza de la gloria de su gracia, con la cual nos hizo
    aceptos en el Amado,

No se trata de egoismo o presunción de Dios.  Él simplemente Es como lo dice Ex 3:14.  Dios no necesita aparentar ni presumir.

Personalmente he experimentado que Dios me creo por amor y me ha hecho libre y en esa libertad me he enamorado de Él y por eso lo alabo.   A las personas que alaban al Señor (por ejemplo a los niños del ministerio de danza) que les he preguntado coincidimos en sentir felicidad y gozo, por su santo Espíritu nos unimos más a Él durante la alabanza y en esa unión nos sentimos completos y creo que Dios también se complace en nosotros  y hasta nos honra.

# 3. ALABANZA Y ADORACIÓN CON CANTOS EN LOS CIELOS

Uno puede preguntarse si acaso en el cielo se alaba con cantos al Señor y según Apocalipsis 5:9 así es:

    9   y cantaban un nuevo cántico, diciendo: Digno eres de tomar el libro
    y de abrir sus sellos; porque tú fuiste inmolado, y con tu sangre nos
    has redimido para Dios, de todo linaje y lengua y pueblo y nación;
    10 y nos has hecho para nuestro Dios reyes y sacerdotes, y reinaremos
    sobre la tierra.

Entonces la alabanza es demasiado importante, de hecho es continúa en los cielos como dice Apocalipsis 4:8 "y no esaban día y noche de decir: Santo, santo, santo es el Señor Dios Todopoderoso, el que era, el que es y que ha de venir"

# 4. ALABANZA Y ADORACIÓN CON CANTOS EN LA TIERRA

En el Padre Nuestro decimos que se haga tu voluntad en la tierra así como en los cielos. Pues entonces pongamos nuestro granito de arena para lograrlo.

## 4.1 Alabanza continúa

De hecho la alabanza continúa es la invitación que nos hace el Salmo 34:

    1 Bendeciré a Jehová en todo tiempo;
    Su alabanza estará de continuo en mi boca.
    2 En Jehová se gloriará mi alma;
    Lo oirán los mansos, y se alegrarán.
    3 Engrandeced a Jehová conmigo,
    Y exaltemos a una su nombre.
    4 Busqué a Jehová, y él me oyó,
    Y me libró de todos mis temores.

Y es que cualquier oportunidad es buena para alabar al Señor con cantos.

### 4.1.1 Para dar gracias

Isaias 12:1-6

    1 En aquel día dirás: Cantaré a ti, oh Jehová; pues aunque te enojaste
    contra mí, tu indignación se apartó, y me has consolado.
    2 He aquí Dios es salvación mía; me aseguraré y no temeré;
    porque mi fortaleza y mi canción es JAH Jehová, quien ha sido
    salvación para mí.
    3 Sacaréis con gozo aguas de las fuentes de la salvación.
    4 Y diréis en aquel día: Cantad a Jehová, aclamad su nombre, haced
    célebres en los pueblos sus obras, recordad que su nombre es
    engrandecido.
    5 Cantad salmos a Jehová, porque ha hecho cosas magníficas;
    sea sabido esto por toda la tierra.
    6 Regocíjate y canta, oh moradora de Sion; porque grande es en
    medio de ti el Santo de Israel.

### 4.1.2. Para expresar nuestra alegría

Santiago 5:13

    ¿Está alguno entre vosotros afligido? Haga oración.
    ¿Está alguno alegre? Cante alabanzas.

### 4.1.3 En medio de la adversidad pues el Señor libra

Este principio lo ejemplifica Hechos 16 (como lo resalta {3}):

    23 Después de haberles azotado mucho, los echaron en la cárcel,
    mandando al carcelero que los guardase con seguridad.
    24 El cual, recibido este mandato, los metió en el calabozo de más
    adentro, y les aseguró los pies en el cepo.
    25 Pero a medianoche, orando Pablo y Silas, cantaban himnos a Dios;
    y los presos los oían.
    26 Entonces sobrevino de repente un gran terremoto, de tal manera
    que los cimientos de la cárcel se sacudían; y al instante se abrieron
    todas las puertas, y las cadenas de todos se soltaron.

## 4.2 Cántico Nuevos

Así como en Apo 5.9 se describen cánticos nuevos en los cielos en el Salmo 40 entre otros se nos enseña sobre esto en la tierra y como el Señor nos inspira:

    1 Pacientemente esperé a Jehová,
    Y se inclinó a mí, y oyó mi clamor.
    2 Y me hizo sacar del pozo de la desesperación, del lodo cenagoso;
    Puso mis pies sobre peña, y enderezó mis pasos.
    3 Puso luego en mi boca cántico nuevo, alabanza a nuestro Dios.
    Verán esto muchos, y temerán,
    Y confiarán en Jehová.

No despreciemos los géneros músicales ni formas de danza, sino pidámosle al Señor que nos enseñe como aprovecharlos para alabarlo a Él y conquistemos para Él

## 4.3 Instrumentos

En varios pasajes se habla de trompetas, por ejemplos los ángeles las emplean para anunciar
Apo 8:6

    6 Y los siete ángeles que tenían las siete trompetas se dispusieron a tocarlas.

Apo 15

    1 Vi en el cielo otra señal, grande y admirable:
    siete ángeles que tenían las siete plagas postreras;
    porque en ellas se consumaba la ira de Dios.
    2 Vi también como un mar de vidrio mezclado con fuego;
    y a los que habían alcanzado la victoria sobre la bestia
    y su imagen, y su marca y el número de su nombre,
    en pie sobre el mar de vidrio, con las arpas de Dios.
    3 Y cantan el cántico de Moisés siervo de Dios, y el cántico
    del Cordero, diciendo: Grandes y maravillosas son tus obras,
    Señor Dios Todopoderoso; justos y verdaderos son tus caminos,
    Rey de los santos.
    4 ¿Quién no te temerá, oh Señor, y glorificará tu nombre?
    pues sólo tú eres santo; por lo cual todas las naciones vendrán
    y te adorarán, porque tus juicios se han manifestado.

Y por ejemplo humanos emplearon bocinas para ayudar en el derrumbamiento de los
muros de Jericó como se describe en Josue 6:20

    Entonces el pueblo gritó, y los sacerdotes tocaron las bocinas;
    y aconteció que cuando el pueblo hubo oído el sonido de la bocina,
    gritó con gran vocerío, y el muro se derrumbó.
    El pueblo subió luego a la ciudad, cada uno derecho hacia adelante,
    y la tomaron.

Empleemos la alabanza para destruir fortalezas del enemigo.

# 5. CONCLUSIÓN Y ORACIÓN

* Necesitamos alabar al que Es para nosotros llegar a ser.  Él lo sabe y por eso nos ayuda a alabarlo.
* Señor gracias porque podemos alabarte y en esta desbordamos de alegría, rompemos las fortalezas del enemigo y nos das liberación.
* Gracias porque nos das la oportunidad de prepararnos para alabarte, ayudanos a prepararnos y a vencer los obstáculos que nos dificultan alabarte mejor, en el nombre de Jesús.

# 6. BIBLIOGRAFÍA

* {1} RV1960
* {2} Blue Letter Bible. [http://www.blueletterbible.org/Bible.cfm?b=Psa&c=34&v=1&t=KJV#conc/1](http://www.blueletterbible.org/Bible.cfm?b=Psa&c=34&v=1&t=KJV#conc/1)
* {3} Hay Liberacion En La Alabanza. Pablo Roman. 2011. [http://cristianobook.com/profiles/blogs/hay-liberacion-en-la-alabanza](http://cristianobook.com/profiles/blogs/hay-liberacion-en-la-alabanza)
* {4} La Alabanza como Llave para Liberar el Poder Milagroso de Yahweh. José Levi Machado. 2006. [http://www.adoradores.com/node/53](http://www.adoradores.com/node/53)
* {5} Alabanza y Adoración. [http://www.escuelasbiblicas.org/material/109-AlabanzaYAdoracion.pdf](http://www.escuelasbiblicas.org/material/109-AlabanzaYAdoracion.pdf)
* {6} Alabando al Señor en todo tiempo. [http://www.restorationnations.com/es/mostrar-articulos.asp?artID=57&pageSz=1045](http://www.restorationnations.com/es/mostrar-articulos.asp?artID=57&pageSz=1045)