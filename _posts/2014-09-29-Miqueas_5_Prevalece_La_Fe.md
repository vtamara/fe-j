---
layout: post
title: Miqueas 5 Prevalece La Fe
author: vtamara
categories:
- Prédica
image: "/assets/images/mapa-edom-moab-israel-juda650.png"
tags: Prédica

---
## 1. INTRODUCCIÓN

El Señor me regaló varios pasajes de manera casi simultanea en los que sentí una promesa de seguridad, entre otros Miqueas 5:4 y el Salmo 135:10 en los que veo como Él cuida a su pueblo.

    4 Y él estará, y apacentará con poder de Jehová, con grandeza del nombre de 
      Jehová su Dios; y morarán seguros, porque ahora será engrandecido hasta los 
      fines de la tierra.
    

Deseo compartir estas promesas, pero también el compromiso de confianza plena sólo en Dios y las terribles consecuencias de no poner la confianza exclusivamente en Él.

## 2. TEXTO

Miqueas 5:1-15

    1  Rodéate ahora de muros, hija de guerreros; nos han sitiado; con vara 
       herirán en la mejilla al juez de Israel.
    2 Pero tú, Belén Efrata, pequeña para estar entre las familias de Judá, de 
      ti me saldrá el que será Señor en Israel; y sus salidas son desde el 
      principio, desde los días de la eternidad.
    3 Pero los dejará hasta el tiempo que dé a luz la que ha de dar a luz; y el 
      resto de sus hermanos se volverá con los hijos de Israel.
    4 Y él estará, y apacentará con poder de Jehová, con grandeza del nombre de 
      Jehová su Dios; y morarán seguros, porque ahora será engrandecido hasta los 
      fines de la tierra.
    5 Y éste será nuestra paz. Cuando el asirio viniere a nuestra tierra, y 
      cuando hollare nuestros palacios, entonces levantaremos contra él siete 
      pastores, y ocho hombres principales;
    6 y devastarán la tierra de Asiria a espada, y con sus espadas la tierra de 
      Nimrod; y nos librará del asirio, cuando viniere contra nuestra tierra y 
      hollare nuestros confines.
    7 El remanente de Jacob será en medio de muchos pueblos como el rocío de 
      Jehová, como las lluvias sobre la hierba, las cuales no esperan a varón, ni 
      aguardan a hijos de hombres.
    8 Asimismo el remanente de Jacob será entre las naciones, en medio de muchos 
      pueblos, como el león entre las bestias de la selva, como el cachorro del 
      león entre las manadas de las ovejas, el cual si pasare, y hollare, y 
      arrebatare, no hay quien escape.
    9 Tu mano se alzará sobre tus enemigos, y todos tus adversarios serán 
      destruidos.
    10 Acontecerá en aquel día, dice Jehová, que haré matar tus caballos de en 
      medio de ti, y haré destruir tus carros.
    11 Haré también destruir las ciudades de tu tierra, y arruinaré todas tus 
      fortalezas.
    12 Asimismo destruiré de tu mano las hechicerías, y no se hallarán en ti 
      agoreros.
    13 Y haré destruir tus esculturas y tus imágenes de en medio de ti, y nunca 
      más te inclinarás a la obra de tus manos.
    14 Arrancaré tus imágenes de Asera de en medio de ti, y destruiré tus 
      ciudades;
    15 y con ira y con furor haré venganza en las naciones que no obedecieron.
    

## 3. CONTEXTO

Se han encontrado partes del libro de Miqueas entre los rollos de Qumram, datados hacía el año 70aC, por ejemplo el siguiente manuscrito en griego (de {6}), con de parte de los capítulos 4 y 5 (incluyendo palabras del texto que tenemos). ![](http://fe.pasosdejesus.org%2Fsites%2FFE.PASOSDEJESUS.ORG%2Fspages%2Frollo-qumram-miqueas.jpg)

Miqueas no es un libro fácil porque salta de la situación en la cual escribió a profecias y porque algunas de sus profecias se aplicarón para su tiempo y se aplican simultaneamente para el nuestro, era un mensaje para el pueblo de Dios y hoy lo sigue siendo un mensaje para la iglesia.

A lo largo del texto encontramos diversas profecias mesiánicas que nos recuerdan que los menonitas no requerimos armas, por ejemplo en Miqueas 4:2-3 dice:

    2 Vendrán muchas naciones, y dirán: Venid, y subamos al monte de Jehová, y a
      la casa del Dios de Jacob; y nos enseñará en sus caminos, y andaremos por
      sus veredas; porque de Sion saldrá la ley, y de Jerusalén la palabra de
      Jehová.
    3 Y él juzgará entre muchos pueblos, y corregirá a naciones poderosas hasta
      muy lejos; y martillarán sus espadas para azadones, y sus lanzas para
      hoces; no alzará espada nación contra nación, ni se ensayarán más para 
      la guerra.
    

También denuncia la maldad de ese momento y actual (idolatría, poderosos que roban y oprimen) y el castigo venidero por parte de Dios, por ejemplo 6:11-13:

    11 ¿Daré por inocente al que tiene balanza falsa y bolsa de pesas engañosas?
    12 Sus ricos se colmaron de rapiña, y sus moradores hablaron mentira, y su
       lengua es engañosa en su boca.
    13 Por eso yo también te hice enflaquecer hiriéndote, asolándote por tus 
      pecados.
    

Pero la fuerza con la que habla Miqueas procede de una situación muy degradada en su tiempo, la precisión de sus profecias, que aumentan su impacto en nuestra vida hoy, requieren que examinemos en más detalle a Miqueas y su tiempo.

Según el mismo libro que lleva su nombre (1:1), nació en Moreset en Judá (reino del Sur). ![](http://st-listas.20minutos.es/images/2010-12/263741/2771245_640px.jpg?1369202393)

![http://www.buenanueva.net/biblia/imag_leccion5/mapa-edom-moab-israel-juda650.png](http://www.buenanueva.net/biblia/imag_leccion5/mapa-edom-moab-israel-juda650.png)

Vivió en tiempos de los reyes Jotam, Acaz y Ezequías, es decir aproximadamente entre los años 740aC y 700aC aprox. (ver [Cronologia Reyes](https://web.archive.org/web/20160117175336/http://fe.pasosdejesus.org/?id=Cronologia+Reyes) de donde sacamos las siguientes fechas de **McFall**[?](https://web.archive.org/web/20160117175336/http://fe.pasosdejesus.org/?id=McFall)). Es contemporaneo con los profetas Oseas e Isaías, .

Hoy se considera a Moreset como un sitio a unos 30 kilometros al suroccidente de Jerusalen donde hay restos arqueológicos (ver {4}).

Los siguientes eventos se describen en 2 Cronicas 28, se dataron siguiendo datación de reyes de **McFall**[?](https://web.archive.org/web/20160117175336/http://fe.pasosdejesus.org/?id=McFall) y lo consignado alli:

| --- | --- | --- | --- | --- | --- |
| Año aC | Judea | Israel | Atacante | Que hizo | Prof. Miqueas |
| 740 |  | PEKA |  |  |  |
| 739 | JOTAM |  |  |  |  |
| 735 | ACAZ |  |  |  |  |
|  |  |  | Sirios | Derrotaron a Judea |  |
|  |  |  | Israel/Juda | Israel derrota a Judea, 120.000 muertos de Judea |  |
|  |  |  | Edom | Derrotaron a Judea |  |
|  |  |  | Filisteos | Les arrebatan varias ciudades |  |
|  |  |  | Asirios Tiglier | Terminan de saquear a Judea |  |
| 732 |  | OSEAS |  |  |  |
| 731 |  |  | Asirios | Derrotan a Israel y lo hacen tributario |  |
| 729 | EZEQUÍAS |  |  |  |  |
| 723 |  |  | Asirios | Acaban con Israel, llevan deportados a Asiria | Miqueas 1:6 |
| 715 |  | - | Asirios/Senaquerib | Toman todas las ciudades de Juda excepto Jerusalen, librada por Dios | Miqueas 5 |
| 697 | MANASÉS | - |  |  |  |
| 643 | AMÓN | - |  |  |  |
| 641 | JOSÍAS | - |  |  |  |
| 609 | JOACAZ | - |  |  |  |
| 609 | JOACIM | - |  |  |  |
| 608 | JOAQUIN | - |  |  |  |
| 600 |  | - | Caldeos | Destrucción de Jerusalén, Exilio en Babilonia | Miqueas 3:12, 4:10 |
| 597 | SEDEQUÍAS | - |  |  |  |
| \~0 | CRISTO | Samaritanos |  | Nace en Belén | Miqueas 5:2 |

El rey Acaz idolatró (con imágenes fundidas de Baal, quemó incienso a dioses falsos y sacrificó a sus hijos) por lo cual Dios permitió tantas desgracias en su tiempo, incluso pidió ayuda aa los asirios, y efectiviamente vinieron pero en vez de ayudarlo lo despojarón aún más.

Aún así Acaz obstinado cerró el templo de Jehova y quebró los utensilios del mismo, tras su muerte no fue enterrado con los reyes, y reino su hijo Ezequias, quien fue bien diferente a su papá, creyó en Jehova, restauró el templo y de hecho confió de verdad en Dios.

Impresiona ver como Miqueas es como un libro de historia pero al revés, en profecía.

## 4. ANÁLISIS

Nos detendremos un momento más en el rey Ezequías, pues con este interpretaremos Miqueas 5, siguiendo a {2} donde dice: "Se refiere a la liberación de Ezequías y su reino del poder de Senaquerib, quien lo invadió, en el tipo, pero bajo la sombra de esto, es una promesa de la seguridad de la iglesia-evangelio y de todoso los creyentes de los diseños e intentos de las potencias de la oscuridad, Satanas y todos sus instrumentos, el dragon y sus ángeles, que buscan devorar la iglesia del Primogénito y a todos los que pertenecen a esta"

Revisemos unos pocos apartes de Ezequias como los describió el profeta Isaias 36:1-37:38.

    36:1 Aconteció en el año catorce del rey Ezequías, que Senaquerib rey de
      Asiria subió contra todas las ciudades fortificadas de Judá, y las tomó.
    
    ...
    18 Mirad que no os engañe Ezequías diciendo: Jehová nos librará. ¿Acaso 
       libraron los dioses de las naciones cada uno su tierra de la mano del rey 
       de Asiria?
    
    19 ¿Dónde está el dios de Hamat y de Arfad? ¿Dónde está el dios de Sefarvaim? 
       ¿Libraron a Samaria de mi mano?
    
    ...
    37:1Aconteció, pues, que cuando el rey Ezequías oyó esto, rasgó sus vestidos,    
      y cubierto de cilicio vino a la casa de Jehová.
    ...
    6 Y les dijo Isaías: Diréis así a vuestro señor: Así ha dicho Jehová: No 
      temas por las palabras que has oído, con las cuales me han blasfemado los 
      siervos del rey de Asiria.
    7 He aquí que yo pondré en él un espíritu, y oirá un rumor, y volverá a su 
      tierra; y haré que en su tierra perezca a espada.
    ...
    15 Entonces Ezequías oró a Jehová, 
    ...
    21 Entonces Isaías hijo de Amoz envió a decir a Ezequías: Así ha dicho Jehová 
       Dios de Israel: Acerca de lo que me rogaste sobre Senaquerib rey de 
       Asiria,
    ...
    29 Porque contra mí te airaste, y tu arrogancia ha subido a mis oídos; 
       pondré, pues, mi garfio en tu nariz, y mi freno en tus labios, y te haré   
       volver por el camino por donde viniste.
    
    30 Y esto te será por señal: Comeréis este año lo que nace de suyo, y el año 
       segundo lo que nace de suyo; y el año tercero sembraréis y segaréis, y 
       plantaréis viñas, y comeréis su fruto.
    
    31 Y lo que hubiere quedado de la casa de Judá y lo que hubiere escapado, 
       volverá a echar raíz abajo, y dará fruto arriba.
    
    32 Porque de Jerusalén saldrá un remanente, y del monte de Sion los que 
       sesalven. El celo de Jehová de los ejércitos hará esto.
    
    33 Por tanto, así dice Jehová acerca del rey de Asiria: No entrará en esta 
       ciudad, ni arrojará saeta en ella; no vendrá delante de ella con escudo, 
       ni levantará contra ella baluarte.
    
    34 Por el camino que vino, volverá, y no entrará en esta ciudad, dice Jehová.
    
    35 Porque yo ampararé a esta ciudad para salvarla, por amor de mí mismo, y 
       por amor de David mi siervo.
    
    36 Y salió el ángel de Jehová y mató a ciento ochenta y cinco mil en el 
       campamento de los asirios; y cuando se levantaron por la mañana, he aquí 
       que todo era cuerpos de muertos.
    
    37 Entonces Senaquerib rey de Asiria se fue, e hizo su morada en Nínive.
    
    38 Y aconteció que mientras adoraba en el templo de Nisroc su dios, sus hijos 
       Adramelec y Sarezer le mataron a espada, y huyeron a la tierra de Ararat; 
       y reinó en su lugar Esar-hadón su hijo.
    

Con este bagaje histórico repasemos entonces Miqueas 5:

    1  Rodéate ahora de muros, hija de guerreros; nos han sitiado; con vara 
       herirán en la mejilla al juez de Israel.
    

Imaginemos que estamos en Jerusalen rodeada de asirios, que vociferan en contra de Dios y alardean porque han destruido a Israel y ahora todas las ciudades de Judea excepto Jerusalén. Incluso a la pequeña Belén, que Miqueas nos profetiza que sobrevirá para algo grandioso, en el tiempo de Dios:

    2 Pero tú, Belén Efrata, pequeña para estar entre las familias de Judá, de 
      ti me saldrá el que será Señor en Israel; y sus salidas son desde el 
      principio, desde los días de la eternidad.
    3 Pero los dejará hasta el tiempo que dé a luz la que ha de dar a luz; y el 
      resto de sus hermanos se volverá con los hijos de Israel.
    

¿En nuestra vida quienes son los asirios que nos rodean? ¿Donde está algo sencillo en mi vida de lo que no espero mucho, pero que Dios usará como uso a Belén?

Miqueas vivió 700 años antes de nuestro Señor Jesús, pero supo que Él era, es y será, y que es en Él en quien debemos confiar:

    4 Y él estará, y apacentará con poder de Jehová, con grandeza del nombre de 
      Jehová su Dios; y morarán seguros, porque ahora será engrandecido hasta los 
      fines de la tierra.
    

    5 Y éste será nuestra paz. Cuando el asirio viniere a nuestra tierra, y 
      cuando hollare nuestros palacios, entonces levantaremos contra él siete 
      pastores, y ocho hombres principales;
    

Con nuestra fe en Jesús no requerimos un ejercito para derrotar a Asiria, así como Ezequías oró y pidió ayuda a profetas y ancianos de su tiempo, basta que oremos, que pidamos oración en la iglesia y que usemos las armas espirituales que Dios nos dio (en ese tiempo no había Espíritu Santo para todo creyente, como si lo tenemos hoy).

    6 y devastarán la tierra de Asiria a espada, y con sus espadas la tierra de 
      Nimrod; y nos librará del asirio, cuando viniere contra nuestra tierra y 
      hollare nuestros confines.
    

La tierra de Nimrod es Babilonia, Miqueas sabía que Dios libraría a Israel primero de los Asirios, después les permitiría sobrevivir a los Babilonios, aún con su falta de fe. Pidamos al Señor fe para que Él de la batalla de los Asirios y mantegamosla para que después nos libre de los babilonios que seguiran hasta que nos encontremos cara a cara con el que nació en Belén.

    7 El remanente de Jacob será en medio de muchos pueblos como el rocío de 
      Jehová, como las lluvias sobre la hierba, las cuales no esperan a varón, ni 
      aguardan a hijos de hombres.
    8 Asimismo el remanente de Jacob será entre las naciones, en medio de muchos 
      pueblos, como el león entre las bestias de la selva, como el cachorro del 
      león entre las manadas de las ovejas, el cual si pasare, y hollare, y 
      arrebatare, no hay quien escape.
    9 Tu mano se alzará sobre tus enemigos, y todos tus adversarios serán 
      destruidos.
    

Tras las deportaciones por parte de Siria, Asiria y Babilonia quedaron israelitas esparcidos por toda la tierra. Hoy la tierra está llena de descendientes espirituales de Jacob, quienes creemos en Cristo y estás promesas de victoria son para nosotros: somo como lluvia que refresca la hierva enviada por Dios sin depender de hombres, somo valientes como leones en un mundo no cristiano con costumbres erradas, que vecemos dia dia. Destruimos a diario las tentaciones que son nuestros adversarios: el adulterio, la fornicación, las adicciones, la mentira, el chisme.

Pero esta victoria que Dios da tiene un precio, no puedo confiar en armas terrenales,:

    10 Acontecerá en aquel día, dice Jehová, que haré matar tus caballos de en 
      medio de ti, y haré destruir tus carros.
    11 Haré también destruir las ciudades de tu tierra, y arruinaré todas tus 
      fortalezas.
    

Ni en armas espirituales de Satanas, a las cuales debemos renunciar:

    12 Asimismo destruiré de tu mano las hechicerías, y no se hallarán en ti 
      agoreros.
    

Ni tener idolos, es decir, no podemos poner nuestra confianza en el dinero, ni la fama, ni en el poder, ni en cualquier tipo de éxito que el mundo nos ofrece:

    13 Y haré destruir tus esculturas y tus imágenes de en medio de ti, y nunca 
      más te inclinarás a la obra de tus manos.
    14 Arrancaré tus imágenes de Asera de en medio de ti, y destruiré tus 
      ciudades;
    

Y finalmente, no podemos confiarnos en que somos hijos de Dios porque "supuestamente" creímos un día y aceptamos a Jesús como Señor y Salvador, pues si realmente un día creímos, habrá obras conformes a nuestra fe y seremos obedientes a nuestros Señor y Salvado:

    15 y con ira y con furor haré venganza en las naciones que no obedecieron.
    

## 5. CONCLUSIÓN Y ORACIÓN

La promesa de seguridad, no necesariamente significa que a mi cuerpo o a mi familia o mis propiedades no les sobrevendrá infortunio --aunque en la misericordia de Dios puede ser-- Lo que si es es una promesa de seguridad plena en que me encontraré con Él al terminar esta prueba de la vida. Esos más de 50 cristianos asesinados por ISIS en meses recientes, que prefirieron la muerte a renunciar a su fe, están con Cristo hoy. Esa es mayor seguridad que tener mucho dinero. Por cierto hay decenas de miles de desplazados por ISIS en estos meses por quienes oramos para que encuentren puertas abiertas en paises donde los reciban o se mantengan en sus sitios protegidos por el Señor. También para la conversión de las personas de ISIS.

Esa seguridad se basa en una fe viva y obediente en Jesús, Santiago 2:17 dice "Así también la fe, si no tiene obras, es muerta en sí misma."

Esa seguridad la garantiza Cristo quien dará las batallas por nosotros, no creamos que vences en Cristo significa que Dios matará personas, pues tras Cristo es claro que la victoria es algo más grande.

Romanos 8:37-39 dice:

    37 Antes, en todas estas cosas somos más que vencedores por medio de aquel 
       que nos amó.
    38 Por lo cual estoy seguro de que ni la muerte, ni la vida, ni ángeles, ni 
       principados, ni potestades, ni lo presente, ni lo por venir,
    39 ni lo alto, ni lo profundo, ni ninguna otra cosa creada nos podrá separar  
       del amor de Dios, que es en Cristo Jesús Señor nuestro.
    

## 6. REFERENCIAS

* {2} Matthew Henry. Commentary on Micah 5. [http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009](http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009 "http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009")
* {4} [http://es.getamap.net/mapas/israel/yerushalayim/_telmoreshetgat/](http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009 "http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009")
* {5} [http://es.wikipedia.org/wiki/Salmanasar_V](http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009 "http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009")
* {6} [http://www.deadseascrolls.org.il/explore-the-archive/image/B-314652](http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009 "http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009")
* {7} [http://www.ecured.cu/index.php/Miqueas_](http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009 "http://www.blueletterbible.org/Comm/mhc/Mic/Mic_005.cfm?a=898009")(**Libro_de_la_Biblia**[?](https://web.archive.org/web/20160117175336/http://fe.pasosdejesus.org/?id=Libro_de_la_Biblia))
  * 

Gracias a Dios esta predica pudo darse el 28 de Septiembre de 2014 en la Iglesia Menonita de Suba.

Imagen de <http://www.buenanueva.net/biblia/imag_leccion5/mapa-edom-moab-israel-juda650.png>