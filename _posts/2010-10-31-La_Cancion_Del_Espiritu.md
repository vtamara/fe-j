---
layout: post
title: "La_Cancion_Del_Espiritu"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Hoy deja que el Señor te envuelva %%%
En su Espíritu de amor, %%%
satisfaga hoy tu alma y corazón %%%
Entrégale lo que te ata %%%
Y su espíritu vendrá, sobre ti  %%%
 y vida nueva te dará %%%

CRISTO OH! CRISTO %%%
 VEN Y LLÉNANOS %%%
CRISTO OH! CRISTO %%%
 LLÉNANOS DE TI %%%
 
Alzamos nuestra voz con gozo %%%
nuestra alabanza a ti,  %%%
con dulzura te entregamos %%%
nuestro ser. %%%
Entrega toda tu tristeza %%%
en el nombre del Señor %%%
y abundante vida hoy %%%
tendrás en Él %%%

CRISTO OH! CRISTO %%%
 VEN Y LLÉNANOS %%%
CRISTO OH! CRISTO %%%
 LLÉNANOS DE TI// %%%

Alzamos nuestra voz con gozo %%%
nuestra alabanza a ti, %%%
con dulzura te entregamos %%%
nuestro ser.%%%
Entrega toda tu tristeza %%%
en el nombre del Señor %%%
//y abundante vida hoy %%%
tendrás en Él// %%%

----


* Pista interpretada por Judith Santiago http://home.coqui.net/jsantiago1/LaCancionDelEspirituSanto.MP3
