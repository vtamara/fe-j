---
layout: post
title: Verdad con amor aunque me duela
author: vtamara
categories:
- Prédica
image: "/assets/images/cruzcol.jpg"
tags: Prédica

---
## 1. Introducción

En la secuencia sobre el sermón del Monte que estamos estudiando en la Iglesia Menonita de Suba, hace 15 días habíamos aprendido con el pastor Jaime Ramirez sobre los estragos del adulterio en el matrimonio (Mateo 5: 27-30) y sobre el divorcio (Mateo 5:31-32 ). Hoy veamos sobre verdad en Mateo 5:33-37

Deseo contar que durante la preparación de esta predica un día le dije a dos celadores del conjunto donde vivo que me habían robado 2 bicicletas en 2 oportunidades, mientras las tenía en el bicicletero y con candado. Estos versículos me hicieron volver frente a ellos tiempo después a disculparme y a decirles que en realidad no tenían candado (aunque si estaban dentro del bicicletero). ¡Que dificil es decir la verdad o reparar cuando hemos mentido!

## 2. Texto: Mateo 5:33-37

    33 Además habéis oído que fue dicho a los antiguos: No perjurarás, sino 
    cumplirás al Señor tus juramentos.
    34 Pero yo os digo: No juréis en ninguna manera; ni por el cielo, porque es 
    el trono de Dios;
    35 ni por la tierra, porque es el estrado de sus pies; ni por Jerusalén, 
    porque es la ciudad del gran Rey.
    36 Ni por tu cabeza jurarás, porque no puedes hacer blanco o negro un solo 
    cabello.
    37 Pero sea vuestro hablar: Sí, sí; no, no; porque lo que es más de esto, de 
    mal procede.
    

## 3. Contexto

Repasemos la estructura general del sermón del monte que habíamos revisado el 20.Ago.2017:

* 5:1-2 Introducción
  * 5:3-12 Bienaventuranzas - Promesas sobre el Reino con perfil
  * 5:13-16 Sal y Luz - Perfil adicional de quienes reciben esas promesas
    * 5:17-48 Aclaración de mandamientos antiguos mediante el amor: 5:17-20 Ley de Moises, 5:21-26 No sólo no matar, sino no enfadarse, 5:27-30 No adulterar en corazón, 5:31-32 Sobre el divorcio, 5:33-37 No jurar, sino decir la verdad, 5:38-42 No resistir a personas que nos hacen mal, 5:43-48 Amar a los enemigos
    * 6:1-7:12 Nuevos mandamientos - Relación correcta con Dios, bienes y otros: 6:1-4 Dar, 6:5-15 Orar, 6:16-18 Ayunar, 6:19-21 Tesoros en el cielo, 6:22-24 No se puede servir a Dios y al dinero, 6:25-34 No preocuparse por lo material sino por el Reino de Dios y su justicia, 7:1-6 No juzgar a otros, 7:7-11 Buscar, pedir y golpear la puerta, 7:12 La regla de oro
  * 7:13-23 Viviendo a la luz del juicio: Fe correcta: 7:13-14 Estrecha es la puerta a la salvación; Enseñanza correcta: 7:15-20 Reconocer a los falsos profetas por sus frutos; Que Jesús me conozca: 7:21-23 Jesús conoce a quienes hace la voluntad del Padre
* 7:24-29 Conclusión: Ser sabio escuchando a Jesús 7:24-29 El hombre sabio que construyó sobre la roca; Reconocer su autoridad 7:28-29 Termina y gente reconoce autoridad de Jesús

Las promesas de las bienaventuranzas son condicionadas, recordemos:

1. Para los pobres en espíritu,
2. Los que lloran,
3. Los mansos,
4. Los que tienen hambre y sed de justicia,
5. Los misericordiosos,
6. Los de limpio corazón,
7. Los pacificadores,
8. Los que padecen persecución por causa de la justicia,
9. Los vituperados, perseguidos y calumniados por causa de Jesús

Esas condiciones se profundizan y entienden en los mandamientos que el Señor nos había dado desde la época de Moisés y que Jesús aclara y complementa.

Por ejemplo no puedo decir que soy de corazón puro, si estoy adulterando en el corazón o mintiendo. El entender y cumplir los mandamientos bíblicos me van haciendo sal y luz para poder alcanzar las promesas de las bienaventuranzas, recordemolas:

1. Poseer el Reino de los Cielos
2. Recibir consolación
3. Heredar la tierra
4. Saciados de justicia
5. Recibir misericordia
6. Ver a Dios
7. Ser llamados hijos de Dios
8. Poseer el Reino de los Cielos
9. Recibir Galardón en el Cielo

Entonces el texto que estudiamos es una aclaración a la ley de Moises que nos va ayudando en la transformación que requerimos para cumplir las condiciones de las bienaventuranzas y recibir las promesas del Señor.

## 4. Análisis

El pasaje entero concuerda con **Santiago 5:12** _Pero sobre todo, hermanos míos, no juréis, ni por el cielo, ni por la tierra, ni por ningún otro juramento; sino que vuestro sí sea sí, y vuestro no sea no, para que no caigáis en condenación._

Casi todos los versículos concuerdan con textos del antiguo testamento. Analicemos uno a uno:

### 33.

**Éxodo 20:7** _No tomarás el nombre de Jehová tu Dios en vano; porque no dará por inocente Jehová al que tomare su nombre en vano._

**Levítico 19:12** _Y no jurareís falsamente por mi nombre, profanando así el nombre de tu Dios. Yo Jehová._

### 34-35

**Isaías 66:1** _Jehová dijo así: El cielo es mi trono, y la tierra estrado de mis pies; ¿dónde está la casa que me habréis de edificar, y dónde el lugar de mi reposo?_

**Salmos 48:2** _Hermosa provincia, el gozo de toda la tierra,Es el monte de Sion, a los lados del norte,La ciudad del gran Rey._

 Algunas corrientes religiosas como los Testigos de Jehova se niegan a realizar juramentos. Un versículo como este podría dar lugar a tal interpretación, sin embargo, veamos **Mateo 26:63-64** _Mas Jesús callaba. Entonces el sumo sacerdote le dijo: Te conjuro por el Dios viviente, que nos digas si eres tú el Cristo, el Hijo de Dios. Jesús le dijo: Tú lo has dicho; y además os digo, que desde ahora veréis al Hijo del Hombre sentado a la diestra del poder de Dios, y viniendo en las nubes del cielo._

 El mismo Señor Jesús respondió bajo juramento y Dios mismo ha jurado como se ve en **Hebreos 6:13** _Porque cuando Dios hizo la promesa a Abraham, no pudiendo jurar por otro mayor, juró por sí mismo. También hay pasajes del nuevo testamento en los que apostoles escriben con gran solemnidad similar a juramentos._

 Entonces nosotros bajo circunstancias especiales también podemos jurar, pero como indica Bruce citado en {4} “Se trata de una frase ...que no debe tomarse literalmente como una nueva ley, sino en el espíritu de inculcar tal amor por la verdad que en tanto nos corresponda no necesitemos jurar.”

 Respecto a la práctica de jurar por todo, los comentaristas Jamieson, Fausset y Brown {3} dicen: “La falta de verdad de nuestra naturaleza corrupta se muestra no sólo en la tendencia de desviarnos de la estricta verdad, sino en la disposición de sospechar que otros hacen lo mismo; y esto no se disminuye sino que se agrava con el hábito de confirmar lo que decimos con un juramento. Así corremos el riesgo de destruir toda reverencia al santo nombre de Dios, y a la verdad, que se destruye en nuestros corazones y nos ‘hacer caer en condenación’.”

### 36

 No concuerda con pasajes del antiguo testamento pero del nuevo con

**Mateo 6:27** _¿Y quién de vosotros podrá, por mucho que se afane, añadir a su estatura un codo?_

**Lucas 12:25** _¿Y quién de vosotros podrá con afanarse añadir a su estatura un codo?_

 Que nos recuerdan cuán vanos somos los seres humanos pues no podemos hacer blanco o negro nuestros cabellos, y ni siquiera hacer crecer nuestros huesos, así que ¿Cómo vamos a jurar por nosotros o nuestra cabeza? La estabilidad y eternidad de Dios todopoderoso es lo único que le da sentido a un juramento, pero como ya estudiamos no debe ser necesario pues siempre debemos decir la verdad.

### 37

 Dado que todo lo que hablemos debe ser como en juramento con la verdad, un versículo del antiguo testamento nos recuerda lo difícil --pero necesario-- que es decir la verdad:

**Salmo 15:4** _Aquel a cuyos ojos el vil es menospreciado, Pero honra a los que temen a Jehová. El que aun jurando en daño suyo, no por eso cambia;_

 Pues en ocasiones la verdad puede que no nos sea conveniente, pero no por eso debemos cambiarla, tal Salmo lo puso en práctica el Señor Jesucristo quien juró en daño suyo:

**Mateo 26:63-64** _Mas Jesús callaba. Entonces el sumo sacerdote le dijo: Te conjuro por el Dios viviente, que nos digas si eres tú el Cristo, el Hijo de Dios. Jesús le dijo: Tú lo has dicho; y además os digo, que desde ahora veréis al Hijo del Hombre sentado a la diestra del poder de Dios, y viniendo en las nubes del cielo._

 En todo lo que hacemos debemos recordar y anhelar la presencia de Dios, así todo lo que digamos es como un juramento ante Él y debe ser cierto. No debemos mentir, no sólo contra un prójimo sino aún en situaciones que nos parecen adversas contra nosotros mismos, tal como el Señor Jesús lo hizo.

 Al decir la verdad aún cuando nos pueda acarrear persecuciones (por causa de la justicia), vituperios y calumnias (por causa de nuestro Señor Jesucristo), sabremos que nuestro corazón queda limpio, que quedaremos habilitados para recibir más promesas de las bienaventuranzas con un galardon en el cielo, y la gracia de poderlo ver y compartir con Él en su Reino.

 Además de esta manera seremos testimonio para quienes no creen como dice Jamieson y colegas (ver {3}):

 “... Así que el reino de verdad y amor de Cristo se revela, en particular a aquellos que no son confiables, cuando el si o el no de los discípulos de Cristo llega a ser más confiable que el más solemne de los juramentos.”

## 5. Conclusión y oración

Del breve estudio que hemos hecho, entiendo que el Señor Jesucristo nos aclara el octavo mandamiento del antiguo testamento, es decir **Exodo 20:16** _No hablarás contra tu prójimo falso testimonio_

Hemos estudiado especialmente 4 puntos:

1. Debemos estar continuamente en la presencia de Dios, todo lo que hablamos es como un juramento por Él y debe ser cierto. Por esto no requerimos agregar “le juro que” o “por Dios que” a lo que decimos (pero si podemos jurar si somos requeridos por autoridades)
2. Debemos decir la verdad aún cuando nos duela o afecte negativamente como hizo el Señor Jesús --claro siempre con amor tanto cuando nos duela a nosotros como a otros.
3. Cuando decimos la verdad le damos a quienes no creen un testimonio del Señor Jesús quien es LA VERDAD. Que bueno que puedan confiar en que nuestro si es si y nuestro no es no.
4. Decir la verdad nos habilita entre otras para anhelar justicia e incluso para ser perseguidos por causa de la justicia y para tener un corazón puro. Con esto recibiremos las promesas de nuestro Señor de verlo, de ser saciados de justicia y de poseer con Él el Reino de los Cielos.

Oremos:

* Señor Jesús Tu has dicho ‘Yo soy el camino, la verdad y la vida’, que recordemos que Tu eres LA VERDAD y te hagamos honor en todo momento, aún y especialmente en situaciones en la que nos afecte negativamente, ayudanos a decir la verdad con amor.


* Gracias por recordarnos cuan leves somos pero cuan estable y cierta debe ser nuestra palabra por ser en Tu presencia, siempre cierta, que sea como el más solemne juramento por Tí porque Tu está continuamente presente en nuestras vidas.


* Señor que por nuestra honestidad y sinceridad puedan acercarse a Ti quienes aún no Te conocen, que anhelen la verdad y la encuentren en Ti Señor Jesucristo.


* Si aun hay personas que no conocen LA VERDAD, al Señor Jesucristo y quieren ser verdaderamente libres y recibirlo en sus vidas, les invitamos a hacer una oración de fe (favor ver <https://fe.pasosdejesus.org/Oracion_de_fe> ).

## 6. Referencias

* {1} Biblia. Reina Valera. 1960
* {2} [http://biblehub.com/matthew/5-36.htm](https://fe.pasosdejesus.org/?id=Oracion+de+fe "https://fe.pasosdejesus.org/?id=Oracion+de+fe")
* {3} The commentaries of Robert Jamieson, A.R. Fausset & David Brown, 1871. * [https://www.blueletterbible.org/commentaries/jfb/](https://fe.pasosdejesus.org/?id=Oracion+de+fe "https://fe.pasosdejesus.org/?id=Oracion+de+fe")
* {4} David Guzik Study Guides, 2017 [https://www.blueletterbible.org/Comm/guzik_david/StudyGuid](https://fe.pasosdejesus.org/?id=Oracion+de+fe "https://fe.pasosdejesus.org/?id=Oracion+de+fe") e2017-Mat/Mat-5.cfm?a=934037
* {5} R. L. Serralta Nogués. Cristo no prohibió todo tipo de juramento. [http://www.bibleserralta.com/CristoJuramentos.html](https://fe.pasosdejesus.org/?id=Oracion+de+fe "https://fe.pasosdejesus.org/?id=Oracion+de+fe") (consultado 2017)

***

Este escrito de dominio público también está disponible en: [https://docs.google.com/document/d/1VsJBBzoM2WBsM8M1cKr_i8__lT6BmBy9vDCiBCIUiP0/edit?usp=sharing](https://docs.google.com/document/d/1VsJBBzoM2WBsM8M1cKr_i8__lT6BmBy9vDCiBCIUiP0/edit?usp=sharing)

---

Dibujo de <https://misdibujoscristianos.blogspot.com/2012/03/crucifixion.html>