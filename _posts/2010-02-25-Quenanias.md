---
layout: post
title: "Quenanias"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Nombre

Quenanías. Dios ha establecido {2}.

Hebreo {2}: 

Jefe de los levitas en tiempos de David. Dirigió a los músicos que acompañaron a David para  trasladar el arca a Jerusalén (1 Cr. 15:22, 27) y también tubo un cargo judicial (26:29). {2}

##Datos sobre Quenanías en la Biblía

! Cr 15:22 Quenanías, principal de los levitas en la música, fue puesto para dirigir el canto, porque era entendido en ello.

1 Cr 15:27 Y David iba vestido de lino fino, y también todos los levitas que llevaban el arca, y asimismo los cantores; y Quenanías era maestro de canto entre los cantores. Llevaba también David sobre sí un efod de lino. {1}


##Otros

* Ministerio de Alabanza http://www.chenaniah.org

##Referencias

* {1} Biblia Reina Valera 1960. http://www.biblegateway.com/passage/?search=1 Chronicles 15:27&version=RVR1960
* {2}  http://www.wikicristiano.org/diccionario-biblico/3733/quenanías/
