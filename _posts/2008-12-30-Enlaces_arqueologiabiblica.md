---
layout: post
title: "Enlaces_arqueologiabiblica"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
* http://www.archaeologynews.org/
* The Bible in its World: The Bible & Archaeology Today Kenneth A. Kitchen. 1977. http://www.biblicalstudies.org.uk/book_bibleinitsworld.html
* Associates for Biblical Research. http://www.biblearchaeology.org/ 
* La Biblia y la Arqueología. Daniel Alejandro Flores. http://labibliaylaarqueologia.blogspot.com/
* Nueva Cronología propuesta por David Rohl. http://tech.groups.yahoo.com/group/NewChronology/
* Old Testament Studies - Reliability and Chronology http://www.oldtestamentstudies.net Incluye buenas críticas a Nueva Cronología, compara con cronología tradicional y hace propuestas
* http://girona.mforos.com/1098670/5287339-arqueologia-geografia-geologia-codices/
