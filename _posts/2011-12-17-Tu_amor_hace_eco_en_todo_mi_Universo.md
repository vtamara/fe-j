---
layout: post
title: "Tu_amor_hace_eco_en_todo_mi_Universo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Rojo

<pre>
En la cruz se lavó 
todo lo que mi alma tanto cargó
Fuiste tú, mi Jesús 
quien pagó el precio de mi perdón

CORO
Y no hay palabra que quiera hablar
Ni hay canción que se me antoje cantar
Si no es para ti
No hay razón para poder existir

Eres el aire, la lluvia
La risa de los niños
La fuerza, la calma
La guía en el camino
Misterios, estrellas
Galaxias y sorpresas que llenan

Eres la noche, el día
La luz que me ilumina
Destino, pasado
Tesoro más preciado
La llave, la puerta
La voz que me alimenta y me alienta
Tu amor hace eco en todo mi universo

Eres tú la razón 
por la cual perdido ya no estoy
Mi dolor se marchó 
por la fuerza de tu abrazo y calor

//CORO//
</pre>

* Video: 
