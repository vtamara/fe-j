---
layout: post
title: "Amor"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

| Griego | Español | Significado | Versículos | Cuando | Palabras | Visible del cuerpo | Gestos y acciones corporales | Acciones |
| Filos | Filial | Amor de hermanos, amor de amigos | 1 Sam 18:1-5 |  | | | | |
| Eros | Erótico | Amor entre esposos, sexual | Cantares 1:9-17 |  | | | | |
| Agape | Cristiano | Amor que Dios nos da, que debemos dar a Dios y al prójimo | 1 Cor 13 |  | | | | |
