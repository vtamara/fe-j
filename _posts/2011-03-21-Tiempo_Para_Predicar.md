---
layout: post
title: "Tiempo_Para_Predicar"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. ORACIÓN
Señor tu nos has enviado a predicar, ayudanos para lograrlo.

##2. INTRODUCCIÓN. CONTEXTOS

[Pablo], carta a los [Romanos] y Roma en el tiempo que Pablo escribió esa carta.

Claudio {9} fue emperador del 41 al 54, a quien le sucedió Nerón.    Claudio llegó a ser emperador puesto por la guardia pretoriana que había matado a su sobrino Calígula.   Murió envenenado, se cree que por parte de su propia esposa Agripina, quien tal vez buscaba que su hijo Nerón llegara más pronto al poder.  Claudio firmó un decreto que ordenaba salir a los Judíos de Jerusalén por unos años (Hechos 18:2).  Claudio era fanático de las luchas de gladiadores y de mandar a ejecutar, su hijo Nerón {8} heredo aumentados esos "gustos", llego a hacer pelear en el circo romano a 400 senadores y 200 caballeros.  Las luchas de gladiadores eran un frenesí para el pueblo romano, sobre ellas Seneca escribió (según {7}):  "Estos peleadores de mediodía salen sin ningún tipo de armadura, se exponen sin defensa a los golpes, y ninguno golpea en vano... Por la mañana echan los hombres a los leones; al mediodía se los echan a los espectadores. La multitud exige que el victorioso que ha matado a sus contrincantes se encare al hombre que, a su vez, lo matará, y el último victorioso lo reservan para otra masacre...  Al hombre ... lo matan por diversión y risas."  Apenas Nerón llego al poder deidificaron a Claudio, en el año 66 Roma fue incendiada, algunos dicen que fue Nerón mismo, pero él culpó a los cristianos e inició una cruel persecución.


##3. MENSAJE. Romanos 10:8-17

<pre>
    8 Mas ¿qué dice? Cerca de ti está la palabra, en tu boca y en tu corazón. Esta es la palabra de fe que predicamos:
    9 que si confesares con tu boca que Jesús es el Señor, y creyeres en tu corazón que Dios le levantó de los muertos, serás salvo.
    10 Porque con el corazón se cree para justicia, pero con la boca se confiesa para salvación.
    11 Pues la Escritura dice: Todo aquel que en él creyere, no será avergonzado.
    12 Porque no hay diferencia entre judío y griego, pues el mismo que es Señor de todos, es rico para con todos los que le invocan;
    13 porque todo aquel que invocare el nombre del Señor, será salvo.
    14 ¿Cómo, pues, invocarán a aquel en el cual no han creído? ¿Y cómo creerán en aquel de quien no han oído? ¿Y cómo oirán sin haber quien les predique?
    15 ¿Y cómo predicarán si no fueren enviados? Como está escrito: ##Cuán hermosos son los pies de los que anuncian la paz, de los que anuncian buenas nuevas!
    16 Mas no todos obedecieron al evangelio; pues Isaías dice: Señor, ¿quién ha creído a nuestro anuncio?
    17 Así que la fe es por el oír, y el oír, por la palabra de Dios.
</pre>

Se trata de unas palabras para cristianos motivando a predicar.  Lo que me motiva a tratar este tema especialmente es:
# Un sueño en el que veía muy esquematizado que unos familiares tenían un accidente.  Aunque les he hablado del Señor, estoy motivado y he estado encontrando gracias a Dios nuevas formas de hablarles, buscando que también proclamen a Jesucristo como Señor ---aún cuando no están en la misma iglesia que yo.
# La situación en Japón, donde además de terremoto, tsunami, crisis radioactiva, un volcán hizo erupción.   ¿Cuantas de esas víctimas proclamaban a Jesucristo como Señor?

Hay algunos puntos relacionados con el mensaje que sintetizo en el siguiente diagrama:
[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/images/circulo.png]

##4. CUESTIONAMIENTO

!4.1 ¿Qué aprendemos o confirmamos de Dios a partir del relato?

* Es cercano 10:8. No es necesario buscarlo en el cielo ni en el hades sino en el corazón (10:6-10:7).
* Es Jesucristo mismo 10:9 (claro trino Rom 8:11).
* Es un Dios de paz, amoroso e incluyente. Rom 10:12.  Sufrió por mi.
* Además es omnipotente Señor de la historia como veremos a continuación.

!4.2 Creer termina siendo fácil

* El pecado del hombre lo separó de Dios, quien es Santo, no se mezcla con el mal y la paga del pecado es la muerte Rom 6:23.
* Por la fe de Noe, Abraham, Moisés y otros, Dios dio promesa y ley para que el justo por la ley viviera (Hab 2:4).  Dio la ley al pueblo de la promesa (Lev 18:5).   Sin embargo no fue posible, pues de acuerdo a Pablo ni uno era justo (Rom 3:10).
* En el plan de Dios también estaba previsto el Señor Jesús  que con su cumplimiento perfecto de la ley, con su sufrimiento y muerte justificara por la fe (Rom 5:8).   
* Además Dios amplió por su gracia y misericordia la posibilidad más allá de los descendientes de la promesa, i.e. el pueblo de Israel, dando oportunidad a los gentiles de tener "nacionalidad celestial" (Rom 10:12).

Entonces por el amor y misericordia del Señor hoy tenemos:
# La posibilidad de acercarnos a Dios aún con el pecado presente en el mundo.
# La posibilidad de ser justificados, es decir, aparecer como sin mancha ante los ojos de Dios, no por nuestro cumplimiento de la ley --que no logramos hacer-- sino por la sangre de nuestro Señor Jesucristo que continuamente nos limpia (basta pedirle).
# La posibilidad de ser del pueblo de Dios, hijos suyos, aún cuando geneticamente no seamos descendientes del pueblo que Él había escogido en la antigüedad.

Es decir ya no hay barreras por el pecado, ni por la ley, ni por la genética, la gracia del Señor se extendió por su misericordia, con sólo una condición: CREER.   Dios ha hecho posible lo imposible y no sólo posible, sino fácil.

!4.3 ¿Qué implica creer?

Pero si creo en el Señor Jesucristo, obviamente creo lo que Él dice, y Él da mandamientos, el central: amar a Dios sobre todo y al prójimo como a mi mismo, pero también otros (amarnos como Él nos ha amado), y en particular ha querido darnos oportunidad de entrar como agentes activos dentro del plan de salvación envíandonos como se consigna en Marcos 16:15-18

!4.4 Mi tarea de predicar

Aunque es más fácil que cumplir la ley, tampoco es trivial, el versículo 16 lo confirma y posiblemente nuestra experiencia.  Pero que no todos escuchen o crean (Rom 10:16), no nos debe desanimar.   La semilla queda plantada cuando predicamos, y con Espíritu Santo y un poquito de voluntad de algunos que escuchen dará fruto y ese fruto lo veremos.

No soy el más experto en este tema, pero creo que el Señor me ha movido a predicar sobre este tema y a compartir unos puntos que nos pueden ayudar:

* El amor: La predicación tiene frutos es gracias al Espíritu Santo, pero creo que lo que nos impulsa a prepararnos y a atrevernos a hacerlo es el amor (que es el mismo Dios).  Entonces amemos con profundidad a esta humanidad, a la naturaleza, a la creación entera de Dios.
* Discernimiento: Él dijo que no diéramos lo sagrado a cerdos, ni las perlas a perros.  Le pedimos entonces al Señor que nos de discernimiento de a quien predicarle, cuando y cómo hacerlo.  Con seguridad nuestra prédica debe salir de la iglesia, podría ser a judíos (de pronto a personas de una iglesia anterior en la que estuvimos) o a gentiles (personas que no han ido a iglesias o que no nos imaginamos).   Sin duda no podemos evadir nuestras familias, no necesariamente veremos frutos inmediatos o mediatos, pero es por nuestra historia a quienes también debemos intentar darles nuestro amor, sin estancarnos sólo en ell@s.
* Vida de testimonio.  Esfuérzate y se valiente (Josue 1:9).  No podemos ser santos, pero tenemos en el corazón al que si puede santificar.  Y además al que me quiere levantar cuando caigo, tantas veces como sea necesario.  El sólo espera, que yo quiera, pues me hizo libre pero me ama como nadie.
* Aprovechar las crisis.  No se trata de aprovecharse del dolor, pero en situaciones extremas, donde se acaba la ciencia humana, las fuerzas humanas, las ideas humanas, allí sigue estando la oportunidad de Dios y eso debemos proclamarlo con valor.
* Prepararse para la tarea.  Hay muchos espacios.   Es importante la teoría y la práctica.
* Los esfuerzos humildes con la ayuda de Dios dan fruto.  No despreciemos ni dejemos de dar una palabra de la Biblia o que el Señor nos ponga. Oremos, Él sabe lo sencillos que somos y nos respalda.
* Ahora escuchemos consejos de un experto en predicación: Pablo, quien llevó el evangelio a los gentiles y a quien esperamos encontrarnos en el Reino de Dios para decirle gracias. Romanos 12 dice:
** Renovar nuestro entendimiento para entender voluntad de Dios
** Humildad sin menospreciarme
** Usar dones que el Señor nos ha dado
** Amarnos unos a otros
** Diligentes
** Gozosos, constantes en oración
** Compartir para cubrir necesidades de herman@s, hospitalarios
** Bendecir a perseguidores, no maldecir.
** Sensible al dolor de los demás
** No vengativos. No ser vencido de lo malo sino vencer con el bien el mal. Rom 12.21
** Obedeciendo autoridades que dan ordenes acordes a ley de Dios (el mismo Pablo se resistió a autoridades judias que pretendían matarlo).
* Ahora los que creemos somos salvos, ya no hay condenación (Rom 8:1) y por el contrario limpieza continúa cuando andamos conforme al Espíritu y pidiéndole al Señor.


##5. MÁS MOTIVACIÓN

* El amor del Señor es eterno. Es inquebrantable nada me separará de su amor. Rom 8:34-39
* Hoy para nosotros aquí en Colombia predicar es más fácil que para Pablo y los cristianos de su tiempo, que predicaron en una sociedad amante de la muerte, que exigían ver morir a gladiadores para reír, donde sus lideres les enseñaba a ser coléricos, a engañarse, a usurpar, a mentir, a creerse dioses, a desmedirse en el poder, a denigrar, a despreciar la vida sagrada.   Bueno hay paises donde hoy sigue siendo muy difícil predicar, donde hay leyes contra cristianos (en paises de Asia y África como China, Vietnam, Sudan, India, por ejemplo donde pastores y misioneros están dando su vida para hacer crecer la semilla del evangelio, ver {10}).
* El Señor está obrando, aunque creo que Dios corrige, yo no me atrevo a decir que lo que ha ocurrido en Japón sea una corrección del Señor, no lo se. Pero he visto que Él aprovecha la destrucción y hasta situaciones originadas en pecado para llamar, para fortalecer, para dar confianza.

!5.1 Japón 

Según {3} el principal sistema religioso en Japón hoy es una mezcla de Shinto y Budismo Japones. Shinto es un politeísmo con énfasis en lugares sagrados, el budismo no es teísta y habla de iluminados como Budhha y un estado Nirvana que se puede alcanzar para escapar del sufrimiento en el ciclo de reencarnaciones aún cuando no haya un alma o espíritu inmortal.  Pero ni estas religiones no teístas, ni monoteístas son las más practicadas en Japón. Según resultados de encuestas,  por citas de (Zuckerman:2007), de acuerdo a Norris e Inglehart (2004) 65% de los habitantes de Japón no creen en Dios y de acuerdo a Demerath (2001:138), 64% no creen en Dios y 55% no cree en Buddha.

Con respecto a comercio sexual un informe de la ONU citado por (BBC:2007) dice que los principales destinos de tráfico humano (calculado en 2.5 millones de personas) son: Tailandia, Japón, Israel, Bélgica, Holanda, Alemania, Italia, Turquía y Estados Unidos.   "Muchas de estas mujeres y niñas jóvenes son forzadas  a trabajar en la industria del sexo, mientras que otras son forzadas por hombres trabajar como operadoras en condiciones peligrosas con poco pago o sin pago, dijo la agencia de Naciones Unidas."

En una lista de paises por tasa de suicidio {5}, los 5 primeros paises son: Lituania (31.5 por cada 100.000), Corea del Sur (31),  Kazakstán (26.9), Belarus (25.3) y Japón (24.5).  Colombia está en el puesto 73 con 4.9 suicidios por cada 100.000 habitantes.   Japón tiene la tasa más alta de suicidios femeninos (13.7).  Por cierto los paises donde no se reportan suicidios en sus estadísticas recientes son: Egipto, Honduras, San Cristobal y Nieves, Antigua y Barbuda y Haití.

En Japón comenzó la misión menonita hacía 1950, de los diversos esfuerzos hay 2 conferencias de iglesias menonitas y una hermandad ecuménica en Tokyo
* Una conferencia en Kyushu con 15 iglesias
* Una conferencia en Hokkaido con 19 iglesias
* La hermandad de Tokyo que consta de 5 igleisas menonitas y de hermandad en Cristo.
Se calcula que hay unos 3000 menonitas en Japón, según {5} las 4 familias misioneras de la Red de Misiones Menonitas, no han reportado lesiones tras el tsunami.

El miércoles 16 de Marzo estuvimos orando en el seminario por Japón, las víctimas, los sobrevivientes y que el Señor obrará pues las fuerzas humanas no alcanzaban.    Hay muchas iglesias y personas orando (hasta actores y actrices y cantantes).

En un país donde la mayoría no cree en Dios o cree en falsos dioses, donde no creen en la inmortalidad del espíritu, para mi es increíble que el viernes pasado 18 de Marzo acordaran como país entero, guardar un minuto de silencio por las víctimas.  Creo que le están dando la oportunidad al Señor de hablarles, que quieren escucharlo y aprender.  

Creo que ahora los cristianos tenemos una oportunidad de hablar con valor, el mundo no tiene respuestas, pero las requiere y nosotros tenemos respuesta y ahora requerimos valor para proclamar nuestra respuesta: El Señor jesús.


##6. REFUTACIÓN

* Jesús es Dios Rom 10:9 y es trino Rom 8:11
* No se complace en la maldad como dice el Salmo 5:4.  El Señor a quien ama corrige (Prov 3:12), pero creo que a Él sufre con nuestro sufrimiento aún cuando sea por nuestro pecado o indiferencia o insensibilidad u orgullo.

##7.  CONCLUSIÓN

!7.1 ACTIO

* Tarea: ¿Cómo predicaré esta semana?  ¿A quien puedo invitar a la iglesia para que venga el próximo domingo?  ¿De que forma nueva puedo invitar?
* ¿Ya conozco la oración de fe?  Si el Señor mueve a una persona a recibir a Cristo en su corazón, y lo acompaño a hacer la oración ¿Qué elementos debo tener en cuenta?
* ¿Siento la paz del Señor en mi vida? ¿Qué problemas tengo con otr@s? Orar por ellos y ellas.
* ¿Me estoy disponiendo para que el Señor muestre sus señales en mi vida? ¿Quiero ver su gloria en sanidad, liberación, en nuevas lenguas, en tomar serpientes o en sobrevivir?

!7.2 ORACIÓN
 
Declaro que no hay condenación para los que creemos en ti, que tu me ayudas y me levantas cuando caigo, que tu me oyes, que me amas, y amas mi voz, que mis problemas los conoces y los haces tuyos porque me quieres ver bien, que tu me corriges porque me amas. Que en ti confío y es sólo en ti que mi confianza tiene respaldo completo. 

Señor ayudanos a predicar tu palabra en este tiempo, enseñanos, ayudanos, preparanos, en teoría y práctica.  Que tenga fruto, que veamos tu gloria cuando lo hagamos. Que sintamos tu respaldo. Que nos esforcemos por vivir en santidad.


##8. BIBLIOGRAFÍA

* {1} Biblia. Reina Valera. 1960
* {2} Phil Zuckerman. Atheism: Contemporany Rates and Patterns.  Cambridge Companion Atheism. University of Cambridge Press. 2007
* {3} http://es.wikipedia.org/wiki/Religión_en_Japón
* {4} http://news.bbc.co.uk/2/hi/6497799.stm   BBC. 2007
* {5} http://en.wikipedia.org/wiki/List_of_countries_by_suicide_rate
* {6} http://www.mennonitemission.net/Stories/News/Pages/JapanEarthquake.aspx
* {7} http://es.wikipedia.org/wiki/Gladiador
* {8} http://es.wikipedia.org/wiki/Ner%C3%B3n
* {9} http://es.wikipedia.org/wiki/Claudio
* {10} http://www.worthynews.org
* {11} http://www.stempublishing.com/authors/kelly/2Newtest/ROM_PT2.html
