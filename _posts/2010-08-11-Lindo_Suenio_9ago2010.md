---
layout: post
title: "Lindo_Suenio_9ago2010"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En la noche del 9 al 10 de Agosto de 2010 soñé con el Señor Jesús, estabamos sentados frente a frente, como en un bus. Él me miraba y yo lo miraba feliz.  Ocasionalmente otras personas aparecían.  Él no estaba del todo cómodo por el pecado mio y de otros alrededor, pero se quedaba permitiendo que lo mirara.    Un dulce sueño  con la paz y gozo que llena cuando en el sueño se siente, o se nombra a Dios. 

"Maravilloso es el Señor Jesús", ver http://www.youtube.com/watch?v=5bPf3Xol2ow
