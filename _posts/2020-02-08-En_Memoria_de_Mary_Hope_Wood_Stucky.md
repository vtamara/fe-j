---
title: En memoria de Mary Hope Wood Stucky
author: vtamara
image: "/assets/images/erfkiziw4aadeth.jpeg"
categories:
- Obituario

---
(Escrito compartido en iglesia Menonita en Colombia con motivo de la velación de la señora María el 9.Feb.2020, posiblemente escrito por Pedro Stucky).

MARY HOPE WOOD STUCKY (la Sra. María), nació en Lansing, Michigan, EEUU, el 26 de diciembre, 1916, a Frank E. Wood y Helen Esselstyn Wood, la segunda de cuatro hijos.

Cuando tenía 6 años su familia atravesó Estados Unidos en un automovil marca Reo, modelo 1916 y se instalaron en Monrovia, California. Allí se crió, hizo parte la Iglesia Presbiteriana, y aprendió a tocar el piano y la flauta traversa. Finalizado el bachillerato, estudio en Wheaton College, una universidad cristiana muy orientada hacia misiones. Fue tenista en el equipo de la Universidad.

Después de termina la Universidad fue a Biblical Seminary of New York, donde obtuvo una maestría en educación cristiana. Posterior a ello, enseñó un año en un colegio presbiteriano para indígenas norteamericanos en Nuevo México.

En el Seminario de Nueva York conoció a Gerardo Stucky con quien después se casó en 1943. Atendieron el llamado de Dios de servirle en Colombia, a donde llegaron en 1945 para iniciar la obra de la iglesia Menonita en nuestro país. Judith ya había nacido y en Colombia nacieron Pedro, Pablo y Timoteo. Hoy ya son ocho nietos, una nieta, dos bisnietas y cinco bisnietos.

Pero realmente son muchos más, porque durante 20 años María sirvió al Señor en Cachipay, Cundinamarca, junto con Gerardo y otras misionaras y misioneros. Allí fue mamá y mentora para centenares de niñas y niños, muchos que venían de Agua de Dios o huyendo de la violencia en otras partes del país. Con su enseñanza y ejemplo les mostró el camino de Jesucristo. Los orientaba en su conducta, y se recuerda como les asignaba los "oficios" que cada chic@ debía asumir durante la semana. Por la noche al acostarlos hacia devocionales con ell@s y en la capilla tocaba el piano mientras Gerardo dirigia el culto.

Después de 20 años en Cachipay, María y Gerardo volvieron a los EEUU donde sirvieron en el pastorado de la Iglesia Menonita de Berne, Indiana, por ocho años. Durante este tiempo, María también hizo estudios en capellanía y consejería para pacientes en hospitales.

En mayo de 1973 María y Gerardo regresaron a Colombia y comenzaron un grupo de estudio bíblico en su apartamento en Chapinero. María los acogía con amabilidad. El grupo creció y posteriormente se organizó como iglesia --hoy la Iglesia Menonita de Teusaquillo.

En los años 70, con Oliva de Bastidas fundó el Hogar Cristiano La Paz, un hogar menonita para personas de la tercer edad, para dar respuesta a una necesidad sentida en el país.

Después de la muerte de Gerardo por cáncer en 1988, María optó por quedarse en Bogotá, permaneciendo activa en la Iglesia Menonita de Teusaquillo, hasta tanto sus fuerzas le alcanzaron. Fue voluntario en el Instituto Nacional de Cancerología. También sirvió en el Comité de Educación que orientaba los colegios de la Iglesia Menonita.

Al cumplir 103 años y 44 días, falleció el sábado 8 de febrero en horas de la mañana. Damos gracias a Dios por la vida de la Sra. María, por su ejemplo de entrega a la misión de Dios desde muy joven, y por como Dios la uso en servicio a centenares de personas en Colombia, un país que para ella inicialmente fue extranjero pero se convirtió en su hogar.

***

Imaǵen de [https://twitter.com/cpt_intl/status/1229874269968048136/photo/1](https://twitter.com/cpt_intl/status/1229874269968048136/photo/1 "https://twitter.com/cpt_intl/status/1229874269968048136/photo/1")