---
layout: post
categories:
- Prédica
title: Relaciones en familia Imagen de la Trinidad
author: vtamara
image: "/assets/images/285111.png"

---
11\.Jun.2017.  Iglesia Menonita de Suba, Caminos de Esperanza

## 1. INTRODUCCIÓN

En el contexto del tema del mes, la familia, recordemos primero algunos detalles de la prédica anterior dada por Jaime (incluía 1 Tim 3:32 y 1 Tim 3:11)

* Ojos del esposo solo para la esposa
* Debe ser sacerdote de su casa y obispo irreprensible con todas las características del diacono
* Hogar es sitio de protección para hijos

Parte de mi testimonio es que ahora estoy en un segundo matrimonio, que en el primero no estaba en los caminos del Señor y lo hice en mis fuerzas con graves consecuencias. En el segundo busqué en oración y esperé y creo que el Señor me respondió con esposa.

## 2. TEXTO

Mateo 19:1-12

    1 Aconteció que cuando Jesús terminó estas palabras, se alejó de Galilea, y 
      fue a las regiones de Judea al otro lado del Jordán.
    2 Y le siguieron grandes multitudes, y los sanó allí.
    3 Entonces vinieron a él los fariseos, tentándole y diciéndole: ¿Es lícito al 
      hombre repudiar a su mujer por cualquier causa?
    4 Él, respondiendo, les dijo: ¿No habéis leído que el que los hizo al 
      principio, varón y hembra los hizo,
    5 y dijo: Por esto el hombre dejará padre y madre, y se unirá a su mujer, y 
      los dos serán una sola carne?
    6 Así que no son ya más dos, sino una sola carne; por tanto, lo que Dios 
      juntó, no lo separe el hombre.
    7 Le dijeron: ¿Por qué, pues, mandó Moisés dar carta de divorcio, y 
      repudiarla?
    8 El les dijo: Por la dureza de vuestro corazón Moisés os permitió repudiar a 
      vuestras mujeres; mas al principio no fue así.
    9 Y yo os digo que cualquiera que repudia a su mujer, salvo por causa de 
      fornicación, y se casa con otra, adultera; y el que se casa con la 
      repudiada, adultera.
    10 Le dijeron sus discípulos: Si así es la condición del hombre con su mujer, 
      no conviene casarse.
    11 Entonces él les dijo: No todos son capaces de recibir esto, sino aquellos a 
      quienes es dado.
    12 Pues hay eunucos que nacieron así del vientre de su madre, y hay eunucos 
      que son hechos eunucos por los hombres, y hay eunucos que a sí mismos se 
      hicieron eunucos por causa del reino de los cielos. El que sea capaz de 
      recibir esto, que lo reciba.

## 3. CONTEXTO

Concuerda casi completo con Marcos 10:1-12

    1 Levantándose de allí, vino a la región de Judea y al otro lado del Jordán; y 
      volvió el pueblo a juntarse a él, y de nuevo les enseñaba como solía
    2 Y se acercaron los fariseos y le preguntaron, para tentarle, si era lícito 
      al marido repudiar a su mujer.
    3 El, respondiendo, les dijo: ¿Qué os mandó Moisés?
    4 Ellos dijeron: Moisés permitió dar carta de divorcio, y repudiarla.
    5 Y respondiendo Jesús, les dijo: Por la dureza de vuestro corazón os escribió 
      este mandamiento;
    6 pero al principio de la creación, varón y hembra los hizo Dios.
    7 Por esto dejará el hombre a su padre y a su madre, y se unirá a su mujer,
    8 y los dos serán una sola carne; así que no son ya más dos, sino uno.
    9 Por tanto, lo que Dios juntó, no lo separe el hombre.
    10 En casa volvieron los discípulos a preguntarle de lo mismo,
    11 y les dijo: Cualquiera que repudia a su mujer y se casa con otra, comete 
      adulterio contra ella;
    12 y si la mujer repudia a su marido y se casa con otro, comete adulterio.

El pasaje que sigue tanto en Mateo (19:13-15) como en Marcos (10:13-16) describe como Jesús abrazaba niños y los bendecía, después de reprender a los discípulos que no permitía que fueran a Él diciéndoles que dejaran a los niños ir a Él, pues el reino de los cielos es de quienes son como ellos.

El capítulo anterior es sobre el pecado, como Dios espera que volvamos a Él y que perdonemos pero comienza con otro pasaje que también menciona a los niños 18:1-5, cuando los discípulos preguntan quien es mayor en el Reino de los Cielos y Jesús pone un niño en el medio y dice que si no nos hacemos como niños, no entraremos en el reino de los cielos, y que quien reciba a un niño, recibe al Señor Jesús.

Los niños son entonces muy apreciados para Dios, en su humildad e inocencia los hace modelos de cómo ser para ingresar al Reino, sabemos que la familia es el lugar donde deben crecer, es el lugar que los protege y cuida.

Dios es nuestro Padre y Madre, él también es Hijo. Y quiere que lo aceptemos como Padre, pero en libertad. Si aceptamos, Jesús es como nuestro hermano, Él quiere llamarnos amigos y no siervos, también en libertad. Así como cada uno, hombre y mujer, es imagen de Dios, la familia y en particular la relación entre hijos y padres puede verse como una imagen de la trinidad.

Volvamos a centrarnos en la pareja de esposos. Jesús participó en bodas en su tiempo, repasemos algunos detalles del matrimonio en el tiempo de Jesús (con base en {3}) que primero constaban de un compromiso y luego de un tiempo la boda:

* **Búsqueda**. Los padres del novio ayudaban a buscar novia del mismo clan. Recordemos como Abraham envió a su siervo a buscar esposa para su hijo Isaac entre sus familiares. Aún así con aprobación por parte de pretendientes (recorder que a Rebeca su papá le preguntó si quería irse con el siervo de Abraham). Por cierto ese matrimonio entre Isaac y Rebeca es un buen ejemplo de buen matrimonio en muchos aspectos.
* **Contrato**. El pretendiente iba a casa de la elegida con vino, un contrato y dinero, hablaba con el padre y hermanos mayores de la pretendida y ofrecía un pago. Según {3} los valores comenzaban en 200 denarios si era doncella, 100 si era viuda, 400 si era hija de sacerdote, como el denario es un día de salario hoy en dia suponiendo que fuese el mínimo (alrededor de $40.000 al día) comenzaría en 8’000.000 para doncellas, 4’000.000 para viudas y 16’000.000 para hijas de sacerdotes. Si el padre aceptaba tomaban vino, después el padre llamaba a la pretendida y le preguntaba si aceptaba, si ella aceptaba todos tomaban vino y se pronunciaba una bendición.
* **Esponsales**. Dentro de 12 meses después del contrato, el novio daba regalo de bodas (según {4}) anillo de oro a novia. Y ya eran tratados como esposos ante ley, pero seguían viviendo en casa de los padres de cada uno. Mientras continuaba este compromiso y hasta la boda, la novia se preparaba para ser esposa y el novio preparaba donde irían a vivir.
* **Boda**. De 6 a 12 meses después de los esponsales. Se realizaba en casa de padre del novio o en casa del novio, típicamente miércoles por si acaso el novio tenía quejas por presentar al sanedrín que se reunía el jueves, respecto a virginidad de la esposa. Comenzaba en casa de la novia donde se reunían los invitados a esperar al novio hasta media hora antes de la medianoche, cuando llegaba el novio con amigos y antorchas. Entonces la comitiva se desplazaba en festividad a la casa del del novio cada quien con antorchas encendidas (que era requisito para unirse a la comitiva). Recordemos la parábola de las 10 vírgenes que debían estar entre ese tipo de comitiva. En casa del novio se pronunciaba una bendición y el novio y la novia entraban a la cámara nupcial, adornados con coronas, en medio de música y mientras la comitiva les arrojaba granos o monedas. Una vez el novio daba a sus amigos señal de la virginidad de la esposa, comenzaba la fiesta a la que se unían los novios (cuando podían), la fiesta continuaba por una semana.

Algunos de estos detalles pueden verse en esta representación [https://www.youtube.com/watch?v=LtFMouLCXt8](https://www.youtube.com/watch?v=LtFMouLCXt8 "https://www.youtube.com/watch?v=LtFMouLCXt8") En la misma se presenta acorde a lo mencionado el compromiso (que llaman esponsal) y la boda. Faltan los esponsales como los investigamos de otras fuentes.

## 4. ANÁLISIS

Antes de repasar el pasaje central, profundicemos en la íntima relación de la trinidad, siguiendo la teología de genero desarrollada en {5}

* Relación entre el Padre y el Hijo: Juan 10:30 Yo y el Padre uno somos.
* Relación entre el Espíritu Santo y el Hijo: 2 corintios 3:17 Porque el Señor es el Espíritu; y donde está el Espíritu del Señor, allí hay libertad.
* Relación entre el Padre y el Espíritu Santo: 1 corintios 2:10-11 10 Pero Dios nos las reveló a nosotros por el Espíritu; porque el Espíritu todo lo escudriña, aun lo profundo de Dios. 11 Porque ¿quién de los hombres sabe las cosas del hombre, sino el espíritu del hombre que está en él? Así tampoco nadie conoció las cosas de Dios, sino el Espíritu de Dios.

Entonces nuestro Dios trino, es un Dios en relación, en amor inescrutable entre las 3 personas que Son Uno. Traducción libre de {5} “Nosotros siendo imagen de Dios tenemos la habilidad y el privilegio de tener relaciones humanas… mostramos Su imagen Trina cuando nos acercamos unos a otros … Dios nos ha dado relaciones entre unos y otros para descrubir más de Él”. Repasemos versículo a versículo

 1. Ver recorrido de Galilea a Judea en mapa de [https://8b8f2b8b-a-62cb3a1a-s-sites.googlegroups.com/site/labuenanoticiadejesus/recapitulacin/Palestina_siglo1.gif?attachauth=ANoY7crTgxq5Qi4F3fEFURENNq0E_F14T3Tt_RmfKhTWoR1slK7AW5fLiGAUz12-7AfOdMB8Q4BVYJ-Eop8CpG8F_EAA4Xr1ei2gZyQ0GhnBtYRF50HN_hDE0Br6Oat7hKFFRmIgp7PfbO8uteDGncdHdBFqVSbhu0ySmnwbJ8qwSVJF3uPXg3ltf7X74eYLp48DV6VAoXhl5IJQQ8nHW8P9yHQaitjAw1Wwbh54tAgOBQyUu1qh6Iq7Rvchxd-w9hKlShS_93QO&attredirects=1](https://8b8f2b8b-a-62cb3a1a-s-sites.googlegroups.com/site/labuenanoticiadejesus/recapitulacin/Palestina_siglo1.gif?attachauth=ANoY7crTgxq5Qi4F3fEFURENNq0E_F14T3Tt_RmfKhTWoR1slK7AW5fLiGAUz12-7AfOdMB8Q4BVYJ-Eop8CpG8F_EAA4Xr1ei2gZyQ0GhnBtYRF50HN_hDE0Br6Oat7hKFFRmIgp7PfbO8uteDGncdHdBFqVSbhu0ySmnwbJ8qwSVJF3uPXg3ltf7X74eYLp48DV6VAoXhl5IJQQ8nHW8P9yHQaitjAw1Wwbh54tAgOBQyUu1qh6Iq7Rvchxd-w9hKlShS_93QO&attredirects=1 "https://8b8f2b8b-a-62cb3a1a-s-sites.googlegroups.com/site/labuenanoticiadejesus/recapitulacin/Palestina_siglo1.gif?attachauth=ANoY7crTgxq5Qi4F3fEFURENNq0E_F14T3Tt_RmfKhTWoR1slK7AW5fLiGAUz12-7AfOdMB8Q4BVYJ-Eop8CpG8F_EAA4Xr1ei2gZyQ0GhnBtYRF50HN_hDE0Br6Oat7hKFFRmIgp7PfbO8uteDGncdHdBFqVSbhu0ySmnwbJ8qwSVJF3uPXg3ltf7X74eYLp48DV6VAoXhl5IJQQ8nHW8P9yHQaitjAw1Wwbh54tAgOBQyUu1qh6Iq7Rvchxd-w9hKlShS_93QO&attredirects=1")
 2. Como solía hacer
 3. Cúan necios, aunque los fariseos veían la obra del Espíritu no creían, querían entar a Jesús pero Él aprovecha para explicar más a la humanidad
 4. Génesis 1:27 Y creó Dios al hombre a su imagen, a imagen de Dios lo creó; varón y hembra los creó. Debería sobrar decir que en la Biblia y desde la tradición antropológica y natural humana el matrimonio es entre hombre y mujer.
 5. Génesis 2:24 Por tanto, dejará el hombre a su padre y a su madre, y se unirá a su mujer, y serán una sola carne. Efesios 5:31 Por esto dejará el hombre a su padre y a su madre, y se unirá a su mujer, y los dos serán una sola carne., 1 Cor 6:16 ¿O no sabéis que el que se une con una ramera, es un cuerpo con ella? Porque dice: Los dos serán una sola carne. Aquí tenemos la razón del matrimonio, unidad y dar sentido último al género, una relación profunda que los estudios (ver {5}) han demostrado puede ser sólo entre hombre y mujer.
 6. 1 Cor 7:10 Pero a los que están unidos en matrimonio, mando, no yo, sino el Señor: Que la mujer no se separe del marido; Matrimonio es una institución sagrada, requiere la mayor seriedad y compromiso.
 7. Deut 24:1 Cuando alguno tomare mujer y se casare con ella, si no le agradare por haber hallado en ella alguna cosa indecente, le escribirá carta de divorcio, y se la entregará en su mano, y la despedirá de su casa. Jer 3:1 Dicen: Si alguno dejare a su mujer, y yéndose ésta de él se juntare a otro hombre, ¿volverá a ella más? ¿No será tal tierra del todo amancillada? Tú, pues, has fornicado con muchos amigos; mas !!vuélvete a mí! dice Jehová.
 8. 
    * 
 9. Mat 5:32 Pero yo os digo que el que repudia a su mujer, a no ser por causa de fornicación, hace que ella adultere; y el que se casa con la repudiada, comete adulterio. Marcos 10:11 y les dijo: Cualquiera que repudia a su mujer y se casa con otra, comete adulterio contra ella; Lucas 16:18: Todo el que repudia a su mujer, y se casa con otra, adultera; y el que se casa con la repudiada del marido, adultera.; 1 Corintios 7:10: Pero a los que están unidos en matrimonio, mando, no yo, sino el Señor: Que la mujer no se separe del marido;
10. 
    * 
11. 1 Cor 7:7 Quisiera más bien que todos los hombres fuesen como yo; pero cada uno tiene su propio don de Dios, uno a la verdad de un modo, y otro de otro. , 1 Cor 7:17: Pero cada uno como el Señor le repartió, y como Dios llamó a cada uno, así haga; esto ordeno en todas las iglesias.
12. 
    * 

## 5. CONCLUSIONES Y ORACIÓN

Las relaciones nos permiten conocer más de Dios, relación profunda entre esposo y esposa y entre padre/madre e hijo/a. Dos importantes puntos de estas relaciones son:

* Conocer (como nadie más) al otro
* Estar cerca física, emocional y espiritualmente

La labor de buscar esposa es del hombre, pero en sus fuerzas es imposible, requiere la guianza de Dios. Por esto es responsabilidad de niños y jóvenes orar por su espos@ y de los padres/profesores orar por espos@ para sus hijos/estudiantes (recordar como hizo Abraham).

En el caso de esposos el sexo es una manera de estar físicamente cerca, de hecho la mayor de carácter físico, que gran regalo de Dios, pero eso no significa acercamiento emocional, ni espiritual, esos debemos buscarlos con alma y espíritu. Igualmente conocer a la pareja como nadie más, trasciende el sexo –aunque también debemos descubrir con espos@ la sexualidad plena.

Dios nos ha dotado de un género que debemos ir descubriendo y conociendo, ese aprendizaje tiene un hito en el matrimonio entre hombre y mujer. Claro está a quienes se hacen eunucos, el Señor directamente va guiando ese aprendizaje.

Entonces es paradójico pero a medida que entro en relación con otros y en particular con mi núcleo familiar (espos@ e hij@s), a medida que con amor me acerco y conozco más del otro voy descubriendo más de mi y mi genero. Así le ha placido a Dios, e increiblemente así también vamos conociendo más de Dios.

Son pequeñas recomendaciones prácticas respecto a las cuales oramos para que el Señor nos recuerde con su Santo Espíritu en el nombre de Jesús:

* Orar en iglesia, orar en familia, orar en pareja, orar individualmente. En la oración nos damos a conocer más entre quienes oramos.
* Jóvenes orar por espos@, padre orar por espos@ de hijos recordar ejemplo de Abraham e Isaac
* Buscar conocer a cada miembro de familia mejor que nadie, recordar trinidad
* Buscar estar cerca de cada miembro de la familia, en grupo y uno a uno, recordar trinidad

## 6. REFERENCIAS

* {1} Biblia. RV1960
* {2} Concordancias: Reina Valera - Geiger. [https://github.com/pasosdeJesus/biblia_dp/blob/master/ref/reina_valera_geiger_nt/40_Matthew.usfm](https://github.com/pasosdeJesus/biblia_dp/blob/master/ref/reina_valera_geiger_nt/40_Matthew.usfm "https://github.com/pasosdeJesus/biblia_dp/blob/master/ref/reina_valera_geiger_nt/40_Matthew.usfm")
* {3} Jan Herca, 2009. Una típica boda judia. [https://buscandoajesus.wordpress.com/articulos/una-tipica-boda-judia/](https://buscandoajesus.wordpress.com/articulos/una-tipica-boda-judia/ "https://buscandoajesus.wordpress.com/articulos/una-tipica-boda-judia/") Licencia Creative Commons Attribution-NonCommercial-ShareAlike 2.5 Spain License
* {4} Miguel Sanchez. 2011. Las bodas judías de tiempos bíblicos y su comparación con las bodas del Cordero, luego del Arrebatamiento de la iglesia. [http://blogmiguelsanchezavila.blogspot.com.co/2011/11/las-bodas-judias-de-tiempos-biblicos-y.html](http://blogmiguelsanchezavila.blogspot.com.co/2011/11/las-bodas-judias-de-tiempos-biblicos-y.html "http://blogmiguelsanchezavila.blogspot.com.co/2011/11/las-bodas-judias-de-tiempos-biblicos-y.html")
* {5} Sam A. Andreades. enGendered. God’s Gift of Gender Difference in Relationship. 2015.

***

Dominio público de acuerdo a la legislación colombiana. También disponible en [https://docs.google.com/document/d/1Jyvz8TomQ3ZWhJmpOQQqQVJ6k4ZWZWsqKIHXlb2j9g0/edit?usp=sharing](https://docs.google.com/document/d/1Jyvz8TomQ3ZWhJmpOQQqQVJ6k4ZWZWsqKIHXlb2j9g0/edit?usp=sharing "https://docs.google.com/document/d/1Jyvz8TomQ3ZWhJmpOQQqQVJ6k4ZWZWsqKIHXlb2j9g0/edit?usp=sharing")

Imagen de portada de OpenClipart