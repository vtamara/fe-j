---
layout: post
categories: []
title: Liderazgo cristiano y el paro nacional
author: Vladimir Támara Patiño
image: "/assets/images/1620233905_641199_1620234328_noticia_normal.jpg"

---
# Liderazgo cristiano y el paro nacional.

# Mateo 7:15-20

### Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 6.Jun.2021

# 1 Introducción

Las coyunturas de la pandemia y el paro están mostrando que ocurre al interior de las iglesias y sus liderazgos. Hay diversas situaciones, por ejemplo:

1. El pastor John Milton Rodrígez de Cali, senador de la república por Colombia Justa Libres, votó contra la moción de censura al ministro de defensa Diego Molano. El senador John Milton con su voto ayudó a mantener en su cargo al actual ministro de defensa aún cuando ha ordenado la militarización contra la protesta (oponiendose al diálogo por el que clama el país) y ha ignorado los abusos de la fuerza pública y las acciones paramilitares entre civiles armados y fuerza pública en contra de manifestantes. En su prédica del 3 de Junio el pastor John Milton explica y promueve el principio de legítima defensa según el cual un ciudadano puede usar armas con salvoconducto o matar para defender sus bienes o su integridad. Ver minuto 28 de [https://www.youtube.com/watch?v=QHGh9vJauhU&t=1735s](https://www.youtube.com/watch?v=QHGh9vJauhU&t=1735s "https://www.youtube.com/watch?v=QHGh9vJauhU&t=1735s")
2. El sacerdote ortodoxo Edinson Huerfáno, conocido por procesos con comunidades de escasos recurso en Cali. Se organiza con otros sacerdotes y pastores para conformar en Cali lo que llaman una “Primera línea ecuménica”, según explica en vídeo del 28 de Mayo, desde minuto 0:50, para estar acompañando al pueblo que está en las calles y que lo necesita [https://www.youtube.com/watch?v=Xt3UqhwHyQo](https://www.youtube.com/watch?v=Xt3UqhwHyQo "https://www.youtube.com/watch?v=Xt3UqhwHyQo")

Es decir son líderes cristianos que están dando mensajes totalmente opuestos, uno apoya el uso de armas para disuadir o terminar con los manifestantes, mientras que el otro sin armas se une a quienes protestan para no dejarlos solos.

![](https://lh4.googleusercontent.com/Q7eCdIjYY4b2QSsrJFlsf8RDd7EEe8htmjUW1cEbfo1I3ZBnfDhyp60t1D49q5-K94XAB2Juvenokh2Wk4j5aL3U1LL62XfMBZBth60S0EolMsycIbwPLQGc_NKQ8OVrIrmSJ_ze =466x348)

Ante esto, surge la pregunta ¿Cómo cristianos que debemos apoyar?

# 2. Texto: Mateo 7:15-20

15 Guardaos de los falsos profetas, que vienen a vosotros con vestidos de ovejas, pero por dentro son lobos rapaces.

16 Por sus frutos los conoceréis. ¿Acaso se recogen uvas de los espinos, o higos de los abrojos?

17 Así, todo buen árbol da buenos frutos, pero el árbol malo da frutos malos.

18 No puede el buen árbol dar malos frutos, ni el árbol malo dar frutos buenos.

19 Todo árbol que no da buen fruto, es cortado y echado en el fuego.

20 Así que, por sus frutos los conoceréis.

# 3. Contexto

No quiero juzgar, ni sacar brizna del ojo ajeno teniendo una viga en el mio, pues soy pecador, en luchas diarias contra mi propia carne en las que he ganado y perdido batallas, pero cada vez que he perdido he ido a Cristo (y también a cristian@s ante quienes rindo cuentas) y Cristo me ha perdonado, limpiado y restaurado cada vez, cada vez dejandome como garantía la presencia de su Santo Espíritu.

Sin embargo, y aunque no soy digno, por el liderazgo en esta iglesia, tampoco puedo quedarme en silencio y no enseñar lo que he aprendido de los cristianos menonitas.

La confesión de Schleitheim, publicada en 1527, es un escrito en el que los primeros menonitas declararon lo que creen, en el pueblo de Schleitheim en Suiza.  Hoy nos sigue guiando. En este escrito dejan claro el principio de los dos reinos, uno terrenal en donde se usa la espada y otro el de la iglesia donde no se usa la espada física sino que la espada es la palabra de Dios. A continuación transcribo parte de una traducción a español disponible en {TRAD}

> La espada constituye un ordenamiento de Dios fuera de la perfección de Cristo. Castiga y mata a los malvados, y guarda y protege a los buenos. En la ley, la espada fue establecida sobre los malvados para castigo y para muerte, y los gobernantes seculares son establecidos para emplearla. Mas en la perfección de Cristo sólo se emplea la excomunión, para la amonestación y la exclusión del que ha pecado, sin la muerte de la carne sino sencillamente mediante la advertencia y el mandamiento de que deje de pecar. Muchos, al no comprender la voluntad de Cristo para nosotros, se preguntarán si un cristiano no puede y debe hacer uso de la espada contra los malvados para protección y defensa de los buenos, o motivados por el amor. La respuesta nos ha sido revelada por unanimidad: Cristo enseña y manda que aprendamos de él, que es manso y humilde de corazón, y que así hallaremos descanso para nuestras almas. Cristo dice a la mujer hallada en adulterio, no que deba ser apedreada según la ley del Padre (aunque dice: «lo que el Padre me ha mandado, eso he hecho»), sino que con misericordia y perdón y una amonestación de que no peque más, dice: «Ve, y no peques más». Es exactamente así como debemos proceder nosotros también … , según la regla de la excomunión. En segundo lugar algunos preguntan respecto a la espada si un cristiano debe dictar sentencia en disputas y contiendas sobre asuntos terrenales, tales como los incrédulos tienen entre sí. La respuesta: Cristo no quiso decidir ni juzgar entre dos hermanos respecto a su herencia, sino que se negó a ello. Así también debemos proceder nosotros. En tercer lugar suele preguntarse respecto a la espada si un Cristiano debe servir como magistrado si resulta nombrado a tal cargo. La respuesta es la siguiente: Quisieron poner a Cristo como rey, pero el huyó y no vio en ello la voluntad de su Padre. Nosotros debemos hacer como él hizo y seguirle a él, y así evitaremos andar en las tinieblas. Porque él mismo dijo: «Todo aquel que quiera venir en pos de mí, niéguese a sí mismo y tome su cruz y sígame». Además él mismo prohibe la violencia de la espada cuando dice: «Los príncipes de este mundo se enseñorean sobre ellos, etc., pero no sea así entre vosotros». Además Pablo dice: «A los que conoció de antemano, Dios los predestinó a ser hechos conformes a la imagen de su Hijo, etc.» Pedro también dice: «Cristo sufrió —no reinó —dejándoos ejemplo para que sigáis sus pisadas». Por último, queda claro por los siguientes puntos que no le corresponde al cristiano la magistratura: el gobierno de las autoridades es conforme a la carne, mas el de los cristianos conforme al Espíritu. Sus casas y morada permanecen en esta tierra, la de los cristianos les aguarda en el cielo. Su ciudadanía está en este mundo, la de los cristianos está en el cielo. Las armas de su contienda y guerra son carnales y tan sólo eficaces contra la carne, mas las armas de los cristianos son espirituales, eficaces contra toda fortaleza del diablo. Los mundanos se arman con acero y hierro, mas los cristianos se arman con la armadura de Dios, con verdad, justicia, paz, fe, salvación, y con la Palabra de Dios. En una palabra: así como piensa Cristo nuestra Cabeza, así también hemos de pensar los miembros del cuerpo de Cristo por medio de él, para que no haya división en su cuerpo, lo cual resultaría en su destrucción. Entonces, puesto que Cristo es tal como se escribe acerca de él, así también sus miembros han de ser iguales, para que su cuerpo permanezca entero y unido para su propio progreso y edificación. Porque cualquier reino que esté dividido en sí mismo, será destruido.

Espero que de esto quede claro que en el Reino de Dios, donde debe estar la iglesia, no existe ese derecho a la legítima defensa. Y así lo vivenció Cristo mismo y muchos y muchas mártires cristian@s que se negaron a defenderse con armas, que además nos permite paramos en la roca firme y obedecemos Éxodo 20:13 que dice “no matarás” y a Jesús mismo quien en Mateo 5:21-22 dice \`21 Oísteis que fue dicho a los antiguos: No matarás; y cualquiera que matare será culpable de juicio. 22 Pero yo os digo que cualquiera que se enoje contra su hermano, será culpable de juicio; y cualquiera que diga: Necio, a su hermano, será culpable ante el concilio; y cualquiera que le diga: Fatuo, quedará expuesto al infierno de fuego.'

Con la claridad que no debemos matar --como si no fuera obvio, pues está hasta en la conciencia de los no cristianos del paro que están exigiendo respeto a la vida-- ni aún excusandonos en la legítima defensa, ¿qué si debemos hacer? Al respecto el Señor me ha puesto en el corazón, defender al débil, aclarado en los siguientes versículos (especialmente provenientes de {DEF}):

* Jeremías 22:3 “ Así ha dicho Jehová: Haced juicio y justicia, y librad al oprimido de mano del opresor, y no engañéis ni robéis al extranjero, ni al huérfano ni a la viuda, ni derraméis sangre inocente en este lugar.”
* Isaías 1:17 “aprended a hacer el bien; buscad el juicio, restituid al agraviado, haced justicia al huérfano, amparad a la viuda.”
* Salmo 82:2-4 “¿Hasta cuándo juzgaréis injustamente, Y aceptaréis las personas de los impíos? _Selah_ 3 Defended al débil y al huérfano; Haced justicia al afligido y al menesteroso. 4 Librad al afligido y al necesitado; Libradlo de mano de los impíos.”
* Miqueas 6:8 “8 Oh hombre, él te ha declarado lo que es bueno, y qué pide Jehová de ti: solamente hacer justicia, y amar misericordia, y humillarte ante tu Dios.”
* Prov 31:8-9 “Abre tu boca por el mudo En el juicio de todos los desvalidos. 9 Abre tu boca, juzga con justicia, Y defiende la causa del pobre y del menesteroso.”
* Salmo 140:12 “Yo sé que Jehová tomará a su cargo la causa del afligido,

  Y el derecho de los necesitados. “
* Mateo 25:35-40 “35 Porque tuve hambre, y me disteis de comer; tuve sed, y me disteis de beber; fui forastero, y me recogisteis; 36 estuve desnudo, y me cubristeis; enfermo, y me visitasteis; en la cárcel, y vinisteis a mí. 37 Entonces los justos le responderán diciendo: Señor, ¿cuándo te vimos hambriento, y te sustentamos, o sediento, y te dimos de beber? 38 ¿Y cuándo te vimos forastero, y te recogimos, o desnudo, y te cubrimos? 39 ¿O cuándo te vimos enfermo, o en la cárcel, y vinimos a ti? 40 Y respondiendo el Rey, les dirá: De cierto os digo que en cuanto lo hicisteis a uno de estos mis hermanos más pequeños, a mí lo hicisteis.”
* Filipenses 2:3-8 “3 Nada hagáis por contienda o por vanagloria; antes bien con humildad, estimando cada uno a los demás como superiores a él mismo; 4 no mirando cada uno por lo suyo propio, sino cada cual también por lo de los otros. 5 Haya, pues, en vosotros este sentir que hubo también en Cristo Jesús, 6 el cual, siendo en forma de Dios, no estimó el ser igual a Dios como cosa a que aferrarse, 7 sino que se despojó a sí mismo, tomando forma de siervo, hecho semejante a los hombres; 8 y estando en la condición de hombre, se humilló a sí mismo, haciéndose obediente hasta la muerte, y muerte de cruz.”

# 4. Análisis

Retomemos los versículos centrales de esta predicación, y del Reino de la Iglesia de la corrección fraterna.

El siguiente video, tomado el 28 de Mayo en la calle 16 con carrera 100 en Cali, muestra paramilitarismo es decir la acción de ataque conjunta de civiles armados en coordinación con fuerza pública en contra de objetivos de la fuerza pública. [https://www.youtube.com/watch?v=eOi1BFMhs7g](https://www.youtube.com/watch?v=eOi1BFMhs7g "https://www.youtube.com/watch?v=eOi1BFMhs7g")

Según diversos medios, incluyendo {PLU} la persona señalada en el vídeo, se congrega en la iglesia del pastor John Milton Rodríguez. Al parecer será citado por fiscalía, y en un vídeo ha pedido perdón, a la vez que explicaba que se había organizado con otros empresarios para defender sus bienes y familias. Es decir ha aprendido y practicado ese principio de legítima defensa, al cual también acudía Castaño el fundador de las AUC, al que acudieron los empresarios que empezaron las Convivir y al que sigue acudiendo Uribe Velez para justificar lo injustificable.![](https://lh5.googleusercontent.com/9jiPU9taEZAfeWYwFkze99qcc5ZQYfrJNt25z1faZ8MtRoyAix3yUldPVMaCCv-9ytc1Lo9PnBjLtOmxCyIRloMEaAE_e1LhbkpUoGpKkW8peKjOpAW3bV1xSHk2WzAUQp19Bh6J =467x263)

Creemos en un llamado fraterno al pastor John Milton Rodríguez, quien ha dado buenos frutos predicando a Cristo para que más personas lo reciban como Señor y salvador, para que vuelva a revisar su enseñanza sobre la legítima defensa --al respecto publiqué un comentario en el vídeo de youtube de la predicación en cuestión .

Del sacerdote ortodoxo Edinson Huerfano, sólo he encontrado diversas entrevistas y declaraciones desde 2007 donde se le ve viviendo y trabajando con comunidades vulnerables y defendiendolas.

# 5. Conclusión y oración

En este paro he encontrado:

* Muchos manifestantes desarmados que piden respeto a la vida
* Declaraciones en Caracol Radio de un jóven de primera línea de Puerto Resistencia en Cali, que explicaba que se protegen unos a otros y que no quiere protección del ejército, ni de la policía porque entonces su protesta dejaría de ser pacífica.
* Personas que se aprovechan de la situación por ejemplo para cobrar peajes en Soacha o para hacer vandalismo o para buscar beneficios políticos.

Como Cristianos les pido oración y discernimiento frente a las siguientes propuestas:

* Defendamos a los manifestantes débiles que no usen armas ni busquen matar
* Promovamos la sacralidad de la vida sobre los bienes materiales
* Opongamonos con la palabra de Dios a quienes usen armas y búsquen matar o militarizar. Propendamos por el diálogo como única manera.
* Que prediquemos a Cristo y la necesidad de salvación y continua limpieza y restauración que sólo Él puede dar y que comienza recibiendolo en el corazón.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* {DEF} [https://www.openbible.info/topics/defending_the_weak](https://www.openbible.info/topics/defending_the_weak "https://www.openbible.info/topics/defending_the_weak")
* {TRAD} [http://www.misioncristianamennonita.org/descargas/Confesi%C3%B3n%20de%20Schleitheim.pdf](http://www.misioncristianamennonita.org/descargas/Confesi%C3%B3n%20de%20Schleitheim.pdf "http://www.misioncristianamennonita.org/descargas/Confesi%C3%B3n%20de%20Schleitheim.pdf")
* {PLUR} [https://pluralidadz.com/tendencias/andres-escobar-supuesto-miembro-de-la-iglesia-de-john-milton-rodriguez-senador-de-colombia-justa-libres/](https://pluralidadz.com/tendencias/andres-escobar-supuesto-miembro-de-la-iglesia-de-john-milton-rodriguez-senador-de-colombia-justa-libres/ "https://pluralidadz.com/tendencias/andres-escobar-supuesto-miembro-de-la-iglesia-de-john-milton-rodriguez-senador-de-colombia-justa-libres/")

----

La fotografía de portada es de El País. [https://elpais.com/elpais/2021/05/05/album/1620233905_641199.html](https://elpais.com/elpais/2021/05/05/album/1620233905_641199.html "https://elpais.com/elpais/2021/05/05/album/1620233905_641199.html")


Esta predica se cede al dominio público de acuerdo a la legislación colombiana.