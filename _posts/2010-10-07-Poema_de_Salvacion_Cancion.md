---
layout: post
title: "Poema_de_Salvacion_Cancion"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

Matt y Sherry McPherson Traducción: Sammy Gondola 

<pre>
Cristo moriste en una cruz     
Resucitaste con poder
perdona mis pecados hoy        
se mi señor mi salvador


cambiame y hasme otra vez    
y ayudame a serte fiel
cambiame y hasme otra vez    
y ayudame a serte fiel
</pre>


!Versión original en inglés

Tomado de {3}

Referencias (John 3:16, Ephes. 4:2B-3, Romans 10: 8-10)

Jesus You died upon a cross / And rose again to save the lost / Forgive me now of all my sin / Come be my Savior, Lord, and Friend //

Refrain:Change my life and make it new / And help me Lord to live for You //

Change my life and make it new / And help me Lord to live for You /


## REFERENCIAS 
* {3} http://www.christianmusic.com/matt_sherry_mcpherson/matt_and_sherry_mcpherson_review.htm
