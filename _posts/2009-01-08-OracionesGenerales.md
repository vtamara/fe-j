---
layout: post
title: "OracionesGenerales"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

ORAR PARA QUE SE ABRAN PUERTAS

que las puertas se abran no se puede dar por sentado. Muchos misioneros trabajan en países que son de difícil acceso o en áreas que son resistentes al evangelio. Sin embargo, "puertas abiertas" incluye algo más que el acceso a las naciones y los grupos de personas. Los corazones también deben ser abiertos y receptivos a la verdad de Dios.

* Ore que Dios abra las puertas del ministerio, bendiga asociaciones y amistades de los misioneros.
* Ore para que aquellos que sirven seán dirigidos por el Espíritu Santo y reconozcan oportunidades y puertas abiertas .
* Ore para que Dios conduzca a su pueblo y derribe los obstáculos y prepare los corazones dispuestos a recibir a Su Palabra.

por valor en los testigos

"Recen también por mí, que cada vez que abro mi boca, las palabras me sean dadas a fin de que de a conocer sin temor el misterio del evangelio" (Efesios 6:19, NIV).

Los Misioneros habitualmente son personas que temen el dolor y el rechazo tanto como cualquier otra persona. Cuando se enfrentan con la oposición, necesitan la fuerza de Dios para ayudarles a mantenerse firme.

* Ore que los misioneros tengán el valor para superar el miedo a la vergüenza o al fracaso.
* Ore que el Espíritu les proporcione palabras que se comuniquen de manera efectiva en otras culturas e idiomas.
* Orar contra las fuerzas del mal que tratan de obstaculizar la propagación del evangelio.

Ore para que la Palabra de Dios se extienda

"Por último, queridos hermanos y hermanas, les pido que oren por nosotros.En primer lugar, que el mensaje del Señor se extienda rápidamente y sea honrado dondequiera que va. ... "(2 Tesalonicenses 3:1, NLT)

Los obstáculos deben ser removidos para permitir que la Palabra de Dios prolifere de forma rápida y libremente. Implica la eliminación de los obstáculos y una decidida resistencia en la guerra espiritual. Así como Aarón y Hur apoyaron los brazos de Moisés en la batalla contra los amalecitas (Éxodo 17:12), usted puede fortalecer los brazos de los misioneros a través de sus oraciones.

* Ruega por fuerza y resistencia para los misioneros en su encuentro espiritual con fuerzas antagónicas. (Efesios 6:10-18)
* Ore para que las personas resistan los planes de Satanás para impedir la propagación del evangelio. (Santiago 4:7)
* Ore para que la Palabra de Dios en efecto, se propague rápidamente y sea honrada dondequiera que vaya.

Ore por la Protección

"Orad,que seamos guardados de hombres perversos y malos, pues no es para todos la fe" (2 Tesalonicenses 3:2, NLT).

En algunos países, las puertas abiertas puede exponer a los misioneros a la posibilidad de peligro y de daño personal. La oposición al evangelio puede incluir el odio y la violencia.

* Ore para que Dios mantengá a los trabajadores Cristianos a salvo de aquellos que buscan hacerles daño.
* Ore para que Dios cambie los corazones de aquellos que son resistentes a Su Palabra.


Ruega por su ministerio

"Oren en primer lugar, que el mensaje del Señor se extienda rápidamente y sea glorificado dondequiera que vaya. . ". (2 Thesslaonians 3:1, NLT)

La cooperación y la asociación son esenciales para el ministerio y vital para el progreso de los trabajos.

* Ore para que el ministerio de los misioneros y su actitud seá digna de aceptación.
* Ore para que las relaciones con colaboradores y compañeros de los creyentes sea favorable.

Ruega a Dios por su dirección

"Así hemos seguido orando por vosotros desde que oimos hablar por vez primera acerca de vosotros. Pedimos a Dios que os de una completa comprensión de lo que Él quiere hacer en vuestra vida, y pedimos que os haga sabios con la sabiduría espiritual "(Colosenses 1:9, NLT).

Muchos misioneros viajan frecuentemente, tanto a nivel nacional como internacional. Su modo de transporte varía de un país a otro y, a menudo, implica situaciones estresantes.

* Ruega por una clara orientación de Dios en relación con las decisiones de viaje.
* Ruega por los permisos necesarios para viajar.
* Ore por protección y provisión durante sus viajes.

Ruega por Refrigerio

"También oramos que seais fortalecidos con Su glorioso poder de modo que tengáis toda la paciencia y la resistencia que ustedes necesitan. Se puede estar lleno de alegría "(Colosenses 1:11, NLT).

Los Misioneros tienen que hacer frente a muchas de las mismas cosas que se enfrentan en la vida como: un abrumador volumen de trabajo, conflictos relacionales y la incertidumbre financiera. A menudo, sin embargo, los misioneros luchan con estas cuestiones solos, sin tener el apoyo de otros cristianos,ni financiero, ni de oración. La vida y trabajo transcultural añade un elemento adicional que puede desafiarlos emocional, espiritual y físicamente en su vitalidad.

* Ore que Dios proporcione oportunidades para los misioneros en zonas solitarias,en que puedan pasar tiempo con otros creyentes.
* Ore que Dios proporcione tiempos de paz y relajación para refrescar a sus trabajadores.
* Ruega a Dios que aliente los misioneros con el conocimiento que la gente cuando estén de vuelta en casa se preocuparan por su bienestar emocional.



http://www.foroekklesia.com/showthread.php?t=58006
