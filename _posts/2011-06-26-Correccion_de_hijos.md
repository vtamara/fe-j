---
layout: post
title: "Correccion_de_hijos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

Hemos visto niños que son separados de sus familias por el ICBF, se trata de implementación de medidas que pretenden restablecer derechos de los niños. Ocurre en el marco del Código de Infancia y Adolescencia que entró en vigencia en 2006 (ver {2}), donde el Estado colombiano a través del ICBF se proclama garante de los derechos de los niños.  Así entre las medidas para restablecer derechos descritas en el artículo 53 del código de infancia está:
<pre>
4. Ubicación en centros de emergencia para los casos en que no procede la ubicación en los hogares de paso.
</pre>

En mi humilde opinión estas disposiciones tienen aspectos positivos y negativos.

!1.1 Aspectos positivos:
* Nos está exigiendo a los papás a educar a nuestros hijos sin violencia y en ambientes más sanos, que oramos para que repercuta en una sociedad más sana y menos violenta. Por ejemplo desde el 2010 hay un énfasis especial por evitar consumo abusivo del alcohol (ver {3}) y entre las obligaciones de la familia numeradas en el artículo 39 de la ley 1098 de 2006 están:
<pre>
1.Protegerles contra cualquier acto que amenace o vulnere su vida, su dignidad y su integridad personal....
9. Abstenerse de realizar todo acto y conducta que implique maltrato físico, sexual o psicológico, y asistir a los centros de orientación y tratamiento cuando sea requerida...
14. Prevenirles y mantenerles informados sobre los efectos nocivos del uso y el consumo de sustancias psicoactivas legales e ilegales.
</pre>
* Está salvaguardando niños que han sufrido graves riesgos por la violencia de sus padres o por un ambiente nocivo para ellos.


!1.2 Aspectos negativos:

* Hay casos que evidencian que el Estado no puede ser garante de derechos (e.g. falsos positivos), y que no tiene (personalmente pienso que tampoco tendrá) la infraestructura para cuidar suficientemente bien a los niños que dice  amparar --e.g. una niña de San Nicolas en Junio de 2011 sufrió hepatitis mientras estaba en hogar sustituto asignado por el ICBF Soacha.
* Se extralimita el poder del Estado sobre el de la familia, tanto al no aplicar las medidas que más garanticen la unidad familiar (por ejemplo antes de enviar a un hogar de paso o sustituto debería amonestarse, después intentarse la reubicación en un hogar de la familia extensa ---abuelos, tios como incluye el mismo artículo 53), como porque el lenguaje del código de infancia podría interpretarse para criminalizar toda corrección.

Por esto he estado pidiendo al Señor que me enseñe como corregir a los hijos, entre varias respuestas la más reciente para esta prédica es el capítulo 38 de Isaías.

##2. Pasaje y Contexto

Isaias 38
<pre>
1 En aquellos días Ezequías enfermó de muerte. Y vino a él el profeta Isaías
   hijo de Amoz, y le dijo: Jehová dice así: Ordena tu casa, porque morirás,
   y no vivirás.
2 Entonces volvió Ezequías su rostro a la pared, e hizo oración a Jehová,
3 y dijo: Oh Jehová, te ruego que te acuerdes ahora que he andado delante de 
  ti en verdad y con íntegro corazón, y que he hecho lo que ha sido agradable 
  delante de tus ojos. Y lloró Ezequías con gran lloro.
4 Entonces vino palabra de Jehová a Isaías, diciendo:
5 Ve y di a Ezequías: Jehová Dios de David tu padre dice así: He oído 
  tu oración, y visto tus lágrimas; he aquí que yo añado a tus días quince
  años.
6 Y te libraré a ti y a esta ciudad, de mano del rey de Asiria; y a esta 
  ciudad ampararé.
7 Y esto te será señal de parte de Jehová, que Jehová hará esto que ha dicho:
8 He aquí yo haré volver la sombra por los grados que ha descendido con el
  sol, en el reloj de Acaz, diez grados atrás. Y volvió el sol diez grados
  atrás, por los cuales había ya descendido.
9 Escritura de Ezequías rey de Judá, de cuando enfermó y sanó de su 
  enfermedad:
10 Yo dije: A la mitad de mis días iré a las puertas del Seol; privado soy 
   del resto de mis años.
11 Dije: No veré a JAH, a JAH en la tierra de los vivientes; ya no veré más
   hombre con los moradores del mundo.
12 Mi morada ha sido movida y traspasada de mí, como tienda de pastor. Como 
   tejedor corté mi vida; me cortará con la enfermedad; me consumirás entre 
   el día y la noche.
13 Contaba yo hasta la mañana. Como un león molió todos mis huesos; de la
   mañana a la noche me acabarás.
14 Como la grulla y como la golondrina me quejaba; gemía como la paloma; 
   alzaba en alto mis ojos. Jehová, violencia padezco; fortaléceme.
15 ¿Qué diré? El que me lo dijo, él mismo lo ha hecho. Andaré humildemente 
   todos mis años, a causa de aquella amargura de mi alma.
16 Oh Señor, por todas estas cosas los hombres vivirán, y en todas ellas está 
   la vida de mi espíritu; pues tú me restablecerás, y harás que viva.
17 He aquí, amargura grande me sobrevino en la paz, mas a ti agradó librar mi 
   vida del hoyo de corrupción; porque echaste tras tus espaldas todos mis 
   pecados.
18 Porque el Seol no te exaltará, ni te alabará la muerte; ni los que 
   descienden al sepulcro esperarán tu verdad.
19 El que vive, el que vive, éste te dará alabanza, como yo hoy; el padre 
   hará notoria tu verdad a los hijos.
20 Jehová me salvará; por tanto cantaremos nuestros cánticos en la casa de 
   Jehová todos los días de nuestra vida.
21 Y había dicho Isaías: Tomen masa de higos, y pónganla en la llaga, y 
   sanará.
22 Había asimismo dicho Ezequías: ¿Qué señal tendré de que subiré a la casa 
   de Jehová?
</pre>

Hay actos que realizamos cuyas consecuencias repercuten en nuestros hijos, así ocurre con diversos pecados, de hecho sabemos que estamos en un mundo caído por el pecado de ancestros y nuestro (pues no hay justo ni aún uno como dice Romanos 3:10), donde debemos sufrir separación de Dios y muerte física, pues la paga del pecado es la muerte (Romanos 6:23)

Aunque Ezequias fue fiel a Dios y confió en Él, tenía que sufrir esa muerte física lo cual lo angustió en extremo.   Más que la enfermedad lo que lo hacía sufrir era la angustia de saber que moriría.  

Es que la corrección por leve que sea produce algún tipo de malestar o sufrimiento.  Ese sufrimiento puede ser físico --en una enfermedad por ejemplo-- o psicológico --por ejemplo la angustia que le producía a Ezequías saber que iba a morir.



##3. Análisis

Aprendamos de nuestro Padre celestial la corrección para los hijos e hijas, veamos que Ezequías como hijo suyo lo que hizo y Dios como Padre/Madre lo que hizo:

<pre>
1. Por ley Ezequías sufriría castigo, aunque excepcionalmente supo cuando ocurriría.
  2-3 Ezequías llora y ora recordandole a Dios que ha hecho cosas buenas
4-8 Dios le aplaza el castigo por 15 años y además le garantiza tranquilidad
    y hasta le da una señal instantaea y sobrenatural de confirmación
</pre>

Los versículos  9-20 son una alabanza de Ezequías por el favor de Dios explicando su sufrimiento y como el Señor lo libró de este.

<pre>
10-13 Ezequías expresa su angustia por ser separado de la tierra de los 
vivientes y su incertidumbre frente a su estado posterior eventualmente de 
eterna separación de Dios en el Seol.  Y Ezequías amaba la vida terrena 
porque no conocía la promesa del Señor Jesucristo --le antecede 700 años-- y 
en allí había conocido la gloria de Dios --recordemos que en el capítulo 
anterior de Isaias se describe como Dios había librado al pueblo de Juda de 
los Asirios de manera sobrenatural después de que aquellos habían acabado con 
todas las ciudades de Juda y se disponian a entrar a Jerusalen y como Dios 
había dado palabra a Ezequías y el pueblo que Él obraría y como Ezequías y el 
pueblo confiaron y contra toda racionalidad esperaron y efectivamente la 
noche antes del ataque Asirio el angel de Jehova terminó la vida terrenal de 
180.000 soldados.

14 Ezequías enseña como clamaba a Jehova

15 Y recordando la misericordia del Señor al librarlo de la amargura profunda 
que vivió en esos instantes de incertidumbre y angustía explica su decisión 
de andar humildemente el resto de sus años. 

16-17 Alaba al Creador por su misericordia y parece profetizar el plan de 
salvación 700 años antes,  de hecho Isaias lo profetizó aún más explicito en 
el capítulo 53 ---aún cuando haya quienes nieguen la posibilidad de la 
profecia pues las copias más antiguas del libro de Isaías (Qumram y 
Septuaginta) tienen prácticamente igual el capítulo 53 a las traducciones 
que empleamos

18-20 Explica la importancia de la alabanza y de que los padres enseñemos a 
nuestros hijos a alabar.
</pre>

Aún cuando el Señor dió sanidad espiritual y sobrenaturla el versículo 21 muestra que físicamente también tuvo que hacerse una acción: aplicar masa de higos en la llaga.

!3.1 Aplicación a nuestra familia

"Creemos que la autoridad proviene de Dios, que se cimenta en la justicia y que la justicia busca la verdad. La autoridad es dada a líderes que la ejercen para tomar decisiones buscando respetar la libertad del otro pero asumiendo las responsabilidades de su liderazgo. Por ejemplo papá y mamá tienen autoridad compartida con respecto a la formación de su hijos. Un hermoso fruto de una autoridad bien ejercida es la sujeción y obediencia voluntaria y por conciencia (por ejemplo entre conyuges, de los hijos, de empleados)" (Ver {4}).

Así como había una ley escrita (por Moises), escribamos nuestro manual de convivencia familiar este es buen momento en una hoja de papel por lo menos con: normas, sanciones y premios.   Por ejemplo:

Metanorma:
* El amor cubre multitud de pecados, si el Señor nos revela arrepentimiento de un hijo o hija por una falta perdonemosle la sanción pero pidamos también que nos revele la masa de higo que ayudará a sanar la llaga ---por ejemplo información completa y clara de las consecuencias y motivaciones de las normas.

Normas
* Dar ejemplo como padres, ser consistentes entre lo que se dice y lo que se hace
* Ser agil al comer y hacer tareas
* Tolerar diferencias y espacio que ocupan los demás
* Decir la verdad
* Cuidar lo propio y lo ajeno
* Obedecer a Dios y papás por ejemplo saliendo sólo con permiso
* Papás, al corregir no tener ira ni enojo como dice Colosenses 3:8 "Pero ahora dejad también vosotros todas estas cosas: ira, enojo, malicia, blasfemia, palabras deshonestas de vuestra boca."  Es decir al instante de una situación errada de hijos sacarlos de situaciones de peligro pero esperar hasta que el enojo pase para corregirlos --digamos varias horas después para aplicar sanciones.  No ir a aplicar sanciones de ningún tipo con ira, porque la ira es mala consejera, es sólo un impulso para ayudar a asegurar que uno atenderá el tema (toca buscar estrategías para esto, agua, aire, lo que sea pero dominar la ira con la ayuda de nuestro Señor Jesucristo). 

Sanciones
* Aislar por un momento por ejemplo 1 hora.
* Quitar televisión, salidas y actividades que anhele el niño o niña (pero no privar de venir a la iglesia).
* Poner tareas breves extra (por ejemplo un oficio de la casa que pueda hacer con instrucción).
* En algunos casos que complete tareas pendientes aún cuando tenga que trasnochar un poco (pero no privar de alimento ni de sueño más de 3 horas).
* Investigaciones sobre temas relacionados con la falta, por ejemplo consecuencias de salirse de la casa.
* Evitar castigos físicos y bajo ninguna circunstancia en menores de 4 años o en mayores de 12 años, ni los que produzcan mutilaciones, cicatrices o hematomas.  

Premios
* Salidas recreativas
* Juegos
* Caritas felices o sellos en un tablero especial para esto.
* Abrazos, besos.  Que nuestras manos se identifiquen con protección más que con castigo.

La propuesta es mejorar este manual inicial aquí y en familia en conjunto con hij@s.


##4. Conclusión y Oración

La corrección produce un sufrimiento en quien es corregid@, inevitable, excepto por misericordia.  Quien corrige entonces es quien inflinge ese sufrimiento. La actual redacción del numeral 9 del artículo 39 del Código de Infancia parece evitar entonces todo tipo de corrección, al pretender evitar todo maltrato físico o psicológico.   Siendo altamente incosistente, pues mientras dice evitar todo maltrato a los menores en su familia, maltrata abiertamente a niñ@s y familias a quienes castiga con medidas de restablecimiento de derechos.  Oramos al Señor para que se refuercen los aspectos buenos de la legislación colombiana y podamos cambiar los negativos.

Señor que aprendamos a clamarte, que aprendamos a comunicarnos mejor unos con otros para exponer nuestra situación y que nuestros hijos puedan hablarnos, que aprendamos a escucharlos y a sentirlos también por medio de tu Santo Espíritu.

Que te alabemos, pues una oportunidad que tenemos aquí y ahora.

Como decía Ezequías que como padres y madres hagamos "notoria tu verdad a los hijos".

Señor domina la ira en nosotros, que sólo nos sirva para asegurar que antenderemos las situaciones, pero no como consejera para aplicar sanciones.

Señor guianos en la formulación de normas, sanciones y premios.  Ayudanos a mantenernos al día en esta responsabilidad parental.

Señor así como tu amor y misericordia nos han salvado, limpiado y perdonado, que con amor guiemos a nuestros hijos e hijas hacia ti, que los corrigamos con amor, anhelando perdonar, ayudandoles a recibir misericordia y aplicando la masa de higo que necesitan ---puede ser información.

Ezequías dijo "Jehova me salvará" y "porque echaste tras tus espaldas todos mis pecados," justamente lo escencial del plan de salvación con nuestro Señor Jesucristo en quien por gracia tenemos salvación como dice completo Romanos 6:23 "Porque la paga del pecado es muerte, mas la dádiva de Dios es vida eterna en Cristo Jesús Señor nuestro."   Ya acepté al Señor Jesucristo como mi Señor y Salvador, es importante hacerlo hoy con una [Oración de Fe].


##5. Bibliografía

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/passage/?search=Isa%C3%ADas+38&version=RVR1960
* {2} Código de la Infancia y la Adolescencia. Ley 1098 de 2006. http://www.icbf.gov.co/transparencia/derechobienestar/ley/ley_1098_2006_pr001.html
* {3} Decreto 120 de 2010. http://www.icbf.gov.co/transparencia/derechobienestar/decreto/decreto_0120_2010.html#1
* {4} Cartilla: La Autoridad. Proyecto de Formación para la Vida. Gimnasio Fidel Cano. 2007. http://fpv.gfc.edu.co/?id=Cartilla%3A+La+Autoridad

----
Esta predica se dió en la iglesia menonita La Resurrección de San Nicolas, Soacha  el 26.Jun.2011
