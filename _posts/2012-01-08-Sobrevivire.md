---
layout: post
title: "Sobrevivire"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Lilly Goodman

<pre>
Imagina cuánto podría perder
si decidiera retroceder
Si después de haber luchado tanto tiempo
escogiera negar mi fe
Si en mi vida he sido testigo de
 los milagros que Él puede hacer
Y que aún en los malos tiempos Él
 siempre ha sido fiel

CORO 

Sobreviviré, Sobreviviré,
No creas que desmayaré
No me rendiré, yo me aferraré
A la promesa de aquel
Que me dijo ayer: 
“Yo te guardaré 
No temas que a tu lado iré” 
Y me dio la fe 
y me dio el poder 
Para confiar para  vencer

Han venido tormentas fuertes intentando mi confianza derribar
Pero ha sido en vano porque aquí parada estoy en el mismo lugar
Como búfalo tengo la fuerza que el Poderoso de Israel me da
Preparada con todas las armas lista para la victoria alcanzar

CORO

Sobreviviré, por que Él me da la fuerza
Sobreviviré, Él es quien me sustenta
Sobreviviré, en medio de las pruebas
Sobreviviré, aquí estoy despierta
Sobreviviré
Sobreviviré, Él rompe las cadenas
Sobreviviré, Sobreviviré


//Puedo ser derribado más
 destruido no seré,
El que está a mi lado es
 mucho más fuerte que aquel//

//CORO//

</pre>

* Video: http://www.youtube.com/watch?v=CgBpAchVuEc
* Letra basada en: http://www.musica.com/letras.asp?letra=941463
