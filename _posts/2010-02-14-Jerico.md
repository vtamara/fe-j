---
layout: post
title: "Jerico"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Nombre

Jerico.  Fragante. {2} &#1497;&#1456;&#1512;&#1460;&#1497;&#1495;&#1493;&#1465;

##Referencias bíblicas

* Desde números 22:1 se relata  que los hijos de Israel acamparon en los campos de Moab junto al Jordán, frente a Jericó.  
* Dios le mostró a Moises lo que habrían de heredera los hijos de Israel en el monte Nebo, la cumbrel del Pisga, que está enfrente de Jericó.  Deut 34:1
* Josue envió espias a la ciudad de Jericó, de donde fueron librados con ayuda de Rahab. Josue 2
* En Jouse 6 se relata como Dios destruyó los muros ---dieron 7 vueltas durante 7 días---, los Israelitas entraron y destruyeron todo (Rahab fue librada) y quemaron la ciudad.
* En Jeremias 39:5 y 52:8  se relata que durante la toma de Babilonia a Israel y Juda el entonces rey de Juda Sedequias fue alcanzado por los caldeos en los llanos de Jericó.
* Según Mateo, Marcos y Lucas, Jesús estuvo en Jericó. Mat 20:29, Mar 10:46, Luc 18:35, Luc 19:1
* En la parábola del buen samaritano, en Lucas 10:30, Jesús habla de un hombre que iba de Jerusalén a Jericó.

##Ubicación

[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/imagenes/map11.jpg]

Se trata de una ciudad a unos 20km de Jerusalén y 16 km del mar muerto es la ciudad que está más abajo a 244t bajo el nivel del mar. Actualmente tiene unos 20000 habitantes {2}, {8}

[http://www.galenfrysinger.com/Photos/palestine01.jpg]

##Arqueología

Excavada desde el siglo 18.  Hay restos que evidencian que es una de las ciudades más antiguas {3}, y que se construyó y reconstruyó varias veces.

Con respecto a la ciudad de la epoca de Bronce, John Garstang (1930-1936) y Kathleen Kenyon (1952-1958) encontraron paredes destruidas, graneros quemados así como evidencia de que toda la ciudad fue quemada.

Los muros destruidos concuerdan con la narrativa de Josue. 
[http://www.biblearchaeology.org/image.axd?picture=Dr.-Wood-at-base-of-Jericho.jpg]

* Garstang que excavo de 1907 y 1909 propuso un datación hacía el 1400aC
* Kenyon que excavo hacia 1950 propuso 1550aC.  
* Bryant Wood en 1991 analizó alfarería encontrada por ambos y concluyó que era del 1400aC (ver {10}).   
* La datación de Wood ha sido atacada por ejemplo por {4} que apoya datación de Kenyon, a partir de datación con 14C de 1995.  Aunque en 1999 Wood reafirma la datación alrededor de 1400aC para la destrucción de los muros (ver {5}).  Según {6} hay contradicción entre dataciones de 14C para le época y dataciones de los egiptologos.

[http://www.answersingenesis.org/assets/images/articles/cm/v21/i2/212wall_sketch.jpg]
Reconstrucción de un artista con base en excavación de Garstang (ver {7}).

##Referencias

* {2} http://en.wikipedia.org/wiki/Jericho
* {3} http://www.jericho-city.org/etemplate.php?id=15
* {4} Is Bryant Wood's chronology of Jericho valid?. http://www.biblicalchronologist.org/answers/bryantwood.php
* {5} http://www.biblearchaeology.org/post/2008/06/The-Walls-of-Jericho.aspx
* {6} http://en.wikipedia.org/wiki/Thera_Eruption#Date
* {7} http://www.answersingenesis.org/Home/Area/Magazines/images/212wall_sketch.jpg  en http://www.answersingenesis.org/creation/v21/i2/jericho.asp
* {8} http://www.galenfrysinger.com/palestine.htm
