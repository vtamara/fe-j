---
layout: post
categories: []
title: Un niño liberador en perfecto tiempo. Isaías 9:1-7
author: vtamara
image: "/assets/images/214517.png"

---
Un niño liberador en perfecto tiempo. Isaías 9:1-7

Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 12.Dic.2021

# 1 Introducción

Estamos en el tiempo de la celebración del nacimiento de Jesús nuestro Señor y Salvador. En el antiguo testamento hay muchas profecías sobre Él y su nacimiento, porque Dios había preparado su propio advenimiento cuidadosamente y lo había revelado en todo el Antiguo Testamentos desde el Pentateuco, pasando por Salmos y hasta los Profetas. Reveló muchos detalles incluso respecto al tiempo en el que vendría.

La aplicación central de esto es que Dios nos quiere liberar del pecado, salvarnos, darnos Espíritu Santo y ricas bendiciones --como las del Salmo 112--, pero debemos ir a Él arrepentidos, cargar nuestra cruz y esperar las promesas que nos ha hecho (y las que nos hará) que se cumplirá cuando Él disponga perfectamente.

# 

# 2. Texto: Isaías 9:1-7

**1** Mas no habrá siempre oscuridad para la que está ahora en angustia, tal como la aflicción que le vino en el tiempo que livianamente tocaron la primera vez a la tierra de Zabulón y a la tierra de Neftalí; pues al fin llenará de gloria el camino del mar, de aquel lado del Jordán, en Galilea de los gentiles. **2** El pueblo que andaba en tinieblas vio gran luz; los que moraban en tierra de sombra de muerte, luz resplandeció sobre ellos. **3** Multiplicaste la gente, y aumentaste la alegría. Se alegrarán delante de ti como se alegran en la siega, como se gozan cuando reparten despojos. **4** Porque tú quebraste su pesado yugo, y la vara de su hombro, y el cetro de su opresor, como en el día de Madián. **5** Porque todo calzado que lleva el guerrero en el tumulto de la batalla, y todo manto revolcado en sangre, serán quemados, pasto del fuego. **6** Porque un niño nos es nacido, hijo nos es dado, y el principado sobre su hombro; y se llamará su nombre Admirable, Consejero, Dios Fuerte, Padre Eterno, Príncipe de Paz. **7** Lo dilatado de su imperio y la paz no tendrán límite, sobre el trono de David y sobre su reino, disponiéndolo y confirmándolo en juicio y en justicia desde ahora y para siempre. El celo de Jehová de los ejércitos hará esto.

# 

# 3. Contexto

Hay muchas profecías sobre Jesús en el antiguo testamento, hay quienes hablan de unas 350 (ver \[P350\]) y de una predica anterior mia pueden verse 28 (ver \[P28\]) pero vamos a retomar brevemente de \[P5\] un listado de 5 sobre su nacimiento:

| --- | --- |
| Profecía en antiguo testamento | Cumplimiento en nuevo testamento |
| Por tanto, el Señor mismo os dará señal: He aquí que la virgen concebirá, y dará a luz un hijo, y llamará su nombre Emanuel. Isaías 7:14 (Emanuel significa Dios con nosotros) | Al sexto mes el ángel Gabriel fue enviado por Dios a una ciudad de Galilea, llamada Nazaret, a una virgen desposada con un varón que se llamaba José, de la casa de David; y el nombre de la virgen era María. Lucas 1:26-27Entonces el ángel le dijo: María, no temas, porque has hallado gracia delante de Dios. Y ahora, concebirás en tu vientre, y darás a luz un hijo, y llamarás su nombre JESÚS.Lucas 1:30-31 (Jesús significa Jehova Salva) |
| Pero tú, Belén Efrata, pequeña para estar entre las familias de Judá, de ti saldrá el que será Señor en Israel; y sus salidas son desde el principio, desde los días de la eternidad. Miqueas 5:2 | Y José subió de Galilea, de la ciudad de Nazaret, a Judea, a la ciudad de David, que se llama Belén, por cuanto era de la casa y familia de David; para ser empadronado con María su mujer, desposada con él, la cual estaba encinta….Y dio a luz a su hijo primogénito … Lucas 2:4-7 |
| Porque un niño nos es nacido, hijo nos es dado, y el principado sobre su hombro; y se llamará su nombre Admirable, Consejero, Dios Fuerte, Padre Eterno, Príncipe de Paz. Lo dilatado de su imperio y la paz no tendrán límite, sobre el trono de David y sobre su reino, disponiéndolo y confirmándolo en juicio y en justicia desde ahora y para siempre. El celo de Jehová de los ejércitos hará esto. Isaías 9:6-7 | que os ha nacido hoy, en la ciudad de David, un Salvador, que es CRISTO el Señor Lucas 2:11 |
| Lo veré, mas no ahora; Lo miraré, mas no de cerca; Saldrá ESTRELLA de Jacob, Y se levantará cetro de Israel,Y herirá las sienes de Moab, Y destruirá a todos los hijos de Set. Números 24:17 | Cuando Jesús nació en Belén de Judea en días del rey Herodes, vinieron del oriente a Jerusalén unos magos, diciendo: ¿Dónde está el rey de los judíos, que ha nacido? Porque su estrella hemos visto en el oriente, y venimos a adorarle Mateo 2:1-2 |
| Multitud de camellos te cubrirá; dromedarios de Madián y de Efa; vendrán todos los de Sabá; traerán oro e incienso, y publicarán alabanzas de Jehová. Isaias 60:6 | Y al entrar en la casa, vieron al niño con su madre María, y postrándose, lo adoraron; y abriendo sus tesoros, le ofrecieron presentes: oro, incienso y mirra. Mateo 2:11 |

En los versículos revisados hasta ahora hemos visto como se había profetizado con gran precisión:

* Que nacería en Belén
* Que nacería de una virgen
* Que saldría una estrella
* Que le darían oro e incienso
* Que su ministerio se desarrollaría en Galilea

# 4. Análisis

Con ideas de Matthew Henry:

| --- | --- |
| Versículos | Observación |
| 1 | En el capítulo anterior de Isaías se relatan sufrimientos del pueblo de Israel que venían por su desobediencia. En este se da esperanza por el primer advenimiento del salvador. Hoy quienes no creen tienen esperanza en el evangelio y hoy quienes creemos tenemos esperanza en el segundo advenimiento de Cristo. |
| 2 | Por unos 300 años Israel no tuvo profetas (vivió en oscuridad tras la muerte de Malaquías) hasta el nacimiento de Jesucristo. |
| 3 | Recordamos la alegría de los pastores que corrieron a conocer al niño recién nacido o de los reyes que se apresuraron a llevarle regalos. Así es la alegría de quienes reciben y comparten el evangelio. |
| 4 | Así como Gedon liberó a Israel de los Madianitas hacia el 1200aC, así Jesús libró a los judios y gentiles que creyeron en Él y hoy sigue librando a quienes creamos. |
| 5 | Gedeón no tuvo que pelear sino que Dios le dió la victoria (aún sin ejército). Hoy no tenemos que derramar sangre ni hacer obras extraordinarias para recibir salvación. Es la fe y sobre todo la gracia de nuestro Señor (aunque una vez salvos cuidemos la salvación con nuestro esfuerzo por obedecer a Dios). |
| 6 | Este versículo revela la trinidad y a la vez que es un solo Dios. Jesús es el Emanuel es Dios con nosotros. |
| 7 | Los creyentes que conformamos la iglesia hacemos parte de ese imperio creciente de Cristo que llegó en un momento perfecto. |

Analizaremos en detalle este versículo 7.

Ese reino de Cristo además de espiritual es terrenal, porque en la tierra está la iglesia, y como imperio terrenal tenía que destruir todos los otros imperios que han clamado ser de toda la tierra y que han existido o existen.

Como esto ha ocurrido se narra Daniel 2: el rey Nabuconodosor, rey de Babilonia que había destruido Jerusalén en el año 587aC y había llevado muchos judios cautivos, incluyendo a Daniel, a Babilonia, tuvo un sueño y amenazó con matar a todos los sabios y adivinos si no le revelaban el sueño y su interpretación. La misión era imposible, excepto para Dios quien por intermedio de Daniel le dijo a Nabuconodosor el sueño que había tenido y su interpretación

## Daniel 2:25-45

25 Entonces Arioc llevó prontamente a Daniel ante el rey, y le dijo así: He hallado un varón de los deportados de Judá, el cual dará al rey la interpretación. 26 Respondió el rey y dijo a Daniel, al cual llamaban Beltsasar: ¿Podrás tú hacerme conocer el sueño que vi, y su interpretación? 27 Daniel respondió delante del rey, diciendo: El misterio que el rey demanda, ni sabios, ni astrólogos, ni magos ni adivinos lo pueden revelar al rey. 28 Pero hay un Dios en los cielos, el cual revela los misterios, y él ha hecho saber al rey Nabucodonosor lo que ha de acontecer en los postreros días. He aquí tu sueño, y las visiones que has tenido en tu cama: 29 Estando tú, oh rey, en tu cama, te vinieron pensamientos por saber lo que había de ser en lo por venir; y el que revela los misterios te mostró lo que ha de ser. 30 Y a mí me ha sido revelado este misterio, no porque en mí haya más sabiduría que en todos los vivientes, sino para que se dé a conocer al rey la interpretación, y para que entiendas los pensamientos de tu corazón.

31 Tú, oh rey, veías, y he aquí una gran imagen. Esta imagen, que era muy grande, y cuya gloria era muy sublime, estaba en pie delante de ti, y su aspecto era terrible. 32 La cabeza de esta imagen era de oro fino; su pecho y sus brazos, de plata; su vientre y sus muslos, de bronce; 33 sus piernas, de hierro; sus pies, en parte de hierro y en parte de barro cocido. 34 Estabas mirando, hasta que una piedra fue cortada, no con mano, e hirió a la imagen en sus pies de hierro y de barro cocido, y los desmenuzó. 35 Entonces fueron desmenuzados también el hierro, el barro cocido, el bronce, la plata y el oro, y fueron como tamo de las eras del verano, y se los llevó el viento sin que de ellos quedara rastro alguno. Más la piedra que hirió a la imagen fue hecha un gran monte que llenó toda la tierra.

36 Este es el sueño; también la interpretación de él diremos en presencia del rey. 37 Tú, oh rey, eres rey de reyes; porque el Dios del cielo te ha dado reino, poder, fuerza y majestad. 38 Y dondequiera que habitan hijos de hombres, bestias del campo y aves del cielo, él los ha entregado en tu mano, y te ha dado el dominio sobre todo; tú eres aquella cabeza de oro. 39 Y después de ti se levantará otro reino inferior al tuyo; y luego un tercer reino de bronce, el cual dominará sobre toda la tierra. 40 Y el cuarto reino será fuerte como hierro; y como el hierro desmenuza y rompe todas las cosas, desmenuzará y quebrantará todo. 41 Y lo que viste de los pies y los dedos, en parte de barro cocido de alfarero y en parte de hierro, será un reino dividido; mas habrá en él algo de la fuerza del hierro, así como viste hierro mezclado con barro cocido. 42 Y por ser los dedos de los pies en parte de hierro y en parte de barro cocido, el reino será en parte fuerte, y en parte frágil. 43 Así como viste el hierro mezclado con barro, se mezclarán por medio de alianzas humanas; pero no se unirán el uno con el otro, como el hierro no se mezcla con el barro. 44 Y en los días de estos reyes el Dios del cielo levantará un reino que no será jamás destruido, ni será el reino dejado a otro pueblo; desmenuzará y consumirá a todos estos reinos, pero él permanecerá para siempre, 45 de la manera que viste que del monte fue cortada una piedra, no con mano, la cual desmenuzó el hierro, el bronce, el barro, la plata y el oro. El gran Dios ha mostrado al rey lo que ha de acontecer en lo por venir; y el sueño es verdadero, y fiel su interpretación.

Analizando en detalle la interpretación y los eventos históricos confirmados por la historia secular tradicional se ve el perfecto cumplimiento de todo lo revelado a Daniel. Esperamos que la siguiente línea de tiempo resulte de ayuda, en particular para ver la anticipación de cientos de años de esta profecía --y las otras mencionadas antes:

![](https://lh3.googleusercontent.com/us0qYc1QZRLWgvbexO7Ef8kwgy7kX6znRY1PZyV1LXvAw3zDCPjnE6IpFHgujriVs1ojjEFKrMwqKKBkWssQ65iqunmIQK5Y6iW2qSTaFQ6IdeVum4hw1gnBaTncolAMMILz5VuR =665x156)

[https://time.graphics/es/line/594239](https://time.graphics/es/line/594239)

Entonces vemos como el imperio de Cristo llegó en tiempo perfecto para destruir 
lo imperios terrenales y liberar como dice Gálatas 4:4,:

_Pero cuando vino el cumplimiento del tiempo, Dios envió a su Hijo, 
nacido de mujer y nacido bajo la ley_

Fue cumplimiento de tiempo:

* En la naturaleza con los astros moviéndose de manera perfecta para guiar a los 
  sabios de oriente a donde estaba el niño, mediante una estrella muy especial. 
  Una hipótesis verificable de esto puede verse en la película La estrella de 
  Belén (agradecemos a Brad y Tim por la referencia)
* Cuando destruirá los demás reinos terrenales según Dios nos reveló en 
  Daniel 2:25-45
* Hay interpretaciones de Daniel 9:20-27 según las cuales profetiza   
  el advenimiento de Cristo y su ministerio.

## Daniel 9:20-27

20 Aún estaba hablando y orando, y confesando mi pecado y el pecado de mi pueblo 
Israel, y derramaba mi ruego delante de Jehová mi Dios por el monte santo de mi Dios; 
21 aún estaba hablando en oración, cuando el varón Gabriel, a quien había 
visto en la visión al principio, volando con presteza, vino a mí como a la 
hora del sacrificio de la tarde. 22 Y me hizo entender, y habló conmigo, 
diciendo: Daniel, ahora he salido para darte sabiduría y entendimiento. 
23 Al principio de tus ruegos fue dada la orden, y yo he venido para 
enseñártela, porque tú eres muy amado. Entiende, pues, la orden, y entiende 
la visión.

24 Setenta semanas están determinadas sobre tu pueblo y sobre tu santa ciudad, 
para terminar la prevaricación, y poner fin al pecado, y expiar la iniquidad, 
para traer la justicia perdurable, y sellar la visión y la profecía, y ungir al 
Santo de los santos. 25 Sabe, pues, y entiende, que desde la salida de la orden 
para restaurar y edificar a Jerusalén hasta el Mesías Príncipe, habrá 
siete semanas, y sesenta y dos semanas; se volverá a edificar la plaza y el 
muro en tiempos angustiosos. 26 Y después de las sesenta y dos semanas se 
quitará la vida al Mesías, mas no por sí; y el pueblo de un príncipe que ha de 
venir destruirá la ciudad y el santuario; y su fin será con inundación, y 
hasta el fin de la guerra durarán las devastaciones. 27 Y por otra semana 
confirmará el pacto con muchos; a la mitad de la semana hará cesar el sacrificio 
y la ofrenda. Después con la muchedumbre de las abominaciones vendrá el 
desolador, hasta que venga la consumación, y lo que está determinado se 
derrame sobre el desolador.

Dos ejemplos de interpretaciones para las setenta semanas son:

* Peter Gentry (citado en \[Chase2018\]) y otros dicen que cada semana 
se refiere a un periodo sabático de 7 años, y comienza a contar más o menos 
con la restauración  de los muros de Jerusalén cuando volvían del exilio.
Se completarían con el nacimiento de Jesús.
* Dan Bruce (ver \[HITO\]) propone que cada semana se refiere al tiempo entre
un pentecostes y otro, es decir un año, y que debe comenzar a contarse desde
el pentecostes del año 44 a.C con un decreto de Julio Cesar permitiendo al 
sacerdote Judio Hyrcanus II y al pueblo judío la reconstrucción de parte de 
los muros de Jerusalen que estaban destruidos. Con esta interpretación las 
70 semanas terminarían en el pentecostes del año 28 d.C (después de la muerte
de Jesús en su cronología, cuando el Espíritu Santo habría sido derramado en 
los discípulos), siendo el bautismo de Jesús por parte de Juan Bautista 
un momento importante en la semana 70 que interpretaría "ungir al Santo 
de los santos".


# 5. Conclusión y oración

Así como Dios le daba esperanza al pueblo judio que estaba en calamidad con el primer advenimiento de Jesús a nosotros también nos da hoy esperanza en medio de las dificultades del mundo y personales, con el segundo advenimiento de Jesús.

Conocer el evangelio es como ser restacado de tinieblas a una gran y dulce luz. Compartamos el evangelio con denuedo para llevar luz y alegría.

Jesús y su Reino llegaron en un tiempo perfecto para la liberación de la humanidad del pecado: (1) en la naturaleza con la estrella, (2) en la historia humana con la destrucción de los demás reinos y (3) para el cumplimiento de todas las profecías (incluyendo la de tiempo).

Los tiempos de Dios en nuestras vidas son perfectos, todas sus promesas, incluyendo el Espíritu Santo y todas las promesas del Salmo 112 de hoy (generación bendita, bienes y riqueza --nada falta-- en casa, justicia para siempre, gobernar asuntos con juicio, no resbalará, no temer malas noticias, corazón firme, tener suficiente para compartir) para quienes crean tiene cumplimiento en tiempo perfecto. Tengamos esperanza en Él, en sus promesas y en su próximo segundo retorno.

Si aún no has recibido a Jesús en el corazón, hoy es tiempo perfecto para que goces de la liberación del pecado que Jesúcristo ha ganado para quienes creen, para que tengas esperanza y para que te conviertas en ladrillo de Su iglesia y disfrutes el cumplimiento de las promesas de Dios para su pueblo, te invitamos a hacer una oración de Fe.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* \[P350\] [https://livingwayfontana.org/wp-content/uploads/2017/07/351-Prophecies-Fulfilled-in-Jesus-Christ.pdf](https://livingwayfontana.org/wp-content/uploads/2017/07/351-Prophecies-Fulfilled-in-Jesus-Christ.pdf "https://livingwayfontana.org/wp-content/uploads/2017/07/351-Prophecies-Fulfilled-in-Jesus-Christ.pdf")
* \[P28\] [https://fe.pasosdejesus.org/Biblia_Centro_Jesucristo/](https://fe.pasosdejesus.org/Biblia_Centro_Jesucristo/ "https://fe.pasosdejesus.org/Biblia_Centro_Jesucristo/")
* \[P5\] [https://calvarycurriculum.com/pdf/Curriculum/Revised-Espa%C3%B1ol/NT001%20-%20LAS%20PROFEC%C3%8DAS%20DEL%20NACIMIENTO%20DE%20JES%C3%9AS%20](https://calvarycurriculum.com/pdf/Curriculum/Revised-Espa%C3%B1ol/NT001%20-%20LAS%20PROFEC%C3%8DAS%20DEL%20NACIMIENTO%20DE%20JES%C3%9AS%20 "https://calvarycurriculum.com/pdf/Curriculum/Revised-Espa%C3%B1ol/NT001%20-%20LAS%20PROFEC%C3%8DAS%20DEL%20NACIMIENTO%20DE%20JES%C3%9AS%20")[(Espa%C3%B1ol).pdf](https://calvarycurriculum.com/pdf/Curriculum/Revised-Espa%C3%B1ol/NT001%20-%20LAS%20PROFEC%C3%8DAS%20DEL%20NACIMIENTO%20DE%20JES%C3%9AS%20(Espa%C3%B1ol).pdf)
* \[Chase2018\] [https://www.crossway.org/articles/what-are-the-seventy-weeks-of-daniel-daniel-9/](https://www.crossway.org/articles/what-are-the-seventy-weeks-of-daniel-daniel-9/ "https://www.crossway.org/articles/what-are-the-seventy-weeks-of-daniel-daniel-9/")
* \[HITO\] Bruce, Dan. He is the One: Proof from the Hebrew Scriptures that Jesus was (and is) the Jewish Mesiah. 2017. [http://www.prophecysociety.org/PDF/HITO_FREE.pdf](http://www.prophecysociety.org/PDF/HITO_FREE.pdf)

Imagén de dominio público [https://openclipart.org/detail/214517/bible-and-bookmark](https://openclipart.org/detail/214517/bible-and-bookmark "https://openclipart.org/detail/214517/bible-and-bookmark")
