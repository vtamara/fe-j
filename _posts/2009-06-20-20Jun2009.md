---
layout: post
title: "20Jun2009"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Anoche un sueño que no recuerdo concluyó con "Isaias 43", desperté de inmediato, no ví la hora, pero abrí la Biblia:

<pre>
    1 Ahora, así dice Jehová, Creador tuyo, oh Jacob, y Formador tuyo, oh Israel: No temas, porque yo te redimí; te puse nombre, mío eres tú.

    2 Cuando pases por las aguas, yo estaré contigo; y si por los ríos, no te anegarán. Cuando pases por el fuego, no te quemarás, ni la llama arderá en ti.

    3 Porque yo Jehová, Dios tuyo, el Santo de Israel, soy tu Salvador; a Egipto he dado por tu rescate, a Etiopía y a Seba por ti.

    4 Porque a mis ojos fuiste de gran estima, fuiste honorable, y yo te amé; daré, pues, hombres por ti, y naciones por tu vida.

    5 No temas, porque yo estoy contigo; del oriente traeré tu generación, y del occidente te recogeré.

    6 Diré al norte: Da acá; y al sur: No detengas; trae de lejos mis hijos, y mis hijas de los confines de la tierra,

    7 todos los llamados de mi nombre; para gloria mía los he creado, los formé y los hice. 
</pre>

Doy gracias a Dios y comparto la bendición con quien lee.

http://www.biblegateway.com/passage/?book_id=29&chapter=43&version=60
