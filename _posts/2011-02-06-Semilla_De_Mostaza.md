---
layout: post
title: Semilla de Mostaza
author: vtamara
categories:
- Prédica
image: "/assets/images/mt13_col.jpg"
tags: 

---
Veamos una parábola de Marcos 4:30-32 (también se presenta de manera similar en Mt 13:31-32 y en Lc 13:19)

|---|
|30 Dijo también ¿A que haremos semejante el Reino de Dios, o con que parábola lo compararemos?|
|31 Es como el grano de mostaza que cuando se siembra en tierra es la más pequeña de todas las semillas que hay en la tierra;|
|32 Pero después de sembrado crece y se hace la mayor de todas las hortalizas, y echa grandes ramas, de tal manera que las aves del cielo pueden morar bajo su sombra.|

Se trata de otra parábola que se refiere a términos agrícolas, y es que en su tiempo la sociedad era eminentemente agrícola (ver {9}), y Él como buen maestro explica lo nuevo desde lo que su audiencia ya conoce, como confirma a continuación Mr 4:33 que en {10} dice "De esta manera les enseñaba Jesús el mensaje, por medio de muchas parábolas como estas, según podían entender."

## Botánica

Aunque Jesús estaba enseñando una parábola a la gente de a pie de su tiempo y no estaba dando una clase de botánica (ver {12}), podemos releer esta parábola sin entrar en contradicción con la botánica hoy conocida. La traducción de Reina Valera podría hacer pensar que sería la más pequeña de todas las semillas que pueden existir sobre el planeta tierra, pero hay semillas más pequeñas (como la de la orquidea ver {12}).   Sin embargo en los diversos manuscritos griegos que contienen el versículo 31, la palabra γῆ (ge) aparece 2 veces y aunque puede traducirse como planeta tierra también se traduce como tierra arable --que sin lugar a dudas es la forma en la que se usa la primera vez en este versículo.  Recordando su audiencia y el contexto de esta parábola ambas podrían traducirse como la misma tierra arable, como el mismo campo, o como el mismo huerto y en un huerto típico de su tiempo, la semilla de mostaza era la más pequeña (ver {12}).   Que se refiere a un huerto es más claro en los versículos paralelos (ver {13}) Mateo 13:31 "Otra parábola les refirió, diciendo: El reino de los cielos es semejante al grano de mostaza, que un hombre tomó y sembró en su campo;" y es evidente del comienzo de Lucas 13:19 "Es semejante al grano de mostaza, que un hombre tomó y sembró en su huerto; ..."

Por otra parte algunos de los que hayamos visto alguna planta de mostaza (el señor me permitió sembrar unas en el 2004), de pronto hemos visto una planta bajita (como de 1m) y no con grandes ramas.  Sin embargo hay varias especies de mostaza:

| Blanca | Sinapis alba o Brassica alba | Semillas entre 1 y 1.5mm (ver {3})|
| Café | Brassica juncea ||
| Del campo | Brassica rapa | |
| Negra | Brassica nigra | Semillas de 1mm (ver {5}), la planta crece entre 60cm y 2.4m |
| Etiope | Brassica carinata | (ver {6})  |
| Salvaje | Brassica oleracea | Llega a crecer entre 1 y 2 metros (ver {7}) |

Según {8} la Brassica nigra o Mostaza negra era el tipo de mostaza cultivado en la región de Jesús (había otras silvestres), cuya semilla es muy pequeña (1mm) y que puede llegar a ser bien alta (2.4m), como para que sus ramas den sombra a pajaritos.

Personalmente he sembrado varias semillas, pero otras que dan plantas más grandes (como el maíz) son mucho más grandes que la semilla de mostaza (ni que decir de las semillas de árboles) y otras que son pequeñas (como la de la zanahoria) dan plantas pequeñas. Entonces como hace notar Spurgeon en {11} tal vez el Señor eligió esta planta por la gran proporción entre el tamaño de la planta madura (2m) y el tamaño de la pequeña semilla (1mm).

## Interpretando

Hemos dicho que el Reino de Dios es el Reino de Jesús y se vive en su Reino cuando a Él se le acepta como Rey (ver <https://fe.pasosdejesus.org/?id=Preparados>).  Pero ¿Cómo aceptar como Rey al que fue crucificado?
Indudablemente requerimos fe, tal como dice en diversas partes de la Biblia y en partícular en Juan 3:16
```
Porque de tal manera amó Dios al mundo que ha dado a su hijo
unigénito para que todo aquel que en Él crea, no se pierda más
tenga vida eterna.
```

Entonces la fe en Jesucristo como Salvador y Rey y la esperanza en el Reino de Dios por Él fundado y proclamado son equivalentes.

A la luz de esa equivalencia revisemos nuevamente la parábola:

La fe en Cristo como Salvador, Señor y Dios, es como una pequeña semilla de mostaza que en lugar de sembrarse en el huerto, uno siembra en su corazón y empieza a crecer, sin que uno entienda como, si acaso se abona o se riega, aunque típicamente el sol, la lluvía y los nutrientes del suelo que envía el Señor bastan junto con muchos procesos químicos por Él diseñados para que se convierta en la planta más grande del huerto, en lo más importante del corazón, y su sombra puede servir a pajaritos.

Una interpretación concordante de este pasaje, del predicador Spurgeon (ver {11}) del siglo XIX comparó el trabajo de sembrar la semilla con el trabajo de los profesores de escuela dominical que siembran la palabra de Dios en los niños para que crezca junto con ellos.  La diminuta semilla también sería la fe en que "Cristo es Salvador", esa es la humilde semilla que parece insignificante frente a otras enseñanzas de ciencias, ética, historia, etc. pero que en realidad es la roca fundamental pues es palabra viva que es la única que da vida y vida en abundancia.   Por eso invita a sembrar esa pequeña semilla con fe, con fe en los frutos que dará, no por recompensas, no porque nos vean, sino porque es lo que da vida.

## Aplicando

La parábola sin embargo resalta las ramas que permiten a las aves resguardarse por un tiempo (según {8} la palabra en griego se refiere más a pasar un tiempo que a anidar).

¿Cuales son las ramas de nuestra fe en Jesús? ¿Cómo pueden llegar aves a resguardarse en ellas?  Que tal:
|---|
| Cuando oramos por otras y otros (por sanidad, liberación, consolación, fortaleza, trabajo, prosperidad, tranquilidad, etc). |
| Cuando damos un consejo sabio inspirado por el Espíritu Santo |
| Cuando el Señor nos usa en profecía |
| Cuando preparamos una prédica |
| Cuando soy amoroso y solidario con quien lo necesita |

¿Que más?

De mi breve experiencia sembrando mostaza en el 2004, me impactó la gran cantidad de frutos (semillas) que una planta daba y la facilidad y rapidez con que crecía.

¿Qué bueno sería que nuestra fe diera mucho fruto y pronto verdad?  Qué predicaramos esa fe para que por el oir y el obrar del Espíritu Santo más personas aceptaran a Cristo como Rey.

Tanto esas posibles ramas de nuestra fe como las semillas por plantar requieren nuestra voluntad y la mano del Señor y su Santo Espíritu:

* Su protección para que crezca esa fe
* Su alimento proveniente del sol, la lluvia y nutrientes del suelo
* Su poda o renovación e incluso la posibilidad de rehacerla por completo.
* Su obrar sobrenatural para que una vez madura salgan semillas y para que esas semillas lleguen a otros, se siembre y repitan el proceso pero siempre de manera única y maravillosa.

### La poda

Repasemos Jeremias 18:1-9.

|---|
|Palabra de Jehová que vino a Jeremías, diciendo: Levántate y vete a casa del alfarero, y allí te haré oír mis palabras. Y descendí a casa del alfarero, y he aquí que él trabajaba sobre la rueda.  Y la vasija de barro que él hacía se echó a perder en su mano; y volvió y la hizo otra vasija, según le pareció mejor hacerla. Entonces vino a mí palabra de Jehová, diciendo:  ¿No podré yo hacer de vosotros como este alfarero, oh casa de Israel? dice Jehová. He aquí que como el barro en la mano del alfarero, así sois vosotros en mi mano, oh casa de Israel. En un instante hablaré contra pueblos y contra reinos, para arrancar, y derribar, y destruir. Pero si esos pueblos se convirtieren de su maldad contra la cual hablé, yo me arrepentiré del mal que había pensado hacerles, y en un instante hablaré de la gente y del reino, para edificar y para plantar. |

En ocasiones nuestra fe se estropea, puede que una poda del Señor baste, pero en ocasiones se requiere Su reconstrucción.  Dispongámonos para que el Señor nos pode, para que nos quebrante y reconstruya nuestra fe, no sólo hoy sino muchas veces, pero aprovechemos hoy.

Pensemos ¿Qué nos está sobrando? ¿Qué nos está arruinando la fe? ¿Qué nos está haciendo desobedecer al Señor? en los diversos aspectos de nuestra vida:

| **Aspecto** | **Me arruina la fe** | **Me hace desobedecer a Jehova** |
|-------------|----------------------|----------------------------------|
| Mi relación con Dios | | |
| Mi relación conmigo mismo | | |
| En familia | | |
| En iglesia | | |
| En vecindario | | |
| En trabajo | | |
| Amistades | | |
| Mi proyecto de vida | | |
| Otros | | |

Pidámosle hoy al Señor que pode de nosotros eso que nos arruina la fe y nos hace desobedecerle, así podremos cargar nuestra cruz y seguirlo sin mirar atras.

### La protección de la semilla

Es una obra del Señor, pero podemos contribuir evitando lo que daña la fe.  Renovando la que tenemos con la lectura diaría de su Palabra. Dandole a Cristo nuestro amor sincero.

### Alimentando la fe

Anhelemos el alimento espiritual del Señor, veamos la obra del Señor porque es maravillosa, veamosla en la naturaleza en lo grande y en lo pequeño, en sus ayudas día a día. Pidamos ver su gloria y su poder porque sus milagros nos renueva la fe.

Pidamosle que nos toque, que presenciemos su Santo Espíritu en nosotros, que sople sobre nosotros o hierva como fuego en nuestro interior para alabarlo y bendecirlo.

Oremos, pidamos al Señor que nos bendiga espiritualmente y nos provea lo necesario, intercedamos por otros y esperemos y proclamemos sus respuestas.

Por cierto frente a los procesos físicos de la vida --como el crecimiento de la semilla de mostaza-- el desconocimiento de la humanidad sigue siendo abrumador.  Por ejemplo se han tenido que descartar teorías del origen de la vida como resultado de la formación espontanea de DNA y RNA por su complejidad. Aunque químicamente la formación del RNA sería más simple, en el 2004 el químico Leslie Orgel declaró en un artículo aceptado en las publicaciones científicas tradicionales: “La sintesis prebiótica de nucleótidos en un estado suficientemente puro para soportar la sintesis de RNA no puede lograrse usando la química que actualmente se conoce" (ver {2}). No sabemos ni cómo comenzó la vida ni como crece la semilla de mostaza, seguimos como hace 2000 años declaró Marcos 4:27 "y la semilla brota y crece sin que él sepa cómo."

### Sembremos

Revisemos nuestro proyecto de vida, y mejoremoslo de forma que nos sirva para sembrar la semilla de fe en Cristo en corazones, de forma que podamos cuidar la fe en nuestro corazón y en otros.

Preparemonos para servirle mejor al Señor, para concordar mi proyecto de vida con su voluntad agradable y perfecta.  Los errores que he cometido y que cometan me harán más dificil la tarea, pero en Él avanzaré.

## Oración de conclusión

Señor y buen Dios, gracias por darnos la oportunidad de creer en Cristo, por la simpleza de "Creer en Cristo y ser salvo", lo imposible Tu lo haces posible y además sencillo.

Señor te pedimos que cuides nuestra fe, Señor que nada pueda arrebatarla, que esa fe se convierta en amor sincero a Cristo porque nada puede separarnos de tu amor.

Señor te pedimos que aumentes nuestra fe, que nos muestres directamente tu gloria resplandeciente, que hagas llover sobre nosotros bendición y respuesta a oraciones, que nos permitas conocer tus trabajadores en esta tierra para que nos ayudemos a renovarnos y aumentarnos mutuamente esa fe.

Señor te ruego que podes en mi lo que no sirve, que podes las excusas, la pereza, el desorden, el orgullo, el egoismo, el chisme, el odio, la indiferencia, los ídolos y todo lo que haga daño a la fe correcta y todo lo que me incline a desobedecerte.

Señor que esa fe tenga ramas que sirvan, dame dones para servir, para que resplandezca tu gloria y respaldame para que los pueda usar mucho y bien. Dame don de lenguas, de profecía, de ciencia, discernimiento, sanidad, enseñanza, de servicio, guíame, hazme amoroso y solidario, revísteme de autoridad para liberar, para expulsar lo que no es tuyo en tu nombre precioso.  Pero aumenta mi humildad también para que todo que lo haga sea para tu gloria.

Señor quiero ser instrumento tuyo, Señor usame, que pueda ayudar a plantar la semilla de la fe en muchos corazones, hazme dar fruto abundante y ayudame en el trabajo de sembrarlo y ayudar a alimentar la fe correcta en otros.

Si la semilla en tu corazón ha crecido a un punto que te mueve hoy a querer declarar que Jesucristo es tu Señor y Salvador te invitamos a hacer una [Oracion_de_fe].

## REFERENCIAS

* {1} Biblia. Reina Valera. 1960
* {2} Cornellius Hunter. Darwin,s Predictions. [http://www.darwinspredictions.com/#_4._L._E. L](). citando E. Orgel, “Prebiotic chemistry and the origin of the RNA world,” Critical Reviews in Biochemistry and Molecular Biology, 39 (2004): 99-123.
* {3} [http://en.wikipedia.org/wiki/White_mustard]()
* {4}
* {5} [http://en.wikipedia.org/wiki/Brassica_nigra]()
* {6} [http://en.wikipedia.org/wiki/Brassica_carinata]()
* {7} [http://en.wikipedia.org/wiki/Brassica_oleracea]()
* {8} Edward Post. A Dictionary of the Bible. [http://www.odu.edu/\~lmusselm/post/dictionary/hastings_dic/pages/mustard.shtml]()
* {9} Cuck Smith. C2000 Series. [http://www.blueletterbible.org/commentaries/comm_view.cfm?AuthorID=1&contentID=7132&commInfo=25&topic=Mark&ar=Mar_4_31]()
* {10} Dios Habla Hoy. [http://www.biblegateway.com/passage/?search=Marcos+4&version=DHH]()
* {11} [http://www.spurgeon.org/sermons/2110.htm]()
* {12} [http://www.christiananswers.net/q-aiia/mustardseed.html]()
* {13} [http://www.crossmarks.com/parable/1seed.htm]()

***

Imagen puede usarse con fines no comerciales y proviene de [https://misdibujoscristianos.blogspot.com/search?q=mostaza](https://misdibujoscristianos.blogspot.com/search?q=mostaza "https://misdibujoscristianos.blogspot.com/search?q=mostaza")