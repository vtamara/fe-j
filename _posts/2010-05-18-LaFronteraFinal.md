---
layout: post
title: "LaFronteraFinal"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Es el título de un libro del médico retirado Richard Kent de Inglaterra, con testimonios de experiencias cercanas a la muerte.  En otro libro "El Efecto Lazaro"  hay testimonios de resurrecciones.

Estos libros y muchos otros materiales están disponibles para descarga y uso con propósitos educativos de  http://www.finalfrontier.org.uk/
