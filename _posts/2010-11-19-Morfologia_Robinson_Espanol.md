---
layout: post
title: "Morfologia_Robinson_Espanol"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
En http://www.byztxt.com/download/ (espejo en http://www.alexanderhamiltoninstitute.org/lp/Burgon/curriculum%5CWelcome%20to%20ByzTxt.htm) se describen códigos para morfología del griego del nuevo testamento de Maurice Robinson (2000), derivados a su vez  del "Lexicón analítico del griego" de Davidson, del "Nuevo lexicón analítico del griego" de Perschbacher y del "Griego analítico del nuevo testamento" de Friberg.

A continuación presentamos las abreviaturas y su explicación en español comparando con terminología de {1}, enfocando su uso en el módulo sword KJV2003.


## FORMAS SIN DECLINACIÓN


* ADV = ADVerbio o adverbio y partícula combinados, puede ponerse algunos sufijos (-C o -I o -S) que se describen en la sección siguiente
* CONJ = CONJunción o partícula conjuntiva
* COND = partícula CONDicional o conjunción, puede tener sufijo -C
* PRT = !PaRTícula, partícula disyuntiva
* PREP = PREPosición
* INJ = !INterJección
* ARAM = palabra transliterada del ARAMeo (indeclibnable)
* HEB = palabra transliterada del HEBreo (indeclinable)
* N-PRI = sustaNtivo PRopio Indeclinavle
* A-NUI = Adjetivo NUmeral Indeclinable. Puede tener sufijo -ABB
* N-LI = (sustaNtivo) Letra Indeclinable
* N-OI = sustaNtivo de Otro tipo Indeclinable

Expresión regular extendida que describe justamente las que aparecen en KJV2003:
<pre>
(ADV(-![CIS])?|CONJ|COND(-C)?|PRT(-![IN])?|PREP|INJ|ARAM|HEB|N-PRI|A-NUI(-ABB)?|N-LI|N-OI)
</pre>


## FORMAS DECLINADAS EXCEPTO VERBOS

Todas siguen el orden:  ''prefijo''-''caso'' ''número'' ''género'' 
Algunas pueden terminar con un sufijo.

! Persona 

* 1 = Primera
* 2 = Segunda
* 3 = Tercera

! Prefijo

* N- = Sustantivo, puede tener sufijo -C
* A- = Adjetivo, puede tener sufijos -C, -S, -ATT y -ABB
* R- = pronombre Relativo, puede tener sufijos -P y -ATT
* C- = pronombre reCiproco
* D- = pronombre Demostrativo, puede tener sufijo -C
* T- = artículo definido
* K- = pronombre correlativo
* I- = pronombre Interrogativo
* X- = pronombre indefinido
* Q- = pronombre cOrrelativo o interrogativo
* F- = pronombre reFlexivo Agregar persona 1,2,3, e.g F-3DSF
* S- = pronombre poSesivo. Agregar persona
* P- = Pronombre personal.  Bien tiene un número y no tiene género, o bien no tiene número pero si género.


! Caso (sistema de sólo 5 casos)

* -N = Nominativo
* -V = Vocativo
* -G = Genitivo
* -D = Dativo
* -A = Acusativo

En KJV2003 sólo hay sustantivos y adjetivos en caso vocativo

! Número

* S = Singular
* P = Plural

! Género

* M = Masculino
* F = Femenino
* N = Neutro

! Sufijo

* -S = Superlativo (usado sólo con adjetivos y algunos adverbios)
* -C = Comparativo (usado sólo con adjetivos y algunos adverbios)
* -ABB = Forma abreviada (usado sólo con varios numerales)
* -I = Interrogativo
* -N = Negativa (usado sólo con partículas como PRT-N)
* -C = forma Contraida, o dos palabras mezcladas con crasis
* -ATT = forma de griego ATico
* -P = Partícula adjunta (con pronombre relativo) 

Infortunadamente en los sufijos se emplea la misma letra para forma contraida que para comparativo.

Expresión regular extendida que incluye las de NT según KJV2003
<pre>
A-![ADGNV]![PS]![FMN](-(![CS]|ATT|ABB))?|
D-![ADGN]![PS]![FMN](-C)?|
N-![ADGNV]![PS]![FMN](-C)?|
P-![ADGN]![PS]![FMN]|
P-(![123]![ADGN]![PS](-C)?|![ADGN]![PS]![FMN])|
S-![123]![ADGN]|
F-![123]![ADGN]![PS]![FMN](-C)?|
R-![ADGN]![PS]![FMN](-(P|ATT))?|
![CIKQTX]-![ADGN]![PS]![FMN]
</pre>


## VERBOS

Todos los verbos se listan en una de tres formas:

# V-''tiempo'' ''voz'' ''modo''
# V-''tiempo'' ''voz'' ''modo''-''persona'' ''número''
# V-''tiempo'' ''voz'' ''modo''-''caso'' ''número'' ''género''

Número, género, caso y persona son como se presentaron en la sección de formas declinadas. Las otras partes se explican a continuación.

! Tiempo

* P = Presente
* I = Imperfecto
* F = Futuro
* 2F = segundo Futuro
* A = Aoristo
* 2A = segundo Aoristo
* R = peRfecto
* 2R = segundo peRfecto
* L = pLuscoamperfecto
* 2L = segundo pLuscoamperfecto
* X = sin tiempo establecido (impertivo adverbial)


! Voz

* A = Activa
* M = Media
* P = Pasiva
* E = bien mEdia o bien pasiva
* D = media Deponente
* O = pasiva depOnente
* N = media o pasiva depoNente
* Q = activa impersOnal
* X = sin voz establecida


! Modo

* I = Indicativo
* S = Subjuntivo
* O = Optativo
* M = iMperativo
* N = iNfinitivo
* P = Participio
* R = participio en sentido impeRativo


! Extra

* -M = significado Medio
* -C = forma Contraída
* -T = Transitivo
* -A = eólico
* -ATT = Ático
* -AP = forma APocopada
* -IRR = forma IRRegular o impura

Expresión regular extendida que los describe:

<pre>
V-(![PIFARLX]|2![FARL])![AMPEDONQX]![ISOMNPR](-![123]![SP])?(-(![MCTA]|ATT|AP|IRR))?
</pre>

##REFERENCIAS

* {1} Griego para Sancho: Introducción al Griego del nuevo Testamento.  Elizabeth de Sendek. henry de Jesús Periñan. Publicaciones FUSBC. 2007
