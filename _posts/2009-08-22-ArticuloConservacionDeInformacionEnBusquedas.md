---
layout: post
title: "ArticuloConservacionDeInformacionEnBusquedas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
El artículo "Conservación de Información en Búsquedas: Midiendo el costo del éxito" de William Dembsky y Robert J. Marks fue publicado por IEEE Transactions en Systems, Man and Cybernetics.  Está disponible en:

http://marksmannet.com/RobertMarks/REPRINTS/2009_ConservationOfInformationInSearch.pdf

Explica que los algoritmos de búsqueda en problemas dificiles que tengan buen desempeño al encontrar soluciones, no deben su desempeño al azar sino a información activa  inyectada al algoritmo por el programador.   Proponen una medida para esa información activa y ejemplifican su uso en varias clases de algoritmos de búsqueda.

Aplicado al ADN humano resulta que para lograrlo en el tiempo que ha ocurrido, no ha sido  por azar sino ha requerido información activa (inteligencia) inyectada por Dios.  Diversos ejemplos del artículo se enfocan hacía este tema, pues sus autores son proponentes del Diseño Inteligente.

