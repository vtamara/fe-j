---
layout: post
title: "Homosexualidad_y_Cristianismo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Apuntes del seminario en la Fundación El Renuevo dado por William Charry el sábado 5 de Octubre.

Contexto terapeútico en Colombia:
* Dos grupos:  Romanos VI y Exodus
* Pastoral: El corazón del Padre pastoreado por Omar Castiblanco

Lenguaje:
* Homosexual: persona con la dignidad de ser imagen de Dios por cuanto es creación de Dios
* Homosexualidad: comportamiento
* Homosexualismo: corriente ideológica que promueve la homosexualidad


El trabajo misional con población homosexual incluye:
* Entender el lenguaje que emplean
* Diálogo en el que antepongamos el mensaje a mis sentimientos.  Con certeza la dificultad de la iglesia primitiva cuando se abrió a los gentiles fue mayor.  Hechos 10:4-5, Hechos 11:1-18, Hechos 15:1 15:12

Acompañamiento a personas con quebrantamiento emocional y sexual

* Presupone que yo tengo un buena vida emocional.  Que he podido elaborar mis perdidas personales.
* 90% han sido víctimas de abuso sexual
* Como acercarme
** Requiere una percepción integral, no basta el componente espiritual, se trata de un proceso, un acompañamiento. __La sanidad es un proceso no un evento__
** Idea: Presentar a Jesús como varón (cordero perfecto macho inmolado).  No fue producto de la cultura, se interculturizó sin perder su identidad.
** En el diálogo confrontar con vacíos.  Establecer límites claros.
** Actitud adecuada en el diálogo (también requiere aptitud, pueden remitirse a grupos con el de la Fundación el Renuevo).
** Pastoral: Revisar contexto familiar, personal, eclesial.
* Para padres recomendar:
** Unirse a un grupo de padres con situación común. Padres dialogando con padres.  Prevención NARTH.
** Elaborar perdida personal.
** No quitarle diálogo.   Castigos emocioales no funcionan.  Ni castigar al otro ni castigarse uno mismo.
** Ser red de apoyo, sin estar de acuerdo con comportamiento
** Poder expresar dolor y escuchar
* Prevención.  Factores de riesgo
** Abuso sexual
** Familias disfuncionales
** Soledad, maltrato
** Presentar sexualidad con carácter antropológico
* Comunidad de fe
** Es una responsabilidad, debe prepararse para no rechazar


Bibliografía entregada:
* Capítulo 16 del libro los problemas que los Cristianos Enfrentamos Hoy. John Stott. 2007.
* Capítulo 4 de La homosexualidad. Compasión y Claridad en el Debate.  Thomas E. Schmidt.  1995.
* Capítulos 6 a 12 de Cuando el Homosexual pide ayuda.  Esly Carvalho.  2004.


