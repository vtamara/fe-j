---
layout: post
title: "Mom_After_Gods_Own_Heart"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
#A Mom After Gods Own Heart
##10 Ways to Love Your Children.  
!Elizabeth George. 

Disponible en Amazon, en inglés, español y con guia de estudio y crecimiento.

Easy reading, good quotes from Bible and reflections, very practical I was impressed in finding these 10 commandments for guiding your children:

# Teach them using God's word (Deut 6:4-9)
# Tell them what is right and wrong (1 Kings  1:6)
# See them as gift from God (Psalm 127:3)
# Guide them in godly ways (Prov 22:6)
# Discipline them (Prov 29:17)
# Love them unconditionally (Luke  15:11-32)
# Do not provoke them to wrath (Ephesians 6:4)
# Earn their respect by example (1 Tim 3:4)
# Provide for their physical needs (1 Tim 5:8)
# Pass your faith along to them (2 Tim 1:5)

as well as the following "rules for rearing Children" of Susannah Wesley

# Allow no eating between meals
# Put all children in bed by eight o'clock
# Require them to take medicine without complaining
# Subdue self-will in a child and thus work together with God to save his soul
# Teach each one to pray as soon as he can speak
# Require all to be stil during family worship
# Give them nothing that they cry for, and only that which they ask for politely.
# To prevent lying, punish no fault which is first confessed and repented of.
# Never allow a sinful act to go unpunished.
# Never punish a child twice for a single offense.
# Commend and reward good behavior.
# Any attempt to please, even if poorly performed, should be commended.
# Preserve property rights, even in the smallest matters.
# Strictly ovbserver all promises.
# Require no daughter to work, before she can read well.
# Teach children to fear teh rod


Free translation

10 mandamientos para guiar a sus hijos e hijas de Elizabeth George:

# Enséñeles usando la palabra de Dios  (Deut 6:4-9)
# Dígales lo que está bien y lo que está mal  (1 Kings  1:6)
# Véalos como un regalo de Dios (Psalm 127:3)
# Guíelos de formas cristianas (Prov 22:6)
# Disciplinemos  (Prov 29:17)
# Amelos incondicionalmente (Luke  15:11-32)
# No  los provoque a ira (Efesios 6:4)
# Gane su respeto con ejemplo (1 Tim 3:4)
# Provea para sus necesidad físicas  (1 Tim 5:8)
# Tansmitales su fe (2 Tim 1:5)

Reglas para Criar Hijos e Hijas de Susannah Wesley

# No permita comer entre comidas
# Acueste todos los niños a las 8:00PM
# Exíjales tomar medicina sin quejarse.
# Domine la voluntad propia de un niño y entonces trabajen juntos con Dios para salvar su alma
# Enséñele a cada uno a orar tan pronto como pueden hablar
# Exija que estén en silencio durante la adoración en familia
# No les de nada de aquello por lo que lloran, y solo aquello que hayan pedido educadamente.
# Para prevenir las mentiras, no castigue una falta que sea confesada y por la que esté arrepentido.
# No permita que actos pecaminosos queden sin castigo.
# No castigue a un niño dos veces por una misma ofensa.
# Felicite y premie el buen comportamiento.
# Cualquier intento de complacer, aún si se realiza pobremente, debe ser felicitado.
# Preserve los derechos de propiedad, aún en los detalles más pequeños.
# Cumpla estrictamente toda promesa.
# No exija que una hija trabaje, antes de que pueda leer bien.
# Enséñele a los niños a temer la vara.

The following sections have a summary of chapters and translation and adaptation of some of the exercises of the study guide.


!Introducción. Lo más importante es el corazón

__Sobre toda cosa guardada, guarda tu corazón; porque de él mana la vida.__
Proverbios 4:23  __Above all else, guard your heart, for everything you do flows from it.__ 

Misión de los padres: criar hijos e hijas conforme al corazón de Dios, sembrar Su palabra en el corazón de ellos y ellas.

Esto requiere un corazón entregado a Dios, corazón se refiere a "vida interior, mente, pensamientos, motivaciones, deseos"

Ejemplos de niñ@s conformes al corazón de Dios:
* Samuel (Ana con oraciones y esfuerzos para educarlo y poderlo entregar en Silo, Elcana cumplido para ir al templo, también lo entregó).  ¿Estoy preparado para entregar mi hij@ a Dios? Es lo más lindo.
* David. Raíces espirituales en sus ancestros quienes tenían fe firme. Salmón-Rahab -> Booz-Rut -> Obed -> Isai -> David. Pidamos para poder transmitir a futuras generaciones fe.
* Daniel. Hijos de Israel.  Sus acciones en la adolescencia muestran educación excelente en caminos de Dios.
* Timoteo. Posiblemente joven, era considerado verdadero hijo de la fe. Padre gentil, madre y abuelas judías convertidas. Seguramente forjado además de en el corazón de Dios en las oraciones de su familia.  Cita a Kendrick y Lukas, 365 Life Lessons "A pesar de vivir en un hogar dividido, la madre de Timoteo inculcó en él un carácter fiel que lo acompaño hasta la vida adulta... No ocultes tu luz en casa: Nuestra familia es un campo fértil para recibir las semillas del evangelio.  Es el campo más difícil de labrar, pero produce las mayores cosechas. Deja que tus ![hijos] ... conozcan tu fe en Jesús"
* María. 14 años. La calificó la disposición de su corazón. Sabía de memorias partes de la Biblia.  En las Escrituras debió ser enseñada. Alguien en casa se encargó de instruirla.

Pequeñas decisiones
* Programa tu agenda
* Analiza el tiempo que pasas frente al televisor
* Consigue un libro devocional
* Memoriza un versículo
* Ora por tu corazón

__Ejercicios__

* Mateo 15:18-19. Pero lo que sale de la boca, del corazón sale; y esto contamina al hombre.  Porque del corazón salen los malos pensamientos, los homicidios, los adulterios, las fornicaciones, los hurtos, los falsos testimonios, las blasfemias.
* Proverbios 4:23. Reflexión sobre esta frase "Dios comienza a moldear a un papá o mamá conforme a su corazón desde el interior --su corazón--, y después continúa hacía el exterior"
* Salmo 40:8 "El hacer tu voluntad, Dios mío, me ha agradado, Y tu ley está en medio de mi corazón." Escriba algo que puede hacer para cuidar más su corazón.
* Personajes de la Biblia que comenzaron a seguir a Dios desde una temprana edad?

* Samuel: 
** ¿Qué impresiona de la devoción de Samuel desde muy temprano como se registra en 1 Sam 1:28 y 1 Sam 2:11?  ¿Qué impresiona de la respuesta de Samuel al oir la voz de Dios, registrada en 1 Sam 3:4 y  10?
** Su papá (Elcana) iba al templo 1 Sam 1:3, Su mamá (Ana) lo dedico a Dios 1 Sam 1:28, su papá  lo dejó en otra ciudad con un sacerdote (Elí) cuando él era niño. 1 Sam 2:11.  Samuel respondió a Dios "Heme Aquí" cuando Él lo llamo. 1 Sam 3:4.  Ana manejó la burla de la otra esposa de Elcana por no haber tenido hijos  (1 Sam 1:2), con oración 1 S 1:10 pidiendo un hijo.  Dios le dio tranquilidad mediante el sacerdote Elí y le concedió el hijo que pedía. Ana a su vez respondió con sacrificios cuando el niño nació.
** Cual era el problema de Ana en 1 Sam 1:2?  Como lo manejo según el verso 10 y qu te enseña?
** Cómo Dios le respondió a Ana en los versos 17 a 20.  Y como Ana respondió a la respuesta de Dios en los versos 24-28.
** "La mayor alegria para una mama al tener un niño es darlo completa y libreemnte a dios".  Al leer esto, ¿ve a Ana como una mamá tras el corazón de Dios, haciendo esto?, y ¿Qué puede hacer para seguir su ejemplo?

* David: Como se describe a David en 1 Sam 13:14 y Hechos 13:22 (conforme al corazón de Dios y obediente).
** De jóven que habilidades ve en David como se registra en 1 Sam 16:11-19, 17:15. 1 Sam 16:16-18 y 23. 1 Sam 17:34-36, 1 Sam 17:48-50.  El menor de sus hermanos y pastor de ovejas, rubio, hermoso de ojos,  buen parecer, ungido, lleno del Espíritu de Dios, sabe tocar bien arpa, valiente, vigoroso, hombre de guerra, prudente, Dios está con él, fiel, aliviaba a Saul con el arpa, mataba osos o leones que tomaran corderos de su manada, no temía a Goliat, rápido, inteligente, preciso.
** Que honor dio Dios a este jóven conforme a su corazón en 1 Sam 16:10-13?  Elegido por Dios, ungido por Samuel y desde entonces lleno del Espíritu de Jehova.
** Ancestros de David.  Lea Ruth 4:17,21-22, Mat 1:4-5 y Lucas 3:31-32 .  ¿Quienes están en el linaje de David y que sabe de ellos?  Rut-Booz->Obed->Isai.  Rut era bisabuela de David.  Salmón->Rahab -> Booz.   Rahab era tatarabuela de David, aunque no es seguro que se trate de la mujer que ayudo a los espias en Jericó cuando Josue la iba a tomar.  [Rut] era extranjera que adoptó el judaismo en tiempos de angustia y fue fiel al Señor.
* "Dios obra por medio de padres fieles, quienes a pesar de los dias dificile sy oscuros, caminan obedientemente con Él".  Los ancestros de David permanecieron fieles a Dios contribuyendo generación, tras generación de hombres y hombres tras el corazón de Dios. ¿Qué dificultades ha encontrado, y como la verdad de 2 Cor 12:9-10 le ayuda a mantenerse fiel mientras moldea los corazones de la siguiente generación?

* Daniel
** Qué aprende de la herencia espiritual de Daniel y sus amigos en Daniel 1:1-6?
** Que evidencia en estos jóvenes respecto a: a) sus convicciones en Daniel 1:5 y 8? b) su vida de oración en Daniel 2:16-18? c) sus creencias en Daniel 3:12-17 y 26-30?
* Familia de Daniel
** "De una familia devota y que cria a los niños, un niño pued aprender como vivir una vida devota en un mundo pecador"  Que dicen Daniel 1:3 y 6 sobre la herencia familiar de Daniel y sus 3 amigos?
* Padres devotos enseñaron y entrenaron acerca de Dios a estos niños que fueron llevados cautivos a una tierra impia.  Qué le dice Proverbios 22:6, que debe hacer como mamá/papá conforme al corazon de Dios con y por sus hijos?


* Timoteo: Relación con Pablo? en 1 Tim 1:2.  Ministerio y carácter de Timoteo de acuerdo a Filipenses 2:19-23? Familia de Timoteo: Que sabemos de su papá y su mamá por Hechos 16:1? (griego es no creyente). De su abuela en 2 Tim 1:5? Qué regalo recibió de su abuela?  Como la fe de Eunice y Loida  lo anima para criar hijos conformes al corazón de Dios? 
* María: 
** Cuales fueron las grandes cosas que Dios hizo por María (como la joven lo dijo) de acuerdo a Lucas 1:28,30 
28 Y entrando el ángel en donde ella estaba, dijo: ##Salve, muy favorecida! El Señor es contigo; bendita tú entre las mujeres.
29 Mas ella, cuando le vio, se turbó por sus palabras, y pensaba qué salutación sería esta.
30 Entonces el ángel le dijo: María, no temas, porque has hallado gracia delante de Dios.
Lucas 1:31-33
31 Y ahora, concebirás en tu vientre, y darás a luz un hijo, y llamarás su nombre JESÚS.
32 Este será grande, y será llamado Hijo del Altísimo; y el Señor Dios le dará el trono de David su padre;
33 y reinará sobre la casa de Jacob para siempre, y su reino no tendrá fin.
Lucas 1:48
48 Porque ha mirado la bajeza de su sierva;
Pues he aquí, desde ahora me dirán bienaventurada todas las generaciones.
** Que más aprendes de María en Lucas 1:27 y 34
27 a una virgen desposada con un varón que se llamaba José, de la casa de David; y el nombre de la virgen era María.
34 Entonces María dijo al ángel: ¿Cómo será esto? pues no conozco varón.
** Cómo responde María al angel y que revela esta respuesta sobre su corazón? Lucas 1:38 38 Entonces María dijo: He aquí la sierva del Señor; hágase conmigo conforme a tu palabra. Y el ángel se fue de su presencia.
* Familia de María
** Considere este pensamiento "El rol de padres devotos es asegurar que los corazones y las mentes de su hijos están saturadas con la Palabra de Dios"  Entonces revise el Magnificat en Lucas 1:46-55.  que evidencia encuentra de que el joven corazón de María estaba saturado de la Palabra de Dios (que alguien en casa se aseguró de que María supiera de Dios)
46 Entonces María dijo:
    Engrandece mi alma al Señor;
47 Y mi espíritu se regocija en Dios mi Salvador.
48 Porque ha mirado la bajeza de su sierva;
Pues he aquí, desde ahora me dirán bienaventurada todas las generaciones.
49 Porque me ha hecho grandes cosas el Poderoso;
Santo es su nombre,
50 Y su misericordia es de generación en generación
A los que le temen.
51 Hizo proezas con su brazo;
Esparció a los soberbios en el pensamiento de sus corazones.
52 Quitó de los tronos a los poderosos,
Y exaltó a los humildes.
53 A los hambrientos colmó de bienes,
Y a los ricos envió vacíos.
54 Socorrió a Israel su siervo,
Acordándose de la misericordia
55 De la cual habló a nuestros padres,
Para con Abraham y su descendencia para siempre.


** 
!1. Take Time to Nurture Your Heart

!2. Teach your Children God's Word

!3. Talk to Your Children About God

!4. Tell Your Children About Jesus

!5. Train Your Children in God's Ways

!6. Take Care of Your Children   

!7. Take Your Children to Church   

!8. Teach Your Children to Pray   

!9. Try Your Best

!10. Talk to God About Your Children
