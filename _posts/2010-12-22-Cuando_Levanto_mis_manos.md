---
layout: post
title: "Cuando_Levanto_mis_manos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Samuel Hernandez


<pre>
Levanto mis manos,
aunque no tenga fuerzas
levanto mis manos,
aunque tenga mil problemas

--- CORO

Cuando levanto mis manos, 
comienzo a sentir, 
una unción que me hace cantar
Cuando levanto mis manos, 
comienzo a sentir 
¡el fuego! ¡ohooooh!

Cuando levanto mis manos, 
mis cargas se van,
nuevas fuerzas, tu me das,
todo esto es posible, 
todo esto es posible,
...cuando levanto mis manos...

---

Levanto mis manos,
aunque no tenga fuerzas
levanto mis manos,
aunque tenga mil problemas

CORO

Todo esto es posible, 
todo esto es posible
¡Cuando levanto mis manos!

</pre>


* Letra: http://www.musica.com/letras.asp?letra=1031098
* Vídeo: http://www.youtube.com/watch?v=oGpqzERdoLQ
* Karaoke: http://www.youtube.com/watch?v=tQdJiTHRe54
* Midi: http://www.midisaya.com/dispcanto.asp?ID=midis12/Levanto.txt
