---
layout: post
categories:
- Prédica
title: Buscar a Dios de corazón. Isaías 55:6
author: vtamara
image: "/assets/images/prophet-isaiah-1968.jpg"

---
Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 25.Jul.2021

# 1 Introducción

El Señor me recordó Isaías 55:6 y me ha enseñado al respecto.

# 2. Texto: Isaías 55:1-13

> 1 A todos los sedientos: Venid a las aguas; y los que no tienen dinero, venid, comprad y comed. Venid, comprad sin dinero y sin precio, vino y leche.
>
> 2 ¿Por qué gastáis el dinero en lo que no es pan, y vuestro trabajo en lo que no sacia? Oídme atentamente, y comed del bien, y se deleitará vuestra alma con grosura.
>
> 3 Inclinad vuestro oído, y venid a mí; oíd, y vivirá vuestra alma; y haré con vosotros pacto eterno, las misericordias firmes a David.
>
> 4 He aquí que yo lo di por testigo a los pueblos, por jefe y por maestro a las naciones.
>
> 5 He aquí, llamarás a gente que no conociste, y gentes que no te conocieron correrán a ti, por causa de Jehová tu Dios, y del Santo de Israel que te ha honrado.
>
> 6 Buscad a Jehová mientras puede ser hallado, llamadle en tanto que está cercano.
>
> 7 Deje el impío su camino, y el hombre inicuo sus pensamientos, y vuélvase a Jehová, el cual tendrá de él misericordia, y al Dios nuestro, el cual será amplio en perdonar.
>
> 8 Porque mis pensamientos no son vuestros pensamientos, ni vuestros caminos mis caminos, dijo Jehová.
>
> 9 Como son más altos los cielos que la tierra, así son mis caminos más altos que vuestros caminos, y mis pensamientos más que vuestros pensamientos.
>
> 10 Porque como desciende de los cielos la lluvia y la nieve, y no vuelve allá, sino que riega la tierra, y la hace germinar y producir, y da semilla al que siembra, y pan al que come,
>
> 11 así será mi palabra que sale de mi boca; no volverá a mí vacía, sino que hará lo que yo quiero, y será prosperada en aquello para que la envié.
>
> 12 Porque con alegría saldréis, y con paz seréis vueltos; los montes y los collados levantarán canción delante de vosotros, y todos los árboles del campo darán palmadas de aplauso.
>
> 13 En lugar de la zarza crecerá ciprés, y en lugar de la ortiga crecerá arrayán; y será a Jehová por nombre, por señal eterna que nunca será raída.

# 3. Contexto

Una cronología detallada de la historia del Pueblo de Dios en el antiguo testamento puede verse en:

[https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0](https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0 "https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0")

La siguiente es una cronología de Reyes en Judea, durante el tiempo de vida de Isaías (según Rodgers, ver {2}), tomamos nombres de reyes de Reina Valera 1960.

| --- | --- | --- |
| Rey | Inicio | Fin |
| Uzías o Azarías | -767 | -739 |
| Jotam | -739 | -732 |
| Acaz | -732 | -715 |
| Ezequías | -715 | -687 |
| Manasés | -687 | -643 |

Aunque no se conocen con precisión los años de nacimiento y muerte de Isaías, sabemos que era un hombre que vívia en Jerusalén, que su papá se llamaba Amoz y posiblemente vivió entre el -760 y el -690 antes de Crissto.

Algunos versículos con profecías en el libro de Isaías. Presentamos unas pocas, pues por ejemplo en {3} se listan 36.

| --- | --- | --- | --- |
| Versículo | Tiempo de Isaías | Profecía/Evento | Tiempo profecía |
| 2:1-5 |  | Reino de Dios | >2021 |
| 2:11-22 |  | Apocalipsis | >=2021 |
| 3:1 |  | Asedio a Jerusalén por Asiria | -600 |
| 5:13-17 |  | Destierro a Babilonia | -600 |
| 6:1 | -739 | Muerte del rey Uzías |  |
| 6:9-13 |  | Destierro a Babilonia | -600 |
| 7:1 | -732 | Inicio del reinado de Acaz |  |
| 7:8-9 |  | Asiria invade Samaria (Israel del norte). | -721 |
| 7:14-15 |  | Mesías | 0 |
| 8:23-9:6 |  | Mesianica |  |
| 13:17 |  | Medos destronarán Babilonia | -530 |
| 14:1-2 |  | Regreso del destierro de Babilonia | -516 |
| 14:3-23 |  | Contra Babilonia y Satanas | -530 y 0 y >=2021 |
| 14:28 | -715 | Muerte del rey Acaz |  |
| 36:1 | -711 | Año 14 del rey Ezequías Senaquerib tomó muchas ciudades de Judá |  |
| 53:5 |  | Martirio del Mesías | 33 |

# 4. Análisis

El señor me ha dado otros versículos del libro de Isaías que explican cómo buscar a Dios.

* Buscar al Señor: 26:9 Con mi alma te he deseado en la noche, y en tanto que me dure el espíritu dentro de mí, madrugaré a buscarte; porque luego que hay juicios tuyos en la tierra, los moradores del mundo aprenden justicia.
* Honrarlo de corazón obedenciedo: 29:13 Dice, pues, el Señor: Porque este pueblo se acerca a mí con su boca, y con sus labios me honra, pero su corazón está lejos de mí, y su temor de mí no es más que un mandamiento de hombres que les ha sido enseñado;
* Pedirle consejo para cada decisión: 30:1 !!Ay de los hijos que se apartan, dice Jehová, para tomar consejo, y no de mí; para cobijarse con cubierta, y no de mi espíritu, añadiendo pecado a pecado!
* Obrar bien: 33:15-17 El que camina en justicia y habla lo recto; el que aborrece la ganancia de violencias, el que sacude sus manos para no recibir cohecho, el que tapa sus oídos para no oír propuestas sanguinarias; el que cierra sus ojos para no ver cosa mala; 16 éste habitará en las alturas; fortaleza de rocas será su lugar de refugio; se le dará su pan, y sus aguas serán seguras. 17 Tus ojos verán al Rey en su hermosura; verán la tierra que está lejos.

Isaías sabía buscar a Jehova de corazón, en consecuencia vivió teofanías (e.g 6:1-7) y Dios lo guiaba en lo que el debía hacer en ocasiones en su día a día (como el nombre por ponerle a su hijo) y por medio de él, Dios guió a los reyes que quisieron oirlo (como Acaz) y le dió profecías muy detalladas del futuro cercano y lejano, a tal punto que aún hoy nos guían porque son futuras para nosotros.

Tengamos en cuenta que el Señor puede probar como en mi humilde opinión le ocurrió a Isaías por ejemplo en 20:1-5

# 5. Conclusión y oración

Señor que podamos buscarte como te buscaba Isaías, y en esto le revelaste algunos pasos para su vida, por medio de él guiaste a los líderes que lo escuchaban e incluso a la humanidad hoy en día. Gracias por tu omnisapiencia y omnipotencia, gracias porque eres justo y misericordioso.

Señor que podamos pagar el precio por tu revelación, preparanos y guíanos incluso para eso mediante tu Santo Espíritu, pido en el nombre de Jesús.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0 "https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0")
* {2} [https://fe.pasosdejesus.org/Cronologia_Reyes/](https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0 "https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0")
* {3} [http://www.indubiblia.org/isaias-1](https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0 "https://danzadoresdejesucristo.onlyoffice.com/Products/Files/DocEditor.aspx?fileid=6209505&doc=a1FBVk40T2FNcHNOU1o5ODl6RmRtVEFsNWt1RExkRUZTSVc0U0F6WmtSaz0_IjYyMDk1MDUi0")

La imagen de portada es "El profeta Isaías" de Marc Chagal. 1968. Disponible en [https://www.wikiart.org/en/marc-chagall/prophet-isaiah-1968](https://www.wikiart.org/en/marc-chagall/prophet-isaiah-1968 "https://www.wikiart.org/en/marc-chagall/prophet-isaiah-1968")