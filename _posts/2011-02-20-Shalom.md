---
layout: post
title: "Shalom"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Lo entendemos como un bienestar integral individual y colectivo precedido y acompañado de justicia social y de verdad, logrando bienestar material, salud, educación, trabajo, vivienda, educación, condiciones de equidad para tod@s, gracias a Cristo Rey.

!Es colectivo
Jeremias 29:7
"Y procurad la paz de la ciudad a la cual os hice transportar, y rogad por ella a Jehová; porque en su paz tendréis vosotros paz."

"Nuestra paz y bienestar está íntimamente relacionada con la paz y el bienestar de la sociedad que nos rodea. Nosotros los cristianos no tenemos una paz separada. El pueblo de Dios no puede hacerse ilusiones de salvarse de la tormenta que ruge en derredor." {2}

!Cristo es Rey

"   22. Creemos que la voluntad de Dios es que
haya paz. Dios creó el mundo en paz, y la paz de
Dios ha sido revelada plenamente en Jesucristo,
quien es nuestra paz y la paz del mundo entero.
Guiados por el Espíritu Santo, seguimos a Cristo
en el camino de la paz, haciendo justicia, trayendo
reconciliación y practicando la no violencia, incluso allí donde hay violencia y guerra." {3}

!Con verdad

La justicia está relacionada con la verdad, ambos son frutos del Espíritu
Efesios 5:9 porque el fruto del Espíritu es en toda bondad, justicia y verdad

!Es el deseo de Dios y se relaciona con bienestar físico y material

"6 He aquí que yo les traeré sanidad y medicina; y los curaré, y les revelaré abundancia de paz y de verdad.    7 Y haré volver los cautivos de Judá y los cautivos de Israel, y los restableceré como al principio.    8 Y los limpiaré de toda su maldad con que pecaron contra mí; y perdonaré todos sus pecados con que contra mí pecaron, y con que contra mí se rebelaron.   9 Y me será a mí por nombre de gozo, de alabanza y de gloria, entre todas las naciones de la tierra, que habrán oído todo el bien que yo les hago; y temerán y temblarán de todo el bien y de toda la paz que yo les haré." Jeremias 33:6-9

De {4}:
"La paz de Dios es mucho más que la ausencia de guerra o una tranquilidad interior.  Los conceptos de shalom (paz en hebreo encontrado en el Antiguo Testamento) y eirene (paz en griego, encontrado en el Nuevo Testamento ) son muy amplios y abarcan un bienestar completo.  el primer sentido de paz en la  biblia es de un bienestar físico y material. Los patriarcas usaban el término como saludo: '¿Tienes paz?' significaba '¿Estás bien?' (Gen 37.14) Una sociedad con shalom implica que toda la comunidad (no solamente unos individuos) tenga alimento, ropa, casa, trabajo, salud y seguridad.  Jeremías describió el deseo de Dios para su pueblo después de regresar del exilio en Babilonia: tranquilidad, seguridad, libertad, una ciudad restaurada y segura, perdón y paz con Dios, bienestar, alegría y respeto a los demás (33,6-9).  Una ciudad 'en paz y bienestar' implicaba casa adecuadas, trabajos dignos, calles seguros, personas sanas.  
Jesús se preocupó por el bienestar integral de las personas.  Las sanó, las alimentó, las liberó de miedos e inseguridades, las capacitó para vivir plenamente (Lc. 4.18-19; 7:21-22).  Los primeros cristianos y cristianas siguieron su ejemplo. en Jerusalén formaron una comunidad con condiciones favorables para el bienestar físico de sus miembros, cuidado de que todos tuvieran alimento (Hch 2:44). Las comunidades de fe de Asia y Macedonia se preocuparon por las necesidades de sus hermanos y hermanas en otras regiones (I Co. 16:1).  La paz de bienestar físico incluye el cuidado y la restauración de la naturaleza (Is 4118-19; 49:9; 65:21-25).  Si los seres humanos no están 'en paz' con el mundo natural, sino que lo explotan y lo destruyen, pronto no habrá bienestar.  El trabajo de Cristo incluye la restauración de la naturaleza destruida, es decir una nueva relación entre la humanidad y la creación (col 1:20)"

##BIBLIOGRAFÍA

* {1} Biblia. Reina Valera 1960.
* {2} Por Pedro Stucky. ES UNA PAZ COLECTIVA: TRABAJEN POR LA PAZ DE LA CIUDAD. http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-26sep2010.shtml 
* {3} Confesión de fe en perspectiva menonita. 1995.  http://www.iglesiamenonitadecolombia.org/historia/dm/documentos/confesiondefe.pdf
* {4} Yo quiero paz: en mi iglesia y en mi barrio.  Serie: Seamos Pacificadores. Mooney, Ruth et al.  Ediciones Lumbrera. 2006
