---
layout: post
title: "Proteinas_Antiguas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Nuevamente se encuentran proteínas en tejidos blandos que se suponen tienen más de 80 millones de años (los primeros fueron investigados en 2007 por John Asara y la paleontóloga Mary Schweitzer).

http://www.sciencedaily.com/releases/2009/04/090430144528.htm

Aunque la investigación se ha encaminado a apoyar el supuesto origen evolutivo de las aves a partir de los dinosaurios,  lo impactante de esta investigación es que revela la ignorancia humana sobre los procesos de fosilización, pues se creía que no podían existir proteínas ni tejidos blandos en fósiles.  ¿Si tendrán todos los millones de años que se les asignan?

Aunque es descabellado pensar que los gorriones, alondras, tórtolas, palomas y cuanto hermoso, gracil, alegre y libre pajarito que vemos tenga por tara...taraabuelo un dinosaurio, y aunque se han encontrado fósiles de aves datados en la misma supuesta epoca de los dinosaurios, continúan investigaciones que pretenden encontrar o establecer un vínculo.

En resumen con estas investigaciones gana puntos el escepticismo frente a la edad de la tierra.
