---
layout: post
title: "La_ultima_Palabra"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Daniel Calveti

<pre>
INTRO

//Dame de Tu eterna paz
Dame el don para esperar
Ayudame a confiar en Ti
Porque en mis fuerzas no puedo más..//

CORO

Tu eres mi sustento, y Tu mi Creador
Y la última palabra, la tienes Tuuuuuu

*INTRO*

Haz que brote en mi, Tu respuesta oh Dios
No quiero darme, no quiero darme en mi humanidad.
Señor

//*CORO*//
</pre>


* Letra basada en: http://www.musica.com/letras.asp?letra=927860
* Video: http://www.youtube.com/watch?v=4M8dlRCkpOk
* Karaoke: http://www.youtube.com/watch?v=ZsrqA921UG8
