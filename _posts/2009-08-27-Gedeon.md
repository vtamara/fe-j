---
layout: post
title: "Gedeon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Nombre

Gedeon. Guerrero poderoso {1}.

Hebreo {2}: &#1490;&#1460;&#1468;&#1491;&#1456;&#1506;&#1493;&#1465;&#1503;

Por destruir altares de Baal, también fue llamado Jerobaal que significa `contienda Baal contra él' Jueces 6:32

[http://dibujosbiblicos.net/data/media/3/gedeon2.jpg]
Dibujo con derechos reservados de http://dibujosbiblicos.net/img455.search.htm ya solicitamos permiso para usar y referenciar mejor la fuente

##Genealogía

| Parentezco | Persona | Referencia |
| Papá | [Joás] abiezerita | Jueces 6:11 |
| Mujeres | muchas | Jueces 8:30 |
| Hijos | 70 | Jueces 8:30 |
| Hijo: | [Abimelec] la mamá fue concubina de Gedeon en Siquem | Jueces 8:31 |

Lugar de origen: Ofra. Jueces 6:11

##Datos sobre Gedeon en Jueces 6 a 8


##Evidencias arqueológicas

* Con respecto a su hijo Abimelec los lugares que se describen en el libro de Jueces coinciden con hallazagos arqueológicos en Siquem {3}.
* El ataque a Siquem por parte de Abimelec coincide con descubrimientos arqueológicos que la datan entre 1125aC y 1117aC.

##Referencias

* {1} Easton Bible Dictionary. 1897.  http://www.ccel.org/ccel/easton/ebd2.html  Dominio Público.
* {2}  http://en.wikipedia.org/wiki/Gideon
* {3} http://www.biblearchaeology.org/post/2006/02/13/Abimelech-at-Shechem.aspx
* {4} http://www.biblearchaeology.org/post/2008/04/02/From-Ramesses-to-Shiloh-Archaeological-Discoveries-Bearing-on-the-Exodus-Judges-Period.aspx
