---
layout: post
title: "Posible_Arca_De_Noe"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
[http://www.noahsarksearch.net/images/20100424/pr_even_20100424_09.jpg]

En diversos medios de comunicación se ha reportado la exploración de un equipo chino en el monte Ararat y la estructura de madera que encontraron a más de 4000 metros.   Tendría diversos compartimentos, en las fotos se ven como debajo de hielo y rocas volcánicas, según algunos artículos las pruebas de carbono 14 revelan madera de 4800 años de antiguedad. Hay un vídeo al respecto en el sitio oficial:

http://www.noahsarksearch.net/eng/

Sin embargo hay por lo menos un artículo de Randall Price según el cual se trata de un engaño perpetrado por los guias turcos: http://www.worldofthebible.com/news.htm


Hace varios años Ron Wyatt había descubierto en otro sitio de los montes Ararat, una estructura simétrica con dimensiones que coincidian con la descripción bíblica, y otras evidencias que hicieron al gobierno turco declarar en 1987 que se trataba del Arca de Noe --aunque no han permitido excavasiones posteriores. 
[http://www.wyattmuseum.com/images/wpe109.jpg] Ver http://www.wyattmuseum.com/

Con respecto al sitió promocionado por Wyatt, por lo menos un ministerio creacionista ha sugerido que se trata de una formación geológica (ver http://www.answersingenesis.org/creation/v14/i4/report.asp) y que Wyatt mintió (por ejemplo con respecto a los resultados de una exploración con radar). Sin embargo hay quienes refutan el reporte de Answers in Genesis (e.g http://www.arkdiscovery.com/faq.htm  http://www.yecheadquarters.org/catalog2.0.6.html).

Considero que cada quien debe evaluar en detalle las evidencias de ambos sitios, mi intuición me invita a evaluar más las encontradas por Wyatt.
