---
layout: post
categories:
- prédica
title: Oración por Afganistán
author: vtamara
image: "/assets/images/afghan_carpet.JPG"

---
Comunidad Menonita de Suba. Caminos de Esperanza

[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 22.Ago.2021

# 1 Introducción

Entre las peticiones de oración por parte del ministerio de mártires cristianos contemporáneos "Voice of the Martyrs" por los cristianos que han decidido quedarse en Afganistán están:

1. Orar por protección de Dios para ellos/as.
2. Orar por sabiduría bien para quedarse o bien para salir.
3. Orar por fraternidad. Los creyentes en Afganistán deberán mantenerse en secreto, especialmente de sus familias, oremos para que puedan conectarse y que puedan saber que no están solos.

En la iglesia Hayat (del árabe significa vida) donde nos congregamos con Larisa en Lancaster hay varias familias y personas que fueron o son musulmanes. Son seres humanos con necesidades como las nuestras y que sólo con perseverancia y amor pueden entender el amor de Cristo. Varias familias de esta iglesia han recibido inmigrantes musulmanes en sus casas y así han podido compartirles en la práctica el amor de Cristo y en varios casos han logrado conversiones al cristianismo. Con mi esposa, Larisa estamos ofreciendo esa posibilidad para nuevos refugiados Afganos que están llegando --oramos y pedimos oración respecto a esto. 

No temamos sino entendamos que es oportunidad de compartir el evangelio.

# 2. Texto: 2 Tesalonicenses 3:1-5

1 Por lo demás, hermanos, orad por nosotros, para que la palabra del Señor corra y sea glorificada, así como lo fue entre vosotros,

2 y para que seamos librados de hombres perversos y malos; porque no es de todos la fe.

3 Pero fiel es el Señor, que os afirmará y guardará del mal.

4 Y tenemos confianza respecto a vosotros en el Señor, en que hacéis y haréis lo que os hemos mandado.

5 Y el Señor encamine vuestros corazones al amor de Dios, y a la paciencia de Cristo

# 3. Contexto

## 3.1 Del texto

Los pasajes antes y después de este incluyen:

**2:13-17 Escogidos para salvación**

13 Pero nosotros debemos dar siempre gracias a Dios respecto a vosotros, hermanos amados por el Señor, de que Dios os haya escogido desde el principio para salvación, mediante la santificación por el Espíritu y la fe en la verdad,

14 a lo cual os llamó mediante nuestro evangelio, para alcanzar la gloria de nuestro Señor Jesucristo.

15 Así que, hermanos, estad firmes, y retened la doctrina que habéis aprendido, sea por palabra, o por carta nuestra.

**3:6-15 El deber de trabajar**

10 Porque también cuando estábamos con vosotros, os ordenábamos esto: Si alguno no quiere trabajar, tampoco coma.

11 Porque oímos que algunos de entre vosotros andan desordenadamente, no trabajando en nada, sino entremetiéndose en lo ajeno.

12 A los tales mandamos y exhortamos por nuestro Señor Jesucristo, que trabajando sosegadamente, coman su propio pan.

## 3.2 De Afganistan

Afganistán es un país islámico que limita al norte con antiguas naciones soviéticas: Turkmekistan, Uzbekistan, Tajikistan por el oriente limita con China, por el Sur con Pakistán y por el Oeste con Irán. Políticamente consta de 34 provincias y su capital es Kabul donde viven cerca de 4.6 millones de personas.

Mapa de GoogleMaps![](/assets/images/afganistan-google-maps-22-8-2021-6-02-28-a-m.png)

Es en su mayoría montañoso, cuenta con grandes reservas de minerales y tierras raras que se calculan en trillones de dólares, aunque por la inestabilidad política y social es uno de los países con más bajo índice de desarrollo del mundo.

Su economía es altamente rural, hay más de 2 millones de nómadas y son populares algunas artesanías como los tapetes

![](/assets/images/afghan_carpet.JPG)

Imagen de [https://upload.wikimedia.org/wikipedia/commons/e/eb/Afghan_carpet.JPG](https://upload.wikimedia.org/wikipedia/commons/e/eb/Afghan_carpet.JPG "https://upload.wikimedia.org/wikipedia/commons/e/eb/Afghan_carpet.JPG")

Hoy en día en Afganistán hay dos idiomas oficiales: Dari y Pashto. Según \[W2021\] la palabra Talibán es una palabra en idioma pashto “طالبان” que se traduce como estudiantes.

Una línea de tiempo ampliada de \[LTIEMPO2021\] incluye:

* 619 Árabes musulmanes llevan el Islam a Afganistán que antes practicaba más Zoroastrismo, Budismo e Hinduismo.
* Siglo 11. Islam conquista todo el país.
* 1919 Independencia de Inglaterra.
* Dic.1979: Invasión soviética que instaló un gobierno pro-soviético, causando rebelión interna y financiación de Estados Unidos a la resistencia.
* 1989 Con la desintegración de la Unión Soviética, salen las tropas soviéticas de Afganistán.
* 1992  Destronan al gobierno pro-soviético y el país entra en guerra civil.
* 1994 Emergen los talibán de islamistas de Pakistán y Afganistán. El gobierno de Pakistán ha apoyado continuamente a los talibán desde entonces, con el interés de mantener en Afganistán un gobierno islámico pro-pakistaní.
* 1996 La mayoría del país queda dominado por los talibán.
* 11.Sep.2001. Miembros de al-Queda desde Afganistán reconcieron haber planeado y ejecutado atentados a edificios en Estados Unidos. Sin embargo hay evidencia de como líderes de Estados Unidos planearon y aumentaron los efectos del atentado (pues los edificios derribados no habrían sido derribados por los impactos de los aviones sino por cargas explosivas puestas en sus bases,  ver \[WTC7\]) para obtener provecho personal, económico, político y militar (ver \[TL911\]).
* 7.Oct.2001 Alianza entre Estados Unidos e Inglaterra bombardea Afganistán para derrocar al gobierno Talibán.
* 17.Dic.2001 Los Talibán pierden control de buena parte de Afganistán e inicia gobierno pro-Estados Unidos. Una parte del país sigue bajo el control de los talibán.
* 2.May.2011 Asesinato de Osama Bin Laden, líder al-Queda por parte de tropas de Estados Unidos.
* Sep.2014 Coalición internacional de la OTAN se retira de Afganistán.
* Feb.2020 Negociación entre Estados Unidos y los Talibán para un retiro de tropas de Estados Unidos en 2021.
* May.2021 Evacuación de tropas de Estados Unidos de Afganistán.
* 6.Ago.2021 Caen primeras capitales de provincias ante Talibán.
* 8.Ago.2021 Caen ante los talibán capitales de las provincias Sar-e-Pul, Kunduz y Taloqan
* 11.Ago.2021 Caen ante los talibán las capitales de las provincias Badakhshan y Baghlan.
* 13.Ago.2021 Cae Kandahar, la segunda ciudad más grande
* 15.Ago.2021 Cae ante los talibán en la capítal Kabul. El presidente Afgano huye y colapsa la República Islámica de Afganistán, instalándose nuevamente el Emirato Islámico de Afganistán.

Los talibán se consideran de la etnia Pashtun en su mayoría se han dedicado a la guerra, practican una mezcla de Islamismo extremo mezclado con leyes anteriores de su etnia Pashtun. En los lugares donde han gobernado ha sido común el asesinato étnicos, es decir de personas de etnias diferentes a la Pashtun, la denigración de las mujeres impidiendoles educarse y manteniendolas en vestidos que les cubre hasta el rostro en público y siempre acompañadas de un hombre de su familia. No permiten una religión diferente al Islam con la interpretación que le dan e incluso no permiten críticas a su interpretaciónd el Coran por parte de otras líneas del Islam. Proclaman la jiha (o guerra santa) como el método de imponer su creencia religiosa, durante su gobierno hay reportes de asesinatos continuos a trabajadores de agencias humanitarias que tenían nombre cristiano porque consideraban que era proselitismo religioso.

Políticamente el gran opositor de los talibanes en Afganistan ha sido el Frente Unido (o alianza del norte) otro grupo islamista con más respeto por las mujeres la diversidad étnica y reconocimiento internacional, pero que militarmente ha resultado reiteradamente derrotado por los talibán.

Como fuentes de financiación hay reportes (ver \[WTALIBAN2021\]) que indican que son impuestos a las drogas (exportación ilegal de opio por parte de terratenientes Afganos), minería, extorsión e impuestos, donaciones y exportaciones. Internacionalmente solo han sido reconocidos por Pakistán, Arabia Saudita y Emiratos Árabes. Motivados seguramente por mantener en el poder grupos Islamistas en tantos territorios como sea posible.

[https://www.youtube.com/watch?v=NH_IKGInYks](https://www.youtube.com/watch?v=NH_IKGInYks "https://www.youtube.com/watch?v=NH_IKGInYks")

# 4. Análisis

Volvamos a leer los versículos centrales de está prédica pensando que se refiere a los y las cristianas en Afganistán.

| Versículos | Observación |
| --- | --- |
| 1 | Oremos por esos cristianos que están en Afganistán y los que estamos fuera para que podamos hacer correr la palabra del Señor |
| 2 | Para que Dios libre a los cristian@s de Afganistán de los talibanes que no respetan la religión, ni la vida de quienes practican otra religión o de quienes no tienen sus raíces étnicas. Y por cambio de esa ideología de guerra en los talibán. |
| 3 | Así como Dios libró a la iglesia primitiva, librará a la iglesia en Afganistán. |
| 4 | El mandamiento central que tenemos los cristianos, resumido en Mateo 22:37-39 es amar a Dios sobre todo y al prójimo como a nosotros mismos. A diferencia de los talibán debemos amar a los talibán como a nosotros mismos aún con sus creencias tan diferentes, oremos por su conversión. Nosotros no vemos cómo pueden salir del encierro social en el que se han metido, pero Dios sí puede verlo y hacerlo. |
| 5 | Aquí están las claves para la evangelización para nosotros: amor y paciencia. |

# 5. Conclusión y oración

Conozco cristianos que buscaron emplear las invasiones de Estados Unidos a Afganistán e Irak como oportunidad para evangelizar en esos y en otros países islámicos, aprovechando la protección que sentían de las fuerzas militares de Estados Unidos.

Según \[CAFG2021\] en Afganistan hay entre 500 y 8000 cristianos --muchos de los cuales se mantienen en secreto por las dificultades de practicar la fe en medio de una sociedad Islamista y más en una extrema como la que se cernía y ahora es ejercida por los talibanes. Oremos para que aumenten esos números ahora que no pueden acudir a protección humana sino sólo de nuestro Señor.

Señor que podamos poner en práctica el amor y la paciencia en la evangelización de las personas cercanas y de otras que en el momento están geográficamente lejos como los musulmanes de Afganistán.

Los cristianos debemos entender la lucha espiritual con el Islam, cuya inflluecia es muy alta, como se muestra en el siguiente mapa de países con mayoría islámica ([https://es.wikipedia.org/wiki/Mundo_isl%C3%A1mico#/media/Archivo:Islam_by_country.svg](https://es.wikipedia.org/wiki/Mundo_isl%C3%A1mico#/media/Archivo:Islam_by_country.svg "https://es.wikipedia.org/wiki/Mundo_isl%C3%A1mico#/media/Archivo:Islam_by_country.svg"))

![](/assets/images/pasted-image-0-1.png)

También oremos por los refugiados de Afganistán que están huyendo en este momento de ese país, para que puedan ser recibidos por otros países --incluyendo Colombia y Estados Unidos. Y para las familias e iglesias que ayuden y abran puertas para que tengan una bendición especial de Dios para lograr mostrar el evangelio del amor en la práctica que se refleje en conversiones.

Y retomemos las peticiones de oración de la introducción

1. Orar por protección de Dios para ellos/as.
2. Orar por sabiduría bien para quedarse o bien para salir.
3. Orar por fraternidad. Los creyentes en Afganistán deberán mantenerse en secreto, especialmente de sus familias, oremos para que puedan conectarse y que puedan saber que no están solos.

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* \[LTIEMPO2021\] [https://www.usatoday.com/story/news/politics/2021/08/15/timeline-afghanistans-history-and-us-involvement/8143131002/](https://www.usatoday.com/story/news/politics/2021/08/15/timeline-afghanistans-history-and-us-involvement/8143131002/ "https://www.usatoday.com/story/news/politics/2021/08/15/timeline-afghanistans-history-and-us-involvement/8143131002/")
* \[WTALIBAN2021\] [https://en.wikipedia.org/wiki/Taliban](https://en.wikipedia.org/wiki/Taliban "https://en.wikipedia.org/wiki/Taliban")
* \[CAFG2021\] [https://en.wikipedia.org/wiki/Christianity_in_Afghanistan](https://en.wikipedia.org/wiki/Christianity_in_Afghanistan "https://en.wikipedia.org/wiki/Christianity_in_Afghanistan")
* \[WTC7\] [https://ine.uaf.edu/wtc7](https://ine.uaf.edu/wtc7 "https://ine.uaf.edu/wtc7")
* \[TL911\] [http://www.historycommons.org/project.jsp?project=911_project](http://www.historycommons.org/project.jsp?project=911_project "http://www.historycommons.org/project.jsp?project=911_project")