---
layout: post
title: "Oseas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##Nombre

Oseas: Salvación ({1}). Hebreo: &#1492;&#1493;&#1465;&#1513;&#1461;&#1473;&#1506;&#1463; H?-&#275;&#259;&#703;.  Griego: &#8040;&#963;&#951;&#941; = &#332;s&#275;e ({2}). Inglés: Hosea.

[http://fe.pasosdejesus.org/sites/FE.PASOSDEJESUS.ORG/spages/imagenes/hosea%25204%2520-%25206%2520because%2520thou%2520hast%2520rejected%2520knowledge.jpg]
Ver créditos en {3}


##Genealogía

| Parentezco | Persona | Referencia |
| Papá | [TBeeri] | Oeas 1:1 |
| Mamá | | |
| Esposa | [Gomer] | Oseas 1:3 |
| Hijos: | |  |
| Segunda esposa | Cetura  | Génesis  25:1 |


##Datos sobre Oseas

##Referencias


* {1} Easton Bible Dictionary. 1897. http://www.ccel.org/ccel/easton/ebd2.html http://www.ccel.org/e/easton/ebd/ebd/T0000000.html#T0000054   Dominio Público.
* {2}  http://es.wikipedia.org/wiki/Oseas_(profeta)
* {3} http://thebiblerevival.com/clipart16.htm. Public Domain Clipart Collection #16
