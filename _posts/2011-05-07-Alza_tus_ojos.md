---
layout: post
title: "Alza_tus_ojos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Jose Luis Reyes

<pre>
Por mucho tiempo yo viví
en la orilla sin saber
que existía la profundidad
Por tanto tiempo yo pensé
que todo se detuvo aquí
y que no hay nada más alla

    CORO
Hasta/Desde que esa voz oi
que me dijo levantate
Y te mostraré lo que tengo para ti
Alza tus ojos
tan lejos como puedas ver,
Dios te ha dado el poder 
de ver más alla para vencer.
Alza tus ojos
y mira a tu alrededor
la promesa está a tu lado justo ahí, 
levanta tus manos porque Dios, 
confía en ti

Desde entonces no es igual
ahora se lo que es real
no quiero volver atrás
ahora soy un vencedor
su promesa me alcanzó
ahora se a donde ir

//CORO//

En tí
</pre>


* Video: http://www.youtube.com/watch?v=rV3RPYjUeno
* Karaoke: http://www.youtube.com/watch?v=2G2LrGfAIrg
