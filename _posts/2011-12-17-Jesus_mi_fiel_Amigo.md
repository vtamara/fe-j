---
layout: post
title: Jesús mi fiel Amigo
author: vtamara
categories:
- Alabanza
image: assets/images/home.jpg
tags: 

---
Abel Zabala

```
HABLADO:

Enamóranos tú Jesús, mi fiel amigo
Enamóranos de ti, Señor

Jesús mi fiel amigo, mi dulce caminar
Quédate conmigo, no quiero volver atrás
No quiero volver atrás

Llévame a allá
Donde se que habrá paz
Donde tengo que callar
Para escucharte hablar
Donde todo es realidad
Y el tiempo no existe más

Una y otra vez
Al estar, yo junto a Ti
No me puedo contener
Cuando me miras así
Ya no hay nada que decir
Eres todo para mí

HABLA: Jesús mi fiel amigo

Jesús mi fiel amigo, mi dulce caminar
Quédate conmigo, no quiero volver atrás
No quiero volver atrás (bis)

No quiero volver atrás (bis)

HABLADO:

Solo en ti Señor, quiero estar
Solo en tu presencia
Solo contigo queremos estar Señor Jesús
Levantas tus manos y deja yo te lo inicie
No quiero volver atrás (bis)

Jesús mi fiel amigo, mi dulce caminar
Quédate conmigo, no quiero volver atrás
No quiero volver atrás

No quiero volver atrás (3)


HABLADO:

Haz una oración, ahí en tu lugar dile Señor
Yo quiero permanecer solo en ti
Que tu presencia nunca se aparte Señor
Ve a mi, derrama tu espíritu Señor
Sobre nosotros

Mi Dios, Jesús mi fiel amigo
Mi dulce caminar
Eme aquí Señor Jesús
Mi amigo Jesús
Tú eres mi fiel amigo

HABLADO:

Gracias Señor Jesús,
Solo en ti podemos confiar Señor,
Tú eres nuestro mejor amigo Dios
```

* Letra basada en la de: <http://www.musica.com/letras.asp?letra=1022905>
* Video: <http://www.youtube.com/watch?v=RHBEXEmkhRs>