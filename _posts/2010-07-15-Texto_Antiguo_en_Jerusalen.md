---
layout: post
title: "Texto_Antiguo_en_Jerusalen"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Eilat Mazar y otros arqueologos que excavan en Jerusalen hallaron un texto escrito en acadio cuneiforme y datado en el 1400aC.  El texto está en arcilla de Jerusalen, y fue escrito con gran habilidad lo que confirma la importancia de esa ciudad en la era de Bronce temprana.

[http://a34.idata.over-blog.com/0/28/20/84/12--IM-Avr-10/ap_israel_oldest_document_13jul10_480.jpg]

##REFERENCIAS

* {1} Oldest Known Document Uncovered in Jerusalem. http://www1.voanews.com/english/news/middle-east/Israel-Uncovers-Oldest-Known-Document-in-Jerusalem-98356309.html
