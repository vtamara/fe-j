---
layout: post
title: "Hechos_con_Concordancia_Strong"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
El 6 de Enero se conmemora la visita de unos sabios de oriente al Rey que nació hace unos 2010 años.  Con motivo de esa fecha, y para celebrar ese cumpleaños, tenemos como regalo una traducción de dominio público de Hechos de los Apostoles con concordancia Strong de cada palabra con respecto al Textus Receptus:

http://traduccion.pasosdeJesus.org/Hechos/

Las mejoras son bienvenidas en el grupo público http://groups.yahoo.com/group/evangelios-dp/
