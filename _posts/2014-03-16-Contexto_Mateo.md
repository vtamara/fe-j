---
layout: post
title: "Contexto_Mateo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Los sitios donde estuvo Jesús de acuerdo a Mateo:


* 2.1. Belén.  Nacimiento de Jesús
* 2.13. Egipto. Huyen allí María, José y Jesús
* 2.23. Nazaret. Retornan a Israel
* 3.13. Jordan en Galilea.   Jesús se bautiza con Juan.
* 4.1. Deiserto. Jesús fue para ser tentado.
* 4.13. Nazaret y pasa a Capernaum. Comienza a anunciar Reino.
* 4.18. Mar de Galilea.  Hizo pescadores de hombres a Simón (Pedro), Andrés, Santiago y Juan.
* 4.23. Galilea.  La recorre enseñando en las sinagogas, predicando evangelio del Reino y sanando enfermedades y dolencias.  También enseña al aire libre en parábolas.
* 8.5. Capernaum. Sana siervo del centurion.
* 8.14. Casa de Pedro. Sana suegra de Pedro.
* 8.18. Monte de Galilea. Enseña. Los que querían seguir. Calma Tempestad.
* 8.28. Lado gadareno del mar de Galilea.  Expulsa demonios de endemoniados gadarenos. 
* 9.1. Su ciduad (Nazaret). Sana Paralítico. Llama a Mateo. Enseña. Da vista a ciegos. Abre boca de mudo.
* 9.35. Aldeas. Las recorría enseñando en sinagogas, predicando el evangelio del Reino y sanando toda enfermedad y dolencia. Dio autoridad a discípulos y los envio de 2 en 2 a ovejas perdidas de Israel.  Enseñó.
* 11.1. Ciudades de apostoles. Fue a enseñar y predicar. Lamento por Betsaida, Corazín y Capernaum.  Señor del dia de Reposos.  Sanidad en sinagoga. Oposición de Fariseos y líderes religiosos.
* 13.1. Junto al mar de Galilea. Enseñó en parábolas: Sembrador. Propósito de Parábolas. Explica Sembrador a sus discípulos. Parábola del trigo y cizaña. Parábola de la Semilla de Mostaza. Parábola de la levadura. Uso de parábolas. Explica a sus discípulos la de la cizaña. Tesoro escondido. Perla de gran precio. Red. Tesoros nuevos y viejos.
* 13:54. Nazaret. Eseña. Muerte de Juan Bautista (en Jerusalén)
* 14:13. Mar de Galilea. Alimentación de 5000. Anduvo sobre el mar. 
* 14:34. Genesaret. Sana enfermos.
* 15:1. Jerusalén. Ante cuestionamiento de Fariseos y Escribas explica lo que contamina al hombre.  
* 15:21. Tiro y Sidón. Sanidad a hija de mujer cananea. 
* 15:29. Junto al mar de Galilea. Sanió a muchos.  Alimentación de 4000.
* 15:39.  Magdala junto al mar de Galilea. Fariseos y Saduceos demandan una señal.  Enseña a sus discípulos sobre levadura de fariseos.
* 16:13 Cesarea de Filipo. Pedro lo reconoce como Mesías. Primer anuncio de muerte y resurrección.
* 17:1 Monte alto. Transfiguración. Al volver sana joven lunático.
* 17:22 Galilea. Segundo anuncio de muerte y resurrección.
* 17:24 Capernaum.  Pago impuestos de Él y Pedro. Enseño directo a discípulos y con Parábolas.
* 19:1. Judea al otro lado del Jordán. Bendice niños. Enseña.
* 20:17. Hacía Jerusalén. Anuncia por 3ra vez su muerte. Enseña a discípulos. Da vista a ciegos.
* 21:1. Betfagé. Prepara entrada a Jerusalén. 
* 21:10. Jesuralén. Entrada. Purifica templo. Maldición de la Higuera Esteril. Autoridad de Jesús. Parábola de los dos hijos. Parábola de los labradores malvados. Parábola de la fiesta de bodas. Cuestión del tributo. Pregunta sobre la resurrección. El gran mandamiento.  De quien es hijo el Cristo. Hipócritas. Se lamenta por Jerusalén. Predice destrucción del templo. Enseñanza a discípulos: señales antes del fin, Advenimiento del Hijo del Hombre. Parábola de Diez Vírgenes. Parábola de los Talentos.  El juicio a las naciones. Complot para prenderlo.
* 26:6. Betania. Ungido.  Judas se ofrece para entregarlo. Celebración de la Pascua. Anuncia negación de Pedro.
* 26:36. Getsemaní.  Oración. Lo apresan.
* 26:57. Jerusalén. Templo de Caifas. Lo llevan, es condenado. Pedro niega a Jesús.   Donde Pilato. Muerte de Judas. Interrogado por Pilato. Es condenando a muerte. Simón ayuda a llevar cruz. 
* 27:33. Gólgota. Crucifixión y muerte. 
* 27:60. Peña en Jerusalén. Sepultado. Guardia ante la tumba. Resurrección. Informe de la guardia. 
* 28:16 Monte de Galilea. La Gran Comisión.
