---
layout: post
title: Yo solo quiero estar donde Tu estás
author: vtamara
categories: 
image: "/assets/images/table-in-tulip-field.jpg"
tags: 

---
## Yo sólo quiero estar donde tu estás
Juan Carlos Alvarado

```
Yo solo quiero estar donde tú estás 
Viviendo a diario en tu presencia 
No quiero darte solo adoración Yo quiero ser adorador

Yo solo quiero estar donde tú estás 
En tu habitación por siempre 
Llévame al lugar donde tú estás 
Yo quiero estar donde tú estás

Yo quiero estar contigo
Morando en tu presencia 
Comiendo de tu mesa Y rodeado de tu Gloria 
En tu presencia es donde siempre quiero estar
Yo solo quiero estar
Yo quiero estar donde tú estás

Oh mi Dios Tú eres mi fuerza y mi canción
Y aunque débil sea En tu presencia fuerte soy

//Yo solo quiero estar donde tú estás
En tu habitación por siempre
Llévame al lugar donde tú estás
Yo quiero estar donde tú estás//

Yo quiero estar contigo
Morando en tu presencia
Comiendo de tu mesa Y rodeado de tu Gloria
En tu presencia es donde siempre quiero estar

///Yo solo quiero estar
Yo quiero estar donde tú estás///
```

# Cancion

http://www.youtube.com/watch?v=W14mg_zQpZc

# Pista

http://www.youtube.com/watch?v=GYWA-nemq4Q

# Midi

http://www.midisaya.com/dispcanto.asp?ID=MIDIS04/YoSoloQuieroEstar.txt