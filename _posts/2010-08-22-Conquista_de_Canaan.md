---
layout: post
title: Conquista_de_Canaan
author: vtamara
categories: 
image: "/assets/images/palestine_time-0f-conquest.png"
tags: 

---
| **Advertencia:** El mandamiento que nuestro Señor Jesucristo dejo de amarnos como Él nos ha amado, excluye matar.  La teología anabautista nos invita a buscar obrar sin violencia pero con firmeza, decisión y valor.  Con respecto a los eventos de violencia narrados en el Antiguo Testamento {2} dice "Para el pueblo cristiano, lo importante debe ser la manera como Jesús interpreta el Antiguo Testamento.  Si miramos la manera como Dios libra a Israel de los ejércitos del faraón, notaremos que lo hizo en el mar, sin batalla alguna ... Aunque el pueblo peregrinó largo tiempo en el desierto en medio de guerras y conflictos y entró a Canaán luego de batallas sangrientas, éste no fue el plan original de Dios puesto que quería un pueblo que practicara la justicia y la paz; definitivamente la desobediencia y la falta de confianza en Dios hacen que se origine la violencia.  El pueblo israelita a pesar de la liberación milagrosa a través del Mar Rojo, no confió en que Dios caminaría con ellos (Éxodo 23:23 y Deut. 1:26-27)."  |

Desde siempre, el Señor ha amado también a quienes no lo han obedecido y a quienes no creen como dice:

| Deuteronomio 10:17-18 |
| Porque Jehová vuestro Dios es Dios de dioses y Señor de señores, Dios grande, poderoso y temible, que no hace acepción de personas, ni toma cohecho; |

Aunque es misericordioso, dando a quien le pide y dejandose encontrar de quien le busca, también es justo y no deja sin castigo la maldad sin arrepentimiento.

El plan de salvación que incluye a todos los que quieran aceptarlo, requería en ese tiempo (alrededor del 1400aC) al pueblo de Israel asentado al menos un tiempo en Canaán, espacio ocupado por otros pueblos: Cananeos, Jebuseos, Amonitas, Filisteos, etc.  Un mapa propuesto para la época de Josué es:

![](/assets/images/palestine_time-0f-conquest.png)Mapa tomado de {3}

Por la desobediencia del pueblo judio tuvieron que luchar bastante con esos pueblos y sufrir tanto para llegar como una vez asentados.

Entre las instrucciones del pueblo judio en Canaan estaban:

| Deuteronomio 9:3-5 |
| Entiende, pues, hoy, que es Jehová tu Dios el que pasa delante de ti como fuego consumidor, que los destruirá y humillará delante de ti; y tú los echarás, y los destruirás en seguida, como Jehová te ha dicho.  No pienses en tu corazón cuando Jehová tu Dios los haya echado de delante de ti, diciendo: Por mi justicia me ha traído Jehová a poseer esta tierra; pues por la impiedad de estas naciones Jehová las arroja de delante de ti.  No por tu justicia, ni por la rectitud de tu corazón entras a poseer la tierra de ellos, sino por la impiedad de estas naciones Jehová tu Dios las arroja de delante de ti, y para confirmar la palabra que Jehová juró a tus padres Abraham, Isaac y Jacob.  |

| Deuteronomio 7:2-4 |
| y Jehová tu Dios las haya entregado delante de ti, y las hayas derrotado, las destruirás del todo; no harás con ellas alianza, ni tendrás de ellas misericordia.  Y no emparentarás con ellas; no darás tu hija a su hijo, ni tomarás a su hija para tu hijo. Porque desviará a tu hijo de en pos de mí, y servirán a dioses ajenos; y el furor de Jehová se encenderá sobre vosotros, y te destruirá pronto. |

Lo central de la conquista duró unos 7 años (1401 a 1394), comenzando con la victoria en Jericó cuyas características se repiten en otros momentos: (1) obrar sobrenatural de Dios, (2) que los Israelitas no preparados en arma tuvieron que derramar sangre, (3) que a los cananeos que temieron al Dios de Israel (como Rahab y su familia y los gabaonitas) Dios les preservó la vida.

El libro de Josué es un relato cronológico que en general exalta a Dios y la obediencia de Josúe para lograr victoría sobre los guerreros Cananeos --la lista en 12:24 de 31 reyes derrotados es impresionante. Aunque en ocasiones parece emplear hipérboles o exageraciones típicas de los relatos de guerra, como hace notar Patton, A. 2020 por ejemplo en 10:36-39 dice que no dejaron habitante vivo en Hebrón ni Debir, pero más adelante en 15:13-15 dice que había cananeos en esas ciudades.

Puede verse un mapa con el orden de varias batallas en [https://www.youtube.com/watch?v=itf-H7BYL9](https://www.youtube.com/watch?v=itf-H7BYL9k "https://www.youtube.com/watch?v=itf-H7BYL9k")

Como indica Patton, A. 2020, el objetivo de la conquista no era destruir a las personas, sino erradicar la idolatría y dar espacios para habitar al pueblo de Israel, por eso tenían el mandamiento de Deuteronomio 20:10-11 era buscar paz antes de combatir: “10 Cuando te acerques a una ciudad para combatirla, le intimarás la paz. 11 Y si respondiere: Paz, y te abriere, todo el pueblo que en ella fuere hallado te será tributario, y te servirá.”

  
Tras la victoria de nuestro Señor Jesucristo sobre el enemigo y el poder que nos ha concedido a quienes creemos, no requerimos destruir, aunque las luchas continúan pero en Espíritu como se expresa en Efesios 6:12 **Porque no tenemos lucha contra sangre y carne, sino contra principados, contra potestades, contra los gobernadores de las tinieblas de este siglo, contra huestes espirituales de maldad en las regiones celestes.**

Desde Adán hasta Cristo, nos ha quedado un testimonio histórico de las consecuencias de vivir en desobediencia, guerras, destrucción y como el mismo pueblo de Dios se vio obligado a entrar en esa lógica de sufrimiento, en Cristo podemos recibir perdón de pecados, reconciliarnos como hijos del Padre Eterno y vivir confiados en Él que es nuestro Protector, ¿queremos aceptarlo hoy? (ver [Oración de fe](https://fe.pasosdejesus.org/Oracion_de_fe/ "Oración de fe")).

## Referencias

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/versions/index.php?action=getVersionInfo&vid=60
* {2} La objeción de Conciencia como Ejercicio de la Noviolencia en la Construcción de Paz. Manuales de capacitación en construcción de paz. Serie Justapaz. 2004. 
* {3} http://www.foundationsforfreedom.net/Article/Bible/OT/OT_Historical/Joshua/Palestine_Time-of-Conquest.png  Copia libre.
* Patton, Andy, 2020. Why Did God Command the Invasion of Canaan in the Book of Joshua? [https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/](https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/ "https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/")