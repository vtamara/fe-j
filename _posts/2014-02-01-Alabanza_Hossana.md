---
layout: post
title: "Alabanza_Hossana"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Marcos Barrientos

<pre>
Levantamos un clamor por sanidad y redención 
Muestranos lo que Tu ves los secretos de Tu corazón. 
Un pueblo unido pide hoy Tu libertad y salvación 
Armanos con Tu valor lo que deseamos es revolución. 

Que el cielo se parta en dos... inundanos 
que en el desierto broten rios... vida sopla hoy 

// Hossana al Rey de Salvación 
Hossana al Dios Altisimo 
Ho-ssa-na 
Jesucristo, Jesucristo es REY // 

Levantamos un clamor por sanidad y redención 
Muestranos lo que Tu ves los secretos de Tu corazón. 
Un pueblo unido pide hoy Tu libertad y salvación 
Armanos con Tu valor lo que deseamos es revolución. 

Que el cielo se parta en dos... inundanos 
que en el desierto broten rios... vida sopla hoy 

// Hossana al Rey de Salvación 
Hossana al Dios Altisimo 
Ho-ssa-na 
Jesucristo, Jesucristo es REY // 

// Hossana, hossana, 
hossana al Rey// 

// Hossana al Rey de Salvación 
Hossana al Dios Altisimo 
Ho-ssa-na 
Jesucristo, Jesucristo es REY //

</pre>


* Letra adaptada de: http://www.musica.com/letras.asp?letra=1721680
* Video: http://www.youtube.com/watch?v=iLQbUfNwz9Y
