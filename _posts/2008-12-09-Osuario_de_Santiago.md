---
layout: post
title: "Osuario_de_Santiago"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Alrededor de la autenticidad del osuario que ha venido a conocerse como "Osuario de Santiago" se formó un enorme debate jurídico (descrito en detalle en {3}) . En el 2002 fue exhibido en Toronto como auténtico por parte del Lemaire. En Marzo de 2003 cuando el osuario regresó a Israel, fue confiscado por el Instituto de Antigüedades de Israel y en Junio de 2003 un comité de expertos declaró que era una falsificación.   El propietario del osuario Oded Golan fue detenido por la policía Israelí desde 2003 y sólo un año después en diciembre de 2004 fue demandado junto con otras cuatro personas por supuesta falsificación de la parte de la inscripción que dice "hermano de Jesús" (pues el resto del osuario se considera auténtico).  Los cargos le fueron retirados a 2 de los acusados, otro se declaró culpable de un delito menor no relacionado con la acusación principal.  El juicio contra Oded Golan y Robert Deutsch (experto en descifrar hebreo antiguo) duró 7 años, contó con declaraciones de 126 expertos que produjeron más de 12.000 páginas de argumentos en favor y en contra,  hasta que el 14 de Marzo de 2012 el juez Aharon Farkash determinó inocencia respecto a falsificación diciendo que los demandantes habían fallado en probar más alla de toda duda razonable que Golan o alguien obrando a nombre de él hubiera falsificado la inscripción.   El juez también dijo que el debate sobre la autenticidad de la inscripción podía continuar en círculos académicos pero lamentó que unas pruebas realizadas por la policía Israelí dañaron la inscripción haciendo eventualmente imposible realizar estudios posteriores.  El osuario le fue devuelto a Oded Golan --quien estuvo detenido al menos 18 meses en prisión domiciliaria por este caso-- en Noviembre de 2013, y planea exhibirlo en algún museo de Israel.
Continúa el juicio contra Oded Golan, iniciado en 2004 por supuesta falsificación de la inscripción de un osuario que dice:
Santiago hijo de José, hermano de Jesús.

[http://1.bp.blogspot.com/-40rbn6XoCyw/Un_0Xk91lQI/AAAAAAAADgo/L34Lva6W6Ys/s1600/%D7%A2%D7%95%D7%93%D7%93+0019.jpg]
(Imagen de !http://1.bp.blogspot.com/-40rbn6XoCyw/Un_0Xk91lQI/AAAAAAAADgo/L34Lva6W6Ys/s1600/%D7%A2%D7%95%D7%93%D7%93+0019.jpg )

!Fechas importantes en el caso


* 1975 Odad Golan compra el osuario, no sabe la importancia de la inscripción
* 2002 André Lemaire lo descubre en unas fotografías de Golan, discierne su importancia y lo exhibe en Toronto afirmando su autenticidad y que se trata del único elemento conocido que se relaciona con la familia de Jesús.
* Mar.2003 Instituto de Antiguedades de Isarel confisca el osuario junto con la tableta de Joás.
* Jun.2003 Un panel de experto de los acusadores declara que es una falsificación.
* 2003 Golan es detenido por supuesta falsificación del osuario la tableta de Josías.
* Otoño de 2005 Comienza el juicio en el distrito de Jerusalen, con el juez  Aharon Farkash.  Golan es acusado de 44 crimenes.
* 5.Ene.2009 De acuerdo a http://www.bib-arch.org/ el juez del caso (Aharon Farkash) invito a los fiscales  (encabezados por Eli Abarbanel) a retirar el caso, pero se negaron.
* 31.Mar.2009 Robert Deutch uno de los acusados dice que se trata de una cacería de brujas iniciada por el IAAA para desacreditarlo a él y otros comerciantes de antiguedades.  {4} y {3}
* 11.Jun.2009 De acuerdo a {5}  Golan continúa declarandose inocente y expresa confianza en que nuevas pruebas lo determinaran.  Se refiere a la patina que cubre el osuario y que sería de origen microbiano, requiriendo más de 100 años para formarse sin que se conozcan técnicas para crearla artificialmente.
* 14.Mar.2012 el juez Aharon Farkash declara inocentes de falsifiación a Robert Detuch y Oded Golan en un veredicto de 475 páginas.

!REFERENCIAS

* {3} Blog. http://jamesossuarytrial.blogspot.com/
* {4} Jesus ossuary trial' stalled after more than three years. THE JERUSALEM POST. Mar. 31, 2009. MATTHEW KALMAN. 
* {5} Exclusive: “I never faked any antiquity”. 11.Jun.2009. Matthew Kalman  http://matthewkalman.blogspot.com/2009/06/exclusive-i-never-faked-any-antiquity.html
