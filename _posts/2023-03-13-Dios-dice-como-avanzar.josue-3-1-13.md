---
layout: post
categories: []
title: Dios dice como avanzar. Josúe 3:1-13
author: vtamara
image: "assets/images/304070.png"

---
# 1 Introducción

Con motivo de esta predica el Señor me dio y me ha estado enseñando de Josué 3:1-13.

# 2. Texto: Josué 3:1-13

1 Josué se levantó de mañana, y él y todos los hijos de Israel partieron de Sitim y vinieron hasta el Jordán, y reposaron allí antes de pasarlo. 2 Y después de tres días, los oficiales recorrieron el campamento, 3 y mandaron al pueblo, diciendo: Cuando veáis el arca del pacto de Jehová vuestro Dios, y los levitas sacerdotes que la llevan, vosotros saldréis de vuestro lugar y marcharéis en pos de ella, 4 a fin de que sepáis el camino por donde habéis de ir; por cuanto vosotros no habéis pasado antes de ahora por este camino. Pero entre vosotros y ella haya distancia como de dos mil codos; no os acercaréis a ella. 5 Y Josué dijo al pueblo: Santificaos, porque Jehová hará mañana maravillas entre vosotros. 6 Y habló Josué a los sacerdotes, diciendo: Tomad el arca del pacto, y pasad delante del pueblo. Y ellos tomaron el arca del pacto y fueron delante del pueblo.

7 Entonces Jehová dijo a Josué: Desde este día comenzaré a engrandecerte delante de los ojos de todo Israel, para que entiendan que como estuve con Moisés, así estaré contigo. 8 Tú, pues, mandarás a los sacerdotes que llevan el arca del pacto, diciendo: Cuando hayáis entrado³3hasta el borde del agua del Jordán, pararéis en el Jordán. 9 Y Josué dijo a los hijos de Israel: Acercaos, y escuchad las palabras de Jehová vuestro Dios.

10 Y añadió Josué: En esto conoceréis que el Dios viviente está en medio de vosotros, y que él echará de delante de vosotros al cananeo, al heteo, al heveo, al ferezeo, al gergeseo, al amorreo y al jebuseo. 11 He aquí, el arca del pacto del Señor de toda la tierra pasará delante de vosotros en medio del Jordán. 12 Tomad, pues, ahora doce hombres de las tribus de Israel, uno de cada tribu. 13 Y cuando las plantas de los pies de los sacerdotes que llevan el arca de Jehová, Señor de toda la tierra, se asienten en las aguas del Jordán, las aguas del Jordán se dividirán; porque las aguas que vienen de arriba se detendrán en un montón.

# 3. Contexto

Hay una gran división histórica con el cruce del Jordán, es el fin del desierto y el comienzo de la conquista de la tierra prometida.

Incluyo a continuación detalle de la línea de tiempo basada en Dan, B. 2013, que dejé pública en [https://time.graphics/line/773070](https://time.graphics/line/773070) y que usaré para las fechas:

![](https://lh5.googleusercontent.com/8GqKRzW1JuK79cObrKUfZVJiUDMeVuq0LA6qB_PpwmuizA7DsKfEojR7r0c1gJhg_riQ7BcF0gbWo3SG08M3teVK666T3fXDJZ4GqBMyhep3EsFYmtYY0DWbe9ggvPvRtkolzs87PihdCDIkroT19jY =624x229)

Recordemos que Moisés había regresado a Egipto de unos 80 años de edad (año 1443 aC) para guiar a los Israelitas durante su liberación por parte de Dios de la esclavitud en Egipto (año 1442, cuando Amenhotep II sería faraón).

Al año siguiente de salir de Egipto (1441 aC) Dios les dió la Ley en el monte Sinaí y un año después (1440 aC) llegaron al borde de Canaán y enviaron espías para ver la tierra prometida, vieron que fluía leche y miel y que sus habitantes eran guerreros, Caleb, Josúe, Moisés y Aaron confiaban en que el Señor los guiaría para ocupar esa tierra, pero los demás adultos temían y se rebelaron por lo que tuvieron que estar 40 años más en el desierto (de 1442 aC a 1402 aC). Un año antes del cruce del Jordán (1403 aC) Moisés murió y sólo los muy jóvenes en 1440 aC junto con Caleb y Josúe pudieron efectivamente ingresar cruzando el Jordán.

![](https://lh6.googleusercontent.com/T3VvBGlUD566NmesBzg7A3gf68TDcqAsxeSGdloO3DKQXTGVd4KvEEXk2uQCc-YmaJ2Zma6f1esedLddK2rRogrZmlqpdDGjY-xvDzm5wINaL4B4dsI3dOv1L3nuiyGWBQATwid0E4pT4TK1J9yus4c =563x730)

Mapa de [https://www.foundationsforfreedom.net/References/OT/Historical/Joshua/_res/Joshua00/Palestine_Time-0f-Conquest.png](https://www.foundationsforfreedom.net/References/OT/Historical/Joshua/_res/Joshua00/Palestine_Time-0f-Conquest.png)

La tierra prometida estaba repleta de pueblos guerreros que en su mayoría fueron expulsados.  Lo central de la conquista duró unos 7 años (1401 a 1394), comenzando con la victoria en Jericó cuyas características se repiten en otros momentos: (1) obrar sobrenatural de Dios para sacar al pueblo cananeo, (2) que los Israelitas, no preparados en armas, tuvieron que derramar sangre, (3) que a los cananeos que temieron al Dios de Israel (como Rahab y su familia o los gabaonitas) Dios les preservó la vida.

El libro de Josué es un relato cronológico que en general exalta a Dios y la obediencia de Josué para lograr victorias  --la lista en 12:24 de 31 reyes derrotados es impresionante. Aunque en ocasiones el libro de Josué parece emplear hipérboles o exageraciones típicas de los relatos de guerra, como hace notar Patton, A. 2020 por ejemplo en Josué 10:36-39 dice que no dejaron habitante vivo en Hebrón ni Debir, pero más adelante en Josué 15:13-15 dice que había cananeos en esas ciudades.

Puede verse un mapa con el orden de varias batallas en [https://www.youtube.com/watch?v=itf-H7BYL9k](https://www.youtube.com/watch?v=itf-H7BYL9k "https://www.youtube.com/watch?v=itf-H7BYL9k")

Como indica Patton, A. 2020, el objetivo de la conquista no era destruir a las personas, sino erradicar la idolatría y dar espacios para habitar al pueblo de Israel, por eso tenían el mandamiento de Deuteronomio 20:10-11 que era buscar paz antes de combatir: “10 Cuando te acerques a una ciudad para combatirla, le intimarás la paz. 11 Y si respondiere: Paz, y te abriere, todo el pueblo que en ella fuere hallado te será tributario, y te servirá.”

El desenlace de la conquista se prevé en Josué 13:1-7 cuando Dios ya había conquistado un amplio territorio y le ordenó a Josué distribuir la tierra a suerte entre las 11 tribus de manera que la tierra que les faltaba conquistar fuera conquistada por la tribu a la que le correspondiera. La conclusión del reparto está descrita en Josué 22:43-45

> 43 De esta manera dio Jehová a Israel toda la tierra que había jurado dar a sus padres, y la poseyeron y habitaron en ella. 44 Y Jehová les dio reposo alrededor, conforme a todo lo que había jurado a sus padres; y ninguno de todos sus enemigos pudo hacerles frente, porque Jehová entregó en sus manos a todos sus enemigos. 45 No faltó palabra de todas las buenas promesas que Jehová había hecho a la casa de Israel; todo se cumplió.

# 4. Análisis

| Versículos | Observación |
| --- | --- |
| 1-2 | Llegaron al borde oriental del Jordán y esperaron 3 días, seguramente hasta que Josúe recibió instrucciones de Dios. |
| 3-4 | La distancia a la que el pueblo de Dios debía estar del Arca era de aprox. 1km (suponiendo un codo egipcio de 45cm). La santidad de Dios es real y temible. Por ejemplo según 2 Samuel 6:7 Uza murió por tocar el arca. |
| 5 | Josúe pide al pueblo santificarse. Como indica Berry, R. 1924, en nuestro caso la santificación comienza por rendir nuestra voluntad a la de Dios y es algo que Dios hace cuando le pedimos mediante su Santo Espíritu como indica Hechos 15:8-9: “8 Y Dios, que conoce los corazones, les dio testimonio, dándoles el Espíritu Santo lo mismo que a nosotros; 9 y ninguna diferencia hizo entre nosotros y ellos, purificando por la fe sus corazones.” |
| 6 | Dios organizó y puso de líder a Josué, el pueblo y hasta los sacerdotes lo entendieron y obedecieron. |
| 7-8 | Josué como buen líder cristiano primero escuchó a Dios, en detalle --quién sabe si uso papel y lápiz o si tenía una memoria excelente, pero ya que tenemos papel y lápiz usemoslos cuando Dios nos hable (y bueno después re-leamos lo que nos dijo). |
| 9-12 | Cómo los padres de esos israelitas habían presenciado maravillas de Dios, como la apertura del mar rojo, pero no habían entendido que era la forma en la que Dios los invitaba a confiar en Él, esta vez Josúe lo aclara antes de que ocurra. Los prodigios de Dios son con propósito, no sólo para dejarnos boquiabiertos un momento, sino para prepararnos para las batallas que vienen. |
| 13 | Los sacerdotes tuvieron a prueba su fe y sus pies inicialmente se mojaron antes de ver como Dios detenía las aguas y abría camino. En ocasiones Dios lo requiere, pero no hay que pisar el agua antes o después de que Él indique. |

¿Estás en el Jordán? ¿Pasaste un desierto y ahora Dios te tiene una tierra prometida? Recuerda que tendrás que luchar y ganarás manteniendo la confianza en Dios

# 5. Conclusión y oración

Cuando estemos junto a un Jordán, esperando una bendición que viene al cruzarlo pero con temor por la lucha que requerirá:

* Confiemos en Dios
* Él nos ha dado y nos dará instrucciones, esperemos pacientemente y con discernimiento y obedezcamos sin temor de los gigantes que derrotaremos.
* Pidamos a Dios santificación que comienza por rendir la voluntad.
* Los milagros nos preparan para las luchas que vienen.
* Al borde del Jordán: fe, discernimiento y acción inmediata cuando Dios nos mande a meter los pies en el agua.

¿Estás pidiendole a Dios santificación?

¿Has visto prodigios de Dios? ¿Liberaciones, sanidades? Recordemos que son para prepararnos para confiar en Dios (y no en nuestras fuerzas o en ídolos) cuando venga cada batalla que nos permita recibir lo prometido por Dios. Claro las batallas después del señor Jesús ya no son contra carne ni sangre, sino contra potestades.

Para cruzar el Jordán posiblemente tendremos que mojarnos los pies y entonces veremos como Dios detiene la aguas y abre camino, pero debe ser con el guianza de Él y en el momento preciso que Él de, en otro momento nos lavamos completos o hasta nos ahogamos.

Señor por favor límpianos, háblanos, guíanos y usanos en el nombre de Jesús.

Gracias por las maravillas que has hecho en medio de nosotros, en mi, las sanidades que me has dado, la forma como me has usado en liberaciones, sanidades, provisiones y enseñándome para enseñar, gracias por la llenura de tu Espíritu Santo y la necesidad que reconozco y que tengo de Tí. Entiendo que todo esto es para luchar espiritualmente y contra mi carne. Guíame por favor en el nombre de Jesús.

Aumenta nuestra fe por favor y danos discernimiento para saber cuando es tu voz la que nos dice hacia donde avanzar y en su momento que metamos los pies al agua.

Probablemente sin desobediencia, como la de Acán descrita en Josué 7, y con más confianza en Dios, los Israelitas hubieran tenido que derramar menos sangre (o tal vez nada como en el Mar Rojo donde Dios hizo todo), pero la paga de la idolatría de los pueblos de Canaán fue su sangre, tal como lo sería la mía por mis pecados (o la tuya por tus pecados), si Cristo no hubiera pagado con su sangre por mi y por quienes creemos que Él es el único y suficiente Señor y Salvador. ¿Lo creés? ¿Ya hiciste una [oración de fe](https://fe.pasosdejesus.org/Oracion_de_fe/)?

# 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* Dan, Bruce. 2013. Sacred Chronology of the Hebrew Kings. [http://www.prophecysociety.org/PDF/SC_FREE.pdf](http://www.prophecysociety.org/PDF/SC_FREE.pdf "http://www.prophecysociety.org/PDF/SC_FREE.pdf")
* Patton, Andy, 2020. Why Did God Command the Invasion of Canaan in the Book of Joshua? [https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/](https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/ "https://bibleproject.com/blog/why-did-god-command-the-invasion-of-canaan-in-the-book-of-joshua/")
* Berry, Robert L. 1924. Adventures in the Land of Canaan. [https://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Crossing_of_the_Jordan/](https://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Crossing_of_the_Jordan/ "https://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Crossing_of_the_Jordan/")

***

Imagen de portada de dominio público de https://openclipart.org/detail/304070/feet-3 

Gracias a Dios pude compartir este mensaje en la Iglesia Menonita de Suba, pastoreada por Jaime Ramírez,  el  12.Mar.2023.
