---
layout: post
title: "La transmision de la biblia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

# ¿Cómo se ha transmitido la Biblia?

De ahora hacía atrás:

En una librería puedo encontrar la Biblia, hay diversas de las cuales escoger, 
inicialmente hay 2 opciones: una biblia católica o una cristiana no católica.

## Biblias cristianas católicas y no católicas

Las diferencia fundamental entre ambos tipos de biblias es el canón en el que se 
basan:

* Las católicas se basan en un canon griego o largo, en particular en la Biblia 
  septuaginta, que es una traducción al griego de la Biblia del siglo III d.C.
* Las cristianas no católicas se basan en un canon hebreo o corto, en particular
  en las traducciones hebreas tradicionales y aún usadas por los judios.
* Por esto las biblias católicas tienen 7 libros más en Antiguo Testamento: 
** 4 históricos Tobías, Judit, Ester, I Macabeos, II Macabeos
** 2 sapienciales: sabiduria, eclesiastico
** 1 profeta mayor más Baruc
* Las Biblias católicas tienen algunos capítulos más en los libros de 
  Ester y de Daniel.
* Ambos canones constan de los mismos libros para el Nuevo Testamento


## Traducciones

Por ejemplo la biblia más usada entre protestantes (ver {5}) es la traducción 
Reina Valera editada en 1960 (ver {1}), y una muy popular entre católicos es 
Dios Habla Hoy.

Cada traducción tiene una historia, unas se basan en traducciones ya existentes,
otras en manuscritos en idiomas antiguos (posiblemente en los que 
fueron escritos) o en combinaciones.

El antiguo testamento fue escrito en Hebreo Antiguo, las copias prácticamente 
completas más antiguas en ese idioma fueron encontradas en 1940 cerca al mar 
muerto y se han llamado "Los Rollos de Qumram" 


##Referencias

* {1} Biblia. Reina Valera 1960. http://www.biblegateway.com/versions/?action=getVersionInfo&vid=60
* {2} http://www.ingfems.com/paginas/biblia.rtf
* {3} http://www.buscadoresdelreino.com/diferenbib.htm 
* {4} http://labibliaweb.com/?p=1662
