---
layout: post
title: "ProclamarLaVerdad"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

El Señor nos llama a decir la verdad, pero dado que no es tarea fácil nos ha proveido herramientas para ser efectivos:

* Amor.
* Valor y prudencia.
* Responsabilidad y discernimiento.

##2. Texto

Marcos 4:21-25

<pre>
21 También les dijo: ¿Acaso se trae la luz para ponerla debajo
 del almud, o debajo de la cama? ¿No es para ponerla en el candelero?
22 Porque no hay nada oculto que no haya de ser manifestado; 
ni escondido, que no haya de  salir a luz.
23 Si alguno tiene oídos para oír, oiga.
24 Les dijo también: Mirad lo que oís; porque con la medida con
que medís, os será medido, y aun se os añadirá a vosotros los que oís.
25 Porque al que tiene, se le dará; y al que no tiene, aun lo que
tiene se le quitará.
</pre>
 
##3. Contexto

Esta perícope está en medio de parábolas sobre semillas, la anterior es la parábola del sembrador   y le sigue la comparación del Reino con una semilla y posteriormente con una semilla de mostaza.

En la parábola anterior,  el sembrador esparció semilla que cayó en diversos tipos de tierra y que representa la palabra en corazones de diversos tipos y sólo en los que bien la reciben da fruto.

La siguiente compara el Reino de Dios el crecimiento de una semilla en la tierra, en cuanto el ser humano no sabe como ocurre, pero gracias a Dios ocurre.   Interpretamos que se trata del crecimiento de la Fe en nuestros corazones gracias al Creador.

Entonces esta porción está en un contexto de sembrar palabra y que esa palabra de fruto gracias a Dios.   Decir la verdad es entonces necesario para que de fruto la semilla que sembramos gracias a Dios.

!Vesículo paralelo

Lucas 8:16-18
<pre>
16 Nadie que enciende una luz la cubre con una vasija, ni la pone debajo
de la cama, sino que la pone en un candelero para que los que entran vean
la luz.
17 Porque nada hay oculto, que no haya de ser manifestado; ni escondido, 
que no haya de ser conocido, y de salir a luz.
18 Mirad, pues, cómo oís; porque a todo el que tiene, se le dará; y a 
todo el que no tiene, aun lo que piensa tener se le quitará.
</pre>

Es decir solo falta que con la medida que uno mida será medido, que está en otra parte: Lucas 6:38
<pre>
... porque con la misma medida con que medís, os volverán a medir.
</pre>

##4. Estudiemos versículo a versículo

!4.1 21 También les dijo: ¿Acaso se trae la luz para ponerla debajo del almud, o debajo de la cama? ¿No es para ponerla en el candelero?

Un almud (ver {2} y {3})  se refiere a un cajón de madera en el que podían medirse aproximadamente 9 litros de capacidad, era usado típicamente para medir granos y la capacidad (un almud también) era una medida romana.
[http://farm1.static.flickr.com/94/266949905_8b95989986_z.jpg?zz=1]

Según {4} en los tiempos de Cristo no se empleaban velas, sino lámparas provenientes de una tradición cananita con "una loza de barro o platillo que contenía el aceite de oliva, y un estrecho borde para sostener la mecha,"  
[http://mushecht.haifa.ac.il/pic/archeology/tematic/lamphero.jpg]
Lampara herodiana (empleada entre siglos IaC y 2dC) encontrada en Judea y mantenida en el museo Hecht en Israel (ver {5}).

También según {4}, estas lamparas se mantenían encendidas durante la noche y solían ponerse sobre una mesa de noche que eran altas.

La luz, la palabra del Señor, la verdad debe proclamarse, no puede esconderse y esto evidentemente requiere valor.    

Así como para encender una luz se requiere cuidado y atención, para decir la verdad aún más, aún más cuidad y mucho amor.   Una verdad sin amor puede ser hiriente, imagien unos padres adoptivos que dijeran a su hijo adoptado sin amor que es adoptado.  Seguramente podrá recordar en su vida ocasiones en las que le dijeron una verdad sin amor para herirlo o que termino doliendole.  Entonces digamos la verdad pero con amor.

En nuestra vida debemos eliminar la mentira y el chisme.

! 4.2 22 Porque no hay nada oculto que no haya de ser manifestado; ni escondido, que no haya de salir a luz.

La verdad completa se conocerá, puede que no inmediatamente, pero tendrá su tiempo.  Este es complementario del anterior pues el valor para decir la verdad debe ir acompañado de prudencia.   Miremos Mateo 10:27-31 
<pre>
27 Lo que os digo en tinieblas, decidlo en la luz; y lo que oís al oído,
proclamadlo desde las azoteas.
28 Y no temáis a los que matan el cuerpo, mas el alma no pueden matar;
temed más bien a aquel que puede destruir el alma y el cuerpo en el infierno.
</pre>

Es decir exhorta al valor para decir la verdad, para denunciar lo que está mal, pero notemos que hay cosas que se escuchan al oido, es decir hay cosas que no pueden decirse de inmediato, que requieren un tiempo antes de proclamarse.

Por ejemplo hay situaciones en las que debe mantenerse en secreto el nombre de personas que proveen información sobre violaciones a derechos humanos.   Al respecto el país entero sigue bajo el control de los violentos, no faltan las masacres por parte de la guerrilla como ocurrió este mes en el Naya (ver {7}), ni el controlo económico paramilitar con aquiesencia de la fuerza pública por ejemplo en Guapi, Cauca donde está ocurriendo una destrucción ambiental, del tejido social, reclutamientos forzados  y muchos asesinatos con la entrada de multinacionales mineras avaladas por el alto gobierno junto con paramilitares que desplazan a la población (Ver {6}).  En Soacha el control paramilitar se relaciona con el narcotráfico y es también extensivo (ver {8}) como en otras zonas de la ciudad y del país.   Esto lo sabemos en parte por el valor de personas que lo denuncian, pero también su prudencia para decirlo sólo a ciertos oidos que mantienen en secreto las fuentes.

En casos de violencia política llega también el día que las fuentes de información pueden revelarse y declarar directamente lo que han visto y oido, pero el momento requiere discernimiento dado por el Señor.


!4.3 23 Si alguno tiene oídos para oír, oiga. 24 Les dijo también: Mirad lo que oís; porque con la medida con que medís, os será medido, y aun se os añadirá a vosotros los que oís.

Responsabilidad por nuestras palabras.  

Emplear los dones que el Señor nos ha dado, pues a quien bien los emplee, le dará más, pero a quien no los emplee le quitará.

Veamos lo que Dios dice a través de Pablo sobre dones en 1 Corintios 12:7-11
<pre>
7 Pero a cada uno le es dada la manifestación del Espíritu para
  provecho.
8 Porque a éste es dada por el Espíritu palabra de sabiduría; a otro,
  palabra de ciencia según el mismo Espíritu;
9 a otro, fe por el mismo Espíritu; y a otro, dones de sanidades por
  el mismo Espíritu.
10 A otro, el hacer milagros; a otro, profecía; a otro, discernimiento
  de espíritus; a otro, diversos géneros de lenguas; y a otro, 
  interpretación de lenguas.
11 Pero todas estas cosas las hace uno y el mismo Espíritu, repartiendo
   a cada uno en particular como él quiere.
</pre>


Pidamos discernimiento al Señor sobre nuestros dones, un triste ejemplo de falta de discernimiento en los dones otorgados por el Señor, es un predicador que manejaba 66 estaciones radiales y que logró gestionar e invertir cien millones de dolares en una campaña anunciando el rapto, el regreso de Jesús y terremotos para el 21 de Mayo.  Aunque el Señor de 89 años parece sincero en haberlo creido, se equivocó en el don pues no tiene el don de profecía.   

Respecto a eventos finales los menonitas creemos que Cristo volverá pero que lo importante en nuestras vidas no debe ser el pitazo final sino jugar bien el partido ahora. Creemos que Cristo volverá, no sabemos cuando, pero sabemos que hoy y en cada momento debemos obederlo y hacer lo mejor que podemos para que cuando Cristo vuelva nos encuentre haciendo lo que debemos hacer.


!4.4 25 Porque al que tiene, se le dará; y al que no tiene, aun lo que tiene se le quitará.

No perdamos tiempo, que nuestra palabra de fruto.   Pongamos en práctica los dones que el Señor nos dió, es nuestra responsabilidad, a quien lo haga el Señor le añadirá, a quien no lo haga el Señor le quitará.

En la iglesia nos hemos organizado, el Señor ha puesto dones y roles a cada quien, que cada quien realice su rol con amor.

Ordenemos dentro de la iglesia como explica 1 Corintios 12:12-15
<pre>
12 Porque así como el cuerpo es uno, y tiene muchos miembros, pero
   todos los miembros del cuerpo, siendo muchos, son un solo cuerpo, 
   así también Cristo.
13 Porque por un solo Espíritu fuimos todos bautizados en un cuerpo, 
sean judíos o griegos, sean esclavos o libres; y a todos se nos dio a 
beber de un mismo Espíritu.
14 Además, el cuerpo no es un solo miembro, sino muchos.
15 Si dijere el pie: Porque no soy mano, no soy del cuerpo, 
   ¿por eso no será del cuerpo?
</pre>

Entonces el que es mano que esté pegado del que es brazo y el brazo de tronco y así sucesivamente para que todo el cuerpo obre con el propósito común de predicar el evangelio.

Respecto a conflictos en la iglesia y haciendo analogía con Mateo 18, cuando la mano está interviniendo en un conflicto que no logra mediar, que lo diga al brazo si este tampoco logra resolución, que se le diga al cuerpo, todo con la guianza de la cabeza (que es Cristo).  Pero que la mano no se salte al brazo, todos somos complementarios y organizados para obrar efectivamente.

Ninguno de nosotros es perfecto, pidamos perdón por nuestras fallas pues el Señor quiere perdonarnos, pues nos quiere a su lado, el nos quiere limpios a su lado en la perfección, en la pureza, en la santidad.


!5. Conclusión y Oración

* El Señor nos está capacitando para predicar su palabra, pero es necesario que cambiemos nuestra vida, nuestra forma de hablar, de pensar y de expresarnos para proclamar la verdad con:
** Amor
** Valor y prudencia
** Responsabilidad y Discernimiento
* En situaciones de conflictos la verdad con amor es indispensable pero búsquemos seguir el conducto para expresarla.

Peticiones de oración:
*  Ayudanos a confesar nuestros pecados y  a reparar, para poder estar más cerca a ti, que nos arrepintamos y te pidamos perdón de corazón.  Perdonanos Señor, ayudanos Señor, a ti clamamos, sabemos que tu escuchas un corazón humillado y arrepentido.
* Señor danos discernimiento para distinguir que proviene de ti y por tanto es verdad de lo que no.
* Señor danos fuerzas para proclamar la verdad.    Señor danos amor, muestranos más de tu amor, enamoranos más y más cada vez de ti, para que cuando hablemos, lo hagamos con amor, con tu amor que es perfecto y mezcla bien misericordia con justicia.
* Nos declaramos valientes en el nombre de Jesús para proclamar la verdad.
* Señor muestranos las situaciones, haznos prudentes, moldeanos a tu imagen, para cuidar a otros y cuidarnos también.
* Señor haznos obedientes a tí, Señor hemos elegido esta iglesia como un espacio para buscarte en grupo, que nos organicemos bien, cada uno en su rol, el de profecía, profecía y así cada uno. Por favor que nos sometamos a esos roles que nos has encomendado y que hemos elegido y que nos sometamos unos a otros en tu amor, para hacer la obra con efectividad. 


!6. Bibliografía
* {1} Biblia. Reina Valera. 1960. http://www.biblegateway.com/passage/?search=Marcos+4&version=RVR1960
* {2} http://es.wikipedia.org/wiki/Almud
* {3} http://www.wikicristiano.org/diccionario-biblico/708/almud/
* {4} Wight, Fred. 2002. Costumbres y Maneras de ls tierras bíblicas.  Bogotá.  
* {5} http://mushecht.haifa.ac.il/archeology/OilLamp_eng.aspx
* {6} http://notimundo2.blogspot.com/2011/01/guapi-cauca-paramilitares-controlan-el.html
* {7} http://justiciaypazcolombia.com/Masacre-en-el-Naya
* {8} http://opinionyanalisispolitico.blogspot.com/2011/02/nuevo-paramilitarismo-busca-tomarse.html

!7. Historia

Predica gracias a Dios dada en Iglesia La Resurrección en Soacha, San Nicolas el 29.May.2011.
