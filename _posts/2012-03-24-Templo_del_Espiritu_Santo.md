---
layout: post
title: "Templo_del_Espiritu_Santo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. INTRODUCCIÓN

Personaje del día: El arca del pacto o el arca de la alianza o el arca del testimonio.

Se trata de un utensilio hoy perdido y muy buscado a lo largo de la historia, incluso se ha presentado en cine por ejemplo quienes vieron a Indiana Jones en Cazadores del Arca Perdida.  Se trata de una caja de madera recubierta en oro con dos querubines esculpidos sobre esta. Los detalles de su construcción se describen en Éxodo 25:10-30.  Se trataba de un elemento esencial en el santuario como se verá, y que hoy se relaciona con nuestro corazón, con el Espíritu Santo y con el templo físico que el Señor nos está levantando para la iglesia.


##2. MENSAJE

<pre>
12 Todas las cosas me son lícitas, mas no todas convienen; todas las cosas
   me son lícitas, mas yo no me dejaré dominar de ninguna.
13 Las viandas para el vientre, y el vientre para las viandas; pero tanto
   al uno como a las otras destruirá Dios. Pero el cuerpo no es para la
   fornicación, sino para el Señor, y el Señor para el cuerpo.
14 Y Dios, que levantó al Señor, también a nosotros nos levantará con su poder.
15 ¿No sabéis que vuestros cuerpos son miembros de Cristo? ¿Quitaré, pues,
   los miembros de Cristo y los haré miembros de una ramera? De ningún modo.
16 ¿O no sabéis que el que se une con una ramera, es un cuerpo con ella?
   Porque dice: Los dos serán una sola carne.
17 Pero el que se une al Señor, un espíritu es con él.
18 Huid de la fornicación. Cualquier otro pecado que el hombre cometa, 
   está fuera del cuerpo; mas el que fornica, contra su propio cuerpo peca.
19 ¿O ignoráis que vuestro cuerpo es templo del Espíritu Santo, el cual 
   está en vosotros, el cual tenéis de Dios, y que no sois vuestros?
20 Porque habéis sido comprados por precio; glorificad, pues, a Dios en
   vuestro cuerpo y en vuestro espíritu, los cuales son de Dios.
</pre>

##3. ANÁLISIS

Nuestro cuerpo es templo del Espíritu Santo y así como el Señor nos ordena no fornicar con nuestro cuerpo carnal en el templo Espiritual no debemos fornicar con idolatría.  

El templo de Dios es y ha sido santo, veamos algo de su historia.

| __Templo__ | __Tiempo__ | __Medidas__ | __Usos__ | __Como se construyó__ |
| Altares | -1400aC | | Conmemorar | Piedras |
| Tabernáculo | 1400aC - 1000aC | !100x50codos | Holocaustos, guianza en desierto | Ofrendas del pueblo, instrucciones a Moisés |
| Primer Templo | 960aC - 600aC | !60x20x30 | | Esfuerzo del pueblo y gobernantes, instrucciones a David |
| Segundo Templo | 550aC - 60dC | 60x?x60 | Holocaustos, Fiestas solemnes y Sacrificios Espontáneos |  Ofrendas del pueblo, instrucciones a Ciro y Hageo |
| Aposento Alto | 33dC | - | Oración continúa, Pentecostés | Cosas en común, instrucciones a apóstoles |
| Mi cuerpo | Hoy | mías | Morada Espíritu Santo | Dios plan salvación, instrucciones que Dios me da |

Creo que el Señor reutiliza diseños que ha hecho, dando una estructura de perfección a su creación, así que podemos aprender de nuestra relación hoy con el Espíritu Santo que habita en nuestro cuerpo como templo, así como detalles de la nueva iglesia de San Nicolás en construcción, estudiando los templos que ha empleado el Señor a lo largo de la historia.


!3.1. Antes del templo

Hay pasajes en los que se habla de la presencia de Dios en sitios y como se erigían altares en su honor, por ejemplo en Génesis 35:1

<pre>
Dijo Dios a Jacob: Levántate y sube a Bet-el, y quédate allí; y haz allí un
altar al Dios que te apareció cuando huías de tu hermano Esaú.
</pre>

Sin embargo no se habla de templos o santuarios sino por primera vez durante el Éxodo cuando se empieza a hacer referencia a un sitio para que Jehová habitara en medio del pueblo.

!3.2. Santuario Móvil:

[http://www.peniel-mdq.com/images/plano_taber.gif]
Imagen de http://www.peniel-mdq.com/images/plano_taber.gif

[http://www.lugaresbiblicos.com/images/Tabernacle-desde_arriba-tb103099103.jpg]
Replica del tabernáculo en parque Timna en Israel http://www.lugaresbiblicos.com/tabernaculo.htm

Dios ordenó a Moisés hacer un santuario, y todos los detalles de como construirlo, como conseguir los recursos necesarios, el atuendo de los sacerdotes, los detalles de los utensilios y sus usos, así como la forma del servicio en el mismo y quienes debían hacerlo, todo porque Él haría visible su presencia en ese lugar:

Éxodo 25:1-9
<pre>
1 Jehová habló a Moisés, diciendo:
2 Di a los hijos de Israel que tomen para mí ofrenda; de todo varón que la
  diere de su voluntad, de corazón, tomaréis mi ofrenda.
3 Esta es la ofrenda que tomaréis de ellos: oro, plata, cobre,
4 azul, púrpura, carmesí, lino fino, pelo de cabras,
5 pieles de carneros teñidas de rojo, pieles de tejones, madera de acacia,
6 aceite para el alumbrado, especias para el aceite de la unción y para
  el incienso aromático,
7 piedras de ónice, y piedras de engaste para el efod y para el pectoral.
8 Y harán un santuario para mí, y habitaré en medio de ellos.
9 Conforme a todo lo que yo te muestre, el diseño del tabernáculo, y el
  diseño de todos sus utensilios, así lo haréis.
</pre>

Según Ex 27:18 el atrio que es la parte exterior estaba rodeada por cortinas con columnas o basas de bronce, media 100 codos por 50 codos que en metros según {2} es 45,72m por 22,86m.  El atrio tenía una puerta a continuación un altar de bronce para sacrificio, a continuación una fuente en la que los sacerdotes se lavaban tras los sacrificios y a continuación estaba el tabernáculo.  El tabernáculo era una tienda cubierta con varios tipos de pieles en cuyo interior estaba el lugar santo y el lugar santisimo separados por un velo.  En el lugar santo estaba el candelabro, un altar de incienso, y la mesa de los panes.  En el lugar santísimo estaba el arca de la alianza que tenía las tablas de la ley, la vara de Aaron y maná.  Dios llegaba al lugar santísimo.   

Éxodo 40:1-8
<pre>
1 Luego Jehová habló a Moisés, diciendo:
2 En el primer día del mes primero harás levantar el tabernáculo, el
  tabernáculo de reunión; 
3 y pondrás en él el arca del testimonio, y la cubrirás con el velo.
4 Meterás la mesa y la pondrás en orden; meterás también el candelero
  y encenderás sus lámparas,
5 y pondrás el altar de oro para el incienso delante del arca del 
  testimonio, y pondrás la cortina delante a la entrada del tabernáculo.
6 Después pondrás el altar del holocausto delante de la entrada del 
  tabernáculo, del tabernáculo de reunión.
7 Luego pondrás la fuente entre el tabernáculo de reunión y el altar, y 
  pondrás agua en ella.
8 Finalmente pondrás el atrio alrededor, y la cortina a la entrada del 
  atrio. 
</pre>

Este templo móvil erra llenó de la gloria de Dios como describe Éxodo 40:34-38 y Números 9:15-23:

<pre>
15 El día que el tabernáculo fue erigido, la nube cubrió el tabernáculo
   sobre la tienda del testimonio; y a la tarde había sobre el tabernáculo
   como una apariencia de fuego, hasta la mañana.
16 Así era continuamente: la nube lo cubría de día, y de noche la apariencia
   de fuego.
17 Cuando se alzaba la nube del tabernáculo, los hijos de Israel partían; y
   en el lugar donde la nube paraba, allí acampaban los hijos de Israel.
...
23 Al mandato de Jehová acampaban, y al mandato de Jehová partían, guardando
   la ordenanza de Jehová como Jehová lo había dicho por medio de Moisés. 
<pre>


Este templo móvil operó desde los tiempos de Moisés en el desierto guiándolos y posteriormente como símbolo de la presencia de Dios (por ejemplo en Jericó).   En el tiempo de David se organizó bastante, por ejemplo 1 Cro 23:5 describe el oficio de algunos de los Levitas en ese templo móvil: 
<pre>
Además, cuatro mil porteros, y cuatro mil para alabar a Jehová, dijo
David, con los instrumentos que he hecho para tributar alabanzas. "
</pre>


!3.3. Primer templo de Jerusalén

No hubo otro templo sino hasta los tiempos de Salomón, pues Dios le dio a David --el papá de Salomón-- instrucciones sobre como construir un nuevo templo pero también le explicó porque el mismo David no podía construirlo:
1 Cro 28:3 

<pre>
Mas Dios me dijo: Tú no edificarás casa a mi nombre, porque eres hombre de 
guerra, y has derramado mucha sangre.
</pre>

Nuevamente Dios dio todos los detalles y David puso todo su esfuerzo y poderío de rey en la construcción.   1 Cro 28:19 

<pre>
19 Todas estas cosas, dijo David, me fueron trazadas por la mano de Jehová, 
   que me hizo entender todas las obras del diseño.
20 Dijo además David a Salomón su hijo: Anímate y esfuérzate, y manos a la 
   obra; no temas, ni desmayes, porque Jehová Dios, mi Dios, estará contigo;
   él no te dejará ni te desamparará, hasta que acabes toda la obra para el 
   servicio de la casa de Jehová.
21 He aquí los grupos de los sacerdotes y de los levitas, para todo el 
   ministerio de la casa de Dios, estarán contigo en toda la obra; asimismo 
   todos los voluntarios e inteligentes para toda forma de servicio, y los 
   príncipes, y todo el pueblo para ejecutar todas tus órdenes. 
</pre>

David instó al pueblo a esforzarse en la construcción del Templo. 1 Cro 22:19
<pre>
Poned, pues, ahora vuestros corazones y vuestros ánimos en buscar a Jehová 
vuestro Dios; y levantaos, y edificad el santuario de Jehová Dios, para 
traer el arca del pacto de Jehová, y los utensilios consagrados a Dios, a la 
casa edificada al nombre de Jehová.
</pre>

La base de este templo era de 20 por 60 codos aproximadamente  9.1m por 27.4m,  su altura era de 30 codos o cerca de 13m.   

[http://3.bp.blogspot.com/_68bbSRZAJ7Q/TFNU_EpWvYI/AAAAAAAACWQ/JqlJpQ7y9Xk/s1600/templo-de-salomon%5B1%5D.jpg]

Imagen de http://3.bp.blogspot.com/_68bbSRZAJ7Q/TFNU_EpWvYI/AAAAAAAACWQ/JqlJpQ7y9Xk/s1600/templo-de-salomon%5B1%5D.jpg

Desde su inauguración cuando los sacerdotes ingresaron el arca del pacto, estuvo lleno de la gloria de Jehová:

1 Reyes 8:4,10-11
<pre>
4 Y llevaron el arca de Jehová, y el tabernáculo de reunión, y todos los 
utensilios sagrados que estaban en el tabernáculo, los cuales llevaban los 
sacerdotes y levitas. 
...
10 Y cuando los sacerdotes salieron del santuario, la nube llenó la casa de Jehová.
11 Y los sacerdotes no pudieron permanecer para ministrar por causa de la 
nube; porque la gloria de Jehová había llenado la casa de Jehová.
</pre>

Al igual que en el caso del Tabernáculo el pueblo no podía entrar, era sólo para los sacerdotes, mientras el pueblo y el rey esperaban afuera haciendo sacrificios.

Los reyes de Judea empezaron a olvidar o ignorar la presencia de Jehova, (incluso desde el mismo Salomón) en su templo y fueron idolatrando cada vez más y de forma cada vez más descarada por ejemplo de Manases  se relata en 2 Reyes 21:3-5:
<pre>
3 Porque volvió a edificar los lugares altos que Ezequías su padre había 
derribado, y levantó altares a Baal, e hizo una imagen de Asera, como había 
hecho Acab rey de Israel; y adoró a todo el ejército de los cielos, y rindió 
culto a aquellas cosas.
4 Asimismo edificó altares en la casa de Jehová, de la cual Jehová había 
dicho: Yo pondré mi nombre en Jerusalén.
5 Y edificó altares para todo el ejército de los cielos en los dos atrios de 
la casa de Jehová.
6 Y pasó a su hijo por fuego, y se dio a observar los tiempos, y fue 
agorero, e instituyó encantadores y adivinos, multiplicando así el hacer lo 
malo ante los ojos de Jehová, para provocarlo a ira. 
</pre>

Por esto, este templo (y Judea e Israel) operó sólo hasta la invasión por parte de Babilonia, como lo habían predicho varios profetas --incluyendo Isaías y Jeremías--  por la desobediencia.


!3.4. Segundo templo de Jerusalén

Una vez el imperio de Babilonia fue vencido por el imperio Persa, el entonces rey de Persia Ciro permitió a los Judios que estaban en el exilio regresar y les devolvió los utensilios que habían sido tomados por los caldeos, incluso declaró que Dios le dio medidas para la reconstrucción, en Esdras 6:3-4

<pre>
3 En el año primero del rey Ciro, el mismo rey Ciro dio orden acerca de la
  casa de Dios, la cual estaba en Jerusalén, para que fuese la casa 
  reedificada como lugar para ofrecer sacrificios, y que sus paredes fuesen 
  firmes; su altura de sesenta codos, y de sesenta codos su anchura;
4 y tres hileras de piedras grandes, y una de madera nueva; y que el gasto 
  sea pagado por el tesoro del rey.
</pre>

Este templo se usaba para ofrecer holocaustos, celebrar fiestas solemnes y sacrificios espontáneos.  Esdras 3:4-5 

<pre> 
4 Celebraron asimismo la fiesta solemne de los tabernáculos, como está 
  escrito, y holocaustos cada día por orden conforme al rito, cada cosa
  en su día;
5 además de esto, el holocausto continuo, las nuevas lunas, y todas las}
  fiestas solemnes de Jehová, y todo sacrificio espontáneo, toda ofrenda
  voluntaria a Jehová.
</pre>


Este templo fue destruido hacia el año 60dC cuando el imperio romano apagaba una sublevación judía.


!3.5 Aposento Alto

Fue el nuevo espacio empleado por los apóstoles para celebrar, seguramente allí recibieron el Espíritu Santo como Jesús les había dicho y se relata en Hechos 1:8-14
<pre>
8 pero recibiréis poder, cuando haya venido sobre vosotros el Espíritu 
  Santo, y me seréis testigos en Jerusalén, en toda Judea, en Samaria, y
  hasta lo último de la tierra.
...
13 Y entrados, subieron al aposento alto, donde moraban Pedro y Jacobo,
   Juan, Andrés, Felipe, Tomás, Bartolomé, Mateo, Jacobo hijo de Alfeo,
   Simón el Zelote y Judas hermano de Jacobo.
14 Todos éstos perseveraban unánimes en oración y ruego, con las mujeres,
   y con María la madre de Jesús, y con sus hermanos.
</pre>

y Hechos 2:
<pre>
1 Cuando llegó el día de Pentecostés, estaban todos unánimes juntos.
2 Y de repente vino del cielo un estruendo como de un viento recio que 
  soplaba, el cual llenó toda la casa donde estaban sentados;
3 y se les aparecieron lenguas repartidas, como de fuego, asentándose sobre 
  cada uno de ellos.
4 Y fueron todos llenos del Espíritu Santo, y comenzaron a hablar en otras 
  lenguas, según el Espíritu les daba que hablasen.
</pre>


!3.6. Mi cuerpo como templo del Espíritu Santo

Los versículos introductorios explican como nuestro cuerpo debe ser templo en la actualidad del Espíritu Santo.

Podemos pensar que este nuevo templo fue construido en su inmensa mayoría por el Señor Jesús quien nos justificó ante el Padre, quien por su sangre nos hace aceptos ante Él en su plena santidad, en la santidad de su Espíritu.   De nuestra parte debemos ayudar en esa construcción solamente creyendo en el Señor Jesús como salvador.

Podemos visualizar como el Señor Jesús nos hizo aceptos ante el Santo con su sacrificio en la cruz, cuando leemos como se rompió el velo que dividía lugar santo de lugar santísimo, por ejemplo en Mateo 27:50-51

<pre>
50 Mas Jesús, habiendo otra vez clamado a gran voz, entregó el espíritu.
51 Y he aquí, el velo del templo se rasgó en dos, de arriba abajo; y la 
   tierra tembló, y las rocas se partieron; 
</pre>

Ya no hay división entre el lugar Santo y el  lugar Santísimo, ni atrio o muros que no permitan la entrada del pueblo, ahora todos somos linaje escogido real sacerdocio (1 Pedro 2:9) y ahora no sólo podemos acercarnos a Jehová sino que Él mismo viene con su Santo Espíritu y nos llena.
  
Y donde quedó el arca, regresemos brevemente, no se sabe de ella ni se menciona en la Biblia desde la destrucción del primer templo, hay diversas teorías de lo que ocurrió (ver {6}), por ejemplo Ron Wyatt sostuvo que la vio en un laberinto de cavernas que existe debajo de Jerusalén (ver {7}).   No tenemos la certeza, pero así como ellas guardaban las tablas de la ley, así nuestro corazón debe guardar la ley del Señor.

##4. CONCLUSIÓN


Llegamos al final de nuestro recorrido por los templos físicos sobre los cuales el Señor ha ido dando instrucciones para habitar en ellos.  Algunos puntos en común, para tener en cuenta en nuestras vidas para que estén llenas de Espíritu Santo y en el templo que estamos construyendo:

* Dios dio instrucciones, diseño y dirigió
* Eligió el lugar
* Siempre se construyeron con esfuerzo voluntarios, y en varios casos pagando constructores, en ningún caso obligando.
* Dios diseño construcción.
* Dios diseño decoración y utensilios.
* Eligió personas para los diversos servicios
* Diseñó fiestas y ofrendas.  
* En algunos casos hubo dificultades en construcción pero el Señor respaldo para que se completara la obra.


* Así como los templos recibían la visita y la gloria de Dios, que nuestro Cuerpo la reciba con su Santo Espíritu
* Así como el arca guardaba las tablas de la ley que nuestro corazón guarde la ley del Señor.

* Agradezcamos al omnipotente quien, aunque no pueden contenerlo los cielos de los cielos, ha hecho un plan maravilloso para habitar humildemente en corazones de carne de quienes humildemente y con amor lo quieran recibir.

* Alguien quiere recibir a Cristo en su corazón y ser justificado ante el Padre, confesando con la boca que Jesús es el Señor y Dios lo levanto de los muertos para nuestra salvación? (Rom 10:9).  Aquí hay una breve [Oración de fe] que puede hacer.


##5. BIBLIOGRAFÍA


* {1} Reina-Valera 1960 (RVR1960) http://www.biblegateway.com/passage/?search=%20mat%2027&version=RVR1960
* {2} Mariano Pierini. El Tabernaculo de Moises. http://www.peniel-mdq.com/tabernaculo1.htm
* {3} http://www.editoriallapaz.org/tabernaculo_desglose.htm
* {4} Construye tu propio modelo del tabernaculo. http://www.visionctd.org/wp-content/uploads/2011/06/anexo-del-tabernaculo.pdf
* {5} http://neoatierra.blogspot.com/2011/03/salomon-constructor-del-templo.html#axzz1pBm6egms
* {6} Dawlin A. Ureña. ¿Qué sucedió con el Arca del Pacto? http://antesdelfin.com/arcadelpacto.html
* {7} The Discovery of the Ark of the Covenant. http://www.wyattarchaeology.com/

----

Gracias a Dios esta prédica de dominio público fue dada en la Iglesia Menonita La Resurreción de San Nicolas el 25 de Marzo de 2012.  vtamara@pasosdeJesus.org
