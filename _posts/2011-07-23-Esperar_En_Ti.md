---
layout: post
title: "Esperar_En_Ti"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Jesús Adrian Romero

<pre>
Esperar en ti
Difícil se que es
Mi mente dice no
No es posible.

Pero mi corazón
Confiado está en ti
Tu siempre haz sido fiel
Me haz sostenido.

Y esperaré pacientemente
Aunque la duda me atormente
Yo no confío con la mente
Lo hago con el corazón.

Y esperaré en la tormenta
Aunque tardare tu respuesta
Yo confiaré en tu providencia
Tu siempre tienes el control.
</pre>


* Canción: http://www.youtube.com/watch?v=uuggue7Plt0
* Letra basada en: http://www.musica.com/letras.asp?letra=883983
