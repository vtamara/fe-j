---
layout: post
title: "Cuadernillos_Metalicos"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Como se reporta en {1} la luz pública ha conocido de unos 70 cuadernillos metálicos que parecen ser del siglo 1 y que por algunos símbolos parecen contener información sobre el cristianismo.    

[http://i.dailymail.co.uk/i/pix/2011/03/30/article-1371290-0B63F32D00000578-102_306x370.jpg]

Una traducción libre de un parrafo es:
"La perspectiva de que puedan contener relatos contemporaneos con los años finales de la vida de Jesús ha excitado a los escolares  -- aunque su entusiasmo es moderado por el hecho que los expertos han sido engañados previamente con sofisticados fraudes."

Dos observaciones frente a este párrafo:
* Para los cristianos nuestro Señor Jesucristo vive y en nosotros
* Con lo de "fraudes sofisticados" podría hacer otra falsa referencia al reciente y publicitado Osario de Santiago, que muchos medios han llamado fraude aunque judicialmente no se ha probado y por el contrario parece que se declarará su autenticidad. Ver [Osuario de Santiago]



##Referencias
* {1} http://www.dailymail.co.uk/sciencetech/article-1371290/70-metal-books-Jordan-cave-change-view-Biblical-history.html
