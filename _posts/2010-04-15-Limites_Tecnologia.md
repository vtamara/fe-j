---
layout: post
title: "Limites_Tecnologia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---

Kraus escribió un artículo el 10 de Mayo de 2004 sobre los límites universales de la computación  en http://arxiv.org/pdf/astro-ph/0404510 
Dijo que el ritmo de desarrollo tecnológico actual (Ley de Moore) se mantendría a lo sumo durante 600 años.


El mismo Moore de la ley de Moore dijo el 13 de Abril de 2010 que su ley estaba muerta:

http://news.techworld.com/operating-systems/3477/moores-law-is-dead-says-gordon-moore/
