---
layout: post
title: "Te_Amo_Mas_que_a_mi_misma_vida"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Rojo

<pre>
Si pudiera contar tus bondades,
los números se acabarían,
los libros no abastecerían.

Y si hubieran suficientes días,
para decir tus maravillas,
la eternidad no alcanzaría.

Y ahora el cielo es más azul,
cuando estoy junto a ti,
no me quiero separar,
te quiero en mi...

*
Te amo por completo,
Te amo Dios eterno
Te amo mas que a mi misma vida,
Te amo con pasión
te amo con fervor
Te amo mas que a mi misma vida.

No tengo otra razón para existir Señor
tú eres mi pasión, eres mi pasión
Mi corazón es tuyo,
mis sentimientos tuyos
tú eres mi pasión, eres mi pasión
*

Los inviernos ya no son tan frios
por que ahora estas con migo
ya encontré lo más querido
y aunqe no se que habrá mañana
mi corazón tiene la calma
pues eres toda mi esperanza

Y ahora el cielo es más azul,
cuando estoy junto a ti,
no me quiero separar,
te quiero en mi...

Te amo por completo,
Te amo Dios eterno
Te amo mas que a mi misma vida,
Te amo con pasión
te amo con fervor
Te amo mas que a mi misma vida.

*
Te amo por completo,
Te amo Dios eterno
Te amo mas que a mi misma vida,
Te amo con pasión
te amo con fervor
Te amo mas que a mi misma vida.

No, no tengo otra razón para existir Señor
tú eres mi pasión, eres mi pasión
Mi corazón es tuyo,
mis sentimientos tuyos
tú eres mi pasión, eres mi pasión
</pre>


* Canción: http://www.youtube.com/watch?v=lJfIiMUbVeQ
* Letra basada en: http://www.musica.com/letras.asp?letra=1573919
