---
layout: post
title: "Templo_En_Elefantina"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Se trata de un templo dedicado a Dios en la pequeña isla de Elefantina en el Nilo.

Por escritos que lo referencian, se cree que fue construido por una colonia militar judia antes del 525aC.  Allí mismo se han encontrado papiros en Hebreo.

http://www.classicalhebrewblog.com/2009/01/26/an-ancient-temple-to-yhwh-in-egypt/
