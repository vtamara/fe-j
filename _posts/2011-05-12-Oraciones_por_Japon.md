---
layout: post
title: "Oraciones_por_Japon"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Señor te pedimos por Japón; Señor cuando la ciencia, la tecnología, la organización, la previsión y en general todos los esfuerzos humanos fallan y resultan insuficientes, sigues estando Tú.  Gracias.

Señor ten misericordia de este pueblo que has amado, consuela a los afligidos, usanos para llevar palabras y bienes que sirvan con amor; y que veamos tu gloria para que anhelemos encontrarnos contigo cuando termine nuestro tiempo en este mundo.

Sitios donde donar:

* !BetterPlace http://www.betterplace.org/campaigns/erdbeben-japan
* Cruz Roja Alemania. http://www.drk.de/weltweit/asien-nahost/japan-hilfe-nach-erdbeben.html
