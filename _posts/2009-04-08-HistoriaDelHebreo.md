---
layout: post
title: "HistoriaDelHebreo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Las imagenes y datos siguientes son de {1}


* ketav ivri - proto cananeo.  ~2000aC tiempo de Abraham
[http://www.hebrew4christians.com/Grammar/Unit_One/History/proto-hebrew.gif]
* ketav Ashurit - sagrado ~1400aC tiempo de Moises ? Readoptado por Ezra. Corresponde al alfabeto clásico que hoy se usa.
[http://www.hebrew4christians.com/Grammar/Unit_One/History/classicscript.gif]

!EJEMPLOS

OSTRACAS: Pedazos de arcilla (e.g de jarras) con escritura sobre estas.

* Ostraca [Texto Hebreo Antiguo] 1000aC
* El-Kerak. 800aC. Moabita. Encontrada en 1958 en Jordan. {2}. [http://www.usc.edu/dept/LAS/wsrp/educational_site/ancient_texts/ElKerak_e.jpg]
* MRZH.  Descubierto hacia 1920 en Ugarit. Cuneiforme pero sólo 30 símbolos a diferencia del Acadio. {2}
* Amman Citadel.  Lenguaje de los Amonitas. 800aC. Descubierto en 1960 {2} [http://www.usc.edu/dept/LAS/wsrp/educational_site/ancient_texts/Citadel_e.jpg]
* Ostracas Amonitas en Heshbon del 700aC referenciadas en {2}
* Estela Incirli. Fenicio del 700aC. Descubierta en 1993. {3} [http://www.humnet.ucla.edu/humnet/nelc/stelasite/images/outlined.jpg]
* [Rollos de plata] 700aC. Bendición Sacerdotal. Descubiertos en 1979.
* Rollos del Mar Muerto. Entre 300aC y 100aC. Descubiertos entre 1947 y 1956. Tienen porciones de casi todos los libros del antiguo testamento. Algunos completos como Isaias.
* Codex de Leningrado. 1010dC. Biblia Judia en Hebreo completa.

!OTROS ALFABETOS ANTIGUOS

Recomendado {4}, según el cual a Mayo 27 de 2009 hay por lo menos 7 posibles alfabetos de la antiguedad aún no descifrados:
* Etrusco
* Meroitic
* Olmeca, Zapoteca, Istmian
* Lineal A
* Rongo-rongo
* Indu
* Proto-Elamita
* Disco de Faisto


Entre los ya descifrados resalta:
* Jeroglificos egipcios
* Cuneiforme babilónico
* Lineal B
* Maya


!REFERENCIAS

* {1} A Brief History of Hebrew Language. http://www.hebrew4christians.com/Grammar/Unit_One/History/history.html
* {2} Ancient Texts relating to the Bible. http://www.usc.edu/dept/LAS/wsrp/educational_site/ancient_texts/ostraca.shtml
* {3} The Incirli Stela. Elizabeth Carter. http://www.humnet.ucla.edu/humnet/nelc/stelasite/zuck.html
* {4} http://www.newscientist.com/article/mg20227106.000-decoding-antiquity-eight-scripts-that-still-cant-be-read.html?full=true&print=true
