---
layout: post
title: "Cancion_de_Sanidad"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Juan Luis Guerra

<pre>
Ven, tócame Señor 
quiero recibir 
Tu preciosa unción 

Ven, sáname Señor 
que un milagro hoy 
quiero yo de Tí 

//En el nombre de Jesús recibo sanidad 
he tocado el borde de Su manto 
sano estoy por su Espíritu Santo //

//Sano estoy por su Espíritu Santo //

Gracias, Señor
</pre>

----

* Canción: http://www.youtube.com/watch?v=tjjC1mnEG7o
* Letra basada en: http://www.musica.com/letras.asp?letra=1150290
