# Fuentes del sitio <https://fe.pasosdeJesus.org>

## Manejador de contenidos

No hay como tal porque se usa el generador de sitios Jekyll, pero nos ha 
resultado útil <https://forestry.io>

## Desarrollo local

Requiere:

- Ruby > 2.5
- Jekyll ~> 3.8

Para correr un servidor de desarrollo:

```sh
git clone git@gitlab.com:vtamara/fe-j.git
cd fe-j
bundle
bundle exec jekyll serve --livereload --drafts
```

## Despliegue

Desplegar sobre adJ con `make` y configurar nginx para que apunte a `_site`

## Recomendaciones

Las imágenes que sean de máximo 512 de ancho (la altura puede variar)

## Derechos de reproducción

Dominio público.

## Créditos

Utiliza el tema Mundana diseñado por [wowthemes](https://github.com/wowthemesnet/mundana-theme-jekyll)
La paloma que está como imagen principal por omisión es de dominio público
disponible en <https://openclipart.org/detail/320578/dove>


