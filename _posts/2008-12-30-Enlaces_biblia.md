---
layout: post
title: "Enlaces_biblia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
* TC: A Journal of Biblical Textual Criticism- http://rosetta.reltech.org/TC/vol13/vol13.html
* New Testament Gateway. http://ntgateway.com/

Transcripciones de manuscritos
* Instituto de la Universidad de Münster para la investigación textual del Nuevo Testamento http://nttranscripts.uni-muenster.de

Fotografías de manuscritos
* Centro para el estudio de manuscritos del nuevo testamento. http://www.csntm.org/
* Literal Translation of the Original Greek New Testament http://chrles.multiply.com/photos

Precisión
* Debate http://users.adam.com.au/bstett/

Wikis
* http://biblewiki.net/static/Bible_Wiki
* http://www.wikichristian.org/index.php?title=Main_Page
* http://en.jesus-wiki.org/wiki/Main_Page

Diccionarios Bíblicos
* http://www.adonai.es/005complementos/diccionario/
* http://www.wikicristiano.org/indice/
* http://www.ccel.org/ccel/easton/ebd2.html
