---
layout: post
title: Testimonio de Gianna Jessen Sep 2015
author: vtamara
categories:
- Traducción
image: "/assets/images/blog_graphics_jessen2.jpg"
tags: Traducción

---
Traducción de la transcripción del testimonio de Gianna Jessen al Comité Judicial de la Cámara del 11.Sep.2015

Fuente: [http://julieroys.com/gianna-jessen-asks-congress-if-abortion-is-about-womens-rights-then-what-were-mine/?utm_campaign=shareaholic&utm_medium=facebook&utm_source=socialnetwork](http://julieroys.com/gianna-jessen-asks-congress-if-abortion-is-about-womens-rights-then-what-were-mine/?utm_campaign=shareaholic&utm_medium=facebook&utm_source=socialnetwork)

***

Video del testimonio: [http://www.c-span.org/video/?c4550358/gianna-jessen](http://www.c-span.org/video/?c4550358/gianna-jessen)

Buenos días,

Mi nombre es Gianna Jessen, y me gustaría darle las gracias por la oportunidad de testificar hoy aquí. Mi madre biológica tenía siete meses y medio de embarazo cuando fue a Planned Parenthood, quienes le aconsejaron efectuar un aborto salino tardío.

Este método de aborto quema el bebé por dentro y por fuera, cegando y sofocando al bebé, quien entonces nace muerto, por lo general en menos de 24 horas.

![http://julieroys.com/wp-content/uploads/2015/09/Screen-Shot-2015-09-09-at-4.19.06-PM.png](http://julieroys.com/wp-content/uploads/2015/09/Screen-Shot-2015-09-09-at-4.19.06-PM.png)

En lugar de morir, después de 18 horas de haber sido quemada en el vientre de mi madre, nací viva en una clínica de abortos en Los Ángeles el 6 de Abril de 1977. Mis registros médicos establecen: "nacida viva durante un aborto salino" a las 6 am.

![http://www.bornalivetruth.org/images/Copy%20of%20GianaMedicalRecord.jpg](http://www.bornalivetruth.org/images/Copy%20of%20GianaMedicalRecord.jpg)

Afortunadamente, el abortista aún no estaba en el trabajo. Si hubiera estado allí, habría terminado mi vida con estrangulación, asfixia, o dejándome allí para morir. En cambio, una enfermera llamó una ambulancia, y fuí trasladada de urgencia a un hospital. Los médicos no esperaban que yo viviera. ¡Lo hice! Posteriormente fui diagnosticada con parálisis cerebral, causada por falta de oxígeno en el cerebro mientras sobrevivía al aborto. Se suponía que yo nunca sostendría mi cabeza ni caminaría. ¡Lo hago! Y la parálisis cerebral es un gran regalo para mí.

Me pusieron finalmente en hogares de acogida y posteriormente fui adoptada. Escuchen bien, perdono a mi madre biológica. En el primer año después de mi nacimiento, me utilizaron como testigo experto en un caso en el que un abortista había sido capturado estrangulando a un niño hasta la muerte después de que había nacido vivo.

Margaret Sanger, la fundadora de Planned Parenthood, dijo lo siguiente: "Lo más misericordioso que una familia grande hace a uno de sus miembros infantiles es matarlo." - Margaret Sanger, "Mujer y la Nueva Raza"

Planned Parenthood no se avergüenza de lo que ha hecho ni de lo que continúan haciendo. Pero vamos a tener que dar cuenta, como nación, ante Dios, por nuestra apatía y por la muerte de más de 50 millones de niños en el vientre materno. Cada vez que nos falta coraje como personas y fallamos en enfrentar este mal, me pregunto ¿cuántas vidas se han perdido en nuestro silencio, mientras nos aseguramos de ser alabados por los hombres sin ofender a ninguno? ¿Cuántos niños han muerto y han sido desmembrados y sus partes vendidas, por nuestro ego, nuestra conveniencia, y nuestra promiscuidad? ¿Cuántos Lamborghini fueron comprados con la sangre de niños inocentes? La sangre clama al Señor desde la tierra, como la sangre de Abel. Ninguno de ellos es olvidado por Él.

Quisiera hacer las siguientes preguntas a Planned Parenthood:

Si el aborto se relaciona con los derechos de las mujeres, entonces, ¿cuáles eran los míos? Ustedes usan de forma continua el argumento: "Si el bebé es discapacitado, tenemos que interrumpir el embarazo", como si pudieran determinar la calidad de la vida de alguien. ¿Es mi vida menos valiosa debido a mi parálisis cerebral?

Ustedes han fallado, con su arrogancia y avaricia, en ver algo: a menudo es de los más débiles de entre nosotros que aprendemos sabiduría - algo que brilla por su ausencia en nuestra nación hoy. Y son tanto nuestra necedad como nuestra vergüenza las que nos ciegan a la belleza de la adversidad. Planned Parenthood utiliza el engaño, la manipulación del lenguaje y lemas como "el derecho de la mujer a elegir", para lograr sus objetivos monetarios.

Voy a ilustrar lo bien que emplean esta técnica con la siguiente cita: "La receptividad de las masas es muy limitada, su inteligencia es pequeña, pero su poder de olvido es enorme. Como consecuencia de estos hechos, toda propaganda eficaz debe ser limitada a muy pocos puntos y debe insistir en estos con lemas hasta que el último miembro del público entienda lo que usted quiere que entendía con su lema. "- Adolf Hitler

A menudo escuchamos que si Planned Parenthood perdiera financiación, habría una crisis de salud entre las mujeres sin los servicios que prestan. Esto es absolutamente falso. Se encuentran Centros de recursos para el embarazo en todo el país como una opción para la mujer en crisis. Todos sus servicios son gratuitos y confidenciales, y se pueden conseguir enviando el mensaje de texto: "LÍNEA DE AYUDA" al 313131. Hay acceso a exámenes vitales para las mujeres fuera de Planned Parenthood. No somos una nación sin opciones.

Planned Parenthood recibe $500 millones de dólares de los contribuyentes al año, principalmente para destruir y desmembrar bebés. No me digas que no se trata de niños. Un latido del corazón lo demuestra. Lo mismo que una ecografía en 4D. Y así mismo yo, y también el hecho de que estén vendiendo órganos humanos con fines de lucro.

No me digas que esto es sólo un problema de la mujer. Se necesita tanto un hombre como una mujer para crear un niño. Y sobre este punto deseo hablar a los hombres que me escuchan: Fuiste creado para la grandeza, no para la pasividad. Naciste para defender a las mujeres y a los niños. No nos uses, ni nos abandones, ni te quedes sentado con los brazos cruzados mientras que sabes que estamos siendo perjudicados.Te estoy pidiendo que seas valiente.

En conclusión, permítanme decir que vivo solamente por el poder de Jesucristo. En quien yo vivo, me muevo y tengo mi ser. Sin Él no tendría nada; con Él, lo tengo todo.

---

Imagen de <https://julieroys.com/gianna-jessen/>