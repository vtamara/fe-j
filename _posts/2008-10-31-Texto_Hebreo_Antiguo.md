---
layout: post
title: "Texto_Hebreo_Antiguo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
[http://www.elahfortress.com/page18/page13/files/page13-1002-full.jpg]

Este texto fue encontrado en lo que se conoce como Fortaleza de Ela.  
Fue encontrado hacía Octubre de 2008 por voluntarios bajo la dirección de  Yosef Garfinkel.  Mediante carbono 14 se ha datado entre los años 1000aC y 950aC, se trata del texto hebreo más antiguo que se conoce en el momento{1}.     

Es un escrito proto-cananeo que no se ha logrado descifrar en su totalidad pero diversas palabras que se han identificado incluyen "juez," "esclavo" y "rey." {1} 

 
!El sitio del hallazgo

[http://www.elahfortress.com/page18/page14/files/page14-1007-full.jpg]

La fortaleza esta localizada al suroeste de Jerusale&#324; (a unos 12km {1}) en lo que era el borde entre el reino de Judea y los territorios costeros filisteos.  Filisteos que posiblemente llegaron de Creta, se ubicaron en la costa sur de Palestina casi al tiempo con los Israelitas hacía el siglo 12 aC. {3}

[http://imgs.sfgate.com/c/pictures/2008/11/16/ba-mideast1117_g_SFCG1226813897.jpg]

Aunque algunos investigadores sugieren que la Fortaleza de Ela no es
judia (por ejemplo el profesor Israel Finkelstein afirma que por su ubicación es filistea {2}), su carácter judio lo confirman:
* El hallazgo de un texto hebreo antiguo
* Una de sus 2 puertas está orientada hacía Jerusalén {4}
* No se han encontrado huesos de cerdo, como habría de esperarse de ciudades judías y a diferencia de ciudades filisteas  {3}
* La alfarería encontrada no es como la de ciudades filisteas de la misma época y si como la de ciudades judías {3} (pueden verse varias muestras en {5}):
[http://www.elahfortress.com/page18/page27/files/page27-1015-full.jpg]


!Implicaciones con respecto a la Biblia

El valle de Ela es el sitio donde la biblia ubica la batalla entre David y Goliat (1 Sam 17:2).

[http://www.bibleplaces.com/images/Elah_Valley_from_Azekah_tb_n022201.jpg]

La datación de la fortaleza (aprox. 1000aC) corresponde al tiempo en el que la cronología bíblica ubica los reinados de Saul y David.    

La tensión entre historiadores minimalistas y maximalistas aumenta por este hallazgo, pues algunos de los primeros han llegado a negar incluso la existencia del reino de David, sin embargo se calcula que esta fortaleza se construyo en unos 10 años, siendo judía, evidencia una alta organización política, económica y militar del reino de Judea {7}.  Esto sería típico de un reino, tal como lo describe la Biblia por parte de David.

Antes de este hallazgo el mismo profesor Finkesltein (actual profesor de arqueología U. Tel Aviv) publicó el libro "La Biblia Desenterrada" negando la historia de los patriarcas, el éxodo, la conquista de Canaan, los reinados de Saul y David y en general todos los relatos bíblicos antes del 800aC.  Por ejemplo un comentario de este libro {6} dice: "Las ciudades cananitas eran pequeñas y sin fortificar --Jericó y algunas otras ciudades mencionadas ni siquiera se habían establecido."

Por su parte el profesor Garfinkel (actual profesor de arqueología U. Hebrea de Jerusalén), aunque no asegura la literalidad de la Biblia ha dicho con respecto a los bloques de más de 10 toneladas con los que se construyó la puerta de la fortaleza: "son los más grandes encontrados para la Era de Hierro. Si el rey David alguna vez vino aquí desde Jerusalén, entró por esta puerta.  Es posible que estemos caminando sobre las pisadas del rey David." {7}, otros comentarios del mismo profesor son:
* "El descubrimiento de este texto hebreo temprano, nos dice por primera vez que la gente aquí podía leer y escribir en el tiempo del rey David, así que el conocimiento histórico pudo transmitirse por escrito y no sólo por tradición oral como algunos han sugerido"
* "las ciudades están todas donde la Biblia dice que estaban, y las fechas de nuestros descubrimientos muestra que fueron establecidas en el tiempo que la Biblia lo sugiere" {7} 

!Traducción

Hay una bitacora de los análisis que se le han hecho al Ostracon en {8}.

!Otros datos

* Sorprendentemente, los colombianos no requerimos visa para ingresar a Israel.
* Continúan planeandose excavaciones en este sitio (se calcula que se ha excavado menos del 5%), hay llamado abierto a participar voluntariamente y a donar http://www.elahfortress.com/index.htm

!Referencias

* {1} Arqueologos reportan hallazgo del texto hebreo más antiguo. Oct 30, 2008 3:06pm EDT. Ari Rabinovitch. http://www.reuters.com/article/scienceNews/idUSTRE49T52620081030?feedType=RSS&feedName=scienceNews&rpc=22&sp=true
* {2} http://www.elahfortress.com/page18/page18.html
* {3} http://news.nationalgeographic.com/news/2008/11/081103-hebrew-text.html  
* {4} http://www.jewishexponent.com/article/17636/
* {5} http://www.elahfortress.com/page18/page27/page27.html
* {6} http://www.theosophical.org.uk/Biblunsbd.htm
* {7} http://www.elahfortress.com/page26/page26.html
* {8} http://qeiyafa.huji.ac.il/ostracon.asp
