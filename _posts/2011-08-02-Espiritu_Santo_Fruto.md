---
layout: post
title: "Espiritu_Santo_Fruto"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción


¿Quién no quiere dar fruto? 

¿Qué es dar fruto?

Como queremos dar fruto abundante para nuestro Señor, necesitamos primero el carácter de Cristo en nuestras vidas.  Necesitamos el fruto del Espíritu en nosotros.

Un poco extraño, pero es la consigna para recordar: para dar fruto necesitamos el Fruto del Espíritu.


##2. Texto


Galatas 5:16- 6:4 Las obras de la carne y el fruto del Espíritu

<pre>
Digo, pues: Andad en el Espíritu, y no satisfagáis los deseos de la carne.
Porque el deseo de la carne es contra el Espíritu, y el del Espíritu es contra
la carne; y éstos se oponen entre sí, para que no hagáis lo que quisiereis.
Pero si sois guiados por el Espíritu, no estáis bajo la ley.
Y manifiestas son las obras de la carne, que son: adulterio, fornicación,
inmundicia, lascivia, idolatría, hechicerías, enemistades, pleitos, celos, 
iras, contiendas, disensiones, herejías,
envidias, homicidios, borracheras, orgías, y cosas semejantes a estas; 
acerca de las cuales os amonesto, como ya os lo he dicho antes, que 
los que practican tales cosas no heredarán el reino de Dios.
Mas el fruto del Espíritu es amor, gozo, paz, paciencia, benignidad, bondad, fe,
mansedumbre, templanza; contra tales cosas no hay ley.
Pero los que son de Cristo han crucificado la carne con sus pasiones y deseos.
Si vivimos por el Espíritu, andemos también por el Espíritu.
No nos hagamos vanagloriosos, irritándonos unos a otros, envidiándonos
unos a otros.

Hermanos, si alguno fuere sorprendido en alguna falta, vosotros que sois espirituales, restauradle con espíritu de mansedumbre, considerándote a ti mismo,
 no sea que tú también seas tentado.
Sobrellevad los unos las cargas de los otros, y cumplid así la ley de Cristo.
Porque el que se cree ser algo, no siendo nada, a sí mismo se engaña.
Así que, cada uno someta a prueba su propia obra, y entonces tendrá 
motivo de gloriarse sólo respecto de sí mismo, y no en otro;
</pre>

Aprendamonos entonces los componentes de ese fruto que queremos:
Amor, gozo, paz, paciencia, benignidad, bondad, fe, mansedumbre y templanza.

¿Queremos ese fruto multiforme para dar fruto?  Siiii

Entonces hagamos lo que dice Pablo: no satisfacer los deseos de la carne y no hacer las obras de la carne.


##3. Análisis

Bueno entonces basta esforzarnos y ser valientes y vamos a vencer y a no satisfacer los deseos de la carne y listo.... Aunque es necesario, no es tan fácil.

##3.1 Esfuerzo personal

Ciertamente debemos hacer lo que está en nuestras fuerzas para alejarnos de esos desos de la carne: : adulterio, fornicación, inmundicia, lascivia, idolatría, hechicerías, enemistades, pleitos, celos, iras, contiendas, disensiones, herejías,  envidias, homicidios, borracheras, orgías, y cosas semejantes a estas.

Entre otras, como dice {2}, tenemos que:
* crucificar (poner a muerte, hacer morir) la carne, Gál. 5:24; Rom. 6:6; Col. 3:5.
* negar o renunciar a los deseos mundanos, Tito 2:12.
* No proveer para los de?seos de la carne, Rom. 13:14.
* Huir de los deseos de la carne, 2 Tim. 2:22 (pasiones juveniles), 1 Cor. 6:18 (la fornicación), 1 Cor. 10:14 (la idolatría), 2 Ped. 1:4 (la corrupción a causa de la concupiscencia).

Sin embargo en vano trabajan los trabajadores si Dios no edifica la casa, más alla de nuestros esfuerzos humanos requerimos la gracia del Señor.  Retomemos el contexto del mensaje de Pablo:
 
##3.2 Gracia

Este pasaje está enmarcado en una discusión sobre la salvación por fe, pues justamente para quienes tenemos fe en Cristo no estamos bajo la ley de Moises, aunque si debemos cumplir el resumen de esos mandamientos: amar a Dios sobre todo y al prójimo como a mi mismo.

El contexto puede resumirse en Galatas 5:5-5:6
<pre>
Pues nosotros por el Espíritu aguardamos por fe la esperanza de la justicia;
porque en Cristo Jesús ni la circuncisión vale algo, ni la incircuncisión, sino la fe que obra por el amor.
</pre>

concordante con  Galatas 6.15-16
<pre>
Porque en Cristo Jesús ni la circuncisión vale nada, ni la incircuncisión, sino una nueva creación.  Y a todos los que anden conforme a esta regla, paz y misericordia sea a ellos, y al Israel de Dios.
</pre>

Tenemos entoneces como la siguiente identificación:

| Gracia |   opuesta a | Ley |
| Libertad | opuesta a | Esclavitud |
| Sara | opuesta a | Agar |

¿Entendemos entonces la magnitud de la gracia de nuestro Señor con nosotros?


Veamos Lucas 7:28  
<pre>
Os digo que entre los nacidos de mujeres no hay mayor profeta que Juan el Bautista, pero el más pequeño en el Reino de Dios es mayor que él.
</pre>

Esa gracia  nos ha hecho herederos del Reino, mayores que los más esforzados cumplidores de la ley mosaica.


##3.3 Guerra espiritual

También es posible que en nuestras vidas alla ataduras, que haya cadenas generacionales o que hayamos abierto puertas al enemigo.   Clamemos al Señor Jesucristo que es más Poderoso, que ya venció todo y al reconocerlo nos libera y sana.

Entonces cuando estemos cercanos a esas pruebas reprendamos y oremos continuamente.


##3.4 El amor

Debemos entonces luchar con un arma: amor como dice Efesios 5:2
<pre>
Y andad en amor, como también Cristo nos amó, y se entregó a sí mismo por nosotros, ofrenda y sacrificio a Dios en olor fragante.
</pre>

Completemos el círculo de lo pararójico, para obtener el fruto que da fruto, una de cuyas partes es el amor, debemos dar amor.

Además la fuente del amor es el Señor, pidamosle más amor hacía Él, que lo ponga en nuestros corazones y más amor hacía esas personas con quienes estamos fallando.

##4. Conclusión y Oración



##5. Bibliografía

* Wayne Partain.  ¿Qué hacer con la carne? Sermon. http://www.waynepartain.com/Sermones/s705.html705.html
