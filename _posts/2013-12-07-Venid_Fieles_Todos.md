---
layout: post
title: Venid Fieles Todos
author: vtamara
categories: 
image: assets/images/home.jpg
tags: 

---
Letra: , circa 1743 (Adeste Fideles)

Traducido al español por (1837-1916). 

Música: John F. Wade, 1743.

```
Venid, fieles todos, a Belén marchemos: De gozo triunfantes, henchidos de amor. Y al rey de los cielos contemplar podremos:

Coro

Venid, adoremos,
Venid, adoremos,
Venid, adoremos,
A Cristo el Señor.

El que es hijo eterno del eterno Padre,
Y Dios verdadero que al mundo creó,
Al seno humilde vino de una madre:

Coro

En pobre pesebre yace reclinado.
Al hombre ofrece eternal salvación,
El santo Mesías, Verbo humanado:

Coro

Cantad jubilosas, célicas criaturas:
Resuene el cielo con vuestra canción:
¡Al Dios bondadoso gloria en las alturas!

Coro
```


* Letra basada en: <http://cyberhymnal.org/non/es/venifito.htm>
* Himno: <http://www.youtube.com/watch?v=zvbLVOaqkV8>