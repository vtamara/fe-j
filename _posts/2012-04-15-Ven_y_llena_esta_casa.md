---
layout: post
title: Ven y llena esta casa
author: vtamara
categories: 
image: assets/images/home.jpg
tags: 

---
Vino nuevo

```
Al mirar a Tu santidad
Somos transformados a Tu imagen
Al mirar Tu majestad
De gloria en gloria cambianos Señor

CORO

//Ven y llena esta casa con tu gloria
Ven y llena esta  casa con tu gloria
La gloria postrera sera mayor que la primera            
Aumenta en mi tu gloria oh Jesús//

```

Referencias:

* Letra basada en la de: <http://www.musica.com/letras.asp?letra=1283213>
* Música: <http://www.youtube.com/watch?v=UcegWYMat9I>