---
layout: post
title: "Cantare_De_Tu_Amor"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
µµDanilo Monteroµµ

Por mucho tiempo busqué%%%
Una razón de vivir%%%
En medio de mil preguntas%%%
Tu amor me respondió%%%

Ahora veo la luz%%%
Y ya no tengo temor%%%
Tu reino vino a mi vida%%%
Y ahora vivo para ti%%%

__CORO__

__Cantaré de tu amor__%%%
__Rendiré mi corazón ante ti__%%%
__Tú serás mi pasión__%%%
__Y mis pasos se guiarán por tu voz__%%%

__Mi Jesús y mi Rey__%%%
__De tu gran amor cantaré__%%%


Por mucho tiempo busqué%%%
Una razón de vivir%%%
En medio de mil preguntas%%%
Tu amor me respondió%%%

Ahora veo la luz%%%
Y ya no tengo temor%%%
Tu reino vino a mi vida%%%
Y ahora vivo para ti%%%

//CORO//

Cantaré ....

//CORO//


//Mi Jesús y mi Rey%%%
De tu gran amor cantaré//

----

Letra: http://www.lyricsbay.com/cantare_de_tu_amor_lyrics-danilo_montero.html

Video de presentación de Danilo Montero: http://www.youtube.com/watch?v=ajTjMM-YBd0

Presentación con letra, música y voz de Danilo Montero: http://www.youtube.com/watch?v=YimzdtHjcBM&feature=related

Pista de la anterior: http://www.youtube.com/watch?v=ZqHSSZrTwzU

Midi: http://www.ccjesucristo.net/midis/midis.html
