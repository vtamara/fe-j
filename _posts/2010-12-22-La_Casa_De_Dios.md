---
layout: post
title: "La_Casa_De_Dios"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Danilo Montero

<pre>
Mejor es un día en la Casa de Dios
que mil años lejos de Él
prefiero un rincón en la casa de Dios
//que todo el palacio de un rey//

     CORO

Ven conmigo a la Casa de Dios
Celebraremos juntos su Amor
Y haremos fiesta en honor de Aquél 
que nos amó
Y estando aquí en la Casa de Dios
Alegraremos Su corazón
Le brindaremos ofrendas
de obediencia y amor

En la Casa de Dios

Arde mi alma, Arde de amor
por Aquél que me dio la vida
por eso le anhela mi corazón
//Anhela de su compañía//


CORO

////En la casa de Dios////

CORO

/////En la casa de Dios/////

</pre>


* Letra: http://www.letrasymas.com/letra.php?p=danilo-montero-en-la-casa-de-dios
* Video: http://www.youtube.com/watch?v=T0g4QRLPjFY
* Pista: http://www.youtube.com/watch?v=p4740FqOslg
