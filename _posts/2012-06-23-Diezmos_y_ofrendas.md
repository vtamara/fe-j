---
layout: post
title: "Diezmos_y_ofrendas"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
##1. Introducción

Las heridas en pies y manos de Jesús fueron por tí y por mí.  ¿Podemos retribuirlas? 

¿Podemos pagarle a Dios por habernos creado?

¿Podemos pagarle por la Creación que hizo para nosotros?

Sólo podemos expresar agradecimiento y nuestros diezmos y ofrendas son una forma más de hacerlo.


##2. Texto


2 Cor 9:6-15
<pre>
6   Pero esto digo: El que siembra escasamente, también segará escasamente; y el que 
     siembra generosamente, generosamente también segará.
7   Cada uno dé como propuso en su corazón: no con tristeza, ni por necesidad, porque Dios
     ama al dador alegre.
8   Y poderoso es Dios para hacer que abunde en vosotros toda gracia, a fin de que, teniendo
     siempre en todas las cosas todo lo suficiente, abundéis para toda buena obra;
9   como está escrito:
     Repartió, dio a los pobres;
     Su justicia permanece para siempre.
10 Y el que da semilla al que siembra, y pan al que come, proveerá y multiplicará vuestra
     sementera, y aumentará los frutos de vuestra justicia,
11 para que estéis enriquecidos en todo para toda liberalidad, la cual produce por medio 
     de nosotros acción de gracias a Dios.
12 Porque la ministración de este servicio no solamente suple lo que a los santos falta, 
     sino que también abunda en muchas acciones de gracias a Dios;
13 pues por la experiencia de esta ministración glorifican a Dios por la obediencia que 
     profesáis al evangelio de Cristo, y por la liberalidad de vuestra contribución para ellos 
     y para todos;
14 asimismo en la oración de ellos por vosotros, a quienes aman a causa de la 
     superabundante gracia de Dios en vosotros.
15 ##Gracias a Dios por su don inefable!
</pre>

##3. Contexto y Análisis

|Tiempo | Templo | Servidores | Sostenimiento |
|-1400aC | | Sacerdote Gen 14:18-20 | Ofrendas Espontaneas Gn 4:3-4.  |
|Moises 1400-1000aC | Tabernáculo construido con ofrendas Ex 35:4-9. Materiales, oro, decoración dirigida Ex 35:22-26 | Sacerdotes y Levitas.   Levitas cargaban y mantenian tabernáculo, Num 1:50. Sacerdotes oficiaban y lugar santo. Num 8:2 | Ofrenda para sacerdotes y diezmo para levitas. Num 18:19-21 |
|David y Reyes. 1000-600aC | 1er Templo. Construido con ofrendas voluntarias. 1 Cr 29:10-16 | Levitas como jueces,. Gobernadores, cuidado y alavanza. 1 Cr 23:4.  Sacerdotes 1 Cro 38:21. | Seguramente Ofrendas y diezmos continuaron. |
|Retornados de exilio 550aC-0| 2do Templo. Ofrenda del rey y voluntarias del pueblo. Esdras 1:5-6. | Sacerdotes y levitas. Nehemias 12:1. | Ofrendas. Esdras 3:5 |
|CRISTO. Dios con nosotros. Espíritu Santo |
| Iglesia Primitiva. 33dC- | Mi Cuerpo. 1 Cor 3:16 | Testigos de resurrección Hc 1:22. Palabra. Diakonos. Hc 6:2-3 | Cosas en común repartiendo según necesidades. Hc 2:44.  Ofrendas. 1 Cor 16:1-2 |
| Iglesia Menonita La Resurrección San Nicolas | Construcción. Materiales, dinero, trabajo. | Todos. Servidores (palabra, dirección, alabanza, administración). | Ministros viven por fe. 2 Cor 5:6. Diezmo y Ofrenda no son exigencia de ministros. 2 Cr 9:5. Dios retribuye a quien diezme u ofrende. 2 Cor 9:6-15.  Comunidad hace veeduría y habla primero con pastor si algo no está en orden. |


##4. Conclusión y Oración

Observación de {3} "5.8 Y una cosa más, darle al Señor es para adelantar la obra de Dios, es
para que su Reino pueda adelantarse, para que su evangelio, su bondad y
misericordia sean conocidas. No es para enriquecer a los pastores, no es para
que tengan una vida suntuosa, no es para que se vuelvan magnates de un negocio
que se llama iglesia. Si Ud. ve que eso está pasando, debe hablar con su pastor
para expresar su preocupación y escuchar su punto de vista; debe pedir un
informe financiero transparente de su congregación." y ¡Cuánto no habrá sufrido y orado Jesús por su pequeño rebaño! ¡Cuanto no habrá sufrido y orado Jesús por sus amigos! ¡Cuanto no habrá sufrido y orado Jesús por los que iban a creer a través del testimonio de éstos! ¡Y cuanto no habrá sufrido y orado Jesús por Ud y por mí, amig@!

* Gracias.  Señor enseñanos a ser agradecidos, recuerdanos ser agradecidos.
* Señor ayudanos a ser organizados con diezmos y ofrendas a reservarlas de lo mejor y llevarlas a tiempo, dandolas con generosidad, alegría, agradecimiento y esperanza (de {4})
* Gracias Señor Jesús por tu sacrifició por mí, por exponer en particular tus manos y pies por mí.   Si aún no la ha hecho, está invitado/a a hacer una [Oración de fe].


##5. BIBLIOGRAFIA

* {1} Biblia. Reina Valera. http://www.biblegateway.com/passage/?search=2%20cor%209:6-15&version=RVR1960
* {2}  http://www.saciatused.com/aguaviva/modules.php?name=News&file=article&sid=294
* {3} http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-12feb2012.shtml
* {4} http://www.iglesiamenonitadecolombia.org/congregaciones/teusaquillo/sermon-5feb2012.shtml

----
Gracias a Dios esta prédica de dominio público fue dada el 23 de Junio de 2012 en la Iglesia Menonita La Resurrección en San Nicolas, Soacha.
