---
layout: post
title: "Palabra_Vida"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
!Tema del mes: La palabra de Dios alumbra nuestro caminar diario

# 1. Introducción

Jesús es el perfecto ejemplo de consistencia entre palabra y vida, en una vida totalmente guiada o alumbrada por la palabra de Dios, hay muchos textos que lo revelan, pero nos concentraremos en uno Mt 4:1-14, cuando el Señor es tentado.

Hay hermosos testimonios de personas que practicamente toda su vida han buscado que la palabra de Dios alumbré su diario caminar deseo citar el ejemplo de Watchman Nee (ver {4}), misionero y predicador chino quien vivió de 1903 a 1972, desde los 17 años cuando recibió a Jesús se dedicó a compartir el evangelio entre sus compañeros de clase y en el pueblo donde nació, sufrió de tuberculosis de la cual el Señor lo sanó, pero quedó de por vida con una angina que podría haberle ocasionado la muerte en cada momento, por lo que aprendió a depender de la providencia continuamente y una fe viva en la Palabra. {4} Fue un estudioso consagrado de la Biblia y de literatura cristiana, se dedicó a fundar iglesias en China con el objetivo de que cada ciudad y pueblo tuviera un iglesia local manteniendo la Unidad del Cuerpo de Cristo --por lo que estuvo en desacuerdo con las denominaciones.  De 1923 a 1949 ayudó a fundar más de 700 iglesias locales con más de 70.000 fieles.  En la primavera de 1952 fue encarcelado por causa de su fe por parte del gobierno comunista chino, sólo podía recibir visitas esporádicas de su esposa, quien murió en 1971 y tras lo cual se quedó sin visitas y murió en la carcel en 1972, debajo de su almohada dejó este escrito: "Cristo es el Hijo de Dios, quien murió por la redención de los pecadores y resucitó después de tres días. Ésta es la verdad más grande que hay en el universo. Muero a causa de mi fe en Cristo".

Por otra parte con humildad, de mi vida recuerdo que desde que pude leer Biblia en la adolescencia, los evangelios, me ha impactado el amor que Jesús destila, nunca he sentido más amor en palabras.  Aún así no quise buscar de corazón al Señor entonces (aún cuando el Señor me llamó de varias formas), sino sólo hasta 2001 cuando tenía 28 años (y el Señor en su misericordia se me reveló en visión) entendí que tenía que leer Biblia por mi cuenta cada mañana y cada noche para aprender por medio del Espíritu Santo, ¡mejor hubiera sido comenzar antes! en todo caso  desde entonces he buscado que lo allí escrito alumbre mi caminar.  Creo que en unas épocas he avanzado mejor que en otras pero no me rindo.


# 2. Texto y Contexto

Mateo 4:1-11:
<pre>
1 Entonces Jesús fue llevado por el Espíritu al desierto, para ser tentado
   por el diablo.
2 Y después de haber ayunado cuarenta días y cuarenta noches, tuvo hambre.
3 Y vino a Él el tentador, y le dijo: Si eres Hijo de Dios, di que estas
  piedras se conviertan en pan.
4 El respondió y dijo: Escrito está: No sólo de pan vivirá el hombre, sino
  de toda palabra que sale de la boca de Dios.
5 Entonces el diablo le llevó a la santa ciudad, y le puso sobre el pináculo
  del templo,
6 y le dijo: Si eres Hijo de Dios, échate abajo; porque escrito está:
    A sus ángeles mandará acerca de ti, 
    En sus manos te sostendrán,
    Para que no tropieces con tu pie en piedra. 
7 Jesús le dijo: Escrito está también: No tentarás al Señor tu Dios.
8 Otra vez le llevó el diablo a un monte muy alto, y le mostró todos los 
  reinos del mundo y la gloria de ellos,
9 y le dijo: Todo esto te daré, si postrado me adorares.
10 Entonces Jesús le dijo: Vete, Satanás, porque escrito está: Al Señor tu
   Dios adorarás, y a él sólo servirás.
11 El diablo entonces le dejó; y he aquí vinieron ángeles y le servían.
</pre>

Este texto en el evangelio de Mateo se presenta a continuación del bautismo de Jesús (Mt 3:13-17) por parte de Juan Bautista (Mt 3:1-12) en el río Jordán y antes de comenzar a predicar en Galilea (Mt 4:12-17), de llamar los primeros discípulos (Mt 4:18-21) y de empezar a sanar (Mt 4:23-25).

También aparece en la misma ubicación en los evangelios de Marcos y Lucas.

Marcos 1:12-13
<pre>
12 Y luego el Espíritu le impulsó al desierto.
13 Y estuvo allí en el desierto cuarenta días, y era tentado por Satanás, 
   y estaba con las fieras; y los ángeles le servían.
</pre>
Lucas 4:1-13
<pre>
1 Jesús, lleno del Espíritu Santo, volvió del Jordán, y fue llevado por el
  Espíritu al desierto
2 por cuarenta días, y era tentado por el diablo. Y no comió nada en 
  aquellos días, pasados los cuales, tuvo hambre.
3 Entonces el diablo le dijo: Si eres Hijo de Dios, di a esta piedra que se
  convierta en pan.
4 Jesús, respondiéndole, dijo: Escrito está: No sólo de pan vivirá el hombre,
  sino de toda palabra de Dios.
5 Y le llevó el diablo a un alto monte, y le mostró en un momento todos los
  reinos de la tierra.
6 Y le dijo el diablo: A ti te daré toda esta potestad, y la gloria de ellos;
  porque a mí me ha sido entregada, y a quien quiero la doy.
7 Si tú postrado me adorares, todos serán tuyos.
8 Respondiendo Jesús, le dijo: Vete de mí, Satanás, porque escrito está: 
  al Señor tu Dios adorarás, y a él solo servirás.
9 Y le llevó a Jerusalén, y le puso sobre el pináculo del templo, y le dijo: 
  Si eres Hijo de Dios, échate de aquí abajo;
10 porque escrito está:
    A sus ángeles mandará acerca de ti, que te guarden;
11 y,
    En las manos te sostendrán,
    Para que no tropieces con tu pie en piedra.
12 Respondiendo Jesús, le dijo: Dicho está: No tentarás al Señor tu Dios.
13 Y cuando el diablo hubo acabado toda tentación, se apartó de él por un
   tiempo.
</pre>

Vemos que Jesús conocía pasajes de las escrituras de memoria, y la empleó como espada contra el diablo (como Pablo enseña en Efesios 6:17).  Jesús cita de memoria parte de Deuteronomio 8:3
<pre>
Y te afligió, y te hizo tener hambre, y te sustentó con maná, comida que
no conocías tú, ni tus padres la habían conocido, para hacerte saber que
no sólo de pan vivirá el hombre, mas de todo lo que sale de la boca de
Jehová vivirá el hombre.
</pre>
parte de Deuteronomio 6:16
<pre>
No tentaréis a Jehová vuestro Dios, como lo tentasteis en Masah.
</pre>
(que a su vez se refieré a Ex. 17:7 cuando los israelitas tuvieron sed y dudaron que Dios estaba entre ellos), y Deuteronomio 6:13
<pre>
A Jehová tu Dios temerás, y a él solo servirás, y por su nombre jurarás.
</pre>
o su traducción en Dios Habla Hoy:
<pre>
Adoren al Señor su Dios y sírvanle sólo a él, y cuando tengan que hacer un juramento, háganlo sólo en el nombre del Señor.
</pre> 

También vemos que el diablo conocía algo de Escritura de memoria, pues cita parte Salmo 91:11-12:
<pre>
11 Pues a sus ángeles mandará acerca de ti,
   Que te guarden en todos tus caminos.
12 En las manos te llevarán,
   Para que tu pie no tropiece en piedra.
</pre>

Geográficamente este pasaje se ubica (según {5} de donde tomamos el mapa) en el desierto de Judea (marcado con 13), Jesús habría llegado allí después de ser bautizado por Juan en el rio Jordán posiblemente en Betábaro (marcado con 12), se habría quedado 40 días y después posiblemente habría salido hacía Nazareth (marcado con 10) y Capernaum (marcado con 7) ambas en la región de Galilea:

[http://classic.scriptures.lds.org/es/biblemaps/map11.jpg]

Como se explica en {6} (de donde son las fotos siguientes), el desierto de Judea desde que la humanidad tiene memoria no ha sido habitado  por lo que ha sido lugar de retiro --por ejemplo en el periodo Bizantino  llegó a tener más de 60 monasterios-- y de refugió --por ejemplo David se escondió de Saul en lugares de este desierto como Zif (1 Sam 23:14), Maon (1 Sam 23:24), y En-Gadi (1 Sam 24:1).

[http://www.bibleplaces.com/images12/Judean-wilderness-west-of-Jericho,-tb092706254-bibleplaces.jpg]

[http://www.bibleplaces.com/images12/Camel-above-En-Gedi-with-Dead-Sea,-tb021603226-bibleplaces.jpg]

También los manuscritos del mar Muerto encontrados en Qumram en 1945 están en este desierto:
[http://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Qumr%C3%A1n.jpg/800px-Qumr%C3%A1n.jpg]

Entre esos manuscritos además de encontrarse las versiones más antiguas del Antiguo Testamento en Hebreo y en Arameo que la humanidad conoce en el momento (datadas hacia el 100aC) se describe lo que algunos han llamado la comunidad de los Esenios que habitó esos lugares entre aprox el 100aC hasta el 70dC (ver {7}) y que tenían una vida ascética que algunos han comparado con la de Juan el Bautista. Vida seguramente inspirada y guiada por las Sagradas Escrituras que conocían.

# 3. Análisis

##3.1 Encuesta

Responda Si o No a cada una de las siguientes 8 preguntas.  Cuente el número de respuestas Si que de

* ¿Tengo Biblia Personal?
* ¿Leo Biblia y oro cada mañana?
* ¿Leo Biblia y oro cada noche?
* ¿He leido la Biblia entera?
* ¿Se de memoria más de 20 versículos y tengo tiempo reservado para repasarlos?
* ¿Dedico al menos una hora diaria de oración y estudio de la Palabra de Dios y de literatura cristiana?
* ¿Estoy aprendiendo al menos un versículo nuevo semanalmente?
* ¿Estoy siendo amoroso con Dios y mi prójimo, para que el Espíritu Santo quiera vivir en mi y me ayude a recordar y aplicar la Palabra de Dios?


##3.2 Jesús es la palabra de Dios y nos ha dado Espíritu Santo para entenderla

Ayer hablaba con unos testigos de Jehova (ver http://fe.pasosdejesus.org/?id=Dialogo+Testigos+de+Jehova) en particular sobre 1 Juan 1:1 pués la biblia que usan es diferente a la nuestra para no reconocer que Jesús es Dios. Nuestra Biblia RV1960 dice:
<pre>
En el principio era el Verbo, y el Verbo era con Dios, y el Verbo era Dios.
</pre>
De la versión en griego se tradujo &#955;&#972;&#947;&#959;&#962; como "verbo", aunque otras traducciones por ejemplo la Traducción en lenguaje actual (ver {8}) se traduce como "palabra":
<pre>
Antes de que todo comenzara
ya existía aquel que es la Palabra.
La Palabra estaba con Dios,
y la Palabra era Dios.
</pre>
Oramos para que estas 2 personas encuentren la traducción correcta, la única que puede salvar y transformar.

En este pasaje es muy evidente la identificación de nuestro señor Jesucristo como "La Palabra de Dios", y es que su vida es la misma palabra de Dios hecha carne:

1. Comenzando por las profecias que hablaban de Él cientos de años antes, por ejemplo:
* El reinado eterno en descendiente de David 2 Sam 7:16
* Lugar de nacimiento Mt 2:6, Miqueas 5:2
* Que sería luz a los géntiles. Mt 4:15. Isaias 9:1-2
* Que entraría a Jerusalén cabalgando sobre un asno. Zacarias 9:9
* Que sería sacrificado para remisión de nuestros pecados. Isaias 53:5

2. Pasando por que hacía lo que estaba escrito en el antiguo testamento, pues :
* Era cumplidor de la ley, por ejemplo pagó el tributo al templo como estaba en Mat 17:24-27
* Impulsaba a otras personas a cumplirla, por ejemplo cuando ordenó al hombre que sanó de lepra a cumplir el rito de purificación en Marcos 1:44 "sino ve, muéstrate al sacerdote, y ofrece por tu purificación lo que Moisés mandó, para testimonio a ellos."

3. Pasando por que vivía los nuevos mandamientos con los que Él completaba  la Palabra de Dios, por ejemplo:
* Predicó vivir sin afan ni ansiedad, Mateo 6:25 dice "Por tanto os digo: No os afanéis por vuestra vida, qué habéis de comer o qué habéis de beber; ni por vuestro cuerpo, qué habéis de vestir. ¿No es la vida más que el alimento, y el cuerpo más que el vestido?"  y efectivamente vivía sin afán ni ansiedad, trabajando fuerte y confiando en la provisión de Dios como se ve en Mt 8:20  "Jesús le dijo: Las zorras tienen guaridas, y las aves del cielo nidos; mas el Hijo del Hombre no tiene dónde recostar su cabeza."

4. Y culminando con su muerte y resurrección para nuestra salvación y con el cumplimiento de su propia promesa de enviarnos Espíritu Santo
* Así declara Juan 14:16  ```Y yo rogaré al Padre, y os dará otro Consolador, para que esté con vosotros para siempre```
* El Espíritu Santo nos ayuda a enteder y recordar Biblia como dice Juan 14:26 "Mas el Consolador, el Espíritu Santo, a quien el Padre enviará en mi nombre, él os enseñará todas las cosas, y os recordará todo lo que yo os he dicho."


##3.2. Vencer tentación con la Palabra

Preparando esta predicación encontré en otra predicación {2} esta situación, llega un pastor a una casa donde la mamá tenía una gran Biblia, ante lo cual ella alardeaba que era "La palabra de Dios", su hijo entonces dijo, "Si es la palabra de Dios hay que devolversela porque aquí nunca usamos ese libro".  Además de tener Biblia en la casa es importante leerla, personal y familiarmente.

Hemos mencionado en esta prédica grupos como  los Esenios estudiosos de la palabra de Dios, que se esforzaron por entenderla y vivirla aunque no llegaron a conocer a Jesús.  Juan Bautista quien siendo hijo de sacerdote prefirió vivir en el desierto vestido con piel de camello para entender y cumplir la Palabra de Dios, no alcanzó a conocer en este mundo  la resurrección de Jesús,  mientras que nosotros podemos conocer todo esto completo.  

Jesús vence la tentación con la Palabra, vence la tentación de comer antes de tiempo, vence la tentación de tentar a Dios, vence la tentación de los bienes de este mundo al no postrarse ante Satanás ni ante las riquezas y recordar que a "Dios temerás y sólo a Él servirás."  Esforcémonos entonces por dedicar más tiempo a Dios, el trabajo es importante, pero el mismo Señor nos dice que no debemos afanarnos ni buscar primero las riquezas sino su Reino y Justicia.


!3.4 Confiar en la Palabra de Dios pero no acomodarla ni tentar a Dios

Creo que tenemos la fortuna de estar en una iglesia de sana doctrina, -- por cierto ¿Están examinando y probando lo que aquí se habla como manda el apóstol Pablo en 1 Tesalonicesense 5:21?  

Pero si creemos tener la fe correcta, ¿estamos realmente creyendo en la Palabra?  El Señor nos promete protección (por ejemplo en Salmo 91) y provisión (por ejemplo en Salmo 23), confiemos en esto y en cada promesa del Señor, pero claro está sin caer en tentar a Dios.

La semana pasada una vendedora ambulante a quien yo compraba hablaba con un señor, criticaba que su esposo (o ex-esposo más bien ?), se había vuelto cristiano y había dejado de trabajar, que se la pasaba leyendo la Biblia y que no salía de la casa cuando llegaba la hora de trabajar.   Uno podría pensar que el ese marido había tomado como muy literalmente el Salmo 23:1 "El Señor es mi pastor, nada me faltará" o los pasajes en los que Jesús nos dice que no debemos afanarnos.  Personalmente he vivido provisión milagrosa del Señor y creo que en el tiempo de Elías el Señor suplió milagrosamente con cuervos y posteriormente para que no se agotara el aceite y la harina de la tinaja, y que con Jesús la multiplicación de los panes fue literal.  Pero son situaciones especiales, y no puedo excusar la pereza en ellas para tentar al Señor, no puedo negar la provisión que el Señor envía como fruto del trabajo honesto y del esfuerzo, pues la Biblia también dice en Josue 1:9 "Mira que te mando que te esfuerces y que seas valiente, no temas ni desmayes porque Jehova tu Dios estará contigo donde quiera que vayas" y en 2 Tesalonicenses 3:10 "Porque también cuando estábamos con vosotros, os ordenábamos esto: Si alguno no quiere trabajar, tampoco coma".  (Por cierto gracias al Espíritu Santo que me recordó parte de ese pasaje para citarlo a la vendedora, pues yo sentía como maltratada la Biblia).

Algo similar puede ocurrir con la enfermedad, ciertamente el Señor da sanidad milagrosa, pero nosotros tras orar y pedir de requerirse debemos actuar y aplicar lo que Dios ha dado en la naturaleza y en técnicas para el cuidado de nuestros cuerpos.

Por cierto, OJO con acomodar la palabra de Dios, el diablo también sabe palabra de memoria pero abusa de ella como se ve en Mt 4:6, no hagamos eso, no saquemos la palabra de Dios de su contexto para nosotros tentar a Dios o hacer que otro tiente a Dios.

Juan Bautista conocía pasajes de la escritura de Memoria, pues declaraba sobre si mismo que era "la voz de uno que clama en el desierto, enderezad el camino del Señor", citando a Isaías según Juan 1:23.  Jesús sabía escrituras de memoria, como el pasaje que estudiamos evidencia.  ¿Y nosotros?

!3.5 Con Espíritu Santo comer palabra de Dios

Jesús confirmó "No sólo de pan vivirá el hombre, sino de toda palabra que sale de la boca de Dios."  La Palabra de Dios es el alimento más completo pues sustenta nuestro espíritu, nuestra alma e incluso nos ayuda con el sustento del cuerpo tanto con consejos (como la alimentación de Daniel) como físicamente como podemos evidenciar cuando ayunamos, y como se ve en nuestro texto en el ayuno de 40 días de Jesús. ¿Por cierto que tal ayunar los sábados en la mañana por nuestra iglesia?

Para que un alimento material sirva a nuestro cuerpo, debemos ponerlo en la boca, masticar y nuestro sistema digestivo opera para terminar de desmenuzarlo y lograr que los nutrientes entren hasta las celulas y nos alimenten y den energía.   En el caso de la Palabra de Dios debemos ponerla en nuestra mente, pasar tiempo entendiéndola, ojala memorizandola, desmenuzandola, escudriñandola para que el Espíritu Santo, por quien clamamos y agradecemos, haga el resto y la lleve a nuestro corazón y surta efecto trasnformandonos y dándonos fuerzas.
 


# 4. Conclusión y Oración

La Biblia tiene prioridades en sus mandamientos, por eso debemos memorizar tanto como podamos y conocerla entera, pero no para citarla a los demás como hacían escribas y fariseos Mt 5:20 (pienso que yo también he caído en eso en ocasiones) sino para vivirla, para anhelarla como el alimento que es, para poder decir de Jesús que queremos comérnoslo por lo amoroso que es, para que me moldee y para que alumbre cada paso en mi vida.

Oremos

* Señor que entendamos que necesitamos una relación personal contigo basada en dedicar tiempo a la oración, a la lectura diaria de Tú palabra y en esfuerzo continúo con Espíritu Santo para aplicar lo que leemos a nuestra vida diaria.
* Señor envía tu Santo Espíritu para que entendamos lo que leemos en tu Palabra, que recordemos orar pidiéndote Espíritu Santo continuamente y que en nuestro corazón haya amor hacía ti y hacia nuestro prójimo para que tu Santo Espíritu esté a gusto en nosotros.
* Señor por favor háblanos
* Señor gracias por el amor que me das, quiero amarte cada día más, tanto personalmente como a través de mi prójimo. Señor que yo pueda ver tu rostro en cada persona.
* Jesús tu eres la Palabra de Dios hecha carne, eres Dios hecho carne, torturado y sacrificado por mis pecados y resucitado para mi salvación, te agradezco y anhelo.   

Si alguién desea entregar su vida a Jesús lo invitamos a hacer una [Oración de fe]

## 5. Referencias

* {1} Biblia Reina Valera 1960.
* {2} http://www.sermoncentral.com/sermons/vivir-en-la-palabra-gabriel-cortes-sermon-on-bible-study-42886.asp?Page=2
* {3} http://spurgeongems.org/schs2577.pdf
* {4} http://en.wikipedia.org/wiki/Watchman_Nee
* {5} http://classic.scriptures.lds.org/es/biblemaps/11?sr=1
* {6} http://www.bibleplaces.com/judeanwilderness.htm
* {7} http://es.wikipedia.org/wiki/Esenio
* {8} http://www.biblegateway.com/passage/?search=Juan%201:1&version=TLA


------

Agradecemos al Señor Jesús por esta prédica que se compartió el 13.Oct.2013 en la Iglesia Menonita La Resurrección de San Nicolas, Soacha.
