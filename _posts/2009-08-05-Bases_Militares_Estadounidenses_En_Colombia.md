---
layout: post
title: "Bases_Militares_Estadounidenses_En_Colombia"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Ramstein es un pueblo en Alemania donde está la base de Estados Unidos más grande de Europa, gracias Dios el autor de este escrito hoy puede contar algo de lo que vivió de 1999 a 2003 en Kaiserslautern, un pueblo a unos pocos kilómetros de esa base (ver [mapa ](http://www.openstreetmap.org/)) y relacionarlo con las actuales políticas del gobierno colombiano.

!La base aérea de Ramstein

En Kaiserslautern, con frecuencia se veían jets y aviones de guerra que llegaban o salían de Ramstein, en el 2001 aumentó su nivel por la guerra en Afganistan, cuyas proviciones de guerra salian en buena parte de esa base.

Igualmente en el 2003 se hizo notoria la base por la guerra en Irak,
pues también era punto importante de provisión para esa guerra.

En Kaiserslautern era usual ver estadounidenses  y muchos más en
Ramstein, fueron especialmente notorios y algo pedantes durante los primeros meses de esas guerras.  En ocasiones también se veían algunos vendados que se recuperaban de lesiones de guerra.

Haciamos una oración por la paz con periodicidad cerca a la base de
Ramstein, participamos en varias manifestaciones en contra de la guerra y
de esa base.  Buena parte de la población estuvo en desacuerdo con
esas guerras y con esa base militar.  Aunque los esfuerzos han continuado, aún no ha sido posible mover esa base y ha continuado la ocupación ilegal en Afganistan e Irak por parte Estados Unidos. 

En estos dias Uribe Velez, sin respaldo del congreso ni la sociedad, anunció que permitirá el uso de al menos 7 bases militares colombianas por parte de
fuerzas de Estados Unidos {2}.

Como ciudadano colombiano, manifiesto que no estoy de acuerdo, ni con más militarización, ni con bases militares y menos con presencia de militares  de Estados Unidos en este país.  Oro a Dios que no lo permita, pero si lo permite que sea para Su Gloria.

!Algunos ejemplos de relaciones internacionales de Estados Unidos 

Hay excelentes ciudadanos de Estados Unidos por ejemplo los que trabajan por el cierre de la Escuela de las Américas (ver {10}), pero en general las políticas de los gobiernos de ese país en cuanto a su relación con el resto de paises y en particular los latinoamericanos han sido de dominación, guiadas por interes económicos y de poder típicamente en contravía de  la
vida y la autodeterminación. Entre otros casos (ver por ejemplo {1}) recordamos:
* Golpe de Estado en Guatemala en 1954 contra Jacobo Arbenz y apoyo a la posterior dictadura de más de 30 años, que dejó más de 200000 muertos. (ver {13})
* Apoyo el golpe de estado en Indonesia en 1965 a Sukamo por parte del dictador Suharto, una dictadura de más de 30 años que dejó entre 100000 y 1.5 millones de muertos (ver archivos desclasificados en {14}). 
* Acción encubierta en Chile de 1963 a 1973 aceptada por el mismo congreso de Estados Unidos (ver {4}). El contexto fue la operación Condor, que buscó  evitar la elección de Allende, apoyo su posterior derrocamiento por parte de Pinochet y la dictadura de este último. Al respecto del entonces secretario de estado Henry Kissinger:
** En 2002 fue requerido en interrogatorio por Baltasar Garzón sin colaboracin por parte de Gran Bretaa ni EEUU (ver {5}).
** En 2006 se le acusó de ser autor intelectual del asesinato del general Schneider durante las elecciones de Allende pero el Tribunal Supremo de Estados Unidos lo "excluyo" de juicio (ver {6})
** En 2007 fue requerido judicialmente en Uruguay como ideologo del plan Condor (ver {7})
* Apoyo a la dictadura de Jose Rafael Videla que en Argentina en 1976  derrocó a Isabel Martinez y dejó al menos 22000 muertos. Hay confirmaciones en documentos desclasificados del NSA (ver {12}).
* Irán-Contra en Nicaragua en 1986.  Cuando el gobierno de Reagan ayudó a financiar contrainsurgentes nicaraguenses (contras) ignorando prohibición por parte del mismo congreso de Estados Unidos.  Para la financiación vendió ilegalmente armas a Irán.  Ver análisis en {3}.

Sin olvidar que su relación con Colombia también ha sido triste:

* Posible intervención en asesinato de Gaitan en 1948. Hasta el momento se ha probado que era considerado como posible dictador por los 2 documentos que la CIA ha desclasificado. Sin embargo lo que más incrimina a las agencias de inteligenicia de Estados Unidos es su negativa persistente por desclasificar la información.  Desde 2001 el ciudadano norteamericano Paul Wolf ha solicitado judicial e insistentemente los registros al respecto a las agencias de inteligencia de EEUU.  En el 2005 el FBI dijo que había destruido tales documentos. La CIA se ha negando a entregar 11 documentos con pretextos aún con orden judicial de 2007 para que los entregue. Ver {8}
* Apoyo en instalación del paramilitarismo durante misión Yarbourough en 1962 (ver {14}).
* Protección o apoyo a empresas que han promovido políticas de muerte:
** United Fruit Compay: En 1929 el gobierno colombiano apoyó a esta empresa, disparando  contra trabajadores colombianos de esa empresa que protestaban por mejores condiciones laborales (ver en {9} este y otras consecuencias de la presencia de esta empresa en varios paises de Centro y Sur América)
** Coca Cola: Acusada de emplear paramilitares para asesinar sindicalistas colombianos en Carepa, Antioquia en 1998.  Se negaron a darme respuesta de fondo a pregunta que les hice al respecto (ver {11}).
** Chiquita Brands: En 2006 confesó haber financiado paramilitares en Colombia, el gobierno de Estados Unidos nego extradicción o juzgamiento en cortes colombianas.


!La política 'Nadie puede juzgarnos'

Estados Unidos no ha ratificado el estatuto de Roma, intentando
excluirse de responsabilidad ante la Corte Penal Internacional por las actuaciones de sus agentes en el extranjero.  De hecho logró un acuerdo desde el consejo de seguridad de Naciones Unidas para no ser juzgado por otros paises que si han ratificado el estatuto de Roma (ver {16}).

Refelexionando considero que la actitud pedante de esos soldados de Ramstein era respaldada por un lineamiento implícito de la política exterior de Estados Unidos:   'nadie puede juzgarnos' que también insinua 'estamos por encima de todos.'   Cuando Baltasar Garzón requirió en interrogatorio a Kissinger en 2002 al entrar a Gran Bretaña, la respuesta del gobierno estadounidense fue que Kissinger no estaba autorizado por ese gobierno para responder preguntas sobre su tiempo en la función pública, es decir que no permitía que nadie lo interrogara (No sería de extrañar que tal respuesta fuera en últimas planeada por el mismo Kissinger).  Otro sofisticado mecanismo de impunidad internacional, que en últimas anteponen a toda responsabilidad la fuerza de su poderío militar. 

Es justamente a estas políticas y a agentes que se consideran por encima del bien y del mal, que se consideran más que el resto de la humanidad avalados por su nación y que han trabajado limpia y suciamente para sentirse así, es justamente a ellos a quienes Uribe está dando la bienvenida a nuestro país.  

Esa es la consecuencia de la política de Seguridad Democrática, que en últimas se puede leer como derrotar militarmente sin importar más.  Cuando la única opción que puede verse es esa, cuando no se dan espacios para dialogar, la alternativa es asociarse con los más fuertes militarmente.  Una política de muerte, que ha convertido en asesinos inconcientes a muchos colombianos y que ahora llama a asesinos más inconcientes para darse continuidad.

!Agencias de inteligencia e inconciencia

Fuera de la impunidad e irresponsabilidad que caracteríza las fuerzas militares de Estados Unidos, sus servicios de inteligencia (particularmente CIA) y diversos políticos de alto rango han sido denunciados por realizar experimentos de control mental por lo menos por parte de 3 mujeres: Brice Taylor (ver {17}), Cathy O'Brien's (ver {22}) y K. Sullivan's (ver {23}). Sin embargo y aún con declaraciones pública de Ted Gunderson, ex-agente del FBI, que ratifican tales prácticas según dice en contextos de cultos satánicos (ver {18}) no han sido posibles investigaciones formales a las agencias ni los programas MKULTRA y Monarch (ver {19}). 

Sorprendentemente en un caso relacionado el juez Warren Urbom condenó el 27.Feb.1999 a Lawrence E. King (ex-adminsitrador de Franklin S&L) por numerosos crimenes contra Paul A. Bonacci "King continuamente subyugó al demandante (Bonacci) a asaltos sexuales repetidos, prisión falsa, inflinjió sufrimiento emocional, organizó y dirijió rituales satánicos, forzó al demandante a 'rebuscar' niños para ser parte de los abusos sexuales de King y del anillo de pornografía, forzó al demandante a tomar parte en numerosos contactos sexuales con el demandado King y otros y a participar en juegos sexuales desviados y orgias masoquistas con otros niños menores" (ver {20}), según !DeCamp el abogado de la víctima, 
"la historia contada por las víctimas de Monarch --una de las cuales fue Paul Bonacci-- es que fueron torturados con el propósito de crear  'multiples personalidades' en ellos... estas multiples personalidad podrían entronces ser programadas --como espias, 'mulas de droga,' prostitutas o asesinos."en que hay claramente dos elementos responsables trabajando: gobierno/militares y cultos satánicos (o más exactamente paganos) que cooperan."

Parece entonces que además de concentrarse en garantizar impunidad de sus agentes y empresas los gobiernos de Estados Unidos han buscado acallar la conciencia de sus ciudadanos. Posiblemente los grandes medios de comunicación y las novedosas campañas publicitarias, que también acuden a mensajes subliminales (ver {21}), buscan el mismo fin, al transmitir ciencia ficción, cosas "divertidas" o sexualmente placenteras e información suave que no vaya a despertar o incomdar la conciencia de los televidentes, para que así se queden quietitos en su silla, y puedan implementarse "tranquilamente" todas las estrategías de muerte que se requieran para alcanzar poder transitorio.

!Conclusiones

Tanto la Seguridad Democrática como las políticas de Estados Unidos buscan dominar por la fuerza y a toda costa, para aceptarlas debe cauterizarse la conciencia pues el costo en vidas humanas y dignidad es inocultable.  Para darse continuidad estas políticas han acudido a acciones e investigacioens cada vez más malevolas y denigrantes.  Hay evidencia explicita de satanismo y masoneria tras algunas de esas políticas e investigaciones, es lo que explica  tanta barbarie y maldad. Como Pablo escribió en Efesios 6:12 "Porque no tenemos lucha contra sangre y carne, sino contra principados, contra potestades, contra los gobernadores de las tinieblas de este siglo, contra huestes espirituales de maldad en las regiones celestes."  

* Preparemonos para la lucha espritual desde una fe firme en Jesucristo (ver Efesios 6:10-20) 
* No satanicemos todo, pero tampoco ignoremos la fuerza del mal. Puede que no lo entendamos, pero Jesucristo ya venció al mal y sólo necesitamos creer en Él para vivirlo.
* Organicemonos con amor para oponernos a que continúe la muerte violenta, la inconciencia y la degradación de la dignidad humana.
** Hay excelentes seres humanos estadounidenses, a quienes llamamos a ejercer presión en su gobierno para detener políticas internacionales de dominación e impunidad y las investigaciones con seres humanos de ese mismo país que han buscado someterlos y minarles su dignidad.
** Invitamos a los seres humanos conscientes a oponerse a la militarización en Colombia y en particular al uso de bases militares en Colombia por parte agentes de Estados Unidos.
* Mantengamonos en oración, alejados del pecado y en estudio de la palabra de Dios. Unamonos a otras personas que estén en el mismo caminar.
* No consumamos mala información, que nos distraiga del Creador y de lo que nos ha encomendado, consumir mala información es ceder conciencia y es dar campo para que se impongan los asesinos, no por su buen trabajo sino por nuestra falta de diligencia para proteger nuestra propia vida.  Más bien consumamos buena información: palabra de Dios (ver {24}).
* Oremos al Creador para que nos avive, para que nos despierte, para que nos limpie de vicios y de todo cuanto nos aleje de Él, para que nos guíe en discernir decisión correctas y nos ayude en su implementación.


Vladimir Támara Patiño. 

Un colombiano que quiere la paz de Dios.

!Referencias

{1} http://www.facebook.com/topic.php?uid=142096250494&topic=11558

{2} http://www.semana.com/noticias-seguridad/estados-unidos-utilizara-total-siete-bases-militares-colombia/127031.aspx

{3} Final Report of the Independent Counsel for Iran/Contra matters. Lawrence E. Walsh. http://www.fas.org/irp/offdocs/walsh/part_i.htm

{4} http://www.derechos.org/nizkor/chile/doc/encubierta.html#%20Acci%C3%B3n%20Encubierta%20en%20Chile

{5} http://news.bbc.co.uk/hi/spanish/latin_america/newsid_1944000/1944755.stm

{6} http://www.lanacion.cl/prontus_noticias/site/artic/20060417/pags/20060417204917.html

{7} http://www.lagaceta.com.ar/vernota.asp?id_seccion=10&id_nota=194539

{8} http://www.counterpunch.org/wolf04092008.html

{9} http://www.unitedfruit.org/chron.htm

{10} http://www.soaw.org/

{11} http://www.geocities.com/v-tamara/cartas/cocacola-19oct2003.txt

{12} http://www.rodolfowalsh.org/spip.php?article1830

{13} http://www.tribunademocratica.com/2009/07/una_democracia_decapitada_guatemala_1954.html

{14} http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB52/

{15} http://www.dhcolombia.info/spip.php?article168

{16} http://www.choike.org/2009/esp/informes/1146.html

{17} http://www.youtube.com/watch?v=_4MDCLH683o

{18} http://video.google.com/videoplay?docid=-1888632350775818856 

{19} http://ca.answers.yahoo.com/question/index?qid=20090621000705AAxpVeL

{20} http://www.raven1.net/ra1.htm

{21} http://www.geocities.com/v-tamara/cartas/disney/disney1.html

{22} http://v8.cache8.googlevideo.com/videoplayback?id=bb357d0af8727a45&itag=7&begin=0&ratebypass=yes&title=Cathy+O%27brien+presents:+TRANCE+Form+America+Pt.+2&ip=0.0.0.0&ipbits=0&expire=1250187388&sparams=ip,ipbits,expire,id,itag,ratebypass,title&signature=84E0EAFDBF645F9794EE47C119B0617FEDE693E6.1BDEDAABC8E763161DECBB130D8A3D4576B5D4F1&key=ck1&redirect_counter=1

{23} http://ritualabuse.us/2008/10/issue-21-july-1998/

{24} http://www.biblegateway.com/versions/index.php?action=getVersionInfo&vid=60
