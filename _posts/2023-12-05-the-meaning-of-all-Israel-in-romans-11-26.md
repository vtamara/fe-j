---
layout: post
categories: []
title: The meaning of "all Israel" in Romans 11:26
author: vtamara
image: "/assets/images/desplazamiento_y_apropiacion_de_tierra.jpg"

---

Romans 11:25-32 is:

<pre>
25 I do not want you to be ignorant of this mystery, brothers and
sisters, so that you may not be conceited: Israel has experienced 
a hardening in part until the full number of the Gentiles has 
come in, 26 and in this way all Israel will be saved. 
As it is written:

    The deliverer will come from Zion; 
    he will turn godlessness away from Jacob. 
    27 And this is my covenant with them 
    when I take away their sins.

28 As far as the gospel is concerned, they are enemies for your 
sake; but as far as election is concerned, they are loved on 
account of the patriarchs, 29 for God's gifts and his call are 
irrevocable. 30 Just as you who were at one time disobedient to 
God have now received mercy as a result of their disobedience, 
31 so they too have now become disobedient in order that they 
too may now receive mercy as a result of God’s mercy to you. 
32 For God has bound everyone over to disobedience so that he 
may have mercy on them all.
</pre>

This is the NIV translation,  that seems correct to me after comparing 
with other translations.

The NIV Study Bible, 1995. Has the following comment about verse 26 (that in
my humble opinion is correct and sufficient):

>> An empathic statement that this is the way all Israel will be saved.  
>> _all Israel_. Three main interpretations of this phrase are: 
>> (1) the total number of elect Jews of every generation (equivalent 
>> to the fullness of Israel [v. 12], which is analogous to the 
>> "fullness [´full number´] of the Gentiles" [v. 25]);
>> (2) the total number of the elect, both Jews and Gentiles, of 
>> every generation; (3) the great majority of Jews of the final 
>> generation. _will be saved_. The salvation of the Jews will, of course, 
>> be on the same basis as anyone's salvation: personal faith in 
>> Jesus Christ, crucified and risen from the dead.

Given that I'm not jewish, but a gentile christian, grafted in the olive 
of the church by the mercy of my Lord Jesus Christ, I speak with 
respect and thankfulness toward the genetic descendants of Jacob, some of 
them staying in the olive by their faith in Jesus although others have 
been taken away from the olive for their unbelief. For the same reason 
I speak cautiously because if I fall in unbelief I will be taken away 
from the olive as it has happened to so many before me.

Matthew Henry comment about this passage says:

>> If the putting out of their candle was the lighting of yours, by that 
>> power of God which brings good out of evil, much more shall the 
>> continued light of your candle, when God’s time shall come, be a means 
>> of lighting theirs again. _That through your mercy they might obtain 
>> mercy_, that is, that they may be beholden to you, as you have been 
>> to them. He takes it for granted that the believing Gentiles would 
>> do their utmost endeavour to work upon the Jews—that, when God had 
>> persuaded Japhet, Japhet would be labouring to persuade Shem. True 
>> grace hates monopolies. Those that have found mercy themselves 
>> should endeavour that through their mercy others also may obtain mercy.

This passage doesn´t have a special comment in the book God's Heart and Mandate
for Mission of Bruce Heckman, however in my humble opinion its comment about
Genesis 12 (pp. 54 y 55) is relevant:

>> This promise was so powerful because it was clearly for Abram, his
>> descendants, and all peoples on earth. This blessing with a purpose, 
>> given to Abram, promised God’s blessing to the world. Yet the sentence, 
>> "I will bless those who bless you and whoever curses you I will curse,"
>> raises questions in our minds. What does it mean to "bless Abram?" 
>> Today, some use this phrase to garner political support for the nation 
>> of Israel without scrutinizing any of Israel's current actions. 
>> Others use this phrase to raise ﬁnances to build houses in the 
>> West Bank for returning Jews, which displace Palestinians. 
>> In this context, does blessing Abram simply mean showing kindness 
>> and hospitality to him, his herds, and his servants? 
>> Could blessing Abram imply joining with the faith of Abram?
>>
>> People within Abram’s household did not become a "blessing to all
>> peoples on earth" by simply belonging to the family. God's blessing 
>> did not describe a club with bloodline membership, but a 
>> promise/covenant between two parties. This meant that those within 
>> Abram's household were to actively seek to represent the
>> God of covenant and thus become a source of blessing to the nations. 
>> They could not treat the na ons as inferior to themselves. 
>> In a similar thought, how did one outside of Abram's household bless 
>> Abram? Do we not err in our thinking when we invite those outside 
>> Abram's household to help Abram to prosper, yet allow
>> them to die without entering God’s covenant promise? 
>> Is it not also folly to help the children of Abram simply a 
>> new home and not share with them the gospel, the true covenant?

Since Jesus is the only way of salvation for anyone, the word **_all_** in
Romans 11:26 has to speak about **_all the israelites who believe in Jesus_**,
it cannot be confused with the inhabitants of the state of Israel.   From this
and the comments quoted it becomes clear how important it is for gentile
christians, like you and me, to make special effort to share Christ with the
genetic descendants of Jacob, who are currently living in the state of Israel
but without Jesus and hence without salvation.

I pray for conversion of more descendants of Jacob so they will be more 
merciful and cease the fire on Palestine population. I pray in Jesus name[^1]. 

I think that verses like this and the interpretation I found support the
"replacement theology" as explained in the article by Dan Bruce
[https://goodnewsforjews.org/2023/09/3560/](https://goodnewsforjews.org/2023/09/3560/):


>> Supersessionism, also called replacement theology, is a Christian theology
>> which describes the theological conviction that the Christian Church has
>> superseded the nation of Israel assuming their role as God’s covenanted 
>> people, thus asserting that the New Covenant through Jesus Christ has 
>> superseded or replaced the Mosaic covenant exclusive to Jews. 
>> Supersessionist theology also holds that the universal Christian Church 
>> has succeeded ancient Israel as God’s true Israel and that Christians 
>> have succeeded the ancient Israelites as the people of God. _from Wikipedia_
>>
>> I know it isn't popular to say out loud, but anyone who believes the words
>> of Jesus as recorded in the New Testament will essentially agree with 
>> the above definition. Jesus himself said it, "_I am the way and the 
>> truth and the life. No one comes to the Father except through me_."
>> ([John 14:6](https://www.blueletterbible.org/search/preSearch.cfm?Criteria=John+14.6&t=HNV)).
>> And, as Jesus passed the cup of wine to his disciples (all of them Jews) 
>> in the upper room on the first night of the Feast of Unleavened 
>> Bread (also called the Passover) just before his crucifixion, Jesus 
>> said, “_For this is my blood of the new testament (covenant), which is 
>> shed for many for the remission of sins_.” 
>> ([Matt. 26:28](https://www.blueletterbible.org/search/preSearch.cfm?Criteria=Matt.+26.28&t=HNV))
>> In this day and age, the New Covenant 
>> ([Jer. >> 31:31-34](https://www.blueletterbible.org/search/preSearch.cfm?Criteria=Jer.+31.31-34&t=HNV))
>> is the only way to have one’s sins removed and to receive atonement
>> (at-one-ment) with the God of Israel.


<!-- Footnotes themselves at the bottom. -->
## Notes

[^1]:
     I found that a succesful organization that is bringing the gospel to jewish
people is Jews fo Jesus. They have a website with possibility to donate:[
https://jewsforjesus.org/give?reference_code=STNETIN23](https://jewsforjesus.org/give?reference_code=STNETIN23)


Map taken from 
<https://twitter.com/AlanRMacLeod/status/1730344621908107465?t=tz3bYO7vIMf__WhmaAP8Zw&s=19> 
that presents the forced displacement and the illegal appropiation of land by Israel
after 1947 when an agreement was reached.
