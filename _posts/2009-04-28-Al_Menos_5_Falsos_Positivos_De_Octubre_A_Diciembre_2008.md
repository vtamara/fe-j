---
layout: post
title: "Al_Menos_5_Falsos_Positivos_De_Octubre_A_Diciembre_2008"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Aunque la información de falsos positivos y en general de ejecuciones extrajudiciales dificilmente se conoce por el temor de las víctimas a denunciar, una sencilla lectura de {1} menciona generalidades de 5 víctimas de este tipo de crimen entre el 1.Oct.2008 y el 31.Dic.2008 y da detalle de 3 de estas:

* El 8.Oct en TOLIMA, COELLO,
* El 13.Oct en ARAUCA,  FORTUL
* Campesino Marco Tulio, el 16.Oct en CASANARE,  TAMARA. 
* Lider Jeferson, el 25.Oct en PUTUMAYO, PUERTO LEGUIZAMO
* Joven Arnobis, el 26.Dic en CÓRDOBA, MONTERIA

Todos por parte del Ejercito Nacional.   El mismo informe anuncia que la información completa está por aparecer en la revista Noche y Niebla del segundo semestre de 2008.

Contrasta esta información con las declaraciones del actual ministro de Defensa Juan Manuel Santos quien según publicación del 21.Abr {2} dijo `desde octubre del año pasado no se ha presentado ni un solo caso' 

Al respecto del informe según {3} `el Ministro ...manifestó que una vez analizado el informe el Ministerio constató que sólo se había registrado otro caso.'

Como Octubre comienza el 1 no el 30, efectivamente se reportaron 5 casos entre Octubre y Diciembre del año pasado.  Contando Octubre comenzando el 30 habría un caso, el de Arnobis el 26.Dic, al respecto según {4} el ministro ha dicho: "Efectivamente, se presentaron denuncias después del 30 de octubre, solamente hay un caso el 26 de diciembre y en la investigación preliminar se nos dice que no se trata de un ?falso positivo,, sino de un individuo que era jefe local de las ?Águila Negras,, quien fue dado de baja en combate con el Ejército".

Se trata de uno de los casos detallados en {1} así:

| Diciembre 26/2008 CORDOBA - MONTERIA |
| Tropas del Batallón de Infantería 31 Rifles de la Brigada 11 del Ejército Nacional, ejecutaron a una  persona, a quien posteriormente presentaron como integrante de una banda criminal muerta en combate. Arnobis, quien tenía 18 años de edad y estudiaba séptimo grado de bachillerato en la jornada nocturna de la institución educativa Cristóbal Colón y quien se dedicaba a oficios varios, había desaparecido el día 25 de diciembre cuando se encontraba en su vivienda, ubicada en el barrio Edmundo López, al sur de Montería, "él estaba regando la calle con agua cuando otro muchacho llegó y conversó con él. Entonces dejó de regar, se cambió y se fue. No volvimos a saber de él hasta (...) cuando vimos su foto (...)". Agrega la fuente que: "La muerte de Negrete Villadiego se produjo un día después de su desaparición, es decir, el (...) 26 de diciembre en el corregimiento de Villa Fátima, en jurisdicción del municipio de Buenavista. Según el reporte del Ejército el joven era un presunto miembro de la banda criminal de "Don Mario".|

Es contradictorio que un lider de las Aguilas Negras pueda tener 18 años y estudiar en 7 en un colegio nocturno.   Con la forma de contar del Ministro (con Octubre comenzando el 30 para favorecer una campaña política) resulta más confiable la información de {1}.


##Referencias

* {1} Falsos Positivos. Balance del Segundo Semestre de 2008. Abr.2009. CINEP.  http://www.cinep.org.co/sites/cinep.cinep.org.co/files/Informe%20falsos%20positivos%202008-II%20-%20Abril%202009%20_FINAL_.pdf
* {2} Cifras sobre ?falsos positivos, han sido infladas, según Mindefensa. Caracol Radio. 21.Abr.2009. http://www.caracoltv.com/noticias/politica/articulo136288-cifras-sobre-falsos-positivos-han-sido-infladas-segun-mindefensa
* {3} Falsos positivos enfrentan a MinDefensa y Senador Galán. 21.Abr.2009. Terra Colombia. http://www.terra.com.co/actualidad/articulo/html/acu20863-falsos-positivos-enfrentan-a-mindefensa-y-senador-galan.htm
* {4} Denuncian seis nuevos casos de "falsos positivos". El nuevo dia. 22.Abr.2009. http://www.elnuevodia.com.co/nuevodia/inicio/archivo/7622-denuncian-seis-nuevos-casos-de-qfalsos-positivosq.html
