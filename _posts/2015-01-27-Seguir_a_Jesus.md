---
layout: post
title: Seguir a Jesús
author: vtamara
categories:
- Prédica
image: "/assets/images/98-01.jpg"
tags: Prédica

---
## 1. INTRODUCCIÓN

La pastora Adaia de la Iglesia La Resurrección de San Nicolas me ha pedido compartir testimonio sobre lo que en mi vida significa "Seguir a Jesús." Por el texto de Lucas 9:21-27 yo lo relaciono con cargar la cruz. Como prediqué el 20 de Abril de 2014 en San Nicolas interpreto cargar la cruz como asumir el propósito que Dios tiene para mí con todo el peso de sus consecuencias.

Para mi entonces seguir Jesús es avanzar en el próposito que Dios tiene para mi, teniendo a Jesús como guía, Él va delante de mi y de cada uno, alumbrando el camino, mostrando que hacer y como hacerlo.

## 2. TEXTO

57 Yendo ellos, uno le dijo en el camino: Señor, te seguiré adondequiera que vayas.

58 Y le dijo Jesús: Las zorras tienen guaridas, y las aves de los cielos nidos; mas el Hijo del Hombre no tiene dónde recostar la cabeza.

59 Y dijo a otro: Sígueme. Él le dijo: Señor, déjame que primero vaya y entierre a mi padre.

60 Jesús le dijo: Deja que los muertos entierren a sus muertos; y tú ve, y anuncia el reino de Dios.

61 Entonces también dijo otro: Te seguiré, Señor; pero déjame que me despida primero de los que están en mi casa.

62 Y Jesús le dijo: Ninguno que poniendo su mano en el arado mira hacia atrás, es apto para el reino de Dios.

## 3. CONTEXTO

Hay mucha información práctica para el seguimiento en el capítulo 9 en el cual este este texto:

| --- | --- |
| Versículos | Tema |
| 1-8 | Envia a discipulos |
| 7-9 | Muerte de Juan el Bautista |
| 10-17 | Alimentación a 5000 |
| 18-20 | Confesión de Pedro de que Jesús es el Mesias |
| 21-27 | Anuncia su muerte y explica como seguirlo |
| 28-36 | Transfiguración confirmando que Él es el Mesias |
| 37-43 | Sana a un endemoniado con ayuno y oración |
| 44-50 | Quien no está en contra está a favor |
| 51- 56 | No vino a destruir y condenar sino a salvar |
| 57 - 62 | Casos de seguimiento |

## 4. ANÁLISIS

El texto principal que hemos escogido menciona 3 casos:

1\. Jesús advierte que Él no tiene casa donde vivir y por tanto quienes lo seguían tampoco tendrían. ¿Entendemos que la misión de predicar es más importante que los bienes materiales? En mi caso sólo renuncié a la herencia porque quería más equidad y le creí al Señor que Él proveería y Él así lo ha hecho y en su misericordia me ha proveido buenos empleos para sostenter a la familia.

2\. Jesús invita a otro a seguirle, pero él pide primero ir a enterrar a su papá. Yo me pregunto ¿por qué no lo había enterrado? En que circunstancia estaba cuando Jesús lo llamo ---posiblemente si fuese en el velorio de su papá y Jesús hubiera ido lo estaría consolando o resucitando a su papá. Que el llamamemiento de Jesús no nos coja fuera de base, ni haciendo lo que no se debe, o dejando de hacer cosas necesarias. Más bien que seamos como Isaías y apenas Dios pregunte ¿Quién irá? respondamos Heme aquí de inmediato. Rut por su parte interpreta que si el papá seguía vivo el hombre lo que está pidiendo es tiempo antes de seguirlo, tiempo hasta que su papá se muera, y que eso ocurre con algunas personas que se sienten demasiado jóvenes para aceptar el seguimiento a Jesús y quieren seguir "libres" más tiempo.

3\. Jesús invita a otro más, quien pide despedirse de su familia primero. Recuerdo a Pedro quien tenía familia, se menciona en los evangelios su suegra, así que Pedro tenía una esposa y eventualmente hijos o hijas, y fue un gran seguidor de Jesús, pero por el relato bíblico no dejó a su familia. Así que no es necesario despedirse de la familia, es necesario invitar a la familia al seguimiento del Señor. Un ejemplo que conocemos de una familia misionera es la familia de Gerald y Maria Stucky, fundadores de nuestra iglesia Menonita aquí en Colombia, que tuvieron hijos aquí y en su tierra, algunos de sus hijos se quedaron alla en Estados Unidos, pero dos de sus hijos aquí, y ahora ellos (Pedro y Pablo junto con sus esposas) siguen haciendo avanzar la obra del Señor en este país, don Gerald entendió que el llamamineto que le hizo el Señor no fue sólo para Él sino para su familia y supo invitar a su esposa y sus hijos.

### 4.1 CARACTERÍSTICAS DEL SEGUIMIENTO: QUE

* El versículo 60 repite junto con el 2 y el 6: anunciar el evangelio. El propósito que Dios tiene en cada uno debe estar intimamente relacionado con anunciar el evangelio. En mi caso el Señor me ha llamado a misión y eso pongo en práctica y aprendo por ejemplo aquí en San Nicolas y oramos para seguir en Suba.
* El versículo 1 dice: expulsar demonios. De mi testimonio deseo contarles que una persona buscaba mostrar que yo tenía un problema mental, tuve que ir a medicina legal y una psicologa -- a la que le dije que yo había oido a Dios-- dijo que si que yo estaba loco. Fueron dos años intentando cambiar ese concepto, y sólo con guerra espiritual finalmente otro equipo de Medicina Legal dijo que no, que yo no estaba loco. Bueno yo si estoy loco por Cristo, pero gracias a Él mis capacidades mentales han estado completas y sanas.
* El versículo 1 también dice: sanar enfermedades. De mi vida deseo recordar que en varias oportunidades he orados por niños o niñas que el Señor me ha hecho ministrar y han sanado, también a adultos gracias a Él. La obra la hace Él, uno sólo debe orar.

### 4.2 CARACTERÍSTICAS DEL SEGUIMIENTO: COMO

* El versículo 2 junto con el 58 nos reitera es más importante el Reino y su Justicia, lo material vendrá por añadidura, seamos felices con lo que el Señor de, bien sea poco o sea mucho y cuando sea mucho compartamos en lugar de atesorar.
* El versículo 23 nos recuerda que para seguirlo debemos negarnos a nosotros mismos, no satisfacer lo que el mundo nos exige, lo que otros digan, sino lo que Dios mande.
* El mismo versículo nos recuerda que debemos cargar nuestras cruz, es decir asumir el propósito personal que Dios me ha dado.
* El versículo 26 nos recuerda no avergonzarnos de Cristo, ni de predicar el evangelio.
* Serle fiel y obediente a Dios es más importante que la vida, como Juan el bautista lo demostró y como el mismo Jesús lo demostraron --así como sus seguidores incluyendo practicamente a todos sus discípulos.
* Orar y ayunar
* Discernir cuando Dios obra y darle gracias.

## 5. ORACIÓN

* Señor que yo cumpla el propósito que tienes para mi, siguiendo tu ejemplo Jesús. Que seas mi camino y luz.


* Señor que prefiramos seguirte venciendo la ambición y todo idolo, que entendamos y vivamos que buscar primero el Reino y su justicia traerá los bienes necesarios por añadidura.


* Señor que yo no ponga pretextos para seguirte, que yo haga lo que debo hacer ahora con diligencia para estar disponible para Tí en todo momento. Que Tú en mi vida siempres estés en el primer lugar.


* Señor que entendamos tu llamamiento y por favor guíanos en invitar a nuestras familias en ese seguimiento, para que el ministerio que nos das de fruto también entre nuestros hij@s y familiares.


* Señor quiero serte fiel hasta la muerte, capacitame para lograrlo.


* Señor que podamos discernir cuando obras y te agradezcamos en lugar de asustarnos o alejarnos o ignorarte.


* Señor que no nos avergoncemos de Tí, ni de recibirte, pues eres salvación para quienes te reciben. ¿Alguien desea hoy recibir al Señor Jesús en su corazón con una [Oracion de fe](https://web.archive.org/web/20160117175413/http://fe.pasosdejesus.org/?id=Oracion+de+fe)?

***

 Gracias a Dios esta prédica pudo compartirse el 25 de Enero de 2015 en la Iglesia La Resurrección de San Nicolás, Soacha.