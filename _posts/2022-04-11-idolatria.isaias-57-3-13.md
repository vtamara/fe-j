---
layout: post
categories:
- Prédica
title: Idolatría. Isaías 57:3-13
author: vtamara
image: "/assets/images/283301.png"

---

# Idolatría. Isaías 57:3-13
## Comunidad Menonita de Suba. Caminos de Esperanza
[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 10.Abr.2022

## 1. Introducción

En estos días Dios me ha estado recordando que Idolatría es poner mi confianza en algo o alguien que no es Dios. Idolo es a lo que se dedica la confianza y la atención y que se la quita a Dios, quien debe ser el centro y lo principal en la vida.

## 2. Texto: 57:3-13

**3** Mas vosotros llegaos acá, hijos de la hechicera, generación del adúltero y de la fornicaria. **4** ¿De quién os habéis burlado? ¿Contra quién ensanchasteis la boca, y alargasteis la lengua? ¿No sois vosotros hijos rebeldes, generación mentirosa, **5** que os enfervorizáis con los ídolos debajo de todo árbol frondoso, que sacrificáis los hijos en los valles, debajo de los peñascos? **6** En las piedras lisas del valle está tu parte; ellas, ellas son tu suerte; y a ellas derramaste libación, y ofreciste presente. ¿No habré de castigar estas cosas? **7** Sobre el monte alto y empinado pusiste tu cama; allí también subiste a hacer sacrificio. **8** Y tras la puerta y el umbral pusiste tu recuerdo; porque a otro, y no a mí, te descubriste, y subiste, y ensanchaste tu cama, e hiciste con ellos pacto; amaste su cama dondequiera que la veías. **9** Y fuiste al rey con ungüento, y multiplicaste tus perfumes, y enviaste tus embajadores lejos, y te abatiste hasta la profundidad del Seol. **10** En la multitud de tus caminos te cansaste, pero no dijiste: No hay remedio; hallaste nuevo vigor en tu mano, por tanto, no te desalentaste.

**11** ¿Y de quién te asustaste y temiste, que has faltado a la fe, y no te has acordado de mí, ni te vino al pensamiento? ¿No he guardado silencio desde tiempos antiguos, y nunca me has temido? **12** Yo publicaré tu justicia y tus obras, que no te aprovecharán.

**13** Cuando clames, que te libren tus ídolos; pero a todos ellos llevará el viento, un soplo los arrebatará; mas el que en mí confía tendrá la tierra por heredad, y poseerá mi santo monte.

## 3. Contexto

Isaías 1:1 dice “Visión de Isaías hijo de Amoz, la cual vio acerca de Judá y Jerusalén en días de Uzías, Jotam, Acaz y Ezequías, reyes de Judá.” Veamos detalles de estos reyes:

| --- | --- | --- |
| Rey | Libro de la Biblia | Resumen |
| Uzías también llamado Azarías | 2 Reyes 15:1-7 ![](https://upload.wikimedia.org/wikipedia/commons/d/dc/Ozias-Uzziah.jpg) | Hizo lo recto, aunque la gente siguió idolatrando en los lugares altos. Por su soberbia fue castigado con lepra y mientras la tuvo su hijo Jotam co-reino con él. |
| Jotam | 2 Reyes 15:32-38 ![](https://upload.wikimedia.org/wikipedia/commons/c/cb/Joatham_rex.jpg) | Hizo lo recto, aumentó la puerta del templo, pero no quitó los lugares altos donde el pueblo idolatraba, quemando incienso a falsos dioses. |
| Acaz | 2 Reyes 16 ![](https://upload.wikimedia.org/wikipedia/commons/7/74/Ahaz.jpg) | No hizo lo recto. Idolatró abiertamente. Para defenderse del ataque de Siria e Israel le pagó tributo al rey de Asiria Tiglat-pileser. Quien primero atacó a los Sirios y se los llevó exiliados, posteriormente en el 733 atacó e hizo vasallos a los de Israel del Norte (más adelante en el 722 Asiria destruyó a Israel del Norte y deportó a muchos). Acaz cambió el diseño del templo, que Dios había dado, para que fuera como el de Asiria. |
| Ezequías | 2 Reyes 18-20 ![](https://upload.wikimedia.org/wikipedia/commons/a/a2/Ezechias-Hezekiah.png) | No siguió los caminos de su papá sino que hizo lo bueno. Asediado por Senaquerib buscó a Dios de corazón y con ayuda del profeta Isaías. Dios los libró milagrosamente de Senaquerib enviando un angel que en una noche mató a 185.000 soldados Asirios |

Los iconos de los Reyes provienen de Wikipedia Portugal, e.g <https://pt.wikipedia.org/wiki/Uzias>


Tradujimos y ampliamos una cronología de los reyes contemporaneos con Isaías:
![](/assets/images/cronologia_Isaias.png)

Ver más detalle en:
<https://www.preceden.com/timelines/821198-cronolog-a-y-l-nea-de-tiempo-de-isa-as-con-base-en-https-www-preceden-com-timelines-216718-chronology-and-timeline-of>


## 4. Análisis

| --- | --- |
| Versículos | Observación |
| 3 | Adúlteros y fornicarios, por lo menos espiritualmente porque en el pueblo no paró de idolatrar y el rey Acaz lo hizo abiertamente. La bisabuela de Acaz fue Atalia, que al parecer era hija de Jezabel y Acab, adoradores de Baal en el tiempo del profeta Elías. |
| 4 | Al idolatrar se va perdiendo primero la atención a Dios y luego la reverencia y respeto que Él merece. |
| 5-6 | 2 Reyes 16:3 Acaz … “hizo pasar por fuego a su hijo … sacrificó y quemó incienso en los lugares altos, y sobre los collados, y debajo de todo árbol frondoso.” |
| 7 | 2 Reyes 15:4 durante el reinado de Uzías “Con todo eso, los lugares altos no se quitaron, porque el pueblo sacrificaba aún y quemaba incienso en los lugares altos.” |
| 8-9 | Dios aquí y por ejemplo en Oseas y a lo largo de la Biblia (e.g Efesios 5:25) compara la idolatría con la infidelidad en el matrimonio. Se trata de romper la promesa de amor incondicional que la esposa/pueblo le hace a su marido/Dios. |
| 10 | Tristemente quien elige la idolatría al ver los malos resultados que da, vuelve a idolatrar en un círculo vicioso. |
| 11-12 | Dios no suele obrar al instante frente a la idolatría, pero su silencio no significa que no la vea o que la acepte, sino que la publicará y juzgará en su tiempo. |
| 13 | Y esos ídolos no librarán del juicio al idólatra, pero para los fieles al Señor habrá buena recompensa. |


## 5. Conclusión y oración

Hay muchas cosas o personas a las que les pudiéramos dar más atención que a Dios:

* Dinero, Trabajo
* Lujuria,
* Tecnología, Diversión, famosos, fútbol, etc.
* Vicio
* Esposo/a, novio/a, amigo/a

Piensalo tu. ¿A qué le estás dedicando la mayoría de tu tiempo? ¿Qué te está quitando la atención que debería ser para Dios?

La infidelidad en el matrimonio evidencia 2 infidelidades: primero a Dios porque no hay temor a su mandamiento y eso revela una primera idolatría, después a la esposa/o. Así como se le rompe el corazón a un esposo/a que sufre una infidelidad, semejante se lastima el corazón de Dios cuando idolatramos, pues nos ama más que uno esposo/a, con amor infinito, inquebrantable y puro.

Señor perdónanos cuando no hemos puesto nuestra confianza y atención en ti, cuando se la hemos dedicado a cosas o personas. Por favor revelanos y ayúdanos a serte fieles, pedimos en el nombre de Jesús.

Ayúdanos a serte fieles y persistentes, en tiempos de angustia pero también en tiempos de tranquilidad o regocijo, en el nombre de Jesús.

Hoy que recordamos el domingo de ramos te damos la bienvenida aquí Señor y a nuestra vida y decimos Osana, bendito el que viene en el nombre de Jesús.

Herman@s dediquemos tiempo a la alabanza diaria, a la lectura de la palabra, a la oración, mañana y noche por lo menos, pero mejor de manera continúa; ayunemos, búsquemos a Dios “buscadme y me hallaréis”. Cuando no queramos invertir tiempo con Dios reprendamos en el nombre de Jesús. No dejemos de congregarnos que es otra forma de recargarnos. Compartamos el evangelio con quienes no conocen. Cuando podamos servir en la iglesia hagámoslo.

## 6. Referencias Bibliográficas

* \[RV1960\] Biblia. Traducción Reina Valera 1960. [https://biblegateway.com](https://biblegateway.com "https://biblegateway.com")
* [https://www.preceden.com/timelines/216718-chronology-and-timeline-of-isaiah](https://www.preceden.com/timelines/216718-chronology-and-timeline-of-isaiah "https://www.preceden.com/timelines/216718-chronology-and-timeline-of-isaiah")
* [https://www.christianity.com/bible/commentary/geneva/isaiah/57](https://www.christianity.com/bible/commentary/geneva/isaiah/57 "https://www.christianity.com/bible/commentary/geneva/isaiah/57")
* Cronología de Reyes. <https://fe.pasosdejesus.org/Cronologia_Reyes/>

---

Imagen de dominio público de
<https://openclipart.org/detail/283301/bed-from-glitch>
