---
layout: post
title: "Caso_Santo_Domingo"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Están disponibles los videos de la audiencia pública sobre excepciones preliminares, eventuales fondo y reparaciones ante la CIDH en el caso del bombardeo a Santo Domingo el 13 de Diciembre de 1998.

* Parte 1: http://vimeo.com/44867576
* Parte 2: http://vimeo.com/44867591 
* Parte 3: http://vimeo.com/44902426
* Parte 4: http://vimeo.com/44902425

Como declarantes de las víctimas estuvieron los sobrevivientes Alba Janet García y Marcos Neite González.  

Como testigo del estado estuvo el ex-general de la armada Jairo García Camargo.

Como perito sobre DIH estuvo Alejandro Valencia Villa que conceptuó que el estado había incurrido en violación a DH y también al DIH. 

El presidente de la corte fue el juez García-Sayán, los otros jueces fueron Vio Grossi, Abreu Blondet, Macaulay, Ventura Robles y Franco ---por cierto este último preguntó a la sobreviviente Alba Janet García sobre su "otra parte" en mi humilde opinión insinuando que ella había sido guerrillera, insinuación que ni siquiera los abogados del estado se atrevieron a hacer.

Tras 10 años de este proceso el Estado colombiano con su abogado Dr. Rafael Nieto, intentó resucitar una hipótesis que inculpaba a la guerrilla de haber detonado un camion bomba el cual habría matado a las 17 personas y dejado heridas a 25.   La hipótesis que ha manejado la fiscalia y que el Estado había manejado en este proceso y otros de jurisdicción interna es que habría sido una bomba en racimo o cluster lanzada desde una areonave colombiana.   

En este proceso se dieron detalles sobre la bomba que según el Estado para estas audiencias si fue lanzada pero no sobre el cacerio sino sobre una mata de monte cercana.  Se trata de una bomba cluster An-M1A2:

[http://fe.pasosdeJesus.org/sites/FE.PASOSDEJESUS.ORG/spages/A2046UF052.jpg]

Como se ve en {1} consta de un armazón con 6 bombeletas,  termino empleado por el testigo Camargo, estas 6 bomkkbeletas se separan en el aire y cada una impacta en un lugar diferente lanzando miles de esquirlas asesinando a personas cercanas e hiriendo gravemente a otras más lejandas.  El testigo Camargo fue un funcionario de la armada por más de 30 años pero no experto en bombas (como brillantemente evidenció la jueza Macaulay), y sin embargo tuvo a cargo la "inspección" de la justicial penal militar unos días tras el bombardeo. Según el testigo Camargo en el pueblo no encontró personas, ni heridos, ni crateres, ni restos de las bombas o de sangre, nada, el pueblo estaba deshabitado e intacto según su testimonio.

El abogado del estado presentó un croquis en el que pretendía mostrar como imposible el asesinato de las 17 personas y los 25 heridos con las bombeletas, pero ubicó la posible área de impacto sacada del manual de las bombeletas en la rotación que él prefirió intentando disminuir el número de personas que habrían muerto o resultado heridas e intentando darle fuerza así a su hipótesis del camión bomba  ---del cual no intentó analizar el área de alcance y la ubicación de las víctimas.

El abogado del estado repetía con fotos supuestamente tomadas desde un avión tras los bombardeos que no había evidencias de explosiones, ni de heridos, que ni siquiera su sangre se veía.   Sin embargo los 17 muertos y 25 heridos efectivamente murieron y fueron heridos, su sangre que tuvo que quedar en ese suelo de Santo Domingo clama y exige verdad.


##REFERENCIAS

* {1} http://maic.jmu.edu/ordata/srdetaildesc.asp?ordid=3769
