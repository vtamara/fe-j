---
layout: post
title: "El centro de la Biblia es Jesucristo (1)"
author: vtamara
categories:
image: assets/images/Jesus_centro.png
tags:
---

# El centro de la Biblia es Jesucristo


Lucas 24:44-49 (de {5})
<pre>
Y les dijo: Estas son las palabras que os hablé, 
estando aún con vosotros: que era necesario que 
se cumpliese todo lo que está escrito de mí en 
la ley de Moisés, en los profetas y en los salmos.

Entonces les abrió el entendimiento, para que 
comprendiesen las Escrituras; y les dijo: Así está
 escrito, y así fue necesario que el Cristo 
padeciese, y resucitase de los muertos al tercer día; 
y que se predicase en su nombre el arrepentimiento 
y el perdón de pecados en todas las naciones, comenzando 
desde Jerusalén.  
Y vosotros sois testigos de estas cosas.

He aquí, yo enviaré la promesa de mi Padre sobre vosotros; 
pero quedaos vosotros en la ciudad de Jerusalén, hasta 
que seáis investidos de poder desde lo alto. 
</pre>


Entonces Jesucristo mismo nos explica que en el antiguo testamento había profecías sobre Él.   
En {1} se referencian 68 profecías que describen la vida y ministerio del Señor, algunas aparecen
varias veces en diferentes partes del Antiguo Testamento, y en algunos versículos del nuevo testamento se hace 
referencia a esas profecías.  

A continuación presentamos algunos ejemplos a partir de los versículos de Lucas:


## "lo que está escrito de mí en la ley de Moisés":

Los 5 primeros libros de la Biblia, o pentateuco, se le atribuyen a Moises y el final del último a su sucesor (Josúe). 

En Deuteronomio 18:15, Moisés habla al pueblo de Israel anunciando que vendría un sucesor de él:
<pre>
Profeta de en medio de ti, de tus hermanos, como yo, 
te levantará Jehová tu Dios; a él oiréis;
</pre>

Además que estos versículos eran profecía cumplida por Jesús se confirma en el nuevo testamento en Hechos 3:20-22

<pre>
y Él envío a Jesucristo, que os fue antes anunciado;
a quien de cierto es necesario que el cielo
reciba hasta los tiempos de la restauración de todas 
las cosas, de que habló Dios por boca de 
sus santos profetas que han sido desde tiempo antiguo. 
Porque Moisés dijo a los padres: El Señor vuestro Dios
os levantará profeta de entre vuestros hermanos, como 
a mí; a él oiréis en todas las cosas que os hable;
</pre>


Otras profecías en el pentateuco son


* Él sería un descendiente de Abraham. Génesis 12:1-3; 18:18; 22:18; Mateo 1:1-2,17; Gálatas 3:8,16
* Él sería de la tribu de Judá. Génesis 49:8-10; Hebreos 7:14; Apocalipsis 5:5
  

## "lo que está escrito de mí en ... los profetas":

Los 39 libros del antiguo testamento (canón breve o hebreo) están ordenados así ({2}):

* Pentateuco (5): Génesis, Exodo, Levítico, Números, Deuteronomio
* Históricos (12): Josué, Jueces, Ruth, I Samuel, II Samuel, I Reyes, II Reyes,   I Crónicas, II Crónicas, Esdras, Nehemías, Ester
* Poéticos y Sapienciales (5): Job, Salmos, Proverbios, Eclesiastés, 
  El Cantar de los Cantares, 
* Profetas Mayores (5): Isaías, Jeremías, Lamentaciones, Ezequiel, Daniel
* Profetas menores (12): Oseas, Joel, Amós, Abdías, Jonás, Miqueas,
  Nahúm, Habacuc, Sofonías, Ageo, Zacarías, Malaquías

Se profetizaron eventos narrados en los evangelios como el sitio de nacimiento:

* Miqueas 5:2
<pre>
Pero tú, Belén Efrata, pequeña para estar entre las 
familias de Judá, de ti me saldrá el que será
Señor en Israel;(A) y sus salidas son desde el principio,
desde los días de la eternidad.
</pre>
* Mateo 2:4-6
<pre>
Y convocados todos los principales sacerdotes, 
y los escribas del pueblo, les preguntó dónde
había de nacer el Cristo.

Ellos le dijeron: En Belén de Judea; porque así 
está escrito por el profeta:
    
Y tú, Belén, de la tierra de Judá, 
No eres la más pequeña entre los príncipes de Judá; 
Porque de ti saldrá un guiador, 
Que apacentará[a] a mi pueblo Israel.
</pre>

y el posterior asesinato de bebes por parte de Herodes:

* Jeremias 31:15
<pre>
Así ha dicho Jehová: Voz fue oída en Ramá, 
llanto y lloro amargo; Raquel que lamenta por sus
hijos, y no quiso ser consolada acerca de sus hijos, 
porque perecieron.
</pre>
* Mateo 2:16-18
<pre>
Herodes entonces, cuando se vio burlado por los magos, 
se enojó mucho, y mandó matar a todos los niños menores 
de dos años que había en Belén y en todos sus alrededores, 
conforme al tiempo que había inquirido de los magos.

Entonces se cumplió lo que fue dicho por el profeta Jeremías, 
cuando dijo:

    Voz fue oída en Ramá, 
    Grande lamentación, lloro y gemido; 
    Raquel que llora a sus hijos, 
    Y no quiso ser consolada, porque perecieron.
</pre>

Pero son muchos otros eventos los profetizados por ejemplo:
* Él nacería de una virgen.  (Isaías 7:14; Mateo 1:20-23; Gálatas 4:4; Génesis 3:15)  
*  Él iría a Egipto y volvería a la tierra de Israel.  (Oseas 11:1; Mateo 2:13-15)  
*  Él sería precedido por un mensajero  quién prepararía el camino del Señor. (Malaquías 3:1; Lucas 1:13,76; Mateo 11:7,10)  
*  El mensajero estaría predicando en el desierto.  (Isaías 40:3-5; Lucas 1:13,80,3:2-6)  
* Él sería ungido del Espíritu Santo   (Isaías 11:1-2; 42:1; Mateo 3:16)  
*  Él predicaría y enseñaría en el templo  (Malaquías 3:1; Lucas 4:16; Mateo 26:55; Juan 7:28; 8:1-2)  
*  Sería recibido por los Gentiles  (Isaías 11:10; 42:6; 49:6,22; 54:3; 60:3,5,11,16; 61:6,9; 62:2; 66:12,19; Malaquías 1:11; Lucas 2:30-32; Hechos 28:28)  
*  Su ministerio estaría en Galilea.  (Isaías 9:1-2; Mateo 4:12-16,23)  
*  Su ministerio sería sanar al enfermo, poner en libertad a los cautivos, y predicar la liberación (Isaías 61:1-2; Lucas 4:16-21; Mateo 4:23; 9:34-35; Hechos 2:22; 10:38)  
*  Sería el pastor de Israel porque Israel no tenía ningún pastor.  (Ezequiel 34:5-10; 1 Reyes 22:17; Zacarías 10:2; Génesis 49:22,24; Salmo 23:1; 80:1; Isaías 40:10-11; Ezequiel 34:23-24; 37:24; Juan 10:11,14-15)  
* Su mensaje no sería creído.  (Isaías 53:1; Juan 12:37-38)  
*  Él no buscaría la publicidad.  (Isaías 42:1-2; Mateo 12:15-19; 9:30; 8:4)  
*  En él se puede confiar y sería compasivo. (Isaías 42:3; Mateo 12:15,20-21)  
* Ninguna palabra mala procedería de Su boca.   (Isaías 53:9; Lucas 23:41; 1 Pedro 2:21-22; 2 Corintios 5:21)  
*  Sus discípulos lo desampararían.  (Zacarías 13:7; Mateo 26:31-35,56)  
*  No había nada físicamente bonito en Él como para ser deseado.  (Isaías 53:2; Salmo 22:6; Marcos 6:1-3; filipenses 2:7)  
* Él entraría en Jerusalén públicamente antes del tiempo de Su crucifixión. (Zacarías 9:9; Mateo 21:1-5)  
* Él montaría en Jerusalén en un asno.  (Zacarías 9:9; Mateo 21:5) 
* Él sería traicionado por 30 piezas de plata. (Zacarías 11:12; Mateo 26:14-16)  
* Las monedas de la traición finalmente se darían para el campo de un alfarero.  (Zacarías 11:13; Mateo 27:3,7-10)  


## "lo que está escrito de mí en ... los Salmos":


Entre bastantes, un ejemplo fue la profecía sobre la repartición de sus vestidos en el Salmo 22:18

* Salmo 22:18
<pre>
Repartieron entre sí mis vestidos, 
Y sobre mi ropa echaron suertes.
</pre>
* Mateo 27:35
<pre>
Cuando le hubieron crucificado, repartieron entre sí 
sus vestidos, echando
suertes, para que se cumpliese lo dicho por el profeta: 
    Partieron entre sí mis vestidos, 
    y sobre mi ropa echaron suertes.
</pre>

Pero también pueden encontrarse en Salmo 34:20, 22:1, 69:21, 2:6, 22:7-8, 16:10, 49:15, 56:13,  16:10, 68:18,  entre muchos otros.


## "Así está escrito, y así fue necesario que el Cristo padeciese, y resucitase"

Por ejemplo el Salmo 68:18 dice:
<pre>
Subiste a lo alto, cautivaste la cautividad, 
Tomaste dones para los hombres,
Y también para los rebeldes, para que habite 
entre ellos JAH Dios.
</pre>

y con más precisión Isaías profetizó 700 años antes como aparece en Isaías 53:3-12

<pre>
Despreciado y desechado entre los hombres, 
varón de dolores, experimentado en quebranto; y 
como que escondimos de él el rostro, fue menospreciado, 
y no lo estimamos.

Ciertamente llevó él nuestras enfermedades, 
y sufrió nuestros dolores; y nosotros le tuvimos
 por azotado, por herido de Dios y abatido.

Mas él herido fue por nuestras rebeliones, 
molido por nuestros pecados; el castigo de nuestra
paz fue sobre él, y por su llaga fuimos nosotros curados.

Todos nosotros nos descarriamos como ovejas, 
cada cual se apartó por su camino; mas 
Jehová cargó en él el pecado de todos nosotros.

Angustiado él, y afligido, no abrió su boca; 
como cordero fue llevado al matadero; y como 
oveja delante de sus trasquiladores, enmudeció, 
y no abrió su boca.

Por cárcel y por juicio fue quitado; y su generación, 
¿quién la contará? Porque fue cortado de la tierra 
de los vivientes, y por la rebelión de mi pueblo fue herido.

Y se dispuso con los impíos su sepultura, mas con 
los ricos fue en su muerte; aunque nunca 
hizo maldad, ni hubo engaño en su boca.

Con todo eso, Jehová quiso quebrantarlo, sujetándole 
a padecimiento. Cuando haya puesto su vida en expiación 
por el pecado, verá linaje, vivirá por largos días, 
y la voluntad de Jehová será en su mano prosperada.

Verá el fruto de la aflicción de su alma, y quedará satisfecho; 
por su conocimiento justificará mi siervo justo a muchos, 
y llevará las iniquidades de ellos.

Por tanto, yo le daré parte con los grandes, y con los 
fuertes repartirá despojos; por cuanto derramó su vida 
hasta la muerte, y fue contado con los pecadores, 
habiendo él llevado el  pecado de muchos, y orado por 
los transgresores.
</pre>


## "y que se predicase en su nombre el arrepentimiento y el perdón de pecados en todas las naciones"

Justamente esto es lo que se describe en el nuevo Testamento, como los primeros discípulos y sus sucesores comenzaron a hacer esa gran comisión de dar a conocer el plan de salvación del Creador.

Además de profecías se encuentran símbolos o tipos a lo largo de la Biblia sobre el plan de salvación (por ejemplo el arca de Noe es como un símbolo del Señor Jesucristo pues quienes entren en Él serán salvos) e incluso en tradiciones judias como las fiestas {3} (por ejemplo la pascua).

Entonces la Biblia completa es un diseño increíble para transmitir el plan de Salvación, que Dios mismo, el Señor Jesús llevaría sobre si el pecado de todos para darnos salvación a quienes creamos.


## REFERENCIAS

* {1} http://jesus-is-the-way.com/FirstComingEsp.html
* {2} http://www.ingfems.com/paginas/biblia.rtf
* {3} http://dabarqodesh.com/fiestas.pdf
* {4} http://apologista.blogdiario.com/1218299880/
* {5} Biblia. Reina Valera 1960. http://www.biblegateway.com/versions/?action=getVersionInfo&vid=60
