---
layout: post
title: "Agradecimiento"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
"La Dra. Helen Roseveare, misionera durante mucho tiempo en el Congo, contaba un episodio de su vida durante el cual sufrió un vergonzoso maltrato por parte de soldados rebeldes. Describía la lucha tan dolorosa que libró dentro de su corazón tratando de encontrarle sentido a la humillación y al sufrimiento físico que había padecido. Luego relató el descanso y la paz que le sobrevinieron al sentir a Dios preguntándole: 'Helen ¿estarías dispuesta a agradecerme aquellas cosas que nunca te concederé el privilegio de entender?'  Haga una lista de circunstancias, eventos, pruebas o relaciones que hayan ocurrido en el pasado o estén ocurriendo en la actualidad y por las cuales no haya tenido todavía la oportunidad de dar gracias. Luego, como expresión de fe y ejercicio de obediencia diga: 'Señor, te doy las gracias por ____ y por ____, circunstancias que tal vez nunca me concedas el privilegio de entender".  Al hacerlo estará reconociendo que Dios es el 'Bendito Controlador' de todas las circunstancias que rodean su vida, y que usted confía en Él y en Su soberana elección para su vida."  {2}


De {3} Dé gracias a Dios en cada situación. Está en la Biblia, 1 Tesalonicenses 5:18, "Dad gracias en todo, porque esta es la voluntad de Dios para con vosotros en Cristo Jesús. ".

"dando siempre gracias por todo al Dios y Padre, en el nombre de nuestro Señor Jesucristo" Efesios 5:20

De {4} 

* La gratitud se puede, y debería ser expresado verbalmente.
* La gratitud es algo que se puede expresar a través de actos de servicio.
* La gratitud se puede, y debería ser expresado por medio del dar regalos y apoyo material. 
* La gratitud también se puede expresar por medio de la oración.

BIBLIOGRAFÍA

* {1} Biblia. RV1960.  http://www.biblegateway.com/passage/?search=1+Tesalonicenses+5&version=RVR1960
* {2} La Gratitud. El Perdón. Nancy Leigh DeMoss. Portavoz. 2006.
* {3} http://www.bibleinfo.com/es/topics/agradecimiento
* {4} http://www.scribd.com/doc/3199134/EL-PODER-DE-LA-GRATITUD
