---
layout: post
categories:
- Predica
title: El nuevo pacto profetizado en Isaías 55:1-5
author: vtamara
image: "/assets/images/pan_y_vino.png"

---
# El nuevo pacto profetizado en Isaías 55:1-5

## Iglesia Menonita de Suba.
[vtamara@pasosdeJesus.org](mailto:vtamara@pasosdeJesus.org). 27.Mar.2024.
Domínio público

Imagen de dominio público disponible en
[https://openclipart.org/detail/12670/bread-and-wine](https://openclipart.org/detail/12670/bread-and-wine) 


# 1. Introducción

El domingo pasado por casualidad en una ciudad a la que fui momentáneamente
(Savannah) estuve en una prédica  (iglesia Faith and Miracles Church, 
predicador Ariel Baszkir) donde el Señor me recordó lo fiel y estable 
que es Su palabra. Lo que Él nos dice en la Biblia, en oración o en 
profecía Él lo cumple.  Y buscando sobre esa estabilidad y fidelidad de la 
Palabra de Dios, llegué a Isaías 55 donde el Señor me recordó el pacto en el 
que estamos los cristianos hoy de una manera nueva, y resulta muy preciso 
para semana santa.


# 2. Texto. Isaías 55:1-5 

<sup>1</sup> A todos los sedientos: Venid a las aguas; y los que no tienen
dinero, venid, comprad y comed. Venid, comprad sin dinero y sin precio, vino 
y leche. <sup>2</sup> ¿Por qué gastáis el dinero en lo que no es pan, y 
vuestro trabajo en lo que no sacia? Oídme atentamente, y comed del bien, 
y se deleitará vuestra alma con grosura. <sup>3</sup> Inclinad vuestro oído, 
y venid a mí; oíd, y vivirá vuestra alma; y haré con vosotros pacto eterno,
las misericordias firmes a David. <sup>4</sup> He aquí que yo lo di por 
testigo a los pueblos, por jefe y por maestro a las naciones.
 <sup>5</sup> He aquí, llamarás a gente que no conociste, y gentes que no 
te conocieron correrán a ti, por causa de Jehová tu Dios, y del Santo de 
Israel que te ha honrado. 

<table>
  <tr>
   <td>
    <img src="/assets/images/vino.png" width="" alt="vino" title="Vino">

     Imagen de dominio público disponible en <a
href="https://openclipart.org/image/400px/292939">https://openclipart.org/image/400px/292939</a> 
   </td>
   <td>
    <img src="/assets/images/leche.png" width="" alt="leche" title="Leche">

     Imagen de dominio público disponible en <a
href="https://openclipart.org/image/400px/292987">https://openclipart.org/image/400px/292987</a>

   </td>
  </tr>
</table>


# 3. Contexto

## 3.1. David

Para entender más las misericordias firmes de David, recordemos que  David que
había sido pastor, valiente en el Señor para enfrentar y derrotar a Goliat y
después designado rey de Israel.

![David y Goliat](/assets/images/david_y_goliat.png)

Imagen de dominio público disponible en <a
href="https://openclipart.org/image/400px/288199">https://openclipart.org/image/400px/288199</a>

Como rey recibió una promesa de Dios por intermedio del profeta Natán 
que quedó registrada en 2 Samuel 7:12-17.  

<blockquote>

<sup>12</sup> Y cuando tus días sean cumplidos, y duermas con tus padres, yo
levantaré después de ti a uno de tu linaje, el cual procederá de tus entrañas, y
afirmaré su reino. <sup>13</sup> Él edificará casa a mi nombre, y yo afirmaré
para siempre el trono de su reino. <sup>14</sup> Yo le seré a él padre, y él me
será a mí hijo. Y si él hiciere mal, yo le castigaré con vara de hombres, y con
azotes de hijos de hombres; <sup>15</sup> pero mi misericordia no se apartará de
él como la aparté de Saúl, al cual quité de delante de ti. <sup>16</sup> Y será
afirmada tu casa y tu reino para siempre delante de tu rostro, y tu trono será
estable eternamente. <sup>17</sup> Conforme a todas estas palabras, y conforme a
toda esta visión, así habló Natán a David.
</blockquote>

Dios prometió a David que mantendría su reino de manera eterna porque 
mediante su descendiente Jesús lo establecería como Reino de Dios.  
Podemos ver a David entre los ascendientes de Jesús en la genealogía 
presentada por ejemplo en Mt. 1:1-17.


## 3.2. El nuevo pacto

En Mt. 26:26-29 (que es paralelo a Mc. 14:22-25 y Lc. 22:17-20), Jesús 
habla del nuevo pacto, justamente durante la cena pascual que celebraba 
con sus discípulos, cuando entonces tomaban vino y comían pan sin levadura, 
hierbas amargas y cordero.  Conmemoraba la liberación del pueblo judío de 
Egipto en tiempos de Moisés --hoy en día los judios siguen celebran esa 
cena a la que llaman séder con esos mismos ingredientes.  El tiempo hoy 
para los cristianos corresponde a semana santa,  el cordero simboliza a 
Jesús sacrificado por nosotros.


![séder](/assets/images/seder.jpg "seder.jpg")

Foto de un séder judio actual tipico, con vino, hierbas amargas, cordero y
pan sin levadura
[https://en.m.wikipedia.org/wiki/File:Passover_Seder_plate_with_wine_and_matzot.jpg](https://en.m.wikipedia.org/wiki/File:Passover_Seder_plate_with_wine_and_matzot.jpg) 


<blockquote>
<sup>26</sup> Y mientras comían, tomó Jesús el pan, y bendijo, y lo partió,
y dio a sus discípulos, y dijo: Tomad, comed; esto es mi cuerpo. 
<sup>27</sup> Y tomando la copa, y habiendo dado gracias, les dio, diciendo: 
Bebed de ella
todos; <sup>28</sup> porque esto es mi sangre del nuevo pacto, que por 
muchos es derramada para remisión de los pecados. <sup>29</sup> Y os digo 
que desde ahora no beberé más de este fruto de la vid, hasta aquel día en 
que lo beba nuevo con vosotros en el reino de mi Padre.
</blockquote>

Un pacto es un acuerdo entre dos en el que cada uno hace compromisos, 
aquí Jesús nos dice que su parte del pacto es (1) derramar su sangre para el 
perdón de pecados de quien pacte, (2) ser pan o alimento de quien pacte, 
y en realidad es más que alimento porque es su presencia viva en nosotros 
por su Espíritu Santo. Ya veremos cual es la parte que le corresponde a 
quien pacta.


# 4. Análisis

Matthew Henry indica que estos versículos profetizan como sería el nuevo 
pacto. Analicemos uno a uno citando a ese autor:

1. La invitación al nuevo pacto con Cristo, es para todos, inicialmente era
   sólo para los judios, pero tras Jeśus para gentiles, pobres, lisiados, 
   ciegos.
   Todos. Sólo se requiere tener sed, no es para los que están satisfechos 
   con el mundo y con lo que tienen. Es para los que están cansados y 
   sedientos.  Los que vengan recibirán el agua de vida que es Cristo 
   y su Santo Espíritu.
  "Vengan a las aguas sanadoras; vengan al agua viva. Quien quiera, que venga y
   tome parte del las aguas de vida (Apo. 22:17).  Nuestro salvador se se 
   refería a esto en Juan 7:37 'Si alguno tiene sed, venga a mí y beba'." 

   Que debemos hacer: Invita a comprar y comer, es decir a pagar con
   arrepentimiento y obediencia a lo que Cristo ordena, sin rebajas, para 
   hacerlo nuestro como si fuese comida.

   Que recibiremos: Dará leche y vino para nutrirnos y para alegrarnos.  
   "Lo que se nos ofrece ya fue comprado y pagado. Cristo lo compró al 
    precio completo, no con dinero sino con su propia sangre. Para ser 
    bienvenidos a los beneficios de la promesa, aunque somos indignos 
    de estos"

2. Las cosas de este mundo no son pan ni alimento para un alma; no nutren ni
   refrescan el espíritu. "Todas las riquezas y placeres del mundo no serán 
   ni una comida para un alma. La verdad eterna y el bien eterno son la única 
   comida para alma racional e inmortal, la vida que consiste en 
   reconciliación y conformidad con Dios, y unión y comunión con Él."

   "Los que escuchan diligente y obedientemente a Cristo tendrán las buenas
   palabras y promesas de Dios, una conciencia limpia y el consuelo del 
   Espíritu de Dios, un banquete continuo".

3. Dios nos asegura que si vamos a Él y le servimos hará un pacto eterno con
   nosotros, nos hará felices y con la misericordia como la necesitamos, en el
   pacto eterno que prometió a David y que cumplió mediante Cristo.

4. Cristo será nuestro lider, nuestro comandante para guiarnos en la vida.

5. La gente correrá a Cristo por ser hijo de Dios, por morir y resucitar,
   por pagar por nuestros pecados con su sangre para dar vida eterna a 
   quienes lo sigamos, por ser  el único mediador entre Dios y los hombres


# 5. Conclusión y oración

Estos versículos escritos 700 años antes de Cristo, describen el nuevo 
pacto con Jesús,  mencionando explícitamente los dos elementos simbólicos 
usados por Jesús el pan y el vino y haciendo invitación a escuchar a 
Dios --y obedecerlo-- y a servirle proclamando su mensaje, me recuerda 
la invitación que Cristo le hizo a Pedro a ser pescador de hombres 
--tras el reconocimiento de parte de Pedro que era pecador, ver 
Lc. 5:8-10.

Hoy el Señor nos recuerda ese llamado a ser pescadores de hombres siempre que
nos arrepintamos por lo que no hemos hecho bien y que vayamos a él como nuestro
comandante y líder a quien seguir e imitar.

En el pacto que Dios nos propone, (1) nos da a Jesús --el pan de vida-- y Su
Santo Espíritu para vivir  en nosotros, guiarnos, consolarnos y ser mucho más
que alimento continuo y (2) mediante su sangre derramada para perdón de 
nuestros pecados y poder tener reconciliación y unidad con Dios para vivir 
con gozo y felices.  Ya sabemos que Él es fiel para cumplir su parte, 
pero claro un pacto tiene dos partes, esforcémonos y pidámosle ayuda para 
que su Santo Espíritu nos guíe y ayude a cumplir la nuestra que es obedecerle 
y servirle compartiendo Su palabra, siendo pescadores de hombres para Él.

Señor gracias por ser pan de vida y darte por nosotros para que nos 
alimentemos de Ti, de Tu Palabra y de Tu Amor.  Ayúdanos a cumplir nuestra 
parte en obediencia sirviéndote, compartiendo tu palabra y siendo 
pescadores de hombres para Ti, en el nombre de Jesús oramos.


# 6. Referencias Bibliográficas

* [RV1960] Biblia. Traducción Reina Valera 1960.
  [https://biblegateway.com](https://biblegateway.com) 
* Matthew Henry's Commentary.
  [https://www.biblegateway.com/passage/?search=Isaias+55&version=RVR1960](https://www.biblegateway.com/passage/?search=Isaias+55&version=RVR1960) 
* La conexión entre Jesús y el Éxodo en el Seder de Pesaj de la Última Cena
  [https://seminary-grace-edu.translate.goog/connection-between-jesus-and-exodus-in-the-last-supper-passover-seder/?_x_tr_sl=en&_x_tr_tl=es&_x_tr_hl=es&_x_tr_pto=rq#:~:text=On%20the%20night%20of%20the,was%20eating%20at%20the%20first](https://seminary-grace-edu.translate.goog/connection-between-jesus-and-exodus-in-the-last-supper-passover-seder/?_x_tr_sl=en&_x_tr_tl=es&_x_tr_hl=es&_x_tr_pto=rq#:~:text=On%20the%20night%20of%20the,was%20eating%20at%20the%20first) 

´

