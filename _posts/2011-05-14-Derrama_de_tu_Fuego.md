---
layout: post
title: "Derrama_de_tu_Fuego"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
Marcos Witt

<pre>
Grande, poderoso eres tú mi Dios
Fuerte y poderoso eres tú mi Rey
Grande, poderoso eres tú mi Dios
Fuerte y poderoso eres tú mi Rey

Coro:
Derrama de tu fuego, sobrenatural
Derrama de tu gloria, sobre este lugar
Derrama de tu fuego, sobrenatural
Derrama de tu gloria, sobre este lugar
Anhelamos mas de ti Señor
Anhelamos mas de tu amor
Anhelamos mas de ti Señor
Derrama de tu fuego hoy Señor

//Coro//

</pre>

* Letra basada en la de: http://www.letrasmania.com/letras/letras_de_canciones_marcos_witt_49898_letras_sobrenatural_88705_letras_derrama_de_tu_fuego_870785.html
* Video: http://www.youtube.com/watch?v=6RKjXWQnlO8

