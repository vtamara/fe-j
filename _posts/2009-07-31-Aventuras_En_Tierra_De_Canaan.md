---
layout: post
title: "Aventuras_En_Tierra_De_Canaan"
author: vtamara
categories:
image: assets/images/home.jpg
tags:
---
http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/Introduction_The_Land_of_Canaan/

http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/Getting_Ready_to_Enter_Canaan/


http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Crossing_of_the_Jordan/

* El cruce del Jordán
** Consagración: Rendirse. Fin de la rebelión. Poner en su altar, talentos, bienes, alma, cuerpo y voluntad. Morir a todo excepto a Jesús.
** Romanos 12:1
** Si ya pusiste todo ante al altar estás listo para cruzar, tus pies tocan el agua del Jordán.
** Pedir a Dios que limpie alma. Pedirle que mate en mi todo lo que no le gusta. Que restaure su imagen en mi, que entre a su templo, que me llene de Espíritu Santo, que viva en mi por siempre. Creer que me santificar por completo, porque soy suyo y confio en Él.. Somos santificados por fe. Hechos 26:18.  Corazones purificados por fe. 


http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Jordan_Memorial_Stone/

* Recordatorios de las obras de Dios: altares
** Altar en Monte Ebal. Ver [Altar de Josue]
** Antes de ese altar Josue 4:1-24 describe otro en Gilgal hecho con 12 piedras tomadas del medio del Jordán
[http://www.bible-history.com/map-israel-joshua/map-7-nations-of-canaan_shg.jpg]

http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/Exploring_Canaan_by_Faith/

* No dudar consagración

http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/The_Best_Inheritance_in_Canaan/

* La experiencia de consagración es diferente de una persona a otra, no permitir que los testimonios de otras personas aminoren la propia.

http://library.timelesstruths.org/texts/Adventures_in_the_Land_of_Canaan/In_the_Hands_of_Giant_Accuser/

* "La santificación no libra de la tentación, pero nos hace más seguros de ganar sobre estas".  Es probable que tenga más batallas por ganar tras santificarse que antes, hay legiones de seres espirituales que lo enfrentarán en Canaan. Pero tenemos armas espirituales.  Un método de Satanas es arrojar a la mente malas palabras, malos pensamientos y después acusar a la persona de tener esos pensamientos.  Pela!  Nadie puede hacerse fuerte sin pelear. Esas luchas espirituales son prueba de tu habilidad para resistir.
